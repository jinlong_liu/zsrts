/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 50730
Source Host           : localhost:3306
Source Database       : zsrts

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-08-21 22:42:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_bus_info
-- ----------------------------
DROP TABLE IF EXISTS `app_bus_info`;
CREATE TABLE `app_bus_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` varchar(255) DEFAULT NULL,
  `displacement` float DEFAULT NULL,
  `actual_service_life` float DEFAULT NULL,
  `isfixed_parking` int(255) DEFAULT NULL,
  `bus_type` varchar(255) DEFAULT NULL,
  `bus_property` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of app_bus_info
-- ----------------------------

-- ----------------------------
-- Table structure for app_family_information
-- ----------------------------
DROP TABLE IF EXISTS `app_family_information`;
CREATE TABLE `app_family_information` (
  `family_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `family_type` int(255) DEFAULT NULL,
  `relationship_householder` int(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `household_type` int(11) DEFAULT NULL,
  `is_permanent_address` int(255) DEFAULT NULL,
  `income` varchar(255) DEFAULT NULL,
  `profession` int(11) DEFAULT NULL,
  `educational` int(11) DEFAULT NULL,
  `ic_card` varchar(255) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `is_marry` int(11) DEFAULT NULL,
  `profession_custom` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`family_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of app_family_information
-- ----------------------------
INSERT INTO `app_family_information` VALUES ('1', '1', '1', '1', '1', '18', '1', null, null, '123', null, null, null, null, null, null);
INSERT INTO `app_family_information` VALUES ('2', '2', '1', '1', '0', '19', '2', '0', '6', '1', null, null, '青岛科技大学', '1', null, null);

-- ----------------------------
-- Table structure for app_log_info
-- ----------------------------
DROP TABLE IF EXISTS `app_log_info`;
CREATE TABLE `app_log_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `mobile_phone_model` varchar(255) DEFAULT NULL,
  `mobile_phone_manufacturers` varchar(255) DEFAULT NULL,
  `operating_system_version` varchar(255) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `city_code` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of app_log_info
-- ----------------------------

-- ----------------------------
-- Table structure for app_sms
-- ----------------------------
DROP TABLE IF EXISTS `app_sms`;
CREATE TABLE `app_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `city_code` varchar(255) DEFAULT NULL,
  `request_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `verification_code` varchar(255) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_success` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of app_sms
-- ----------------------------

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `district_number` varchar(255) DEFAULT NULL,
  `district_name` varchar(255) DEFAULT NULL,
  `parent_suboffice` varchar(255) DEFAULT NULL,
  `suboffice_number` varchar(255) DEFAULT NULL,
  `suboffice_name` varchar(255) DEFAULT NULL,
  `parent_communiity` varchar(255) DEFAULT NULL,
  `community_number` varchar(255) DEFAULT NULL,
  `community_name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL,
  `everyday_count` int(11) DEFAULT NULL,
  `follow_count` int(11) DEFAULT NULL,
  `submit_count` int(11) DEFAULT NULL,
  `not_complete_count` int(11) DEFAULT NULL,
  `city_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of area
-- ----------------------------

-- ----------------------------
-- Table structure for auth
-- ----------------------------
DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of auth
-- ----------------------------

-- ----------------------------
-- Table structure for bus_info
-- ----------------------------
DROP TABLE IF EXISTS `bus_info`;
CREATE TABLE `bus_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inhabitant_id` int(11) DEFAULT NULL,
  `displacement` float DEFAULT NULL,
  `actual_ervice_life` float(255,0) DEFAULT NULL,
  `isfixed_parking` int(255) DEFAULT NULL,
  `bus_type` varchar(255) DEFAULT NULL,
  `bus_property` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bus_info
-- ----------------------------

-- ----------------------------
-- Table structure for charge
-- ----------------------------
DROP TABLE IF EXISTS `charge`;
CREATE TABLE `charge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `charge_type` int(11) DEFAULT NULL,
  `registered_quantity` int(255) DEFAULT NULL,
  `is_charge_status` int(11) DEFAULT NULL,
  `charge_call_times` int(11) DEFAULT NULL,
  `extended1` varchar(255) DEFAULT NULL,
  `extended2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of charge
-- ----------------------------
INSERT INTO `charge` VALUES ('1', '1', null, null, '1', null, null, '2');

-- ----------------------------
-- Table structure for city_code_url
-- ----------------------------
DROP TABLE IF EXISTS `city_code_url`;
CREATE TABLE `city_code_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_code` varchar(255) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `city_url` varchar(255) DEFAULT NULL,
  `extended_field_one` varchar(255) DEFAULT NULL,
  `extended_field_two` varchar(255) DEFAULT NULL,
  `prize_sign` varchar(255) DEFAULT NULL,
  `extended_field_three` varchar(255) DEFAULT NULL,
  `signature_content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of city_code_url
-- ----------------------------

-- ----------------------------
-- Table structure for famliy_infomation
-- ----------------------------
DROP TABLE IF EXISTS `famliy_infomation`;
CREATE TABLE `famliy_infomation` (
  `family_id` int(11) NOT NULL AUTO_INCREMENT,
  `family_type` int(11) DEFAULT NULL,
  `relationship_householder` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `household_type` int(255) DEFAULT NULL,
  `is_permanent_address` int(11) DEFAULT NULL,
  `income` varchar(255) DEFAULT NULL,
  `profession` varchar(255) DEFAULT NULL,
  `educational` int(11) DEFAULT NULL,
  `is_card` varchar(255) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `is_marry` int(11) DEFAULT NULL,
  PRIMARY KEY (`family_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of famliy_infomation
-- ----------------------------

-- ----------------------------
-- Table structure for inhabitan
-- ----------------------------
DROP TABLE IF EXISTS `inhabitan`;
CREATE TABLE `inhabitan` (
  `inhabitant_id` int(11) NOT NULL AUTO_INCREMENT,
  `inhabitant_number` int(11) DEFAULT NULL,
  `inhabitant_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `bike_count` int(11) DEFAULT NULL,
  `electric_bikes` int(11) DEFAULT NULL,
  PRIMARY KEY (`inhabitant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of inhabitan
-- ----------------------------

-- ----------------------------
-- Table structure for out_kilometre_scene
-- ----------------------------
DROP TABLE IF EXISTS `out_kilometre_scene`;
CREATE TABLE `out_kilometre_scene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kilometre` varchar(255) DEFAULT NULL,
  `scene` varchar(255) DEFAULT NULL,
  `time_total` varchar(255) DEFAULT NULL,
  `time_go` varchar(255) DEFAULT NULL,
  `time_wait` varchar(255) DEFAULT NULL,
  `time_riding` varchar(255) DEFAULT NULL,
  `price_ticket` varchar(255) DEFAULT NULL,
  `extend_one` varchar(255) DEFAULT NULL,
  `extend_two` varchar(255) DEFAULT NULL,
  `cost_total` varchar(255) DEFAULT NULL,
  `cost_working` varchar(255) DEFAULT NULL,
  `transportation` varchar(255) DEFAULT NULL,
  `extend_three` varchar(255) DEFAULT NULL,
  `extend_four` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of out_kilometre_scene
-- ----------------------------

-- ----------------------------
-- Table structure for rechargeable_card
-- ----------------------------
DROP TABLE IF EXISTS `rechargeable_card`;
CREATE TABLE `rechargeable_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recharge_password` varchar(255) DEFAULT NULL,
  `recharge_type` int(11) DEFAULT NULL,
  `valid_state` int(11) DEFAULT NULL,
  `extended1` varchar(255) DEFAULT NULL,
  `extended2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of rechargeable_card
-- ----------------------------

-- ----------------------------
-- Table structure for register_number
-- ----------------------------
DROP TABLE IF EXISTS `register_number`;
CREATE TABLE `register_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `register_type` int(255) DEFAULT NULL,
  `register_num_day` int(255) DEFAULT NULL,
  `register_num_total` int(255) DEFAULT NULL,
  `register_num_actual` int(255) DEFAULT NULL,
  `extend1` varchar(255) DEFAULT NULL,
  `extend2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of register_number
-- ----------------------------

-- ----------------------------
-- Table structure for residents_travel_information
-- ----------------------------
DROP TABLE IF EXISTS `residents_travel_information`;
CREATE TABLE `residents_travel_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inhabitant_id` int(11) DEFAULT NULL,
  `family_id` int(11) DEFAULT NULL,
  `star_trip_time` int(11) DEFAULT NULL,
  `end_trip_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trip_mode` varchar(255) DEFAULT NULL,
  `trip_address` varchar(255) DEFAULT NULL,
  `bus_line` varchar(255) DEFAULT NULL,
  `trip_purposes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of residents_travel_information
-- ----------------------------

-- ----------------------------
-- Table structure for statistics_particular
-- ----------------------------
DROP TABLE IF EXISTS `statistics_particular`;
CREATE TABLE `statistics_particular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) DEFAULT NULL,
  `submit_date` varchar(255) DEFAULT NULL,
  `submitter` varchar(255) DEFAULT NULL,
  `title_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `lon` float DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `family_id` varchar(255) DEFAULT NULL,
  `submit_count` int(255) DEFAULT NULL,
  `submit_stage` int(11) DEFAULT NULL,
  `follow_date` varchar(255) DEFAULT NULL,
  `district_number` varchar(255) DEFAULT NULL,
  `district_name` varchar(255) DEFAULT NULL,
  `parent_suboffice` varchar(255) DEFAULT NULL,
  `suboffice_number` varchar(255) DEFAULT NULL,
  `suboffice_name` varchar(255) DEFAULT NULL,
  `parent_communiity` varchar(255) DEFAULT NULL,
  `community_number` varchar(255) DEFAULT NULL,
  `community_name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `ctiy_code` varchar(255) DEFAULT NULL,
  `is_surveryor` varchar(255) DEFAULT NULL,
  `extened_field_one` varchar(255) DEFAULT NULL,
  `extened_field_two` varchar(255) DEFAULT NULL,
  `extened_field_three` varchar(255) DEFAULT NULL,
  `submit_lon` float DEFAULT NULL,
  `submit_lat` float DEFAULT NULL,
  `position_deviate` varchar(255) DEFAULT NULL,
  `remind_number` int(11) DEFAULT NULL,
  `num_title` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of statistics_particular
-- ----------------------------

-- ----------------------------
-- Table structure for sys_appuser
-- ----------------------------
DROP TABLE IF EXISTS `sys_appuser`;
CREATE TABLE `sys_appuser` (
  `appuser_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `electric_bikes` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` int(255) DEFAULT NULL,
  `Occupation` varchar(255) DEFAULT NULL,
  `family_address` varchar(255) DEFAULT NULL,
  `stag` int(11) DEFAULT NULL,
  `car_count` int(11) DEFAULT NULL,
  `family_mumber_count` int(11) DEFAULT NULL,
  `city_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`appuser_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_appuser
-- ----------------------------
INSERT INTO `sys_appuser` VALUES ('1', '测试1', '123456', '2020-08-20 18:24:06', '1', '13', '1', '苦逼开发', '地球', '1', '1', '4', '1');
INSERT INTO `sys_appuser` VALUES ('2', '刘金龙', '123', '2020-08-20 18:35:40', '2', '15', '0', '苦逼测试', '山东', '2', '2', '2', '2');

-- ----------------------------
-- Table structure for sys_authorization
-- ----------------------------
DROP TABLE IF EXISTS `sys_authorization`;
CREATE TABLE `sys_authorization` (
  `auth_id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_degree` int(255) DEFAULT NULL,
  `auth_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`auth_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_authorization
-- ----------------------------
INSERT INTO `sys_authorization` VALUES ('1', '1', '人员管理权限');
INSERT INTO `sys_authorization` VALUES ('2', '2', '角色管理权限');
INSERT INTO `sys_authorization` VALUES ('3', '3', '市级负责人');
INSERT INTO `sys_authorization` VALUES ('4', '4', '一级负责人');
INSERT INTO `sys_authorization` VALUES ('5', '5', '二级负责人');
INSERT INTO `sys_authorization` VALUES ('6', '6', '三级负责人');
INSERT INTO `sys_authorization` VALUES ('7', '7', '调查员');
INSERT INTO `sys_authorization` VALUES ('8', '8', '区域管理权限');
INSERT INTO `sys_authorization` VALUES ('9', '9', '参数管理权限');
INSERT INTO `sys_authorization` VALUES ('10', '10', '审核管理权限');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `create_date` varchar(255) DEFAULT NULL,
  `describeActor` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', null, null, null, null);
INSERT INTO `sys_role` VALUES ('2', '管理员', '人员管理权限', '2020-08-20 17:36:28', '一级负责人', 'joker');
INSERT INTO `sys_role` VALUES ('3', '超级超级管理员', '全部权限', '2020-08-20 19:22:31', '全部权限', 'joker');

-- ----------------------------
-- Table structure for sys_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_auth`;
CREATE TABLE `sys_role_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_role_id` int(11) DEFAULT NULL,
  `sys_auth_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_role_auth
-- ----------------------------
INSERT INTO `sys_role_auth` VALUES ('1', '1', '3');
INSERT INTO `sys_role_auth` VALUES ('2', '1', '1');
INSERT INTO `sys_role_auth` VALUES ('3', '1', '2');
INSERT INTO `sys_role_auth` VALUES ('4', '2', '1');
INSERT INTO `sys_role_auth` VALUES ('5', '2', '4');
INSERT INTO `sys_role_auth` VALUES ('6', '3', '1');
INSERT INTO `sys_role_auth` VALUES ('7', '3', '2');
INSERT INTO `sys_role_auth` VALUES ('8', '3', '8');
INSERT INTO `sys_role_auth` VALUES ('9', '3', '9');
INSERT INTO `sys_role_auth` VALUES ('10', '3', '10');
INSERT INTO `sys_role_auth` VALUES ('11', '3', '3');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `realName` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `create_date` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `is_super_admin` int(11) DEFAULT NULL,
  `city_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'joker', '刘金龙', null, null, null, null, null, null, '1', null);
INSERT INTO `sys_user` VALUES ('3', 'kinron', '刘金龙', 'e10adc3949ba59abbe56e057f20f883e', '1', '2020-08-20 05:37:27', 'joker', null, '17664521331', null, null);
INSERT INTO `sys_user` VALUES ('4', 'Joker', '刘金龙', '123321', null, null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('5', 'Joker1', '刘金龙', 'e10adc3949ba59abbe56e057f20f883e', '123', '2020-08-20 07:24:04', 'joker', null, '17664521331', null, null);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_User_id` int(11) DEFAULT NULL,
  `sys_role_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1', null, null);
INSERT INTO `sys_user_role` VALUES ('2', '3', '2', '2020-08-20 17:37:28', 'joker');
INSERT INTO `sys_user_role` VALUES ('3', '5', '3', '2020-08-20 19:24:05', 'joker');

-- ----------------------------
-- Table structure for trajectory_2020_08_21
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_08_21`;
CREATE TABLE `trajectory_2020_08_21` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trajectory_gps` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trajectory_2020_08_21
-- ----------------------------
INSERT INTO `trajectory_2020_08_21` VALUES ('2', '1', '2020-08-21 18:19:22', '2020-08-21 18:19:09', '120.48471,36.120222');
INSERT INTO `trajectory_2020_08_21` VALUES ('3', '1', '2020-08-21 19:10:48', '2020-08-21 19:10:48', '116.377823,39.931978');
INSERT INTO `trajectory_2020_08_21` VALUES ('4', '1', '2020-08-21 19:11:18', '2020-08-21 19:11:20', '110.165241,20.032594');
INSERT INTO `trajectory_2020_08_21` VALUES ('5', '1', '2020-08-21 19:11:55', '2020-08-21 19:11:57', '87.604729,43.828587');

-- ----------------------------
-- Table structure for trajectory_date
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_date`;
CREATE TABLE `trajectory_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trajectory_gps` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of trajectory_date
-- ----------------------------
INSERT INTO `trajectory_date` VALUES ('1', '1', '2020-08-21 18:18:51', '2020-08-21 18:18:51', '120.487189,36.125455');
INSERT INTO `trajectory_date` VALUES ('2', '1', '2020-08-21 18:19:22', '2020-08-21 18:19:09', '120.48471,36.120222');

-- ----------------------------
-- Table structure for transportation_time
-- ----------------------------
DROP TABLE IF EXISTS `transportation_time`;
CREATE TABLE `transportation_time` (
  `id` int(11) NOT NULL,
  `time_start` varchar(255) DEFAULT NULL,
  `time_end` varchar(255) DEFAULT NULL,
  `transportation` varchar(255) DEFAULT NULL,
  `kilometre` varchar(255) DEFAULT NULL,
  `extend_one` varchar(255) DEFAULT NULL,
  `extend_two` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of transportation_time
-- ----------------------------

-- ----------------------------
-- Table structure for travel_information
-- ----------------------------
DROP TABLE IF EXISTS `travel_information`;
CREATE TABLE `travel_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `start_address` varchar(255) DEFAULT NULL,
  `end_address` varchar(255) DEFAULT NULL,
  `start_gps` varchar(255) DEFAULT NULL,
  `end_gps` varchar(255) DEFAULT NULL,
  `trip_mode` varchar(255) DEFAULT NULL,
  `trip_objective` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `trip_time` int(11) DEFAULT NULL,
  `is_send_children` int(11) DEFAULT NULL,
  `send_adress` varchar(255) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trip_gps` varchar(255) DEFAULT NULL,
  `start_format_address` varchar(255) DEFAULT NULL,
  `end_format_address` varchar(255) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `city_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of travel_information
-- ----------------------------
INSERT INTO `travel_information` VALUES ('1', '1', '2020-08-20 19:33:41', '2020-08-20 19:33:41', '泰安', '济南', '21.21.21.21', '22.22.22.22', '1', '1', '1', '2020-08-20 00:00:00', null, null, null, '1', '青岛科技大学附属幼儿园', '2020-08-20 19:33:41', '1', null, null, null, null);
INSERT INTO `travel_information` VALUES ('2', '1', '2020-08-20 19:36:59', '2020-08-20 19:36:59', '泰安', '青岛', '22.2.2.2.2', '2.2.2.2.2.2', '2', '2', '1', '2020-08-20 00:00:00', null, null, null, '1', null, '2020-08-20 19:36:59', null, null, null, null, null);

-- ----------------------------
-- Table structure for user_area
-- ----------------------------
DROP TABLE IF EXISTS `user_area`;
CREATE TABLE `user_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user_area
-- ----------------------------
INSERT INTO `user_area` VALUES ('1', '3', '0');

-- ----------------------------
-- Table structure for wechart_info
-- ----------------------------
DROP TABLE IF EXISTS `wechart_info`;
CREATE TABLE `wechart_info` (
  `wechart_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `wechart_number` varchar(255) DEFAULT NULL,
  `wechart_name` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_willing` int(11) DEFAULT NULL,
  `issubmit` int(11) DEFAULT NULL,
  PRIMARY KEY (`wechart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of wechart_info
-- ----------------------------

-- ----------------------------
-- Table structure for wechat_app
-- ----------------------------
DROP TABLE IF EXISTS `wechat_app`;
CREATE TABLE `wechat_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` int(11) DEFAULT NULL,
  `familyid` int(11) DEFAULT NULL,
  `addid` varchar(255) DEFAULT NULL,
  `appwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of wechat_app
-- ----------------------------
