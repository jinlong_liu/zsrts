package com.zs.timedTasks;

import com.zs.traffic.quartz.trajectorydiff.Test;

import java.io.IOException;

public class TimedTask {
    public static int a = 0;
    public void work(){
        try {
            System.out.println("===================================================");
            Test.start();
            System.out.println("===================================================");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
