package com.zs.utils;

import java.sql.Connection;
 import java.sql.DriverManager;
 import java.sql.ResultSet;
 import java.sql.SQLException;
 import java.sql.Statement;
 
 public class JDBCUtils {
     private static String url="jdbc:oracle:thin:@//localhost:1521/myoracle";
     private static String user="scott";
     private static String password="tiger";
     
     private JDBCUtils(){
         
     }
     
     static{
      try {
             Class.forName("oracle.jdbc.driver.OracleDriver");
         } catch (ClassNotFoundException e) {
             throw new ExceptionInInitializerError(e);
         }
     }     
     public static Connection getConnection() throws SQLException{
         return DriverManager.getConnection(url, user, password);
     }
     
     public static void free(ResultSet rs,Statement st,Connection conn){
        try{
            if(rs!=null)
                rs.close();
         }catch(SQLException e){
             e.printStackTrace();
        }finally{
             try{
                 if(st!=null)
                    st.close();
             }catch(SQLException e){
                e.printStackTrace();
            }finally{
                if(conn!=null)
                     try {
                        conn.close();
                    } catch (SQLException e) {
                       e.printStackTrace();
                    }
            }
         }
     }
 }
