package com.zs.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.xml.sax.SAXException;

/**
 * @description
 * 		read and write xml file, use dom4j library
 * 
 * @author LiLiang
 * 
 * @version 1.0
 *
 */
@SuppressWarnings("unchecked")
public class XmlDisposerUtil {
	
	/**
	 * @description get document element from xml file
	 * @param fileName
	 * @return
	 * @throws DocumentException
	 */
	public static Document getDocumentElement(String fileName) throws DocumentException {
		if(null == fileName) {
			return null;
		}
		
		//whether file exist
		File file = new File(fileName);
		if(!file.exists()){
			return null;
		}
		
		Document document = null;
		SAXReader reader = new SAXReader();
        document = reader.read(new File(fileName));
        return document;
	}
	
/*	
    <sld:Rule>
    	<sld:Name>200000</sld:Name>
		<sld:Title>200000</sld:Title>
    	<ogc:Filter>
        	<ogc:PropertyIsEqualTo>
            	<ogc:PropertyName>ID</ogc:PropertyName>
            	<ogc:Literal>200000</ogc:Literal>
        	</ogc:PropertyIsEqualTo>                          
    	</ogc:Filter>
    	<sld:LineSymbolizer>
        	<sld:Stroke>
            	<sld:CssParameter name="stroke">#1B9E77</sld:CssParameter>
            	<sld:CssParameter name="stroke-width">2</sld:CssParameter>
            	<sld:CssParameter name="stroke-opacity">1</sld:CssParameter>
        	</sld:Stroke>
    	</sld:LineSymbolizer>
	</sld:Rule>
*/
	public static LinkedList<Map<String, Object>> readStyleList(String fileName) 
			throws DocumentException{

		if(null == fileName){
			return null;
		}
		
		//whether file exist
		File file = new File(fileName);
		if(!file.exists()){
			return null;
		}
		
		LinkedList<Map<String, Object>> styleList = new LinkedList<Map<String, Object>>();
				
		Document document = null;
		SAXReader reader = new SAXReader();
        document = reader.read(new File(fileName));
        
        //get root element
        Element rootEle = document.getRootElement();
        
        Element userLayer = rootEle.element("UserLayer");
        if(null == userLayer){
        	return null;
        }
        Element userStyle = userLayer.element("UserStyle");
        if(null == userStyle){
        	return null;
        }
        
        Element featureTypeStyle = userStyle.element("FeatureTypeStyle");
        if(null == featureTypeStyle){
        	return null;
        }       
     
        
        @SuppressWarnings("rawtypes")
		List ruleNodeList = featureTypeStyle.elements("Rule");
        
        for(@SuppressWarnings("rawtypes")
		Iterator it= ruleNodeList.iterator(); it.hasNext();){
        	HashMap<String, Object> map = new HashMap<String, Object>();
        	Element ele = (Element) it.next();
        	
        	//get <sld:Name>
        	Element nameNode = ele.element("Name");       	
        	String nameValue= nameNode.getText();

        	//get <sld:Stroke>
        	Element lineSymbolizer = ele.element("LineSymbolizer");
        	Element strokeNode = lineSymbolizer.element("Stroke");
        	@SuppressWarnings("rawtypes")
			List cssNodeList = strokeNode.elements("CssParameter");
        	for(@SuppressWarnings("rawtypes")
			Iterator itCss= cssNodeList.iterator(); itCss.hasNext();){
        		Element eleCss = (Element) itCss.next();
        		Attribute attr = eleCss.attribute("name"); 
            	String tempVal  = attr.getValue();
            	if(tempVal.equals("stroke")){
            		String strokeValue = eleCss.getTextTrim();
            		if( !map.containsKey(nameValue)){
            			map.put(nameValue, strokeValue);
                		styleList.add(map);
            		}
            		
            		break;
            	}
        	}
        }
        		
		return styleList;
	}
	
	/**
	 * 线路sld
	 * @param featureTypeStyle
	 */
	private static void addRuleRoute(Element featureTypeStyle, List<Map<String, Object>> dataList) {
		
		if (dataList == null || dataList.size() < 1) {
			return;
		}
		
		for(int i = 0; i < dataList.size(); i++) {
			
			Element rule = featureTypeStyle.addElement("sld:Rule");
			
			Element sld = rule.addElement("sld:Name");
			//需要修改的内容
			sld.setText(dataList.get(i).get("FakeId").toString());
			Element title = rule.addElement("sld:Title");
			//需要修改的内容
			title.setText(dataList.get(i).get("FakeId").toString());
			Element filter = rule.addElement("ogc:Filter");
			Element property = filter.addElement("ogc:PropertyIsEqualTo");
			Element propertyName = property.addElement("ogc:PropertyName");
			propertyName.setText("FakeId");
			Element literal = property.addElement("ogc:Literal");
			//需要修改的内容
			literal.setText(dataList.get(i).get("FakeId").toString());
			
			Element lineSymbolizer = rule.addElement("sld:LineSymbolizer");
			Element stroke = lineSymbolizer.addElement("sld:Stroke");
			Element stroke_name = stroke.addElement("sld:CssParameter");
			stroke_name.setAttributeValue("name", "stroke");
			//需要修改的内容
			stroke_name.setText(dataList.get(i).get("color").toString());
			Element stroke_width = stroke.addElement("sld:CssParameter");
			stroke_width.setAttributeValue("name", "stroke-width");
			stroke_width.setText("4");
			Element stroke_opacity = stroke.addElement("sld:CssParameter");
			stroke_opacity.setAttributeValue("name", "stroke-opacity");
			stroke_opacity.setText("1");
			
			if (i % 2000 == 0) {
				System.out.println("添加节点数：" + i);
			}
			
		}
	}
	
	/**
	 * 
	 * @param featureTypeStyle
	 * @param dataList
	 */
	private static void modifyRuleRoute(Element featureTypeStyle, List<Map<String, Object>> dataList){
		@SuppressWarnings("rawtypes")
		List ruleNodeList = featureTypeStyle.elements("Rule");
        
        @SuppressWarnings("rawtypes")
		Iterator it= ruleNodeList.iterator();
        @SuppressWarnings("rawtypes")
		Iterator itData= dataList.iterator();
        while(it.hasNext() && itData.hasNext()){
        	HashMap<String, Object> map = new HashMap<String, Object>();
        	map = (HashMap<String, Object>)itData.next();
        	Element ele = (Element) it.next();
        	
        	//get <sld:Stroke>
        	Element lineSymbolizer = ele.element("LineSymbolizer");
        	Element strokeNode = lineSymbolizer.element("Stroke");
        	
        	@SuppressWarnings("rawtypes")
			List cssNodeList = strokeNode.elements("CssParameter");	
        	
        	@SuppressWarnings("rawtypes")
        	Iterator itCss= cssNodeList.iterator();
        	while(itCss.hasNext()) {
        		Element eleCss = (Element) itCss.next();
        		Attribute attr = eleCss.attribute("name"); 
            	String tempVal  = attr.getValue();
            	if(tempVal.equals("stroke")){
            		String fluxValue = map.get("color").toString();
            		eleCss.setText(fluxValue);
            		break;
            	}
        	}
        }
	}
	
	/**
	 * 
	 * @param featureTypeStyle
	 * @param dataList
	 */
	private static void modifyRuleRouteClass(Element featureTypeStyle, List<Map<String, Object>> dataList){
		@SuppressWarnings("rawtypes")
		List ruleNodeList = featureTypeStyle.elements("Rule");
        
        @SuppressWarnings("rawtypes")
		Iterator it= ruleNodeList.iterator();
        @SuppressWarnings("rawtypes")
		Iterator itData= dataList.iterator();
        while(it.hasNext() && itData.hasNext()){
        	HashMap<String, Object> map = new HashMap<String, Object>();
        	map = (HashMap<String, Object>)itData.next();
        	Element ele = (Element) it.next();
        	
        	//get <sld:Stroke>
        	Element lineSymbolizer = ele.element("LineSymbolizer");
        	Element strokeNode = lineSymbolizer.element("Stroke");
        	
        	@SuppressWarnings("rawtypes")
			List cssNodeList = strokeNode.elements("CssParameter");	
        	
        	@SuppressWarnings("rawtypes")
        	Iterator itCss= cssNodeList.iterator();
        	while(itCss.hasNext()) {
        		Element eleCss = (Element) itCss.next();
        		Attribute attr = eleCss.attribute("name"); 
            	String tempVal  = attr.getValue();
            	if(tempVal.equals("stroke")){
            		String fluxValue = map.get("color").toString();
            		eleCss.setText(fluxValue);
            		break;
            	}
        	}
        }
	}
	
	/**
	 * 专用道sld
	 * @param featureTypeStyle
	 */
	private static void addRuleBusWay(Element featureTypeStyle, List<Map<String, Object>> dataList) {
		
		if (dataList == null || dataList.size() < 1) {
			return;
		}
		
		for(int i = 0; i < dataList.size(); i++) {
			
			Element rule = featureTypeStyle.addElement("sld:Rule");
			
			Element sld = rule.addElement("sld:Name");
			sld.setText(dataList.get(i).get("lineId").toString());
			Element title = rule.addElement("sld:Title");
			title.setText(dataList.get(i).get("lineId").toString());
			Element filter = rule.addElement("ogc:Filter");
			Element property = filter.addElement("ogc:PropertyIsEqualTo");
			Element propertyName = property.addElement("ogc:PropertyName");
			propertyName.setText("Id");
			Element literal = property.addElement("ogc:Literal");
			literal.setText(dataList.get(i).get("lineId").toString());
			
			Element lineSymbolizer = rule.addElement("sld:LineSymbolizer");
			Element stroke = lineSymbolizer.addElement("sld:Stroke");
			Element stroke_name = stroke.addElement("sld:CssParameter");
			stroke_name.setAttributeValue("name", "stroke");
			stroke_name.setText(dataList.get(i).get("color").toString());
			Element stroke_width = stroke.addElement("sld:CssParameter");
			stroke_width.setAttributeValue("name", "stroke-width");
			stroke_width.setText("2");
			Element stroke_opacity = stroke.addElement("sld:CssParameter");
			stroke_opacity.setAttributeValue("name", "stroke-opacity");
			stroke_opacity.setText("1");
		}
	}
	
	/**
	 * 线网状态sld
	 * @param featureTypeStyle
	 */
	private static void addRuleRouteClass(Element featureTypeStyle, List<Map<String, Object>> dataList) {
		
		if (dataList == null || dataList.size() < 1) {
			return;
		}
		
		for(int i = 0; i < dataList.size(); i++) {
			
			Element rule = featureTypeStyle.addElement("sld:Rule");
			
			Element sld = rule.addElement("sld:Name");
			sld.setText(dataList.get(i).get("IDCOPY").toString());
			Element title = rule.addElement("sld:Title");
			title.setText(dataList.get(i).get("IDCOPY").toString());
			Element filter = rule.addElement("ogc:Filter");
			Element property = filter.addElement("ogc:PropertyIsEqualTo");
			Element propertyName = property.addElement("ogc:PropertyName");
			propertyName.setText("IDCOPY");
			Element literal = property.addElement("ogc:Literal");
			literal.setText(dataList.get(i).get("IDCOPY").toString());
			
			Element lineSymbolizer = rule.addElement("sld:LineSymbolizer");
			Element stroke = lineSymbolizer.addElement("sld:Stroke");
			Element stroke_name = stroke.addElement("sld:CssParameter");
			stroke_name.setAttributeValue("name", "stroke");
			stroke_name.setText(dataList.get(i).get("color").toString());
			Element stroke_width = stroke.addElement("sld:CssParameter");
			stroke_width.setAttributeValue("name", "stroke-width");
			stroke_width.setText("2");
			Element stroke_opacity = stroke.addElement("sld:CssParameter");
			stroke_opacity.setAttributeValue("name", "stroke-opacity");
			stroke_opacity.setText("1");
		}
	}
	
	/**
	 * @description write sld file 
	 * @param doc
	 * @param filePath
	 * @param encoding
	 */
	private static void writeToFile(Document doc, String filePath,
            String encoding) {
        try {
            OutputFormat fmt = OutputFormat.createPrettyPrint();
            fmt.setEncoding(encoding);

            XMLWriter xmlWriter = new XMLWriter(new OutputStreamWriter(
                    new FileOutputStream(filePath), encoding), fmt);
            xmlWriter.write(doc);
            xmlWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	/**
	 * 
	 * @param fileTemplate
	 * @param dataList
	 * @return
	 * @throws DocumentException
	 */
	public static String getStyleListString(String fileTemplate, List<Map<String, Object>> dataList) throws DocumentException {
		//whether file exist
		File file = new File(fileTemplate);
		if(!file.exists()){
			return "";
		}
				
		Document document = null;
		SAXReader reader = new SAXReader();
        document = reader.read(new File(fileTemplate));
        
        //get root element
        Element rootEle = document.getRootElement();
        
        Element userLayer = rootEle.element("UserLayer");
        if(null == userLayer){
        	return "";
        }
        Element userStyle = userLayer.element("UserStyle");
        if(null == userStyle){
        	return "";
        }
        
        Element featureTypeStyle = userStyle.element("FeatureTypeStyle");
        if(null == featureTypeStyle){
        	return "";
        }
        
        addRuleRoute(featureTypeStyle, dataList);
        return document.asXML();
	}
	
	
	/**
	 * @description
	 * @param list, all elements order by asc
	 * @throws DocumentException 
	 * @throws SAXException 
	 */
	/**
	 * 
	 * @param fileTemplate
	 * @param fileName
	 * @param dataList
	 * @param type 1:专用道，2全网运行状态
	 * @return
	 * @throws DocumentException
	 * @throws SAXException
	 */
	public static boolean writeStyleList(String fileTemplate, String fileName, List<Map<String, Object>> dataList, int type) 
			throws DocumentException, SAXException {
		if(null == dataList){
			return false;
		}
		
		if(null == fileName){
			return false;
		}
		
		//whether file exist
		File file = new File(fileTemplate);
		if(!file.exists()){
			return false;
		}
		
		System.out.println("开始读入sld文件");
		long startTime = System.currentTimeMillis();
		
		boolean ret = true;
		Document document = null;
		SAXReader reader = new SAXReader();
        document = reader.read(new File(fileTemplate));
		
        long endTime = System.currentTimeMillis();
        float seconds = (endTime - startTime) / 1000F;
        System.out.println("读入文件耗时：" + Float.toString(seconds) + " 秒.");
        
        //get root element
        Element rootEle = document.getRootElement();
        
        Element userLayer = rootEle.element("UserLayer");
        
        if(null == userLayer){
        	return false;
        }
        Element userStyle = userLayer.element("UserStyle");
        if(null == userStyle){
        	return false;
        }
        
        Element featureTypeStyle = userStyle.element("FeatureTypeStyle");
        if(null == featureTypeStyle){
        	return false;
        }

        if(1 == type) {//专用道
        	addRuleBusWay(featureTypeStyle, dataList);
        } else if (2 == type) {//网络运行状态  update
    		modifyRuleRouteClass(featureTypeStyle, dataList);
        	//addRouteClass(featureTypeStyle,dataList);//测试
		} else if (3 == type) {//线路运行速度 add
    		addRuleRoute(featureTypeStyle, dataList);
		}
        
        //write sld data
        System.out.println("开始写入sld文件");
		startTime = System.currentTimeMillis();
		writeToFile(document, fileName, "UTF-8");
		endTime = System.currentTimeMillis();
        seconds = (endTime - startTime) / 1000F;
        System.out.println("写入文件耗时：" + Float.toString(seconds) + " 秒.");
		
		return ret;
	}
	
	/**
	 * 修改BusWay模板节点内容
	 * @param rootEle Element
	 * @throws DocumentException 
	 */
	public static void updateBusWayTemplateText(String fileTemplate,String geoServerPath) throws DocumentException {
		
		//read xml file
		Document document = null;
		SAXReader reader = new SAXReader();
        document = reader.read(new File(fileTemplate));
        
        //get root element
        Element rootEle = document.getRootElement();
        
        //update userLayer Name
        Element userLayer = rootEle.element("UserLayer");
        Element userName=userLayer.element("Name");
        userName.setText("dcutp:BusWay_"+geoServerPath);
        
        //update userStyle Name
        Element userStyle = userLayer.element("UserStyle");
        Element userStyleName=userStyle.element("Name");
        userStyleName.setText("BusWay_"+geoServerPath);
        
        //write the file
        writeToFile(document, fileTemplate, "UTF-8");
	}
	
	/**
	 * 修改RouteSegment模板节点内容
	 * @param rootEle Element
	 * @throws DocumentException 
	 */
	public static void updateRouteSegmentTemplateText(String fileTemplate,String geoServerPath) throws DocumentException {
		
		//read xml file
		Document document = null;
		SAXReader reader = new SAXReader();
        document = reader.read(new File(fileTemplate));
        
        //get root element
        Element rootEle = document.getRootElement();
        
        //update userLayer Name
        Element userLayer = rootEle.element("UserLayer");
        Element userName=userLayer.element("Name");
        userName.setText("dcutp:RouteSegment_"+geoServerPath);
        
        //update userStyle Name
        Element userStyle = userLayer.element("UserStyle");
        Element userStyleName=userStyle.element("Name");
        userStyleName.setText("RouteSegment_"+geoServerPath);
        
        //write the file
        writeToFile(document, fileTemplate, "UTF-8");
	}
	
	//测试
	@SuppressWarnings("unused")
	private static void addRouteClass(Element featureTypeStyle, List<Map<String, Object>> dataList) {
		
		if (dataList == null || dataList.size() < 1) {
			return;
		}
		
		for(int i = 0; i < dataList.size(); i++) {
			
				Element rule = featureTypeStyle.addElement("sld:Rule");
				
				Element sld = rule.addElement("sld:Name");
				//需要修改的内容
				sld.setText(dataList.get(i).get("IDCOPY").toString());
				Element title = rule.addElement("sld:Title");
				//需要修改的内容
				title.setText(dataList.get(i).get("IDCOPY").toString());
				Element filter = rule.addElement("ogc:Filter");
				Element property = filter.addElement("ogc:PropertyIsEqualTo");
				Element propertyName = property.addElement("ogc:PropertyName");
				propertyName.setText("IDCOPY");
				Element literal = property.addElement("ogc:Literal");
				//需要修改的内容
				literal.setText(dataList.get(i).get("IDCOPY").toString());
				
				Element lineSymbolizer = rule.addElement("sld:LineSymbolizer");
				Element stroke = lineSymbolizer.addElement("sld:Stroke");
				Element stroke_name = stroke.addElement("sld:CssParameter");
				stroke_name.setAttributeValue("name", "stroke");
				//需要修改的内容
				stroke_name.setText(dataList.get(i).get("color").toString());
				Element stroke_width = stroke.addElement("sld:CssParameter");
				stroke_width.setAttributeValue("name", "stroke-width");
				stroke_width.setText("2");
				Element stroke_opacity = stroke.addElement("sld:CssParameter");
				stroke_opacity.setAttributeValue("name", "stroke-opacity");
				stroke_opacity.setText("1");
				
				if (i % 2000 == 0) {
					System.out.println("添加节点数：" + i);
				}
			
			
			
		}
	}
	
	public static String readFile(String path){
		StringBuffer sb= new StringBuffer("");
		try {
	        FileReader reader = new FileReader(path);
	        BufferedReader br = new BufferedReader(reader);
	       
	        String str = null;
	       
	        while((str = br.readLine()) != null) {
	              sb.append(str+"/n");
	              System.out.println(str);
	        }
	        br.close();
	        reader.close();
		} catch (Exception e) {
			return "";
		}
		return sb.toString();
	}
}
