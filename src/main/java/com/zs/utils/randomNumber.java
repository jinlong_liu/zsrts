package com.zs.utils;

/**
 * @description 生成随机数
 * @author 		antenglei
 * @date 		2017-06-13
 *
 */

public class randomNumber {
	public static int getRandNum(int min, int max) {
	    int randNum = min + (int)(Math.random() * ((max - min) + 1));
	    return randNum;
	}
}
