package com.zs.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {

	/**
	 * 
	 * @param classPath	属性资源文件的类路径,相对于src的路径
	 * @return
	 */
	public static Properties loadJdbcConfig(String classPath) {
		Properties p = new Properties();

		InputStream is = PropertiesLoader.class.getResourceAsStream(classPath);
		try {
			p.load(is);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return p;
	}
}
