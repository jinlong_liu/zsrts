/**
 * 
 */
package com.zs.utils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @project_name: wpk_manage
 * @classname:	  MD5Util.java
 * @createtiem:   2012-9-20
 * @author:		  renxw
 */
public class MD5Util {
	private char md5Chars[] =
		{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
		'e', 'f' };
	private MessageDigest messagedigest;

	/*获取一个文件的md5码 */
	public String getFileMD5String(String inputFile) throws Exception
	{
		// 缓冲区大小（这个可以抽出一个参数）
		  int bufferSize = 256 * 1024;
		  FileInputStream fileInputStream = null;
		  DigestInputStream digestInputStream = null;

		  try {
		     // 拿到一个MD5转换器（同样，这里可以换成SHA1）
		     MessageDigest messageDigest =MessageDigest.getInstance("MD5");
		     // 使用DigestInputStream
		     fileInputStream = new FileInputStream(inputFile);
		     digestInputStream = new DigestInputStream(fileInputStream,messageDigest);
		     // read的过程中进行MD5处理，直到读完文件
		     byte[] buffer =new byte[bufferSize];
		     while (digestInputStream.read(buffer) > 0);
		     // 获取最终的MessageDigest
		     messageDigest= digestInputStream.getMessageDigest();
		     // 拿到结果，也是字节数组，包含16个元素
		     byte[] resultByteArray = messageDigest.digest();
		     // 同样，把字节数组转换成字符串
		     return byteArrayToHex(resultByteArray);

		  } catch (NoSuchAlgorithmException e) {

		     return null;

		  } finally {

		     try {

		        digestInputStream.close();

		     } catch (Exception e) {

		     }

		     try {

		        fileInputStream.close();

		     } catch (Exception e) {

		     }

		  }

		  
		/*String value = "";
		File file = new File(fileName);
		messagedigest = MessageDigest.getInstance("MD5");
		FileInputStream fis = new FileInputStream(file);
		try {
			FileChannel ch = fis.getChannel();
			MappedByteBuffer byteBuffer = ch.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
			messagedigest.update(byteBuffer);
			value = bufferToHex(messagedigest.digest());
			messagedigest.reset();
			messagedigest = null;
		} catch (Exception e) {
			if (null != fis) {
				fis.close();
				fis = null;
			}
		}
		
		return value;*/
	}
	
	/*获取一个文件的md5码 */
	public String getFileMD5String(File file) throws Exception
	{
		messagedigest = MessageDigest.getInstance("MD5");
		FileInputStream fis = new FileInputStream(file);
		FileChannel ch = fis.getChannel();
		MappedByteBuffer byteBuffer = ch.map(FileChannel.MapMode.READ_ONLY, 0,
				file.length());
		messagedigest.update(byteBuffer);
		if (null != fis) {
			fis.close();
			fis = null;
		}

		return bufferToHex(messagedigest.digest());
	}

	/*获取一个字符串的md5码 */
	public String getStringMD5String(String str) throws Exception
	{
		messagedigest = MessageDigest.getInstance("MD5");
		messagedigest.update(str.getBytes()); 
		return bufferToHex(messagedigest.digest());
	}

	private String bufferToHex(byte bytes[])
	{
		return bufferToHex(bytes, 0, bytes.length);
	}

	private String bufferToHex(byte bytes[], int m, int n)
	{
		StringBuffer stringbuffer = new StringBuffer(2 * n);
		int k = m + n;
		for (int l = m; l < k; l++)
		{
			appendHexPair(bytes[l], stringbuffer);
		}
		return stringbuffer.toString();
	}

	private void appendHexPair(byte bt, StringBuffer stringbuffer)
	{
		char c0 = md5Chars[(bt & 0xf0) >> 4];
		char c1 = md5Chars[bt & 0xf];
		stringbuffer.append(c0);
		stringbuffer.append(c1);
	}
	
	public String byteArrayToHex(byte[] byteArray) {
		// 首先初始化一个字符数组，用来存放每个16进制字符
		  char[] hexDigits = {'0','1','2','3','4','5','6','7','8','9', 'A','B','C','D','E','F' };
		  // new一个字符数组，这个就是用来组成结果字符串的（解释一下：一个byte是八位二进制，也就是2位十六进制字符（2的8次方等于16的2次方））
		  char[] resultCharArray =new char[byteArray.length * 2];
		  // 遍历字节数组，通过位运算（位运算效率高），转换成字符放到字符数组中去
		  int index = 0;
		  for (byte b : byteArray) {
		     resultCharArray[index++] = hexDigits[b>>> 4 & 0xf];
		     resultCharArray[index++] = hexDigits[b& 0xf];
		  }

		  // 字符数组组合成字符串返回
		  return new String(resultCharArray);
	}

}
