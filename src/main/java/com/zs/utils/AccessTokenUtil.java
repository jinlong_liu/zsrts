package com.zs.utils;



import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.zs.traffic.controller.BaseController;
import com.zs.traffic.wx.model.event.AccessToken;

import net.sf.json.JSONObject;

public class AccessTokenUtil extends BaseController{
	//public static final String ID = "wx8ebd582644d0bb29";
	//public static final String Scret = "9bf0522749a796aece4e241f79748a5f";
	public static final String ID = getProperties().getProperty("appID");
    public static final String Scret = getProperties().getProperty("appScret");
	//public static final String ID = "wx1c6fb297bf4bf502";
	//public static final String Scret = "f5304b500937d1596f616d9facabe22a";
	public static int num = 0;
	/**
	 * 获取accessToken
	 * @param appID		微信公众号凭证
	 * @param appScret	微信公众号凭证秘钥
	 * @return
	 */
	public static AccessToken getAccessToken(String appID, String appScret) {
		AccessToken token = new AccessToken();
		// 访问微信服务器
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appID + "&secret="+appScret;
		try {
			URL getUrl=new URL(url);
			HttpURLConnection http=(HttpURLConnection)getUrl.openConnection();
			http.setRequestMethod("GET"); 
			http.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);

			http.connect();
			InputStream is = http.getInputStream(); 
			int size = is.available(); 
			byte[] b = new byte[size];
			is.read(b);
			String message = new String(b, "UTF-8");	
			JSONObject json = JSONObject.fromObject(message);
			token.setAccess_token(json.getString("access_token"));
			token.setExpires_in(new Integer(json.getString("expires_in")));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println("出现异常，试图重新换取token！第"+num+1+"次");
			if(num!=3){
				getAccessToken(ID,Scret);
			}
			num++;
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("出现异常，试图重新换取token！第"+num+1+"次");
			if(num!=3){
				getAccessToken(ID,Scret);
			}
			num++;
		}
		return token;
	}
}
