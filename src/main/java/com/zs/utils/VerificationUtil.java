package com.zs.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * 参数验证
 * @author wzf
 * @date 2017-10-17
 */
public class VerificationUtil {
	
	/**
     * 利用数字格式化验证参数是否为数字
     * @param str
     * @return
     */
    public static boolean isNumeric(String str){
    	  try{  
    		  Integer.parseInt(str);//转成Integer类型
         }catch(Exception e){  
             return true;
         } 
    	  return false;  
    }
    
    /**
     * 利用日期格式化來验证时间格式
     * @param str
     * @return
     */
    public static boolean isValidDateDay(String str) {  
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
        try{  
        	 sdf.setLenient(false); //验证参数是否是一个合法日期格式
        	 sdf.parse(str);
        }catch(Exception e){ 
        	return true;//表示异常，不是时间格式字符串
        } 
        return false;//表示是时间类型格式  
    } 
    /**
     * 利用日期格式化來验证时间格式
     * @param str
     * @return
     */
    public static boolean isValidDateMinute(String str) {  
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try{  
        	 sdf.setLenient(false); //验证参数是否是一个合法日期格式
        	 sdf.parse(str);
        }catch(Exception e){ 
        	return true;//表示异常，不是时间格式字符串
        } 
        return false;//表示是时间类型格式  
    } 
    /**
     * 利用日期格式化來验证时间格式
     * @param str
     * @return
     */
    public static boolean isValidDateSecond(String str) {  
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try{  
        	 sdf.setLenient(false); //验证参数是否是一个合法日期格式
        	 sdf.parse(str);
        }catch(Exception e){ 
        	return true;//表示异常，不是时间格式字符串
        } 
        return false;//表示是时间类型格式  
    } 
    /**
	 * 验证时间字符串格式输入是否正确
	 * (无法yy-MM-dd类型，暂时弃用)
	 * @param timeStr
	 * @return
	 */
	public static boolean valiDateTimeWithLongFormat(String timeStr) {
		String format = "((19|20)[0-9]{2})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01]) "
				+ "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";
		Pattern pattern = Pattern.compile(format);
		Matcher matcher = pattern.matcher(timeStr);
		if (matcher.matches()) {
			pattern = Pattern.compile("(\\d{4})-(\\d+)-(\\d+).*");
			matcher = pattern.matcher(timeStr);
			if (matcher.matches()) {
				int y = Integer.valueOf(matcher.group(1));
				int m = Integer.valueOf(matcher.group(2));
				int d = Integer.valueOf(matcher.group(3));
				if (d > 28) {
					Calendar c = Calendar.getInstance();
					c.set(y, m-1, 1);
					int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
					return (lastDay < d);
				}
			}
			return false;//false表示是时间类型格式
		}
		return true;
	}
	
	/**
	 * 获取当天时间下一个日期的1点
	 * @return
	 */
	public static Date Tomorrow(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = sdf.parse(sdf.format(date));
			
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			calendar.add(calendar.DATE, 1);// 把日期往后增加一天.整数往后推,负数往前移动
			calendar.add(calendar.HOUR, 1);
			Date tomorrowDate = calendar.getTime(); // 这个时间就是日期往后推一天的结果
		
			return tomorrowDate;
		} catch (ParseException e) {
			return null;
		}
		
	}
	

}