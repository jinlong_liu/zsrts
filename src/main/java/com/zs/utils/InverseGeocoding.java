package com.zs.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class InverseGeocoding {
	
	
	public static String httpRequest(String requestUrl,String requestMethod,String outputStr){  
		StringBuffer buffer=null;  
		try{  
		URL url=new URL(requestUrl);  
		HttpURLConnection conn=(HttpURLConnection)url.openConnection();  
		conn.setDoOutput(true);  
		conn.setDoInput(true);  
		conn.setRequestMethod(requestMethod);  
		conn.connect();  
		//往服务器端写内容 也就是发起http请求需要带的参数  
		if(null!=outputStr){  
		OutputStream os=conn.getOutputStream();  
		os.write(outputStr.getBytes("utf-8"));  
		os.close();  
		}  
		//读取服务器端返回的内容  
		InputStream is=conn.getInputStream();  
		InputStreamReader isr=new InputStreamReader(is,"utf-8");  
		BufferedReader br=new BufferedReader(isr);  
		buffer=new StringBuffer();  
		String line=null;  
		while((line=br.readLine())!=null){  
		buffer.append(line);  
		}  
		}catch(Exception e){  
		e.printStackTrace();  
		}  
		return buffer.toString();  
		}  
	
	/*public static void main(String[] args){  
		String s=httpRequest("http://restapi.amap.com/v3/geocode/regeo?output=JSON&location=116.396574,39.992706&key=f93f36784283d0d846be899dc38257d9&radius=1000&extensions=base","GET",null);  
		System.out.println(s); 
		s = httpRequest("http://restapi.amap.com/v3/distance?origins=116.481028,39.989643|114.481028,39.989643|115.481028,39.989643&destination=114.465302,40.004717&output=JSON&key=f93f36784283d0d846be899dc38257d9","GET",null);
		System.out.println(s);
		} */ 
	/**
	 * 根据经纬度获取地址
	 * @param lng
	 * @param lat
	 * @param key
	 * @return
	 */
	public static String address(Object lng,Object lat,String key){
		String s=InverseGeocoding.httpRequest("http://restapi.amap.com/v3/geocode/regeo?output=JSON&location="+lng+","+lat+"&key="+key+"&radius=1000&extensions=base","GET",null);  
		System.out.println(s);
		s = "["+s+"]";
		JSONArray jsonArry = new JSONArray(s); 
		
		JSONObject jsonObject = jsonArry.getJSONObject(0);
		Object status = jsonObject.get("status");
		System.out.println(jsonObject.get("status"));
		if("1".equals(status)){
			System.out.println("=========================================");
		}
		String  regeocode = "["+jsonObject.get("regeocode").toString()+"]";
		JSONArray jsonArryRegeocode = new JSONArray(regeocode); 
		JSONObject jsonRegeocodeObject = jsonArryRegeocode.getJSONObject(0);
		System.out.println(jsonRegeocodeObject.get("formatted_address"));//详细地址
		System.out.println(jsonRegeocodeObject.get("addressComponent"));
		
		
		JSONArray jsonArryAddressComponent = new JSONArray("["+jsonRegeocodeObject.get("addressComponent").toString()+"]"); 
		JSONObject jsonAddressComponentObject = jsonArryAddressComponent.getJSONObject(0);
		System.out.println(jsonAddressComponentObject.get("building"));
		//String township = (String) jsonAddressComponentObject.get("township");//乡镇
		String neighborhood = jsonAddressComponentObject.get("neighborhood").toString();//社区信息
		System.out.println(neighborhood);
		
		JSONArray jsonArryNeighborhood = new JSONArray("["+jsonAddressComponentObject.get("neighborhood").toString()+"]"); 
		JSONObject jsonNeighborhoodObject = jsonArryNeighborhood.getJSONObject(0);
		System.out.println(jsonNeighborhoodObject.get("name"));
		
		JSONArray jsonArrybuilding = new JSONArray("["+jsonAddressComponentObject.get("building").toString()+"]"); 
		JSONObject jsonBuildingObject = jsonArrybuilding.getJSONObject(0);
		System.out.println(jsonBuildingObject.get("name"));
		
		JSONArray jsonArryStreetNumber = new JSONArray("["+jsonAddressComponentObject.get("streetNumber").toString()+"]"); 
		JSONObject jsonstreetNumberObject = jsonArryStreetNumber.getJSONObject(0);
		System.out.println(jsonstreetNumberObject.get("street")+""+jsonstreetNumberObject.get("number"));
		
		return status+",,"+jsonRegeocodeObject.get("formatted_address").toString()+
				",,"+jsonBuildingObject.get("name").toString()+
				",,"+jsonstreetNumberObject.get("street").toString()+""+jsonstreetNumberObject.get("number").toString()+
				",,"+jsonAddressComponentObject.get("township").toString()+",,"+jsonNeighborhoodObject.get("name");
	}
	
	/**
	 * 根据经纬度获取地址
	 * @param lng
	 * @param lat
	 * @param key
	 * @return
	 */
	public static List<String> addressPois(Object lng,Object lat,String key){
		List<String> list = new ArrayList<String>();
		String s=InverseGeocoding.httpRequest("http://restapi.amap.com/v3/geocode/regeo?output=JSON&location="+lng+","+lat+"&key="+key+"&radius=1000&extensions=all","GET",null);  
		System.out.println(s);
		s = "["+s+"]";
		JSONArray jsonArry = new JSONArray(s); 
		
		JSONObject jsonObject = jsonArry.getJSONObject(0);
		Object status = jsonObject.get("status");
		System.out.println(jsonObject.get("status"));
		if("1".equals(status)){
			String  regeocode = "["+jsonObject.get("regeocode").toString()+"]";
			JSONArray jsonArryRegeocode = new JSONArray(regeocode); 
			JSONObject jsonRegeocodeObject = jsonArryRegeocode.getJSONObject(0);
			System.out.println(jsonRegeocodeObject.get("formatted_address"));//详细地址
			list.add(jsonRegeocodeObject.get("formatted_address").toString());
			
			//建筑物地址
			/*
		 	JSONArray jsonArryAddressComponent = new JSONArray("["+jsonRegeocodeObject.get("addressComponent").toString()+"]"); 
			JSONObject jsonAddressComponentObject = jsonArryAddressComponent.getJSONObject(0);
			System.out.println(jsonAddressComponentObject.get("building"));
			JSONArray jsonArrybuilding = new JSONArray("["+jsonAddressComponentObject.get("building").toString()+"]"); 
			JSONObject jsonBuildingObject = jsonArrybuilding.getJSONObject(0);
			System.out.println(jsonBuildingObject.get("name"));
			
			if(!"[]".equals(jsonBuildingObject.get("name").toString())){
				list.add(jsonBuildingObject.get("name").toString());
			}
				 */
			//pois地址
			String pois = jsonRegeocodeObject.get("pois").toString();//社区信息
			System.out.println(pois);
			
			JSONArray jsonArryPois = new JSONArray(jsonRegeocodeObject.get("pois").toString());
			
			for(int i=0;i<jsonArryPois.length();i++){
				JSONObject jsonPoisObject = jsonArryPois.getJSONObject(i);
				//System.out.println(jsonPoisObject.get("name"));
				list.add(jsonPoisObject.get("name").toString());
			}
		}
		
		return list;
	}
	
	/**
	 * 根据经纬度获取距离
	 * @param lng
	 * @param lat
	 * @param key
	 * @return
	 */
	public static String distance(Double lng1,Double lat1,Double lng2,Double lat2,String key){
		//String s = httpRequest("http://restapi.amap.com/v3/distance?origins="+lng1+","+lat1+"&destination="+lng2+","+lat2+"&output=JSON&key="+key+'"',"GET",null);
		//String s = httpRequest("http://restapi.amap.com/v3/distance?origins=116.259232,40.159459&destination=116.259366,40.159510&output=JSON&key="+key+'"',"GET",null);
		String s = httpRequest("http://restapi.amap.com/v3/distance?origins="+lng1+","+lat1+"&destination="+lng2+","+lat2+"&output=JSON&key="+key,"GET",null);

		System.out.println(s);

		s = "["+s+"]";
		JSONArray jsonArry = new JSONArray(s); 
		
		JSONObject jsonObject = jsonArry.getJSONObject(0);
		Object status = jsonObject.get("status");
		System.out.println(jsonObject.get("status"));
		if("1".equals(status)){
			System.out.println("=========================================");
		}
		String  results = jsonObject.get("results").toString();
		JSONArray jsonArryResults = new JSONArray(results); 
		JSONObject jsonRegeocodeObject = jsonArryResults.getJSONObject(0);
		System.out.println(jsonRegeocodeObject.get("distance"));//距离
		
		return status+",,"+jsonRegeocodeObject.get("distance").toString();
				
	}

}
