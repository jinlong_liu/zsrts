package com.zs.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 导出Excel公共方法
 * 
 * @version 1.0
 * 
 * @author wangcp
 * 
 */
public class ExportExcel{
	
	/*
	 * 导出数据
	 */
	public void export(String title,String downFileName, int column,List<Map<String, Object>> dataList,HttpServletRequest request,HttpServletResponse response) throws Exception {
		try {
			HSSFWorkbook workbook = new HSSFWorkbook(); // 创建工作簿对象
			String hssfTitle = title.substring(0, title.indexOf('('));
			HSSFSheet sheet = workbook.createSheet(hssfTitle); // 创建工作表
			// 产生表格标题行
			HSSFRow rowm = sheet.createRow(0);
			HSSFCell cellTiltle = rowm.createCell(0);
			
			// sheet样式定义【getColumnTopStyle()/getStyle()均为自定义方法 - 在下面 - 可扩展】
			HSSFCellStyle columnTopStyle = this.getColumnTopStyle(workbook);// 获取列头样式对象
			HSSFCellStyle leftStyle = this.getLeftStyle(workbook);// 获取列头样式对象
			HSSFCellStyle rightStyle = this.getRightStyle(workbook);// 获取列头样式对象
			HSSFCellStyle centerStyle = this.getCenterStyle(workbook);// 获取列头样式对象

			sheet.addMergedRegion(new CellRangeAddress(0, 1, 0,(column - 1)));
			cellTiltle.setCellStyle(columnTopStyle);
			cellTiltle.setCellValue(title);
			
			for (int i = 0; i < dataList.size(); i++) {
				Map<String,Object> map = dataList.get(i);// 遍历每个对象
				HSSFRow row = sheet.createRow(i + 2);// 创建所需的行数
				
				if (i==0) {//表头
					for (int j = 0; j < column; j++) {
						HSSFCell cell = row.createCell(j, HSSFCell.CELL_TYPE_STRING);// 设置单元格的数据类型
						cell.setCellValue(map.get("col_"+j).toString());
						cell.setCellStyle(columnTopStyle);
						sheet.setColumnWidth(j, Integer.parseInt(map.get("style_"+j).toString()));
					}
				}else {//数据
					for (int j = 0; j < column; j++) {
						HSSFCell cell = row.createCell(j, HSSFCell.CELL_TYPE_STRING);// 设置单元格的数据类型
						if ("left".equals(map.get("style_"+j))) {
							cell.setCellValue(map.get("col_"+j).toString());
							cell.setCellStyle(leftStyle);
						}else if("right".equals(map.get("style_"+j))){
							cell.setCellValue(map.get("col_"+j).toString());
							cell.setCellStyle(rightStyle);
						}else {
							cell.setCellValue(map.get("col_"+j).toString());
							cell.setCellStyle(centerStyle);
						}
						
						
					}
				}
				
					
			}
			
			if (workbook != null) {
				try {
					String path=request.getSession().getServletContext().getRealPath("/");
					SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
					Date date = new Date();
					File file = new File(path+"\\excelFile");
					if (!file.isDirectory() && !file.exists() ) {
						file.mkdir();
					}
					String fileName=file.getAbsolutePath()+"\\"+formatter.format(date)+".xls";
					FileOutputStream out=new FileOutputStream(new File(fileName));//生成xls文件
	    	        workbook.write(out); //下载
	    	        fileDown(response,fileName,downFileName);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/*
	 * 列头单元格样式
	 */
	public HSSFCellStyle getColumnTopStyle(HSSFWorkbook workbook) {

		// 设置字体
		HSSFFont font = workbook.createFont();
		// 设置字体大小
		font.setFontHeightInPoints((short) 11);
		// 字体加粗
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// 设置字体名字
		font.setFontName("Courier New");
		// 设置样式;
		HSSFCellStyle style = workbook.createCellStyle();
		// 设置底边框;
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		// 设置底边框颜色;
		style.setBottomBorderColor(HSSFColor.BLACK.index);
		// 设置左边框;
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		// 设置左边框颜色;
		style.setLeftBorderColor(HSSFColor.BLACK.index);
		// 设置右边框;
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		// 设置右边框颜色;
		style.setRightBorderColor(HSSFColor.BLACK.index);
		// 设置顶边框;
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		// 设置顶边框颜色;
		style.setTopBorderColor(HSSFColor.BLACK.index);
		// 在样式用应用设置的字体;
		style.setFont(font);
		// 设置自动换行;
		style.setWrapText(false);
		// 设置水平对齐的样式为居中对齐;
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// 设置垂直对齐的样式为居中对齐;
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		

		return style;

	}

	/*
	 * 列数据信息单元格样式
	 */
	public HSSFCellStyle getLeftStyle(HSSFWorkbook workbook) {
		// 设置字体
		HSSFFont font = workbook.createFont();
		// 设置字体大小
		font.setFontHeightInPoints((short)10);
		// 字体加粗
		//font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// 设置字体名字
		font.setFontName("Courier New");
		// 设置样式;
		HSSFCellStyle style = workbook.createCellStyle();
		// 设置底边框;
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		// 设置底边框颜色;
		style.setBottomBorderColor(HSSFColor.BLACK.index);
		// 设置左边框;
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		// 设置左边框颜色;
		style.setLeftBorderColor(HSSFColor.BLACK.index);
		// 设置右边框;
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		// 设置右边框颜色;
		style.setRightBorderColor(HSSFColor.BLACK.index);
		// 设置顶边框;
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		// 设置顶边框颜色;
		style.setTopBorderColor(HSSFColor.BLACK.index);
		// 在样式用应用设置的字体;
		style.setFont(font);
		// 设置自动换行;
		style.setWrapText(false);
		// 设置水平对齐的样式为左对齐;
		style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		
		// 设置垂直对齐的样式为居中对齐;
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		return style;

	}
	
	public HSSFCellStyle getRightStyle(HSSFWorkbook workbook) {
		// 设置字体
		HSSFFont font = workbook.createFont();
		// 设置字体大小
		font.setFontHeightInPoints((short)10);
		// 字体加粗
		//font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// 设置字体名字
		font.setFontName("Courier New");
		// 设置样式;
		HSSFCellStyle style = workbook.createCellStyle();
		// 设置底边框;
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		// 设置底边框颜色;
		style.setBottomBorderColor(HSSFColor.BLACK.index);
		// 设置左边框;
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		// 设置左边框颜色;
		style.setLeftBorderColor(HSSFColor.BLACK.index);
		// 设置右边框;
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		// 设置右边框颜色;
		style.setRightBorderColor(HSSFColor.BLACK.index);
		// 设置顶边框;
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		// 设置顶边框颜色;
		style.setTopBorderColor(HSSFColor.BLACK.index);
		// 在样式用应用设置的字体;
		style.setFont(font);
		// 设置自动换行;
		style.setWrapText(false);
		// 设置水平对齐的样式为左对齐;
		style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		
		// 设置垂直对齐的样式为居中对齐;
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		return style;

	}
	
	public HSSFCellStyle getCenterStyle(HSSFWorkbook workbook) {
		// 设置字体
		HSSFFont font = workbook.createFont();
		// 设置字体大小
		font.setFontHeightInPoints((short)10);
		// 字体加粗
		//font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// 设置字体名字
		font.setFontName("Courier New");
		// 设置样式;
		HSSFCellStyle style = workbook.createCellStyle();
		// 设置底边框;
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		// 设置底边框颜色;
		style.setBottomBorderColor(HSSFColor.BLACK.index);
		// 设置左边框;
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		// 设置左边框颜色;
		style.setLeftBorderColor(HSSFColor.BLACK.index);
		// 设置右边框;
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		// 设置右边框颜色;
		style.setRightBorderColor(HSSFColor.BLACK.index);
		// 设置顶边框;
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		// 设置顶边框颜色;
		style.setTopBorderColor(HSSFColor.BLACK.index);
		// 在样式用应用设置的字体;
		style.setFont(font);
		// 设置自动换行;
		style.setWrapText(false);
		// 设置水平对齐的样式为左对齐;
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		
		// 设置垂直对齐的样式为居中对齐;
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		return style;

	}
	
	/**
	 * 文件下载
	 * @param response
	 */
	public void fileDown(HttpServletResponse response,String filepath,String downFileName){
		try {
			response.reset();
			response.setContentType("application/vnd.ms-excel");
			downFileName = new String(downFileName.getBytes(), "ISO8859-1");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + downFileName+ ".xls" + "\"");
			File saveDirFile = new File(filepath);
			if(saveDirFile.exists()){
				OutputStream output  =response.getOutputStream();
				InputStream fis = new BufferedInputStream(new FileInputStream(filepath));
				byte[] b = new byte[1024];
				int i = 0;
				while((i = fis.read(b)) != -1){
					output.write(b, 0, i);//写入输出流
				}
				output.flush();//清空流
				
			}
		} catch (Exception e) {
			return ;
		}
		 
    }	
	
}