package com.zs.utils;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginFilter implements Filter{
	Properties pro = null;
	
	public void init(FilterConfig filterConfig) throws ServletException {
		if (null == pro) {
			pro = PropertiesLoader.loadJdbcConfig("/conf/config.properties");
		}
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		
		//add by renxianwei 2017-04-05
		//解决appscan 使用 HTTP 动词篡改的认证旁路漏洞
		String method = servletRequest.getMethod();
		if(!(method.equalsIgnoreCase("post")||method.equalsIgnoreCase("get")||method.equalsIgnoreCase("head")
			    ||method.equalsIgnoreCase("trace")||method.equalsIgnoreCase("connect")||method.equalsIgnoreCase("options"))){
			return;
		}
		
		// 修复跨站点请求伪造的漏洞
		/*Properties pro = PropertiesLoader.loadJdbcConfig("/conf/config.properties");
		String referer = servletRequest.getHeader("Referer");
		if (null != referer) {
			String[] domStrings = pro.getProperty("domainName").split("&");
			int pos = -1;
			for (int i = 0; i < domStrings.length; i++) {
				pos = referer.indexOf(domStrings[i]);
				if (-1 != pos) {
					break;
				}
			}

			if (-1 == pos) {
				return;
			}
		}*/
		
		
		String rootPath=servletRequest.getContextPath();//获取项目名称
		String path = servletRequest.getRequestURI();//获取用户url
		
		//资源文件无需过滤
		if(path.endsWith(".woff")||path.endsWith(".ttf")||path.endsWith(".svg")||path.endsWith(".eot")||path.endsWith(".zip")||path.endsWith(".png") || path.endsWith(".gif") || path.endsWith(".css") 
				|| path.endsWith(".js") || path.endsWith(".json") || path.endsWith(".sld")
				|| path.endsWith("trafficsurveyer-pc.jsp") || path.endsWith("trafficsurveyer.jsp")) {
			chain.doFilter(servletRequest, servletResponse);
			return;
		}
		
		//app接口无需过滤
		if (path.indexOf("/app/") != -1) {
			chain.doFilter(servletRequest, servletResponse);
			return;
		}
		
		//判断是否登录
		String   user=(String) session.getAttribute("userName");
		if (user == null) {
			if ((rootPath+"/login").equals(path) || (rootPath+"/login.jsp").equals(path) || (rootPath+"/services/loginSystem").equals(path)
					||path.indexOf("wx")!=-1||(rootPath+"/index.jsp").equals(path)||path.indexOf("getXml")!=-1||path.indexOf("getOpenId")!=-1) {
				chain.doFilter(servletRequest, servletResponse);
				return;
			}else {//跳转登录页
				//servletResponse.sendRedirect(servletRequest.getContextPath()+"/session.jsp");
				request.getRequestDispatcher("/session/session.jsp").forward(request,response);
			}
		} else {
			if(path.endsWith("home") || path.endsWith("login") || path.endsWith(".jsp") || path.startsWith(rootPath+"/services")) {
				if (path.endsWith(".jsp")) {
					String header=servletRequest.getHeader("referer");
					if (header!=null) {
						chain.doFilter(servletRequest, servletResponse);
						return;
					}else {
						//request.getRequestDispatcher("/index.jsp").forward(request,response);
						request.getRequestDispatcher("/session/session.jsp").forward(request,response);
					}
				}else {
					chain.doFilter(servletRequest, servletResponse);
					return;
				}
				
			}else {//跳转登录页
				servletResponse.sendRedirect(servletRequest.getContextPath()+"/login");
			}	
		}
	}

	public void destroy() {
		
	}

}
