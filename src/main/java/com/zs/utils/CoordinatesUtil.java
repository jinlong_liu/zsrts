package com.zs.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/** 
* 类说明
* 坐标转换工具
* @author 作者：renxw: 
* @version 创建时间：2016年7月24日 上午8:55:42 
*/

public class CoordinatesUtil {
	//gist坐标转换接口地址
	static String gistBaseUrl = "http://gist.sz95000.com/coorconvert/coor_convert?ver=1.0&from=wgs84&to=wgs84&encrypt=1";
	//gist坐标转换,wgs84转wgs84
	/**
	 * 
	 * @param dataList 坐标：lon, lat
	 * @return
	 */
	public static List<Map<String, Object>> gistCoorConvert(List<Map<String, Object>> dataList) {
		String strResult = "";
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	    for (int i = 0; i < dataList.size(); i++) {
	    	Map<String, Object> map = new HashMap<String, Object>();
	    	map.put("type", 1);
		    map.put("id", i);
		    map.put("point", dataList.get(i).get("lon").toString()
		    	+ "," + dataList.get(i).get("lat").toString() + "");
		    list.add(map);
		}
	    
		try {
	    	HttpClient httpClient = new DefaultHttpClient();
		    HttpPost post = new HttpPost(gistBaseUrl);
		    StringEntity entity_ = new StringEntity(ZsJsonUtil.listMap2Json(list), "utf-8");//解决中文乱码问题    
            entity_.setContentEncoding("UTF-8");    
            entity_.setContentType("application/json");    
            post.setEntity(entity_); 
			HttpResponse responseIn = httpClient.execute(post);
			System.out.println("=========" + responseIn.getStatusLine().getStatusCode());
			if(responseIn.getStatusLine().getStatusCode() == 200)
			{
				HttpEntity entity = responseIn.getEntity();    
		        if (entity != null) {    
		            InputStream instreams = entity.getContent();    
		            strResult = convertStreamToString(instreams); 
		            System.out.println(strResult);
		        } 
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (null != strResult && strResult.length() > 1) {
			JSONObject object = JSONObject.fromObject(strResult);
			if (object.getString("status").equals("0")) {
				JSONArray array = JSONArray.fromObject(JSONObject.fromObject(strResult).get("objects"));
				if (null != array && array.size() > 0) {
					for(int j = 0; j < array.size(); j++) {
						object = array.getJSONObject(j);
						dataList.get(object.getInt("id")).put("lon", object.getString("point").split(",")[0]);
						dataList.get(object.getInt("id")).put("lat", object.getString("point").split(",")[1]);
					}
				}
			}
		}
		
		return dataList;
	}
	
	/**
	 * 
	 * @param is
	 * @return
	 */
	public static String convertStreamToString(InputStream is) {      
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));      
        StringBuilder sb = new StringBuilder();      
       
        String line = null;      
        try {      
            while ((line = reader.readLine()) != null) {  
                sb.append(line + "\n");      
            }      
        } catch (IOException e) {      
            e.printStackTrace();      
        } finally {      
            try {      
                is.close();      
            } catch (IOException e) {      
               e.printStackTrace();      
            }      
        }      
        return sb.toString();      
    } 
}
