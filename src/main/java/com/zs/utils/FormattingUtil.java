package com.zs.utils;

/**
 * @description App wx问卷信息格式化方法
 * @author 		antenglei
 * @date 		2017-04-21
 *
 */
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormattingUtil {
	private static final String PATTERN_STANDARD19H = null;

	/**
	 * 格式化工作的方法
	 */
//	public static String FormatWork(String work) {
//		String professional = null;
//		if (work.equals("0")) {
//			professional = "企业公司员工";
//		} else if (work.equals("1")) {
//			professional = "政府及事业单位人";
//		} else if (work.equals("2")) {
//			professional = "学生";
//		} else if (work.equals("3")) {
//			professional = "退休";
//		} else if (work.equals("4")) {
//			professional = "个体经营或自由职业";
//		} else if (work.equals("5")) {
//			professional = "农林牧鱼业人员";
//		} else if (work.equals("6")) {
//			professional = "无业";
//		} else if (work.equals("7")) {
//			professional = "其他";
//		} else {
//			professional = "没有固定职业";
//		}
//		return professional;
//	}

	/**
	 * app格式化工作的方法
	 */
	public static String AppFormatWork(int work) {
		String professional = null;
		if (work == 1) {
			professional = "企业公司员工";
		} else if (work == 2) {
			professional = "政府及事业单位人";
		} else if (work == 3) {
			professional = "学生";
		} else if (work == 4) {
			professional = "退休";
		} else if (work == 5) {
			professional = "个体经营或自由职业";
		} else if (work == 6) {
			professional = "农林牧鱼业人员";
		} else if (work == 7) {
			professional = "无业";
		} else if (work == 8) {
			professional = "其他";
		} else {
			professional = "没有固定职业";
		}
		return professional;
	}

	/**
	 * 格式化月收入的方法
	 */
	public static String FormatMonthlyIncome(String Income) {
		String MonthlyIncome = null;
		if (Income.equals("0")) {
			MonthlyIncome = "2000千元以下";
		} else if (Income.equals("1")) {
			MonthlyIncome = "2000-3000元";
		} else if (Income.equals("2")) {
			MonthlyIncome = "3000-5000元";
		} else if (Income.equals("3")) {
			MonthlyIncome = "5000-8000元";
		} else if (Income.equals("4")) {
			MonthlyIncome = "8000-10000元";
		} else if (Income.equals("5")) {
			MonthlyIncome = "10000元以上";
		} else {
			MonthlyIncome = "";
		}
		return MonthlyIncome;
	}

	/**
	 * 格式化枣庄，临沂月收入的方法 
	 */
	public static String FormatMonthlyIncomeZL(String Income) {
		String MonthlyIncome = null;
		if (Income.equals("0")) {
			MonthlyIncome = "无收入";
		} else if (Income.equals("1")) {
			MonthlyIncome = "3000元以下";
		} else if (Income.equals("2")) {
			MonthlyIncome = "3000-5000元";
		} else if (Income.equals("3")) {
			MonthlyIncome = "5000-8000元";
		} else if (Income.equals("4")) {
			MonthlyIncome = "8000-10000元";
		} else if (Income.equals("5")) {
			MonthlyIncome = "10000元以上";
		} else {
			MonthlyIncome = "";
		}
		return MonthlyIncome;
	}

	/**
	 * 格式化性別的方法
	 *
	 */
	public static String FormatSex(String sex) {
		String sexi = null;
		if (sex.equals("0")) {
			sexi = "男";
		} else if (sex.equals("1")) {
			sexi = "女";

		}
		return sexi;
	}

	/**
	 * 格式化居住情況的方法
	 */
	public static String FormatLocal(String local) {
		String localo = null;
		if (local.equals("0")) {
			localo = "常住";
		} else if (local.equals("1")) {
			localo = "暂住";

		} else {
			localo = "其他";
		}
		return localo;
	}

	/**
	 * 格式化是否有停车位的方法
	 */
	public static String FormatTop(String top) {
		String Topi = null;
		if (top.equals("0")) {
			Topi = "是";
		} else if (top.equals("1")) {
			Topi = "否";

		}
		return Topi;
	}

	/**
	 * 格式化车辆类型的方法
	 */
	public static String FormatType(String type) {
		String typeo = null;
		if (type.equals("0")) {
			typeo = "私家车";
		} else if (type.equals("1")) {
			typeo = "单位车";

		}
		return typeo;
	}

	/**
	 * WX格式化是否接送小孩的方法
	 */
	public static String child(String type) {
		String typeo = null;
		if (type.equals("0")) {
			typeo = "接小孩";
		} else if (type.equals("null")) {
			typeo = "";
		} else if (type.equals("2")) {
			typeo = "否";
		}else{
			typeo = "送小孩";
		}

		return typeo;
	}

	/**
	 * 格式化出行方式的方法
	 */
//	public static String FormatWork(String Mode) {
//		String tripMode = null;
//		if (Mode.equals("1")) {
//			tripMode = "步行";
//		} else if (Mode.equals("2")) {
//			tripMode = "电动车";
//		} else if (Mode.equals("3")) {
//			tripMode = "共享单车(如:小黄车)";
//		} else if (Mode.equals("4")) {
//			tripMode = "私人自行车";
//		} else if (Mode.equals("5")) {
//			tripMode = "地铁";
//		} else if (Mode.equals("6")) {
//			tripMode = "公交车";
//		} else if (Mode.equals("7")) {
//			tripMode = "公交+地铁";
//		} else if (Mode.equals("8")) {
//			tripMode = "自行车+地铁";
//		} else if (Mode.equals("9")) {
//			tripMode = "驾驶私人小汽车";
//		} else if (Mode.equals("10")) {
//			tripMode = "乘坐私人小汽车";
//		} else if (Mode.equals("11")) {
//			tripMode = "班车";
//		} else if (Mode.equals("12")) {
//			tripMode = "校车";
//		} else if (Mode.equals("13")) {
//			tripMode = "出租车";
//		} else if (Mode.equals("14")) {
//			tripMode = "网约车";
//		} else if (Mode.equals("15")) {
//			tripMode = "驾驶单位小汽车";
//		} else if (Mode.equals("16")) {
//			tripMode = "乘坐单位小汽车";
//		} else if (Mode.equals("17")) {
//			tripMode = "摩托车";
//		}
//		return tripMode;
//	}

	/**
	 * 出行
	 */
	public static String tripObjective(String Objective) {
		String tripObjective = null;
		if (Objective.equals("0")) {
			tripObjective = "上班";
		} else if (Objective.equals("1")) {
			tripObjective = "上学";
		} else if (Objective.equals("2")) {
			tripObjective = "工作外出";
		} else if (Objective.equals("3")) {
			tripObjective = "生活类出行";
		} else if (Objective.equals("4")) {
			tripObjective = "回家";
		} else if (Objective.equals("5")) {
			tripObjective = "回程";
		} else if (Objective.equals("6")) {
			tripObjective = "接送小孩";
		} else {
			tripObjective = "其他";
		}
		return tripObjective;
	}

	/**
	 * wx出行目的
	 */
	public static String purpose(String Objective) {
		String tripObjective = null;
		if (Objective.equals("0")) {
			tripObjective = "上班";
		} else if (Objective.equals("1")) {
			tripObjective = "上学";
		} else if (Objective.equals("2")) {
			tripObjective = "工作外出";
		} else if (Objective.equals("3")) {
			tripObjective = "生活类出行";
		} else if (Objective.equals("4")) {
			tripObjective = "回家";
		} else if (Objective.equals("5")) {
			tripObjective = "回程";
		} else if (Objective.equals("6")) {
			tripObjective = "接送小孩";
		} else {
			tripObjective = "其他";
		}
		return tripObjective;
	}
	public static String FormatWork(String work) {
		String professional = null;
		switch (work) {
			case "1":
				professional = "企业公司员工";
				break;
			case "2":
				professional = "政府及事业单位人";
				break;
			case "3":
				professional = "学生";
				break;
			case "4":
				professional = "退休";
				break;
			case "5":
				professional = "个体经营或自由职业";
				break;
			case "6":
				professional = "农林牧鱼业人员";
				break;
			case "7":
				professional = "无业";
				break;
			case "9":
				professional = "其他";
				break;
			default:
				professional = "没有固定职业";
				break;
		}
		return professional;
	}
	/**
	 * 格式化出行方式的方法
	 */
	public static String WxtripMode(String Mode) {
		String tripMode = null;
		if (Mode.equals("0")) {
			tripMode = "步行";
		} else if (Mode.equals("1")) {
			tripMode = "电动车";
		} else if (Mode.equals("2")) {
			tripMode = "共享单车(如:小黄车)";
		} else if (Mode.equals("3")) {
			tripMode = "私人自行车";
		} else if (Mode.equals("4")) {
			tripMode = "地铁";
		} else if (Mode.equals("5")) {
			tripMode = "公交车";
		} else if (Mode.equals("6")) {
			tripMode = "公交+地铁";
		} else if (Mode.equals("7")) {
			tripMode = "自行车+地铁";
		} else if (Mode.equals("8")) {
			tripMode = "驾驶私人小汽车";
		} else if (Mode.equals("9")) {
			tripMode = "乘坐私人小汽车";
		} else if (Mode.equals("10")) {
			tripMode = "班车";
		} else if (Mode.equals("11")) {
			tripMode = "校车";
		} else if (Mode.equals("12")) {
			tripMode = "出租车";
		} else if (Mode.equals("13")) {
			tripMode = "网约车";
		} else if (Mode.equals("14")) {
			tripMode = "驾驶单位小汽车";
		} else if (Mode.equals("15")) {
			tripMode = "乘坐单位小汽车";
		} else if (Mode.equals("16")) {
			tripMode = "摩托车";
		} else {
			tripMode = "";
		}
		return tripMode;
	}
	public static String tripMode(String Mode) {
		String tripMode = null;
		if (Mode.equals("0")) {
			tripMode = "步行";
		} else if (Mode.equals("1")) {
			tripMode = "电动车";
		} else if (Mode.equals("2")) {
			tripMode = "共享单车(如:小黄车)";
		} else if (Mode.equals("3")) {
			tripMode = "私人自行车";
		} else if (Mode.equals("4")) {
			tripMode = "地铁";
		} else if (Mode.equals("5")) {
			tripMode = "公交车";
		} else if (Mode.equals("6")) {
			tripMode = "公交+地铁";
		} else if (Mode.equals("7")) {
			tripMode = "自行车+地铁";
		} else if (Mode.equals("8")) {
			tripMode = "驾驶私人小汽车";
		} else if (Mode.equals("9")) {
			tripMode = "乘坐私人小汽车";
		} else if (Mode.equals("10")) {
			tripMode = "班车";
		} else if (Mode.equals("11")) {
			tripMode = "校车";
		} else if (Mode.equals("12")) {
			tripMode = "出租车";
		} else if (Mode.equals("13")) {
			tripMode = "网约车";
		} else if (Mode.equals("14")) {
			tripMode = "驾驶单位小汽车";
		} else if (Mode.equals("15")) {
			tripMode = "乘坐单位小汽车";
		} else if (Mode.equals("16")) {
			tripMode = "摩托车";
		} else {
			tripMode = "";
		}
		return tripMode;
	}
	/**
	 * 化出行方式的方法l
	 */
	public static String WxtripModeOne(String Mode) {
		String tripMode = null;
		if (Mode.equals("0")) {
			tripMode = "步行";
		} else if (Mode.equals("1")) {
			tripMode = "电动车";
		} else if (Mode.equals("2")) {
			tripMode = "公租自行车";
		} else if (Mode.equals("3")) {
			tripMode = "私人自行车";
		} else if (Mode.equals("4")) {
			tripMode = "地铁";
		} else if (Mode.equals("5")) {
			tripMode = "公交车";
		} else if (Mode.equals("6")) {
			tripMode = "公交+地铁";
		} else if (Mode.equals("7")) {
			tripMode = "自行车+地铁";
		} else if (Mode.equals("8")) {
			tripMode = "驾驶私人小汽车";
		} else if (Mode.equals("9")) {
			tripMode = "乘坐私人小汽车";
		} else if (Mode.equals("10")) {
			tripMode = "班车";
		} else if (Mode.equals("11")) {
			tripMode = "校车";
		} else if (Mode.equals("12")) {
			tripMode = "出租车";
		} else if (Mode.equals("13")) {
			tripMode = "网约车";
		} else if (Mode.equals("14")) {
			tripMode = "驾驶单位小汽车";
		} else if (Mode.equals("15")) {
			tripMode = "乘坐单位小汽车";
		} else if (Mode.equals("16")) {
			tripMode = "摩托车";
		} else {
			tripMode = "";
		}
/*		if (Mode.equals("1")) {
			tripMode = "公交";
		} else if (Mode.equals("2")) {
			tripMode = "地铁";
		} else if (Mode.equals("3")) {
			tripMode = "小汽车";
		} else if (Mode.equals("4")) {
			tripMode = "出租车";
		} else if (Mode.equals("5")) {
			tripMode = "自行车(含共享单车)";
		} else if (Mode.equals("6")) {
			tripMode = "电动车";
		} else{
			tripMode = "";
		}*/
		return tripMode;
	}

	// 将字符串格式的数据转化为时间格式
	public static Date string2Date(String strDate, String pattern) {
		if (strDate == null || strDate.equals("")) {
			throw new RuntimeException("strDate is null");
		}
		pattern = (pattern == null || pattern.equals("")) ? PATTERN_STANDARD19H : pattern;
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = sdf.parse(strDate);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return date;
	}
	//date--》string
	public static String dateToString(Date date) {
		SimpleDateFormat sformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//日期格式
		String tiem = sformat.format(date);

		return tiem;
	}
	//string ---> date
	public static Date stringToDate(String time) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//日期格式
		Date date = null;
		try {
			date = format.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
}