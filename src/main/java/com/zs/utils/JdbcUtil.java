package com.zs.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Statement;

/**
 * 
 * @author Administrator
 *
 */

public  class  JdbcUtil{
	private static String url = null;
	private static String user = null;
	private static String password = null;
	private static String driverClass = null;

	/**
	 * 静态代码块中（只加载一次）
	 */
	static {
		try {
			// 读取db.properties文件
			Properties props = new Properties();
			/**
			 * . 代表java命令运行的目录 在java项目下，. java命令的运行目录从项目的根目录开始 在web项目下， .
			 * java命令的而运行目录从tomcat/bin目录开始 所以不能使用点.
			 */
			// FileInputStream in = new FileInputStream("./src/db.properties");
			//FileInputStream fis = new FileInputStream("/usr/local/tomcat/jdbc.properties");
			/**
			 * 使用类路径的读取方式 / : 斜杠表示classpath的根目录 在java项目下，classpath的根目录从bin目录开始
			 * 在web项目下，classpath的根目录从WEB-INF/classes目录开始
			 */
			InputStream in = JdbcUtil.class
					.getResourceAsStream("/conf/jdbc.properties");

			// 加载文件
			props.load(in);
			// 读取信息
			url = props.getProperty("jdbc_url");
			user = props.getProperty("jdbc_username");
			password = props.getProperty("jdbc_password");
			driverClass = "com.mysql.jdbc.Driver";

			// 注册驱动程序
			Class.forName(driverClass);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("驱程程序注册出错");
		}
	}

	/**
	 * 抽取获取连接对象的方法
	 */
	public static Connection getConnection() {
		try {
			Connection conn = DriverManager.getConnection(url, user, password);
			return conn;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * 释放资源的方法
	 */
	public static void close(Connection conn, Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}
	
	public static  void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}

	public static void close(Connection conn, Statement stmt, ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
				throw new RuntimeException(e1);
			}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}


	
	//动态创建志愿者实时轨迹记录表
	public  static synchronized  void creatTrajectoryDate(){//定时器，每2小时自动调用一次此方法
		Connection conn = JdbcUtil.getConnection();
		try {
			for(int i=0;i<3;i++){//连续创建往后三天的表
				Date date= new Date();//获取当天时间
				Calendar calendar = new GregorianCalendar(); 
			    calendar.setTime(date); 
			    calendar.add(calendar.DATE,i);//把日期往后增加i天.整数往后推,负数往前移动 
			    date=calendar.getTime();   //这个时间就是日期往后推一天的结果 
			 	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			    String time = sdf.format(date);
			    String[] str = time.split("-");
				String tableName = "trajectory_"+str[0]+"_"+str[1]+"_"+str[2];
				
				ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);//判断表是否存在
				if (rs.next()) {
					System.out.println(tableName+"表已存在！");
					}else {
						String sql = "CREATE TABLE " +tableName+ " LIKE trajectory_date";
						System.out.println(sql);
						PreparedStatement pstmt = conn.prepareStatement(sql);					
						pstmt.addBatch();
						pstmt.executeBatch();
						pstmt.clearBatch();
					}
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			JdbcUtil.close(conn);
		}
	}
	
}
