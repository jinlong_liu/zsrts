package com.zs.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.io.BufferedReader;
import java.io.FileReader;

public class TxtUtil {
	
	//保存TXT文件
	public static void newFile(String filePathAndName, String fileContent) {
        try {
            File myFilePath = new File(filePathAndName.toString());
            //String strPath = "E:\\a\\aa\\aaa.txt";  
    		//File file = new File(strPath);  
    		File fileParent = myFilePath.getParentFile();  
    		if(!fileParent.exists()){ 
    			System.out.println("文件夹不存在，开始创建！");
    		    fileParent.mkdirs();  
    		    System.out.println("创建完成！");
    		}  
    		//file.createNewFile();  
    		//System.out.println("创建文件完成！");
            
            
            if (!myFilePath.exists()) { // 如果该文件不存在,则创建
                myFilePath.createNewFile();
            }
            // FileWriter(myFilePath, true); 实现不覆盖追加到文件里
             //FileWriter(myFilePath); 覆盖掉原来的内容
            //FileWriter resultFile = new FileWriter(myFilePath, true);//给文件里面写内容,原来内容不会覆盖掉
            FileWriter resultFile = new FileWriter(myFilePath);// 给文件里面写内容,原来的会覆盖掉
            PrintWriter myFile = new PrintWriter(resultFile);
            
            myFile.println(fileContent);
            resultFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	
	/**
	 * main方法测试读取TXT文件内容
	 * @throws IOException 
	 */
	/*public static void main(String[] args) throws Exception{

	File file = new File("E:\\15135161937base.txt");

	StringBuilder sb = new StringBuilder();
	String s ="";
	BufferedReader br = new BufferedReader(new FileReader(file));

	while( (s = br.readLine()) != null) {
	sb.append(s + "\n");
	}

	br.close();
	String str = sb.toString();
	System.out.println(str);
	} */

	
	public static void main(String[] args) throws IOException {
		String strPath = "E:\\a\\aa\\aaa.txt";  
		File file = new File(strPath);  
		File fileParent = file.getParentFile();  
		if(!fileParent.exists()){ 
			System.out.println("文件夹不存在，开始创建！");
		    fileParent.mkdirs();  
		    System.out.println("创建完成！");
		}  
		file.createNewFile();  
		System.out.println("创建文件完成！");
	}

}
