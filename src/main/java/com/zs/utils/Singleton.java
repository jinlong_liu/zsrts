package com.zs.utils;

import java.util.HashMap;
import java.util.Map;


/**
 * @description 家庭编号生成
 * 
 * @author rxw
 * 
 * @date 2017-06-03
 * 
 */

public class Singleton {
	private static Singleton instance = null; 
	private final static Object syncLock = new Object();  
	public Map<String, Object> submitCount =  new HashMap<String, Object>();
	
	private Singleton() {  
		;
	}
	
	/**
	 * 
	 * @return
	 */
	public static Singleton getInstance(){  
		synchronized (syncLock) {  
            if (instance == null) {  
                instance = new Singleton();  
            }  
        }
  
		return instance; 
	}
	
	/**
	 * 初始化社区问卷提交数
	 * @param map
	 */
	public synchronized void setSubmitCount(Map<String, Object> map) {
		for (Map.Entry<String, Object> m :map.entrySet())  {  
            submitCount.put(m.getKey(), m.getValue()); 
        }  
	}
	
	/**
	 * 获取家庭编号
	 * @param communitCode
	 * @return
	 */
	public synchronized int getCount(String communitCode) {
		int count = 0;
		boolean flag=submitCount.containsKey(communitCode);
		if(flag==true){
			count = (Integer)submitCount.get(communitCode);
		}	
		count++;
		submitCount.put(communitCode, count);
		
		return count;
	}
}
