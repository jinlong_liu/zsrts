package com.zs.traffic.dao;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.ResidentsTravelInformation;

@Repository
public interface ResidentsTravelInformationMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(ResidentsTravelInformation record);

    int insertSelective(ResidentsTravelInformation record);

    ResidentsTravelInformation selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ResidentsTravelInformation record);

    int updateByPrimaryKey(ResidentsTravelInformation record);
}