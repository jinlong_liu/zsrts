package com.zs.traffic.dao;


import org.apache.ibatis.annotations.Param;


import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.zs.traffic.model.Charge;

public interface ChargeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Charge record);

    int insertSelective(Charge record);

    Charge selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Charge record);

    int updateByPrimaryKey(Charge record);


    int Appaudit(@Param(value="userId")Integer userId,@Param(value="status")Integer status, 
    		@Param(value="opportunity")String opportunity);
    
    Charge selectAppaudit(@Param(value="userId")Integer userId);


    
    int selectTotal(@Param(value="rechargeType")Integer rechargeType); 
    
    int selectTodayNumber(@Param(value="rechargeType")Integer rechargeType,
    		@Param(value="today")String today,@Param(value="tomorrowDay") String tomorrowDay);

    
    List<Charge> selectByStateForBill(Integer state);
    


}