package com.zs.traffic.dao;



import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zs.traffic.model.TransportationTime;

public interface TransportationTimeMapper {
	
	List<TransportationTime> selectOne(String kilometre);
	
	List<TransportationTime> selectAll();
	
    int deleteByPrimaryKey(Integer id);

    int insert(TransportationTime record);

    int insertSelective(TransportationTime record);

    TransportationTime selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TransportationTime record);

    int updateByPrimaryKey(TransportationTime record);
}