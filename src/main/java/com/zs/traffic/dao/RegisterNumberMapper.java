package com.zs.traffic.dao;

import org.apache.ibatis.annotations.Param;

import com.zs.traffic.model.RegisterNumber;

public interface RegisterNumberMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RegisterNumber record);

    int insertSelective(RegisterNumber record);

    RegisterNumber selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RegisterNumber record);

    int updateByPrimaryKey(RegisterNumber record);
    
    RegisterNumber selectByRechargeType(@Param(value="rechargeType")Integer rechargeType);
}