package com.zs.traffic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.UserArea;

@Repository
public interface UserAreaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserArea record);

    int insertSelective(UserArea record);

    UserArea selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserArea record);

    int updateByPrimaryKey(UserArea record);
    
    int deleteBySysUserId(@Param(value="userId")Integer userId);
    
    List<UserArea> getUser(@Param(value="areaId")Integer areaId);
}