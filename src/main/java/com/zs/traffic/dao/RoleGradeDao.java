package com.zs.traffic.dao;

import java.util.List;

import com.zs.traffic.model.SysRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.SysAuthorization;
import com.zs.traffic.model.RoleGrade;
import com.zs.traffic.model.SysRoleAuth;

@Repository
public interface RoleGradeDao {

    public List<RoleGrade> getGradeRoleList();
    public int deleteGradeRole(int id);
}
