package com.zs.traffic.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.AppBusInfo;

@Repository
public interface AppBusInfoMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(AppBusInfo record);

    int insertSelective(AppBusInfo record);

    AppBusInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AppBusInfo record);

    int updateByPrimaryKey(AppBusInfo record);
    
    List<AppBusInfo> selectByAppuserId(Integer appuserId);
    
    int deleteByAppUserId(Integer appuserId);
}