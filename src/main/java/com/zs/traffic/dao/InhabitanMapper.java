package com.zs.traffic.dao;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.Inhabitan;

@Repository
public interface InhabitanMapper {
	
    int deleteByPrimaryKey(Integer inhabitantId);

    int insert(Inhabitan record);

    int insertSelective(Inhabitan record);

    Inhabitan selectByPrimaryKey(Integer inhabitantId);

    int updateByPrimaryKeySelective(Inhabitan record);

    int updateByPrimaryKey(Inhabitan record);
}