package com.zs.traffic.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.TrajectoryDate;

@Repository
public interface TrajectoryDateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TrajectoryDate record);

    int insertSelective(TrajectoryDate record);

    TrajectoryDate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TrajectoryDate record);

    int updateByPrimaryKey(TrajectoryDate record);
    
    int insertSelectives(@Param(value="record")TrajectoryDate record,@Param(value="tableName")String tableName);
    
    List<TrajectoryDate> selectByAppuserAndStartDate(@Param(value="record")TrajectoryDate record,@Param(value="tableName")String tableName);
    
    List<TrajectoryDate> gettrajectory(@Param(value="appuserId")Integer appuserId,@Param(value="startDate")Date startDate, @Param(value="endDate")Date endDate, @Param(value="tableName")String tableName);

    
    List<TrajectoryDate> selectByTableName(@Param(value="tableName")String tableName
    		,@Param(value="appuserId")Integer appuserId,@Param(value="startDate")Date startDate);
    
    List selectAppuserIdByTableName(@Param(value="tableName")String tableName);

    
    List<TrajectoryDate> getTrajectoryMorning(@Param(value="appuserId")Integer appuserId,@Param(value="startDate")Date startDate, @Param(value="endDate")Date endDate, @Param(value="tableName")String tableName);
    
    List<TrajectoryDate> getTrajectoryAfternoon(@Param(value="appuserId")Integer appuserId,@Param(value="startDate")Date startDate, @Param(value="endDate")Date endDate, @Param(value="tableName")String tableName);

}