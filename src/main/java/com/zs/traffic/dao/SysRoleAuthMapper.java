package com.zs.traffic.dao;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.SysRoleAuth;

@Repository
public interface SysRoleAuthMapper {
    int insert(SysRoleAuth record);

    int insertSelective(SysRoleAuth record);
    
    SysRoleAuth getSysRoleAuthByRoleId(Integer roleId);
}