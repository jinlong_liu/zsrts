package com.zs.traffic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zs.traffic.model.RechargeableCard;

public interface RechargeableCardMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RechargeableCard record);

    int insertSelective(RechargeableCard record);

    RechargeableCard selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RechargeableCard record);

    int updateByPrimaryKey(RechargeableCard record);
    
    List<RechargeableCard> selectListByTypeSign(@Param(value="state")Integer state,@Param(value="type")Integer type);
}