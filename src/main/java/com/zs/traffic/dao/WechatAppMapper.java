package com.zs.traffic.dao;

import com.zs.traffic.model.WechatApp;

public interface WechatAppMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(WechatApp record);

    int insertSelective(WechatApp record);

    WechatApp selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(WechatApp record);

    int updateByPrimaryKey(WechatApp record);
}