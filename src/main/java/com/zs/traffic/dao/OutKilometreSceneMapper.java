package com.zs.traffic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zs.traffic.model.OutKilometreScene;

public interface OutKilometreSceneMapper {
	
	List<OutKilometreScene> selectByKmSceneTransportation(@Param(value="kilometre")String kilometre,@Param(value="scene")String scene);
	
    int deleteByPrimaryKey(Integer id);

    int insert(OutKilometreScene record);

    int insertSelective(OutKilometreScene record);

    OutKilometreScene selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OutKilometreScene record);

    int updateByPrimaryKey(OutKilometreScene record);
    
    
    List<OutKilometreScene> selectByTimeWord(@Param(value="timeTotal")String timeTotal,@Param(value="timeGo")String timeGo,@Param(value="timeWait")String timeWait,@Param(value="timeRiding")String timeRiding,@Param(value="priceTicket")String priceTicket,@Param(value="costTotal")String costTotal,@Param(value="costWorking")String costWorking,@Param(value="costPark")String costPark,@Param(value="transportation")String transportation);
    
    List<OutKilometreScene> selectByTimeWord1(@Param(value="timeTotal")String timeTotal,@Param(value="timeGo")String timeGo,@Param(value="timeWait")String timeWait,@Param(value="timeRiding")String timeRiding,@Param(value="priceTicket")String priceTicket,@Param(value="transportation")String transportation);
    
    List<OutKilometreScene> selectByTimeWord2(@Param(value="timeTotal")String timeTotal,@Param(value="costTotal")String costTotal,@Param(value="costWorking")String costWorking,@Param(value="costPark")String costPark,@Param(value="transportation")String transportation);

    List<OutKilometreScene> selectByTimeWord3(@Param(value="timeTotal")String timeTotal,@Param(value="priceTicket")String priceTicket,@Param(value="transportation")String transportation);

    List<OutKilometreScene> selectByTimeWord31(@Param(value="timeTotal")String timeTotal,@Param(value="costTotal")String costTotal,@Param(value="transportation")String transportation);

    
    List<OutKilometreScene> selectByTimeWord4(@Param(value="timeTotal")String timeTotal,@Param(value="transportation")String transportation);

    List<OutKilometreScene> selectByTimeWord5(@Param(value="timeTotal")String timeTotal,@Param(value="transportation")String transportation);

}