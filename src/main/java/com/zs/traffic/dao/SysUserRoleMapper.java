package com.zs.traffic.dao;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.SysUserRole;

@Repository
public interface SysUserRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);
    
    int updateBySysUserId(SysUserRole sysUserRole);
    
    int deleteByUserId(Integer userId);
}