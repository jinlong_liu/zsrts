package com.zs.traffic.dao;


import org.apache.ibatis.annotations.Param;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.StatisticsParticular;

@Repository
public interface StatisticsParticularMapper {
	 
	
	List<StatisticsParticular> selectAllNumForTitle();
	
	List<StatisticsParticular> selectByFamilyList(String familyId);

	StatisticsParticular selectByFamily(String familyId);
	
	List<StatisticsParticular>  selectBySubmitterForList(String openId);
	
	List<StatisticsParticular> selectAll();
	
	List<StatisticsParticular> selectforyestoday();
	
	/*查询社区总条数*/
	int selectTotalCountByComm(String titleName);
	
	/*查询社区当天的条数*/
	int selectCountByComm(@Param(value="titleName")String titleName,@Param(value="submitDate")String submitDate);
    
	int deleteByPrimaryKey(Integer id);

    int insert(StatisticsParticular record);

    int insertSelective(StatisticsParticular record);

    StatisticsParticular selectByPrimaryKey(Integer id);
    
    StatisticsParticular selectBySubmitter(String Submitter);
    
    StatisticsParticular selectBySubmitterIs(String Submitter);
    
    StatisticsParticular selectBySubmitterriginal(String Submitter);

    int updateByPrimaryKeySelective(StatisticsParticular record);
    
    int updateByPrimaryKeySelectiveForPublic(StatisticsParticular record,String openId,String textAddress,String communityNumber);
    
    int updateByPrimaryKey(StatisticsParticular record);

    
    int selectFollowCount(@Param(value="areaId")Integer areaId);

	int selectubmitCount(@Param(value="areaId")Integer areaId);
		
	int selectNotCompleteCount(@Param(value="areaId")Integer areaId);

    
    List<StatisticsParticular> getStatisticsParticularList(@Param(value="cityCode")String cityCode,@Param(value="limit")Integer limit,@Param(value="offset")Integer offset);

    List<StatisticsParticular> getTitle();
    
    int intoAreaId(@Param(value="areaId")Integer areaId,@Param(value="titleName")String titleName);
    
    List<StatisticsParticular> getQuestionnaire(@Param(value="areaNumber")List<String> areaNumber,@Param(value="type")Integer type,@Param(value="cityCode")String cityCode ,@Param(value="limit")Integer limit,@Param(value="offset")Integer offset);
    
    int getQuestionnaireTotal(@Param(value="areaNumber")List<String> areaNumber,@Param(value="cityCode")String cityCode);
    
    int getTwoQuestionnaireTotal(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int getSanQuestionnaireTotal(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    List<StatisticsParticular> getTwoQuestionnaire(@Param(value="areaNumber")List<String> areaNumber,@Param(value="type")Integer type,@Param(value="cityCode")String cityCode,@Param(value="limit")Integer limit,@Param(value="offset")Integer offset);
    
    List<StatisticsParticular> getSanQuestionnaire(@Param(value="areaNumber")List<String> areaNumber,@Param(value="type")Integer type,@Param(value="cityCode")String cityCode,@Param(value="limit")Integer limit,@Param(value="offset")Integer offset);
    
    List<StatisticsParticular> getConditionsDistrictName(@Param(value="districtName")String districtName);
    
    List<StatisticsParticular> getConditions(@Param(value="districtName")String districtName,@Param(value="subofficeName")String subofficeName);
    
    List<StatisticsParticular> getConditionsFamilyId(@Param(value="familyId")String familyId);
    
    List<StatisticsParticular> getPublicQuestionnaire(@Param(value="cityCode")String cityCodeInteger,@Param(value="limit")Integer limit,@Param(value="offset")Integer offset);
    
    List<StatisticsParticular> getaudit(@Param(value="familyId")String familyId);
    
    StatisticsParticular getProgress(@Param(value="familyId")String familyId);
    
    int auditQuestionnaire(@Param(value="familyId")String familyId, @Param(value="audit")Integer audit);
    
    int selecttotalAll(@Param(value="cityCode")String cityCode);
    
    int selectResidentsQuestionnaire(@Param(value="cityCode")String cityCode);
    
    int selectReviewNot(@Param(value="cityCode")String cityCode);
    
    int selectReviewYes(@Param(value="cityCode")String cityCode);
    
    List<StatisticsParticular> selectTravelTotal(@Param(value="cityCode")String cityCode);
    
    int selectStageTotal(@Param(value="cityCode")String cityCode);
    
    int selectConditions(@Param(value="districtNumber")String districtNumber, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="familyId")String familyId,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    int selectConditionsResidents(@Param(value="districtNumber")String districtNumber, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="familyId")String familyId,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    int selectCondditionNotaudit(@Param(value="districtNumber")String districtNumber, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="familyId")String familyId,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    int selectCondditionYesaudit(@Param(value="districtNumber")String districtNumber, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="familyId")String familyId,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    int selectCondditionStageTotal(@Param(value="districtNumber")String districtNumber, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="familyId")String familyId,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    List<StatisticsParticular>selectTotal(@Param(value="districtNumber")String districtNumber, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="familyId")String familyId,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    List<StatisticsParticular> selectContent(@Param(value="districtNumber")String districtNumber, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="familyId")String familyId,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode,@Param(value="limit")Integer limit,@Param(value="offset")Integer offset);
    
    List<StatisticsParticular> selectDeviation(@Param(value="districtNumber")String districtNumber, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="familyId")String familyId,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    List<StatisticsParticular> selectDis(@Param(value="cityCode")String cityCode);
    
    List<StatisticsParticular> selectPublic(@Param(value="cityCode")String cityCode,@Param(value="district") String district,@Param(value="limit")Integer limit,@Param(value="offset")Integer offset);
    
    int selectPublicTotal(@Param(value="cityCode")String cityCode,@Param(value="district") String district);
    
    List<StatisticsParticular> getStatistics(@Param(value="cityCode")String cityCode);
    
    int selectContentTotal(@Param(value="districtNumber")String districtNumber, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="familyId")String familyId,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    int getPublicQuestionnaireTotal(@Param(value="cityCode")String cityCode);
    
    int selectThreeTotal(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectOneTotal(@Param(value="areaNumber")List<String> areaNumber,@Param(value="cityCode")String cityCode);

    int selectTwoTotal(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectResidentsOne(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectResidentsTwo(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectResidentsThree(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectReviewNotOne(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectReviewNotTwo(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectReviewNotThree(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectReviewYesOne(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectReviewYesTwo(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectReviewYesThree(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectStageTotalOne(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectStageTotalTwo(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    int selectStageTotalThree(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    List<StatisticsParticular> getStatisticsOne(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    List<StatisticsParticular> getStatisticsTwo(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    List<StatisticsParticular> getStatisticsThree(@Param(value="areaNumber")List<String> areaNumber, @Param(value="cityCode")String cityCode);
    
    StatisticsParticular getFamilyId(@Param(value="id")int id);
/*    int selectPublicTotal();*/
    
}