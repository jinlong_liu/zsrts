package com.zs.traffic.dao;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.WechartInfo;

@Repository
public interface WechartInfoMapper {
    int deleteByPrimaryKey(Integer wechartId);

    int insert(WechartInfo record);

    int insertSelective(WechartInfo record);

    WechartInfo selectByPrimaryKey(Integer wechartId);

    int updateByPrimaryKeySelective(WechartInfo record);

    int updateByPrimaryKey(WechartInfo record);
}