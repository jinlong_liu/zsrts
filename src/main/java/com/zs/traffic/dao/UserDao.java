package com.zs.traffic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.User;

@Repository
public interface UserDao {
	public List<User> getUserList(@Param(value="cityCode")String cityCode);
	
	public List<User> getUserListByUserId(@Param(value="areaNumber")List<String> areaNumber,
			@Param(value="type")Integer type,@Param(value="authDegree")Integer authDegree,@Param(value="cityCode")String cityCode);//
	
	    int deleteByPrimaryKey(Integer userId);

	    int insert(User record);

	    int insertSelective(User record);

	    User selectByPrimaryKey(Integer userId);

	    int updateByPrimaryKeySelective(User record);

	    int updateByPrimaryKey(User record);
	    
	    User selectByUserName(@Param(value="userName")String UserName);
	    
	    List<User> getUserInfoByuserId(@Param(value="userId")Integer userId);
	    
	    List<User> getUserInfosByuserId(@Param(value="userId")Integer userId);
}
