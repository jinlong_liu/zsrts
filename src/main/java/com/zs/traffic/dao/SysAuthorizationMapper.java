package com.zs.traffic.dao;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.SysAuthorization;

@Repository
public interface SysAuthorizationMapper {
    int deleteByPrimaryKey(Integer authId);

    int insert(SysAuthorization record);

    int insertSelective(SysAuthorization record);

    SysAuthorization selectByPrimaryKey(Integer authId);

    int updateByPrimaryKeySelective(SysAuthorization record);

    int updateByPrimaryKey(SysAuthorization record);
}