package com.zs.traffic.dao;

import java.util.List;

import com.zs.traffic.model.TravelInformation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.AppFamilyInformation;

@Repository
public interface AppFamilyInformationMapper {
	
    int deleteByPrimaryKey(Integer familyId);

    int insert(AppFamilyInformation record);

    int insertSelective(AppFamilyInformation record);

    AppFamilyInformation selectByPrimaryKey(Integer familyId);

    int updateByPrimaryKeySelective(AppFamilyInformation record);

    int updateByPrimaryKey(AppFamilyInformation record);
    
    List<AppFamilyInformation> selectByAppuserId(Integer appuserId);
    
    int deleteByAppUserId(Integer appuserId);
    
    AppFamilyInformation getListPersonal(@Param(value="appuserId")Integer appuserId);
    
    List<AppFamilyInformation> getListAppFamilyInformation(@Param(value="cityCode")String cityCode);
    
    int selectAppUserTwentyNoaudit();
    
    int selectAppUserTwentyYesaudit();
    
    List<AppFamilyInformation> selectAppFamilyInformationForWork(@Param(value="work")Integer work);
    
    List<AppFamilyInformation> selectAppFamilyInformationForAge(@Param(value="starAge")int starAge,@Param(value="endAge") int endAge);
    
    int selectAppUserThirtyNoaudit();
    
    int selectAppUserThirtyYesaudit();
    
    List<AppFamilyInformation> selectAppFamilyInformationForSex(@Param(value="sex")int sex);
    
    List<AppFamilyInformation> selectAppFamilyInformationForIsLocal(@Param(value="local")int local);
    
    List<AppFamilyInformation> selectAppFamilyInformationForUserName(@Param(value="userName")String userName);
    
    List<AppFamilyInformation> selectAppuser(@Param(value="starAge")int starAge, @Param(value="endAge")int endAge,
    		@Param(value="work")int work, @Param(value="sex")int sex,@Param(value="local") int local, @Param(value="userName")String userName,@Param(value="cityCode")String cityCode);
    
    int selectAppUserTotal();
    
    int selectAppUserNoaudit();
    
    int selectAppUserYesaudit();
    
    int selectAppUserFiftyNoaudit();
    
    int selectAppUserFiftyYesaudit();

    List<TravelInformation> getTravelTime(@Param(value = "userId")int userId);
}