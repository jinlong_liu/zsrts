package com.zs.traffic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.Area;

/**
 * 
 * @author xxl 区域管理
 *	@date 2017.3.24
 */
@Repository
public interface AreaMapper {
	
	
	/*
	 * 根据城市编号获取社区
	 */
	public List<Area> selectAllForCity(String city);
	
	/*
	 * 根据社区标号查询一条实体返回类型是Area
	 */
	public Area getAreaIdByTitltName(String titltName);
	
	/*
	 * 根据社区标号查询一条实体
	 */
	public Area getSelectByConumber(String communityNumber);
	public List<Area> getListForQrAll(@Param(value="districtSelectId")String districtSelectId,@Param(value="subOfficeSelectId")String subOfficeSelectId,@Param(value="communitySelectName")String communitySelectName,@Param(value="cityCode")String cityCode);
	/*
	 * 查询表中所有行政区
	 */
	public List<Area> getSelectTypeOne(String cityCode);
	/*
	 * 查询表中社区
	 */
	public List<Area> selectAll();
	
	/*
	 * 1.根据名称获取list
	 * type:1行政区，2街道 3.小区  供树形菜单使用
	 */
	public List<Area> getListByName(@Param(value="name")String Name,@Param(value="type")Integer type);
	
	public List<Area> getListSubByNameWlj(@Param(value="type")Integer type,@Param(value="cityCode")String cityCode,@Param(value="areaId")String areaId);
	
	/*
	 * 2.根据type获取相应集合
	 */
	public List<Area> getListByType(@Param(value="type")Integer type);
	
	/*
	 * 3.根据修改所选标题修改下属的所有区域
	 */
	public int updateName( @Param("area")Area record,@Param(value="name")String name);
	
	/**
	 * 获取所有区域信息
	 * @param record
	 * @return
	 */
	public List<Area> getAllList();
	
	/**
	 * 
	 * @param 根据名称获取list name 为多参数逗号隔开
	 * @param name
	 * @return
	 */
	public List<Area> getListByNames(@Param(value="name")String name,@Param(value="type")Integer type);
	/*
	 * 4.删除行政区或者街道
	 */
	
	/*
	 * 4.根据选择名称获取list
	 * type:1行政区，2街道 3.小区  供树形菜单使用
	 */
	public List<Area> getSelectListByName(@Param(value="name")String Name,@Param(value="type")Integer type);
	
	/*
	 * 5.根据选择type获取相应集合
	 */
	public List<Area> getSelectListByNames(@Param(value="name")String Name,@Param(value="type")Integer type,@Param(value="cityCode")String cityCode);
	
	int insertSelective(Area record);
	
	//6.查询判断是否重名
	public List<Area> getListByNameOrNum(@Param(value="name")String Name,@Param(value="number")String number,@Param(value="type")Integer type,@Param(value="cityCode")String cityCode,@Param(value="cityCode")String districtNumber);
		
	//7.修改行政区实判断是否重名
	public List<Area> getListByNameOrNumById(@Param(value="name")String Name,@Param(value="number")String number,@Param(value="id")Integer id,@Param(value="type")Integer type);
	
	//7.上传文件判断是否数据库中包含
	public List<Area> getListByNameOrNumInclude
	(@Param(value="names")String names,
	@Param(value="numbers")String numbers,
	@Param(value="type")Integer type,
	@Param(value="cityCode")String cityCode
	);
	/*communityNumbers
	 * 废弃
	 */
    int deleteByPrimaryKey(@Param(value="areaId")Integer areaId);

    int insert(Area record);

    Area selectByPrimaryKey(@Param(value="areaId")Integer areaId);

    int updateByPrimaryKeySelective(Area record);

    int updateByPrimaryKey(Area record);
    
    List<Area> getAreaList(@Param(value="cityCode")String cityCode);
    
    int updateParameter(@Param(value="record")Area record,@Param(value="areaId")Integer areaId);
    
    int updateTotal(@Param(value="totalCount")Integer totalCount,@Param(value="areaId")Integer areaId);
    
    int updateEveryday(@Param(value="everydayCount")Integer everydayCount,@Param(value="areaId")Integer areaId);
    
    int updateEverydayCount(@Param(value="everydayCount")Integer everydayCount,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    int updateTotalCount(@Param(value="totalCount")Integer totalCount,@Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);
    
    List<Area>  getAreaByuserId(@Param(value="userId")Integer userId,@Param(value="type")Integer type);//根据用户id获取用户所管辖的区域
    
    List<Area> getAllListArea(@Param(value="type")Integer type,@Param(value="cityCode")String cityCode);
    
    List<Area> getAreaListByAuthDegree(@Param(value="areaNumber")List<String> areaNumber, @Param(value="authDegree")Integer authDegree
    		,@Param(value="cityCOde")String cityCOde);
    
    List<Area> queryByareaNumber(@Param(value="areaNumber")List<String> areaNumber, @Param(value="authDegree")Integer authDegree
    		,@Param(value="cityCOde")String cityCOde);

    List<Area> queryByRegion();

    List<Area> queryListArea(@Param(value="districtCode")String districtName);
	 
    List<Area> queryAreaList(@Param(value="subofficeCode")String subofficeName);
    
    List<Area> queryAreaIst(@Param(value="districtCode")String districtName, @Param(value="subofficeCode")String subofficeName);
    
    List<Area> queryListParameter(@Param(value="districtNumber")String districtNumber,@Param(value="subofficeNumber")String subofficeNumber);
    
    List<Area> queryParameterList(@Param(value="districtName")String districtName, @Param(value="subofficeName")String subofficeName);
    
    List<Area> getAreaId(@Param(value="communityNumber")String communityNumber);
    
    List<Area> queryAreaByRole(@Param(value="userId")int userId,@Param(value="authDegree") int authDegree, 
    		@Param(value="type")int type,@Param(value="cityCode")String cityCode);
    
    List<Area> selectCommunityName(@Param(value="subofficeNumber")String subofficeNumber);
    
    public Area addEcho(@Param(value="areaId")Integer areaId);
    
    List<Area> selectDistrictNumber(@Param(value="districtNumberUpdate")String districtNumberUpdate,@Param(value="cityCode")String cityCode);
    
    List<Area> selectDistrictName(@Param(value="districtNameUpdate")String districtNameUpdate,@Param(value="cityCode")String cityCode);
    
    List<Area> selectSubofficeNumber(@Param(value="districtNumber")String districtNumber);
    
    int updateDistrict(@Param(value="districtNumberUpdate")String districtNumberUpdate,@Param(value="districtNameUpdate")String districtNameUpdate, @Param(value="districtNumber")String districtNumber,@Param(value="cityCode")String cityCode);
    
    List<Area> selectSubofficeNumbers(@Param(value="subofficeNumberUpdate")String subofficeNumberUpdate,@Param(value="cityCode")String cityCode);
    
    List<Area> selectSubofficeNames(@Param(value="subofficeNameUpdate")String subofficeNameUpdate,@Param(value="cityCode")String cityCode);
    
    List<Area> selectCommunityNumber(@Param(value="subofficeNumber")String subofficeNumber,@Param(value="cityCode")String cityCode);
    
    public int updateSuboffice(@Param(value="subofficeNumberUpdate")String subofficeNumberUpdate,@Param(value="subofficeNameUpdate")String subofficeNameUpdate, @Param(value="subofficeNumber")String subofficeNumber,@Param(value="cityCode")String cityCode);
   
    List<Area> selectCommunityNumberUpdates(@Param(value="communityNumberUpdate")String communityNumberUpdate,@Param(value="cityCode")String cityCode);

    List<Area> selectCommunityNameUpdates(@Param(value="communityNameUpdate")String communityNameUpdate,@Param(value="cityCode")String cityCode);
    
    List<Area> selectSub(@Param(value="communityNumber")String communityNumber);
    
    int updateCommunity(@Param(value="communityNumberUpdate")String communityNumberUpdate,@Param(value="communityNameUpdate")String communityNameUpdate, @Param(value="communityNumber")String communityNumber,@Param(value="cityCode")String cityCode);

}