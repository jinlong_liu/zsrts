package com.zs.traffic.dao;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.BusInfo;

@Repository
public interface BusInfoMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(BusInfo record);

    int insertSelective(int iid);

    BusInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BusInfo record);

    int updateByPrimaryKey(BusInfo record);
}