package com.zs.traffic.dao;


import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;


import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.zs.traffic.model.AppSms;

public interface AppSmsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AppSms record);

    int insertSelective(AppSms record);

    AppSms selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AppSms record);

    int updateByPrimaryKey(AppSms record);

    //查询出验证码的集合
    List<AppSms> selectByUserNameTime(@Param(value="userName")String userName,@Param(value="cityCode")String cityCode,@Param(value="sendTime")Date sendTime);

    
    List<AppSms> queryRequestTime(@Param(value="userName")String userName);

}