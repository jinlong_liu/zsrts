package com.zs.traffic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.Auth;

/**
 * @description 获取可查询的数据的日期列表 
 * 
 * @author renxw
 * 
 * @date 2016.6.18
 *
 */
@Repository
public interface AuthDao {
	
	/**
	 * 获取权限
	 * @return
	 */
	public List<Auth> getAuthList();

	
}
