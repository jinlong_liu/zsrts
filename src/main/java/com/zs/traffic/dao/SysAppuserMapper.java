package com.zs.traffic.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.SysAppuser;

@Repository
public interface SysAppuserMapper {
	
    int deleteByPrimaryKey(Integer appuserId);

    int insert(SysAppuser record);

    int insertSelective(SysAppuser record);

    SysAppuser selectByPrimaryKey(Integer appuserId);

    int updateByPrimaryKeySelective(SysAppuser record);

    int updateByPrimaryKey(SysAppuser record);
    
    SysAppuser selectByUserName(String userName);
    
    List<SysAppuser> getListVolunteers();
    
    List<SysAppuser> getListHomeAddress(@Param(value="appuserId")Integer appuserId);
    
    int selectQuantity(@Param(value="todayDate")String todayDate, @Param(value="tomorrowDate")String tomorrowDate);
    
    List<SysAppuser> selectByOccupation(@Param(value="ip")String ip);

    int changeAppUser(@Param(value="userID")String userId,@Param(value="username")String username,@Param(value="bike")String bike,@Param(value="car")String car,@Param(value="number")String number,@Param(value="address")String address);

    int changeAppUserInformation(@Param(value="userID")String userId,@Param(value="sex")String sex,@Param(value="age")String age,@Param(value="islocal")String islocal,@Param(value="income")String income,@Param(value="profession")String profession);

    int changeMacAddress(@Param(value="userID")String userId,@Param(value="macAddress")String macAddress);

    String getMacAddress(@Param(value="userID")String userId);

    Integer macAddressIsExist(@Param(value="userID")String userId,@Param(value="macAddress")String macAddress);

    int deleteUserAllInformation(@Param(value="start_date")String start_date, @Param(value="end_date")String end_date);

    List<Integer> findDeleteUserAllInformation(@Param(value="start_date")String start_date, @Param(value="end_date")String end_date);

    int deleteUserAllCharge(@Param(value="appuser_id")Integer appuser_id);

    /**
     * 获取该时间段内注册的所有用户的appuser_id
     * @param startDate
     * @param endDate
     * @return  该时间段内注册的所有用户的appuser_id
     */
    List<Integer> getAllAppUserByDate(@Param(value="startDate")String startDate, @Param(value="endDate")String endDate);

    /**
     * 根据appuserId解绑手环
     * @param userId
     * @return
     */
    int unbindByAppUserId(@Param(value="userID")Integer userId);

    /**
     * 根据appuserId删除用户
     * @param appuserId
     * @return
     */
    int deleteUserThirdParty(Integer appuserId);
}