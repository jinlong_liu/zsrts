package com.zs.traffic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.SysAuthorization;
import com.zs.traffic.model.SysRole;
import com.zs.traffic.model.SysRoleAuth;

@Repository
public interface RoleDao {
	public List<SysRole> getRoleList();
	
	public int deleteRole(int id);
	
	public int deleteRoleAuto(int id);
	
	public int selectRoleUser(int id);
	
	public List<SysRole> getRoleListByid(@Param(value="id")int  id);
	
	public List<SysAuthorization> getAuthListByid(@Param(value="id")int  id);
	
	public List<SysRoleAuth> getSysRoleAuthByid(@Param(value="sysRoleId")int  sysRoleId);
	
	public int insert(SysRole sysRole);
	
	public int updateRole(SysRole sysRole);
	
	public int getIsRoleName( @Param(value="roleName")String roleName);
	
	public List<SysRole> getRoleListByName(@Param(value="roleName")String  roleName);
	
	public int insertAuthRole(SysRoleAuth sysRoleAuth );
	
	public int updateAuthRole(SysRoleAuth sysRoleAuth );
	
	public List<SysRole> getSysRoleByAuthDegree(@Param(value="authDegree")Integer authDegree);
	
	public SysRole getSysRoleById(@Param(value="roleId")Integer roleId);
	
	public SysAuthorization getAuthByDegree(@Param(value="authDergee")Integer authDergee);
}
