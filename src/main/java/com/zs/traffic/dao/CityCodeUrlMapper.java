package com.zs.traffic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zs.traffic.model.CityCodeUrl;

public interface CityCodeUrlMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CityCodeUrl record);

    int insertSelective(CityCodeUrl record);

    CityCodeUrl selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CityCodeUrl record);

    int updateByPrimaryKey(CityCodeUrl record);
    
    CityCodeUrl selectByCityCode(@Param(value="cityCode")String cityCode,@Param(value="extendedFieldOne")String extendedFieldOne);
    
    List<CityCodeUrl> getSignature(@Param(value="cityCode")String cityCode);
}