package com.zs.traffic.dao;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.FamilyInformation;

@Repository
public interface FamilyInformationDao {
	
    int deleteByPrimaryKey(Integer familyId);

    int insert(FamilyInformation record);

    int insertSelective(FamilyInformation record);

    FamilyInformation selectByPrimaryKey(Integer familyId);

    int updateByPrimaryKeySelective(FamilyInformation record);

    int updateByPrimaryKey(FamilyInformation record);
}