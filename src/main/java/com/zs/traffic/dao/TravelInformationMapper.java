package com.zs.traffic.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zs.traffic.model.TravelInformation;

@Repository
public interface TravelInformationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TravelInformation record);

    int insertSelective(TravelInformation record);

    TravelInformation selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TravelInformation record);

    int updateByPrimaryKey(TravelInformation record);
    
    List<TravelInformation> selectByAppuserId(@Param(value="appuserId")Integer appuserId,
    		@Param(value="todayDate")Date todayDate);
    
    List<TravelInformation> selectByAppuserIdNear(@Param(value="appuserId")Integer appuserId,
    		@Param(value="todayDate")Date todayDate);
    
    List<TravelInformation> getTravelInformation(@Param(value="appuserId")Integer appuserId);
    
    public TravelInformation selectTotalNumber(@Param(value="todayDate")Date todayDate,@Param(value="cityCode")String cityCode);
    
    public List<TravelInformation> selectByDate(@Param(value="todayDate")Date todayDate,@Param(value="cityCode")String cityCode);
    
    public List<TravelInformation> selectTripByAppuserId(@Param(value="appuserId")Integer appuserId,
	          @Param(value="todayDate")Date todayDate);
    
    public List<TravelInformation> selectTripObjectiveByAppuserId(@Param(value="appuserId")Integer appuserId,
	          @Param(value="todayDate")Date todayDate);
    
    //List<TravelInformation> selectSeMan(@Param(value="time")Date time);
    

    List<TravelInformation> getTravelConditions(@Param(value="appuserId")Integer appuserId);
    

    List<TravelInformation> selectSeMan(TravelInformation travelInformation);
    
    List<TravelInformation> getAnalysis(@Param(value="todayDate")Date todayDate);
    
    int deleteByAppuserId(@Param(value="appuserId")Integer appuserId,@Param(value="date")Date date);
    
    int deleteByAppuserIdStatus(@Param(value="appuserId")Integer appuserId,@Param(value="date")Date date);
    
    List<TravelInformation> getIsTravel(@Param(value="appuserId")Integer appuserId);
    
    List<TravelInformation> selectTraGps(@Param(value="appuserId")Integer appuserId,@Param(value="date") Date dayDate);
    
    List<TravelInformation> selectByRanking(@Param(value="appuserId")int appuserId,@Param(value="startDate")Date startDate
    		,@Param(value="ranking")int ranking);//查询出当天第n条出行信息，status=1
    
    int deleteByRank(@Param(value="appuserId")int appuserId,@Param(value="startDate")Date startDate
    		,@Param(value="ranking")int ranking);//删除当天第n条出行信息，status=1
    
    List<TravelInformation> selectPush(@Param(value="appuserId")Integer appuserId,@Param(value="date") Date dayDate);
    //    获取轨迹的日期
    List<String> getTravelTIme(int userId);

    List<TravelInformation> getTravelTime(@Param(value = "userId")int userId);
}