package com.zs.traffic.dao;

import org.springframework.stereotype.Repository;

import com.zs.traffic.model.AppLogInfo;

@Repository
public interface AppLogInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AppLogInfo record);

    int insertSelective(AppLogInfo record);

    AppLogInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AppLogInfo record);

    int updateByPrimaryKey(AppLogInfo record);
}