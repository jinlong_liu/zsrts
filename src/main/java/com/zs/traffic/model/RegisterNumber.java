package com.zs.traffic.model;

public class RegisterNumber {
    private Integer id;

    private Integer registerType;

    private Integer registerNumDay;

    private Integer registerNumTotal;

    private Integer registerNumActual;

    private String extend1;

    private String extend2;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegisterType() {
        return registerType;
    }

    public void setRegisterType(Integer registerType) {
        this.registerType = registerType;
    }

    public Integer getRegisterNumDay() {
        return registerNumDay;
    }

    public void setRegisterNumDay(Integer registerNumDay) {
        this.registerNumDay = registerNumDay;
    }

    public Integer getRegisterNumTotal() {
        return registerNumTotal;
    }

    public void setRegisterNumTotal(Integer registerNumTotal) {
        this.registerNumTotal = registerNumTotal;
    }

    public Integer getRegisterNumActual() {
        return registerNumActual;
    }

    public void setRegisterNumActual(Integer registerNumActual) {
        this.registerNumActual = registerNumActual;
    }

    public String getExtend1() {
        return extend1;
    }

    public void setExtend1(String extend1) {
        this.extend1 = extend1 == null ? null : extend1.trim();
    }

    public String getExtend2() {
        return extend2;
    }

    public void setExtend2(String extend2) {
        this.extend2 = extend2 == null ? null : extend2.trim();
    }
}