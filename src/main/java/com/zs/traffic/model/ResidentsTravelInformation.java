package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class ResidentsTravelInformation implements Serializable{
    private Integer id;

    private Integer inhabitantId;

    private Integer familyId;

    private Integer starTripTime;

    private Date endTripTime;

    private String tripMode;

    private String tripAddress;

    private String busLine;

    private String tripPurposes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInhabitantId() {
        return inhabitantId;
    }

    public void setInhabitantId(Integer inhabitantId) {
        this.inhabitantId = inhabitantId;
    }

    public Integer getFamilyId() {
        return familyId;
    }

    public void setFamilyId(Integer familyId) {
        this.familyId = familyId;
    }

    public Integer getStarTripTime() {
        return starTripTime;
    }

    public void setStarTripTime(Integer starTripTime) {
        this.starTripTime = starTripTime;
    }

    public Date getEndTripTime() {
        return endTripTime;
    }

    public void setEndTripTime(Date endTripTime) {
        this.endTripTime = endTripTime;
    }

    public String getTripMode() {
        return tripMode;
    }

    public void setTripMode(String tripMode) {
        this.tripMode = tripMode == null ? null : tripMode.trim();
    }

    public String getTripAddress() {
        return tripAddress;
    }

    public void setTripAddress(String tripAddress) {
        this.tripAddress = tripAddress == null ? null : tripAddress.trim();
    }

    public String getBusLine() {
        return busLine;
    }

    public void setBusLine(String busLine) {
        this.busLine = busLine == null ? null : busLine.trim();
    }

    public String getTripPurposes() {
        return tripPurposes;
    }

    public void setTripPurposes(String tripPurposes) {
        this.tripPurposes = tripPurposes == null ? null : tripPurposes.trim();
    }
}