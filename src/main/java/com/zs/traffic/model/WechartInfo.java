package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class WechartInfo implements Serializable{
    private Integer wechartId;

    private Integer userId;

    private String wechartNumber;

    private String wechartName;

    private Date createDate;

    private Integer isWilling;

    private Integer issubmit;

    public Integer getWechartId() {
        return wechartId;
    }

    public void setWechartId(Integer wechartId) {
        this.wechartId = wechartId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getWechartNumber() {
        return wechartNumber;
    }

    public void setWechartNumber(String wechartNumber) {
        this.wechartNumber = wechartNumber == null ? null : wechartNumber.trim();
    }

    public String getWechartName() {
        return wechartName;
    }

    public void setWechartName(String wechartName) {
        this.wechartName = wechartName == null ? null : wechartName.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getIsWilling() {
        return isWilling;
    }

    public void setIsWilling(Integer isWilling) {
        this.isWilling = isWilling;
    }

    public Integer getIssubmit() {
        return issubmit;
    }

    public void setIssubmit(Integer issubmit) {
        this.issubmit = issubmit;
    }
}