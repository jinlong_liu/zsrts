package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class TrajectoryDate implements Serializable{
    private Integer id;

    private Integer appuserId;

    private Date startDate;

    private Date endDate;

    private String trajectoryGps;
    
    private String cityCode;//城市编码

  	public String getCityCode() {
  		return cityCode;
  	}

  	public void setCityCode(String cityCode) {
  		this.cityCode = cityCode;
  	}
    
    
	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTrajectoryGps() {
        return trajectoryGps;
    }

    public void setTrajectoryGps(String trajectoryGps) {
        this.trajectoryGps = trajectoryGps == null ? null : trajectoryGps.trim();
    }
}