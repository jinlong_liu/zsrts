package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class SysAuthorization implements Serializable{
    private Integer authId;

    private Integer authDegree;

    private String authName;

    private String remark;

    private Date createDate;

    private String operator;

    public Integer getAuthId() {
        return authId;
    }

    public void setAuthId(Integer authId) {
        this.authId = authId;
    }

    public Integer getauthDegree() {
        return authDegree;
    }

    public void setauthDegree(Integer authDegree) {
        this.authDegree = authDegree;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName == null ? null : authName.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }
}