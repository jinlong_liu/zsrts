package com.zs.traffic.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Inhabitan implements Serializable{
    private Integer inhabitantId;

    private Integer inhabitantNumber;

    private String inhabitantName;

    private String phone;

    private String address;

    private Integer bikeCount;

    private Integer electricBikes;

    public Integer getInhabitantId() {
        return inhabitantId;
    }

    public void setInhabitantId(Integer inhabitantId) {
        this.inhabitantId = inhabitantId;
    }

    public Integer getInhabitantNumber() {
        return inhabitantNumber;
    }

    public void setInhabitantNumber(Integer inhabitantNumber) {
        this.inhabitantNumber = inhabitantNumber;
    }

    public String getInhabitantName() {
        return inhabitantName;
    }

    public void setInhabitantName(String inhabitantName) {
        this.inhabitantName = inhabitantName == null ? null : inhabitantName.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Integer getBikeCount() {
        return bikeCount;
    }

    public void setBikeCount(Integer bikeCount) {
        this.bikeCount = bikeCount;
    }

    public Integer getElectricBikes() {
        return electricBikes;
    }

    public void setElectricBikes(Integer electricBikes) {
        this.electricBikes = electricBikes;
    }
}