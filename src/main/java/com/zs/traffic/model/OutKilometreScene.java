package com.zs.traffic.model;

public class OutKilometreScene {
    private Integer id;

    private String kilometre;

    private String scene;

    private String timeTotal;

    private String timeGo;

    private String timeWait;

    private String timeRiding;

    private String priceTicket;

    private String extendOne;

    private String extendTwo;

    private String costTotal;

    private String costWorking;

    private String costPark;

    private String transportation;

    private String extendThree;

    private String extendFour;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKilometre() {
        return kilometre;
    }

    public void setKilometre(String kilometre) {
        this.kilometre = kilometre == null ? null : kilometre.trim();
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene == null ? null : scene.trim();
    }

    public String getTimeTotal() {
        return timeTotal;
    }

    public void setTimeTotal(String timeTotal) {
        this.timeTotal = timeTotal == null ? null : timeTotal.trim();
    }

    public String getTimeGo() {
        return timeGo;
    }

    public void setTimeGo(String timeGo) {
        this.timeGo = timeGo == null ? null : timeGo.trim();
    }

    public String getTimeWait() {
        return timeWait;
    }

    public void setTimeWait(String timeWait) {
        this.timeWait = timeWait == null ? null : timeWait.trim();
    }

    public String getTimeRiding() {
        return timeRiding;
    }

    public void setTimeRiding(String timeRiding) {
        this.timeRiding = timeRiding == null ? null : timeRiding.trim();
    }

    public String getPriceTicket() {
        return priceTicket;
    }

    public void setPriceTicket(String priceTicket) {
        this.priceTicket = priceTicket == null ? null : priceTicket.trim();
    }

    public String getExtendOne() {
        return extendOne;
    }

    public void setExtendOne(String extendOne) {
        this.extendOne = extendOne == null ? null : extendOne.trim();
    }

    public String getExtendTwo() {
        return extendTwo;
    }

    public void setExtendTwo(String extendTwo) {
        this.extendTwo = extendTwo == null ? null : extendTwo.trim();
    }

    public String getCostTotal() {
        return costTotal;
    }

    public void setCostTotal(String costTotal) {
        this.costTotal = costTotal == null ? null : costTotal.trim();
    }

    public String getCostWorking() {
        return costWorking;
    }

    public void setCostWorking(String costWorking) {
        this.costWorking = costWorking == null ? null : costWorking.trim();
    }

    public String getCostPark() {
        return costPark;
    }

    public void setCostPark(String costPark) {
        this.costPark = costPark == null ? null : costPark.trim();
    }

    public String getTransportation() {
        return transportation;
    }

    public void setTransportation(String transportation) {
        this.transportation = transportation == null ? null : transportation.trim();
    }

    public String getExtendThree() {
        return extendThree;
    }

    public void setExtendThree(String extendThree) {
        this.extendThree = extendThree == null ? null : extendThree.trim();
    }

    public String getExtendFour() {
        return extendFour;
    }

    public void setExtendFour(String extendFour) {
        this.extendFour = extendFour == null ? null : extendFour.trim();
    }
}