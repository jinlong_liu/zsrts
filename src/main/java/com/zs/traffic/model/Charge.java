package com.zs.traffic.model;

public class Charge {
    private Integer id;

    private Integer appuserId;

    private Integer chargeType;

    private Integer registeredQuantity;

    private Integer isChargeStatus;

    private Integer chargeCallTimes;

    private String extended1;

    private String extended2;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public Integer getChargeType() {
        return chargeType;
    }

    public void setChargeType(Integer chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getRegisteredQuantity() {
        return registeredQuantity;
    }

    public void setRegisteredQuantity(Integer registeredQuantity) {
        this.registeredQuantity = registeredQuantity;
    }

    public Integer getIsChargeStatus() {
        return isChargeStatus;
    }

    public void setIsChargeStatus(Integer isChargeStatus) {
        this.isChargeStatus = isChargeStatus;
    }

    public Integer getChargeCallTimes() {
        return chargeCallTimes;
    }

    public void setChargeCallTimes(Integer chargeCallTimes) {
        this.chargeCallTimes = chargeCallTimes;
    }

    public String getExtended1() {
        return extended1;
    }

    public void setExtended1(String extended1) {
        this.extended1 = extended1 == null ? null : extended1.trim();
    }

    public String getExtended2() {
        return extended2;
    }

    public void setExtended2(String extended2) {
        this.extended2 = extended2 == null ? null : extended2.trim();
    }
}