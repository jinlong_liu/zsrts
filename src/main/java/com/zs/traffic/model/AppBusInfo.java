package com.zs.traffic.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class AppBusInfo implements Serializable{
    private Integer id;

    private Integer appuserId;

    private Float displacement;

    private Float actualServiceLife;

    private Integer isfixedParking;

    private String busType;

    private Integer busProperty;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public Float getDisplacement() {
        return displacement;
    }

    public void setDisplacement(Float displacement) {
        this.displacement = displacement;
    }

    public Float getActualServiceLife() {
        return actualServiceLife;
    }

    public void setActualServiceLife(Float actualServiceLife) {
        this.actualServiceLife = actualServiceLife;
    }

    public Integer getIsfixedParking() {
        return isfixedParking;
    }

    public void setIsfixedParking(Integer isfixedParking) {
        this.isfixedParking = isfixedParking;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType == null ? null : busType.trim();
    }

    public Integer getBusProperty() {
        return busProperty;
    }

    public void setBusProperty(Integer busProperty) {
        this.busProperty = busProperty;
    }
}