package com.zs.traffic.model;

import java.util.Date;

import javax.sql.rowset.Predicate;

public class StatisticsParticular {
    private Integer id;

    private Integer areaId;
    
    private String submitDate;
    
	private String submitter;

    private String titleName;
    
    private String address;

    private Float lon;

    private Float lat;

    private Integer status;
    
    private String familyId;
    
    private Integer submitCount;
    
    private Integer submitStage;
    
    private String followDate;
    
    private String districtNumber;
    
    private String districtName;
    
    private String parentSuboffice;
    
    private String subofficeNumber;
    
    private String subofficeName;
    
    private String parentCommuniity;
    
    private String communityNumber;
    
    private String communityName;
    
    private Integer type;
    
    private String ctiyCode;
    
    private String isSurveyor;
    
    private String extendedFieldOne;
    
	private String extendedFieldTwo;
    
	private String extendedFieldThree;
	
    private Float submitLon;

    private Float submitLat;
    
    private String positionDeviate;
	
	private Integer remindNumber;
	
	private Integer numTitle;
	
    public String getDistrictNumber() {
		return districtNumber;
	}

	public void setDistrictNumber(String districtNumber) {
		this.districtNumber = districtNumber;
	}

	public String getParentSuboffice() {
		return parentSuboffice;
	}

	public void setParentSuboffice(String parentSuboffice) {
		this.parentSuboffice = parentSuboffice;
	}

	public String getSubofficeNumber() {
		return subofficeNumber;
	}

	public void setSubofficeNumber(String subofficeNumber) {
		this.subofficeNumber = subofficeNumber;
	}

	public String getParentCommuniity() {
		return parentCommuniity;
	}

	public void setParentCommuniity(String parentCommuniity) {
		this.parentCommuniity = parentCommuniity;
	}

	public String getCommunityNumber() {
		return communityNumber;
	}

	public void setCommunityNumber(String communityNumber) {
		this.communityNumber = communityNumber;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getSubofficeName() {
		return subofficeName;
	}

	public Integer getNumTitle() {
		return numTitle;
	}

	public void setNumTitle(Integer numTitle) {
		this.numTitle = numTitle;
	}

	public String getExtendedFieldThree() {
		return extendedFieldThree;
	}

	public Float getSubmitLon() {
		return submitLon;
	}

	public void setSubmitLon(Float submitLon) {
		this.submitLon = submitLon;
	}

	public Float getSubmitLat() {
		return submitLat;
	}

	public void setSubmitLat(Float submitLat) {
		this.submitLat = submitLat;
	}

	public String getPositionDeviate() {
		return positionDeviate;
	}

	public void setPositionDeviate(String positionDeviate) {
		this.positionDeviate = positionDeviate;
	}

	public void setExtendedFieldThree(String extendedFieldThree) {
		this.extendedFieldThree = extendedFieldThree;
	}

	public Integer getRemindNumber() {
		return remindNumber;
	}

	public void setRemindNumber(Integer remindNumber) {
		this.remindNumber = remindNumber;
	}

	public String getCtiyCode() {
		return ctiyCode;
	}

	public void setCtiyCode(String ctiyCode) {
		this.ctiyCode = ctiyCode;
	}

	public String getIsSurveyor() {
		return isSurveyor;
	}

	public void setIsSurveyor(String isSurveyor) {
		this.isSurveyor = isSurveyor;
	}

	public String getExtendedFieldOne() {
		return extendedFieldOne;
	}

	public void setExtendedFieldOne(String extendedFieldOne) {
		this.extendedFieldOne = extendedFieldOne;
	}

	public String getExtendedFieldTwo() {
		return extendedFieldTwo;
	}

	public void setExtendedFieldTwo(String extendedFieldTwo) {
		this.extendedFieldTwo = extendedFieldTwo;
	}


    

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}

	public String getSubmitter() {
		return submitter;
	}

	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Float getLon() {
		return lon;
	}

	public void setLon(Float lon) {
		this.lon = lon;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public Integer getSubmitCount() {
		return submitCount;
	}

	public void setSubmitCount(Integer submitCount) {
		this.submitCount = submitCount;
	}

	public Integer getSubmitStage() {
		return submitStage;
	}

	public void setSubmitStage(Integer submitStage) {
		this.submitStage = submitStage;
	}

	public String getFollowDate() {
		return followDate;
	}

	public void setFollowDate(String followDate) {
		this.followDate = followDate;
	}

	

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	

	public void setSubofficeName(String subofficeName) {
		this.subofficeName = subofficeName;
	}

	
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

    


    
    
    
   
}