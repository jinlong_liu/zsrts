package com.zs.traffic.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Area implements Serializable{
    private Integer areaId;

    private String districtNumber;

    private String districtName;

    private String parentSuboffice;

    private String subofficeNumber;

    private String subofficeName;

    private String parentCommuniity;

    private String communityNumber;

    private String communityName;

    private Integer type;

    private Integer totalCount;//总指标数

    private Integer everydayCount;//当天指标数
    
    private Integer followCount;//该社区下的关注数
    
    private Integer submitCount;//该社区下问卷提交完成的数量
    
    private Integer notCompleteCount;//该社区下问卷提交未完成的数量
    
    private String cityCode;//城市编码
    
    private String extendedFieldOne;//扩展字段1
    
    private String extendedFieldTwo;//扩展字段2

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getExtendedFieldOne() {
		return extendedFieldOne;
	}

	public void setExtendedFieldOne(String extendedFieldOne) {
		this.extendedFieldOne = extendedFieldOne;
	}

	public String getExtendedFieldTwo() {
		return extendedFieldTwo;
	}

	public void setExtendedFieldTwo(String extendedFieldTwo) {
		this.extendedFieldTwo = extendedFieldTwo;
	}

	public Integer getFollowCount() {
		return followCount;
	}

	public void setFollowCount(Integer followCount) {
		this.followCount = followCount;
	}

	public Integer getSubmitCount() {
		return submitCount;
	}

	public void setSubmitCount(Integer submitCount) {
		this.submitCount = submitCount;
	}

	public Integer getNotCompleteCount() {
		return notCompleteCount;
	}

	public void setNotCompleteCount(Integer notCompleteCount) {
		this.notCompleteCount = notCompleteCount;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getDistrictNumber() {
		return districtNumber;
	}

	public void setDistrictNumber(String districtNumber) {
		this.districtNumber = districtNumber;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getParentSuboffice() {
		return parentSuboffice;
	}

	public void setParentSuboffice(String parentSuboffice) {
		this.parentSuboffice = parentSuboffice;
	}

	public String getSubofficeNumber() {
		return subofficeNumber;
	}

	public void setSubofficeNumber(String subofficeNumber) {
		this.subofficeNumber = subofficeNumber;
	}

	public String getSubofficeName() {
		return subofficeName;
	}

	public void setSubofficeName(String subofficeName) {
		this.subofficeName = subofficeName;
	}

	public String getParentCommuniity() {
		return parentCommuniity;
	}

	public void setParentCommuniity(String parentCommuniity) {
		this.parentCommuniity = parentCommuniity;
	}

	public String getCommunityNumber() {
		return communityNumber;
	}

	public void setCommunityNumber(String communityNumber) {
		this.communityNumber = communityNumber;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getEverydayCount() {
		return everydayCount;
	}

	public void setEverydayCount(Integer everydayCount) {
		this.everydayCount = everydayCount;
	}

   
}