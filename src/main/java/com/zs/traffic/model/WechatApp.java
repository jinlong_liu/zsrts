package com.zs.traffic.model;

public class WechatApp {
    private Integer id;

    private String openid;

    private String familyid;

    private String addid;

    private String apppwd;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public String getFamilyid() {
        return familyid;
    }

    public void setFamilyid(String familyid) {
        this.familyid = familyid == null ? null : familyid.trim();
    }

    public String getAddid() {
        return addid;
    }

    public void setAddid(String addid) {
        this.addid = addid == null ? null : addid.trim();
    }

    public String getApppwd() {
        return apppwd;
    }

    public void setApppwd(String apppwd) {
        this.apppwd = apppwd == null ? null : apppwd.trim();
    }
}