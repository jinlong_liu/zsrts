package com.zs.traffic.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FamilyInformation implements Serializable{
    private Integer familyId;

    private Integer familyType;

    private Integer relationshipHouseholder;

    private Integer sex;

    private Integer age;

    private Integer householdType;

    private Integer isPermanentAddress;

    private String income;

    private String profession;

    private Integer educational;

    private String icCard;

    private String companyAddress;

    private Integer isMarry;

    public Integer getFamilyId() {
        return familyId;
    }

    public void setFamilyId(Integer familyId) {
        this.familyId = familyId;
    }

    public Integer getFamilyType() {
        return familyType;
    }

    public void setFamilyType(Integer familyType) {
        this.familyType = familyType;
    }

    public Integer getRelationshipHouseholder() {
        return relationshipHouseholder;
    }

    public void setRelationshipHouseholder(Integer relationshipHouseholder) {
        this.relationshipHouseholder = relationshipHouseholder;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHouseholdType() {
        return householdType;
    }

    public void setHouseholdType(Integer householdType) {
        this.householdType = householdType;
    }

    public Integer getIsPermanentAddress() {
        return isPermanentAddress;
    }

    public void setIsPermanentAddress(Integer isPermanentAddress) {
        this.isPermanentAddress = isPermanentAddress;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income == null ? null : income.trim();
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession == null ? null : profession.trim();
    }

    public Integer getEducational() {
        return educational;
    }

    public void setEducational(Integer educational) {
        this.educational = educational;
    }

    public String getIcCard() {
        return icCard;
    }

    public void setIcCard(String icCard) {
        this.icCard = icCard == null ? null : icCard.trim();
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress == null ? null : companyAddress.trim();
    }

    public Integer getIsMarry() {
        return isMarry;
    }

    public void setIsMarry(Integer isMarry) {
        this.isMarry = isMarry;
    }
}