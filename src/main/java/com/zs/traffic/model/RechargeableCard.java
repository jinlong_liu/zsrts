package com.zs.traffic.model;

public class RechargeableCard {
    private Integer id;

    private String rechargePassword;

    private Integer rechargeType;

    private Integer validState;

    private String extended1;

    private String extended2;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRechargePassword() {
        return rechargePassword;
    }

    public void setRechargePassword(String rechargePassword) {
        this.rechargePassword = rechargePassword == null ? null : rechargePassword.trim();
    }

    public Integer getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(Integer rechargeType) {
        this.rechargeType = rechargeType;
    }

    public Integer getValidState() {
        return validState;
    }

    public void setValidState(Integer validState) {
        this.validState = validState;
    }

    public String getExtended1() {
        return extended1;
    }

    public void setExtended1(String extended1) {
        this.extended1 = extended1 == null ? null : extended1.trim();
    }

    public String getExtended2() {
        return extended2;
    }

    public void setExtended2(String extended2) {
        this.extended2 = extended2 == null ? null : extended2.trim();
    }
}