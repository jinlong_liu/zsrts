package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class AppLogInfo implements Serializable{
	
    private Integer id;

    private Integer appuserId;

    private String mobilePhoneModel;

    private String mobilePhoneManufacturers;

    private String operatingSystemVersion;

    private Date loginTime;

    private String cityCode;

    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public String getMobilePhoneModel() {
        return mobilePhoneModel;
    }

    public void setMobilePhoneModel(String mobilePhoneModel) {
        this.mobilePhoneModel = mobilePhoneModel == null ? null : mobilePhoneModel.trim();
    }

    public String getMobilePhoneManufacturers() {
        return mobilePhoneManufacturers;
    }

    public void setMobilePhoneManufacturers(String mobilePhoneManufacturers) {
        this.mobilePhoneManufacturers = mobilePhoneManufacturers == null ? null : mobilePhoneManufacturers.trim();
    }

    public String getOperatingSystemVersion() {
        return operatingSystemVersion;
    }

    public void setOperatingSystemVersion(String operatingSystemVersion) {
        this.operatingSystemVersion = operatingSystemVersion == null ? null : operatingSystemVersion.trim();
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode == null ? null : cityCode.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}