package com.zs.traffic.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BusInfo implements Serializable{
    private int id;

    private int inhabitantId;

    private Float displacement;

    private Float actualServiceLife;

    private int isfixedParking;

    private String busType;

    private int busProperty;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInhabitantId() {
        return inhabitantId;
    }

    public void setInhabitantId(int inhabitantId) {
        this.inhabitantId = inhabitantId;
    }

    public Float getDisplacement() {
        return displacement;
    }

    public void setDisplacement(Float displacement) {
        this.displacement = displacement;
    }

    public Float getActualServiceLife() {
        return actualServiceLife;
    }

    public void setActualServiceLife(Float actualServiceLife) {
        this.actualServiceLife = actualServiceLife;
    }

    public int getIsfixedParking() {
        return isfixedParking;
    }

    public void setIsfixedParking(int isfixedParking) {
        this.isfixedParking = isfixedParking;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType == null ? null : busType.trim();
    }

    public int getBusProperty() {
        return busProperty;
    }

    public void setBusProperty(int busProperty) {
        this.busProperty = busProperty;
    }
}