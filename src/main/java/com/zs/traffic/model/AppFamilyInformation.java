package com.zs.traffic.model;

import java.io.Serializable;

/**
 * 用于用户基本信息的填充，有注释的字段需要使用
 */
@SuppressWarnings("serial")
public class AppFamilyInformation implements Serializable{
    //主键
    private Integer familyId;
    //关联用户的主键
    private Integer appuserId;
    //性别
    private Integer sex;
    //年龄
    private Integer age;
    //是否久居
    private Integer isPermanentAddress;
    //收入
    private String income;
    //手机号
    private String phone;
    //创建日期
    private String createDate;

    private Integer familyType;

    private Integer relationshipHouseholder;


    private Integer householdType;


    private Integer profession;

    private Integer educational;

    private String icCard;

    private String companyAddress;

    private Integer isMarry;

    private String professionCustom;

    
    private String userName;
    
    private Integer chargeType;
    
    private Integer isChargeStatus;
    
    private String cityCode;//城市编码

    
    private Integer status;//状态

    public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public Integer getIsChargeStatus() {
		return isChargeStatus;
	}

	public void setIsChargeStatus(Integer isChargeStatus) {
		this.isChargeStatus = isChargeStatus;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getChargeType() {
		return chargeType;
	}

	public void setChargeType(Integer chargeType) {
		this.chargeType = chargeType;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getFamilyId() {
        return familyId;
    }

    public void setFamilyId(Integer familyId) {
        this.familyId = familyId;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public Integer getFamilyType() {
        return familyType;
    }

    public void setFamilyType(Integer familyType) {
        this.familyType = familyType;
    }

    public Integer getRelationshipHouseholder() {
        return relationshipHouseholder;
    }

    public void setRelationshipHouseholder(Integer relationshipHouseholder) {
        this.relationshipHouseholder = relationshipHouseholder;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHouseholdType() {
        return householdType;
    }

    public void setHouseholdType(Integer householdType) {
        this.householdType = householdType;
    }

    public Integer getIsPermanentAddress() {
        return isPermanentAddress;
    }

    public void setIsPermanentAddress(Integer isPermanentAddress) {
        this.isPermanentAddress = isPermanentAddress;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income == null ? null : income.trim();
    }

    public Integer getProfession() {
        return profession;
    }

    public void setProfession(Integer profession) {
        this.profession = profession;
    }

    public Integer getEducational() {
        return educational;
    }

    public void setEducational(Integer educational) {
        this.educational = educational;
    }

    public String getIcCard() {
        return icCard;
    }

    public void setIcCard(String icCard) {
        this.icCard = icCard == null ? null : icCard.trim();
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress == null ? null : companyAddress.trim();
    }

    public Integer getIsMarry() {
        return isMarry;
    }

    public void setIsMarry(Integer isMarry) {
        this.isMarry = isMarry;
    }

    public String getProfessionCustom() {
        return professionCustom;
    }

    public void setProfessionCustom(String professionCustom) {
        this.professionCustom = professionCustom == null ? null : professionCustom.trim();
    }
}