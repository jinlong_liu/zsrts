package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class TravelInformation implements Serializable{
    public Integer getIsSendChildren() {
		return isSendChildren;
	}

	public void setIsSendChildren(Integer isSendChildren) {
		this.isSendChildren = isSendChildren;
	}

	public String getSendAdress() {
		return sendAdress;
	}

	public void setSendAdress(String sendAdress) {
		this.sendAdress = sendAdress;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	private Integer id; //主键 需要给微信段返回的，方便修改

    private Integer appuserId; //关联userid 需要给微信段返回的，方便修改

    private Date startDate; //开始时间

    private Date endDate;  //结束时间

    private String startAddress; //开始地址

    private String endAddress; //结束地址

    private String startGps; //开始时的gps

    private String endGps; //结束时的gps

	private String tripMode;//出行方式

	private String tripObjective;//出行目的

	private Integer status;//状态 是否完善

	private int ranking;//当天的第几条出行信息

    private Date currentDate;//当前日期，到天


    
    private String remark;
    
    private Double distance;//出行距离
    
    private Integer travelTime;//出行时长
    
    private Integer isSendChildren;//是否接送小孩
    
    private String sendAdress;//接送小孩地址
    
    private Date sendDate;//接送时间
    
    private int totalNumber;//全市每天所有参与调查人员的总出行次数
    
    private double totalAvgDis;//全市每天所有参与调查人员的平均出行距离
    
    private double totalAvgTripTime;//全市每天所有参与调查人员的平均出行时长
    
    private double avgDis;//每个人每天出行的平均距离
    
    private double avgTripTime;//每个人每天出行的平均时长
    
    private int trips;//每个人每天出行的次数
    
    private int tripsMode;//某出行方式一天的出行次数
    
    private int tripsObjective;//某出行目的一天的出行次数
    
    private int sex;//性别
    
    private String tripGps;//每次出行的经纬度
    
    private String startFormatAddress;//开始地址（不包含省市区等）
    
    private String endFormatAddress;//结束地址（不包含省市区等）
    

    
    public int getRanking() {
		return ranking;
	}

	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	public String getTripGps() {
		return tripGps;
	}

	public void setTripGps(String tripGps) {
		this.tripGps = tripGps;
	}

	public String getStartFormatAddress() {
		return startFormatAddress;
	}

	public void setStartFormatAddress(String startFormatAddress) {
		this.startFormatAddress = startFormatAddress;
	}

	public String getEndFormatAddress() {
		return endFormatAddress;
	}

	public void setEndFormatAddress(String endFormatAddress) {
		this.endFormatAddress = endFormatAddress;
	}

	public int getStarAge() {
		return starAge;
	}

	public void setStarAge(int starAge) {
		this.starAge = starAge;
	}

	public int getEndAge() {
		return endAge;
	}

	public void setEndAge(int endAge) {
		this.endAge = endAge;
	}

	public int getIsPermanentAddress() {
		return isPermanentAddress;
	}

	public void setIsPermanentAddress(int isPermanentAddress) {
		this.isPermanentAddress = isPermanentAddress;
	}

	public int getProfession() {
		return profession;
	}

	public void setProfession(int profession) {
		this.profession = profession;
	}

	private int personNumber;
    
    private int number;
    
    private int starAge;//开始年龄，年龄范围比较
    
    private int endAge;//结束年龄，年龄范围比较
    
    private int isPermanentAddress;//是否常住
    
    private int profession;//职业
    
    
    private Integer isChargeStatus;
    
    private String cityCode;//城市编码

	public TravelInformation() {
	}

	public TravelInformation(Integer id, Integer appuserId, Date startDate, Date endDate, String startAddress, String endAddress, String startGps, String endGps, String tripMode, String tripObjective, Integer status, Date currentDate, String remark, Double distance, Integer travelTime, Integer isSendChildren, String sendAdress, Date sendDate, int totalNumber, double totalAvgDis, double totalAvgTripTime, double avgDis, double avgTripTime, int trips, int tripsMode, int tripsObjective, int sex, String tripGps, String startFormatAddress, String endFormatAddress, int ranking, int personNumber, int number, int starAge, int endAge, int isPermanentAddress, int profession, Integer isChargeStatus, String cityCode) {
		this.id = id;
		this.appuserId = appuserId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.startAddress = startAddress;
		this.endAddress = endAddress;
		this.startGps = startGps;
		this.endGps = endGps;
		this.tripMode = tripMode;
		this.tripObjective = tripObjective;
		this.status = status;
		this.currentDate = currentDate;
		this.remark = remark;
		this.distance = distance;
		this.travelTime = travelTime;
		this.isSendChildren = isSendChildren;
		this.sendAdress = sendAdress;
		this.sendDate = sendDate;
		this.totalNumber = totalNumber;
		this.totalAvgDis = totalAvgDis;
		this.totalAvgTripTime = totalAvgTripTime;
		this.avgDis = avgDis;
		this.avgTripTime = avgTripTime;
		this.trips = trips;
		this.tripsMode = tripsMode;
		this.tripsObjective = tripsObjective;
		this.sex = sex;
		this.tripGps = tripGps;
		this.startFormatAddress = startFormatAddress;
		this.endFormatAddress = endFormatAddress;
		this.ranking = ranking;
		this.personNumber = personNumber;
		this.number = number;
		this.starAge = starAge;
		this.endAge = endAge;
		this.isPermanentAddress = isPermanentAddress;
		this.profession = profession;
		this.isChargeStatus = isChargeStatus;
		this.cityCode = cityCode;
	}

	public String getCityCode() {
  		return cityCode;
  	}

  	public void setCityCode(String cityCode) {
  		this.cityCode = cityCode;
  	}
    
    public Integer getIsChargeStatus() {
		return isChargeStatus;
	}

	public void setIsChargeStatus(Integer isChargeStatus) {
		this.isChargeStatus = isChargeStatus;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getPersonNumber() {
		return personNumber;
	}

	public void setPersonNumber(int personNumber) {
		this.personNumber = personNumber;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getTripsObjective() {
		return tripsObjective;
	}

	public void setTripsObjective(int tripsObjective) {
		this.tripsObjective = tripsObjective;
	}

	public int getTripsMode() {
		return tripsMode;
	}

	public void setTripsMode(int tripsMode) {
		this.tripsMode = tripsMode;
	}

	public double getAvgDis() {
		return avgDis;
	}

	public void setAvgDis(double avgDis) {
		this.avgDis = avgDis;
	}

	public double getAvgTripTime() {
		return avgTripTime;
	}

	public void setAvgTripTime(double avgTripTime) {
		this.avgTripTime = avgTripTime;
	}

	public int getTrips() {
		return trips;
	}

	public void setTrips(int trips) {
		this.trips = trips;
	}

	public int getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(int totalNumber) {
		this.totalNumber = totalNumber;
	}

	public double getTotalAvgDis() {
		return totalAvgDis;
	}

	public void setTotalAvgDis(double totalAvgDis) {
		this.totalAvgDis = totalAvgDis;
	}

	public double getTotalAvgTripTime() {
		return totalAvgTripTime;
	}

	public void setTotalAvgTripTime(double totalAvgTripTime) {
		this.totalAvgTripTime = totalAvgTripTime;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Integer getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(Integer travelTime) {
		this.travelTime = travelTime;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTripObjective() {
		return tripObjective;
	}

	public void setTripObjective(String tripObjective) {
		this.tripObjective = tripObjective;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress == null ? null : startAddress.trim();
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress == null ? null : endAddress.trim();
    }

    public String getStartGps() {
        return startGps;
    }

    public void setStartGps(String startGps) {
        this.startGps = startGps == null ? null : startGps.trim();
    }

    public String getEndGps() {
        return endGps;
    }

    public void setEndGps(String endGps) {
        this.endGps = endGps == null ? null : endGps.trim();
    }

    public String getTripMode() {
        return tripMode;
    }

    public void setTripMode(String tripMode) {
        this.tripMode = tripMode == null ? null : tripMode.trim();
    }

	@Override
	public String toString() {
		return "TravelInformation{" +
				"id=" + id +
				", appuserId=" + appuserId +
				", startDate=" + startDate +
				", endDate=" + endDate +
				", startAddress='" + startAddress + '\'' +
				", endAddress='" + endAddress + '\'' +
				", startGps='" + startGps + '\'' +
				", endGps='" + endGps + '\'' +
				", tripMode='" + tripMode + '\'' +
				", tripObjective='" + tripObjective + '\'' +
				", status=" + status +
				", currentDate=" + currentDate +
				", remark='" + remark + '\'' +
				", distance=" + distance +
				", travelTime=" + travelTime +
				", isSendChildren=" + isSendChildren +
				", sendAdress='" + sendAdress + '\'' +
				", sendDate=" + sendDate +
				", totalNumber=" + totalNumber +
				", totalAvgDis=" + totalAvgDis +
				", totalAvgTripTime=" + totalAvgTripTime +
				", avgDis=" + avgDis +
				", avgTripTime=" + avgTripTime +
				", trips=" + trips +
				", tripsMode=" + tripsMode +
				", tripsObjective=" + tripsObjective +
				", sex=" + sex +
				", tripGps='" + tripGps + '\'' +
				", startFormatAddress='" + startFormatAddress + '\'' +
				", endFormatAddress='" + endFormatAddress + '\'' +
				", ranking=" + ranking +
				", personNumber=" + personNumber +
				", number=" + number +
				", starAge=" + starAge +
				", endAge=" + endAge +
				", isPermanentAddress=" + isPermanentAddress +
				", profession=" + profession +
				", isChargeStatus=" + isChargeStatus +
				", cityCode='" + cityCode + '\'' +
				'}';
	}
}