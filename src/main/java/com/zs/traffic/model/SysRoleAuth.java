package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class SysRoleAuth implements Serializable{
    private Integer id;

    private Integer sysRoleId;

    private Integer sysAuthId;

    private Date createDate;

    private String operator;
    
    private Integer authDegree;//关联查询权限等级
    

    public Integer getAuthDegree() {
		return authDegree;
	}

	public void setAuthDegree(Integer authDegree) {
		this.authDegree = authDegree;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSysRoleId() {
        return sysRoleId;
    }

    public void setSysRoleId(Integer sysRoleId) {
        this.sysRoleId = sysRoleId;
    }

    public Integer getSysAuthId() {
        return sysAuthId;
    }

    public void setSysAuthId(Integer sysAuthId) {
        this.sysAuthId = sysAuthId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }
}