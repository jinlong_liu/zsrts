package com.zs.traffic.model;

public class CityCodeUrl {
    private Integer id;

    private String cityCode;

    private String cityName;

    private String cityUrl;

    private String extendedFieldOne;

    private String extendedFieldTwo;
    
    private String prizeSign;
    
    private String extendedFieldThree;
    
    private String signatureContent;
    

    public String getSignatureContent() {
		return signatureContent;
	}

	public void setSignatureContent(String signatureContent) {
		this.signatureContent = signatureContent;
	}

	public String getPrizeSign() {
		return prizeSign;
	}

	public void setPrizeSign(String prizeSign) {
		this.prizeSign = prizeSign;
	}

	public String getExtendedFieldThree() {
		return extendedFieldThree;
	}

	public void setExtendedFieldThree(String extendedFieldThree) {
		this.extendedFieldThree = extendedFieldThree;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode == null ? null : cityCode.trim();
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName == null ? null : cityName.trim();
    }

    public String getCityUrl() {
        return cityUrl;
    }

    public void setCityUrl(String cityUrl) {
        this.cityUrl = cityUrl == null ? null : cityUrl.trim();
    }

    public String getExtendedFieldOne() {
        return extendedFieldOne;
    }

    public void setExtendedFieldOne(String extendedFieldOne) {
        this.extendedFieldOne = extendedFieldOne == null ? null : extendedFieldOne.trim();
    }

    public String getExtendedFieldTwo() {
        return extendedFieldTwo;
    }

    public void setExtendedFieldTwo(String extendedFieldTwo) {
        this.extendedFieldTwo = extendedFieldTwo == null ? null : extendedFieldTwo.trim();
    }
}