/**
 * Copyright (C), 2015-2020, XXX有限公司
 * FileName: RoleGrade
 * Author:   dellxh
 * Date:     2020/11/11 14:41
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zs.traffic.model;

import java.io.Serializable;
/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author dellxh
 * @create 2020/11/11
 * @since 1.0.0
 */

public class RoleGrade  implements Serializable {

    private Integer snumber;
    private String rolegradeName;
    private Integer math;
    private Integer english;
    private Integer chinese;


    public String getRolegradeName() {
        return rolegradeName;
    }

    public void setRolegradeName(String rolegradeName) {
        this.rolegradeName = rolegradeName;
    }

    @Override
    public String toString() {
        return "SysRole [snumber=" + snumber + ", roleName=" + rolegradeName
                + ", math=" + math + ", english=" + english
                + ", chinese=" + chinese + "]";
    }

    public Integer getSnumber() {
        return snumber;
    }

    public void setSnumber(Integer snumber) {
        this.snumber = snumber;
    }



    public Integer getMath() {
        return math;
    }

    public void setMath(Integer math) {
        this.math = math;
    }

    public Integer getEnglish() {
        return english;
    }

    public void setEnglish(Integer english) {
        this.english = english;
    }

    public Integer getChinese() {
        return chinese;
    }

    public void setChinese(Integer chinese) {
        this.chinese = chinese;
    }
}
