package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class User implements Serializable{
    private Integer userId;

    private String userName;

    private String realName;
    
    //  关联数据
    private Integer roleId;
    
    private String roleName;
    
    private	Integer authId;
    
    private Integer authDegree;
    
    public Integer getAuthDegree() {
		return authDegree;
	}

	public void setAuthDegree(Integer authDegree) {
		this.authDegree = authDegree;
	}

	private String authName;
	
	//区域id
	private Integer areaId;
    public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	// 行政区
    private String districtNumber;
    
    private String	districtName;
    // 街道
    private String subofficeNumber;
    
    private String subofficeName;
    //社区
    private String communityNumber;
    
    private String communityName;
    
    private Integer typeInt;
    //关联结束
    

	private String userPassword;

    private String remark;

    private String createDate;

    private String operator;

    private Integer status;

    private String phone;//电话号码
    
    private Integer isSuperAdmin;//是否是超级管理员
    
    private String cityCode;//城市编码
    

	public Integer getIsSuperAdmin() {
		return isSuperAdmin;
	}

	public void setIsSuperAdmin(Integer isSuperAdmin) {
		this.isSuperAdmin = isSuperAdmin;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getTypeInt() {
		return typeInt;
	}

	public void setTypeInt(Integer typeInt) {
		this.typeInt = typeInt;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Integer getAuthId() {
		return authId;
	}

	public void setAuthId(Integer authId) {
		this.authId = authId;
	}

	public String getAuthName() {
		return authName;
	}

	public void setAuthName(String authName) {
		this.authName = authName;
	}



	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	

	public String getSubofficeName() {
		return subofficeName;
	}

	public void setSubofficeName(String subofficeName) {
		this.subofficeName = subofficeName;
	}

	

	public String getDistrictNumber() {
		return districtNumber;
	}

	public void setDistrictNumber(String districtNumber) {
		this.districtNumber = districtNumber;
	}

	public String getSubofficeNumber() {
		return subofficeNumber;
	}

	public void setSubofficeNumber(String subofficeNumber) {
		this.subofficeNumber = subofficeNumber;
	}

	public String getCommunityNumber() {
		return communityNumber;
	}

	public void setCommunityNumber(String communityNumber) {
		this.communityNumber = communityNumber;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

  

    public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

	@Override
	public String toString() {
		return "User{" +
				"userId=" + userId +
				", userName='" + userName + '\'' +
				", realName='" + realName + '\'' +
				", roleId=" + roleId +
				", roleName='" + roleName + '\'' +
				", authId=" + authId +
				", authDegree=" + authDegree +
				", authName='" + authName + '\'' +
				", areaId=" + areaId +
				", districtNumber='" + districtNumber + '\'' +
				", districtName='" + districtName + '\'' +
				", subofficeNumber='" + subofficeNumber + '\'' +
				", subofficeName='" + subofficeName + '\'' +
				", communityNumber='" + communityNumber + '\'' +
				", communityName='" + communityName + '\'' +
				", typeInt=" + typeInt +
				", userPassword='" + userPassword + '\'' +
				", remark='" + remark + '\'' +
				", createDate='" + createDate + '\'' +
				", operator='" + operator + '\'' +
				", status=" + status +
				", phone='" + phone + '\'' +
				", isSuperAdmin=" + isSuperAdmin +
				", cityCode='" + cityCode + '\'' +
				'}';
	}
}