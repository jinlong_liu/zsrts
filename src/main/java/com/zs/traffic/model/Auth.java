package com.zs.traffic.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *用户表
 */
@SuppressWarnings("serial")
public class Auth implements Serializable{
 
    private int id;
    private String name;//名字
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
}
