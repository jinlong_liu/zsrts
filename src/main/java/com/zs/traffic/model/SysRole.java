package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class SysRole implements Serializable{
    private Integer roleId;

    private String roleName;

    private String remark;

    private String createDate;
    
    private String describeActor;
    
    private String operator;

    private Integer authDegree;//关联查询
  

	public Integer getAuthDegree() {
		return authDegree;
	}

	public void setAuthDegree(Integer authDegree) {
		this.authDegree = authDegree;
	}

	@Override
	public String toString() {
		return "SysRole [roleId=" + roleId + ", roleName=" + roleName
				+ ", remark=" + remark + ", createDate=" + createDate
				+ ", describeActor=" + describeActor + ", operator=" + operator
				+ "]";
	}

	public String getDescribeActor() {
		return describeActor;
	}

	public void setDescribeActor(String describeActor) {
		this.describeActor = describeActor;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }
}