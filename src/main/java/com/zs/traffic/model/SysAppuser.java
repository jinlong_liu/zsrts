package com.zs.traffic.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信公众号用户
 */
@SuppressWarnings("serial")
public class SysAppuser implements Serializable{
	//主键
    private Integer appuserId;

    private String userName;

    private String userPassword;
	//创建日期
    private String createDate;
	//电车
    private Integer electricBikes;
    //年龄
    private Integer age;
    //性别
    private Integer sex;
	//汽车数量
	private Integer carCount;

    private String Occupation;//废弃字段，改为ip
    
    public String getOccupation() {
		return Occupation;
	}

	public void setOccupation(String occupation) {
		Occupation = occupation;
	}

	private String familyAddress;
    
    private Integer stag;//填写进度
    

    
    private Integer homesCount;//家庭成员数量
    
    private Integer isChargeStatus;
    
    private String cityCode;//城市编码

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public Integer getIsChargeStatus() {
		return isChargeStatus;
	}

	public void setIsChargeStatus(Integer isChargeStatus) {
		this.isChargeStatus = isChargeStatus;
	}

	public Integer getCarCount() {
		return carCount;
	}

	public void setCarCount(Integer carCount) {
		this.carCount = carCount;
	}

	public Integer getHomesCount() {
		return homesCount;
	}

	public void setHomesCount(Integer homesCount) {
		this.homesCount = homesCount;
	}

	public Integer getAppuserId() {
		return appuserId;
	}

	public void setAppuserId(Integer appuserId) {
		this.appuserId = appuserId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getFamilyAddress() {
		return familyAddress;
	}

	public void setFamilyAddress(String familyAddress) {
		this.familyAddress = familyAddress;
	}

	public Integer getStag() {
		return stag;
	}

	public void setStag(Integer stag) {
		this.stag = stag;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public Integer getElectricBikes() {
		return electricBikes;
	}

	public void setElectricBikes(Integer electricBikes) {
		this.electricBikes = electricBikes;
	}
	
	//重写toString()方法
	/*public String toString(){
		return appuserId + " " + userName + " " + userPassword+ " "
				+ "" + remark + " " +createDate + " " + bikeCount+ " " +electricBikes;
	}*/
    
}