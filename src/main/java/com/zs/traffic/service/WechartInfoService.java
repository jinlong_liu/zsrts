package com.zs.traffic.service;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.WechartInfo;
@Service
public interface WechartInfoService {
	public int insertSelective(WechartInfo wechartInfo);
}
