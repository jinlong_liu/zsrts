package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.ChargeMapper;
import com.zs.traffic.model.Charge;
import com.zs.traffic.service.ChargeService;

@Service
public class ChargeServiceImp implements ChargeService{

	@Autowired
	private ChargeMapper model;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Charge record) {
		// TODO Auto-generated method stub
		return model.insert(record);
	}

	@Override
	public int insertSelective(Charge record) {
		// TODO Auto-generated method stub
		return model.insertSelective(record);
	}

	@Override
	public Charge selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Charge record) {
		// TODO Auto-generated method stub
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Charge record) {
		// TODO Auto-generated method stub
		return model.updateByPrimaryKey(record);
	}


	@Override
	public int Appaudit(Integer userId, Integer status , String opportunity ) {
		// TODO Auto-generated method stub
		return model.Appaudit(userId, status, opportunity);
	}

	@Override
	public Charge selectAppaudit(Integer userId) {
		// TODO Auto-generated method stub
		return model.selectAppaudit(userId);
	}

	


	@Override
	public int selectTotal(Integer rechargeType) {
		return model.selectTotal(rechargeType);
	}

	@Override
	public int selectTodayNumber(Integer rechargeType, String today, String tomorrowDay) {
		return model.selectTodayNumber(rechargeType, today, tomorrowDay);
	}


	@Override
	public List<Charge> selectByStateForBill(Integer state) {
		// TODO Auto-generated method stub
		return model.selectByStateForBill(state);
	}


}

