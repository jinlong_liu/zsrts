package com.zs.traffic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.WechartInfoMapper;
import com.zs.traffic.model.WechartInfo;
import com.zs.traffic.service.WechartInfoService;

@Service
public class WechartInfoServiceImp implements WechartInfoService{
	@Autowired
	private WechartInfoMapper wechartInfoMapper;
	@Override
	public int insertSelective(WechartInfo wechartInfo) {
		return wechartInfoMapper.insertSelective(wechartInfo);
	}

}
