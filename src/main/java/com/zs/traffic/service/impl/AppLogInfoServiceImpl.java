package com.zs.traffic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zs.traffic.dao.AppLogInfoMapper;
import com.zs.traffic.model.AppLogInfo;
import com.zs.traffic.service.AppLogInfoService;
/**
 * 
 * @author
 *
 */
@Service
public class AppLogInfoServiceImpl implements  AppLogInfoService{

	@Autowired
	private AppLogInfoMapper model;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(AppLogInfo record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(AppLogInfo record) {
		return model.insertSelective(record);
	}

	@Override
	public AppLogInfo selectByPrimaryKey(Integer id) {
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(AppLogInfo record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(AppLogInfo record) {
		return model.updateByPrimaryKey(record);
	}
	

	
}
