package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.AreaMapper;
import com.zs.traffic.model.Area;
import com.zs.traffic.service.AreaService;

/**
 * 
 * @author
 *
 */
@Service
public class AreaServiceImpl implements AreaService{
	
	@Autowired
	private AreaMapper model;

	@Override
	public int deleteByPrimaryKey(Integer areaId) {
		return model.deleteByPrimaryKey(areaId);
	}

	@Override
	public int insert(Area record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(Area record) {
		return model.insertSelective(record);
	}

	@Override
	public Area selectByPrimaryKey(Integer areaId) {
		return model.selectByPrimaryKey(areaId);
	}

	@Override
	public int updateByPrimaryKeySelective(Area record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Area record) {
		return model.updateByPrimaryKey(record);
	}

	@Override
	public List<Area> getListByName(String Name, Integer type) {
		return model.getListByName(Name, type);
	}

	@Override
	public List<Area> getListByType(Integer type) {
		return model.getListByType(type);
	}

	@Override
	public int updateName(Area record, String name) {
		return model.updateName(record, name);
	}

	@Override
	public List<Area> getListByNames(String name,Integer type) {
		return model.getListByNames(name, type);
	}

	@Override
	public List<Area> getAllList() {
		return model.getAllList();
	}

	@Override
	public List<Area> getSelectListByName(String Name, Integer type) {
		return model.getSelectListByName(Name, type);
	}

	@Override
	public List<Area> getSelectListByNames(String Name,Integer type,String cityCode) {
		return model.getSelectListByNames(Name,type,cityCode);
	}

	@Override
	public List<Area> getListByNameOrNum(String Name, String number,
			Integer type,String cityCode,String districtNumber) {
		return model.getListByNameOrNum(Name, number, type,cityCode, districtNumber);
	}

	@Override
	public List<Area> getListByNameOrNumById(String Name, String number,
			Integer id, Integer type) {
		return model.getListByNameOrNumById(Name, number, id, type);
	}

	@Override
	public List<Area> getListByNameOrNumInclude(String name,String number,Integer type,String cityCode) {
		return model.getListByNameOrNumInclude(name, number,type,cityCode);
	}


	@Override
	public List<Area> selectAll() {
		// TODO Auto-generated method stub
		return model.selectAll();
	}

	@Override
	public List<Area> getSelectTypeOne(String cityCode) {
		// TODO Auto-generated method stub
		return model.getSelectTypeOne(cityCode);
	}

	@Override
	public List<Area> getListForQrAll(String districtSelectId, String subOfficeSelectId,
			String communitySelectName,String cityCode) {
		// TODO Auto-generated method stub
		return model.getListForQrAll(districtSelectId, subOfficeSelectId, communitySelectName, cityCode);
	}


	public List<Area> getAreaByuserId(Integer userId ,Integer type) {
		return model.getAreaByuserId(userId,type);
	}

	@Override
	public List<Area> getAllListArea(Integer type,String cityCode) {
		return model.getAllListArea(type,cityCode);
	}

	@Override
	public List<Area> getAreaList(List<String> areaNumber, Integer authDegree,String cityCode) {
		return model.getAreaListByAuthDegree(areaNumber,authDegree,cityCode);
	}

	@Override
	public List<Area> queryByareaNumber(List<String> areaNumber, Integer authDegree,String cityCode) {
		return model.queryByareaNumber(areaNumber,authDegree,cityCode);
	}


	@Override
	public List<Area> queryByRegion() {
		// TODO Auto-generated method stub
		return model.queryByRegion();
	}

	@Override
	public List<Area> queryListArea(String districtName) {
		// TODO Auto-generated method stub
		return model.queryListArea(districtName);
	}

	@Override
	public List<Area> queryAreaList(String subofficeName) {
		// TODO Auto-generated method stub
		return model.queryAreaList(subofficeName);
	}

	@Override
	public List<Area> queryAreaIst(String districtName, String subofficeName) {
		// TODO Auto-generated method stub
		return model.queryAreaIst(districtName, subofficeName);
	}

	@Override
	public List<Area> queryListParameter(String districtNumber,String subofficeNumber) {
		// TODO Auto-generated method stub
		return model.queryListParameter(districtNumber,subofficeNumber);
	}

	@Override
	public List<Area> queryParameterList(String districtName,
			String subofficeName) {
		// TODO Auto-generated method stub
		return model.queryParameterList(districtName, subofficeName);
	}

	@Override
	public Area getSelectByConumber(String communityNumber) {
		// TODO Auto-generated method stub
		return model.getSelectByConumber(communityNumber);
	}

	@Override
	public List<Area> getAreaId(String communityNumber) {
		// TODO Auto-generated method stub
		return model.getAreaId(communityNumber);
	}

	@Override
	public Area getAreaIdByTitltName(String titltName) {
		// TODO Auto-generated method stub
		return model.getAreaIdByTitltName(titltName);
	}


	@Override
	public List<Area> queryAreaByRole(int userId, int authDegree, int type,String cityCode) {
		return model.queryAreaByRole(userId, authDegree, type,cityCode);
	}

	@Override
	public List<Area> selectCommunityName(String subofficeNumber) {
		// TODO Auto-generated method stub
		return model.selectCommunityName(subofficeNumber);
	}

	@Override
	public Area addEcho(Integer areaId) {
		// TODO Auto-generated method stub
		return model.addEcho(areaId);
	}

	@Override
	public List<Area> selectDistrictNumber(String districtNumberUpdate,String cityCode) {
		// TODO Auto-generated method stub
		return model.selectDistrictNumber(districtNumberUpdate,cityCode);
	}

	@Override
	public List<Area> selectDistrictName(String districtNameUpdate,String cityCode) {
		// TODO Auto-generated method stub
		return model.selectDistrictName(districtNameUpdate,cityCode);
	}

	@Override
	public List<Area> selectSubofficeNumber(String districtNumber) {
		// TODO Auto-generated method stub
		return model.selectSubofficeNumber(districtNumber);
	}

	@Override
	public int updateDistrict(String districtNumberUpdate,
			String districtNameUpdate, String districtNumber,String cityCode) {
		// TODO Auto-generated method stub
		return model.updateDistrict(districtNumberUpdate, districtNameUpdate, districtNumber,cityCode);
	}

	@Override
	public List<Area> selectSubofficeNumbers(String subofficeNumberUpdate,String cityCode) {
		// TODO Auto-generated method stub
		return model.selectSubofficeNumbers(subofficeNumberUpdate,cityCode);
	}

	@Override
	public List<Area> selectSubofficeNames(String subofficeNameUpdate,String cityCode) {
		// TODO Auto-generated method stub
		return model.selectSubofficeNames(subofficeNameUpdate,cityCode);
	}

	@Override
	public List<Area> selectCommunityNumber(String subofficeNumber,String cityCode) {
		// TODO Auto-generated method stub
		return model.selectCommunityNumber(subofficeNumber,cityCode);
	}

	@Override
	public int updateSuboffice(String subofficeNumberUpdate,
			String subofficeNameUpdate, String subofficeNumber,String cityCode) {
		// TODO Auto-generated method stub
		return model.updateSuboffice(subofficeNumberUpdate, subofficeNameUpdate, subofficeNumber,cityCode);
	}

	@Override
	public List<Area> selectCommunityNumberUpdates(String communityNumberUpdate,String cityCode) {
		// TODO Auto-generated method stub
		return model.selectCommunityNumberUpdates(communityNumberUpdate,cityCode);
	}

	@Override
	public List<Area> selectCommunityNameUpdates(String communityNameUpdate,String cityCode) {
		// TODO Auto-generated method stub
		return model.selectCommunityNameUpdates(communityNameUpdate,cityCode);
	}

	@Override
	public List<Area> selectSub(String communityNumber) {
		// TODO Auto-generated method stub
		return model.selectSub(communityNumber);
	}

	@Override
	public int updateCommunity(String communityNumberUpdate,
			String communityNameUpdate, String communityNumber,String cityCode) {
		// TODO Auto-generated method stub
		return model.updateCommunity(communityNumberUpdate, communityNameUpdate, communityNumber,cityCode);
	}

	@Override
	public List<Area> getListSubByNameWlj(Integer type, String cityCode, String areaId) {
		// TODO Auto-generated method stub
		return model.getListSubByNameWlj(type, cityCode, areaId);
	}

	@Override
	public List<Area> selectAllForCity(String city) {
		// TODO Auto-generated method stub
		return model.selectAllForCity(city);
	}





	


}
