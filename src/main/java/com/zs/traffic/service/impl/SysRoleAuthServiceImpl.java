package com.zs.traffic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.AppBusInfoMapper;
import com.zs.traffic.dao.SysRoleAuthMapper;
import com.zs.traffic.model.AppBusInfo;
import com.zs.traffic.model.SysRoleAuth;
import com.zs.traffic.service.AppBusInfoService;
import com.zs.traffic.service.SysRoleAuthService;

/**
 * 
 * @author
 *
 */
@Service
public class SysRoleAuthServiceImpl implements SysRoleAuthService{
	
	@Autowired
	private SysRoleAuthMapper model;

	@Override
	public SysRoleAuth getSysRoleAuthByRoleId(Integer roleId) {
		return model.getSysRoleAuthByRoleId(roleId);
	}

	
}
