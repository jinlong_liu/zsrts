package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.RechargeableCardMapper;
import com.zs.traffic.model.RechargeableCard;
import com.zs.traffic.service.RechargeableCardService;

@Service
public class RechargeableCardServiceImp implements RechargeableCardService{
	@Autowired
	private RechargeableCardMapper model;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(RechargeableCard record) {
		// TODO Auto-generated method stub
		return model.insert(record);
	}

	@Override
	public int insertSelective(RechargeableCard record) {
		// TODO Auto-generated method stub
		return model.insertSelective(record);
	}

	@Override
	public RechargeableCard selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return model.selectByPrimaryKey(id);
		}

	@Override
	public int updateByPrimaryKeySelective(RechargeableCard record) {
		// TODO Auto-generated method stub
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(RechargeableCard record) {
		// TODO Auto-generated method stub
		return model.updateByPrimaryKey(record);
	}

	@Override
	public List<RechargeableCard> selectListByTypeSign(Integer state, Integer type) {
		// TODO Auto-generated method stub
		return model.selectListByTypeSign(state, type);
	}
	
}

