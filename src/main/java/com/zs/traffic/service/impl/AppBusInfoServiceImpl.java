package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.AppBusInfoMapper;
import com.zs.traffic.model.AppBusInfo;
import com.zs.traffic.service.AppBusInfoService;

/**
 * 
 * @author
 *
 */
@Service
public class AppBusInfoServiceImpl implements AppBusInfoService{
	
	@Autowired
	private AppBusInfoMapper model;

	@Override
	public AppBusInfo selectByPrimaryKey(Integer id) {
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int insertSelective(AppBusInfo record) {
		return model.insertSelective(record);
	}

	@Override
	public int insert(AppBusInfo record) {
		return model.insert(record);
	}

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(AppBusInfo record) {
		return model.updateByPrimaryKey(record);
	}

	@Override
	public int updateByPrimaryKey(AppBusInfo record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<AppBusInfo> selectByAppuserId(Integer appuserId) {
		return model.selectByAppuserId(appuserId);
	}

	@Override
	public int deleteByAppUserId(Integer appuserId) {
		return model.deleteByAppUserId(appuserId);
	}
	
}
