package com.zs.traffic.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.TravelInformationMapper;
import com.zs.traffic.model.TravelInformation;
import com.zs.traffic.service.TravelInformationService;

/**
 * 
 * @author
 *
 */
@Service
public class TravelInformationServiceImpl implements TravelInformationService{

	@Autowired
	public TravelInformationMapper model;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(TravelInformation record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(TravelInformation record) {
		return model.insertSelective(record);
	}

	@Override
	public TravelInformation selectByPrimaryKey(Integer id) {
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(TravelInformation record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(TravelInformation record) {
		return model.updateByPrimaryKey(record);
	}

	@Override
	public List<TravelInformation> selectByAppuserId(Integer appuserId,Date todayDate) {
		return model.selectByAppuserId(appuserId,todayDate);
	}

	@Override
	public List<TravelInformation> getTravelInformation(Integer appuserId) {
		// TODO Auto-generated method stub
		return model.getTravelInformation(appuserId);
	}

	@Override
	public TravelInformation selectTotalNumber(Date todayDate,String cityCode) {
		return model.selectTotalNumber(todayDate,cityCode);
	}

	@Override
	public List<TravelInformation> selectByDate(Date todayDate,String cityCode) {
		return model.selectByDate(todayDate,cityCode);
	}

	@Override
	public List<TravelInformation> selectTripByAppuserId(Integer appuserId, Date todayDate) {
		return model.selectTripByAppuserId(appuserId,todayDate);
	}

	@Override
	public List<TravelInformation> selectTripObjectiveByAppuserId(Integer appuserId, Date todayDate) {
		return model.selectTripObjectiveByAppuserId(appuserId,todayDate);
	}

	@Override
	public List<TravelInformation> getTravelConditions(Integer appuserId) {
		// TODO Auto-generated method stub
		return model.getTravelConditions(appuserId);
	}

	@Override
	public List<TravelInformation> getDetailsInformation(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TravelInformation> getAnalysis(Date todayDate) {
		return model.getAnalysis(todayDate);
	}

	@Override
	public int deleteByAppuserId(Integer appuserId,Date date) {
		return model.deleteByAppuserId(appuserId,date);
	}

	@Override
	public int deleteByAppuserIdStatus(Integer appuserId, Date date) {
		return model.deleteByAppuserIdStatus(appuserId, date);
	}

	@Override
	public List<TravelInformation> getIsTravel(Integer appuserId) {
		// TODO Auto-generated method stub
		return model.getIsTravel(appuserId);
	}

	@Override
	public List<TravelInformation> selectTraGps(Integer appuserId, Date dayDate) {
		return model.selectTraGps(appuserId, dayDate);
	}

	@Override
	public List<TravelInformation> selectByRanking(int appuserId, Date startDate, int ranking) {
		return model.selectByRanking(appuserId, startDate, ranking);
	}

	@Override
	public int deleteByRank(int appuserId, Date startDate, int ranking) {
		return model.deleteByRank(appuserId,startDate,ranking);
	}

	@Override
	public List<TravelInformation> selectByAppuserIdNear(Integer appuserId, Date todayDate) {
		return model.selectByAppuserIdNear(appuserId,todayDate);
	}

	@Override
	public List<TravelInformation> selectPush(Integer appuserId, Date dayDate) {
		return model.selectPush(appuserId,dayDate);
	}
	 
}
