package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.AreaMapper;
import com.zs.traffic.dao.OutKilometreSceneMapper;
import com.zs.traffic.model.OutKilometreScene;
import com.zs.traffic.service.OutKilometreSceneService;

@Service
public class OutKilometreSceneServiceImp implements OutKilometreSceneService{

	@Autowired
	public OutKilometreSceneMapper model;
	
	@Override
	public List<OutKilometreScene> selectByKmSceneTransportation(String kilometre, String scene) {
		// TODO Auto-generated method stub
		return model.selectByKmSceneTransportation(kilometre, scene);
	}

	@Override
	public List<OutKilometreScene> selectByTimeWord(String timeTotal, String timeGo, String timeWait, String timeRiding,
			String priceTicket, String costTotal, String costWorking, String costPark, String transportation) {
		// TODO Auto-generated method stub
		return model.selectByTimeWord(timeTotal, timeGo, timeWait, timeRiding, priceTicket, costTotal, costWorking, costPark, transportation);
	}

	@Override
	public List<OutKilometreScene> selectByTimeWord1(String timeTotal, String timeGo, String timeWait,
			String timeRiding, String priceTicket, String transportation) {
		// TODO Auto-generated method stub
		return model.selectByTimeWord1(timeTotal, timeGo, timeWait, timeRiding, priceTicket, transportation);
	}

	@Override
	public List<OutKilometreScene> selectByTimeWord3(String timeTotal, String priceTicket, String transportation) {
		// TODO Auto-generated method stub
		return model.selectByTimeWord3(timeTotal, priceTicket, transportation);
	}

	@Override
	public List<OutKilometreScene> selectByTimeWord4(String timeTotal, String transportation) {
		// TODO Auto-generated method stub
		return model.selectByTimeWord4(timeTotal, transportation);
	}

	@Override
	public List<OutKilometreScene> selectByTimeWord5(String timeTotal, String transportation) {
		// TODO Auto-generated method stub
		return model.selectByTimeWord5(timeTotal, transportation);
	}

	@Override
	public List<OutKilometreScene> selectByTimeWord2(String timeTotal, String costTotal, String costWorking,
			String costPark, String transportation) {
		// TODO Auto-generated method stub
		return model.selectByTimeWord2(timeTotal, costTotal, costWorking, costPark, transportation);
	}

	@Override
	public List<OutKilometreScene> selectByTimeWord31(String timeTotal, String costTotal, String transportation) {
		// TODO Auto-generated method stub
		return model.selectByTimeWord31(timeTotal, costTotal, transportation);
	}

}
