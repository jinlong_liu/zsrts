/**
 * Copyright (C), 2015-2020, XXX有限公司
 * FileName: GradeRoleServiceImpl
 * Author:   dellxh
 * Date:     2020/11/11 14:34
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zs.traffic.service.impl;

import java.util.List;

import com.zs.traffic.dao.RoleGradeDao;
import com.zs.traffic.model.RoleGrade;
import com.zs.traffic.service.RoleGradeService;
import com.zs.traffic.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zs.traffic.model.SysAuthorization;
import com.zs.traffic.model.SysRole;
import com.zs.traffic.model.SysRoleAuth;

@Service

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author dellxh
 * @create 2020/11/11
 * @since 1.0.0
 */
public class RoleGradeServiceImpl implements RoleGradeService {

    @Autowired
    public RoleGradeDao roleDao;

    @Override
    public List<RoleGrade> getGradeRoleList() {
        return roleDao.getGradeRoleList();
    }

    @Override
    public int deleteGradeRole(int id) {
        return roleDao.deleteGradeRole(id);
    }

    @Override
    public int selectGradeRoleUser(int id) {
        return 0;
    }


}
