package com.zs.traffic.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.AppBusInfoMapper;
import com.zs.traffic.dao.AppSmsMapper;
import com.zs.traffic.model.AppBusInfo;
import com.zs.traffic.model.AppSms;
import com.zs.traffic.service.AppBusInfoService;
import com.zs.traffic.service.AppMsmService;

/**
 * 
 * @author
 *
 */
@Service
public class AppMsmServiceImpl implements AppMsmService{
	
	@Autowired
	private AppSmsMapper model;


	@Override
	public List<AppSms> selectByUserNameTime(String userName, String cityCode, Date sendTime) {
		// TODO Auto-generated method stub
		return model.selectByUserNameTime(userName, cityCode, sendTime);
	}

	@Override
	public int insertSelective(AppSms record) {
		// TODO Auto-generated method stub
		return model.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(AppSms record) {
		// TODO Auto-generated method stub
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<AppSms> queryRequestTime(String userName) {
		// TODO Auto-generated method stub
		return model.queryRequestTime(userName);
	}

	
}
