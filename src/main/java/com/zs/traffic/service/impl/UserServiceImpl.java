package com.zs.traffic.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.zs.constant.ZsAuthDegree;
import com.zs.traffic.dao.UserDao;
import com.zs.traffic.model.SysRoleAuth;
import com.zs.traffic.model.SysUserRole;
import com.zs.traffic.model.User;
import com.zs.traffic.model.UserArea;
import com.zs.traffic.service.AreaService;
import com.zs.traffic.service.SysRoleAuthService;
import com.zs.traffic.service.SysUserRoleService;
import com.zs.traffic.service.UserAreaService;
import com.zs.traffic.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	public UserDao userDao;
	
	
	@Autowired
	public SysUserRoleService sysUserRoleService;
	@Autowired
	public UserAreaService userAreaService;
	@Autowired
	public SysRoleAuthService sysRoleAuthService;
	
	@Override
	public List<User> getUserList(String cityCode) {
		return userDao.getUserList(cityCode);
	}

	@Override
	public List<User> getUserListByUserId(List<String> areaNumber,Integer type,Integer authDegree,String cityCode) {
		return userDao.getUserListByUserId(areaNumber,type,authDegree,cityCode);
	}
	
	@Override
	public int deleteByPrimaryKey(Integer userId) {
		return userDao.deleteByPrimaryKey(userId);
	}


	@Override
	public int insert(User record) {
		return userDao.insert(record);
	}


	@Override
	public int insertSelective(User record) {
		return userDao.insertSelective(record);
	}


	@Override
	public User selectByPrimaryKey(Integer userId) {
		return userDao.selectByPrimaryKey(userId);
	}


	@Override
	public int updateByPrimaryKeySelective(User record) {
		return userDao.updateByPrimaryKeySelective(record);
	}


	@Override
	public int updateByPrimaryKey(User record) {
		return userDao.updateByPrimaryKey(record);
	}


	@Override
	public User selectByUserName(String userName) {
		return userDao.selectByUserName(userName);
	}


	@Transactional(isolation = Isolation.READ_UNCOMMITTED)//加上事务
	public int insertUser(User model, String roleId, String area) {

		int ret = userDao.insertSelective(model);
		
		User user = userDao.selectByUserName(model.getUserName());
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRole.setSysUserId(user.getUserId());
		sysUserRole.setSysRoleId(Integer.parseInt(roleId));
		sysUserRole.setCreateDate(new Date());
		sysUserRole.setOperator(model.getOperator());
	    sysUserRoleService.insertSelective(sysUserRole);//用户分配角色
	    ret = ret +1;
	    
	    SysRoleAuth sysRoleAuth=sysRoleAuthService.getSysRoleAuthByRoleId(Integer.parseInt(roleId));
	    if(sysRoleAuth.getAuthDegree()!=ZsAuthDegree.INTCITY_AUTH){
	    	String[] areaId = area.split(",");
			System.out.println("=====================================");
	    	for(String id:areaId){
	    		UserArea record = new UserArea();
	    		record.setAreaId(Integer.parseInt(id));
	    		record.setUserId(user.getUserId());
	    		userAreaService.insertSelective(record); 
	    		ret++;
	    	}
			System.out.println("=========================================");
	    }
	
		if(ret>0){//
			return 1;

		}else{
			
			return 0;
		}
	}


	@Override
	public List<User> getUserInfoByuserId(Integer userId) {
		return userDao.getUserInfoByuserId(userId);
	}


	@Transactional(rollbackFor = Exception.class)
	public int updateSysUserByPrimaryKey(User model, String roleId, String area) {
		
		int ret = userDao.updateByPrimaryKeySelective(model);
		
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.setSysUserId(model.getUserId());
			sysUserRole.setSysRoleId(Integer.parseInt(roleId));
			sysUserRoleService.updateBySysUserId(sysUserRole);//用户分配角色
			
			 SysRoleAuth sysRoleAuth=sysRoleAuthService.getSysRoleAuthByRoleId(Integer.parseInt(roleId));
			 userAreaService.deleteBySysUserId(model.getUserId());//先删除区域关联
			 if(sysRoleAuth.getAuthDegree()!=ZsAuthDegree.INTCITY_AUTH){
				//更新区域
				String[] areaId = area.split(","); //再添加新的区域
				for(String id:areaId){
					UserArea record = new UserArea();
					record.setAreaId(Integer.parseInt(id));
					record.setUserId(model.getUserId());
					userAreaService.insertSelective(record); 
					ret++;
				}
		    }
		if(ret>0){
			return 1;
		}else{
			return 0;
		}
	}


	@Transactional(rollbackFor = Exception.class)
	public int deleteSysUserByUserId(Integer userId) {
		
		int ret =sysUserRoleService.deleteByUserId(userId);
		
		int retu =userAreaService.deleteBySysUserId(userId);//删除绑定的区域
		if(ret>0 && retu>=0){
			userDao.deleteByPrimaryKey(userId);
			return 1;
		}else{
			return 0;
		}
	}

	@Override
	public List<User> getUserInfosByuserId(Integer userId) {
		return userDao.getUserInfosByuserId(userId);
	}




}
