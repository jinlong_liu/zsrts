package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.AreaMapper;
import com.zs.traffic.model.Area;
import com.zs.traffic.service.ParametersChangeService;
/**
 * @description 社区完成了参数修改
 * 
 * @author antl
 * 
 * @date 2017.4.14
 *
 */
@Service
public class ParametersChangeServiceImpl implements ParametersChangeService {
	@Autowired
	public AreaMapper model;

	
	@Override
	public List<Area> getAreaList(String cityCode) {
		// TODO Auto-generated method stub
		return model.getAreaList(cityCode);
	}


	@Override
	public int updateParameter(Area record, Integer areaId) {
		// TODO Auto-generated method stub
		return model.updateParameter(record, areaId);
	}


	@Override
	public int updateTotal(Integer totalCount, Integer areaId) {
		// TODO Auto-generated method stub
		return model.updateTotal(totalCount, areaId);
	}


	@Override
	public int updateEveryday(Integer everydayCount, Integer areaId) {
		// TODO Auto-generated method stub
		return model.updateEveryday(everydayCount, areaId);
	}


	@Override
	public int updateEverydayCount(Integer everydayCount,
			String communityNumber,String cityCode) {
		// TODO Auto-generated method stub
		return model.updateEverydayCount(everydayCount, communityNumber,cityCode);
	}


	@Override
	public int updateTotalCount(Integer totalCount, String communityNumber,String cityCode) {
		// TODO Auto-generated method stub
		return model.updateTotalCount(totalCount, communityNumber,cityCode);
	}


	
}