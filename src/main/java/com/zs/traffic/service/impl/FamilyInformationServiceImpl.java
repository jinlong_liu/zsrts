package com.zs.traffic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.FamilyInformationMapper;
import com.zs.traffic.model.FamilyInformation;
import com.zs.traffic.service.FamilyInformationService;

/**
 * 
 * @author
 *
 */
@Service
public class FamilyInformationServiceImpl implements FamilyInformationService{
	
	@Autowired
	private FamilyInformationMapper model;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(FamilyInformation record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(FamilyInformation record) {
		return model.insertSelective(record);
	}

	@Override
	public FamilyInformation selectByPrimaryKey(Integer id) {
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(FamilyInformation record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(FamilyInformation record) {
		return model.updateByPrimaryKey(record);
	}

   	@Override
   public int setFa(String familyId, String chuxing) {
		// TODO Auto-generated method stub
		return model.setFa(familyId, chuxing);
	}
  
	

}
