package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zs.traffic.dao.RoleDao;
import com.zs.traffic.model.SysAuthorization;
import com.zs.traffic.model.SysRole;
import com.zs.traffic.model.SysRoleAuth;
import com.zs.traffic.service.RoleService;
@Service
public class RoleServiceImpl implements RoleService{
	
	
	@Autowired
	public RoleDao roleDao;
	
	@Override
	public List<SysRole> getRoleList() {
		return roleDao.getRoleList();
	}
	@Override
	public int deleteRole(int id) {
		return roleDao.deleteRole(id);
	}
	
	@Override
	public int selectRoleUser(int id) {
		return roleDao.selectRoleUser(id);
	}
	@Override
	public List<SysRole> getRoleListByid(int id) {
		return roleDao.getRoleListByid(id);
	}
	@Override
	public List<SysAuthorization> getAuthListByid(int id) {
		return roleDao.getAuthListByid(id);
	}
	@Override
	public int insert(SysRole sysRole) {
		return roleDao.insert(sysRole);
	}
	@Override
	public int updateRole(SysRole sysRole) {
		return roleDao.updateRole(sysRole);
	}
	@Override
	public int getIsRoleName(String roleName) {
		return roleDao.getIsRoleName(roleName);
	}
	@Override
	public List<SysRole> getRoleListByName(String roleName) {
		return roleDao.getRoleListByName(roleName);
	}
	@Override
	public int insertAuthRole(SysRoleAuth sysRoleAuth ) {
		return roleDao.insertAuthRole(sysRoleAuth);
	}
	@Override
	public int updateAuthRole(SysRoleAuth sysRoleAuth ) {
		return roleDao.updateAuthRole(sysRoleAuth);
	}
	@Override
	public int deleteRoleAuto(int id) {
		return roleDao.deleteRoleAuto(id);
	}
	@Override
	public List<SysRoleAuth> getSysRoleAuthByid(int sysRoleId) {
		return roleDao.getSysRoleAuthByid(sysRoleId);
	}
	@Override
	public List<SysRole> getSysRoleByAuthDegree(Integer authDegree){
		return roleDao.getSysRoleByAuthDegree(authDegree);
	}
	@Override
	public SysRole getSysRoleById(Integer roleId) {
		return roleDao.getSysRoleById(roleId);
	}
	@Override
	public SysAuthorization getAuthByDegree(Integer authDergee) {
		return roleDao.getAuthByDegree(authDergee);
	}
	
}
