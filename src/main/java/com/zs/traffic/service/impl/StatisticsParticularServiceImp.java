package com.zs.traffic.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.zs.traffic.controller.BaseController;
import com.zs.traffic.dao.AreaMapper;
import com.zs.traffic.dao.StatisticsParticularMapper;
import com.zs.traffic.model.Area;
import com.zs.traffic.model.StatisticsParticular;
import com.zs.traffic.service.StatisticsParticularService;
import com.zs.utils.Singleton;
import com.zs.utils.TxtUtil;

@Service
public class StatisticsParticularServiceImp extends BaseController implements
		StatisticsParticularService {
	@Autowired
	private StatisticsParticularMapper statisticsParticularMapper;
	@Autowired
	private AreaMapper areaMapper;

	//@Transactional(isolation = Isolation.SERIALIZABLE)
	// @Transactional(isolation=Isolation.READ_UNCOMMITTED)
	@Override
	public int insertSelective(StatisticsParticular statisticsParticular) {
		return statisticsParticularMapper.insertSelective(statisticsParticular);
	}

	//@Transactional(isolation = Isolation.SERIALIZABLE)
	@Override
	public int insertSelectiveForOne(String openId, String textAddress) {

		int num = 0;
		StatisticsParticular sp = statisticsParticularMapper.selectBySubmitterIs(openId);
		String city = sp.getCtiyCode();
		String communityNumber = sp.getTitleName();
		
		//单例模式为家庭编号+1
		Singleton singleton = Singleton.getInstance();
		Map<String, Object> map = null;
		if (singleton.submitCount.size()<1) {
			List<StatisticsParticular> spBySign = statisticsParticularMapper.selectAllNumForTitle();
			map = new HashMap<String, Object>();
			for (int i = 0; i < spBySign.size(); i++) {
				map.put(spBySign.get(i).getTitleName(), spBySign.get(i).getNumTitle());
			}
			
			singleton.setSubmitCount(map);
		}
		
		// 统计这个社区当天总提交数
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
		String submitDate = df.format(new Date());// new
													// Date()为获取当前系统时间
		int countNow = statisticsParticularMapper.selectCountByComm(communityNumber, submitDate);
		
		
		// 该社区的总提交数
		int totalCountNow = statisticsParticularMapper.selectTotalCountByComm(communityNumber);

		Area area = areaMapper.getSelectByConumber(communityNumber);
		// 该社区每天的提交数
		int totalEveryday = 0;
		// 查询该社区的总提交数
		int totalCount = 0;
		if (area != null) {
			totalCount = area.getTotalCount();
			totalEveryday = area.getEverydayCount();
		}

		if (countNow >= totalEveryday || totalCountNow >= totalCount) {

		} else {
			String[] Strings = textAddress.split(",");
			StatisticsParticular stst = new StatisticsParticular();
			stst.setAddress(Strings[0]);
			if(Strings.length<2){
				num=99;
				return num;
			}
			
				stst.setLon(Float.parseFloat(Strings[1]));
				stst.setLat(Float.parseFloat(Strings[2]));
			stst.setSubmitStage(1);
			stst.setTitleName(sp.getTitleName());
			stst.setSubmitter(sp.getSubmitter());
			stst.setCtiyCode(sp.getCtiyCode());
			// 99特殊标识 代表调查员帮忙填写的
			stst.setIsSurveyor("99");
			Formatter f = new Formatter(System.out);
			// 存家庭标号
			String family = String.format("%03d", singleton.getCount(communityNumber));
			String code = sp.getTitleName();
			stst.setFamilyId(code + family);
			// 存当前时间
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");// 设置日期格式
			String nowDate = sdf.format(new Date());// new
													// Date()为获取当前系统时间
			stst.setSubmitDate(nowDate);
			num = statisticsParticularMapper.insertSelective(stst);

			StringBuffer sb = new StringBuffer();
			sb.append("[{");
			sb.append("\"家庭地址\":");
			sb.append("\"" + textAddress + "\"");
			sb.append("}]");
			if (textAddress != null && !"".equals(textAddress)) {
				String filePathAndName = getProperties().getProperty("filePathDetail") +city+"/address/base_"
						+ (code + family).replace(":", "") + ".txt";// 路径,TXT文件命名不能包含":"
				TxtUtil.newFile(filePathAndName, sb.toString());// 将数据保存到服务器
			}

		}
		return num;
	}

	@Override
	public StatisticsParticular selectBySubmitter(String submitter) {
		return statisticsParticularMapper.selectBySubmitter(submitter);
	}

	@Transactional(isolation = Isolation.SERIALIZABLE)
	// @Override
	public int updateByPrimaryKeySelective(
			StatisticsParticular statisticsParticular) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper
				.updateByPrimaryKeySelective(statisticsParticular);
	}

	@Override
	public int selectFollowCount(Integer areaId) {
		return statisticsParticularMapper.selectFollowCount(areaId);
	}

	@Override
	public int selectubmitCount(Integer areaId) {
		return statisticsParticularMapper.selectubmitCount(areaId);
	}

	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(StatisticsParticular record) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.insert(record);
	}

	@Override
	public StatisticsParticular selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKey(StatisticsParticular record) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<StatisticsParticular> getStatisticsParticularList(
			String cityCode ,Integer limit,Integer offset) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getStatisticsParticularList(cityCode,limit,offset);
	}

	@Override
	public int selectCountByComm(String titleName, String submitDate) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectCountByComm(titleName,
				submitDate);
	}

	// 加上事务 查询出未提交的数据
	// @Transactional(isolation=Isolation.READ_UNCOMMITTED)
	// 序列化事务
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public int selectTotalCountByComm(String titleName) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectTotalCountByComm(titleName);
	}

	@Override
	public int selectNotCompleteCount(Integer areaId) {
		return statisticsParticularMapper.selectNotCompleteCount(areaId);
	}

	@Override
	public StatisticsParticular selectBySubmitterIs(String Submitter) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectBySubmitterIs(Submitter);
	}

	@Override
	public StatisticsParticular selectBySubmitterriginal(String Submitter) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectBySubmitterriginal(Submitter);
	}

	@Override
	public List<StatisticsParticular> getTitle() {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getTitle();
	}

	@Override
	public int intoAreaId(Integer areaId, String titleName) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.intoAreaId(areaId, titleName);
	}

	@Override
	public List<StatisticsParticular> getQuestionnaire(List<String> areaNumber,
			Integer type, String cityCode,Integer limit,Integer offset) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getQuestionnaire(areaNumber, type,
				cityCode,limit,offset);
	}

	@Override
	public List<StatisticsParticular> getTwoQuestionnaire(
			List<String> areaNumber, Integer type, String cityCode,
			Integer limit,Integer offset) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getTwoQuestionnaire(areaNumber, type,
				cityCode, limit,offset);
	}

	@Override
	public List<StatisticsParticular> getSanQuestionnaire(
			List<String> areaNumber, Integer type, String cityCode,Integer limit,Integer offset) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getSanQuestionnaire(areaNumber, type,
				cityCode, limit,offset);
	}

	@Override
	public List<StatisticsParticular> getConditionsDistrictName(
			String districtName) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper
				.getConditionsDistrictName(districtName);
	}

	@Override
	public List<StatisticsParticular> getConditions(String districtName,
			String subofficeName) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getConditions(districtName,
				subofficeName);
	}

	@Override
	public List<StatisticsParticular> getConditionsFamilyId(String familyId) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getConditionsFamilyId(familyId);
	}

	@Override
	public List<StatisticsParticular> getPublicQuestionnaire(String cityCode,Integer limit,Integer offset) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getPublicQuestionnaire(cityCode,limit, offset);
	}

	@Override
	public List<StatisticsParticular> getaudit(String familyId) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getaudit(familyId);
	}

	@Override
	public StatisticsParticular getProgress(String familyId) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getProgress(familyId);
	}

	@Override
	public List<StatisticsParticular> selectforyestoday() {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectforyestoday();
	}

	@Override
	public List<StatisticsParticular> selectAll() {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectAll();
	}

	@Override
	public List<StatisticsParticular> selectBySubmitterForList(String openId) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectBySubmitterForList(openId);
	}

	@Override
	public StatisticsParticular selectByFamily(String familyId) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectByFamily(familyId);
	}

	@Override
	public List<StatisticsParticular> selectByFamilyList(String familyId) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectByFamilyList(familyId);
	}

	@Override
	public int auditQuestionnaire(String familyId, Integer audit) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.auditQuestionnaire(familyId, audit);
	}

	@Override
	public int selecttotalAll(String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selecttotalAll(cityCode);
	}

	@Override
	public int selectResidentsQuestionnaire(String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectResidentsQuestionnaire(cityCode);
	}

	@Override
	public int selectReviewNot(String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectReviewNot(cityCode);
	}

	@Override
	public int selectReviewYes(String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectReviewYes(cityCode);
	}

	@Override
	public List<StatisticsParticular> selectTravelTotal(String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectTravelTotal(cityCode);
	}

	@Override
	public int selectStageTotal(String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectStageTotal(cityCode);
	}

	@Override
	public int selectConditions(String districtNumber, String subofficeNumber,
			String familyId, String communityNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectConditions(districtNumber,
				subofficeNumber, familyId, communityNumber, cityCode);
	}

	@Override
	public int selectConditionsResidents(String districtNumber,
			String subofficeNumber, String familyId, String communityNumber,
			String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectConditionsResidents(
				districtNumber, subofficeNumber, familyId, communityNumber,
				cityCode);
	}

	@Override
	public int selectCondditionNotaudit(String districtNumber,
			String subofficeNumber, String familyId, String communityNumber,
			String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectCondditionNotaudit(
				districtNumber, subofficeNumber, familyId, communityNumber,
				cityCode);
	}

	@Override
	public int selectCondditioYesaudit(String districtNumber,
			String subofficeNumber, String familyId, String communityNumber,
			String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectCondditionYesaudit(
				districtNumber, subofficeNumber, familyId, communityNumber,
				cityCode);
	}

	@Override
	public int selectCondditionStageTotal(String districtNumber,
			String subofficeNumber, String familyId, String communityNumber,
			String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectCondditionStageTotal(
				districtNumber, subofficeNumber, familyId, communityNumber,
				cityCode);
	}

	@Override
	public List<StatisticsParticular> selectTotal(String districtNumber,
			String subofficeNumber, String familyId, String communityNumber,
			String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectTotal(districtNumber,
				subofficeNumber, familyId, communityNumber, cityCode);
	}

	@Override
	public List<StatisticsParticular> selectContent(String districtNumber,
			String subofficeNumber, String familyId, String communityNumber,
			String cityCode ,Integer limit,Integer offset) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectContent(districtNumber,
				subofficeNumber, familyId, communityNumber, cityCode,limit,offset);
	}

	@Override
	public List<StatisticsParticular> selectDeviation(String districtNumber,
			String subofficeNumber, String familyId, String communityNumber,
			String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectDeviation(districtNumber,
				subofficeNumber, familyId, communityNumber, cityCode);
	}

	@Override
	public int updateByPrimaryKeySelectiveForPublic(String openId,String textAddress,String communityNumber) {
		String familyId = "";
		int num = 0;
		String city = "";
		int totalCountNow = statisticsParticularMapper
				.selectTotalCountByComm(communityNumber);
		StatisticsParticular statisticsParticular = statisticsParticularMapper
				.selectBySubmitter(openId);
		if (statisticsParticular != null) {
			String[] Strings = textAddress.split(",");
			if(Strings.length<2){
				num=99;
				return num;
			}
			statisticsParticular.setAddress(Strings[0]);
				statisticsParticular.setLon(Float.parseFloat(Strings[1]));
				statisticsParticular.setLat(Float.parseFloat(Strings[2]));
			statisticsParticular.setSubmitStage(1);
			Formatter f = new Formatter(System.out);
			
			//单例模式为家庭编号+1
			Singleton singleton = Singleton.getInstance();
			Map<String, Object> map = null;
			if (singleton.submitCount.size()<1) {
				List<StatisticsParticular> spBySign = statisticsParticularMapper.selectAllNumForTitle();
				map = new HashMap<String, Object>();
				for (int i = 0; i < spBySign.size(); i++) {
					map.put(spBySign.get(i).getTitleName(), spBySign.get(i).getNumTitle());
				}
				
				singleton.setSubmitCount(map);
				System.out.println("第一次进入单例");
			}
			
			// 存家庭标号
			String code = statisticsParticular.getTitleName();
			String family = String.format("%05d", singleton.getCount(code));
			city = statisticsParticular.getCtiyCode();
			
			statisticsParticular.setFamilyId(code + family);
			// 存当前时间
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");// 设置日期格式
			String nowDate = sdf.format(new Date());// new
													// Date()为获取当前系统时间
			statisticsParticular.setSubmitDate(nowDate);
			num = statisticsParticularMapper
					.updateByPrimaryKeySelective(statisticsParticular);
			familyId = code + family;
		}

		StringBuffer sb = new StringBuffer();
		sb.append("[{");
		sb.append("\"家庭地址\":");
		sb.append("\"" + textAddress + "\"");
		sb.append("}]");
		// 家庭
		if (textAddress != null && !"".equals(textAddress)) {
			String filePathAndName = getProperties().getProperty("filePathDetail")+city+ "/address/base_"
					+ familyId.replace(":", "") + ".txt";// 路径,TXT文件命名不能包含":"
			TxtUtil.newFile(filePathAndName, sb.toString());// 将数据保存到服务器
		}
		return num;
	}

	@Override
	public List<StatisticsParticular> selectAllNumForTitle() {
		// TODO Auto-generated method stub
		//Map<Object, Object> map = new HashMap<Object, Object>();  
		//List<StatisticsParticular> sp = statisticsParticularMapper.selectAllNumForTitle();
		return statisticsParticularMapper.selectAllNumForTitle();
	}

	@Override
	public List<StatisticsParticular> selectDis(String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectDis(cityCode);
	}

	@Override
	public List<StatisticsParticular> getStatistics(String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getStatistics(cityCode);
	}

	@Override
	public int getQuestionnaireTotal(List<String> areaNumber,String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getQuestionnaireTotal(areaNumber, cityCode);
	}

	@Override
	public int getTwoQuestionnaireTotal(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getTwoQuestionnaireTotal(areaNumber, cityCode);
	}

	@Override
	public int getSanQuestionnaireTotal(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getSanQuestionnaireTotal(areaNumber, cityCode);
	}

	@Override
	public int selectContentTotal(String districtNumber, String subofficeNumber, String familyId,
			String communityNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectContentTotal(districtNumber, subofficeNumber, familyId, communityNumber, cityCode);
	}

	@Override
	public List<StatisticsParticular> selectPublic(String cityCode, String district, Integer limit, Integer offset) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectPublic(cityCode, district,limit,offset);
	}

	@Override
	public int getPublicQuestionnaireTotal(String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getPublicQuestionnaireTotal(cityCode);
	}

	@Override
	public int selectPublicTotal(String cityCode, String district) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectPublicTotal(cityCode, district);
	}

	@Override
	public int selectThreeTotal(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectThreeTotal(areaNumber, cityCode);
	}

	@Override
	public int selectOneTotal(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectOneTotal(areaNumber, cityCode);
	}

	@Override
	public int selectTwoTotal(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectTwoTotal(areaNumber, cityCode);
	}

	@Override
	public int selectResidentsOne(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectResidentsOne(areaNumber, cityCode);
	}

	@Override
	public int selectResidentsTwo(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectResidentsTwo(areaNumber, cityCode);
	}

	@Override
	public int selectResidentsThree(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectResidentsThree(areaNumber, cityCode);
	}

	@Override
	public int selectReviewNotOne(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectReviewNotOne(areaNumber, cityCode);
	}

	@Override
	public int selectReviewNotTwo(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectReviewNotTwo(areaNumber, cityCode);
	}

	@Override
	public int selectReviewNotThree(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectReviewNotThree(areaNumber, cityCode);
	}

	@Override
	public int selectReviewYesOne(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectReviewYesOne(areaNumber, cityCode);
	}

	@Override
	public int selectReviewYesTwo(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectReviewYesTwo(areaNumber, cityCode);
	}

	@Override
	public int selectReviewYesThree(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectReviewYesThree(areaNumber, cityCode);
	}

	@Override
	public int selectStageTotalOne(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectStageTotalOne(areaNumber, cityCode);
	}

	@Override
	public int selectStageTotalTwo(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectStageTotalTwo(areaNumber, cityCode);
	}

	@Override
	public int selectStageTotalThree(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectStageTotalThree(areaNumber, cityCode);
	}

	@Override
	public List<StatisticsParticular> getStatisticsOne(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getStatisticsOne(areaNumber, cityCode);
	}

	@Override
	public List<StatisticsParticular> getStatisticsTwo(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getStatisticsTwo(areaNumber, cityCode);
	}

	@Override
	public List<StatisticsParticular> getStatisticsThree(List<String> areaNumber, String cityCode) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getStatisticsThree(areaNumber, cityCode);
	}

	@Override
	public StatisticsParticular getFamilyId(int id) {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.getFamilyId(id);
	}

	/*@Override
	public int selectPublicTotal() {
		// TODO Auto-generated method stub
		return statisticsParticularMapper.selectPublicTotal();
	}*/

	}
