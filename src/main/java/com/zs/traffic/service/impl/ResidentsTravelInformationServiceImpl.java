package com.zs.traffic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.zs.traffic.dao.ResidentsTravelInformationMapper;
import com.zs.traffic.model.ResidentsTravelInformation;
import com.zs.traffic.service.ResidentsTravelInformationService;

/**
 * 
 * @author
 *
 */
@Service
public class ResidentsTravelInformationServiceImpl implements ResidentsTravelInformationService{
	
	@Autowired
	private ResidentsTravelInformationMapper model;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ResidentsTravelInformation record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(ResidentsTravelInformation record) {
		return model.insertSelective(record);
	}

	@Override
	public ResidentsTravelInformation selectByPrimaryKey(Integer id) {
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ResidentsTravelInformation record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ResidentsTravelInformation record) {
		return model.updateByPrimaryKey(record);
	}

	

}
