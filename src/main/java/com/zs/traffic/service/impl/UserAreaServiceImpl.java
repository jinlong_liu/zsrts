package com.zs.traffic.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.UserAreaMapper;
import com.zs.traffic.dao.UserDao;
import com.zs.traffic.model.User;
import com.zs.traffic.model.UserArea;
import com.zs.traffic.service.UserAreaService;
import com.zs.traffic.service.UserService;

@Service
public class UserAreaServiceImpl implements UserAreaService {
	@Autowired
	public UserAreaMapper UserAreaDao;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return UserAreaDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(UserArea record) {
		return UserAreaDao.insert(record);
	}

	@Override
	public int insertSelective(UserArea record) {
		return UserAreaDao.insertSelective(record);
	}

	@Override
	public UserArea selectByPrimaryKey(Integer id) {
		return UserAreaDao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(UserArea record) {
		return UserAreaDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(UserArea record) {
		return UserAreaDao.updateByPrimaryKey(record);
	}

	@Override
	public int deleteBySysUserId(Integer userId) {
		return UserAreaDao.deleteBySysUserId(userId);
	}

	@Override
	public List<UserArea> getUser(Integer areaId) {
		// TODO Auto-generated method stub
		return UserAreaDao.getUser(areaId);
	}
	
	

}
