package com.zs.traffic.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.zs.traffic.dao.TravelInformationMapper;
import com.zs.traffic.model.TravelInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.AppFamilyInformationMapper;
import com.zs.traffic.model.AppFamilyInformation;
import com.zs.traffic.service.AppFamilyInformationService;

/**
 * 
 * @author
 *
 */
@Service
public class AppFamilyInformationServiceImpl implements AppFamilyInformationService{

	@Autowired
	private AppFamilyInformationMapper model;
	@Autowired
	private TravelInformationMapper travelInformationMapper;

	@Override
	public int deleteByPrimaryKey(Integer familyId) {
		return model.deleteByPrimaryKey(familyId);
	}

	@Override
	public int insert(AppFamilyInformation record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(AppFamilyInformation record) {
		return model.insertSelective(record);
	}

	@Override
	public AppFamilyInformation selectByPrimaryKey(Integer familyId) {
		return model.selectByPrimaryKey(familyId);
	}

	@Override
	public int updateByPrimaryKeySelective(AppFamilyInformation record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(AppFamilyInformation record) {
		return model.updateByPrimaryKey(record);
	}

	@Override
	public List<AppFamilyInformation> selectByAppuserId(Integer appuserId) {
		return model.selectByAppuserId(appuserId);
	}

	@Override
	public int deleteByAppUserId(Integer appuserId) {
		return model.deleteByAppUserId(appuserId);
	}

	@Override
	public AppFamilyInformation getListPersonal(Integer appuserId) {
		// TODO Auto-generated method stub
		return model.getListPersonal(appuserId);
	}

	@Override
	public List<AppFamilyInformation> getListAppFamilyInformation(String cityCode) {
		// TODO Auto-generated method stub
		return model.getListAppFamilyInformation(cityCode);
	}

	@Override
	public List<AppFamilyInformation> selectAppFamilyInformationForAge(
			int starAge, int endAge) {
		// TODO Auto-generated method stub
		return model.selectAppFamilyInformationForAge(starAge, endAge);
	}

	@Override
	public List<AppFamilyInformation> selectAppFamilyInformationForWork(int work) {
		// TODO Auto-generated method stub
		return model.selectAppFamilyInformationForWork(work);
	}

	@Override
	public List<AppFamilyInformation> selectAppFamilyInformationForSex(int sex) {
		// TODO Auto-generated method stub
		return model.selectAppFamilyInformationForSex(sex);
	}

	@Override
	public List<AppFamilyInformation> selectAppFamilyInformationForIsLocal(
			int local) {
		// TODO Auto-generated method stub
		return model.selectAppFamilyInformationForIsLocal(local);
	}

	@Override
	public List<AppFamilyInformation> selectAppFamilyInformationForUserName(
			String userName) {
		// TODO Auto-generated method stub
		return model.selectAppFamilyInformationForUserName(userName);
	}

	@Override
	public List<AppFamilyInformation> selectAppuser(int starAge, int endAge,
			int work, int sex, int local, String userName,String cityCode) {
		// TODO Auto-generated method stub
		return model.selectAppuser(starAge, endAge, work, sex, local, userName,cityCode);
	}

	@Override
	public int selectAppUserTotal() {
		// TODO Auto-generated method stub
		return model.selectAppUserTotal();
	}

	@Override
	public int selectAppUserNoaudit() {
		// TODO Auto-generated method stub
		return model.selectAppUserNoaudit();
	}

	@Override
	public int selectAppUserYesaudit() {
		// TODO Auto-generated method stub
		return model.selectAppUserYesaudit();
	}

	@Override
	public int selectAppUserTwentyNoaudit() {
		// TODO Auto-generated method stub
		return model.selectAppUserTwentyNoaudit();
	}

	@Override
	public int selectAppUserTwentyYesaudit() {
		// TODO Auto-generated method stub
		return model.selectAppUserTwentyYesaudit();
	}

	@Override
	public int selectAppUserThirtyNoaudit() {
		// TODO Auto-generated method stub
		return model.selectAppUserThirtyNoaudit();
	}

	@Override
	public int selectAppUserThirtyYesaudit() {
		// TODO Auto-generated method stub
		return model.selectAppUserThirtyYesaudit();
	}

	@Override
	public int selectAppUserFiftyNoaudit() {
		// TODO Auto-generated method stub
		return model.selectAppUserFiftyNoaudit();
	}

	@Override
	public int selectAppUserFiftyYesaudit() {
		// TODO Auto-generated method stub
		return model.selectAppUserFiftyYesaudit();
	}

	@Override
	public List<String> getTravelTIme(int userId) {
		try{
			List<TravelInformation> travelTime = travelInformationMapper.getTravelTime(userId);

			Date startDate = travelTime.get(0).getStartDate();


			Date endDate = travelTime.get(travelTime.size() - 1).getEndDate();
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String startTime = sf.format(startDate);
			String endTime = sf.format(endDate);
			System.out.println("========================================================");
			for (TravelInformation travelInformation : travelTime) {
				System.out.println(travelInformation);
			}
			System.out.println("========================================================");
			List<String> travelDate = new ArrayList<>();
			travelDate.add(startTime);
			travelDate.add(endTime);
			return travelDate;
		} catch (Exception e){
			System.out.println("没有出行轨迹");
		}
		return null;
	}
}
