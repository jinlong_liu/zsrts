package com.zs.traffic.service.impl;

import java.util.List;
import java.util.Map;

import com.zs.traffic.dao.AppBusInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;





import com.zs.traffic.dao.SysAppuserMapper;
import com.zs.traffic.model.AppFamilyInformation;
import com.zs.traffic.model.SysAppuser;
import com.zs.traffic.service.AppVolunteerInformationService;
/**
 * 
 * @author
 *
 */
@Service
public class AppVolunteerInformationImpl implements AppVolunteerInformationService {
	@Autowired
	public SysAppuserMapper model;

	
	
	@Override
	public List<SysAppuser> getListVolunteers() {
		// TODO Auto-generated method stub
		return model.getListVolunteers();
	}

	@Override
	public List<SysAppuser> getListHomeAddress(Integer appuserId) {
		// TODO Auto-generated method stub
		return model.getListHomeAddress(appuserId);
	}

	/**
	 * 删除用户表与第三方表
	 * @param userId
	 * @return
	 */
	@Override
	public int deleteByPrimaryKey(Integer userId) {
		this.model.deleteUserThirdParty(userId);
		return model.deleteByPrimaryKey(userId);
	}

	@Override
	public int deleteUserAllInformation(String start_date,String end_date) {
		System.out.println("herererererererer!!"+model.deleteUserAllInformation(start_date,end_date));
		return model.deleteUserAllInformation(start_date,end_date);
	}

	@Override
	public List<Integer> findDeleteUserAllInformation(String start_date, String end_date) {
		System.out.println("herererererererer!!"+model.findDeleteUserAllInformation(start_date,end_date));
		return model.findDeleteUserAllInformation(start_date,end_date);
	}

	@Override
	public int deleteUserAllCharge(Integer appuser_id) {
		return model.deleteUserAllCharge(appuser_id);
	}

	@Override
	public int insert(SysAppuser appuser) {
		return model.insert(appuser);
	}

	/**
	 * 注册方法
	 * @param username 姓名
	 * @param family_address 家庭地址
	 * @param mac_address mac地址
	 * @param electric_bikes 电车
	 * @param car_count 汽车
	 * @param identity 用户身份 是否是户主
	 * @param sex 性别
	 * @param age 年龄
	 * @param salary 薪水
	 * @param occupation 职业
	 * @param isPermanentAddress 是否久住
	 * @param telphone 电话
	 * @param openid 微信识别号
	 * @return
	 */
	@Override
	public int register(String username ,String family_address, String mac_address, String electric_bikes, String car_count, String identity, String sex, String age, String salary, String occupation, String isPermanentAddress, String telphone,String openid) {
		return 1;
	}

	/**
	 * 查询审核结果成功返回1  未审核+查不到数据 0  审核不通过返回-1
	 * @param openid 微信id
	 * @return
	 */
	@Override
	public int getCheckResult(String openid) {
		return 1;
	}

	/**
	 * 查询轨迹信息list里装的是轨迹对象，然后字段拆分成为了map里的k-v
	 * @param openid
	 * @return
	 */
	@Override
	public List<Map<String, Object>> getTrajectory(String openid) {
		return null;
	}

	/**
	 * 更新轨迹表
	 * @param id
	 * @param startDate
	 * @param endDate
	 * @param startAddress
	 * @param endAddress
	 * @param tripMode
	 * @param tripObjective
	 * @return
	 */
	@Override
	public int updateTrajectory(String id, String startDate, String endDate, String startAddress, String endAddress, String tripMode, String tripObjective) {
		return 1;
	}

	@Override
	public int changeVolunteerInformation(String id, String username, String age, String sex, String isLocal, String address, String car, String bike, String number,String income,String profession,String macAddress) {
		Integer integer = model.macAddressIsExist(id,macAddress);
		if(integer>0){
			return 3;
		}
		model.changeMacAddress(id,macAddress);
		model.changeAppUserInformation(id,sex,age,isLocal,income,profession);

		return model.changeAppUser(id, username, bike, car, number, address);
	}

	@Override
	public String getMacAddress(String id) {
		return model.getMacAddress(id);
	}

	@Override
	public int Unbind(String startDate, String endDate) {
		List<Integer> allAppUserByDate = model.getAllAppUserByDate(startDate, endDate);
		for (Integer integer : allAppUserByDate) {
			model.unbindByAppUserId(integer);
		}
		return 0;
	}

	/**
	 * 根据appuserId删除第三方表中的数据
	 * @param appuserId
	 * @return
	 */
	@Override
	public int deleteUserThirdParty(Integer appuserId) {
		return model.deleteUserThirdParty(appuserId);
	}

	@Override
	public int unbindByUserId(Integer appuser_id) {
		return model.unbindByAppUserId(appuser_id);
	}
}
