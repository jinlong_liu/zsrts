package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.AuthDao;
import com.zs.traffic.model.Auth;
import com.zs.traffic.service.AuthService;

/**
 * @description 获取可查询的数据的日期列表  
 * 
 * @author renxw
 * 
 * @date 2016.6.18
 *
 */
@Service
public class AuthServiceImpl implements AuthService {
	
	@Autowired
	public AuthDao authDao;

	public List<Auth> getAuthList() {
		return authDao.getAuthList();
	}

}
