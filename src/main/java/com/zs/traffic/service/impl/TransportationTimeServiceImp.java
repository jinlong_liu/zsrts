package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.TravelInformationMapper;
import com.zs.traffic.dao.TransportationTimeMapper;
import com.zs.traffic.model.TransportationTime;
import com.zs.traffic.service.TransportationTimeService;

/**
 * 
 * @author
 *
 */
@Service
public class TransportationTimeServiceImp implements TransportationTimeService{
	@Autowired
	public TransportationTimeMapper model;

	@Override
	public List<TransportationTime> selectAll() {
		// TODO Auto-generated method stub
		return model.selectAll();
	}

	@Override
	public List<TransportationTime> selectOne(String kilometre) {
		// TODO Auto-generated method stub
		return model.selectOne(kilometre);
	}
	
	
}
