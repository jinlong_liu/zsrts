package com.zs.traffic.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.TrajectoryDateMapper;
import com.zs.traffic.dao.TravelInformationMapper;
import com.zs.traffic.model.TrajectoryDate;
import com.zs.traffic.model.TravelInformation;
import com.zs.traffic.service.TrajectoryDateService;
import com.zs.traffic.service.TravelInformationService;

/**
 * 
 * @author
 *
 */
@Service
public class TrajectoryDateServiceImpl implements TrajectoryDateService{

	@Autowired
	public TrajectoryDateMapper model;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(TrajectoryDate record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(TrajectoryDate record) {
		return model.insertSelective(record);
	}

	@Override
	public TrajectoryDate selectByPrimaryKey(Integer id) {
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(TrajectoryDate record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(TrajectoryDate record) {
		return model.updateByPrimaryKey(record);
	}

	@Override
	public int insertSelectives(TrajectoryDate record, String tableName) {
		return model.insertSelectives(record,tableName);
	}
	
	@Override
	public List<TrajectoryDate> selectByAppuserAndStartDate(TrajectoryDate record, String tableName) {
		return model.selectByAppuserAndStartDate(record,tableName);
	}

	@Override
	public List<TrajectoryDate> gettrajectory(Integer appuserId,
			Date startDate, Date endDate, String tableName) {
		// TODO Auto-generated method stub
		return model.gettrajectory(appuserId, startDate, endDate, tableName);
	}


	@Override
	public List<TrajectoryDate> selectByTableName(String tableName,Integer appuserId,Date startDate) {
		// TODO Auto-generated method stub
		return model.selectByTableName(tableName,appuserId,startDate);
	}

	@Override
	public List selectAppuserIdByTableName(String tableName) {
		// TODO Auto-generated method stub
		return model.selectAppuserIdByTableName(tableName);
	}


	@Override
	public List<TrajectoryDate> getTrajectoryMorning(Integer appuserId,
			Date startDate, Date endDate, String tableName) {
		// TODO Auto-generated method stub
		return model.getTrajectoryMorning(appuserId, startDate, endDate, tableName);
	}

	@Override
	public List<TrajectoryDate> getTrajectoryAfternoon(Integer appuserId,
			Date startDate, Date endDate, String tableName) {
		// TODO Auto-generated method stub
		return model.getTrajectoryAfternoon(appuserId, startDate, endDate, tableName);
	}


	

	
	
}
