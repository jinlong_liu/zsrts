package com.zs.traffic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.InhabitanMapper;
import com.zs.traffic.model.Inhabitan;
import com.zs.traffic.service.InhabitanService;

/**
 * 
 * @author
 *
 */
@Service
public class InhabitanServiceImpl implements InhabitanService{
	
	@Autowired
	private InhabitanMapper model;

	@Override
	public int deleteByPrimaryKey(Integer inhabitantId) {
		return model.deleteByPrimaryKey(inhabitantId);
	}

	@Override
	public int insert(Inhabitan record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(Inhabitan record) {
		return model.insertSelective(record);
	}

	@Override
	public Inhabitan selectByPrimaryKey(Integer inhabitantId) {
		return model.selectByPrimaryKey(inhabitantId);
	}

	@Override
	public int updateByPrimaryKeySelective(Inhabitan record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Inhabitan record) {
		return model.updateByPrimaryKey(record);
	}

	

}
