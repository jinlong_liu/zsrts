package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.ChargeMapper;
import com.zs.traffic.dao.RegisterNumberMapper;
import com.zs.traffic.model.RegisterNumber;
import com.zs.traffic.model.Charge;
import com.zs.traffic.service.ChargeService;
import com.zs.traffic.service.RegisterNumberService;

@Service
public class RegisterNumberServiceImp implements RegisterNumberService{

	@Autowired
	private RegisterNumberMapper model;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(RegisterNumber record) {
		// TODO Auto-generated method stub
		return model.insert(record);
	}

	@Override
	public int insertSelective(RegisterNumber record) {
		// TODO Auto-generated method stub
		return model.insertSelective(record);
	}

	@Override
	public RegisterNumber selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(RegisterNumber record) {
		// TODO Auto-generated method stub
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(RegisterNumber record) {
		// TODO Auto-generated method stub
		return model.updateByPrimaryKey(record);
	}

	@Override
	public RegisterNumber selectByRechargeType(Integer rechargeType) {
		return model.selectByRechargeType(rechargeType);
	}

	
}

