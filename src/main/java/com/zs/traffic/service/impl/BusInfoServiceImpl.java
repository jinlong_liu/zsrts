package com.zs.traffic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.BusInfoMapper;
import com.zs.traffic.model.BusInfo;
import com.zs.traffic.service.BusInfoService;

/**
 * 
 * @author
 *
 */
@Service
public class BusInfoServiceImpl implements BusInfoService{
	
	@Autowired
	private BusInfoMapper model;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(BusInfo record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(int iid) {
		return model.insertSelective(iid);
	}

	@Override
	public BusInfo selectByPrimaryKey(Integer id) {
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(BusInfo record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(BusInfo record) {
		return model.updateByPrimaryKey(record);
	}

}
