package com.zs.traffic.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.SysAppuserMapper;
import com.zs.traffic.model.SysAppuser;
import com.zs.traffic.service.SysAppuserService;

/**
 * 
 * @author
 *
 */
@Service
public class SysAppuserServiceImpl implements SysAppuserService{
	
	@Autowired
	private SysAppuserMapper model;

	@Override
	public int deleteByPrimaryKey(Integer appuserId) {
		return model.deleteByPrimaryKey(appuserId);
	}

	@Override
	public int insert(SysAppuser record) {
		return model.insert(record);
	}

	@Override
	public int insertSelective(SysAppuser record) {
		return model.insertSelective(record);
	}

	@Override
	public SysAppuser selectByPrimaryKey(Integer appuserId) {
		return model.selectByPrimaryKey(appuserId);
	}

	@Override
	public int updateByPrimaryKeySelective(SysAppuser record) {
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysAppuser record) {
		return model.updateByPrimaryKey(record);
	}

	@Override
	public SysAppuser selectByUserName(String userName) {
		return model.selectByUserName(userName);
	}

	@Override
	public int selectQuantity(String todayDate, String tomorrowDate) {
		return model.selectQuantity(todayDate,tomorrowDate);
	}

	@Override
	public List<SysAppuser> selectByOccupation(String ip) {
		return model.selectByOccupation(ip);
	}

	
}
