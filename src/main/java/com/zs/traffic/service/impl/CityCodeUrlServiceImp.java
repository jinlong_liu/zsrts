package com.zs.traffic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.ChargeMapper;
import com.zs.traffic.dao.CityCodeUrlMapper;
import com.zs.traffic.model.CityCodeUrl;
import com.zs.traffic.service.CityCodeUrlService;


@Service
public class CityCodeUrlServiceImp implements CityCodeUrlService{

	@Autowired
	private  CityCodeUrlMapper model;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return model.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(CityCodeUrl record) {
		// TODO Auto-generated method stub
		return model.insert(record);
	}

	@Override
	public int insertSelective(CityCodeUrl record) {
		// TODO Auto-generated method stub
		return model.insertSelective(record);
	}

	@Override
	public CityCodeUrl selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return model.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(CityCodeUrl record) {
		// TODO Auto-generated method stub
		return model.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(CityCodeUrl record) {
		// TODO Auto-generated method stub
		return model.updateByPrimaryKey(record);
	}

	@Override
	public CityCodeUrl selectByCityCode(String cityCode, String extendedFieldOne) {
		// TODO Auto-generated method stub
		return model.selectByCityCode(cityCode, extendedFieldOne);
	}

	@Override
	public List<CityCodeUrl> getSignature(String cityCode) {
		// TODO Auto-generated method stub
		return model.getSignature(cityCode);
	}


}
