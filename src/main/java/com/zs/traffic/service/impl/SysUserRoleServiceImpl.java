package com.zs.traffic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.AppBusInfoMapper;
import com.zs.traffic.dao.SysRoleAuthMapper;
import com.zs.traffic.dao.SysUserRoleMapper;
import com.zs.traffic.model.AppBusInfo;
import com.zs.traffic.model.SysRoleAuth;
import com.zs.traffic.model.SysUserRole;
import com.zs.traffic.service.AppBusInfoService;
import com.zs.traffic.service.SysRoleAuthService;
import com.zs.traffic.service.SysUserRoleService;

/**
 * 
 * @author
 *
 */
@Service
public class SysUserRoleServiceImpl implements SysUserRoleService{

	@Autowired
	public SysUserRoleMapper sysUserRoleMapper;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return sysUserRoleMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysUserRole record) {
		return sysUserRoleMapper.insert(record);
	}

	@Override
	public int insertSelective(SysUserRole record) {
		return sysUserRoleMapper.insertSelective(record);
	}

	@Override
	public SysUserRole selectByPrimaryKey(Integer id) {
		return sysUserRoleMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysUserRole record) {
		return sysUserRoleMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysUserRole record) {
		return sysUserRoleMapper.updateByPrimaryKey(record);
	}

	@Override
	public int updateBySysUserId(SysUserRole sysUserRole) {
		return sysUserRoleMapper.updateBySysUserId(sysUserRole);
	}

	@Override
	public int deleteByUserId(Integer userId) {
		return sysUserRoleMapper.deleteByUserId(userId);
	}
	


	
}
