package com.zs.traffic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.traffic.dao.WechatAppMapper;
import com.zs.traffic.model.WechatApp;
import com.zs.traffic.service.WechatAppService;
@Service
public class WechatAppServiceImp implements WechatAppService{
	@Autowired
	private WechatAppMapper wechatAppInfoMapper;

	@Override
	public int insertSelective(WechatApp wechatApp) {
		return wechatAppInfoMapper.insertSelective(wechatApp);
	}

}
