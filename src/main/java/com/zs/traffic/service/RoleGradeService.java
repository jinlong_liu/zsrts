package com.zs.traffic.service;


import java.util.List;

import com.zs.traffic.model.RoleGrade;
import org.springframework.stereotype.Service;

@Service
public interface RoleGradeService  {

    public List<RoleGrade> getGradeRoleList();
    public int deleteGradeRole(int id);
    public int selectGradeRoleUser(int id);

}

