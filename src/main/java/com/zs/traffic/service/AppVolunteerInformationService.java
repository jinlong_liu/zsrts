package com.zs.traffic.service;

import java.util.List;
import java.util.Map;


import com.zs.traffic.model.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * @description 获取志愿者信息
 * 
 * @author antl
 * 
 * @date 2017.3.29
 *
 */
@Service
public interface AppVolunteerInformationService {
	
	public List<SysAppuser> getListVolunteers();
	
    public List<SysAppuser> getListHomeAddress(Integer appuserId);

    public int deleteByPrimaryKey(Integer userId);

    public int deleteUserAllInformation(String start_date,String end_date);

    public int insert(SysAppuser appuser);
    //注册，成功返回1 否则返回0
    public int register(String username,String family_address, String mac_address, String electric_bikes, String car_count, String identity, String sex, String age, String salary, String occupation, String isPermanentAddress, String telphone,String openId);

    public int getCheckResult(String openid);

    public List<Map<String, Object>> getTrajectory(String openid);

    public int updateTrajectory(String id, String startDate, String endDate, String startAddress, String endAddress, String tripMode, String tripObjective);

    /**
     * 修改调查用户信息
     * @param userId
     * @param username
     * @param age
     * @param sex
     * @param isLocal
     * @param address
     * @param car
     * @param bike
     * @param number
     * @param income
     * @param profession
     * @param macAddress
     * @return 0表示修改失败 1表示修改成功 3表示MAC地址已经被使用
     */
    public int changeVolunteerInformation(String userId,String username,String age,String sex,String isLocal,String address,String car,String bike,String number,String income,String profession,String macAddress);

    public String getMacAddress(String id);

    public List<Integer> findDeleteUserAllInformation(String start_date, String end_date);

    public int deleteUserAllCharge(Integer appuser_id);

    public int Unbind(String startDate,String endDate);

    /**
     * 根据appuserId删除第三方表中的数据
     * @param appuserId
     * @return
     */
    public int deleteUserThirdParty(Integer appuserId);

    public int unbindByUserId(Integer appuser_id);
}

