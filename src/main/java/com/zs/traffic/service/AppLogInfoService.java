package com.zs.traffic.service;

import org.springframework.stereotype.Service;
import com.zs.traffic.model.AppLogInfo;
/**
 * 
 * @author
 *
 */
@Service
public interface AppLogInfoService {
	
	public int deleteByPrimaryKey(Integer id);

	public int insert(AppLogInfo record);

	public int insertSelective(AppLogInfo record);

	public AppLogInfo selectByPrimaryKey(Integer id);

	public int updateByPrimaryKeySelective(AppLogInfo record);

	public int updateByPrimaryKey(AppLogInfo record);
	
}
