package com.zs.traffic.service;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.BusInfo;

@Service
public interface BusInfoService {
	
	 public int deleteByPrimaryKey(Integer id);

	 public int insert(BusInfo record);

	 public int insertSelective(int iid);

	 public BusInfo selectByPrimaryKey(Integer id);

	 public int updateByPrimaryKeySelective(BusInfo record);

	 public  int updateByPrimaryKey(BusInfo record);

}
