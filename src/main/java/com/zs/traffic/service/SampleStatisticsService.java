package com.zs.traffic.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.TravelInformation;
import com.zs.traffic.model.User;


@Service
public interface SampleStatisticsService{
	public List<TravelInformation> selectSeMan(TravelInformation travelInformation); 
	
}
