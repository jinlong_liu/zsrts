package com.zs.traffic.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.SysAuthorization;
import com.zs.traffic.model.SysRole;
import com.zs.traffic.model.SysRoleAuth;

@Service
public interface RoleService {
	public List<SysRole> getRoleList();
	
	public int deleteRole(int id);
	
	public int deleteRoleAuto(int id);
	
	public int selectRoleUser(int id);
	
	public List<SysRole> getRoleListByid(int id);
	
	public List<SysAuthorization> getAuthListByid(int id);
	
	public List<SysRoleAuth> getSysRoleAuthByid(int sysRoleId);
	
	public int insert(SysRole sysRole);
	
	public int updateRole(SysRole sysRole);
	
	public int getIsRoleName(String roleName);
	
	public List<SysRole> getRoleListByName(String roleName);
	
	public int insertAuthRole(SysRoleAuth sysRoleAuth );
	
	public int updateAuthRole(SysRoleAuth sysRoleAuth );
	
	public List<SysRole> getSysRoleByAuthDegree(Integer authDegree);
	
	public  SysRole getSysRoleById(Integer roleId);
	
	public SysAuthorization getAuthByDegree(Integer authDergee);
}

