package com.zs.traffic.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.User;


@Service
public interface UserService {
	public List<User> getUserList(String cityCode);

	public List<User> getUserListByUserId(List<String> areaNumber,Integer type,Integer authDegree,String cityCode);//当前登录用户下的所有区域
	
	public int deleteByPrimaryKey(Integer userId);

	public int insert(User record);

	public int insertSelective(User record);

	public User selectByPrimaryKey(Integer userId);

	public int updateByPrimaryKeySelective(User record);

	public int updateByPrimaryKey(User record);
	
	public User selectByUserName(String userName);
	
	public int insertUser(User model,String roleId,String  area);
	
	public List<User> getUserInfoByuserId(Integer userId);//不带权限等级
	
	public List<User> getUserInfosByuserId(Integer userId);//带权限等级
	
	public int updateSysUserByPrimaryKey(User model,String roleId,String  area);//通过主键id修改
	
	public int deleteSysUserByUserId(Integer userId);
	
}
