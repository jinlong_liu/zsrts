package com.zs.traffic.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import com.zs.traffic.model.Area;

/**
 * 
 * @author
 *
 */
@Service
public interface ParametersChangeService {
	
	public List<Area> getAreaList(String cityCode);
    
	public int updateParameter(Area record, Integer areaId);
	
	public int updateTotal(Integer totalCount,Integer areaId);
	
	public int updateEveryday(Integer everydayCount,Integer areaId);
	
	public int updateEverydayCount(Integer everydayCount,String communityNumber,String cityCode);
	
	public int updateTotalCount(Integer totalCount,String communityNumber,String cityCode);
}
