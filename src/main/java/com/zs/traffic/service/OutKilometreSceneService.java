package com.zs.traffic.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zs.traffic.model.OutKilometreScene;

public interface OutKilometreSceneService {
    List<OutKilometreScene> selectByTimeWord(@Param(value="timeTotal")String timeTotal,@Param(value="timeGo")String timeGo,@Param(value="timeWait")String timeWait,@Param(value="timeRiding")String timeRiding,@Param(value="priceTicket")String priceTicket,@Param(value="costTotal")String costTotal,@Param(value="costWorking")String costWorking,@Param(value="costPark")String costPark,@Param(value="transportation")String transportation);


	List<OutKilometreScene> selectByKmSceneTransportation(@Param(value="kilometre")String kilometre,@Param(value="scene")String scene);

	
	 List<OutKilometreScene> selectByTimeWord1(@Param(value="timeTotal")String timeTotal,@Param(value="timeGo")String timeGo,@Param(value="timeWait")String timeWait,@Param(value="timeRiding")String timeRiding,@Param(value="priceTicket")String priceTicket,@Param(value="transportation")String transportation);
	    
	    List<OutKilometreScene> selectByTimeWord2(@Param(value="timeTotal")String timeTotal,@Param(value="costTotal")String costTotal,@Param(value="costWorking")String costWorking,@Param(value="costPark")String costPark,@Param(value="transportation")String transportation);

	    List<OutKilometreScene> selectByTimeWord3(@Param(value="timeTotal")String timeTotal,@Param(value="priceTicket")String priceTicket,@Param(value="transportation")String transportation);

	    List<OutKilometreScene> selectByTimeWord31(@Param(value="timeTotal")String timeTotal,@Param(value="costTotal")String costTotal,@Param(value="transportation")String transportation);
	    
	    List<OutKilometreScene> selectByTimeWord4(@Param(value="timeTotal")String timeTotal,@Param(value="transportation")String transportation);

	    List<OutKilometreScene> selectByTimeWord5(@Param(value="timeTotal")String timeTotal,@Param(value="transportation")String transportation);
}
