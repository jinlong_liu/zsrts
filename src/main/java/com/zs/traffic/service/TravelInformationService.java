package com.zs.traffic.service;


import java.util.List;
import java.util.Date;
import java.util.List;

import org.apache.xmlbeans.impl.jam.visitor.TraversingJVisitor;
import org.springframework.stereotype.Service;

import com.zs.traffic.model.TravelInformation;

/**
 * 
 * @author
 *
 */
@Service
public interface TravelInformationService {
	
    public int deleteByPrimaryKey(Integer id);

    public int insert(TravelInformation record);

    public int insertSelective(TravelInformation record);

    public TravelInformation selectByPrimaryKey(Integer id);

    public int updateByPrimaryKeySelective(TravelInformation record);

    public int updateByPrimaryKey(TravelInformation record);
    
    public List<TravelInformation> selectByAppuserId(Integer appuserId,Date todayDate);
    
    public List<TravelInformation> selectByAppuserIdNear(Integer appuserId,Date todayDate);
    
    public List<TravelInformation> getTravelInformation(Integer appuserId);
    
    public TravelInformation selectTotalNumber(Date todayDate,String cityCode);//查询当天参与调查的总人数
    
    public List<TravelInformation> selectByDate(Date todayDate,String cityCode);//查询当天每个人的所有信息
    
    public List<TravelInformation> selectTripByAppuserId(Integer appuserId,Date todayDate);//查询出当前登陆人的出行方式和次数
    
    public List<TravelInformation> selectTripObjectiveByAppuserId(Integer appuserId,Date todayDate);//查询出当前登陆人的出行目的和次数
    
    public List<TravelInformation> getTravelConditions(Integer appuserId );
    
    public List<TravelInformation> getDetailsInformation(Integer id);
    
    public List<TravelInformation> getAnalysis(Date todayDate);
    
    public int deleteByAppuserId(Integer appuserId,Date date);
    
    public int  deleteByAppuserIdStatus(Integer appuserId,Date date);
    
    public List<TravelInformation> getIsTravel(Integer appuserId);
    
    public List<TravelInformation> selectTraGps(Integer appuserId,Date dayDate);
    
    public List<TravelInformation> selectByRanking(int appuserId,Date startDate,int ranking);//查询出当天第n条出行信息，status=1
    
    public int deleteByRank(int appuserId,Date startDate,int ranking);//删除当天第n条出行信息，status=1
    
    public List<TravelInformation> selectPush(Integer appuserId,Date dayDate);//推送
}
