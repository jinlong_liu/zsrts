package com.zs.traffic.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import com.zs.traffic.model.RechargeableCard;
import com.zs.traffic.model.RegisterNumber;
import com.zs.traffic.model.Charge;

@Service
public interface RechargeableCardService {

	int deleteByPrimaryKey(Integer id);

    int insert(RechargeableCard record);

    int insertSelective(RechargeableCard record);

    RechargeableCard selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RechargeableCard record);

    int updateByPrimaryKey(RechargeableCard record);
    
    //查询有效的充值卡
    List<RechargeableCard> selectListByTypeSign(@Param(value="state")Integer state,@Param(value="type")Integer type);

}

