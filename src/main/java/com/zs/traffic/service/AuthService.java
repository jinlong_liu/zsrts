package com.zs.traffic.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.Auth;

/**
 * @description 获取可查询的数据的日期列表  
 * 
 * @author renxw
 * 
 * @date 2016.6.18
 *
 */
@Service
public interface AuthService {
	
	/**
	 * 获取权限
	 * @return
	 */
	public List<Auth> getAuthList();

	
}
