package com.zs.traffic.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.RegisterNumber;
import com.zs.traffic.model.Charge;

@Service
public interface RegisterNumberService {

	int deleteByPrimaryKey(Integer id);

    int insert(RegisterNumber record);

    int insertSelective(RegisterNumber record);

    RegisterNumber selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RegisterNumber record);

    int updateByPrimaryKey(RegisterNumber record);
    
    RegisterNumber selectByRechargeType(Integer rechargeType);
}

