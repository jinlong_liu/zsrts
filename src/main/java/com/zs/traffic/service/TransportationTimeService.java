package com.zs.traffic.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.TransportationTime;

/**
 * 
 * @author
 *
 */
@Service
public interface TransportationTimeService {

	List<TransportationTime> selectAll();

	List<TransportationTime> selectOne(String kilometre);

}
