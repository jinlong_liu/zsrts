package com.zs.traffic.service;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.SysUserRole;


/**
 * 
 * @author
 *
 */
@Service
public interface SysUserRoleService {
	int deleteByPrimaryKey(Integer id);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);
    
    int updateBySysUserId(SysUserRole sysUserRole);//用户分配角色
    
    int deleteByUserId(Integer userId);
	
}
