package com.zs.traffic.service;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import com.zs.traffic.model.TrajectoryDate;
import com.zs.traffic.model.TravelInformation;

/**
 * 
 * @author
 *
 */
@Service
public interface TrajectoryDateService {
	
	 public int deleteByPrimaryKey(Integer id);

	 public  int insert(TrajectoryDate record);

	 public int insertSelective(TrajectoryDate record);

	 public  TrajectoryDate selectByPrimaryKey(Integer id);

	 public int updateByPrimaryKeySelective(TrajectoryDate record);

	 public  int updateByPrimaryKey(TrajectoryDate record);
	 
	 public int insertSelectives(@Param(value="record")TrajectoryDate record,@Param(value="tableName")String tableName);
	 
	 public List<TrajectoryDate> selectByAppuserAndStartDate(@Param(value="record")TrajectoryDate record,@Param(value="tableName")String tableName);
	 
	 public List<TrajectoryDate> gettrajectory(Integer appuserId,Date startDate, Date endDate, String tableName);

	 
	 public List<TrajectoryDate> selectByTableName(@Param(value="tableName")String tableName
			 ,@Param(value="appuserId")Integer appuserId,@Param(value="startDate")Date startDate);
	 
	 public List selectAppuserIdByTableName(@Param(value="tableName")String tableName);
   

	 public List<TrajectoryDate> getTrajectoryMorning(Integer appuserId,Date startDate, Date endDate, String tableName);
	 
	 public List<TrajectoryDate> getTrajectoryAfternoon (Integer appuserId,Date startDate, Date endDate, String tableName);

}
