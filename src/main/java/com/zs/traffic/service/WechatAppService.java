package com.zs.traffic.service;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.WechatApp;


@Service
public interface WechatAppService {
	public int insertSelective(WechatApp wechatApp);
}
