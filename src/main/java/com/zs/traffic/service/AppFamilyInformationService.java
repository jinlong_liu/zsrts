package com.zs.traffic.service;

import java.util.Date;
import java.util.List;

import com.zs.traffic.model.TravelInformation;
import org.springframework.stereotype.Service;

import com.zs.traffic.model.AppFamilyInformation;

/**
 *
 * @author
 *
 */
@Service
public interface AppFamilyInformationService {

    public int deleteByPrimaryKey(Integer familyId);

    public int insert(AppFamilyInformation record);

    public int insertSelective(AppFamilyInformation record);

    public AppFamilyInformation selectByPrimaryKey(Integer familyId);

    public int updateByPrimaryKeySelective(AppFamilyInformation record);

    public int updateByPrimaryKey(AppFamilyInformation record);

    public List<AppFamilyInformation> selectByAppuserId(Integer appuserId);

    public int deleteByAppUserId(Integer appuserId);

    public AppFamilyInformation getListPersonal(Integer appuserId);

    public List<AppFamilyInformation>getListAppFamilyInformation(String cityCode);

    public List<AppFamilyInformation>selectAppFamilyInformationForAge(int starAge,int endAge);

    public List<AppFamilyInformation>selectAppFamilyInformationForWork(int work);

    public List<AppFamilyInformation>selectAppFamilyInformationForSex(int sex);

    public List<AppFamilyInformation>selectAppFamilyInformationForIsLocal(int local);

    public List<AppFamilyInformation>selectAppFamilyInformationForUserName(String userName);

    public List<AppFamilyInformation>selectAppuser(int starAge,int endAge,int work,int sex,int local,String userName,String cityCode);

    public int selectAppUserTotal();

    public int selectAppUserNoaudit();

    public int selectAppUserYesaudit();

    public int selectAppUserTwentyNoaudit();

    public int selectAppUserTwentyYesaudit();

    public int selectAppUserThirtyNoaudit();

    public int selectAppUserThirtyYesaudit();

    public int selectAppUserFiftyNoaudit();

    public int selectAppUserFiftyYesaudit();

    //    获取轨迹的日期
    public List<String> getTravelTIme(int userId);


}
