package com.zs.traffic.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import com.zs.traffic.model.Area;

/**
 * 
 * @author
 *
 */
@Service
public interface AreaService {
	
	/*
	 * 根据城市编号获取社区
	 */
	public List<Area> selectAllForCity(String city);

	
	public List<Area> getListSubByNameWlj(@Param(value="type")Integer type,@Param(value="cityCode")String cityCode,@Param(value="areaId")String areaId);

	
	public Area getAreaIdByTitltName(String titltName);
	
	/*
	 * 根据社区标号查询一条实体
	 */
	public Area getSelectByConumber(String communityNumber);
	public List<Area> getListForQrAll(String districtSelectId,String subOfficeSelectId,String communitySelectName,String cityCode);
	
	//public List<Area> getSelectTypeOne();
	public List<Area> getSelectTypeOne(String cityCode);

	public List<Area> selectAll();
	/*
	 * 1.根据名称获取list
	 * type:1行政区，2街道 3.小区  供树形菜单使用
	 */
	public List<Area> getListByName(String Name,Integer type);
	
	
	public List<Area> getListByNameOrNum(String Name,String number,Integer type,String cityCode,String districtNumber);
	
	public List<Area> getListByNameOrNumById(String Name,String number,Integer id,Integer type);

	public List<Area> getListByNameOrNumInclude(String name,String number,Integer type,String cityCode);
	
	/*
	 * 2.根据type获取相应集合
	 */
	public List<Area> getListByType(Integer type);
	
	/*
	 * 3.根据修改所选标题修改下属的所有区域
	 */
	
	
	public int updateName(Area record,String name);
	
	/*
	 * 4.根据选择名称获取list
	 * type:1行政区，2街道 3.小区  供树形菜单使用
	 */
	public List<Area> getSelectListByName(@Param(value="name")String Name,@Param(value="type")Integer type);
	
	/*
	 * 5.根据选择type获取相应集合
	 */
	public List<Area> getSelectListByNames(String Name,@Param(value="type")Integer type,String cityCode);
	
	public List<Area> getListByNames(String name,Integer type);
	
	public List<Area> getAllList();
	
	 public int deleteByPrimaryKey(Integer areaId);

	 public int insert(Area record);

	 public int insertSelective(Area record);

	 public Area selectByPrimaryKey(Integer areaId);

	 public int updateByPrimaryKeySelective(Area record);

	 public int updateByPrimaryKey(Area record);
	 
	 public List<Area> getAreaByuserId(Integer userId,Integer type);
	
	 public List<Area> getAllListArea(Integer type,String cityCode);
	 
	 public List<Area> getAreaList(List<String> areaNumber,Integer authDegree,String cityCode);
	 
	 public List<Area> queryByareaNumber(List<String> areaNumber,Integer authDegree,String cityCode);
	 
	 public List<Area> queryByRegion();
     
	 public List<Area> queryListArea(String districtName);
	 
	 public List<Area> queryAreaList(String subofficeName);
	 
	 public List<Area> queryAreaIst(String districtName,String subofficeName);
	 
	 public List<Area> queryListParameter(String districtNumber,String subofficeNumber);
	 
	 public List<Area> queryParameterList(String districtName,String subofficeName);
	 
	 public List<Area> getAreaId(String communityNumber);
	 

	 public List<Area> queryAreaByRole(int userId,int authDegree,int type,String cityCode);
	 

	 public List<Area> selectCommunityName(String subofficeNumber);
	 
	 public Area addEcho(Integer areaId);

	 public List<Area> selectDistrictNumber(String districtNumberUpdate,String cityCode);
	 
	 public List<Area> selectDistrictName(String districtNameUpdate,String cityCode);
	 
	 public List<Area> selectSubofficeNumber(String districtNumber);
	 
	 public int updateDistrict(String districtNumberUpdate,String districtNameUpdate,String districtNumber,String cityCode);
	 
	 public List<Area> selectSubofficeNumbers(String subofficeNumberUpdate,String cityCode);
	 
	 public List<Area> selectSubofficeNames(String subofficeNameUpdate,String cityCode);
	 
	 public List<Area> selectCommunityNumber(String subofficeNumber,String cityCode);
	 
	 public int updateSuboffice(String subofficeNumberUpdate,String subofficeNameUpdate,String subofficeNumber,String cityCode);
	 
     public List<Area> selectCommunityNumberUpdates(String communityNumberUpdate,String cityCode);
	 
	 public List<Area> selectCommunityNameUpdates(String communityNameUpdate,String cityCode);
	 
	 public List<Area> selectSub(String communityNumber);
	 
	 public int updateCommunity(String communityNumberUpdate,String communityNameUpdate,String communityNumber,String cityCode);
	 
	
}
