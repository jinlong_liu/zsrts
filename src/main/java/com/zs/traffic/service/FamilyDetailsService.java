package com.zs.traffic.service;

import java.util.List;





import java.util.Map;

import org.springframework.stereotype.Service;



/**
 * @description 获取问卷查询信息
 * 
 * @author antl
 * 
 * @date 2017.3.29
 *
 */
@Service
public interface FamilyDetailsService {
	
	public String getFamilyCar(String urlPath);
	
	public String getFaminyHome(String urlPath);
    
	public String getFaminy(String urlPath);
	
	public String getInformation (String urlPath);
	
	

}
