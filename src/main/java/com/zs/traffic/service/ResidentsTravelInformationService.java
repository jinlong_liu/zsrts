package com.zs.traffic.service;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.ResidentsTravelInformation;

@Service
public interface ResidentsTravelInformationService {
	
	public int deleteByPrimaryKey(Integer id);

	public int insert(ResidentsTravelInformation record);

	public int insertSelective(ResidentsTravelInformation record);

	public ResidentsTravelInformation selectByPrimaryKey(Integer id);

	public int updateByPrimaryKeySelective(ResidentsTravelInformation record);

	public int updateByPrimaryKey(ResidentsTravelInformation record);
}
