package com.zs.traffic.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import com.zs.traffic.model.CityCodeUrl;

@Service
public interface CityCodeUrlService {
	int deleteByPrimaryKey(Integer id);

    int insert(CityCodeUrl record);

    int insertSelective(CityCodeUrl record);

    CityCodeUrl selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CityCodeUrl record);

    int updateByPrimaryKey(CityCodeUrl record);
    
    CityCodeUrl selectByCityCode(@Param(value="cityCode")String cityCode,@Param(value="extendedFieldOne")String extendedFieldOne);
    
    public List<CityCodeUrl> getSignature(String cityCode);

}
