package com.zs.traffic.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.AppBusInfo;
import com.zs.traffic.model.SysAppuser;

/**
 * 
 * @author
 *
 */
@Service
public interface SysAppuserService {
	
	public int deleteByPrimaryKey(Integer appuserId);

	public int insert(SysAppuser record);

	public  int insertSelective(SysAppuser record);

	public SysAppuser selectByPrimaryKey(Integer appuserId);

	public  int updateByPrimaryKeySelective(SysAppuser record);

	public  int updateByPrimaryKey(SysAppuser record);
	
	public SysAppuser selectByUserName(String userName);
	
	public int selectQuantity(String todayDate,String tomorrowDate);
	
	public List<SysAppuser> selectByOccupation(String ip);

	
}
