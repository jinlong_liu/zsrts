package com.zs.traffic.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import com.zs.traffic.model.Area;
import com.zs.traffic.model.FamilyInformation;
import com.zs.traffic.model.StatisticsParticular;
import com.zs.traffic.model.User;

/**
 * @description 获取问卷查询信息
 * 
 * @author antl
 * 
 * @date 2017.3.29
 *
 */
@Service
public interface StatisticsParticularService {
	
	List<StatisticsParticular> selectAllNumForTitle();
	
	List<StatisticsParticular> selectByFamilyList(String familyId);
	
	StatisticsParticular selectByFamily(String familyId);
	
	List<StatisticsParticular>  selectBySubmitterForList(String openId);
	
	List<StatisticsParticular> selectAll();
	
	List<StatisticsParticular> selectforyestoday();
	
    StatisticsParticular selectBySubmitterriginal(String Submitter);
	
	 StatisticsParticular selectBySubmitterIs(String Submitter);
	
	int selectTotalCountByComm(String titleName);
	
	int selectCountByComm(String titleName,String submitDate);

	public List<StatisticsParticular> getStatisticsParticularList(String cityCode ,Integer limit,Integer offset);
	
	int deleteByPrimaryKey(Integer id);

    int insert(StatisticsParticular record);

    StatisticsParticular selectByPrimaryKey(Integer id);
   
    int updateByPrimaryKey(StatisticsParticular record);

	public int updateByPrimaryKeySelective(StatisticsParticular statisticsParticular);

	public int insertSelective(StatisticsParticular statisticsParticular);
	

	public int selectFollowCount(Integer areaId);//查询关注数
	
	public int selectubmitCount(Integer areaId);//查询提交数
	
	public int selectNotCompleteCount(Integer areaId);//查询未完成提交数
	
	public StatisticsParticular selectBySubmitter(String submitter);
	
	public  List<StatisticsParticular> getTitle();
	
	int intoAreaId(Integer areaId, String titleName );
	
	public List<StatisticsParticular> getQuestionnaire (List<String> areaNumber,Integer type,String cityCode,Integer limit,Integer offset);
	
	public List<StatisticsParticular> getTwoQuestionnaire (List<String> areaNumber,Integer type,String cityCode,Integer limit,Integer offset);
	
	public List<StatisticsParticular> getSanQuestionnaire (List<String> areaNumber,Integer type,String cityCode,Integer limit,Integer offset);
	
	public List<StatisticsParticular> getConditionsDistrictName(String districtName);
	
	public List<StatisticsParticular> getConditions(String districtName,String subofficeName);
	
	public List<StatisticsParticular> getConditionsFamilyId(String familyId);
	
	public List<StatisticsParticular> getPublicQuestionnaire(String cityCode,Integer limit,Integer offset); 
	
	public List<StatisticsParticular> getaudit(String familyId);

	public StatisticsParticular getProgress(String familyId);
	
	public int auditQuestionnaire(String familyId, Integer audit);

	public int selecttotalAll(String cityCode);
	
	public int selectResidentsQuestionnaire(String cityCode);
	
	public int selectReviewNot(String cityCode);
	
	public int selectReviewYes(String cityCode);
	
	public List<StatisticsParticular>selectTravelTotal(String cityCode);
	
	public List<StatisticsParticular>selectDis(String cityCode);
	
	public List<StatisticsParticular>selectPublic(String cityCode,String district,Integer limit,Integer offset);
	
	public int selectPublicTotal(String cityCode,String district);
	
	public int selectStageTotal(String cityCode);
	
	public int selectConditions(String districtNumber,String subofficeNumber,String familyId,String communityNumber,String cityCode);
	
	public int selectConditionsResidents (String districtNumber,String subofficeNumber,String familyId,String communityNumber,String cityCode);
	
	public int selectCondditionNotaudit(String districtNumber,String subofficeNumber,String familyId,String communityNumber,String cityCode);
	
	public int selectCondditioYesaudit(String districtNumber,String subofficeNumber,String familyId,String communityNumber,String cityCode);
	
	public int selectCondditionStageTotal(String districtNumber,String subofficeNumber,String familyId,String communityNumber,String cityCode);
	
	public List<StatisticsParticular>selectTotal(String districtNumber,String subofficeNumber,String familyId,String communityNumber,String cityCode);
	
	public List<StatisticsParticular>selectContent(String districtNumber,String subofficeNumber,String familyId,String communityNumber,String cityCode,Integer limit,Integer offset);
	
	public List<StatisticsParticular> selectDeviation(String districtNumber,String subofficeNumber,String familyId,String communityNumber,String cityCode);

	int insertSelectiveForOne(String openId,String textAddress);
	
	int updateByPrimaryKeySelectiveForPublic(String openId,String textAddress,String communityNumber);
	
	public List<StatisticsParticular> getStatistics(String cityCode);
	
	public int getQuestionnaireTotal (List<String> areaNumber,String cityCode);
	
	public int getTwoQuestionnaireTotal (List<String> areaNumber,String cityCode);
	
	public int getSanQuestionnaireTotal (List<String> areaNumber,String cityCode);
	
	public int selectContentTotal(String districtNumber,String subofficeNumber,String familyId,String communityNumber,String cityCode);
	
	public int getPublicQuestionnaireTotal(String cityCode); 
	
	public int selectOneTotal (List<String> areaNumber,String cityCode);
	
	public int selectTwoTotal (List<String> areaNumber,String cityCode);
	
	public int selectThreeTotal (List<String> areaNumber,String cityCode);
	
	public int selectResidentsOne (List<String> areaNumber,String cityCode);
	
	public int selectResidentsTwo (List<String> areaNumber,String cityCode);
	
    public int selectResidentsThree (List<String> areaNumber,String cityCode);
    
    public int selectReviewNotOne (List<String> areaNumber,String cityCode);
    
    public int selectReviewNotTwo (List<String> areaNumber,String cityCode);
    
    public int selectReviewNotThree (List<String> areaNumber,String cityCode);
    
    public int selectReviewYesOne (List<String> areaNumber,String cityCode);
    
    public int selectReviewYesTwo (List<String> areaNumber,String cityCode);
    
    public int selectReviewYesThree (List<String> areaNumber,String cityCode);
    
    public int selectStageTotalOne (List<String> areaNumber,String cityCode);
    
    public int selectStageTotalTwo (List<String> areaNumber,String cityCode);
    
    public int selectStageTotalThree (List<String> areaNumber,String cityCode);
    
    public List<StatisticsParticular>getStatisticsOne(List<String> areaNumber,String cityCode);
    
    public List<StatisticsParticular>getStatisticsTwo(List<String> areaNumber,String cityCode);
    
    public List<StatisticsParticular>getStatisticsThree(List<String> areaNumber,String cityCode);
    
    public StatisticsParticular getFamilyId(int id);
    
/*    public int selectPublicTotal();*/
    
}
