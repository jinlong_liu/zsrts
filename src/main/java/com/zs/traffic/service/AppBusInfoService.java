package com.zs.traffic.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.AppBusInfo;

/**
 * 
 * @author
 *
 */
@Service
public interface AppBusInfoService {
	
	public AppBusInfo selectByPrimaryKey(Integer id);
	
	public List<AppBusInfo> selectByAppuserId(Integer appuserId);
	
	public int insertSelective(AppBusInfo record);
	
	public int insert(AppBusInfo record);
	
	public int deleteByPrimaryKey(Integer id);
	
	public int updateByPrimaryKeySelective(AppBusInfo record);

    public int updateByPrimaryKey(AppBusInfo record);
	
    public int deleteByAppUserId(Integer appuserId);

	
}
