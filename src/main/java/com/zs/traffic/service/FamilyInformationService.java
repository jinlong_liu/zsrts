package com.zs.traffic.service;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.FamilyInformation;



/**
 * 
 * @author
 *
 */
@Service
public interface FamilyInformationService {
	
	 public int deleteByPrimaryKey(Integer familyId);

	 public int insert(FamilyInformation record);

	 public  int insertSelective(FamilyInformation record);

	 public FamilyInformation selectByPrimaryKey(Integer familyId);

	 public int updateByPrimaryKeySelective(FamilyInformation record);

	 public int updateByPrimaryKey(FamilyInformation record);
	 
	 public int setFa(String familyId, String chuxing);

}
