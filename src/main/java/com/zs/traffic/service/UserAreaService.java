package com.zs.traffic.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.User;
import com.zs.traffic.model.UserArea;


@Service
public interface UserAreaService {
	
	 public int deleteByPrimaryKey(Integer id);

	 public   int insert(UserArea record);

	 public int insertSelective(UserArea record);

	 public UserArea selectByPrimaryKey(Integer id);

	 public int updateByPrimaryKeySelective(UserArea record);

	 public  int updateByPrimaryKey(UserArea record);
	 
	 public int deleteBySysUserId(Integer userId);
	 
	 public List<UserArea> getUser(Integer areaId);
}
