package com.zs.traffic.service;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.AppBusInfo;
import com.zs.traffic.model.SysRoleAuth;

/**
 * 
 * @author
 *
 */
@Service
public interface SysRoleAuthService {
	
	  public SysRoleAuth getSysRoleAuthByRoleId(Integer roleId);
}
