package com.zs.traffic.service;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.Inhabitan;

@Service
public interface InhabitanService {
	
	 public int deleteByPrimaryKey(Integer inhabitantId);

	 public  int insert(Inhabitan record);

	 public  int insertSelective(Inhabitan record);

	 public Inhabitan selectByPrimaryKey(Integer inhabitantId);

	 public int updateByPrimaryKeySelective(Inhabitan record);

	 public  int updateByPrimaryKey(Inhabitan record);
}
