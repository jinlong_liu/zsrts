package com.zs.traffic.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zs.traffic.model.Charge;

@Service
public interface ChargeService {

	int deleteByPrimaryKey(Integer id);

    int insert(Charge record);

    int insertSelective(Charge record);

    Charge selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Charge record);

    int updateByPrimaryKey(Charge record);

    
    public int Appaudit(Integer userId, Integer status, String opportunity );  
    
    public Charge selectAppaudit(Integer userId);
    
    int selectTotal(Integer rechargeType);
    
	int selectTodayNumber(Integer rechargeType,String today,String tomorrowDay);

    List<Charge> selectByStateForBill(Integer state);



}

