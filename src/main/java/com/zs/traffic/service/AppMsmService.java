package com.zs.traffic.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;


import com.zs.traffic.model.AppSms;
import com.zs.traffic.model.AppBusInfo;




/**
 * 
 * @author
 *
 */
@Service
public interface AppMsmService {

	
    //int insertSelective(AppSms record);

    public int insertSelective(AppSms record);
    
    public List<AppSms> queryRequestTime(String userName);

    List<AppSms> selectByUserNameTime(String userName,String cityCode,Date sendTime);
    
    int updateByPrimaryKeySelective(AppSms record);


}
