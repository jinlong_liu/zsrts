package com.zs.traffic.wx.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import com.zs.traffic.controller.BaseController;
import com.zs.traffic.controller.WxQuestionnaireController;
import com.zs.traffic.wx.model.event.AccessToken;
import com.zs.utils.AccessTokenUtil;
import com.zs.utils.HttpRequestUtil;

import net.sf.json.JSONObject;




public class TokenThread extends BaseController implements Runnable {

		//微信公众号的凭证和秘钥wanglijian2017.03.25
		//public static final String appID = "wx8ebd582644d0bb29";
		//public static final String appScret = "9bf0522749a796aece4e241f79748a5f";
		public static final String appID = getProperties().getProperty("appID");
	    public static final String appScret = getProperties().getProperty("appScret");
	 //public static final String appID = "wx1c6fb297bf4bf502";
	 //public static final String appScret = "f5304b500937d1596f616d9facabe22a";
		public static AccessToken access_token=null;		
		
		@Override
		public void run() {
			String accessToken = WxQuestionnaireController.token.getAccess_token();
			while(true){
				//调用工具类获取access_token(每日最多获取2000次，每次获取的有效期为7200秒)
				System.out.println("accessToken间接获取成功");
				//access_token=AccessTokenUtil.getAccessToken(appID, appScret);		
				if(null!=accessToken){
					System.out.println("accessToke间接获取成功："+accessToken);
					//创建与删除自定义菜单方法
				    //this.createMenu();
					//this.deleteMenu();
					//7000秒之后重新进行获取
					try {
						Thread.sleep((access_token.getExpires_in()-200)*1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					//60秒之后尝试重新获取
					//Thread.sleep(60*1000);
				}
			}
		}
	
		/**
		  * 创建Menu
		 * @Title: createMenu
		 * @Description: 创建Menu
		 * @param @return
		 * @param @throws IOException    设定文件
		 * @return int    返回类型
		 * @要求 同时支持get post请求
		 * wanglijian2017.03.25
		  */
		    public static String createMenu() {
		    	
		    	String accesstoken = WxQuestionnaireController.token.getAccess_token();
		      //开发环境 http://www.trafficsurveyer.com/    http%3a%2f%2fwww.trafficsurveyer.com%2fzsrts%2fservices%2fgetOpenId
		      //String menu = "{\"button\":[{\"type\":\"view\",\"name\":\"问卷填写\",\"url\":\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1c6fb297bf4bf502&redirect_uri=http%3a%2f%2f170a508b98.imwork.net%2fzsrts%2fservices%2fgetOpenId&response_type=code&scope=snsapi_base&state=123#wechat_redirect\"}]}";
		      //域名
		      String menu = "{\"button\":[{\"type\":\"view\",\"name\":\"问卷填写\",\"url\":\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1c6fb297bf4bf502&redirect_uri=http%3a%2f%2fwww.trafficsurveyer.com%2fzsrts%2fservices%2fgetOpenId&response_type=code&scope=snsapi_base&state=123#wechat_redirect\"}]}";

		      
		      //String menu = "{\"button\":[{\"type\":\"view\",\"name\":\"问卷填写\",\"url\":\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx8ebd582644d0bb29&redirect_uri=http%3a%2f%2f16820215zl.iask.in%2fzsrts%2fservices%2fgetOpenId&response_type=code&scope=snsapi_base&state=123#wechat_redirect\"},{\"type\":\"click\",\"name\":\"中奖查询\",\"key\":\"2\"},{\"name\":\"日常活动\",\"sub_button\":[{\"type\":\"click\",\"name\":\"待办\",\"key\":\"01_WAITING\"},{\"type\":\"click\",\"name\":\"已办\",\"key\":\"02_FINISH\"},{\"type\":\"click\",\"name\":\"我的\",\"key\":\"03_MYJOB\"},{\"type\":\"click\",\"name\":\"公告\",\"key\":\"04_MESSAGEBOX\"},{\"type\":\"click\",\"name\":\"签到\",\"key\":\"05_SIGN\"}]}]}";
		      //String menu = "{\"button\":[{\"type\":\"view\",\"name\":\"问卷填写\",\"url\":\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx5bca91f9ddaabcbf&redirect_uri=http%3a%2f%2f16820215zl.iask.in%2fzsrts%2fservices%2fgetOpenId&response_type=code&scope=snsapi_base&state=123#wechat_redirect\"},{\"type\":\"click\",\"name\":\"中奖查询\",\"key\":\"2\"},{\"name\":\"日常活动\",\"sub_button\":[{\"type\":\"click\",\"name\":\"待办\",\"key\":\"01_WAITING\"},{\"type\":\"click\",\"name\":\"已办\",\"key\":\"02_FINISH\"},{\"type\":\"click\",\"name\":\"我的\",\"key\":\"03_MYJOB\"},{\"type\":\"click\",\"name\":\"公告\",\"key\":\"04_MESSAGEBOX\"},{\"type\":\"click\",\"name\":\"签到\",\"key\":\"05_SIGN\"}]}]}";

		      
		      //String access_token = "KN1qsZJj1Ii6Pq58Vs2FG3dul2CkUqnAqnzLB723WawQFaLWK8x7kyZaVkBBjeZ58hzeD0K4Lwgfu-J0Urzzj7IcsC3y-mvfsGGtdVwj5_l5syFKtpLhBoM7550c9CO4SGAbAHAMAU";                    
	            String action = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+accesstoken;
	            System.out.println("执行创建菜单命令！"+action);
		        try {
		           URL url = new URL(action);
		           HttpURLConnection http =   (HttpURLConnection) url.openConnection();    
		           http.setRequestMethod("POST");        
		           http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");    
		           http.setDoOutput(true);        
		           http.setDoInput(true);
		           System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
		           System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
		           http.connect();
		           OutputStream os= http.getOutputStream();    
		           os.write(menu.getBytes("UTF-8"));//传入参数    
		           os.flush();
		           os.close();

		           InputStream is =http.getInputStream();
		           int size =is.available();
		           byte[] jsonBytes =new byte[size];
		           is.read(jsonBytes);
		           String message=new String(jsonBytes,"UTF-8");
		           System.out.println("菜单创建成功！");
		           return "返回信息"+message;
		           } catch (MalformedURLException e) {
		               e.printStackTrace();
		           } catch (IOException e) {
		               e.printStackTrace();
		           }   
		        System.out.println("菜单创建失败！");
		        return "createMenu 失败";
		   }
		    
		    
		    /**
		     * 删除当前Menu
		    * @Title: deleteMenu
		    * @Description: 删除当前Menu
		    * @param @return    设定文件
		    * @return String    返回类型
		    * @要求 同时支持get post请求
		    * wanglijian2017.03.25
		     */
		   public static String deleteMenu()
		   {
			   String accesstoken = WxQuestionnaireController.token.getAccess_token();
			   //String access_token = "EcKgcMvu15EZHRiX3OII0w_Z7fPsKcPKMBKFIEY_h7Qe4y_pne6tIFJ0nnRrtIdYt21DjdCplG06VS8Ka0ewDOx7we8eYZNQE0CtLDyOQ60anl9M1KENvl949molbsWUMCQaABAVLZ";
	           String action = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token="+accesstoken;
	           System.out.println("执行删除菜单命令!"+action);
		       try {
		          URL url = new URL(action);
		          HttpURLConnection http =   (HttpURLConnection) url.openConnection();    
		          http.setRequestMethod("GET");        
		          http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");    
		          http.setDoOutput(true);        
		          http.setDoInput(true);
		          System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
		          System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
		          http.connect();
		          OutputStream os= http.getOutputStream();    
		          os.flush();
		          os.close();
		          InputStream is =http.getInputStream();
		          int size =is.available();
		          byte[] jsonBytes =new byte[size];
		          is.read(jsonBytes);
		          String message=new String(jsonBytes,"UTF-8");
		          System.out.println("菜单创建成功！");
		          return "deleteMenu返回信息:"+message;
		          } catch (MalformedURLException e) {
		              e.printStackTrace();
		          } catch (IOException e) {
		              e.printStackTrace();
		          }
		       System.out.println("菜单创建失败！");
		       return "deleteMenu 失败";   
		   }
}
