package com.zs.traffic.wx.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/*
 * 过滤器
 * @autoh wanglijian
 * date 20170414
 * 逻辑  访问地址存在wx的进行验证，
 * 如果来自移动端且微信客户端
 * 且session中存在openId可以则正常访问
 */
public class LoginWeFilter implements Filter {
	// 需要定义系统页面访问中可放行的连接
	private List<String> list = new ArrayList<String>();
	public void init(FilterConfig arg0) throws ServletException {
	    list.add("wx/false405.jsp");
	}

	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String rootPath=servletRequest.getContextPath();//获取项目名称
		String path = servletRequest.getRequestURI();//获取用户url
		//资源文件无需过滤
		if(path.endsWith(".png") || path.endsWith(".gif") || path.endsWith(".css") 
				|| path.endsWith(".js") || path.endsWith(".json")) {
			chain.doFilter(servletRequest, servletResponse);
			return;
		}
		if (list != null && list.contains(path)) {
			// 如果页面中获取的访问连接于定义的可放行的连接一致，则放行
			chain.doFilter(request, response);
			return;
		}

		if(path.indexOf("wx")!=-1||(rootPath+"/services/getStage").equals(path)) {
		String  openId=(String)session.getAttribute("openId");
		if(openId==null){
			request.getRequestDispatcher("/wx/error.jsp").forward(request,response);
			return;
		}
		//判断请求是否来自手机
	    String isMoblie = "false";  
		            String[] mobileAgents = { "iphone", "android", "phone", "mobile", "wap", "netfront", "java", "opera mobi","opera mini","ucweb", "windows ce", "symbian", "series", "webos", "sony", "blackberry", "dopod",  "nokia", "samsung", "palmsource", "xda", "pieplus", "meizu", "midp", "cldc", "motorola", "foma", "docomo", "up.browser", "up.link", "blazer", "helio", "hosin", "huawei", "novarra", "coolpad", "webos",  "techfaith", "palmsource", "alcatel", "amoi", "ktouch", "nexian","ericsson", "philips", "sagem","wellcom", "bunjalloo", "maui","smartphone", "iemobile", "spice", "bird", "zte-", "longcos","pantech", "gionee", "portalmmm", "jig browser", "hiptop", "benq", "haier", "^lct", "320x320", "240x320", "176x220", "w3c ", "acs-", "alav", "alca", "amoi", "audi", "avan", "benq", "bird", "blac","blaz", "brew", "cell", "cldc", "cmd-", "dang", "doco", "eric", "hipt", "inno", "ipaq", "java", "jigs","kddi", "keji", "leno", "lg-c", "lg-d", "lg-g", "lge-", "maui", "maxo", "midp", "mits", "mmef", "mobi","mot-", "moto", "mwbp", "nec-", "newt", "noki", "oper", "palm", "pana", "pant", "phil", "play", "port","prox", "qwap", "sage", "sams", "sany", "sch-", "sec-", "send", "seri", "sgh-", "shar", "sie-", "siem","smal", "smar", "sony", "sph-", "symb", "t-mo", "teli", "tim-", "tosh", "tsm-", "upg1", "upsi", "vk-v","voda", "wap-", "wapa", "wapi", "wapp", "wapr", "webc", "winw", "winw", "xda", "xda-","Googlebot-Mobile" };  
		            if (((HttpServletRequest) request).getHeader("User-Agent") != null) {  
		                for (String mobileAgent : mobileAgents) {  
		                    if (((HttpServletRequest) request).getHeader("User-Agent").toLowerCase().indexOf(mobileAgent) >= 0) {  
		                        isMoblie = "true";  
		                        break;  
		                    }  
		                }  
		            }
		if(isMoblie!="true"||!"true".equals(isMoblie)){
			request.getRequestDispatcher("/wx/error.jsp").forward(request,response);
			return;
		}
		//判断是否来自微信游览器
		String we = "false";
		String ua = ((HttpServletRequest) request).getHeader("user-agent") .toLowerCase();  
		if (ua.indexOf("micromessenger") > 0) {// 是微信浏览器  
		    we = "true";  
		}  
		if(we!="true"&&!"we".equals(we)){
			request.getRequestDispatcher("/wx/error.jsp").forward(request,response);
			return;
		}else{
			chain.doFilter(servletRequest, servletResponse);
			return;
		} 
		}else{
			chain.doFilter(servletRequest, servletResponse);
			return;
		}
	}
	public void destroy() {

	}
}
