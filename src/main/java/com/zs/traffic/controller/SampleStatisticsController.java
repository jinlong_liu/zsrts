package com.zs.traffic.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.traffic.model.AppFamilyInformation;
import com.zs.traffic.model.Area;
import com.zs.traffic.model.StatisticsParticular;
import com.zs.traffic.model.TravelInformation;
import com.zs.traffic.service.AreaService;
import com.zs.traffic.service.SampleStatisticsService;
import com.zs.traffic.service.StatisticsParticularService;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;


/**
 * @description app样本统计
 * 
 * @author Antl
 * 
 * @date 2017-05-16
 *
 */

@Controller
@RequestMapping("/")
public class SampleStatisticsController extends BaseController {
	
	
	@Autowired
	public SampleStatisticsService aaService;

	/**
	 * 样本统计
	 * @throws ParseException 
	 * 
	 */
	@RequestMapping(value="/selectSex", method=RequestMethod.POST)
    public void selectSex(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException, ParseException{
    	request.setCharacterEncoding("UTF-8");
    	HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		
		String cityCode =  (String) session.getAttribute("cityCode");//获取用户城市编码
		
    	String sex = request.getParameter("sex");
    	String work = request.getParameter("work");
    	String isLocal = request.getParameter("isLocal");
    	String starAge = request.getParameter("starAge").trim();
    	String endAge = request.getParameter("endAge").trim();
    	String today = request.getParameter("toDate").trim();
    	
    	TravelInformation model = new TravelInformation();
    	
    	model.setCityCode(cityCode);
    	
    	if(!sex.isEmpty()){
    		model.setSex(Integer.parseInt(sex));
    	}else{
    		model.setSex(-1);
    	}
    	if(!work.isEmpty()){
    		model.setProfession(Integer.parseInt(work));
    	}else{
    		model.setProfession(-1);
    	}
    	if(!isLocal.isEmpty()){
    		model.setIsPermanentAddress(Integer.parseInt(isLocal));
    	}else{
    		model.setIsPermanentAddress(-1);
    	}
    	
    	if(!starAge.isEmpty()){
    		model.setStarAge(Integer.parseInt(starAge));
    	}else{
    		model.setStarAge(-1);
    	}
    	if(!endAge.isEmpty()){
    		model.setEndAge(Integer.parseInt(endAge));
    	}else{
    		model.setEndAge(-1);
    	}
    	
    	SimpleDateFormat bf = new SimpleDateFormat("yyyy-MM-dd");
    	if(today.isEmpty()){
    		Date date = new Date();
    	    today = bf.format(date);
    	}
       
    	Date todayDate = bf.parse(today);
    	 
    	Calendar calendar = new GregorianCalendar();
 		calendar.setTime(todayDate);
 		calendar.add(calendar.DATE, 1);// 把日期往后增加一天.整数往后推,负数往前移动
 		Date tomorrowDate = calendar.getTime(); // 这个时间就是日期往后推一天的结果
    	 
    	model.setStartDate(todayDate);
    	 
    	model.setEndDate(tomorrowDate);
    	 
    	 List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
    	 //List<TravelInformation> list = aaService.selectSeMan(parse);
    	 
    	 List<TravelInformation> list = aaService.selectSeMan(model);
    	 
    	 if(list!=null && list.size()>0){
    		 for (int i = 0; i <list.size(); i++) {
				TravelInformation entity = list.get(i);
				
				 Map<String,Object> map= new HashMap<String,Object>();
				 map.put("total", entity.getNumber());
				 map.put("personNumber",entity.getPersonNumber());
				 System.out.println("次数"+entity.getPersonNumber());
			     listResult.add(map);
			}
    		 
    	 }
    	 response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
	
  	   
}
    			
		
		











    			
