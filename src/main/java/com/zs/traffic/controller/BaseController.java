package com.zs.traffic.controller;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.http.HttpServletResponse;

import com.zs.utils.PropertiesLoader;


/**
 * @class name
 * 
 * @author liliang
 * 
 * @description 
 *
 */
public class BaseController {
	
	/**
	 * @desctiption write and response
	 * @param response
	 * @param ret
	 * @throws IOException
	 */
	public void writeRetValue(HttpServletResponse response, String ret) throws IOException{
		if(null == response){
			return;
		}
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain;charset=UTF-8");
		response.getWriter().write(ret);
		response.getWriter().close();
	}
	
	/**
	 * 获取最大值或最小值
	 * @param number 传入的小数
	 * @param str 最大值 :max 最小值:min
	 * @return
	 */
	public static float getMaxOrMin(double number,String  str) {
		float res=0.0f;
		if("max".equals(str)){
			int value = (int) (number * 100);
			if(value % 10 > 0){
				res = (float) (((value + 10) / 10) / 10.0);
			} else {
				res = (float) (value / 100.0);
			}
		}else {
			int value=(int)(number*10);
			res = (float) (value / 10.0);
		}
		return res;
		
	}
	
	/**
	 * 获取配置文件参数
	 * @return Properties
	 */
	public static Properties getProperties(){
		Properties pro = PropertiesLoader.loadJdbcConfig("/conf/config.properties");
		return pro;
	}
}
