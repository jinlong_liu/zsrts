package com.zs.traffic.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.zs.traffic.model.StatisticsParticular;
import com.zs.traffic.service.FamilyDetailsService;
import com.zs.traffic.service.StatisticsParticularService;
import com.zs.utils.FormattingUtil;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;

/**
 * @description 查询基本信息
 * 
 * @author antl
 * 
 * @date 2017-03-30
 *
 */

@Controller
@RequestMapping("/")
public class FamilyDetailsController extends BaseController {

	@Autowired
	public FamilyDetailsService aaService;
	@Autowired
	public StatisticsParticularService auService;

	/**
	 * 查询车辆信息
	 * 
	 */
	@RequestMapping(value = "/getListCar", method = RequestMethod.POST)
	public void getToListCar(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		String idType = request.getParameter("familyId");
		int id = 0;
		String familyId = "";
		if (idType != null) {
			id = Integer.parseInt(idType.trim());
		}
		StatisticsParticular statisticsParticular = auService.getFamilyId(id);
		if(statisticsParticular != null){
		   familyId = statisticsParticular.getFamilyId();
		}
		/* String urlPath = "F:\\wen\\carInfo\\car_"+familyId+".txt"; */

		// System.out.println(familyId);
		/*
		 * String realPath =
		 * request.getSession().getServletContext().getRealPath(request.
		 * getRequestURI()); System.out.println(realPath);
		 */
		/* System.out.println(familyId); */
		String urlPath = getProperties().getProperty("filePathDetail") + cityCode + "/carInfo/car_" + familyId + ".txt";
		String familyDetails = aaService.getFamilyCar(urlPath);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		if (familyDetails != null) {
			JSONArray jsonArry = new JSONArray(familyDetails);// 将前台传过来的json数据转换成json数组
			for (int i = 0; i < jsonArry.length(); i++) {
				JSONObject jsonObject = jsonArry.getJSONObject(i);
				String object = (String) jsonObject.get("车辆车位");
				String object2 = (String) jsonObject.get("车辆属性");
				String displacementType = (String) jsonObject.get("车辆排量");
				String displacement = "";
				String[] splitDisplacement = displacementType.split(":");
				if (splitDisplacement.length > 1) {
					displacement = splitDisplacement[1];
				} else {
					displacement = splitDisplacement[0];
				}
				String formatType = FormattingUtil.FormatType(object2);
				String formatTop = FormattingUtil.FormatTop(object);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("displacement", displacement); // 汽车的排量
				if (cityCode.equals("0371")) {
					map.put("mileage", jsonObject.get("车辆公里") + "万公里"); // 汽车的行驶里程
				} else {
					map.put("mileage", jsonObject.get("车辆公里") + "公里"); // 汽车的行驶里程
				}
				map.put("parkingSpace", formatTop); // 汽车是否有停车位
				map.put("VehiclepProperties", formatType); // 汽车的属性
				listResult.add(map);
			}
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("displacement", "暂无车辆信息"); // 汽车的排量
			map.put("mileage", "暂无车辆信息"); // 汽车的行驶里程
			map.put("parkingSpace", "暂无车辆信息"); // 汽车是否有停车位
			map.put("VehiclepProperties", "暂无车辆信息"); // 汽车的属性
			listResult.add(map);
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	/**
	 * 查询家庭成员信息
	 * 
	 */
	@RequestMapping(value = "/getListHome", method = RequestMethod.POST)
	public void getToListHome(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		String idType = request.getParameter("familyId");
		int id = 0;
		String familyId = "";
		if (idType != null) {
			id = Integer.parseInt(idType.trim());
		}
		StatisticsParticular statisticsParticular = auService.getFamilyId(id);
		if(statisticsParticular != null){
		   familyId = statisticsParticular.getFamilyId();
		}
		String urlPath = getProperties().getProperty("filePathDetail") + cityCode + "/personal/personalInfo_" + familyId
				+ ".txt";
		System.out.println(urlPath);
		String people = aaService.getFaminyHome(urlPath);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		// System.out.println(people);
		if (people != null) {
			JSONArray jsonArry = new JSONArray(people);// 将前台传过来的json数据转换成json数组
			for (int i = 0; i < jsonArry.length(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				JSONObject entity = jsonArry.getJSONObject(i);
				String sexType = (String) entity.get("性别");
				String locaType = (String) entity.get("是否常住本地");
				String workType = (String) entity.get("职业");
				String incoaType = "";
				boolean hass = entity.has("月收入");// 判断是否有地址这个K
				if (hass == true) {
					if (cityCode.equals("0371")) {
						incoaType = (String) entity.get("月收入");
						incoaType = FormattingUtil.FormatMonthlyIncome(incoaType);// 格式化月收入
						map.put("monthlyIncome", incoaType); // 月收入
					} else {
						incoaType = (String) entity.get("月收入");
						incoaType = FormattingUtil.FormatMonthlyIncomeZL(incoaType);// 格式化月收入
						map.put("monthlyIncome", incoaType); // 月收入
					}
				} else {
					map.put("monthlyIncome", incoaType); // 月收入
				}
				boolean has = entity.has("地址");// 判断是否有地址这个K
				String address = "";
				if (has == true) {
					address = (String) entity.get("地址");
					address = address.replace("::", "");
					// System.out.println(addres);
					if (!address.equals("null")) {
						String[] splitAddress = address.split(":");
						address = splitAddress[0];
					}
				}
				if (address.equals("null")) {
					address = "";
				}
				String professional = null;
				if (!workType.equals("null")) {
					String[] splitWork = workType.split(":");
					workType = splitWork[0];
					if (workType.endsWith("7")) {
						if (splitWork.length > 1) {
							map.put("professional", splitWork[1]); // 职业
						} else {
							professional = FormattingUtil.FormatWork(splitWork[0]);// 格式化工作
							map.put("professional", professional); // 职业
						}
					} else {
						professional = FormattingUtil.FormatWork(splitWork[0]);// 格式化工作
						map.put("professional", professional); // 职业
					}
				} else {
					map.put("professional", FormattingUtil.FormatWork(workType)); // 职业
				}
				String sex = FormattingUtil.FormatSex(sexType);// 格式性别

				String local = FormattingUtil.FormatLocal(locaType);
				map.put("sex", sex); // 性别
				map.put("age", entity.get("年龄")); // 年龄
				map.put("local", local); // 居住情况
				map.put("unitAddress", address); // 单位地址
				listResult.add(map);
			}
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("sex", "暂无其他家庭成员");
			listResult.add(map);
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	/**
	 * 查询家庭信息
	 * 
	 */
	@RequestMapping(value = "/getListfamily", method = RequestMethod.POST)
	public void getToListFa(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		// System.out.println(familyId);
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		String idType = request.getParameter("familyId");
		int id = 0;
		String familyId = "";
		if (idType != null) {
			id = Integer.parseInt(idType.trim());
		}
		StatisticsParticular statisticsParticular = auService.getFamilyId(id);
		if(statisticsParticular != null){
		   familyId = statisticsParticular.getFamilyId();
		}
		/* String urlPath = "F:\\wen\\address\\base_"+familyId+".txt"; */
		String urlPath = getProperties().getProperty("filePathDetail") + cityCode + "/address/base_" + familyId
				+ ".txt";
		String faminy = aaService.getFaminy(urlPath);
		/* System.out.println(urlPath); */
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		if (faminy != null) {
			JSONArray jsonArry = new JSONArray(faminy);// 将查出来的json数据转换成json数组
			/* if(jsonArry.length()>1){ */
			for (int k = 0; k < jsonArry.length(); k++) {
				Map<String, Object> map = new HashMap<String, Object>();
				JSONObject entity = jsonArry.getJSONObject(k);
				boolean has = entity.has("家庭地址");// 判断是否有家庭住址这个K
				if (has == true) {
					String homeAddress = (String) entity.get("家庭地址");
					String[] splitAddress = homeAddress.split(",");
					homeAddress = splitAddress[0];
					map.put("homeAddress", homeAddress);
					listResult.add(map);
				} else {
					map.put("homeAddress", "1");
					listResult.add(map);
				}
			}
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("homeAddress", "1");
			listResult.add(map);

		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	/**
	 * 查询出行信息
	 * 
	 * @throws ParseException
	 * 
	 */
	@RequestMapping(value = "/getInformation", method = RequestMethod.POST)
	public void getToListInformation(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ParseException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		String idType = request.getParameter("familyId");
		int id = 0;
		String familyId = "";
		if (idType != null) {
			id = Integer.parseInt(idType.trim());
		}
		StatisticsParticular statisticsParticular = auService.getFamilyId(id);
		if(statisticsParticular != null){
		   familyId = statisticsParticular.getFamilyId();
		}
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		// 查出共有多少个家庭成员
		int len = 0;
		String path = "";

		/*
		 * String urlPathh = "F:\\wen\\personal\\personalInfo_"+familyId+".txt";
		 */

		String urlPathh = getProperties().getProperty("filePathDetail") + cityCode + "/personal/personalInfo_"
				+ familyId + ".txt";

		String people = aaService.getFaminyHome(urlPathh);
		if (people != null) {
			JSONArray jsonArry = new JSONArray(people);
			len = jsonArry.length();
		}
		if (len == 0) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("name", "");
			listResult.add(map);
		}
		for (int i = 0; i < len; i++) {
			/* path = "F:\\wen\\outInfo\\outInfo_"+(i+1)+"_"+familyId+".txt"; */
			path = getProperties().getProperty("filePathDetail") + cityCode + "/outInfo/outInfo_" + (i + 1) + "_"
					+ familyId + ".txt";
			System.out.println(path);
			String Information = aaService.getInformation(path);
			if (Information != null) {
				JSONArray jsonArry = new JSONArray(Information);// 将查出来的json数据转换成json数组
				/* if(jsonArry.length()>1){ */
				for (int k = 0; k < jsonArry.length(); k++) {
					Map<String, Object> map = new HashMap<String, Object>();
					JSONObject entity = jsonArry.getJSONObject(k);
					boolean note = entity.has("未出行信息注明");// 判断是否有接送小孩这个K
					if (note == true) {
						map.put("name", i + 1);
						map.put("noTravel", "备注");
						map.put("note", entity.get("未出行信息注明"));
						listResult.add(map);
					} else {
						String starAddres = (String) entity.get("出发地点");
						String starTim = (String) entity.get("出发时间");
						String transpor = (String) entity.get("交通方式");
						String endAddres = (String) entity.get("到达地点");
						String purpose = (String) entity.get("出行目的");
						String endTim = (String) entity.get("到达时间");
						map.put("noTravel", "");
						boolean has = entity.has("是否接送小孩");// 判断是否有接送小孩这个K
						String child = "1";
						if (has == true) {
							String chil = (String) entity.get("是否接送小孩");
							// 截取字符串接送小孩
							String chi = chil;
							if (chil.length() > 1 && chil.indexOf(":") != -1) {
								chi = chil.substring(0, chil.indexOf(":"));
							} else {
								child = "1";
							}
							// 是否接送小孩
							child = FormattingUtil.child(chi);
							if (child != null) {
								if (child.equals("送小孩")) {
									String chilaa = chil.substring(2, chil.length());
									String[] splitAddress = chilaa.split(":");
									chilaa = splitAddress[0];
									map.put("childAddress", "送小孩地址：" + chilaa);
								} else if (child.equals("接小孩")) {
									String chilaa = chil.substring(2, chil.length());
									String[] splitAddress = chilaa.split(":");
									chilaa = splitAddress[0];
									map.put("childAddress", "接小孩地址：" + chilaa);
								}
							}
						}
						// 截取出发时间
						String starTi = starTim.substring(0, starTim.indexOf(":"));
						String starTime = starTim.substring(2, starTim.length());

						if (starTi.equals("0")) {
							String[] splitStarTime = starTime.split(":");
							starTime = splitStarTime[2] + " 上午 " + splitStarTime[0] + "时 " + splitStarTime[1] + "分";
							map.put("starTime", starTime);

						} else if (starTi.equals("1")) {
							String[] splitStarTime = starTime.split(":");
							starTime = splitStarTime[2] + " 下午 " + splitStarTime[0] + "时 " + splitStarTime[1] + "分";
							map.put("starTime", starTime);
						}
						// 截取字符串结束时间
						String endTi = endTim.substring(0, endTim.indexOf(":"));
						String endTime = endTim.substring(2, endTim.length());
						if (endTi.equals("0")) {
							String[] splitEndTime = endTime.split(":");
							endTime = splitEndTime[2] + " 上午 " + splitEndTime[0] + "时 " + splitEndTime[1] + "分";
							map.put("endTime", endTime);
						} else if (endTi.equals("1")) {
							String[] splitEndTime = endTime.split(":");
							endTime = splitEndTime[2] + " 下午 " + splitEndTime[0] + "时 " + splitEndTime[1] + "分";
							map.put("endTime", endTime);
						}

						// 截取字符串出发地点
						String addr = starAddres;
						if (starAddres.length() > 1 && starAddres.indexOf(":") != -1) {
							addr = starAddres.substring(0, starAddres.indexOf(":"));
						}

						if (addr.equals("0")) {
							map.put("starAddress", "家");
						} else if (addr.equals("1")) {
							String Faminy = aaService.getFaminyHome(urlPathh);
							JSONObject jsonn;
							String addree = null;
							if (Faminy != null) {
								JSONArray json = new JSONArray(Faminy);// 将查出来的json数据转换成json数组
								if (json.length() > 1) {
									jsonn = json.getJSONObject(i);
									boolean addressKey = jsonn.has("地址");// 判断是否有地址这个K
									if (addressKey == true) {
										addree = (String) jsonn.get("地址");
										String[] splitAddress = addree.split(":");
										addree = splitAddress[0];
										map.put("starAddress", addree);
									} else {
										map.put("starAddress", "");
									}
								} else {
									jsonn = json.getJSONObject(0);
									boolean addressKey = jsonn.has("地址");// 判断是否有地址这
									if (addressKey == true) {
										addree = (String) jsonn.get("地址");
										String[] splitAddress = addree.split(":");
										addree = splitAddress[0];
										map.put("starAddress", addree);
									} else {
										map.put("starAddress", "");
									}
								}
							} else {
								map.put("starAddress", "");
							}
						} else if (addr.equals("2")) {
							String addree = starAddres.substring(2, starAddres.length());
							String[] splitAddress = addree.split(":");
							addree = splitAddress[0];
							map.put("starAddress", addree);
						}
						// 截取到达地点进行判断
						String endAddre = endAddres;
						if (endAddres.length() > 1 && endAddres.indexOf(":") != -1) {
							endAddre = endAddres.substring(0, endAddres.indexOf(":"));
						}
						if (endAddre.equals("0")) {
							map.put("endAddress", "家");
						} else if (endAddre.equals("1")) {
							String Faminy = aaService.getFaminyHome(urlPathh);
							JSONObject jsonn;
							String addree = null;
							if (Faminy != null) {
								JSONArray json = new JSONArray(Faminy);// 将查出来的json数据转换成json数组
								if (json.length() > 1) {
									jsonn = json.getJSONObject(i);
									boolean addressKey = jsonn.has("地址");// 判断是否有地址这个K
									if (addressKey == true) {
										addree = (String) jsonn.get("地址");
										String[] splitAddress = addree.split(":");
										addree = splitAddress[0];
										map.put("endAddress", addree);
									} else {
										map.put("endAddress", "");
									}
								} else {
									jsonn = json.getJSONObject(0);
									boolean addressKey = jsonn.has("地址");// 判断是否有地址这k
									if (addressKey == true) {
										addree = (String) jsonn.get("地址");
										String[] splitAddress = addree.split(":");
										addree = splitAddress[0];
										map.put("endAddress", addree);
									} else {
										map.put("endAddress", "");
									}
								}
							} else {
								map.put("endAddress", "");
							}
						} else if (endAddre.equals("2")) {
							String endaddree = endAddres.substring(2, endAddres.length());
							String[] splitAddress = endaddree.split(":");
							endaddree = splitAddress[0];
							map.put("endAddress", endaddree);
						}
						String[] splitTranspor = transpor.split(":");// 交通方式
						String wxtripMode = "";
						if (splitTranspor[0].equals("17")) {
							wxtripMode = "其他-" + splitTranspor[1];
						} else {
							if (!cityCode.equals("0539")) {
								wxtripMode = FormattingUtil.WxtripMode(transpor);// 格式化交通方式
							} else {
								wxtripMode = FormattingUtil.WxtripModeOne(transpor);// 格式化交通方式
							}
						}
						String purposee = "";
						String[] splitPurpose = purpose.split(":");// 出行目的
						String Purpose = splitPurpose[0];
						if (Purpose.equals("7")) {
							purposee = "其他-" + splitPurpose[1];
						} else {
							purposee = FormattingUtil.purpose(purpose);// 格式化出行目的
						}
						map.put("transpor", wxtripMode);// 出行的方式
						map.put("purposee", purposee);// 出行目的
						map.put("child", child);// 是否接送小孩
						map.put("name", i + 1);
						listResult.add(map);
					}
				}
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("name", i + 1);
				map.put("endTime", "");
				map.put("starAddress", "");
				map.put("transpor", "");// 出行的方式
				map.put("purposee", "");// 出行目的
				map.put("child", "");// 是否接送小孩
				map.put("endAddress", "");
				map.put("starTime", "");
				listResult.add(map);
			}

		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 问卷审核
	@RequestMapping(value = "/getaudit", method = RequestMethod.POST)
	public void getaudit(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		String idType = request.getParameter("familyId");
		int id = 0;
		String familyId = "";
		if (idType != null) {
			id = Integer.parseInt(idType.trim());
		}
		StatisticsParticular statisticsParticulars = auService.getFamilyId(id);
		if(statisticsParticulars != null){
		   familyId = statisticsParticulars.getFamilyId();
		}
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		List<StatisticsParticular> list = auService.getaudit(familyId);
		if (list != null && list.size() != 0) {
			StatisticsParticular statisticsParticular = list.get(0);
			if (statisticsParticular != null) {
				for (int i = 0; i < list.size(); i++) {
					Map<String, Object> map = new HashMap<String, Object>();
					StatisticsParticular entity = list.get(i);
					Integer status = entity.getStatus();
					if (status == -1) {
						map.put("status", "未审核");
					} else if (status == 0) {
						map.put("status", "不通过");
					} else if (status == 1) {
						map.put("status", "已通过");
					} else {
						map.put("status", "未审核");
					}
					listResult.add(map);
				}
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("status", "");
				listResult.add(map);
			}
			// 问卷填写进度
			StatisticsParticular entity = auService.getProgress(familyId);
			if (entity != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				Integer submitStage = entity.getSubmitStage();
				if (submitStage != null) {
					if (submitStage == 7) {
						map.put("back", "已完成");
						listResult.add(map);
					} else {
						map.put("back", "未完成");
						listResult.add(map);
					}
				} else {
					map.put("back", "");
					listResult.add(map);
				}
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("back", "");
				listResult.add(map);
			}
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
		}
	}

	@RequestMapping(value = "/getListWinning ", method = RequestMethod.POST)
	public void getListWinning(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		// System.out.println(familyId);
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");
		Integer userId = (Integer) session.getAttribute("userId");// 获取用户id
		String authDegrees = (String) session.getAttribute("authDegree");// 获取用户权限
		String userName = (String) session.getAttribute("userName");// 获取用户名
		String authDegree = "";
		String[] array = null;
		if (authDegrees != null) {
			array = authDegrees.split(",");
			Boolean flag = true;
			for (String auth : array) {
				if (auth.equals("3") || auth.equals("4") || auth.equals("5") || auth.equals("6") || auth.equals("7")) {
					authDegree = auth;
				}
			}
		}
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		if (authDegree.equals("3")) {
			String idType = request.getParameter("familyId");
			int id = 0;
			String familyId = "";
			if (idType != null) {
				id = Integer.parseInt(idType.trim());
			}
			StatisticsParticular statisticsParticular = auService.getFamilyId(id);
			if(statisticsParticular != null){
			   familyId = statisticsParticular.getFamilyId();
			}
			/* String urlPath = "F:\\wen\\reward\\_"+familyId+".txt"; */
			String urlPath = getProperties().getProperty("filePathDetail") + cityCode + "/reward/reward_" + familyId
					+ ".txt";
			String faminy = aaService.getFaminy(urlPath);
			if (faminy != null) {
				JSONArray jsonArry = new JSONArray(faminy);// 将查出来的json数据转换成json数组
				Map<String, Object> map = new HashMap<String, Object>();
				/* if(jsonArry.length()>1){ */
				for (int k = 0; k < jsonArry.length(); k++) {
					JSONObject entity = jsonArry.getJSONObject(k);
					map.put("cityCode", cityCode);// 城市编码
					map.put("recipientAddress", entity.get("收件人地址"));
					map.put("recipientName", entity.get("收件人姓名"));
					map.put("recipientPhone", entity.get("收件人电话"));
					listResult.add(map);
				}
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("cityCode", cityCode);// 城市编码
				map.put("recipientAddress", "");
				map.put("recipientName", "");
				map.put("recipientPhone", "");
				listResult.add(map);
			}
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("recipientAddress", "没有权限");
			map.put("cityCode", cityCode);// 城市编码
			listResult.add(map);
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 问卷审核
	@RequestMapping(value = "/questionnaireReview", method = RequestMethod.POST)
	public void questionnaireReview(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		Integer isSuperAdmin = (Integer) session.getAttribute("isSuperAdmin");// 获取用户是否为超级管理员
		if (isSuperAdmin == null) {
			isSuperAdmin = 0;
		}
		Integer userId = (Integer) session.getAttribute("userId");// 获取用户id
		String authDegrees = (String) session.getAttribute("authDegree");// 获取用户权限
		String userName = (String) session.getAttribute("userName");// 获取用户名
		String authDegree = "";
		String[] array = null;
		String idType = request.getParameter("familyId");
		int id = 0;
		String familyId = "";
		if (idType != null) {
			id = Integer.parseInt(idType.trim());
		}
		StatisticsParticular statisticsParticular = auService.getFamilyId(id);
		if(statisticsParticular != null){
		   familyId = statisticsParticular.getFamilyId();
		}
		String audit = request.getParameter("audit");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Boolean flag = false;
		if (authDegrees != null) {
			array = authDegrees.split(",");
			for (String auth : array) {
				if (auth.equals("3")) {
					flag = true;
					break;
				}
				/*
				 * if(auth.equals("3")||auth.equals("4")||auth.equals("5")||auth
				 * .equals("6")||auth.equals("7")||auth.equals("10")){
				 * authDegree = auth; }
				 */
			}
		}
		if (isSuperAdmin == 1) {
			if (familyId != null && audit != null && !familyId.equals("") && !audit.equals("")) {
				int auditQuestionnaire = auService.auditQuestionnaire(familyId, Integer.parseInt(audit));
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("back", auditQuestionnaire);
				listResult.add(map);
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("back", "error");
				listResult.add(map);
			}
		} else {
			List<StatisticsParticular> list = auService.getaudit(familyId);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					StatisticsParticular entity = list.get(i);
					Integer status = entity.getStatus();
					if (status == -1) {
						if (familyId != null && audit != null && !familyId.equals("") && !audit.equals("")) {
							int auditQuestionnaire = auService.auditQuestionnaire(familyId, Integer.parseInt(audit));
							Map<String, Object> map = new HashMap<String, Object>();
							map.put("back", auditQuestionnaire);
							listResult.add(map);
						} else {
							Map<String, Object> map = new HashMap<String, Object>();
							map.put("back", "error");
							listResult.add(map);
						}
					} else {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("back", "没有权限");
						listResult.add(map);
					}
				}
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
}
