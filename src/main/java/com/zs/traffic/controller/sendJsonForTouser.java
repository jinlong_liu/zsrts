package com.zs.traffic.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import net.sf.json.JSONObject;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.zs.traffic.model.StatisticsParticular;
import com.zs.traffic.service.StatisticsParticularService;
import com.zs.traffic.wx.model.event.AccessToken;

public class sendJsonForTouser extends BaseController {
	public static final String appID = getProperties().getProperty("appID");
	public static final String appScret = getProperties().getProperty("appScret");
	public static int num = 0;
	@Autowired
	public StatisticsParticularService statisticsParticularService;
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(sendJsonForTouser.class);
	
	public static AccessToken token = null;
	/**
	 * 48小时内定时给用户推送消息
	 * 
	 * @param request
	 * @param response
	 * @throws ParseException
	 * @throws IOException
	 * @throws ServletException
	 * @autho wanglijian
	 * @date 20170516
	 */
	public void sendTextForTouser() throws ParseException {
		//从全局变量中拿到asstoken
		String accessToken = WxQuestionnaireController.token.getAccess_token();
		//this.getAccessToken(appID, appScret);
        System.out.println("进入微信推送消息定时器！");	
        logger.info("进入微信推送消息定时器！");
        //需要添加cityCode
        
		List<StatisticsParticular> sp = statisticsParticularService.selectAll();
		String content = "";
		// AccessToken access = AccessTokenUtil.getAccessToken(appID, appScret);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		Date dateNow = new Date();// new Date()为获取当前系统时间
		// String accessToken = access.getAccess_token();
		// 循环判断是否有未完成提交的问卷
		if(sp!=null){
		for (int i = 0; i < sp.size(); i++) {
			String originalDate = sp.get(i).getFollowDate();
			if (originalDate != null && !"".equals(originalDate)) {
			Date followDate = df.parse(originalDate);
			// 调查员代填写的关注时间为空，不进行推送		
				long diff = dateNow.getTime() - followDate.getTime();
				String openId = sp.get(i).getSubmitter();
				int sign = sp.get(i).getRemindNumber();
				String city = sp.get(i).getCtiyCode();
				if("0371".equals(city)){
					content = "您还未完成全部问卷的提交，填写完整后可获得礼品并参与抽奖。";
				}else{
					content = "您的问卷还未提交，请填写完问卷后提交问卷。";
				}
				logger.info("进入微信端主动推送!");
				// 使用时间计算保证更加精确
				int min = (int) diff / (60 * 1000);
				if (min > (2 * 60) && sign == 0) {
					// 发送奖励信息给用户
					System.out.println(accessToken);
					String tokenurl = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token="
							+ accessToken;
					// 发送文字消息，无连接
					String jsontext = "{\"touser\":\"" + openId + "\",\"msgtype\":\"text\",\"text\":{\"content\":\""
							+ content + "\"}}";
					// String jsontext =
					// "{\"touser\":\"or5UjxNWEpTofPg7SJb-CA2rGexk\",\"msgtype\":\"text\",\"text\":{\"content\":\""+content+"\"}}";
					System.out.println("这里是json" + jsontext);
					// 请求方法，然后放回OK 成功，否则错误。这里这个请求方法在下边
					try {
						String resultWe = this.sendPost(tokenurl, jsontext);
						sp.get(i).setRemindNumber(1);
						statisticsParticularService.updateByPrimaryKeySelective(sp.get(i));
						System.out.println(resultWe);
					} catch (Exception e) {
						System.out.println("微信端主动推送消息失败!");
						logger.info("微信端主动推送消息失败!");
						e.printStackTrace();
					}
				} else if (min > (11 * 60) && sign == 1) {
					// 发送奖励信息给用户
					System.out.println(accessToken);
					String tokenurl = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token="
							+ accessToken;
					// 发送文字消息，无连接
					String jsontext = "{\"touser\":\"" + openId + "\",\"msgtype\":\"text\",\"text\":{\"content\":\""
							+ content + "\"}}";
					// String jsontext =
					// "{\"touser\":\"or5UjxNWEpTofPg7SJb-CA2rGexk\",\"msgtype\":\"text\",\"text\":{\"content\":\""+content+"\"}}";
					System.out.println("这里是json" + jsontext);
					// 请求方法，然后放回OK 成功，否则错误。这里这个请求方法在下边
					try {
						String resultWe = this.sendPost(tokenurl, jsontext);
						sp.get(i).setRemindNumber(2);
						statisticsParticularService.updateByPrimaryKeySelective(sp.get(i));
						System.out.println(resultWe);
					} catch (Exception e) {
						System.out.println("微信端主动推送消息失败!");
						e.printStackTrace();
					}
				} else if (min > (23 * 60) && sign == 2) {
					// 发送奖励信息给用户
					System.out.println(accessToken);
					String tokenurl = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token="
							+ accessToken;
					// 发送文字消息，无连接
					String jsontext = "{\"touser\":\"" + openId + "\",\"msgtype\":\"text\",\"text\":{\"content\":\""
							+ content + "\"}}";
					// String jsontext =
					// "{\"touser\":\"or5UjxNWEpTofPg7SJb-CA2rGexk\",\"msgtype\":\"text\",\"text\":{\"content\":\""+content+"\"}}";
					System.out.println("这里是json" + jsontext);
					// 请求方法，然后放回OK 成功，否则错误。这里这个请求方法在下边
					try {
						String resultWe = this.sendPost(tokenurl, jsontext);
						sp.get(i).setRemindNumber(3);
						statisticsParticularService.updateByPrimaryKeySelective(sp.get(i));
						System.out.println(resultWe);
					} catch (Exception e) {
						System.out.println("微信端主动推送消息失败!");
						e.printStackTrace();
					}
				} else if (min > (47 * 60) && sign == 3) {
					// 发送奖励信息给用户
					System.out.println(accessToken);
					String tokenurl = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token="
							+ accessToken;
					// 发送文字消息，无连接
					String jsontext = "{\"touser\":\"" + openId + "\",\"msgtype\":\"text\",\"text\":{\"content\":\""
							+ content + "\"}}";
					// String jsontext =
					// "{\"touser\":\"or5UjxNWEpTofPg7SJb-CA2rGexk\",\"msgtype\":\"text\",\"text\":{\"content\":\""+content+"\"}}";
					System.out.println("这里是json" + jsontext);
					// 请求方法，然后放回OK 成功，否则错误。这里这个请求方法在下边
					try {
						String resultWe = this.sendPost(tokenurl, jsontext);
						sp.get(i).setRemindNumber(4);
						statisticsParticularService.updateByPrimaryKeySelective(sp.get(i));
						System.out.println(resultWe);
					} catch (Exception e) {
						System.out.println("微信端主动推送消息失败!");
						e.printStackTrace();
					}

				} else {
					logger.info("进入定时器，但未发现符合条件的openId");
					//System.out.println("进入定时器，但未发现符合条件的openId");
				}
			}else{
				logger.info("进入定时器，查询到符合条件的信息，但关注时间为空，即调查员代填用户！");
				//System.out.println("进入定时器，查询到符合条件的信息，但关注时间为空，即调查员代填用户！");
			}
		}
	}else{
		logger.info("进入定时器，但未发现符合条件的List集合。");
	}
	}

	/**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param param请求参数，请求参数应该是
	 *            name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 * @auoth wanglijan date 20170509
	 */
	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("user-agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			OutputStreamWriter outWriter = new OutputStreamWriter(conn.getOutputStream(), "utf-8");
			out = new PrintWriter(outWriter);
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 获取accessToken
	 * 
	 * @param appID
	 *            微信公众号凭证
	 * @param appScret
	 *            微信公众号凭证秘钥
	 * @return
	 */
	public static void getAccessToken(String appID, String appScret) {
		// AccessToken token = new AccessToken();
		// 访问微信服务器
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appID + "&secret="
				+ appScret;
		try {
			URL getUrl = new URL(url);
			HttpURLConnection http = (HttpURLConnection) getUrl.openConnection();
			http.setRequestMethod("GET");
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);

			http.connect();
			InputStream is = http.getInputStream();
			int size = is.available();
			byte[] b = new byte[size];
			is.read(b);
			String message = new String(b, "UTF-8");
			JSONObject json = JSONObject.fromObject(message);
			//accessToken = json.getString("access_token");
			//System.out.println("提醒用户未完成问卷,accessToken为："+accessToken);
			// token.setExpires_in(new Integer(json.getString("expires_in")));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println("出现异常，试图重新换取token！第" + num + 1 + "次");
			if (num != 3) {
				getAccessToken(appID, appScret);
			}
			num++;
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("出现异常，试图重新换取token！第" + num + 1 + "次");
			if (num != 3) {
				getAccessToken(appID, appScret);
			}
			num++;
		}
		// return token;
	}

}
