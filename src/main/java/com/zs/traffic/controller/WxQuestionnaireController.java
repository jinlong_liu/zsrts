package com.zs.traffic.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.traffic.model.Area;
import com.zs.traffic.model.BusInfo;
import com.zs.traffic.model.CityCodeUrl;
import com.zs.traffic.model.OutKilometreScene;
import com.zs.traffic.model.StatisticsParticular;
import com.zs.traffic.model.TransportationTime;
import com.zs.traffic.model.WechatApp;
import com.zs.traffic.service.AreaService;
import com.zs.traffic.service.BusInfoService;
import com.zs.traffic.service.CityCodeUrlService;
import com.zs.traffic.service.OutKilometreSceneService;
import com.zs.traffic.service.StatisticsParticularService;
import com.zs.traffic.service.TransportationTimeService;
import com.zs.traffic.service.WechartInfoService;
import com.zs.traffic.service.WechatAppService;
import com.zs.traffic.wx.model.event.AccessToken;
import com.zs.utils.AccessTokenUtil;
import com.zs.utils.DeleteFileUtils;
import com.zs.utils.FormattingUtil;
import com.zs.utils.HttpRequestUtil;
import com.zs.utils.JDBCUtils;
import com.zs.utils.ReadFromFile;
import com.zs.utils.Singleton;
import com.zs.utils.TxtUtil;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZipUtils;
import com.zs.utils.ZsJsonUtil;

@SuppressWarnings("deprecation")
@Controller
@RequestMapping("/")
public class WxQuestionnaireController extends BaseController implements Runnable {
	@Autowired
	public BusInfoService busInfoService;
	@Autowired
	public WechartInfoService wechartInfoService;
	@Autowired
	public StatisticsParticularService statisticsParticularService;
	@Autowired
	public WechatAppService wechatAppService;
	@Autowired
	public AreaService areaService;
	@Autowired
	public TransportationTimeService transportationTimeService;
	@Autowired
	public OutKilometreSceneService outKilometreSceneService;
	@Autowired
	public CityCodeUrlService cityCodeUrlService;

	// 微信公众号的凭证和秘钥wanglijian2017.03.25
	// public static final String appID = "wx8ebd582644d0bb29";
	// public static final String appScret = "9bf0522749a796aece4e241f79748a5f";
	public static final String appID = getProperties().getProperty("appID");
	public static final String appScret = getProperties().getProperty("appScret");
	// 生产环境公众号微信
	// public static final String appID = "wx1c6fb297bf4bf502";
	// public static final String appScret = "f5304b500937d1596f616d9facabe22a";
	public static AccessToken token = null;
	
	private static BufferedReader reader;
	/**
	 * 读取文件所有行
	 * @param filePath
	 * @return
	 */
	public static String getStringFromFile(String filePath) {
		File file = new File(filePath);
		StringBuffer sb = new StringBuffer("");
		String tempString = null;
		String result = "";
		
		if (!file.exists()) {
			return result;
		}
		
        try {
			reader = new BufferedReader(new FileReader(file));
			while ((tempString = reader.readLine()) != null) {
				sb.append(tempString);
			}
			
			result = sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return result;
	}
	
	
	
	public  void getHSSFWorkbook(String fileName, String sheetName, String[] title, String[][] values, HSSFWorkbook wb) {
		int num = 0;
		// 第一步，创建一个webbook，对应一个Excel文件
		if (wb == null) {
			wb = new HSSFWorkbook();
		}
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		HSSFSheet sheet = wb.createSheet("sheetName");

		// 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
		HSSFRow row = sheet.createRow(0);
		
		
		// 第四步，创建单元格，并设置值表头 设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

		HSSFCell cell = null;
		// 创建标题
		for (int i = 0; i < title.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(title[i]);
			cell.setCellStyle(style);
		}
		// 创建内容
		for (int i = 0; i < values.length; i++) {
			if(num>60000){
				break;
			}
				row = sheet.createRow(i + 1);	
			for (int j = 0; j < values[i].length; j++) {				
					row.createCell(j).setCellValue(values[i][j]);			
			}
			num++;
		}

		try {
			OutputStream os = new FileOutputStream(fileName);
			wb.write(os);
			os.flush();  
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}   
	}
	
	
	
	/**
	 * 反向获取时间信息
	 * 
	 * @auoth wanglijan
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 *             date 20170512
	 * @throws ParseException
	 */
	@RequestMapping(value = "/getTimeWord")
	public void getTimeWord(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException, ParseException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		String path = "C:/Users/zskj_003/Desktop/微信问卷文件备份20170712/0371/time2";
		File file = new File(path);
		if(file.exists()) {
			List<Map<String, String>> listResult = new ArrayList<Map<String,String>>();
			File[] files = file.listFiles();  //返回目录中的文件 或者文件夹
			for (File f : files) {
				if (f.getName().endsWith(".txt")) {
					String familyNum = f.getName().replace("time_", "").replace(".txt", "");
					//读取文件
					List<Map<?, ?>> list = ZsJsonUtil.json2MapOfArrayList(getStringFromFile(f.getPath()));
					
					//根据每一个参数查询公里数和情景数以及交通方式
					String thatTime = "";
					String outType = null;
					String time = null;
					String price = null;
					String beforeTime = null;
					String waitTime = null;
					String rideTime = null;
									
					if (null != list && list.size() > 0) {
						for (int i = 0; i < list.size(); i++) {
							

							if (null != list.get(i).get("哪次出行")) {
								thatTime = list.get(i).get("哪次出行").toString();
							}
							if (null != list.get(i).get("出行方式")) {
								outType = list.get(i).get("出行方式").toString();
							}
							
							
							//公交和地铁
							if("1".equals(outType)||"2".equals(outType)){
								if (null != list.get(i).get("时间")) {
									time = list.get(i).get("时间").toString().replace("分钟", "");
								}
								if (null != list.get(i).get("票价")) {
									price = list.get(i).get("票价").toString().replace("元", "");
									price = price.replace(".0", "");
								}
								if (null != list.get(i).get("走到站时间")) {
									beforeTime = list.get(i).get("走到站时间").toString().replace("分钟", "");
								}
								if (null != list.get(i).get("等车时间")) {
									waitTime = list.get(i).get("等车时间").toString().replace("分钟", "");
								}
								if (null != list.get(i).get("乘车时间")) {
									rideTime = list.get(i).get("乘车时间").toString().replace("分钟", "");
								}
							}
							
							//小汽车
							if("3".equals(outType)){
								if (null != list.get(i).get("时间")) {
									time = list.get(i).get("时间").toString().replace("分钟", "");
								}
								if (null != list.get(i).get("费用")) {
									price = list.get(i).get("费用").toString().replace("元", "");
								}
								if (null != list.get(i).get("运行费用")) {
									beforeTime = list.get(i).get("运行费用").toString().replace("元", "");
								}
								if (null != list.get(i).get("停车费用")) {
									waitTime = list.get(i).get("停车费用").toString().replace("元", "");
								}
							}
							
							//出租车
							if("4".equals(outType)){
								if (null != list.get(i).get("乘车时间")) {
									time = list.get(i).get("乘车时间").toString().replace("分钟", "");
								}
								if (null != list.get(i).get("费用")) {
									price = list.get(i).get("费用").toString().replace("元", "");
								}
							}
							
							
							//自行车
							if("5".equals(outType)){
								if (null != list.get(i).get("时间")) {
									time = list.get(i).get("时间").toString().replace("分钟", "");
								}
							}
							
							//电动车
							if("6".equals(outType)){
								if (null != list.get(i).get("时间")) {
									time = list.get(i).get("时间").toString().replace("分钟", "");
								}
							}
							
						}
					
					List<OutKilometreScene> outList = null;
					if("1".equals(outType)||"2".equals(outType)){
						outList = outKilometreSceneService.selectByTimeWord1(time, beforeTime, waitTime, rideTime, price, outType);
					}else if("3".equals(outType)){
						outList = outKilometreSceneService.selectByTimeWord2(time, price, beforeTime, waitTime, outType);
					}else if("4".equals(outType)){
						outList = outKilometreSceneService.selectByTimeWord3(time, price, outType);
						if(outList==null||outList.size()==0){
							outList = outKilometreSceneService.selectByTimeWord31(time, price, outType);
						}
					}else if("5".equals(outType)){
						outList = outKilometreSceneService.selectByTimeWord4(time, outType);
					}else if("6".equals(outType)){
						outList = outKilometreSceneService.selectByTimeWord5(time, outType);
					}
					
		
					
					System.out.println("============================="+familyNum);
					
					
					Map<String, String> map = new HashMap<String, String>();
					
					if(outList.size()>0){
					String kilometre = outList.get(0).getKilometre();
					String scene = outList.get(0).getScene();
					String transportation = outList.get(0).getTransportation();
					

					List<TransportationTime> tst = transportationTimeService.selectOne(kilometre);
					
					
					biubiu:for(int z=0;z<tst.size();z++){
					String st = tst.get(z).getTimeStart();
					String sh = tst.get(z).getTimeEnd();
					int stNum = 0;
					int shNum = 0;
					
					if("".equals(st)||st==null){
						stNum = 0;
					}else{
						stNum = Integer.valueOf(st);
					}
					
					if("".equals(sh)||sh==null){
						shNum = 1000000;
					}else{
						shNum = Integer.valueOf(sh);
					}
					
					
					List<Map<String, Object>> timelList1 = null;
					String fileNameOne = "C:/Users/zskj_003/Desktop/微信问卷文件备份20170712/0371/outInfo";
					String fileNameTwo = fileNameOne+"/outInfo_1_" + familyNum + ".txt";
					List<String> list1 = ReadFromFile.readFileByLines(fileNameTwo);
					List<Map<String, Object>> outList1 = timelList1 = new ArrayList<Map<String, Object>>();
					if (list1.size() > 0) {
						String outinfo = "";
						outList1 = new ArrayList<Map<String, Object>>();
						// 将数据放到List<Map<String,Object>>里面

						for (int i = 0; i < list1.size(); i++) {
							outinfo = list1.get(i);
						}
						// 读取json数据
						JSONArray jsonArr = JSONArray.fromObject(outinfo);
						List<Map<String, Object>> listforjson = new ArrayList<Map<String, Object>>();
						Iterator<JSONObject> it = jsonArr.iterator();
						while (it.hasNext()) {
							JSONObject json2 = it.next();
							listforjson.add(parseJSON2Map(json2.toString()));
						}
					
					
					String transportationTxt = "";
					String outTime = "";
					String getTime = "";
					String getday = "";
					String gethour = "";
					String getmin = "";
					String outTimeDate = "";
					String outday = "";
					String outhour = "";
					String outmin = "";
					String getTimeDate = "";
					// 对比每一次出行
					bibi:for (int i = 0; i < listforjson.size(); i++) {
						
								transportation =  (String) listforjson.get(i).get("交通方式");
								outTime =  (String) listforjson.get(i).get("出发时间");
								getTime =  (String) listforjson.get(i).get("到达时间");
							
								// 得到出发时间和到达时间
							    if(outTime!=""&&getTime!=""&&outTime!=null&&getTime!=null){
								if (outTime.indexOf(":") != -1) {
									String[] outTimes = outTime.split(":");
										outday = outTimes[3];
										outhour = String.format("%02d", Integer.valueOf(outTimes[1]));
										outmin = String.format("%02d", Integer.valueOf(outTimes[2]));

								}
							    
								outTimeDate = outday + " " + outhour + ":" + outmin + ":00";

							
								
								if (transportation == "0" || "0".equalsIgnoreCase(transportation) || transportation == "17"
										|| "17".equalsIgnoreCase(transportation)) {
									transportation = "1";
								} else if (transportation == "1" || "1".equalsIgnoreCase(transportation)) {
									transportation = "3";
								} else if (transportation == "2" || "2".equalsIgnoreCase(transportation)
										|| transportation == "3" || "3".equalsIgnoreCase(transportation)) {
									transportation = "2";
								} else if (transportation == "5" || "5".equalsIgnoreCase(transportation)) {
									transportation = "4";
								} else if (transportation == "4" || "4".equalsIgnoreCase(transportation)
										|| transportation == "6" || "6".equalsIgnoreCase(transportation)
										|| transportation == "7" || "7".equalsIgnoreCase(transportation)) {
									transportation = "5";
								} else if (transportation == "13" || "13".equalsIgnoreCase(transportation)
										|| transportation == "12" || "12".equalsIgnoreCase(transportation)) {
									transportation = "6";
								} else if (transportation == "8" || "8".equalsIgnoreCase(transportation)
										|| transportation == "9" || "9".equalsIgnoreCase(transportation)
										|| transportation == "15" || "15".equalsIgnoreCase(transportation)
										|| transportation == "14" || "14".equalsIgnoreCase(transportation)) {
									transportation = "7";
								} else if (transportation == "10" || "10".equalsIgnoreCase(transportation)
										|| transportation == "11" || "11".equalsIgnoreCase(transportation)) {
									transportation = "8";
								} else if (transportation == "16" || "16".equalsIgnoreCase(transportation)) {
									transportation = "9";
								}
								
								
								
								
								
								if (getTime.indexOf(":") != -1) {
									String[] getTimes = getTime.split(":");
										outday = getTimes[3];
										outhour = String.format("%02d", Integer.valueOf(getTimes[1]));
										outmin = String.format("%02d", Integer.valueOf(getTimes[2]));

								}
								getTimeDate = outday + " " + outhour + ":" + outmin + ":00";
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								long result = sdf.parse(getTimeDate).getTime() - sdf.parse(outTimeDate).getTime();
								int minutes = (int) result / (1000 * 60);
								System.out.println("输出时间差：" + minutes);
								
							  
								
								
								if(stNum<=minutes&&minutes<=shNum&&tst.get(z).getKilometre().equals(kilometre)){
									
											map.put("thatTime", String.valueOf((i+1)));
											break biubiu;
																
								}else{
									map.put("thatTime", "");
								}		
							    }else{
							    	map.put("thatTime", "");
							    }
					}				
					}
					
					}

					
					
					List<OutKilometreScene> outKilometreScene = outKilometreSceneService.selectByKmSceneTransportation(kilometre, scene);
					
					
					
					
					
					for (int i = 0; i < outKilometreScene.size(); i++) {
						
						
						    
							map.put("familyNum", familyNum);
							map.put("peopleNum", "1");					
							
							map.put("outType", FormattingUtil.WxtripModeOne(outType));
												
						// 公交车
						if ("1".equals(outKilometreScene.get(i).getTransportation())) {
							map.put("busTotalTime", outKilometreScene.get(i).getTimeTotal());
							map.put("busPriceTicket", outKilometreScene.get(i).getPriceTicket());
							map.put("busGoTime", outKilometreScene.get(i).getTimeGo());
							map.put("busWaitTime", outKilometreScene.get(i).getTimeWait());
							map.put("busRidingTime", outKilometreScene.get(i).getTimeRiding());
	
						}
						// 地铁
						if ("2".equals(outKilometreScene.get(i).getTransportation())) {
							
							
							map.put("metroTotalTime", outKilometreScene.get(i).getTimeTotal());
							map.put("metroPriceTicket", outKilometreScene.get(i).getPriceTicket());
							map.put("metroGoTime", outKilometreScene.get(i).getTimeGo());
							map.put("metroWaitTime", outKilometreScene.get(i).getTimeWait());
							map.put("metroRidingTime", outKilometreScene.get(i).getTimeRiding());

							
						}
						// 小汽车
						if ("3".equals(outKilometreScene.get(i).getTransportation())) {

							
							map.put("carTotalTime", outKilometreScene.get(i).getTimeTotal());
							map.put("carTotalCost", outKilometreScene.get(i).getCostTotal());
							map.put("carWorkingCost", outKilometreScene.get(i).getCostWorking());
							map.put("carParkCost", outKilometreScene.get(i).getCostPark());
							

						}
						// 出租车
						if ("4".equals(outKilometreScene.get(i).getTransportation())) {
							

							map.put("taxiTotalTime", outKilometreScene.get(i).getTimeTotal());
							map.put("taxiTotalCost", outKilometreScene.get(i).getCostTotal());

						}
						// 自行车
						if ("5".equals(outKilometreScene.get(i).getTransportation())) {
							
							
							map.put("bicycleTotalTime", outKilometreScene.get(i).getTimeTotal());
							
						}
						// 电动车
						if ("6".equals(outKilometreScene.get(i).getTransportation())) {
							
							
							map.put("electricVehicleTotalTime", outKilometreScene.get(i).getTimeTotal());
						}
							
					}
					listResult.add(map);
					
					
					}else{
						
					    
						map.put("familyNum", familyNum);
						map.put("peopleNum", "1");					
						map.put("thatTime", thatTime);
						map.put("outType", FormattingUtil.WxtripModeOne(outType));
											
					// 公交车
					
						map.put("busTotalTime", "");
						map.put("busPriceTicket", "");
						map.put("busGoTime", "");
						map.put("busWaitTime", "");
						map.put("busRidingTime", "");
					
					// 地铁
					
						map.put("metroTotalTime", "");
						map.put("metroPriceTicket", "");
						map.put("metroGoTime", "");
						map.put("metroWaitTime", "");
						map.put("metroRidingTime", "");
				
					// 小汽车
					
						map.put("carTotalTime", "");
						map.put("carTotalCost", "");
						map.put("carWorkingCost", "");
						map.put("carParkCost", "");
					
					// 出租车
					
						map.put("taxiTotalTime", "");
						map.put("taxiTotalCost", "");
					
					// 自行车
				
						map.put("bicycleTotalTime", "");
				
					// 电动车
					
						map.put("electricVehicleTotalTime", "");
						
						listResult.add(map);
					}
					
					
				}
					
					System.out.println("家庭编号：" + familyNum);
					System.out.println("时间价值：" + list.toString());
					System.out.println("--------------------" + listResult.size());
				}
			}
			
			String []title = new String[]{"序号","家庭编号","成员编号","出行序号", "公交_总时间", "公交_走到站时间", "公交_等车时间", "公交_乘车时间", "公交_费用"
					, "地铁_总时间", "地铁_走到站时间", "地铁_等车时间", "地铁_乘车时间", "地铁_费用"
					, "小汽车_时间", "小汽车_总费用", "小汽车_运行费用", "小汽车_停车费用"
					, "出租车_时间", "出租车_费用"
					, "自行车_时间", "电动车_时间", "出行方式选择"};//标题
			String [][]values = new String[listResult.size()][];
			
			for(int i = 0;i < listResult.size(); i++){
	            values[i] = new String[title.length];
	            //将对象内容转换成string
	            values[i][0] = (i + 1) + "";
	            values[i][1] = listResult.get(i).get("familyNum");
	            values[i][2] = listResult.get(i).get("peopleNum");
	            values[i][3] = listResult.get(i).get("thatTime");
	          
	            
	            values[i][4] = listResult.get(i).get("busTotalTime");
	            values[i][5] = listResult.get(i).get("busPriceTicket");
	            values[i][6] = listResult.get(i).get("busGoTime");
	            values[i][7] = listResult.get(i).get("busWaitTime");
	            values[i][8] = listResult.get(i).get("busRidingTime");
	            
	            values[i][9] = listResult.get(i).get("metroTotalTime");
	            values[i][10] = listResult.get(i).get("metroPriceTicket");
	            values[i][11] = listResult.get(i).get("metroGoTime");
	            values[i][12] = listResult.get(i).get("metroWaitTime");
	            values[i][13] = listResult.get(i).get("metroRidingTime");
	            
	            values[i][14] = listResult.get(i).get("carTotalTime");
	            values[i][15] = listResult.get(i).get("carTotalCost");
	            values[i][16] = listResult.get(i).get("carWorkingCost");
	            values[i][17] = listResult.get(i).get("carParkCost");


	            values[i][18] = listResult.get(i).get("taxiTotalTime");
	            values[i][19] = listResult.get(i).get("taxiTotalCost");
	            
	            values[i][20] = listResult.get(i).get("bicycleTotalTime");
	            values[i][21] = listResult.get(i).get("electricVehicleTotalTime");
			
	            values[i][22] = listResult.get(i).get("outType");
	        }
			
			getHSSFWorkbook(path + "time2.xls2", "时间价值", title, values, null);
			
			System.out.println("导出时间价值完成。");
		}

	}

	
	
	

	/**
	 * 调用存储过程方法
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @throws SQLException
	 * @date 20170602
	 */
	@RequestMapping(value = "/wxProut", method = RequestMethod.POST)
	public void wxProut(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException, SQLException {
		Map<String, Object> json = new HashMap<String, Object>();
		Connection conn = null;
		CallableStatement csmt = null;
		int num = 1001;
		int num2 = 2000;
		conn = JDBCUtils.getConnection();
		conn.setAutoCommit(false);

		for (int i = 1001; i < num2; i++) {
			csmt = conn.prepareCall("call sp(?)");
			csmt.setInt(1, i);
			csmt.execute();
			conn.commit();
		}

		json.put("flag", "1");
		json.put("message", "成功！");
		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());

	}

//	/**
//	 * 家庭成员1未填写出行信息 或未有满足填写出行信息的 成员时，转化成进度7
//	 *
//	 * @param request
//	 * @param response
//	 * @throws IOException
//	 * @throws ServletException
//	 * @date 20170602
//	 */
//	@RequestMapping(value = "/wxChangStage", method = RequestMethod.POST)
//	public void wxChangStage(XssHttpServletRequestWrapper request, HttpServletResponse response)
//			throws IOException, ServletException {
//		Map<String, Object> json = new HashMap<String, Object>();
//
//		HttpSession session = request.getSession(false);
//		// 取出会话数据
//		String openId = (String) session.getAttribute("openId");
//		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
//		if (openId != null) {
//			// List<StatisticsParticular> sp =
//			// statisticsParticularService.selectBySubmitterForList(openId);
//			StatisticsParticular sp = statisticsParticularService.selectBySubmitter(openId);
//			if (sp != null) {
//				this.wxSendWordError(openId);
//				sp.setSubmitStage(7);
//				statisticsParticularService.updateByPrimaryKeySelective(sp);
//			}
//			json.put("flag", "1");
//			json.put("message", "成功！");
//			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
//		} else {
//			RequestDispatcher rd = request.getRequestDispatcher("/wx/false405.jsp");
//			rd.forward(request, response);
//			return;
//		}
//	}

	/**
	 * 根据城市编号获取url
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @date 20170602
	 */
	@RequestMapping(value = "/wxgetCityForTimeurl", method = RequestMethod.POST)
	public void wxgetCityForTimeurl(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();

		HttpSession session = request.getSession(false);
		// 取出会话数据
		String weCityCode = (String) session.getAttribute("weCityCode");
		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
		if (weCityCode != null) {
			// List<StatisticsParticular> sp =
			// statisticsParticularService.selectBySubmitterForList(openId);

			CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(weCityCode, "99");
			String cityUrl = "";
			if (cityCodeUrl != null) {
				cityUrl = cityCodeUrl.getCityUrl();
			}
			json.put("flag", "1");
			json.put("message", cityUrl);
			json.put("weCityCode", weCityCode);

			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		}
	}

	/**
	 * 查询城市编码修改相应文字
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @date 20170602
	 */
	@RequestMapping(value = "/wxgetCityForWord", method = RequestMethod.POST)
	public void wxgetCityForWord(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();

		HttpSession session = request.getSession(false);
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
		if (openId != null) {
			List<StatisticsParticular> sp = statisticsParticularService.selectBySubmitterForList(openId);
			if (sp.size() > 0) {
				String city = sp.get(0).getCtiyCode();
				CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(city, "0");
				cityCodeUrl.getCityUrl();
				cityCodeUrl.getCityName();
				json.put("flag", "1");
				json.put("message", cityCodeUrl.getCityName());
				json.put("extended", cityCodeUrl.getExtendedFieldOne());
			}

			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		}
	}

	/**
	 * @获取两个点，两个页面计算， @防止取不到经纬度报错。 @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @date 20170514
	 */
	@RequestMapping(value = "/wxGetLnglat", method = RequestMethod.POST)
	public void wxSavePositionDeviate(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();

		HttpSession session = request.getSession(false);
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
		if (openId != null) {
			StatisticsParticular sp = statisticsParticularService.selectBySubmitter(openId);
			if (sp != null) {
				Float messagelon = sp.getLon();
				Float messagelat = sp.getLat();
				Float messageslon = sp.getSubmitLon();
				Float messageslat = sp.getSubmitLat();
				json.put("flag", "1");
				json.put("messagelon", messagelon);
				json.put("messagelat", messagelat);
				json.put("messageslon", messageslon);
				json.put("messageslat", messageslat);
			}

			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		}
	}

	/**
	 * @获取两个点，两个页面计算， @防止取不到经纬度报错。 @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @date 20170514
	 */
	@RequestMapping(value = "/wxSavePositionDeviate", method = RequestMethod.POST)
	public void wxGetLnglat(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();
		String echolocation = request.getParameter("echolocation");
		HttpSession session = request.getSession(false);
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
		if (openId != null) {
			StatisticsParticular sp = statisticsParticularService.selectBySubmitter(openId);
			if (sp != null) {
				sp.setPositionDeviate(echolocation);
				int num = statisticsParticularService.updateByPrimaryKeySelective(sp);
				if (num > 0) {
					json.put("flag", "1");
					json.put("messagelon", "成功！");
				} else {
					json.put("flag", "2");
					json.put("messagelon", "成功！");
				}
			}

			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		}
	}

	/**
	 * 保存提交位置的经纬度
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @date 20170514
	 */
	@RequestMapping(value = "/wxSubmitLnglat", method = RequestMethod.POST)
	public void wxSubmitLnglat(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();

		String lnglat = request.getParameter("lnglat");
		String district = request.getParameter("district");

		HttpSession session = request.getSession(false);
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "or5UjxNWEpTofPg7SJb-CA2rGexk";
		if (openId != null) {
			StatisticsParticular sp = statisticsParticularService.selectBySubmitter(openId);
			if (sp != null) {
				String[] lnglatTwo = lnglat.split(",");
				sp.setSubmitLon(Float.parseFloat(lnglatTwo[0]));
				sp.setSubmitLat(Float.parseFloat(lnglatTwo[1]));
				sp.setExtendedFieldThree(district);
				statisticsParticularService.updateByPrimaryKeySelective(sp);
			}
			json.put("flag", "1");
			json.put("message", "成功");
			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		}
	}

//	/**
//	 * 保存时间调查问卷信息
//	 *
//	 * @param request
//	 * @param response
//	 * @throws IOException
//	 * @throws ServletException
//	 * @date 20170514
//	 */
//	@RequestMapping(value = "/wxsaveTimeText", method = RequestMethod.POST)
//	public void saveTimeText(XssHttpServletRequestWrapper request, HttpServletResponse response)
//			throws IOException, ServletException {
//		Map<String, Object> json = new HashMap<String, Object>();
//		HttpSession session = request.getSession(false);
//		// 取出会话数据
//		String openId = (String) session.getAttribute("openId");
//		// String openId = "or5UjxNWEpTofPg7SJb-CA2rGexk";
//		if (openId != null) {
//			// 获取城市的URl
//			// 取出会话数据
//			String weCityCode = (String) session.getAttribute("weCityCode");
//			CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(weCityCode, "99");
//			String prizeSign = cityCodeUrl.getPrizeSign();
//
//			if (!"1".equals(prizeSign)) {
//				// 推送是否可以获得礼品信息
//				this.wxSendWord(openId);
//				StatisticsParticular sp = statisticsParticularService.selectBySubmitter(openId);
//				sp.setSubmitStage(7);
//				statisticsParticularService.updateByPrimaryKeySelective(sp);
//
//			}
//			// 封装json数据
//			String timeTextValue = request.getParameter("timeTextValue");
//			String familyId = request.getParameter("familyId");
//			String indexText = request.getParameter("indexText");
//
//			List<StatisticsParticular> statisticsParticular = statisticsParticularService.selectByFamilyList(familyId);
//			String city = "";
//			if (statisticsParticular.size() > 0) {
//				city = statisticsParticular.get(0).getCtiyCode();
//			}
//			StringBuffer sb = new StringBuffer();
//			sb.append("[{");
//			if (timeTextValue == "1" || "1".equals(timeTextValue)) {
//
//				sb.append("\"哪次出行\":");
//				sb.append("\"" + indexText + "\",");
//				sb.append("\"出行方式\":");
//				sb.append("\"" + timeTextValue + "\",");
//				sb.append("\"时间\":");
//				sb.append("\"" + request.getParameter("busTotalTime") + "\",");
//				sb.append("\"票价\":");
//				sb.append("\"" + request.getParameter("busPriceTicket") + "\",");
//				sb.append("\"走到站时间\":");
//				sb.append("\"" + request.getParameter("busGoTime") + "\",");
//				sb.append("\"等车时间\":");
//				sb.append("\"" + request.getParameter("busWaitTime") + "\",");
//				sb.append("\"乘车时间\":");
//				sb.append("\"" + request.getParameter("busRidingTime") + "\"");
//			}
//			if (timeTextValue == "2" || "2".equals(timeTextValue)) {
//				sb.append("\"哪次出行\":");
//				sb.append("\"" + indexText + "\",");
//				sb.append("\"出行方式\":");
//				sb.append("\"" + timeTextValue + "\",");
//				sb.append("\"时间\":");
//				sb.append("\"" + request.getParameter("metroTotalTime") + "\",");
//				sb.append("\"票价\":");
//				sb.append("\"" + request.getParameter("metroPriceTicket") + "\",");
//				sb.append("\"走到站时间\":");
//				sb.append("\"" + request.getParameter("metroGoTime") + "\",");
//				sb.append("\"等车时间\":");
//				sb.append("\"" + request.getParameter("metroWaitTime") + "\",");
//				sb.append("\"乘车时间\":");
//				sb.append("\"" + request.getParameter("metroRidingTime") + "\"");
//			}
//			if (timeTextValue == "3" || "3".equals(timeTextValue)) {
//				sb.append("\"哪次出行\":");
//				sb.append("\"" + indexText + "\",");
//				sb.append("\"出行方式\":");
//				sb.append("\"" + timeTextValue + "\",");
//				sb.append("\"时间\":");
//				sb.append("\"" + request.getParameter("carTotalTime") + "\",");
//				sb.append("\"费用\":");
//				sb.append("\"" + request.getParameter("carTotalCost") + "\",");
//				sb.append("\"运行费用\":");
//				sb.append("\"" + request.getParameter("carWorkingCost") + "\",");
//				sb.append("\"停车费用\":");
//				sb.append("\"" + request.getParameter("carParkCost") + "\"");
//			}
//			if (timeTextValue == "4" || "4".equals(timeTextValue)) {
//				sb.append("\"哪次出行\":");
//				sb.append("\"" + indexText + "\",");
//				sb.append("\"出行方式\":");
//				sb.append("\"" + timeTextValue + "\",");
//				sb.append("\"乘车时间\":");
//				sb.append("\"" + request.getParameter("taxiTotalTime") + "\",");
//				sb.append("\"费用\":");
//				sb.append("\"" + request.getParameter("taxiTotalCost") + "\"");
//			}
//			if (timeTextValue == "5" || "5".equals(timeTextValue)) {
//				sb.append("\"哪次出行\":");
//				sb.append("\"" + indexText + "\",");
//				sb.append("\"出行方式\":");
//				sb.append("\"" + timeTextValue + "\",");
//				sb.append("\"时间\":");
//				sb.append("\"" + request.getParameter("bicycleTotalTime") + "\"");
//			}
//			if (timeTextValue == "6" || "6".equals(timeTextValue)) {
//				sb.append("\"哪次出行\":");
//				sb.append("\"" + indexText + "\",");
//				sb.append("\"出行方式\":");
//				sb.append("\"" + timeTextValue + "\",");
//				sb.append("\"时间\":");
//				sb.append("\"" + request.getParameter("electricVehicleTotalTime") + "\"");
//			}
//			sb.append("}]");
//			// 保存文件
//			String filePathAndName = getProperties().getProperty("filePathDetail") + city + "/time/time_"
//					+ familyId.replace(":", "") + ".txt";// 路径,TXT文件命名不能包含":"
//			TxtUtil.newFile(filePathAndName, sb.toString());// 将数据保存到服务器
//			System.out.println("保存text文件成功，位置D:carInfo！");
//
//			json.put("flag", "1");
//			json.put("message", prizeSign);
//			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
//		} else {
//			json.put("flag", "2");
//			json.put("message", "失败");
//			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
//		}
//	}
//
//	/**
//	 * 推送消息为错误页面
//	 *
//	 * @param request
//	 * @param response
//	 * @throws IOException
//	 * @throws ServletException
//	 * @date 20170514
//	 */
//	public void wxSendWordError(String openId) throws IOException, ServletException {
//		Map<String, Object> json = new HashMap<String, Object>();
//		// 取出会话数据
//		// HttpSession session = request.getSession(false);
//		// String openId = (String) session.getAttribute("openId");
//		String sign = "";
//		String familyId = "";
//		String city = "";
//		StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
//		if (statisticsParticular != null) {
//			familyId = statisticsParticular.getFamilyId();
//			city = statisticsParticular.getCtiyCode();
//			// CityCodeUrl cityCodeUrl =
//			// cityCodeUrlService.selectByCityCode(city, "0");
//
//			// 判断是否存在一次出行信息的出行
//			String content = "";
//			if ("0371".equals(city)) {
//				// content = "抱歉，您的问卷审核不通过，不能获得礼品";
//				content = "您的问卷已提交成功，感谢您的参与！";
//			} else if ("0632".equals(city)) {
//				content = "您的问卷已提交成功，感谢您的参与！";
//			} else {
//				content = "您的问卷已提交成功，感谢您的参与！";
//			}
//			// 发送奖励信息给用户
//			String accessToken = token.getAccess_token();
//			String tokenurl = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + accessToken;
//			// 发送文字消息，无连接
//			String jsontext = "{\"touser\":\"" + openId + "\",\"msgtype\":\"text\",\"text\":{\"content\":\"" + content
//					+ "\"}}";
//			System.out.println("这里是json" + jsontext);
//			// 请求方法，然后放回OK 成功，否则错误。这里这个请求方法在下边
//			try {
//				String resultWe = this.sendPost(tokenurl, jsontext);
//				System.out.println(resultWe);
//			} catch (Exception e) {
//				System.out.println("微信端主动推送消息失败!");
//				e.printStackTrace();
//			}
//
//		}
//	}

	/**
	 * 推送消息
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @date 20170514
	 */
	public void wxSendWord(String openId) throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();
		// 取出会话数据
		// HttpSession session = request.getSession(false);
		// String openId = (String) session.getAttribute("openId");
		String sign = "";
		String familyId = "";
		String city = "";
		StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
		if (statisticsParticular != null) {
			familyId = statisticsParticular.getFamilyId();
			city = statisticsParticular.getCtiyCode();
			// CityCodeUrl cityCodeUrl =
			// cityCodeUrlService.selectByCityCode(city, "0");

			// 判断是否存在一次出行信息的出行
			String extendedTwo = statisticsParticular.getExtendedFieldTwo();
			if (extendedTwo != null && !"".equals(extendedTwo)) {
				String[] extendedTwos = extendedTwo.split(",");
				for (int i = 0; i < extendedTwos.length; i++) {
					String[] extendedTwosfori = extendedTwos[i].split(":");
					int extendedTwoforInt = Integer.valueOf(extendedTwosfori[1]);
					if (extendedTwoforInt < 2 || extendedTwoforInt == 99) {
						sign = "false";
					}
				}
			}
			// 发送消息
			String titleName = statisticsParticular.getTitleName();

			String content = "";
			if (sign != "false" && !"false".equals(sign)) {
				if (getNoTraCount(familyId, city) < 2) {
					if ("0371".equals(city) && "public".equals(titleName)) {
						// content = "抱歉，您的问卷审核不通过，不能获得礼品";
						content = "您的问卷已提交成功，感谢您的参与！";
					} else if ("0632".equals(city)) {
						content = "您的问卷已提交成功，感谢您的参与！";
					} else {
						content = "您的问卷已提交成功，感谢您的参与！";
					}
				} else {
					if ("0371".equals(city)) {
						// content = "抱歉，您的问卷审核不通过，不能获得礼品";
						content = "您的问卷已提交成功，感谢您的参与！";
					} else if ("0632".equals(city)) {
						content = "您的问卷已提交成功，感谢您的参与！";
					} else {
						content = "您的问卷已提交成功，感谢您的参与！";
					}

				}
			} else {
				if ("0371".equals(city)) {
					// content = "抱歉，您的问卷审核不通过。";
					content = "您的问卷已提交成功，感谢您的参与！";

				} else {
					content = "您的问卷已提交成功，感谢您的参与！";
				}
			}
			// 发送奖励信息给用户
			String accessToken = token.getAccess_token();
			String tokenurl = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + accessToken;
			// 发送文字消息，无连接
			String jsontext = "{\"touser\":\"" + openId + "\",\"msgtype\":\"text\",\"text\":{\"content\":\"" + content
					+ "\"}}";
			System.out.println("这里是json" + jsontext);
			// 请求方法，然后放回OK 成功，否则错误。这里这个请求方法在下边
			try {
				String resultWe = this.sendPost(tokenurl, jsontext);
				System.out.println(resultWe);
			} catch (Exception e) {
				System.out.println("微信端主动推送消息失败!");
				e.printStackTrace();
			}

		}
	}

	/**
	 * 随机获取填写问卷人的出行信息
	 * 
	 * @auoth wanglijan
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 *             date 20170512
	 * @throws ParseException
	 */
	@RequestMapping(value = "/wxgetPersonalRandomOutInfo")
	public void getPersonalRandomOutInfo(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException, ParseException {
		request.setCharacterEncoding("UTF-8");
		// AccessToken access = AccessTokenUtil.getAccessToken(appID, appScret);
		Map<String, Object> json = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "";
		List<Map<String, Object>> timelList = null;
		String sign = "";
		String transportation = "";
		String outTime = "";
		String getTime = "";
		String outObjective = "";
		String outStartingpoint = "";
		String getObjective = "";
		String noText = "";
		String Kilometre = "";
		String outTimeDate = "";
		String getTimeDate = "";
		String chooseForAge = "";
		String city = "";

		// 根据城市决定是否推送消息
		// 取出会话数据
		/*
		 * String weCityCode = (String) session.getAttribute("weCityCode");
		 * CityCodeUrl cityCodeUrl =
		 * cityCodeUrlService.selectByCityCode(weCityCode, "99"); String
		 * prizeSign = cityCodeUrl.getPrizeSign();
		 */

		int index = 0;
		if (openId != null) {
			String familyId = request.getParameter("familyId");
			List<StatisticsParticular> statisticsParticular = statisticsParticularService.selectByFamilyList(familyId);
			if (statisticsParticular.size() > 0) {
				city = statisticsParticular.get(0).getCtiyCode();
			}
			if (familyId != null && familyId != "" && !"".equals(familyId)) {
				String fileNameOne = getProperties().getProperty("filePathDetail");
				String fileNameTwo = fileNameOne + city + "/outInfo/outInfo_1_" + familyId + ".txt";
				List<String> list = ReadFromFile.readFileByLines(fileNameTwo);
				List<Map<String, Object>> outList = timelList = new ArrayList<Map<String, Object>>();
				if (list.size() > 0) {
					String outinfo = "";
					outList = new ArrayList<Map<String, Object>>();
					// 将数据放到List<Map<String,Object>>里面

					for (int i = 0; i < list.size(); i++) {
						outinfo = list.get(i);
					}
					// 读取json数据
					JSONArray jsonArr = JSONArray.fromObject(outinfo);
					List<Map<String, Object>> listforjson = new ArrayList<Map<String, Object>>();
					Iterator<JSONObject> it = jsonArr.iterator();
					while (it.hasNext()) {
						JSONObject json2 = it.next();
						listforjson.add(parseJSON2Map(json2.toString()));
					}

					index = (int) (Math.random() * (listforjson.size() - 1));
					System.out.println("------------------------------"+index);

					// 随机取出来一次出行信息
					for (int i = 0; i < listforjson.size(); i++) {
						Map<String, Object> outMap = new HashMap<String, Object>();

						if (i == index) {
							for (int j = 0; j < listforjson.get(i).size(); j++) {
								transportation = (String) listforjson.get(i).get("交通方式");
								outTime = (String) listforjson.get(i).get("出发时间");
								getTime = (String) listforjson.get(i).get("到达时间");
								outObjective = (String) listforjson.get(i).get("出行目的");
								outStartingpoint = (String) listforjson.get(i).get("出发地点");
								getObjective = (String) listforjson.get(i).get("到达地点");
								noText = (String) listforjson.get(i).get("未出行信息注明");
							}
						}
					}

					// 判断是否存在一次出行信息的出行
					List<Map<String, Object>> listforjsonChoose = null;
					if (statisticsParticular.size() > 0) {
						// city = statisticsParticular.get(0).getCtiyCode();
						String extendedTwo = statisticsParticular.get(0).getExtendedFieldTwo();
						if (extendedTwo != null && !"".equals(extendedTwo)) {
							String[] extendedTwos = extendedTwo.split(",");
							for (int i = 0; i < extendedTwos.length; i++) {

								String[] extendedTwosfori = extendedTwos[i].split(":");
								int extendedTwoforInt = Integer.valueOf(extendedTwosfori[1]);
								if (extendedTwoforInt < 2 || extendedTwoforInt == 99) {
									sign = "false";
								}
							}
						}
					}

					// 没有出行细信息跳走
					if (noText == null || noText.equals(null)) {
						// 归类交通方式
						if (transportation == "0" || "0".equalsIgnoreCase(transportation) || transportation == "17"
								|| "17".equalsIgnoreCase(transportation)) {
							transportation = "1";
						} else if (transportation == "1" || "1".equalsIgnoreCase(transportation)) {
							transportation = "3";
						} else if (transportation == "2" || "2".equalsIgnoreCase(transportation)
								|| transportation == "3" || "3".equalsIgnoreCase(transportation)) {
							transportation = "2";
						} else if (transportation == "5" || "5".equalsIgnoreCase(transportation)) {
							transportation = "4";
						} else if (transportation == "4" || "4".equalsIgnoreCase(transportation)
								|| transportation == "6" || "6".equalsIgnoreCase(transportation)
								|| transportation == "7" || "7".equalsIgnoreCase(transportation)) {
							transportation = "5";
						} else if (transportation == "13" || "13".equalsIgnoreCase(transportation)
								|| transportation == "12" || "12".equalsIgnoreCase(transportation)) {
							transportation = "6";
						} else if (transportation == "8" || "8".equalsIgnoreCase(transportation)
								|| transportation == "9" || "9".equalsIgnoreCase(transportation)
								|| transportation == "15" || "15".equalsIgnoreCase(transportation)
								|| transportation == "14" || "14".equalsIgnoreCase(transportation)) {
							transportation = "7";
						} else if (transportation == "10" || "10".equalsIgnoreCase(transportation)
								|| transportation == "11" || "11".equalsIgnoreCase(transportation)) {
							transportation = "8";
						} else if (transportation == "16" || "16".equalsIgnoreCase(transportation)) {
							transportation = "9";
						}
						// 得到出发时间和到达时间
						String outday = "";
						String outhour = "";
						String outmin = "";
						if (outTime.indexOf(":") != -1) {
							String[] outTimes = outTime.split(":");
							for (int i = 0; i < outTimes.length; i++) {
								outday = outTimes[3];
								outhour = String.format("%02d", Integer.valueOf(outTimes[1]));
								outmin = String.format("%02d", Integer.valueOf(outTimes[2]));
							}
						}
						outTimeDate = outday + " " + outhour + ":" + outmin + ":00";

						String getday = "";
						String gethour = "";
						String getmin = "";
						if (getTime.indexOf(":") != -1) {
							String[] getTimes = getTime.split(":");
							for (int i = 0; i < getTimes.length; i++) {
								outday = getTimes[3];
								outhour = String.format("%02d", Integer.valueOf(getTimes[1]));
								outmin = String.format("%02d", Integer.valueOf(getTimes[2]));
							}
						}
						getTimeDate = outday + " " + outhour + ":" + outmin + ":00";
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						long result = sdf.parse(getTimeDate).getTime() - sdf.parse(outTimeDate).getTime();
						int minutes = (int) result / (1000 * 60);
						System.out.println("输出时间差：" + minutes);
						// 查询出所有出行可能判断出公里数
						// int transportationNum =
						// Integer.valueOf(transportation);

						List<TransportationTime> listTransportationTime = transportationTimeService.selectAll();
						for (int i = 0; i < listTransportationTime.size(); i++) {
							int transportationStartTime = Integer.valueOf(listTransportationTime.get(i).getTimeStart());
							String transportationEndtTime = listTransportationTime.get(i).getTimeEnd();
							int transportationEndtTimeNum = 0;
							if (null == transportationEndtTime || "".equals(transportationEndtTime)) {
								// 定义为一千万，虚拟为无限大的分钟。
								transportationEndtTimeNum = 10000000;
							} else {
								transportationEndtTimeNum = Integer.valueOf(transportationEndtTime);
							}
							String getTransportation = listTransportationTime.get(i).getTransportation();
							if ((transportation == getTransportation || transportation.endsWith(getTransportation))
									&& transportationStartTime <= minutes && minutes <= transportationEndtTimeNum) {
								Kilometre = listTransportationTime.get(i).getKilometre();
							}
						}

						if ("".equals(Kilometre)) {
							// 如果时间无限大，取出最后一条的公里数。
							for (int i = 0; i < listTransportationTime.size(); i++) {
								String transportationRandom = listTransportationTime.get(i).getTransportation();
								if (transportation == transportationRandom
										|| transportationRandom.equals(transportation)) {
									System.out.println("时间已超出随机推送情景的最大值，公里数为表中存储的最大值。");
									Kilometre = listTransportationTime.get(i).getKilometre();
								}
							}
						}

					} else {
						Kilometre = "20";
						chooseForAge = "1";
					}
				} else {
					Kilometre = "20";
					chooseForAge = "1";
				}

				System.out.println("输出公里数" + Kilometre);
				Random random = new Random();
				int randomScene = random.nextInt(4) + 1;
				String scene = String.valueOf(randomScene);
				// 根据公里数和交通方式去查询详情信息
				List<OutKilometreScene> outKilometreScene = outKilometreSceneService
						.selectByKmSceneTransportation(Kilometre, scene);
				// 封装数据到前台

				Map<String, Object> timeMap = new HashMap<String, Object>();
				for (int i = 0; i < outKilometreScene.size(); i++) {
					// 公交车
					if ("1".equals(outKilometreScene.get(i).getTransportation())) {
						timeMap.put("busTotalTime", outKilometreScene.get(i).getTimeTotal());
						timeMap.put("busPriceTicket", outKilometreScene.get(i).getPriceTicket());
						timeMap.put("busGoTime", outKilometreScene.get(i).getTimeGo());
						timeMap.put("busWaitTime", outKilometreScene.get(i).getTimeWait());
						timeMap.put("busRidingTime", outKilometreScene.get(i).getTimeRiding());
					}
					// 地铁
					if ("2".equals(outKilometreScene.get(i).getTransportation())) {
						timeMap.put("metroTotalTime", outKilometreScene.get(i).getTimeTotal());
						timeMap.put("metroPriceTicket", outKilometreScene.get(i).getPriceTicket());
						timeMap.put("metroGoTime", outKilometreScene.get(i).getTimeGo());
						timeMap.put("metroWaitTime", outKilometreScene.get(i).getTimeWait());
						timeMap.put("metroRidingTime", outKilometreScene.get(i).getTimeRiding());
					}
					// 小汽车
					if ("3".equals(outKilometreScene.get(i).getTransportation())) {
						timeMap.put("carTotalTime", outKilometreScene.get(i).getTimeTotal());
						timeMap.put("carTotalCost", outKilometreScene.get(i).getCostTotal());
						timeMap.put("carWorkingCost", outKilometreScene.get(i).getCostWorking());
						timeMap.put("carParkCost", outKilometreScene.get(i).getCostPark());
					}
					// 出租车
					if ("4".equals(outKilometreScene.get(i).getTransportation())) {
						timeMap.put("taxiTotalTime", outKilometreScene.get(i).getTimeTotal());
						timeMap.put("taxiTotalCost", outKilometreScene.get(i).getCostTotal());
					}
					// 自行车
					if ("5".equals(outKilometreScene.get(i).getTransportation())) {
						timeMap.put("bicycleTotalTime", outKilometreScene.get(i).getTimeTotal());
					}
					// 电动车
					if ("6".equals(outKilometreScene.get(i).getTransportation())) {
						timeMap.put("electricVehicleTotalTime", outKilometreScene.get(i).getTimeTotal());
					}

					if ("1".equals(chooseForAge)) {
						sign = "false";
						timeMap.put("index", "*");
						outObjective = "";
						outTimeDate = "";
						getObjective = "";
						getTimeDate = "";
						outStartingpoint = "";
					} else {
						timeMap.put("index", (index + 1));
					}

					timeMap.put("outObjective", outObjective);
					timeMap.put("noText", noText);
					timeMap.put("outTimeDate", outTimeDate.trim());
					timeMap.put("getObjective", getObjective);
					timeMap.put("getTimeDate", getTimeDate);
					timeMap.put("outStartingpoint", outStartingpoint);

					// timeMap.put("prizeSign", prizeSign);

				}
				timelList.add(timeMap);
				response.getWriter().write(ZsJsonUtil.listMap2Json(timelList));
			} else {
				System.out.println("未发现familyId，用户填写的家庭成员不存在添加出行信息的人员。");
			}
		}
	}

	/**
	 * 获取所有未出行的人数
	 * 
	 * @param familyId
	 * @return
	 */
	int getNoTraCount(String familyId, String city) {
		int count = 0;

		String fileNameOne = getProperties().getProperty("filePathDetail");
		for (int k = 1; k < 20; k++) {
			String fileNameTwo = fileNameOne + city + "/outInfo/outInfo_" + k + "_" + familyId + ".txt";
			// 判断文件是否存在
			File file = new File(fileNameTwo);
			if (!file.exists()) {
				System.out.println("未出行人数：" + count);
				return count;
			}

			List<String> list = ReadFromFile.readFileByLines(fileNameTwo);
			List<Map<String, Object>> outList = new ArrayList<Map<String, Object>>();
			String outinfo = "";
			outList = new ArrayList<Map<String, Object>>();
			// 将数据放到List<Map<String,Object>>里面

			for (int i = 0; i < list.size(); i++) {
				outinfo = list.get(i);
			}
			// 读取json数据
			JSONArray jsonArr = JSONArray.fromObject(outinfo);
			List<Map<String, Object>> listforjson = new ArrayList<Map<String, Object>>();
			Iterator<JSONObject> it = jsonArr.iterator();
			while (it.hasNext()) {
				JSONObject json2 = it.next();
				listforjson.add(parseJSON2Map(json2.toString()));
			}

			// 随机取出来一次出行信息
			for (int i = 0; i < listforjson.size(); i++) {
				Map<String, Object> outMap = new HashMap<String, Object>();
				for (int j = 0; j < listforjson.get(i).size(); j++) {
					String noText = (String) listforjson.get(i).get("未出行信息注明");
					if (noText != null && !noText.equals("")) {
						count++;
					}
				}
			}
		}

		System.out.println("未出行人数：" + count);
		return count;
	}

	/*
	 * 通过递归得到某一路径下所有的目录及其文件
	 */
	static void getFiles(String filePath) {
		File root = new File(filePath);
		File[] files = root.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				/*
				 * 递归调用
				 */
				getFiles(file.getAbsolutePath());
				filelist.add(file.getAbsolutePath());
				System.out.println("显示" + filePath + "下所有子目录及其文件" + file.getAbsolutePath());
			} else {
				filelist.add(file.getAbsolutePath());
				System.out.println("显示" + filePath + "下所有子目录" + file.getAbsolutePath());
			}
		}
	}

	/**
	 * 修改家庭成员文件信息
	 * 
	 * @auoth wanglijan
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	private static ArrayList<String> filelist = new ArrayList<String>();

	@RequestMapping(value = "/xgCarInfo")
	public void xgCarInfo(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();
		int count1 = 0;
		int count2 = 0;
		int count3 = 0;

		int count4 = 0;

		String filePath = "E:\\0614";
		getFiles(filePath);

		for (int m = 0; m < filelist.size(); m++) {
			String strname = String.valueOf(filelist.get(m));
			List<String> listread = ReadFromFile.readFileByLines(strname);
			String personalinfo = "";
			System.out.println("文件名称++++++++++++++++++++++++" + strname);

			count4++;
			System.out.println("文件数量++++++++++++++++++++++++" + count4);
			for (int i = 0; i < listread.size(); i++) {
				personalinfo = listread.get(i);
			}

			// 读取json数据
			JSONArray jsonArr = JSONArray.fromObject(personalinfo);
			List<Map<String, Object>> listforjson = new ArrayList<Map<String, Object>>();
			Iterator<JSONObject> it = jsonArr.iterator();
			while (it.hasNext()) {
				JSONObject json2 = it.next();
				listforjson.add(parseJSON2Map(json2.toString()));
			}

			String address = "";
			String name = "";
			String dh = "";
			for (Map<String, Object> w : listforjson) {
				for (String k : w.keySet()) {
					address = (String) w.get("收件人地址");
					name = (String) w.get("收件人姓名");
					dh = (String) w.get("收件人电话");
				}
			}

			if (!"".equals(address) || !"".equals(name) || !"".equals(dh)) {
				count1++;
			}
			if (!"".equals(address) && !"".equals(dh)) {
				count2++;
			}
			if (!"".equals(address) && !"".equals(dh) && !"".equals(name)) {
				count3++;
			}

		}

		System.out.println("只填写其中一项的为+++++++++++++++++++++++++++++++++++++++++++++++++++++" + count1);
		System.out.println("填写地址和联系电话的为+++++++++++++++++++++++++++++++++++++++++++++++++++++" + count2);
		System.out.println("三项都填写的为+++++++++++++++++++++++++++++++++++++++++++++++++++++" + count3);

	}

	/**
	 * 修改出行文件信息
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @auoth wanglijan
	 */
	@RequestMapping(value = "/xgCarInfo2")
	public void xgCarInfo2(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();

		String filePath = "E:\\0614";
		getFiles(filePath);
		int num = 0;
		for (int m = 0; m < filelist.size(); m++) {
			String strname = String.valueOf(filelist.get(m));
			List<String> listread = this.readFileByLines(strname);
			String personalinfo = "";
			// personalList = new ArrayList<Map<String, Object>>();
			// 将数据放到List<Map<String,Object>>里面
			for (int i = 0; i < listread.size(); i++) {
				personalinfo = listread.get(i);
			}
			String arrStringq = "";
			StringBuffer sb = new StringBuffer();
			System.out.println(strname);
			if (personalinfo != "" && personalinfo != null && !"".equals(personalinfo)) {
				sb.append(personalinfo);
				TxtUtil.newFile(strname, sb.toString());// 将数据保存到服务器
				System.out.println("++++++++++++++++++++++++++" + num + 1 + "++++++++++++++++++++++++++++++++++");
				num++;
			}
		}
	}

	/**
	 * 以行为单位读取文件，常用于读面向行的格式化文件
	 */
	public static List<String> readFileByLines(String fileName) {
		File file = new File(fileName);
		BufferedReader reader = null;

		List<String> lines = new ArrayList<>();

		int line = 0;
		int num = 2;
		String tempStringTwo = null;
		try {
			// System.out.println("以行为单位读取文件内容，一次读一整行：");
			reader = new BufferedReader(new FileReader(file));
			String tempString = "";

			// 一次读入一行，直到读入null为文件结束
			while (num != 0) {
				// 显示行号
				// System.out.println("line " + line + ": " + tempString);
				num--;
				tempStringTwo = reader.readLine();
				if (tempStringTwo != null) {
					tempString = tempString + tempStringTwo;
				}

				lines.add(tempString);
				line++;
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
			// System.out.println("数据读取完毕，行数："+line);
			return lines;
		}
	}

	/**
	 * 线程查询该推送消息给用户
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @auoth wanglijan
	 * @date 20170510
	 */
	/*
	 * @Override public void run() { while(true){ try {
	 * //调用工具类获取access_token(每日最多获取2000次，每次获取的有效期为7200秒) Thread.sleep(60*1000);
	 * try { this.sendjsonfortouser(null, null); } catch (IOException e) { //
	 * TODO Auto-generated catch block e.printStackTrace(); } catch
	 * (ParseException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } //60秒之后尝试重新获取 //Thread.sleep(60*1000); } catch
	 * (InterruptedException e) { e.printStackTrace(); } } }
	 */

	/*
	 * 定时发送消息给关注但未提交问卷的用户
	 * 
	 * @param request
	 * 
	 * @param response
	 * 
	 * @throws IOException
	 * 
	 * @auoth wanglijan
	 * 
	 * @date 20170509
	 */
	@RequestMapping(value = "/wxsendjsonfortouser")
	public void sendjsonfortouser(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ParseException {
		Map<String, Object> json = new HashMap<String, Object>();
		AccessToken access = AccessTokenUtil.getAccessToken(appID, appScret);
		String accessToken = access.getAccess_token();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		Date nowDate = new Date();// new Date()为获取当前系统时间
		List<StatisticsParticular> selectAll = statisticsParticularService.getStatisticsParticularList("", 1, 10);
		for (int i = 0; i < selectAll.size(); i++) {
			String followDateStr = selectAll.get(i).getFollowDate();
			Date followDate = sdf.parse(followDateStr);
			long diff = nowDate.getTime() - followDate.getTime();
			long days = diff / (1000 * 60 * 60 * 24);
			// 得到相差时间
			long hours = (diff - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
			int hour = (int) hours;
			if (hour > 12) {
				String openId = selectAll.get(i).getSubmitter();

				String tokenurl = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + accessToken;
				// 发送文字消息，无连接
				String jsontext = "{\"touser\":\"+openId+\",\"msgtype\":\"text\",\"text\":{\"content\":\"Hello 我给你回复消息了哦\"}}";
				System.out.println("这里是json" + jsontext);
				// 请求方法，然后放回OK 成功，否则错误。这里这个请求方法在下边
				try {
					String result = this.sendPost(tokenurl, jsontext);
					System.out.println(result);
				} catch (Exception e) {
					System.out.println("微信端主动推送消息失败!");
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param param请求参数，请求参数应该是
	 *            name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 * @auoth wanglijan date 20170509
	 */
	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("user-agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			OutputStreamWriter outWriter = new OutputStreamWriter(conn.getOutputStream(), "utf-8");
			out = new PrintWriter(outWriter);
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/*
	 * 生成APP账号与密码
	 * 
	 * @param request
	 * 
	 * @param response
	 * 
	 * @throws IOException
	 * 
	 * @auoth wanglijan
	 * 
	 * @date 20170425
	 */
	@RequestMapping(value = "/wxproduceAppInfoForTable", method = RequestMethod.POST)
	public void produceAppInfoForTable(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
		StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
		if (statisticsParticular != null) {
			String familyId = statisticsParticular.getFamilyId();
			WechatApp wechat = new WechatApp();
			wechat.setOpenid(openId);
			wechat.setFamilyid(familyId);
			wechat.setApppwd("123456");
			int number = (int) (Math.random() * 1000);
			String appId = familyId + number;
			wechat.setAddid(appId);
			wechatAppService.insertSelective(wechat);
			json.put("flag", "1");
			json.put("message", appId);
			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		}
	}

	/*
	 * 生成APP账号与密码
	 * 
	 * @param request
	 * 
	 * @param response
	 * 
	 * @throws IOException
	 * 
	 * @auoth wanglijan
	 * 
	 * @date 20170425
	 */
	@RequestMapping(value = "/wxproduceAppInfo", method = RequestMethod.POST)
	public void produceAppInfo(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		String value = request.getParameter("value");
		Map<String, Object> json = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "or5UjxNWEpTofPg7SJb-CA2rGexk";
		StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
		if (statisticsParticular != null) {
			String familyId = statisticsParticular.getFamilyId();
			// int number = (int)(Math.random() * 1000);
			String appId = familyId + value;
			json.put("flag", "1");
			json.put("message", appId);
			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());

		}
	}

	/*
	 * 保存奖励的邮寄信息
	 * 
	 * @param request
	 * 
	 * @param response
	 * 
	 * @throws IOException
	 * 
	 * @auoth wanglijan
	 * 
	 * @date 20170425
	 */
	@RequestMapping(value = "/wxsavePrizeInfo", method = RequestMethod.POST)
	public void savePrizeInfo(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		String text = request.getParameter("text");
		String people = request.getParameter("people");
		String telphone = request.getParameter("telphone");
		StringBuffer sb = new StringBuffer();
		sb.append("[{");
		sb.append("\"收件人地址\":");
		sb.append("\"" + text + "\",");
		sb.append("\"收件人姓名\":");
		sb.append("\"" + people + "\",");
		sb.append("\"收件人电话\":");
		sb.append("\"" + telphone + "\"");
		sb.append("}]");
		// String data = text + "," + people + "," + telphone;
		HttpSession session = request.getSession();
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "or5UjxNWEpTofPg7SJb-CA2rGexk";
		StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
		String city = "";
		if (statisticsParticular != null) {

			// 推送是否中奖消息
			this.wxSendWord(openId);

			city = statisticsParticular.getCtiyCode();
			String familyId = statisticsParticular.getFamilyId();
			statisticsParticular.setSubmitStage(7);
			statisticsParticularService.updateByPrimaryKeySelective(statisticsParticular);
			// String familyId = "102123247";

			// String filePathAndName =
			// getProperties().getProperty("filePathWxRewardInfo") + "reward_"

			String filePathAndName = getProperties().getProperty("filePathDetail") + city + "/reward/reward_"

					+ familyId.replace(":", "") + ".txt";// 路径,TXT文件命名不能包含":"
			TxtUtil.newFile(filePathAndName, sb.toString());// 将数据保存到服务器
			System.out.println("文件生成成功，保存在reward文件夹！");
			json.put("flag", "1");
			json.put("message", familyId);
			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());

		}
	}

	/*
	 * 保存家庭成员的出行信息
	 * 
	 * @param request
	 * 
	 * @param response
	 * 
	 * @throws IOException
	 * 
	 * @auoth wanglijan
	 * 
	 * @date 20170415
	 */
	@RequestMapping(value = "/wxsavePersonalOutInfo", method = RequestMethod.POST)
	public void savePersonalOutInfo(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		String data = request.getParameter("arrString");
		String pageNum = request.getParameter("pageNum");
		String noOuttx = request.getParameter("noOuttx");
		String outInfoNumforData = request.getParameter("outInfoNumforData");

		HttpSession session = request.getSession();
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "or5UjxNWEpTofPg7SJb-CA2rGexk";
		StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
		StringBuffer sb = new StringBuffer();
		String city = "";
		if (statisticsParticular != null) {
			city = statisticsParticular.getCtiyCode();
			if (data != null && !"".equals(data)) {
				String dataStr = data.substring(0, data.length() - 1);
				String[] arr = dataStr.split("!");
				List<String> list = new ArrayList<String>();
				sb.append("[");
				for (int i = 0; i < arr.length; i++) {
					String[] arri = arr[i].split(",");
					sb.append("{");
					for (int j = 0; j < arri.length; j++) {
						if (arri[0] != "" && !"".equals(arri[0]) && arri[0] != null) {
							if (j == 0) {
								sb.append("\"出发地点\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 1) {
								sb.append("\"出发时间\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 2) {
								sb.append("\"交通方式\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 3) {
								sb.append("\"到达地点\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 4) {
								sb.append("\"到达时间\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 5) {
								sb.append("\"出行目的\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 6) {
								sb.append("\"是否接送小孩\":");
								sb.append("\"" + arri[j] + "\",");
							}

						} else {

							if (j == 1) {
								sb.append("\"出发地点\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 2) {
								sb.append("\"出发时间\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 3) {
								sb.append("\"交通方式\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 4) {
								sb.append("\"到达地点\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 5) {
								sb.append("\"到达时间\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 6) {
								sb.append("\"出行目的\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 7) {
								sb.append("\"是否接送小孩\":");
								sb.append("\"" + arri[j] + "\",");
							}

						}
					}
					String sbone = sb.toString().substring(0, sb.toString().length() - 1);
					sb = new StringBuffer();
					sb.append(sbone);
					sb.append("},");
					System.out.println(arr[i]);
				}
				String sbtwo = sb.toString().substring(0, sb.toString().length() - 1);
				sb = new StringBuffer();
				sb.append(sbtwo);
				sb.append("]");
			}
			String familyId = statisticsParticular.getFamilyId();
			System.out.println("我是pageNum等于" + pageNum);
			// 在这里存下第几个人的几次出行信息
			outInfoNumforData = pageNum + ":" + outInfoNumforData;
			String eft = statisticsParticular.getExtendedFieldTwo();

			/*
			 * String ef = ""; if (eft != null && !"".equals(eft)) {
			 * 
			 * String[] efts = eft.split(","); int pageNumInt =
			 * Integer.valueOf(pageNum); // 如果是重复提交将以前存储的去掉 if ((efts.length +
			 * 1) >= pageNumInt) { for (int i = 0; i < pageNumInt - 1; i++) { ef
			 * += efts[i]; } }
			 */

			if (eft != null) {
				outInfoNumforData = eft + "," + outInfoNumforData;
			}
			// outInfoNumforData = eft + "," + outInfoNumforData;
			System.out.println("成功保存家庭成员的出行信息！");
			/*
			 * String one = "";
			 * if(eft!=null&&!eft.equals(null)&&!"".equals(eft)&&eft!=""){ one =
			 * eft.substring(0, 1); }
			 */

			statisticsParticular.setExtendedFieldTwo(outInfoNumforData);
			statisticsParticular.setSubmitStage(Integer.valueOf(6 + pageNum));
			System.out.println("我是stage等于" + Integer.valueOf(6 + pageNum));
			statisticsParticularService.updateByPrimaryKeySelective(statisticsParticular);
			String filePathAndName = getProperties().getProperty("filePathDetail") + city + "/outInfo/outInfo_"
					+ pageNum + "_" + familyId.replace(":", "") + ".txt";// 路径,TXT文件命名不能包含":"
			if (noOuttx != null) {
				StringBuffer sbnot = new StringBuffer();
				sbnot.append("[{");
				sbnot.append("\"未出行信息注明\":");
				sbnot.append("\"" + noOuttx + "\"");
				sbnot.append("}]");
				TxtUtil.newFile(filePathAndName, sbnot.toString());
				System.out.println("数据封装成功，返回getPersonalList()！");
			} else {
				TxtUtil.newFile(filePathAndName, sb.toString());// 将数据文件保存到服务器
				System.out.println("数据封装成功，返回getPersonalList()！");
			}
			json.put("flag", "1");
			json.put("message", familyId);
			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		}
	}

	/**
	 * 根据城市编号获取地铁信息
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @date 20170602
	 */
	@RequestMapping(value = "/wxgetMetroSign", method = RequestMethod.POST)
	public void wxgetMetroSign(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		// 取出会话数据
		// String openId = (String) session.getAttribute("openId");
		String weCityCode = (String) session.getAttribute("weCityCode");
		// String weCityCode = "0632";
		// 查询城市中是否有地铁
		String eft = "";
		// 城市中是否有抽大奖活动
		String prize = "";
		if (weCityCode != null) {
			CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(weCityCode, "0");
			eft = cityCodeUrl.getExtendedFieldThree();
			prize = cityCodeUrl.getPrizeSign();
		}

		json.put("flag", "1");
		json.put("message", eft);
		json.put("prize", prize);
		json.put("weCityCode", weCityCode);
		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
	}

	/*
	 * 查询家庭成员列表
	 * 
	 * @param request
	 * 
	 * @param response
	 * 
	 * @throws IOException
	 * 
	 * @auoth wanglijan
	 * 
	 * @date 20170415
	 */
	@RequestMapping(value = "/wxgetPersonalList", method = RequestMethod.POST)
	public void getPersonalList(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		String weCityCode = (String) session.getAttribute("weCityCode");
		// 查询城市中是否有地铁
		String eft = "";
		if (weCityCode != null) {
			CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(weCityCode, "0");
			eft = cityCodeUrl.getExtendedFieldThree();
		}

		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
		// String openId = "or5UjxNWEpTofPg7SJb-CA2rGexk";
		StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
		List<Map<String, Object>> personalList = null;
		String city = "";
		if (statisticsParticular != null) {
			city = statisticsParticular.getCtiyCode();
			String familyId = statisticsParticular.getFamilyId();
			// String familyId = "102123247";
			String fileNameOne = getProperties().getProperty("filePathDetail");
			String fileNameTwo = fileNameOne + city + "/personal/personalInfo_" + familyId + ".txt";
			List<String> list = ReadFromFile.readFileByLines(fileNameTwo);
			String personalinfo = "";
			personalList = new ArrayList<Map<String, Object>>();
			// 将数据放到List<Map<String,Object>>里面

			for (int i = 0; i < list.size(); i++) {
				personalinfo = list.get(i);
			}

			JSONArray jsonArr = JSONArray.fromObject(personalinfo);
			List<Map<String, Object>> listforjson = new ArrayList<Map<String, Object>>();
			Iterator<JSONObject> it = jsonArr.iterator();
			while (it.hasNext()) {
				JSONObject json2 = it.next();
				listforjson.add(parseJSON2Map(json2.toString()));
			}
			String sex = "";
			String agestr = "";
			String profession = "";
			String[] professionTwo = null;
			for (Map<String, Object> m : listforjson) {
				Map<String, Object> personalMap = new HashMap<String, Object>();
				for (String k : m.keySet()) {
					String age = (String) m.get("年龄");
					int ageInt = Integer.valueOf(age);
					if (ageInt >= 6) {
						sex = (String) m.get("性别");
						agestr = (String) m.get("年龄");
						profession = (String) m.get("职业");
						// personalList.add(personalMap);
					}
				}
				if (sex != "" && agestr != "") {
					personalMap.put("sex", sex);
					sex = "";
					personalMap.put("age", agestr);
					agestr = "";

					professionTwo = profession.split(":");
					personalMap.put("profession", professionTwo[0]);
					profession = "";
					professionTwo = null;
					personalList.add(personalMap);
				}

			}

			System.out.println("数据封装完成");
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(personalList));
	}

	/**
	 * 
	 * json转换map. <br>
	 * 详细说明
	 * 
	 * @param jsonStr
	 *            json字符串
	 * @return map
	 * @return Map<String,Object> 集合
	 * @throws @author
	 *             wanglijain
	 * @date 20170508
	 */
	public static Map<String, Object> parseJSON2Map(String jsonStr) {
		ListOrderedMap map = new ListOrderedMap();
		// 最外层解析
		JSONObject json = JSONObject.fromObject(jsonStr);
		for (Object k : json.keySet()) {
			Object v = json.get(k);
			// 如果内层还是数组的话，继续解析
			if (v instanceof JSONArray) {
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				Iterator<JSONObject> it = ((JSONArray) v).iterator();
				while (it.hasNext()) {
					JSONObject json2 = it.next();
					list.add(parseJSON2Map(json2.toString()));
				}
				map.put(k.toString(), list);
			} else {
				map.put(k.toString(), v);
			}
		}
		return map;
	}

	/**
	 * 条件查询
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @auoth wanglijan
	 * @date 20170415
	 */
	@RequestMapping(value = "/getListForQr", method = RequestMethod.POST)
	public void getListForQr(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		List<Map<String, Object>> listForQr = new ArrayList<Map<String, Object>>();
		HttpSession session = request.getSession();
		// 取出会话数据
		String cityCode = (String) session.getAttribute("cityCode");
		// String cityCode = "0371";
		String districtSelectName = request.getParameter("districtSelectName");
		String subOfficeSelectName = request.getParameter("subOfficeSelectName");
		String communitySelectName = request.getParameter("communitySelectName");

		String districtSelectId = request.getParameter("districtSelectId");
		String subOfficeSelectId = request.getParameter("subOfficeSelectId");
		List<Area> list = areaService.getListForQrAll(districtSelectId, subOfficeSelectId, communitySelectName,
				cityCode);
		Map<String, Object> map = null;
		for (int i = 0; i < list.size(); i++) {
			map = new HashMap<String, Object>();
			map.put("id", list.get(i).getAreaId());// 序号
			map.put("area", list.get(i).getDistrictName());// 行政区
			map.put("street", list.get(i).getSubofficeName());// 街道
			map.put("community", list.get(i).getCommunityName());// 社区
			listForQr.add(map);
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listForQr));
	}

	/**
	 * 查询街道相对应的社区
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @auoth wanglijan
	 * @date 20170415
	 */
	@RequestMapping(value = "/getCommunity", method = RequestMethod.POST)
	public void getCommunity(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		List<Map<String, Object>> listCommunity = new ArrayList<Map<String, Object>>();
		String data = request.getParameter("subOfficeSelectName");
		HttpSession session = request.getSession();
		// 取出会话数据
		String cityCode = (String) session.getAttribute("cityCode");
		// String cityCode = "0371";
		List<Area> area = areaService.getSelectListByName(data, 3);
		if (area != null) {
			Map<String, Object> mapNode = null;
			for (int i = 0; i < area.size(); i++) {
				mapNode = new HashMap<String, Object>();
				mapNode.put("id", area.get(i).getCommunityNumber());
				mapNode.put("text", area.get(i).getCommunityName());
				listCommunity.add(mapNode);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listCommunity));
	}

	/**
	 * 查询行政区相对应的街道
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @auoth wanglijan
	 * @date 20170415
	 */
	@RequestMapping(value = "/getStreet", method = RequestMethod.POST)
	public void getStreet(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		List<Map<String, Object>> listStreet = new ArrayList<Map<String, Object>>();
		String data = request.getParameter("districtSelectName");
		String areaId = request.getParameter("areaId");
		HttpSession session = request.getSession();
		// 取出会话数据
		String cityCode = (String) session.getAttribute("cityCode");
		// String cityCode = "0632";

		List<Area> area = areaService.getListSubByNameWlj(2, cityCode, areaId);
		Map<String, Object> mapNode = null;
		for (int i = 0; i < area.size(); i++) {
			mapNode = new HashMap<String, Object>();
			mapNode.put("id", area.get(i).getSubofficeNumber());
			mapNode.put("text", area.get(i).getSubofficeName());
			listStreet.add(mapNode);
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listStreet));
	}

	/**
	 * 查询所有行政区
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @auoth wanglijan
	 * @date 20170415
	 */
	@RequestMapping(value = "/getDistrictSelect", method = RequestMethod.POST)
	public void getDistrictSelect(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		List<Map<String, Object>> listArea = new ArrayList<Map<String, Object>>();
		String data = request.getParameter("data");
		HttpSession session = request.getSession();
		// 取出会话数据
		String cityCode = (String) session.getAttribute("cityCode");
		// String cityCode = "0371";

		List<Area> area = areaService.getSelectTypeOne(cityCode);
		Map<String, Object> mapNode = null;
		for (int i = 0; i < area.size(); i++) {
			mapNode = new HashMap<String, Object>();
			mapNode.put("id", area.get(i).getDistrictNumber());
			mapNode.put("text", area.get(i).getDistrictName());
			listArea.add(mapNode);
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listArea));
	}

	/**
	 * 生成带参数的二维码
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/getWeQrCode", method = RequestMethod.POST)
	public void getWeQrCode(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		// access_token = AccessTokenUtil.getAccessToken(appID, appScret);
		Map<String, Object> json = new HashMap<String, Object>();
		// 获取table中选中的checkbox
		ArrayList<String> list = new ArrayList<String>();
		String buffer = request.getParameter("buffer");
		String weradioValue = request.getParameter("weradioValue");
		String weChechboxValue = request.getParameter("weChechboxValue");

		HttpSession session = request.getSession();
		// 取出会话数据
		String cityCode = (String) session.getAttribute("cityCode");
		//String cityCode = "431000";

		if (buffer != "" && buffer != null) {
			String[] buffers = buffer.split(","); 
			for (int i = 0; i < buffers.length; i++) {
				Area area = areaService.selectByPrimaryKey(Integer.valueOf(buffers[i]));
				StringBuffer Sbuffer = new StringBuffer();
				Sbuffer.append(area.getCommunityNumber());
				Sbuffer.append(",");
				Sbuffer.append(area.getDistrictName());
				Sbuffer.append(area.getSubofficeName());
				Sbuffer.append(area.getCommunityName());
				list.add(Sbuffer.toString());
			}
		}
		System.out.println(buffer);
		System.out.println("accessToken获取成功：" + token.getAccess_token());
		// String[] codes = checkboxsValue.split(";");
		// 创建随机数为是的文件夹，防止存在重复文件。
		String faterFile = getProperties().getProperty("filePathWxQrCode");
		Random rm = new Random();
		// 获得随机数
		double pross = (1 + rm.nextDouble()) * Math.pow(10, 10);
		// 将获得的获得随机数转化为字符串
		String fixLenthString = String.valueOf(pross);
		// 删除原有文件,节约服务器资源
		File srcDir = new File(faterFile);
		DeleteFileUtils deleteFile = new DeleteFileUtils();
		deleteFile.deleteFile(srcDir);
		// 重新生成文件夹
		String filename = faterFile + fixLenthString;
		File outfile = new File(filename);
		if (!outfile.isFile()) {
			outfile.mkdir();
		} else {
			outfile.delete();
			outfile.mkdirs();
		}

		if ("1".equals(weradioValue)) {
			// codeStr二维码名称
			String codeStrforpublic = "公众问卷二维码";
			// code二维码参数
			String codeforpublic = cityCode + ";public;0";
			if (null != token) {
				String ticket = this.createPermanentORCode(token.getAccess_token(), codeforpublic);
				try {
					String showUrl = this.showQRcode(ticket);
					try {
						this.download(showUrl, codeStrforpublic + ".png", filename);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("accessToken获取失败：" + token.getAccess_token());
			}
		}
		if ("2".equals(weradioValue)) {
			for (int i = 0; i < list.size(); i++) {
				if (null != token) {
					String[] codeTwo = list.get(i).split(",");
					String code = null;
					String codeStr = null;
					for (int j = 0; j < codeTwo.length; j++) {

						if ("".equals(weChechboxValue)) {
							// codeStr二维码名称
							codeStr = codeTwo[1];
							// code二维码参数 城市编码 社区编号 是否为调查员 0否 1是
							code = cityCode + ";" + codeTwo[0] + ";0";
						} else {
							codeStr = "调查员专用：" + codeTwo[1];
							//codeTwo[0] = "431000121";
							code = cityCode + ";" + codeTwo[0] + ";1";
						}
					}
					String ticket = this.createPermanentORCode(token.getAccess_token(), code);
					try {
						String showUrl = this.showQRcode(ticket);
						try {
							this.download(showUrl, codeStr + ".png", filename);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("accessToken获取失败：" + token.getAccess_token());
				}
			}
		}
		ZipUtils zipUtils = new ZipUtils();
		// 生成zip文件
		zipUtils.createZip(filename, filename + ".zip");
		json.put("flag", "1");
		// 将文件夹返回，供用户下载。
		json.put("message", fixLenthString);
		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
	}

	/**
	 * 查询区域code
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @auoth wanglijan date 20170414
	 */
	@RequestMapping(value = "/getQrCode", method = RequestMethod.POST)
	public void getQrCode(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> json = new HashMap<String, Object>();
		ArrayList<String> list = new ArrayList<String>();
		String buffer = request.getParameter("buffer");
		String[] buffers = buffer.split(",");
		for (int i = 0; i < buffers.length; i++) {
			Area area = areaService.selectByPrimaryKey(Integer.valueOf(buffers[i]));
			StringBuffer Sbuffer = new StringBuffer();
			Sbuffer.append(area.getDistrictNumber());
			Sbuffer.append(area.getSubofficeNumber());
			Sbuffer.append(area.getCommunityNumber());
			list.add(Sbuffer.toString());
		}
	}

	/**
	 * 查询区域code
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @auoth wanglijan
	 * @date 20170414
	 */
	@RequestMapping(value = "/getAreaCodeList", method = RequestMethod.POST)
	public void getAreaCodeList(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listCode = new ArrayList<Map<String, Object>>();
		HttpSession session = request.getSession();
		// 取出会话数据
		String cityCode = (String) session.getAttribute("cityCode");
		List<Area> list = areaService.selectAllForCity(cityCode);
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", list.get(i).getAreaId());// 序号
				map.put("area", list.get(i).getDistrictName());// 行政区
				map.put("street", list.get(i).getSubofficeName());// 街道
				map.put("community", list.get(i).getCommunityName());// 社区
				map.put("revise", "生成二维码");
				listCode.add(map);
			}

		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listCode));
	}

	/**
	 * 保存家庭成员数到数据库
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	@RequestMapping(value = "/wxsavefamilyNum", method = RequestMethod.POST)
	public void wxsavefamilyNum(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();
		String familyNumValue = request.getParameter("familyNumValue");

		HttpSession session = request.getSession(false);
		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		if (openId != null) {
			StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
			statisticsParticular.setExtendedFieldOne(familyNumValue);
			statisticsParticularService.updateByPrimaryKeySelective(statisticsParticular);
			json.put("flag", "1");
			json.put("message", "成功");
			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		} else {
			json.put("flag", "2");
			json.put("message", "失败");
			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		}
	}

	/**
	 * 保存车辆信息
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	@RequestMapping(value = "/wxsaveCarInfo", method = RequestMethod.POST)
	public void saveCarInfo(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();
		BusInfo busInfo = new BusInfo();
		String arrString = request.getParameter("arr");
		String arrStringq = "";
		StatisticsParticular statisticsParticular = null;
		// 保存提交进度 如果session非等于空，不进行新建
		HttpSession session = request.getSession(false);
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "or5UjxNWEpTofPg7SJb-CA2rGexk";
		if (openId != null) {
			statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
			String city = statisticsParticular.getCtiyCode();
			if (statisticsParticular != null) {
				statisticsParticular.setSubmitStage(3);
				statisticsParticularService.updateByPrimaryKeySelective(statisticsParticular);
			}
			StringBuffer sb = new StringBuffer();
			// 保存text文件
			if (arrString != null && !"".equals(arrString)) {
				String family = statisticsParticular.getFamilyId();
				arrStringq = arrString.substring(0, arrString.length() - 1);
				String[] arr = arrStringq.split("!");
				List<String> list = new ArrayList<String>();
				sb.append("[");
				for (int i = 0; i < arr.length; i++) {
					String[] arri = arr[i].split(",");
					sb.append("{");
					for (int j = 0; j < arri.length; j++) {
						if (arri[0] != "" && !"".equals(arri[0]) && arri[0] != null) {
							if (j == 0) {
								sb.append("\"车辆排量\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 1) {
								sb.append("\"车辆公里\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 2) {
								sb.append("\"车辆车位\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 3) {
								sb.append("\"车辆属性\":");
								sb.append("\"" + arri[j] + "\",");
							}

						} else {

							if (j == 1) {
								sb.append("\"车辆排量\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 2) {
								sb.append("\"车辆公里\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 3) {
								sb.append("\"车辆车位\":");
								sb.append("\"" + arri[j] + "\",");
							}
							if (j == 4) {
								sb.append("\"车辆属性\":");
								sb.append("\"" + arri[j] + "\",");
							}

						}
					}
					String sbone = sb.toString().substring(0, sb.toString().length() - 1);
					sb = new StringBuffer();
					sb.append(sbone);
					sb.append("},");
					System.out.println(arr[i]);
				}
				String sbtwo = sb.toString().substring(0, sb.toString().length() - 1);
				sb = new StringBuffer();
				sb.append(sbtwo);
				sb.append("]");
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
				String sj = df.format(new Date());// new Date()为获取当前系统时间
				String filePathAndName = getProperties().getProperty("filePathDetail") + city + "/carInfo/car_"
						+ family.replace(":", "") + ".txt";// 路径,TXT文件命名不能包含":"
				TxtUtil.newFile(filePathAndName, sb.toString());// 将数据保存到服务器
				System.out.println("保存text文件成功，位置D:carInfo！");
			}
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("/wx/false405.jsp");
			rd.forward(request, response);
		}

		json.put("flag", "1");
		json.put("message", "成功");
		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
	}

	/**
	 * 保存家庭地址
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	@RequestMapping(value = "/wxsaveAddress", method = RequestMethod.POST)
	public void saveAddress(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();
		// 保存提交进度 如果session非等于空，不进行新建
		HttpSession session = request.getSession(false);
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
		String textAddress = request.getParameter("text");
		StringBuffer sb = new StringBuffer();
		sb.append("[{");
		sb.append("\"家庭地址\":");
		sb.append("\"" + textAddress + "\"");
		sb.append("}]");
		StatisticsParticular statisticsParticular = null;
		String familyId = "";
		if (openId != null) {

			statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
			if (statisticsParticular != null) {
				String issurveyor = statisticsParticular.getIsSurveyor();

				String communityNumber = statisticsParticular.getTitleName();
				// 查询存在的openid并且非等于7的一行数据
				if ("public".equals(communityNumber)) {

					int num = statisticsParticularService.updateByPrimaryKeySelectiveForPublic(openId, textAddress,
							communityNumber);
					if (num > 0) {
						if (num == 99) {
							json.put("flag", "99");
							json.put("message", "定位失败！");
						} else {
							json.put("flag", "1");
							json.put("message", "成功");
						}
						System.out.println("保存text文件成功，位置D:address！");
						response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
					}
				} else {
					// 统计这个社区当天总提交数
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
					String submitDate = df.format(new Date());// new
																// Date()为获取当前系统时间
					int totalCountNow = statisticsParticularService.selectTotalCountByComm(communityNumber);
					Area area = areaService.getSelectByConumber(communityNumber);
					// 该社区每天的提交数
					int totalEveryday = 0;
					// 查询该社区规定的总提交数
					int totalCount = 0;
					if (area != null) {
						totalCount = area.getTotalCount();
						totalEveryday = area.getEverydayCount();
					}
					if (totalCountNow >= totalCount) {
						json.put("flag", "3");
						json.put("message", "超出调查员每天可提交数量。");
						response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
						// request.getRequestDispatcher("/wx/beyondSubmit.jsp").forward(request,response);
					} else {
						String[] Strings = textAddress.split(",");

						if (Strings.length < 2) {
							json.put("flag", "99");
							json.put("message", "定位失败");
							// System.out.println("保存text文件成功，位置D:address！");
							response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
							return;
						}
						statisticsParticular.setAddress(Strings[0]);
						statisticsParticular.setLon(Float.parseFloat(Strings[1]));
						statisticsParticular.setLat(Float.parseFloat(Strings[2]));

						statisticsParticular.setSubmitStage(1);
						Formatter f = new Formatter(System.out);

						// 单例模式为家庭编号+1
						Singleton singleton = Singleton.getInstance();
						Map<String, Object> map = null;
						if (singleton.submitCount.size() < 1) {
							List<StatisticsParticular> sp = statisticsParticularService.selectAllNumForTitle();
							map = new HashMap<String, Object>();
							for (int i = 0; i < sp.size(); i++) {
								map.put(sp.get(i).getTitleName(), sp.get(i).getNumTitle());
							}

							singleton.setSubmitCount(map);
						}

						// 存家庭标号
						String code = statisticsParticular.getTitleName();
						String family = String.format("%03d", singleton.getCount(code));
						statisticsParticular.setFamilyId(code + family);
						// 存当前时间
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");// 设置日期格式
						String nowDate = sdf.format(new Date());// new
																// Date()为获取当前系统时间
						statisticsParticular.setSubmitDate(nowDate);
						statisticsParticularService.updateByPrimaryKeySelective(statisticsParticular);
						familyId = statisticsParticular.getFamilyId();
						String city = "";
						// 家庭编号
						if (textAddress != null && !"".equals(textAddress)) {
							city = statisticsParticular.getCtiyCode();
							String filePathAndName = getProperties().getProperty("filePathDetail") + city
									+ "/address/base_" + familyId.replace(":", "") + ".txt";// 路径,TXT文件命名不能包含":"
							TxtUtil.newFile(filePathAndName, sb.toString());// 将数据保存到服务器
						}

						if (Strings.length < 2) {
							json.put("flag", "99");
							json.put("message", "定位失败");
							// System.out.println("保存text文件成功，位置D:address！");
							response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
							return;
						} else {
							json.put("flag", "1");
							json.put("message", "成功");
							System.out.println("保存text文件成功，位置D:address！");
							response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
							return;
						}
					}
				}
			} else {
				// 查询存在openid但是等于7的一行数据。证明是调查员。
				StatisticsParticular sp = statisticsParticularService.selectBySubmitterIs(openId);
				if (sp != null) {
					int num = statisticsParticularService.insertSelectiveForOne(openId, textAddress);

					if (num > 0) {
						if (num == 99) {
							json.put("flag", "99");
							json.put("message", "定位失败！");
						} else {
							json.put("flag", "1");
							json.put("message", "成功");
						}
						System.out.println("保存text文件成功，位置D:address！");
						response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
					} else {
						json.put("flag", "2");
						json.put("message", "超出每天可提交数量。");
						response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
					}
				} else {
					json.put("flag", "5");
					json.put("message", "查询不到openId");
					response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
				}
			}

		} else {
			json.put("flag", "4");
			json.put("message", "查询不到session");
			response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		}
	}

	/**
	 * 保存车辆数量
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @author wanglijian
	 * @throws ServletException
	 * @date 20170412
	 */
	@RequestMapping(value = "/wxsaveCarNum", method = RequestMethod.POST)
	public void saveCarNum(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();
		// 保存提交进度 如果session非等于空，不进行新建
		HttpSession session = request.getSession(false);
		// String openId = "or5UjxNWEpTofPg7SJb-CA2rGexk";
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		if (openId != null) {
			StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
			if (statisticsParticular != null) {
				statisticsParticular.setSubmitStage(2);
				statisticsParticularService.updateByPrimaryKeySelective(statisticsParticular);
			}
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("/wx/false405.jsp");
			rd.forward(request, response);
		}

		json.put("flag", "1");
		json.put("message", "成功");
		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
	}

	/**
	 * 保存电动车信息
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	@RequestMapping(value = "/wxsaveEleInfo", method = RequestMethod.POST)
	public void saveEleInfo(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();
		String eleNumValue = request.getParameter("eleNumValue");
		String everdayNumValue = request.getParameter("everdayNumValue");
		StringBuffer data = new StringBuffer();
		data.append("[{");
		data.append("\"电动车数量\":");
		data.append("\"" + eleNumValue + "\"");
		data.append(",");
		data.append("\"电动车常用数量\":");
		data.append("\"" + everdayNumValue + "\"");
		data.append("}]");

		// 保存提交进度 如果session非等于空，不进行新建
		HttpSession session = request.getSession(false);
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "or5UjxNWEpTofPg7SJb-CA2rGexk";
		String city = "";
		String familyId = "";
		if (openId != null) {
			StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
			if (statisticsParticular != null) {
				city = statisticsParticular.getCtiyCode();
				familyId = statisticsParticular.getFamilyId();
				statisticsParticular.setSubmitStage(3);
				statisticsParticularService.updateByPrimaryKeySelective(statisticsParticular);
			}
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("/wx/false405.jsp");
			rd.forward(request, response);
		}
		json.put("flag", "1");
		json.put("message", "成功");
		System.out.println("保存text文件成功，位置D:eleInfo！");
		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
		if (data.toString() != null && !"".equals(data.toString())) {
			String filePathAndName = getProperties().getProperty("filePathDetail") + city + "/eleInfo/eleInfo_"
					+ familyId.replace(":", "") + ".txt";// 路径,TXT文件命名不能包含":"
			// String fileContent =
			// ZsJsonUtil.map2Json(map).toString();map数据转成字符串json
			TxtUtil.newFile(filePathAndName, data.toString());// 将数据保存到服务器
		}
	}

	/**
	 * 保存个人信息及家庭成员信息
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	@RequestMapping(value = "/wxsavePersonalInfo", method = RequestMethod.POST)
	public void savePersonalInfo(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> json = new HashMap<String, Object>();
		String arrs = request.getParameter("arrString");

		StringBuffer data = new StringBuffer();
		data.append(arrs);
		// 如果session非等于空,不进行新建
		HttpSession session = request.getSession(false);
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// String openId = "obmc-045QKmAoueQ4bqirl4f_aRQ";
		if (openId != null) {
			StatisticsParticular statisticsParticular = statisticsParticularService.selectBySubmitter(openId);
			String familyId = "";
			String city = "";
			StringBuffer sb = new StringBuffer();
			if (statisticsParticular != null) {
				familyId = statisticsParticular.getFamilyId();
				city = statisticsParticular.getCtiyCode();
				if (data.toString() != null && !"".equals(data.toString())) {

					String dataStr = data.substring(0, data.length() - 1);
					String[] arr = dataStr.split("!");
					List<String> list = new ArrayList<String>();
					sb.append("[");
					for (int i = 0; i < arr.length; i++) {
						String[] arri = arr[i].split(",");
						sb.append("{");
						for (int j = 0; j < arri.length; j++) {
							if (arri[0] != "" && !"".equals(arri[0]) && arri[0] != null) {
								if (j == 0) {
									sb.append("\"性别\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 1) {
									sb.append("\"年龄\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 2) {
									sb.append("\"是否常住本地\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 3) {
									sb.append("\"职业\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 4) {
									sb.append("\"月收入\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 5) {
									if (arri.length > 5) {
										sb.append("\"地址\":");
										sb.append("\"" + arri[j] + ":" + arri[j + 1] + ":" + arri[j + 2] + "\",");
									} else {
										sb.append("\"地址\":");
										sb.append("\"" + arri[j] + "\",");
									}
								}

							} else {

								if (j == 1) {
									sb.append("\"性别\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 2) {
									sb.append("\"年龄\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 3) {
									sb.append("\"是否常住本地\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 4) {
									sb.append("\"职业\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 5) {
									sb.append("\"月收入\":");
									sb.append("\"" + arri[j] + "\",");
								}
								if (j == 6) {
									if (arri.length > 6) {
										sb.append("\"地址\":");
										sb.append("\"" + arri[j] + ":" + arri[j + 1] + ":" + arri[j + 2] + "\",");
									} else {
										sb.append("\"地址\":");
										sb.append("\"" + arri[j] + "\",");
									}

								}
							}
						}
						String sbone = sb.toString().substring(0, sb.toString().length() - 1);
						sb = new StringBuffer();
						sb.append(sbone);
						sb.append("},");
						System.out.println(arr[i]);
					}
					String sbtwo = sb.toString().substring(0, sb.toString().length() - 1);
					sb = new StringBuffer();
					sb.append(sbtwo);
					sb.append("]");

					String filePathAndName = getProperties().getProperty("filePathDetail") + city
							+ "/personal/personalInfo_" + familyId.replace(":", "") + ".txt";// 路径,TXT文件命名不能包含":"
					TxtUtil.newFile(filePathAndName, sb.toString());// 将数据保存到服务器
					json.put("flag", "1");
					json.put("message", "成功");
					System.out.println("保存text文件成功，位置D:personal！");
					response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
				}
				// 保存提交进度
				statisticsParticular.setSubmitStage(5);
				statisticsParticularService.updateByPrimaryKeySelective(statisticsParticular);
			}
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("/wx/false405.jsp");
			try {
				rd.forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/**
	 * 查询用户填写位置并直接跳入
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletExceptionwxgetCityForTimeurl
	 * @支持GET请求
	 */
	@RequestMapping(value = "/wxgetStage")
	public void getStage(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ServletException {
		Map<String, Object> json = new HashMap<String, Object>();
		// String openId = request.getParameter("openId");
		String cityCodegd = request.getParameter("cityCodegd");
		System.out.println("++++++++++++++++++++" + cityCodegd + "+++++++++++++++++++++++");
		HttpSession session = request.getSession();
		// 取出会话数据
		String openId = (String) session.getAttribute("openId");
		// 根据城市编号和进度查询出相应的链接
		List<StatisticsParticular> spList = statisticsParticularService.selectBySubmitterForList(openId);
		String cityCode = "";
		if (spList.size() > 0) {
			cityCode = spList.get(0).getCtiyCode();
			session.setAttribute("weCityCode", cityCode);
		}

		/*
		 * if(!cityCodegd.equals(cityCode)){ RequestDispatcher rd =
		 * request.getRequestDispatcher("/wx/repeatedformap.jsp");
		 * rd.forward(request, response); }
		 */

		System.out.println("openId已经成功进入getStage方法" + openId);
		if (openId == null) {
			RequestDispatcher rd = request.getRequestDispatcher("/wx/error.jsp");
			rd.forward(request, response);
			return;
		}
		StatisticsParticular sp = statisticsParticularService.selectBySubmitter(openId);

		if (sp != null) {
			// if(!"public".equals(sp.getTitleName())){
			String city = sp.getCtiyCode();
			int stage = 99;
			String isSurveyor = sp.getIsSurveyor();
			if (sp.getSubmitStage() == null) {
				stage = 99;
			} else {
				stage = sp.getSubmitStage();
			}
			try {
				// int stage = 7;
				if (stage == 7 && !"1".equals(isSurveyor)) {
					// RequestDispatcher rd =
					// request.getRequestDispatcher("/wx/home.jsp");
					RequestDispatcher rd = request.getRequestDispatcher("/wx/repeated.jsp");
					rd.forward(request, response);
					return;
				} else if (stage == 7 && "1".equals(isSurveyor)) {
					CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(cityCode, "1");
					String url = cityCodeUrl.getCityUrl();
					// 跳转home.jsp页面
					RequestDispatcher rd = request.getRequestDispatcher(url);
					rd.forward(request, response);
					return;
				} else if (stage > 60 && stage != 99) {
					String stageStr = String.valueOf(stage);
					// 已经填写了几个人
					int stageLast = Integer.valueOf(stageStr.substring(stageStr.length() - 1, stageStr.length()));
					String familyId = sp.getFamilyId();
					// String familyId = "102123247";
					String fileNameOne = getProperties().getProperty("filePathDetail");
					String fileNameTwo = fileNameOne + city + "/personal/personalInfo_" + familyId + ".txt";
					List<String> list = ReadFromFile.readFileByLines(fileNameTwo);
					String personalinfo = "";
					List<Map<String, Object>> personalList = null;
					personalList = new ArrayList<Map<String, Object>>();
					// 将数据放到List<Map<String,Object>>里面
					for (int i = 0; i < list.size(); i++) {
						personalinfo = list.get(i);
					}

					JSONArray jsonArr = JSONArray.fromObject(personalinfo);
					List<Map<String, Object>> listforjson = new ArrayList<Map<String, Object>>();
					Iterator<JSONObject> it = jsonArr.iterator();
					while (it.hasNext()) {
						JSONObject json2 = it.next();
						listforjson.add(parseJSON2Map(json2.toString()));
					}
					int jsNum = 0;
					String agestr = "";
					for (Map<String, Object> m : listforjson) {
						for (String k : m.keySet()) {
							String age = (String) m.get("年龄");
							// int ageInt = Integer.valueOf(age);
							agestr = (String) m.get("年龄");
						}
						int ageInt = Integer.valueOf(agestr);
						if (ageInt > 6) {
							System.out.println("我是计数的，看看有多少的家庭成员" + m);
							jsNum++;
						}
					}

					if (jsNum - stageLast > 0) {
						int orientation = stageLast + 1;
						request.getRequestDispatcher("/wx/personal-outInfo-choose.jsp?pageNum=" + orientation)
								.forward(request, response);
						return;
					} else {
						// String familyId = sp.getFamilyId();
						// 获取城市是否有抽大奖活动
						CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(cityCode, "1");
						String prizeSign = cityCodeUrl.getPrizeSign();
						// 进入抽奖地址界面
						if (prizeSign == "1" || "1".equals(prizeSign)) {
							RequestDispatcher rd = request.getRequestDispatcher("/wx/prizeInfo.jsp");
							rd.forward(request, response);
							return;
						} else {
							// 进入时间调查问卷 可以传递参数
							// familyId = "12345678910";
							RequestDispatcher rd = request
									.getRequestDispatcher("/wx/time-investigation-choose-zz.jsp?family=" + familyId);
							rd.forward(request, response);
							return;
						}

					}
				} else if (stage == 1 || stage == 2) {
					RequestDispatcher rd = request.getRequestDispatcher("/wx/wxTjcar.jsp");
					rd.forward(request, response);
					return;
				} else if (stage == 3) {
					RequestDispatcher rd = request.getRequestDispatcher("/wx/personal-info.jsp");
					rd.forward(request, response);
					return;
				} else if (stage == 4) {
					RequestDispatcher rd = request.getRequestDispatcher("/wx/wxTwoWrite.jsp");
					rd.forward(request, response);
					return;
				} else if (stage == 5) {
					RequestDispatcher rd = request.getRequestDispatcher("/wx/out-info.jsp");
					rd.forward(request, response);
					return;
				} else {
					CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(cityCode, "1");
					String url = cityCodeUrl.getCityUrl();
					// 否则条进入home.jsp页面
					RequestDispatcher rd = request.getRequestDispatcher(url);
					rd.forward(request, response);
					return;
				}
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			StatisticsParticular spar = statisticsParticularService.selectBySubmitterIs(openId);
			String titleName = "";
			if (spar != null) {
				titleName = spar.getTitleName();
				if (!"public".equals(titleName)) {
					// 调查员跳转home页面
					CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(cityCode, "1");
					String url = cityCodeUrl.getCityUrl();
					RequestDispatcher rd = request.getRequestDispatcher(url);
					rd.forward(request, response);
					return;
				} else {
					RequestDispatcher rd = request.getRequestDispatcher("/wx/repeated.jsp");
					rd.forward(request, response);
					return;
				}

			} else {
				if ("public".equals(titleName)) {
					RequestDispatcher rd = request.getRequestDispatcher("/wx/repeated.jsp");
					rd.forward(request, response);
					return;
				}
				// 调查员关注了公众问卷
				StatisticsParticular sparfornummSubmitter = statisticsParticularService.selectBySubmitter(openId);
				StatisticsParticular sparfornumm = statisticsParticularService.selectBySubmitterriginal(openId);
				if (sparfornummSubmitter != null) {
					if ("public".equals(sparfornummSubmitter.getTitleName())) {
						RequestDispatcher rd = request.getRequestDispatcher("/wx/again-follow.jsp");
						rd.forward(request, response);
						return;
					}
				} else {
					if (sparfornumm != null) {
						RequestDispatcher rd = request.getRequestDispatcher("/wx/repeatedTwo.jsp");
						rd.forward(request, response);
						return;
					}
				}

				if (sparfornumm == null) {
					RequestDispatcher rd = request.getRequestDispatcher("/wx/again-follow.jsp");
					rd.forward(request, response);
					return;
				} else {
					RequestDispatcher rd = request.getRequestDispatcher("/wx/repeated.jsp");
					rd.forward(request, response);
					return;
				}

			}
		}
		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
	}

	/**
	 * 创建永久二维码
	 * 
	 * @param accessToken
	 * @param sceneId
	 *            场景Id
	 * @param sceneStr
	 *            场景IdsceneStr
	 * @return
	 */
	// 数字ID用这个{"action_name": "QR_LIMIT_SCENE", "action_info": {"scene":
	// {"scene_id": 123}}}
	// 或者也可以使用以下POST数据创建字符串形式的二维码参数：
	// 字符ID用这个{"action_name": "QR_LIMIT_STR_SCENE", "action_info": {"scene":
	// {"scene_str": "hfrunffgha"}}}
	public String createPermanentORCode(String accessToken, String sceneStr) {
		String ticket = null;
		String requestUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN";
		requestUrl = requestUrl.replace("TOKEN", accessToken);
		String outputStr = "{\"action_name\": \"QR_LIMIT_STR_SCENE\", \"action_info\":{\"scene\": {\"scene_str\":\"canshu\"}}}";
		outputStr = outputStr.replace("canshu", sceneStr);
		JSONObject jsonObject = HttpRequestUtil.httpRequestJSONObject(requestUrl, "POST", outputStr);
		if (null != jsonObject) {
			try {
				ticket = jsonObject.getString("ticket");
				System.out.println("创建永久带参二维码成功,ticket=" + ticket);
			} catch (Exception e) {
				String errorCode = jsonObject.getString("errcode");
				System.out.println("创建永久带参二维码失败,错误码是=" + errorCode);
				String errorMsg = jsonObject.getString("errmsg");
				System.out.println("创建永久带参二维码失败,错误信息是=" + errorMsg);
			}
		}
		return ticket;
	}

	/**
	 * 通过ticket换取二维码 ticket正确情况下，是一张图片，可以直接展示或者下载
	 * 
	 * @param ticket
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String showQRcode(String ticket) throws UnsupportedEncodingException {
		String showUrl = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET";
		showUrl = showUrl.replace("TICKET", URLEncoder.encode(ticket, "UTF-8"));
		return showUrl;
	}

	/**
	 * 保存二维码到本地
	 * 
	 * @param ticket
	 * @return
	 */
	public static void download(String urlString, String filenamefor, String savePath) throws Exception {
		// 构造URL
		URL url = new URL(urlString);
		// 打开连接
		URLConnection con = url.openConnection();
		// 设置请求超时为5s
		con.setConnectTimeout(5 * 1000);
		// 输入流
		InputStream is = con.getInputStream();

		// 1K的数据缓冲
		byte[] bs = new byte[1024];
		// 读取到的数据长度
		int len;
		// 输出的文件流
		File sf = new File(savePath);
		if (!sf.exists()) {
			sf.mkdirs();
		}
		OutputStream os = new FileOutputStream(sf.getPath() + "/" + filenamefor);
		// 开始读取
		while ((len = is.read(bs)) != -1) {
			os.write(bs, 0, len);
		}
		// 完毕，关闭所有链接
		os.close();
		is.close();
	}

	@Override
	public void run() {
		while (true) {
			try {
				// 调用工具类获取access_token(每日最多获取2000次，每次获取的有效期为7200秒)
				System.out.println("accessToken获取成功");
				token = AccessTokenUtil.getAccessToken(appID, appScret);
				if (null != token) {
					System.out.println("accessToken获取成功：" + token.getAccess_token());
					// 创建与删除自定义菜单方法
					// this.createMenu();
					// this.deleteMenu();
					// 7000秒之后重新进行获取
					Thread.sleep((token.getExpires_in() - 200) * 1000);
				} else {
					// 60秒之后尝试重新获取
					Thread.sleep(60 * 1000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	// 查询的社区数量
	@RequestMapping(value = "/getTitleNameNumber")
	public void getTitleNameNumber() {
		Map<Object, Object> map = new HashMap<Object, Object>();
		List<StatisticsParticular> sp = statisticsParticularService.selectAllNumForTitle();
		for (int i = 0; i < sp.size(); i++) {
			StatisticsParticular sps = sp.get(i);
		}
	}

}
