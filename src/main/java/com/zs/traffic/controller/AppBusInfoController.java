package com.zs.traffic.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.traffic.model.AppBusInfo;
import com.zs.traffic.model.SysAppuser;
import com.zs.traffic.service.AppBusInfoService;
import com.zs.traffic.service.SysAppuserService;
import com.zs.utils.TxtUtil;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;


@Controller
@RequestMapping("/app")
public class AppBusInfoController extends BaseController{
	
	@Autowired
	public AppBusInfoService service;
	@Autowired
	public SysAppuserService sysAppuserService;
	
	
	/**
	 * @description APP端车辆信息提交  (版本更新后弃用)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-03-31
	 */
	@RequestMapping(value="/insertBusInfo",method=RequestMethod.POST)
	public void insertBusInfo(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception{
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		//Integer appuserId =  (Integer) session.getAttribute("appuserId");
		//String userName =  (String) session.getAttribute("userName");//账户电话号码,保存文件是文件名用
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String userName = request.getParameter("userName");//必传字段，不能为空
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
	
		//String displacement = request.getParameter("displacement").trim();//排量
		//String actualServiceLife = request.getParameter("actualServiceLife").trim();//里程
		//String busProperty = request.getParameter("busProperty").trim();//车辆性能
		//String  isfixedParking = request.getParameter("isfixedParking").trim();//是否有固定车位
		//String busType = request.getParameter("busType").trim();
		String busInfos = request.getParameter("busInfo").trim();//车辆信息
		String  stag = request.getParameter("stag").trim();//提交记录
		int ret = 0;
		if(busInfos == ""){//车辆信息不填的话，直接通过
			SysAppuser appUser = new SysAppuser();//对应的实体类
         	if(!stag.isEmpty()){
     			Integer sta = Integer.parseInt(stag);//显示提交步骤
     			appUser.setStag(sta);
     		}
         	appUser.setAppuserId(appuserId);
         	sysAppuserService.updateByPrimaryKeySelective(appUser);//记录提交步骤
    		return;
		}else{
			String[] bus = busInfos.split(",");
			for(String busInfo : bus){
				String[] busInfomation = busInfo.split(" ");
				String displacement = busInfomation[0];
				String actualServiceLife = busInfomation[1];
				String busProperty =  busInfomation[2];
				String isfixedParking =  busInfomation[3];
				
				AppBusInfo model = new AppBusInfo();
				
				model.setDisplacement(Float.parseFloat(displacement));
				model.setActualServiceLife(Float.parseFloat(actualServiceLife));
				model.setBusProperty(Integer.parseInt(busProperty));  
				model.setIsfixedParking(Integer.parseInt(isfixedParking));
				model.setAppuserId(appuserId);
				service.insertSelective(model);
				ret++;
			}
		}
		/*if(!displacement.isEmpty()){
			Float displacemen =  Float.parseFloat(request.getParameter("displacement").trim());
			model.setDisplacement(displacemen);
		}
		if(!actualServiceLife.isEmpty()){
			Float actualServiceLif =  Float.parseFloat(actualServiceLife);
			model.setActualServiceLife(actualServiceLif);
		}
        if(!isfixedParking.isEmpty()){
        	Integer isfixedParkin = Integer.parseInt(isfixedParking);
        	model.setIsfixedParking(isfixedParkin);
        }
        if(!busType.isEmpty()){
        	model.setBusType(busType);
        }
        if(!busProperty.isEmpty()){
        	  Integer busPropert = Integer.parseInt(busProperty);
        	  model.setBusProperty(busPropert);        	 
        }
        model.setAppuserId(appuserId);
        */
       // int ret = service.insertSelective(model);
        if(ret>0){
        	SysAppuser appUser = new SysAppuser();//对应的实体类
         	if(!stag.isEmpty()){
     			Integer sta = Integer.parseInt(stag);//显示提交步骤
     			appUser.setStag(sta);
     		}
         	appUser.setAppuserId(appuserId);
         	sysAppuserService.updateByPrimaryKeySelective(appUser);//记录提交步骤
           // AppBusInfo appBusInfo = service.selectByAppuserId(appuserId);
			map.put("appBusInfo", busInfos);
			String filePathAndName = getProperties().getProperty("filePath")+userName+"Car.txt";
			String fileContent = ZsJsonUtil.map2Json(map).toString();
			map.remove("appBusInfo");
			TxtUtil.newFile(filePathAndName,fileContent);
        	map.put("errNo", "0");
    		map.put("result", "保存成功！");
    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    		return;
        }else{
        	map.put("errNo", "-1");
    		map.put("result", "保存失败，请稍候再试");
    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    		return;
        }
	
	}
	
	/**
	 * @description APP端车辆信息修改  (版本更新后弃用)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-03-31
	 */
	@RequestMapping(value="/updateBusInfo",method=RequestMethod.POST)
	public void updateBusInfo(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception{
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		//Integer appuserId =  (Integer) session.getAttribute("appuserId");
		//String userName =  (String) session.getAttribute("userName");//账户电话号码,保存文件是文件名用
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String userName = request.getParameter("userName");//必传字段，不能为空
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		
		/*String busInfoId = request.getParameter("id").trim();//id必传字段
		Integer busid = Integer.parseInt(busInfoId);
*/		
		String busInfos = request.getParameter("busInfo").trim();//车辆信息
		
		int ret = service.deleteByAppUserId(appuserId);//修改车辆前先删除
		if(ret<=0){
			map.put("errNo", "-1");
    		map.put("result", "保存失败，请稍候再试");
    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    		return;
		}
		int retu = 0;
		String[] bus = busInfos.split(",");
		
		for(String busInfo : bus){
			String[] busInfomation = busInfo.split(" ");
			String displacement = busInfomation[0];
			String actualServiceLife = busInfomation[1];
			String busProperty =  busInfomation[2];
			String isfixedParking =  busInfomation[3];
			//String busInfoId =  busInfomation[4];//修改时id必填
			
			AppBusInfo model = new AppBusInfo();
			
			model.setDisplacement(Float.parseFloat(displacement));
			model.setActualServiceLife(Float.parseFloat(actualServiceLife));
			model.setBusProperty(Integer.parseInt(busProperty));  
			model.setIsfixedParking(Integer.parseInt(isfixedParking));
			model.setAppuserId(appuserId);
			//model.setId(Integer.parseInt(busInfoId));//根据主键id更新
			service.insertSelective(model);
			retu++;
		}
		
		/*String displacement = request.getParameter("displacement").trim();//排量
		String actualServiceLife = request.getParameter("actualServiceLife").trim();//里程
		String  isfixedParking = request.getParameter("isfixedParking").trim();//是否有固定车位
		String busType = request.getParameter("busType").trim();
		String busProperty = request.getParameter("busProperty").trim();//车辆性能
		if(!displacement.isEmpty()){
			Float displacemen =  Float.parseFloat(request.getParameter("displacement").trim());
			model.setDisplacement(displacemen);
		}
		if(!actualServiceLife.isEmpty()){
			Float actualServiceLif =  Float.parseFloat(actualServiceLife);
			model.setActualServiceLife(actualServiceLif);
		}
        if(!isfixedParking.isEmpty()){
        	Integer isfixedParkin = Integer.parseInt(isfixedParking);
        	model.setIsfixedParking(isfixedParkin);
        }
        if(!busType.isEmpty()){
        	model.setBusType(busType);
        }
        if(!busProperty.isEmpty()){
        	  Integer busPropert = Integer.parseInt(busProperty);
        	  model.setBusProperty(busPropert);        	 
        }
        model.setAppuserId(appuserId);
        model.setId(busid);//必填字段 主键
*/       
       // int ret = service.updateByPrimaryKeySelective(model);
        if(retu>0){
        	//AppBusInfo appBusInfo = service.selectByPrimaryKey(busid);
        	//map.put("appBusInfo", appBusInfo);
        	map.put("appBusInfo", busInfos);
 			String filePathAndName = getProperties().getProperty("filePath")+userName+"Car.txt";
 			String fileContent = ZsJsonUtil.map2Json(map).toString();
 			map.remove("appBusInfo");
 			TxtUtil.newFile(filePathAndName,fileContent);
        	map.put("errNo", "0");
    		map.put("result", "修改成功！");
    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    		return;
        }else{
        	map.put("errNo", "-1");
    		map.put("result", "修改失败，请稍候再试");
    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    		return;
        }
	
	}
	
	
	/**
	 * @description APP端车辆信息查询  (版本更新后弃用)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-03-31
	 */
	@RequestMapping(value="/queryBusInfo",method=RequestMethod.POST)
	public void queryBusInfo(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception{
		request.setCharacterEncoding("UTF-8");
		
		List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
		
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		//Integer appuserId =  (Integer) session.getAttribute("appuserId");
		//String userName =  (String) session.getAttribute("userName");//账户电话号码,保存文件是文件名用
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String userName = request.getParameter("userName");//必传字段，不能为空
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
	
        List<AppBusInfo> list = service.selectByAppuserId(appuserId);
        if(list.size()>0){
        	for(int i = 0; i < list.size(); i++){
        		Map<String, Object> map = new HashMap<String, Object>();
        		//System.out.println(list.get(i).getId());
        		map.put("busInfoId", list.get(i).getId());//车辆信息id，唯一标识
        		map.put("appUserId", list.get(i).getAppuserId());//登录用户id
    			map.put("displacement", list.get(i).getDisplacement());
    			map.put("actualServiceLife", list.get(i).getActualServiceLife());
    			map.put("busProperty", list.get(i).getBusProperty());
    			map.put("isfixedParking", list.get(i).getIsfixedParking());
    			listResult.add(map);
        	}
        	response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
        }
	
	}


}
