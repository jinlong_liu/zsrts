package com.zs.traffic.controller;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.mysql.jdbc.Statement;

/**
 * @description 获取可查询的数据的日期列表
 * 
 * @author renxw
 * 
 * @date 2016-06-18
 * 
 */

public  class  AreaJdbcUtil {
	private static String url = null;
	private static String user = null;
	private static String password = null;
	private static String driverClass = null;

	/**
	 * 静态代码块中（只加载一次）
	 */
	static {
		try {
			// 读取db.properties文件
			Properties props = new Properties();
			/**
			 * . 代表java命令运行的目录 在java项目下，. java命令的运行目录从项目的根目录开始 在web项目下， .
			 * java命令的而运行目录从tomcat/bin目录开始 所以不能使用点.
			 */
			// FileInputStream in = new FileInputStream("./src/db.properties");

			/**
			 * 使用类路径的读取方式 / : 斜杠表示classpath的根目录 在java项目下，classpath的根目录从bin目录开始
			 * 在web项目下，classpath的根目录从WEB-INF/classes目录开始
			 */
			InputStream in = AreaJdbcUtil.class
					.getResourceAsStream("/conf/jdbc.properties");

			// 加载文件
			props.load(in);
			// 读取信息
			url = props.getProperty("jdbc_url");
			user = props.getProperty("jdbc_username");
			password = props.getProperty("jdbc_password");
			driverClass = "com.mysql.jdbc.Driver";

			// 注册驱动程序
			Class.forName(driverClass);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("驱程程序注册出错");
		}
	}

	/**
	 * 抽取获取连接对象的方法
	 */
	public static Connection getConnection() {
		try {
			Connection conn = DriverManager.getConnection(url, user, password);
			return conn;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * 释放资源的方法
	 */
	public static void close(Connection conn, Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}
	
	public static  void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}

	public static void close(Connection conn, Statement stmt, ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
				throw new RuntimeException(e1);
			}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}

	public static synchronized  int insertTable(List<Map<String, Object>> dataList,Integer type) {
		Connection conn = AreaJdbcUtil.getConnection();
		int executeBatch =-1;	
		try {
			String sql ="";
			PreparedStatement pstmt =null;
			if(type==1){
				 sql ="insert into area ( district_number, district_name,type,city_code) values (?,?,?,?) ";
				  pstmt = conn.prepareStatement(sql);	
				 // 循环生成数据
					for (int i = 0; i < dataList.size(); i++) {
						if (!String.valueOf(dataList.get(i).get("districtNumber")).equals("null")) {
							pstmt.setString(1, String.valueOf(dataList.get(i).get("districtNumber")
									));
						} else {
							pstmt.setString(1, "0");
						}

						if (!String.valueOf(dataList.get(i).get("districtName")).equals("null")) {
							pstmt.setString(2, String.valueOf(dataList.get(i).get("districtName")
									));
						} else {
							pstmt.setString(2, "null");
						}

						if (!String.valueOf(dataList.get(i).get("type")).equals("null")) {
							pstmt.setString(3, String.valueOf(dataList.get(i).get("type")
									));
						} else {
							pstmt.setString(3, "null");
						}
						if (!String.valueOf(dataList.get(i).get("cityCode")).equals("null")) {
							pstmt.setString(4, String.valueOf(dataList.get(i).get("cityCode")
									));
						} else {
							pstmt.setString(4, "null");
						}

					
						pstmt.addBatch();
					}
			
			}else if(type==2){
				sql ="insert into area ( district_number, district_name,type,parent_suboffice, suboffice_number, suboffice_name, city_code ) values (?,?,?,?,?,?,?) ";
				  pstmt = conn.prepareStatement(sql);	
				 // 循环生成数据
					for (int i = 0; i < dataList.size(); i++) {
						if (!String.valueOf(dataList.get(i).get("districtNumber")).equals("null")) {
							pstmt.setString(1, String.valueOf(dataList.get(i).get("districtNumber")
									));
						} else {
							pstmt.setString(1, "null");
						}

						if (!String.valueOf(dataList.get(i).get("districtName")).equals("null")) {
							pstmt.setString(2, String.valueOf(dataList.get(i).get("districtName")
									));
						} else {
							pstmt.setString(2, "null");
						}

						if (!String.valueOf(dataList.get(i).get("type")).equals("null")) {
							pstmt.setString(3, String.valueOf(dataList.get(i).get("type")
									));
						} else {
							pstmt.setString(3, "null");
						}
						if (!String.valueOf(dataList.get(i).get("districtNumber")).equals("null")) {
							pstmt.setString(4, String.valueOf(dataList.get(i).get("districtNumber")
									));
						} else {
							pstmt.setString(4, "null");
						}
						if (!String.valueOf(dataList.get(i).get("subofficeNumber")).equals("null")) {
							pstmt.setString(5, String.valueOf(dataList.get(i).get("subofficeNumber")
									));
						} else {
							pstmt.setString(5, "null");
						}
						if (!String.valueOf(dataList.get(i).get("subofficeName")).equals("null")) {
							pstmt.setString(6, String.valueOf(dataList.get(i).get("subofficeName")
									));
						} else {
							pstmt.setString(6, "null");
						}
						if (!String.valueOf(dataList.get(i).get("cityCode")).equals("null")) {
							pstmt.setString(7, String.valueOf(dataList.get(i).get("cityCode")
									));
						} else {
							pstmt.setString(7, "null");
						}
					
						pstmt.addBatch();
					}
			}else if(type==3){
				sql ="insert into area ( district_number, district_name,type,parent_suboffice, suboffice_number, suboffice_name, "
     +" parent_communiity, community_number, community_name, "+
      " total_count, everyday_count ,city_code) values (?,?,?,?,?,?,?,?,?,?,?,?) ";
				  pstmt = conn.prepareStatement(sql);	
				 // 循环生成数据
					for (int i = 0; i < dataList.size(); i++) {
						if (!String.valueOf(dataList.get(i).get("districtNumber")).equals("null")) {
							pstmt.setString(1, String.valueOf(dataList.get(i).get("districtNumber")
									));
						} else {
							pstmt.setString(1, "null");
						}

						if (!String.valueOf(dataList.get(i).get("districtName")).equals("null")) {
							pstmt.setString(2, String.valueOf(dataList.get(i).get("districtName")
									));
						} else {
							pstmt.setString(2, "null");
						}

						if (!String.valueOf(dataList.get(i).get("type")).equals("null")) {
							pstmt.setString(3, String.valueOf(dataList.get(i).get("type")
									));
						} else {
							pstmt.setString(3, "null");
						}
						if (!String.valueOf(dataList.get(i).get("districtNumber")).equals("null")) {
							pstmt.setString(4, String.valueOf(dataList.get(i).get("districtNumber")
									));
						} else {
							pstmt.setString(4, "null");
						}
						if (!String.valueOf(dataList.get(i).get("subofficeNumber")).equals("null")) {
							pstmt.setString(5, String.valueOf(dataList.get(i).get("subofficeNumber")
									));
						} else {
							pstmt.setString(5, "null");
						}
						if (!String.valueOf(dataList.get(i).get("subofficeName")).equals("null")) {
							pstmt.setString(6, String.valueOf(dataList.get(i).get("subofficeName")
									));
						} else {
							pstmt.setString(6, "null");
						}
						if (!String.valueOf(dataList.get(i).get("subofficeNumber")).equals("null")) {
							pstmt.setString(7, String.valueOf(dataList.get(i).get("subofficeNumber")
									));
						} else {
							pstmt.setString(7, "null");
						}
						if (!String.valueOf(dataList.get(i).get("communityNumber")).equals("null")) {
							pstmt.setString(8, String.valueOf(dataList.get(i).get("communityNumber")
									));
						} else {
							pstmt.setString(8, "null");
						}
						if (!String.valueOf(dataList.get(i).get("communityName")).equals("null")) {
							pstmt.setString(9, String.valueOf(dataList.get(i).get("communityName")
									));
						} else {
							pstmt.setString(9, "null");
						}
						if (!String.valueOf(dataList.get(i).get("totalCount")).equals("null")) {
							pstmt.setString(10, String.valueOf(dataList.get(i).get("totalCount")
									));
						} else {
							pstmt.setString(10, "null");
						}
						if (!String.valueOf(dataList.get(i).get("everydayCount")).equals("null")) {
							pstmt.setString(11, String.valueOf(dataList.get(i).get("everydayCount")
									));
						} else {
							pstmt.setString(11, "null");
						}
						if (!String.valueOf(dataList.get(i).get("cityCode")).equals("null")) {
							pstmt.setString(12, String.valueOf(dataList.get(i).get("cityCode")
									));
						} else {
							pstmt.setString(12, "null");
						}
					
						pstmt.addBatch();
					}
			}

			pstmt.executeBatch();
			pstmt.clearBatch();
			executeBatch=1;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			executeBatch=-1;
			e.printStackTrace();
		}finally{
			AreaJdbcUtil.close(conn);
		}
		return executeBatch;

	}

	public static synchronized  void updeteTable(List<Map<String, Object>> dataList) {
		Connection conn = AreaJdbcUtil.getConnection();
		
		try {
			String sql = "update BUSREALTIMETABLE set"
					+ " STARTSTATIONNAME=?,ENDSTATIONNAME=?,SNGSERIAL=?,NEXTSTOPNAME=?,LONGITUDE=?"
					+ ",UPSTOPNAMES=?,LATITUDE=?,BUSSPEED=?,LINENAME=?,DIRECTION=?,STATIONSORT=?"
					+ ",UPSORT=?,UPSTATION=?,DOWNSORT=?,DOWNSTOPNAMES=?,DOWNSTATION=?,CURTIME=?"
					+ " WHERE BUSNUMBER=?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			// 循环生成数据
			for (int i = 0; i < dataList.size(); i++) {
				if (!String.valueOf(dataList.get(i).get("StartStationName")).equals("null")) {
					pstmt.setString(1, String.valueOf(dataList.get(i).get("StartStationName")
							));
				} else {
					pstmt.setString(1, "null");
				}

				if (!String.valueOf(dataList.get(i).get("EndStationName")).equals("null")) {
					pstmt.setString(2, String.valueOf(dataList.get(i).get("EndStationName")
							));
				} else {
					pstmt.setString(2, "null");
				}

				if (!String.valueOf(dataList.get(i).get("SngSerial")).equals("null")) {
					pstmt.setString(3, String.valueOf(dataList.get(i).get("SngSerial")
							));
				} else {
					pstmt.setString(3, "null");
				}

				if (!String.valueOf(dataList.get(i).get("NextStopName")).equals("null")) {
					pstmt.setString(4, String.valueOf(dataList.get(i).get("NextStopName")
							));
				} else {
					pstmt.setString(4, "null");
				}

				if (!String.valueOf(dataList.get(i).get("Longitude")).equals("null")) {
					pstmt.setString(5, String.valueOf(dataList.get(i).get("Longitude")
							));
				} else {
					pstmt.setString(5, "0");
				}

				if (!String.valueOf(dataList.get(i).get("UpStopNames")).equals("null")) {
					pstmt.setString(6, String.valueOf(dataList.get(i).get("UpStopNames")
							));
				} else {
					pstmt.setString(6, "null");
				}

				if (!String.valueOf(dataList.get(i).get("Latitude")).equals("null")) {
					pstmt.setString(7, String.valueOf(dataList.get(i).get("Latitude")
							));
				} else {
					pstmt.setString(7, "0");
				}

				if (!String.valueOf(dataList.get(i).get("BusSpeed")).equals("null")) {
					pstmt.setString(8, String.valueOf(dataList.get(i).get("BusSpeed")
							));
				} else {
					pstmt.setString(8, "0");
				}
				if (!String.valueOf(dataList.get(i).get("LineName")).equals("null")) {
					pstmt.setString(9, String.valueOf(dataList.get(i).get("LineName")
							));
				} else {
					pstmt.setString(9, "null");
				}
				if (!String.valueOf(dataList.get(i).get("Direction")).equals("null")) {
					pstmt.setString(10, String.valueOf(dataList.get(i).get("Direction")
							));
				} else {
					pstmt.setString(10, "null");
				}
				if (!String.valueOf(dataList.get(i).get("StationSort")).equals("null")) {
					pstmt.setString(11, String.valueOf(dataList.get(i).get("StationSort")
							));
				} else {
					pstmt.setString(11, "-1");
				}
				if (!String.valueOf(dataList.get(i).get("UpSort")).equals("null")) {
					pstmt.setString(12, String.valueOf(dataList.get(i).get("UpSort")
							));
				} else {
					pstmt.setString(12, "-1");
				}
				if (!String.valueOf(dataList.get(i).get("UpStation")).equals("null")) {
					pstmt.setString(13, String.valueOf(dataList.get(i).get("UpStation")
							));
				} else {
					pstmt.setString(13, "null");
				}

				if (!String.valueOf(dataList.get(i).get("DownSort")).equals("null")) {
					pstmt.setString(14, String.valueOf(dataList.get(i).get("DownSort")
							));
				} else {
					pstmt.setString(14, "-1");
				}

				if (!String.valueOf(dataList.get(i).get("DownStopNames")).equals("null")) {
					pstmt.setString(15, String.valueOf(dataList.get(i).get("DownStopNames")
							));
				} else {
					pstmt.setString(15, "null");
				}

				if (!String.valueOf(dataList.get(i).get("DownStation")).equals("null")) {
					pstmt.setString(16, String.valueOf(dataList.get(i).get("DownStation")
							));
				} else {
					pstmt.setString(16, "null");
				}

				if (!String.valueOf(dataList.get(i).get("CurTime")).equals("null")) {
					pstmt.setString(17, String.valueOf(dataList.get(i).get("CurTime")
							));
				} else {
					pstmt.setString(17, "null");
				}

				if (!String.valueOf(dataList.get(i).get("BusNumber")).equals("null")) {
					pstmt.setString(18, String.valueOf(dataList.get(i).get("BusNumber")
							));
				} else {
					pstmt.setString(18, "null");
				}

				pstmt.addBatch();
			}

			 pstmt.executeBatch();
			pstmt.clearBatch();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}finally{
			AreaJdbcUtil.close(conn);
		}
		
	}

	public static synchronized  void deleteTable(List<Map<String, Object>> dataList) {
		Connection conn = AreaJdbcUtil.getConnection();
		try {
			String sql = "delete from BUSREALTIMETABLE where BUSNUMBER =?";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			// 循环生成数据
			for (int i = 0; i < dataList.size(); i++) {
				pstmt.setString(1, String.valueOf(dataList.get(i).get("BusNumber")));
				pstmt.addBatch();
			}

			pstmt.executeBatch();
			pstmt.clearBatch();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			AreaJdbcUtil.close(conn);
		}

	}
}
