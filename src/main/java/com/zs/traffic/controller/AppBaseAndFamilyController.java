package com.zs.traffic.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.zs.traffic.model.AppFamilyInformation;
import com.zs.traffic.model.SysAppuser;
import com.zs.traffic.service.AppFamilyInformationService;
import com.zs.traffic.service.SysAppuserService;
import com.zs.utils.TxtUtil;
import com.zs.utils.VerificationUtil;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;

import java.util.List;

@Controller
@RequestMapping("/app")
class AppBaseAndFamilyController extends BaseController  {

	@Autowired
	public SysAppuserService service;
	@Autowired
	public AppFamilyInformationService appFamilyInformationService;
	
	/**
	 * @description APP端个人及家庭信息提交
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author wangzf
	 * @date 2017-06-21
	 */
	@RequestMapping(value = "/insertBaseAndFamilyInfo", method = RequestMethod.POST)
	public void insertBaseAndFamilyInfo(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		SysAppuser model = new SysAppuser();// 对应的实体类

		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		//HttpSession session = servletRequest.getSession();
		
		String id = request.getParameter("appuserId");// 必传字段，不能为空
		String userName = request.getParameter("userName");// 必传字段，不能为空

		String familyAddress = request.getParameter("familyAddress");
		String stag = request.getParameter("stag");
		String electricBikes = request.getParameter("electricBikes");// 电动车数量
		String carCount = request.getParameter("carCount");// 汽车数量
		String homesCount = request.getParameter("homesCount");// 家庭成员数量
		//接收个人信息
        String cityCode = request.getParameter("cityCode");//必传字段，不能为空
        String sex = request.getParameter("sex");
		String age = request.getParameter("age");
		String isPermanentAddress = request.getParameter("isPermanentAddress");//是否常住
		String income = request.getParameter("income");
		String profession = request.getParameter("profession");
		//String icCard = request.getParameter("icCard").trim();
		String companyAddress = request.getParameter("companyAddress");
		
		if(id==null || userName==null || familyAddress==null || stag==null ||
				electricBikes==null || carCount==null || homesCount==null ||
				cityCode==null || sex==null || age==null || isPermanentAddress == null ||
				income == null || profession==null || companyAddress==null){
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		id = id.trim();
		userName = userName.trim();
		familyAddress = familyAddress.trim();
		stag = stag.trim();
		electricBikes = electricBikes.trim();
		carCount = carCount.trim();
		homesCount = homesCount.trim();
		cityCode = cityCode.trim();
		sex = sex.trim();
		age = age.trim();
		isPermanentAddress = isPermanentAddress.trim();
		income = income.trim();
		profession = profession.trim();
		companyAddress = companyAddress.trim();
		
		if(id.isEmpty() || userName.isEmpty() || stag.isEmpty() ||
				electricBikes.isEmpty() || carCount.isEmpty() || homesCount.isEmpty() ||
				cityCode.isEmpty() || sex.isEmpty() || age.isEmpty() || isPermanentAddress.isEmpty() ||
				profession.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		
		if(id.length()>11 || userName.length()>20 || stag.length()>11 || electricBikes.length()>11 || carCount.length()>11 || homesCount.length()>11 || 
				familyAddress.length()>2000 || cityCode.length()>255 || sex.length()>11 || age.length()>11 || isPermanentAddress.length()>11 || 
				companyAddress.length()>2000 || profession.length()>11 || income.length()>20){
			map.put("errNo", "-5");
	 		map.put("result", "参数长度超出范围");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
	
		if(VerificationUtil.isNumeric(id)  || VerificationUtil.isNumeric(stag) ||
				VerificationUtil.isNumeric(electricBikes) || VerificationUtil.isNumeric(carCount) ||
				VerificationUtil.isNumeric(homesCount) || VerificationUtil.isNumeric(sex) ||
				VerificationUtil.isNumeric(age) || VerificationUtil.isNumeric(isPermanentAddress) ||
				VerificationUtil.isNumeric(profession)){
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		
		Integer appuserId = Integer.parseInt(id);// 转成Integer类型
		if(!familyAddress.isEmpty()){
			model.setFamilyAddress(familyAddress);
		}
		Integer sta = Integer.parseInt(stag);
		model.setStag(sta);
		Integer electricBike = Integer.parseInt(electricBikes);
		model.setElectricBikes(electricBike);
		Integer carCoun = Integer.parseInt(carCount);
		model.setCarCount(carCoun);
		Integer homesCoun = Integer.parseInt(homesCount);
		model.setHomesCount(homesCoun);
		model.setAppuserId(appuserId);
		
		int ret = service.updateByPrimaryKeySelective(model);
		
		AppFamilyInformation personInfo = new AppFamilyInformation();//对应的实体
		
		Integer se = Integer.parseInt(sex);
		personInfo.setSex(se);
		Integer ag = Integer.parseInt(age);
		personInfo.setAge(ag);
		Integer isPermanentAddres = Integer.parseInt(isPermanentAddress);
		personInfo.setIsPermanentAddress(isPermanentAddres);
		Integer professio = Integer.parseInt(profession);
		personInfo.setProfession(professio);
		if(!income.isEmpty()){
			personInfo.setIncome(income);
		}
		/*if(!icCard.isEmpty()){
			model.setIcCard(icCard);
		}*/
		if(!companyAddress.isEmpty()){
			personInfo.setCompanyAddress(companyAddress);
		}
		
		personInfo.setAppuserId(appuserId);
		
		int retu = appFamilyInformationService.insertSelective(personInfo);
		
		if (ret > 0 && retu >0) {
			SysAppuser appUser = service.selectByPrimaryKey(appuserId);
			// map.put("appUser", appUser);
			map.put("appuserId", appUser.getAppuserId());
			map.put("userName", appUser.getUserName());
			map.put("userPassword", appUser.getUserPassword());
			map.put("createDate", appUser.getCreateDate());
			map.put("electricBikes", appUser.getElectricBikes());
			map.put("familyAddress", appUser.getFamilyAddress());
			map.put("homesCount", appUser.getHomesCount());
			map.put("carCount", appUser.getCarCount());
			map.put("stag", appUser.getStag());
			
			map.put("cityCode", appUser.getCityCode());

			String filePathAndName = getProperties().getProperty("filePathDetail") +"Family/"+appUser.getCityCode()+"/"+ appUser.getUserName() + "family.txt";
			String fileContent = ZsJsonUtil.map2Json(map).toString();
			map.clear();
			TxtUtil.newFile(filePathAndName, fileContent);
			
			//保存个人信息文件
			map.put("appuserId", appuserId);//登录用户id
	 	    map.put("sex", sex);
			map.put("age", age);
			map.put("isPermanentAddress", isPermanentAddress);
			map.put("profession", profession);
			map.put("income", income);
			map.put("companyAddress", companyAddress);
	 		filePathAndName = getProperties().getProperty("filePathDetail")+"Base/"+cityCode+"/"+userName+"base.txt";
	 		fileContent = ZsJsonUtil.map2Json(map).toString();
	 		map.clear();
	 		TxtUtil.newFile(filePathAndName,fileContent);
			
			map.put("errNo", "0");
			map.put("result", "保存成功!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		} else {
			map.put("errNo", "-1");
			map.put("result", "保存失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
	}
	
	/**
	 * @description APP端个人及家庭信息修改
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author wangzf
	 * @date 2017-06-21
	 */
	@RequestMapping(value = "/updateBaseAndFamilyInfo", method = RequestMethod.POST)
	public void updateBaseAndFamilyInfo(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		SysAppuser model = new SysAppuser();// 对应的实体类

		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		//HttpSession session = servletRequest.getSession();
		
		String id = request.getParameter("appuserId");// 必传字段，不能为空
		String userName = request.getParameter("userName");// 必传字段，不能为空
		
		String familyAddress = request.getParameter("familyAddress");
		String stag = request.getParameter("stag");
		String electricBikes = request.getParameter("electricBikes");// 电动车数量
		String carCount = request.getParameter("carCount");// 汽车数量
		String homesCount = request.getParameter("homesCount");// 家庭成员数量
		
		//接收个人信息
        String cityCode = request.getParameter("cityCode");//必传字段，不能为空
        String familyId = request.getParameter("familyId");//必传id
        String sex = request.getParameter("sex");
		String age = request.getParameter("age");
		String isPermanentAddress = request.getParameter("isPermanentAddress");//是否常住
		String income = request.getParameter("income");
		String profession = request.getParameter("profession");
		//String icCard = request.getParameter("icCard").trim();
		String companyAddress = request.getParameter("companyAddress");
		
		if(id==null || userName==null || familyAddress==null || stag==null ||
				electricBikes==null || carCount==null || homesCount==null ||
				cityCode==null || sex==null || age==null || isPermanentAddress == null ||
				income == null || profession==null || companyAddress==null || familyId == null){
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		id = id.trim();
		userName = userName.trim();
		familyAddress = familyAddress.trim();
		stag = stag.trim();
		electricBikes = electricBikes.trim();
		carCount = carCount.trim();
		homesCount = homesCount.trim();
		cityCode = cityCode.trim();
		familyId = familyId.trim();
		sex = sex.trim();
		age = age.trim();
		isPermanentAddress = isPermanentAddress.trim();
		income = income.trim();
		profession = profession.trim();
		companyAddress = companyAddress.trim();
		if(id.isEmpty() || userName.isEmpty() || stag.isEmpty() ||
				electricBikes.isEmpty() || carCount.isEmpty() || homesCount.isEmpty() ||
				cityCode.isEmpty() || sex.isEmpty() || age.isEmpty() || isPermanentAddress.isEmpty() ||
				profession.isEmpty() || familyId.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(id.length()>11 || userName.length()>20 || stag.length()>11 || electricBikes.length()>11 || carCount.length()>11 || homesCount.length()>11 || 
				familyAddress.length()>2000 || cityCode.length()>255 || sex.length()>11 || age.length()>11 || isPermanentAddress.length()>11 || 
				companyAddress.length()>2000 || profession.length()>11 || income.length()>20 || familyId.length()>11){
			map.put("errNo", "-5");
	 		map.put("result", "参数长度超出范围");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}

		if(VerificationUtil.isNumeric(id) || VerificationUtil.isNumeric(stag) ||
				VerificationUtil.isNumeric(electricBikes) || VerificationUtil.isNumeric(carCount) ||
				VerificationUtil.isNumeric(homesCount) || VerificationUtil.isNumeric(sex) ||
				VerificationUtil.isNumeric(age) || VerificationUtil.isNumeric(isPermanentAddress) ||
				VerificationUtil.isNumeric(profession) || VerificationUtil.isNumeric(familyId)){
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		Integer appuserId = Integer.parseInt(id);// 转成Integer类型
		if(!familyAddress.isEmpty()){
			model.setFamilyAddress(familyAddress);
		}
		Integer sta = Integer.parseInt(stag);
		model.setStag(sta);
		Integer electricBike = Integer.parseInt(electricBikes);
		model.setElectricBikes(electricBike);
		Integer carCoun = Integer.parseInt(carCount);
		model.setCarCount(carCoun);
		Integer homesCoun = Integer.parseInt(homesCount);
		model.setHomesCount(homesCoun);
		model.setAppuserId(appuserId);
		
		int ret = service.updateByPrimaryKeySelective(model);
		
		AppFamilyInformation personInfo = new AppFamilyInformation();//对应的实体
		
		personInfo.setFamilyId(Integer.parseInt(familyId));
		Integer se = Integer.parseInt(sex);
		personInfo.setSex(se);
		Integer ag = Integer.parseInt(age);
		personInfo.setAge(ag);
		Integer isPermanentAddres = Integer.parseInt(isPermanentAddress);
		personInfo.setIsPermanentAddress(isPermanentAddres);
		Integer professio = Integer.parseInt(profession);
		personInfo.setProfession(professio);
		if(!income.isEmpty()){
			personInfo.setIncome(income);
		}
		if(!companyAddress.isEmpty()){
			personInfo.setCompanyAddress(companyAddress);
		}
		personInfo.setAppuserId(appuserId);
		
		int retu = appFamilyInformationService.updateByPrimaryKeySelective(personInfo);
		
		if (ret > 0 && retu >0) {
			SysAppuser appUser = service.selectByPrimaryKey(appuserId);
			// map.put("appUser", appUser);
			map.put("appuserId", appUser.getAppuserId());
			map.put("userName", appUser.getUserName());
			map.put("userPassword", appUser.getUserPassword());
			map.put("createDate", appUser.getCreateDate());
			map.put("electricBikes", appUser.getElectricBikes());
			map.put("familyAddress", appUser.getFamilyAddress());
			map.put("homesCount", appUser.getHomesCount());
			map.put("carCount", appUser.getCarCount());
			map.put("stag", appUser.getStag());
			
			map.put("cityCode", appUser.getCityCode());

			String filePathAndName = getProperties().getProperty("filePathDetail") +"Family/"+appUser.getCityCode()+"/"+ appUser.getUserName() + "family.txt";
			String fileContent = ZsJsonUtil.map2Json(map).toString();
			map.clear();
			TxtUtil.newFile(filePathAndName, fileContent);
			
			//保存个人信息文件
			map.put("appuserId", appuserId);
	 	    map.put("sex", sex);//登录用户id
			map.put("age", age);
			map.put("isPermanentAddress", isPermanentAddress);
			map.put("profession", profession);
			map.put("income", income);
			map.put("companyAddress", companyAddress);
	 		filePathAndName = getProperties().getProperty("filePathDetail")+"Base/"+cityCode+"/"+userName+"base.txt";
	 		fileContent = ZsJsonUtil.map2Json(map).toString();
	 		map.clear();
	 		TxtUtil.newFile(filePathAndName,fileContent);
			
			map.put("errNo", "0");
			map.put("result", "保存成功!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		} else {
			map.put("errNo", "-1");
			map.put("result", "修改失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
	}

	/**
	 * @description APP端个人及家庭信息查看
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author wangzf
	 * @date 2017-06-21
	 */
	@RequestMapping(value = "/queryBaseAndFamilyInfoById", method = RequestMethod.POST)
	public void queryBaseAndFamilyInfoById(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		//List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
		String id = request.getParameter("appuserId");// 必传字段，不能为空
		//String userName = request.getParameter("userName");// 必传字段，不能为空
		if(id == null){
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		id = id.trim();
		if(id.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return;
		}
		if(id.length()>11){
			map.put("errNo", "-5");
	 		map.put("result", "参数长度超出范围");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(VerificationUtil.isNumeric(id)){
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		Integer appuserId = Integer.parseInt(id);// 转成Integer类型
		SysAppuser appUser = service.selectByPrimaryKey(appuserId);
		
		List<AppFamilyInformation> list = appFamilyInformationService.selectByAppuserId(appuserId);
		
		if (appUser != null && list.size()>0) {
			//Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "0");
			map.put("appuserId", appUser.getAppuserId());
			map.put("userName", appUser.getUserName());
			map.put("userPassword", appUser.getUserPassword());
			map.put("createDate", appUser.getCreateDate());
			map.put("electricBikes", appUser.getElectricBikes());
			// map.put("age", appUser.getAge());
			// map.put("sex", appUser.getSex());
			// map.put("Occupation", appUser.getOccupation());
			map.put("familyAddress", appUser.getFamilyAddress());
			map.put("homesCount", appUser.getHomesCount());
			map.put("carCount", appUser.getCarCount());
			map.put("stag", appUser.getStag());
			map.put("cityCode",appUser.getCityCode());
			
			for(int i = 0; i < list.size(); i++){
				map.put("familyId", list.get(i).getFamilyId());
        		map.put("sex", list.get(i).getSex());
    			map.put("age", list.get(i).getAge());
    			map.put("isPermanentAddress", list.get(i).getIsPermanentAddress());
    			map.put("profession", list.get(i).getProfession());
    			map.put("income", list.get(i).getIncome());
    			//map.put("icCard", list.get(i).getIcCard());
    			//map.put("phone", list.get(i).getPhone());
    			map.put("companyAddress", list.get(i).getCompanyAddress());
    			//listResult.add(map);
			}
			
		}else{
			map.put("errNo", "-1");
	 		map.put("result", "查询失败，请稍候再试");
		}

		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
		//response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

}
