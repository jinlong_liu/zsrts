package com.zs.traffic.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.traffic.model.TrajectoryDate;
import com.zs.traffic.service.TrajectoryDateService;
import com.zs.utils.TxtUtil;
import com.zs.utils.VerificationUtil;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;

@Controller
@RequestMapping("/app")
public class TrajectoryDateController extends BaseController{
	
	@Autowired
	public TrajectoryDateService service;

	/**
	 * @description 出行轨迹提交
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-03-31
	 */
	@RequestMapping(value="insertTrajectoryDate",method=RequestMethod.POST)
	public void insertTrajectoryDate(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception{
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String userName = request.getParameter("userName");//必传字段，不能为空
		String cityCode = request.getParameter("cityCode");//必传字段，不能为空
		    
		String startDate = request.getParameter("startDate");//开始时间
		String endDate = request.getParameter("endDate");//结束时间
		String traGps = request.getParameter("traGps");
		
		if(id==null || userName==null || cityCode==null || startDate==null ||
				endDate==null || traGps==null){
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		id = id.trim();
		userName = userName.trim();
		cityCode = cityCode.trim();
		startDate = startDate.trim();
		endDate = endDate.trim();
		traGps = traGps.trim();
		if(id.isEmpty() || userName.isEmpty() || cityCode.isEmpty() || startDate.isEmpty() ||
				endDate.isEmpty() || traGps.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(id.length()>11 || userName.length()>20 || cityCode.length()>255 || traGps.length()>21836){
			map.put("errNo", "-8");
	 		map.put("result", "参数长度超出范围");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(VerificationUtil.isNumeric(id)){
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(VerificationUtil.isValidDateMinute(startDate) || VerificationUtil.isValidDateMinute(endDate)){
			map.put("errNo", "-5");
	 		map.put("result", "日期类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		} 
		 
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		TrajectoryDate model = new TrajectoryDate();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
		Date sDate = sdf.parse(startDate);
		Date eDate = sdf.parse(endDate);
		
		if(sDate.after(eDate)){
			map.put("errNo", "-6");
	 		map.put("result", "开始时间必须小于结束时间");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		String trajectoryGps = traGps.replace(" ", ";");
		 
		Date date = VerificationUtil.Tomorrow();//获取明天凌晨1点时间。
		if(date == null){
			map.put("errNo", "-1");
			map.put("result", "保存失败，请稍候再试!");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		//传入参数的时间不能超过第二天时间。(加一小时是防止手机时间故意调快几分钟，太多认为手机时间错误)
		if(date.before(sDate) || date.before(eDate)){
			map.put("errNo", "-7");
	 		map.put("result", "时间日期不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		//String time = startDate.replaceAll("-","_");//数据库表名命名规则只能有字母、数字、_,"-"不能识别
		sdf = new SimpleDateFormat("yyyy_MM_dd");
		String time = sdf.format(sDate);
		String tableName =  "trajectory_"+time;
		
		model.setAppuserId(appuserId);
        model.setStartDate(sDate);
        model.setEndDate(eDate);
        model.setTrajectoryGps(trajectoryGps);
         
        List<TrajectoryDate> list =  service.selectByAppuserAndStartDate(model,tableName);
        if(list.size()>0){
            map.put("errNo", "1");
 			map.put("result", "数据已经保存过了!");
 			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
 			return; 
        }
		 
		int ret = service.insertSelectives(model,tableName);
		if(ret>0){
			//map.put("轨迹", model);
			map.put("UserId", appuserId);
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			map.put("traGps", traGps);
			
			String filePathAndName = 
					getProperties().getProperty("filePathDetail")+"tarGps/"+cityCode+"/"+time+"/"+userName
					+"_"+startDate.replace(":","")+".txt";//路径,TXT文件命名不能包含":"
		    String fileContent = ZsJsonUtil.map2Json(map).toString();
			map.clear();
			TxtUtil.newFile(filePathAndName,fileContent);//将数据保存到服务器
			map.put("errNo", "0");
			map.put("result", "保存成功!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return; 
		 }else{
			map.put("errNo", "-1");
			map.put("result", "保存失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return; 
		 }

	}
	
	
	
}
