package com.zs.traffic.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FileUtils;
import com.zs.traffic.model.Area;
import com.zs.traffic.model.UserArea;
import com.zs.traffic.service.AreaService;
import com.zs.traffic.service.UserAreaService;
import com.zs.utils.ExcelReader;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;

/**
 * @description 区域管理
 * 
 * @author xxl
 * 
 * @date 2017-03-24
 * 
 */
@Controller
@RequestMapping("/")
public class AreaController extends BaseController {
	@Autowired
	public AreaService areaService;
	@Autowired
	public UserAreaService userService;

	/**
	 * 新增行政区
	 * 
	 */
	@RequestMapping(value = "/insertDistract", method = RequestMethod.POST)
	public void insertDistract(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		Map<String, Object> backMap = new HashMap<String, Object>();
		String districtNumber = request.getParameter("districtNumber").trim();// 行政区编号
		String districtName = request.getParameter("districtName").trim();// 行政区名称
		Integer type = 1;// 1行政区，2街道 3.小区
		List<Area> list = areaService.getListByNameOrNum(districtName, districtNumber, type, cityCode, null);
		if (list.size() > 0) {
			backMap.put("flag", "3");
			backMap.put("message", "该名称已存在！");
		} else {
			Area record = new Area();
			record.setDistrictNumber(districtNumber);// Integer.parseInt(districtNumber)
			record.setDistrictName(districtName);
			record.setType(type);
			record.setCityCode(cityCode);
			int insertSelective = areaService.insertSelective(record);
			if (insertSelective > 0) {
				backMap.put("flag", "1");
				backMap.put("message", "操作成功！");
			} else {
				backMap.put("flag", "0");
				backMap.put("message", "操作失败！");
			}
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 新增街道办
	 * 
	 */
	@RequestMapping(value = "/insertSubOffice", method = RequestMethod.POST)
	public void insertSubOffice(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		Map<String, Object> backMap = new HashMap<String, Object>();
		String districtNumber = request.getParameter("districtNumber").trim();// 行政区编号
		String districtName = request.getParameter("districtName").trim();// 行政区名称
		String subofficeNumber = request.getParameter("subofficeNumber").trim();// 街道办编号
		String subofficeName = request.getParameter("subofficeName").trim();// 街道办名称
		Integer type = 2;// 1行政区，2街道 3.小区
		List<Area> list = areaService.getListByNameOrNum(subofficeName, subofficeNumber, type, cityCode,
				districtNumber);
		if (list.size() > 0) {
			backMap.put("flag", "3");
			backMap.put("message", "该名称已存在！");
		} else {
			Area record = new Area();
			record.setDistrictNumber(districtNumber);// Integer.parseInt(districtNumber)
			record.setDistrictName(districtName);
			record.setParentSuboffice(districtNumber);// Integer.parseInt(districtNumber)
			record.setSubofficeNumber(subofficeNumber);// Integer.parseInt(subofficeNumber)
			record.setSubofficeName(subofficeName);
			record.setType(type);
			record.setCityCode(cityCode);
			int insertSelective = areaService.insertSelective(record);
			if (insertSelective > 0) {
				backMap.put("flag", "1");
				backMap.put("message", "操作成功！");
			} else {
				backMap.put("flag", "0");
				backMap.put("message", "操作失败！");
			}
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 新增社区
	 * 
	 */
	@RequestMapping(value = "/insertCommunity", method = RequestMethod.POST)
	public void insertCommunity(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		Map<String, Object> backMap = new HashMap<String, Object>();
		String districtNumber = request.getParameter("districtNumber").trim();// 行政区编号
		String districtName = request.getParameter("districtName").trim();// 行政区名称
		String subofficeNumber = request.getParameter("subofficeNumber").trim();// 街道办编号
		String subofficeName = request.getParameter("subofficeName").trim();// 街道办名称
		String communityNumber = request.getParameter("communityNumber").trim();// 街道办编号
		String communityName = request.getParameter("communityName").trim();// 街道办名称
		Integer type = 3;// 1行政区，2街道 3.小区
		List<Area> list = areaService.getListByNameOrNum(communityName, communityNumber, type, cityCode,
				districtNumber);

		if (list.size() > 0) {
			backMap.put("flag", "3");
			backMap.put("message", "该名称已存在！");
		} else {
			Area record = new Area();
			record.setDistrictNumber(districtNumber);// Integer.parseInt(districtNumber)
			record.setDistrictName(districtName);
			record.setParentSuboffice(districtNumber);// Integer.parseInt(districtNumber)
			record.setSubofficeNumber(subofficeNumber);// Integer.parseInt(subofficeNumber)
			record.setSubofficeName(subofficeName);
			record.setCommunityNumber(communityNumber);// Integer.parseInt(communityNumber)
			record.setCommunityName(communityName);
			record.setType(type);
			record.setCityCode(cityCode);
			int insertSelective = areaService.insertSelective(record);
			if (insertSelective > 0) {
				backMap.put("flag", "1");
				backMap.put("message", "操作成功！");
			} else {
				backMap.put("flag", "0");
				backMap.put("message", "操作失败！");
			}
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 修改行政区
	 * 
	 */
	@RequestMapping(value = "/amendDistract", method = RequestMethod.POST)
	public void amendDistract(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> backMap = new HashMap<String, Object>();
		String districtNumber = request.getParameter("districtNumber").trim();// 行政区编号
		String districtName = request.getParameter("districtName").trim();// 行政区名称
		String id = request.getParameter("id").trim();// 街道办id
		String nameOld = request.getParameter("nameOld").trim();// 街道办名称
		Integer type = 1;// 1行政区，2街道 3.小区
		List<Area> list = areaService.getListByNameOrNumById(districtName, districtNumber, Integer.parseInt(id), type);
		// 判断该行政区下是否包含街道
		List<Area> listInclude = areaService.getListByName(districtName, 2);

		if (list.size() > 0) {
			backMap.put("flag", "3");
			backMap.put("message", "该名称已存在！");
		} else {
			if (listInclude.size() > 0) {
				backMap.put("flag", "3");
				backMap.put("message", "该名称已存在！");
			} else {
				Area record = new Area();
				record.setDistrictNumber(districtNumber);// Integer.parseInt(districtNumber)
				record.setDistrictName(districtName);
				record.setType(type);
				int updateSelective = areaService.updateName(record, nameOld);
				if (updateSelective > 0) {
					backMap.put("flag", "1");
					backMap.put("message", "操作成功！");
				} else {
					backMap.put("flag", "0");
					backMap.put("message", "操作失败！");
				}
			}
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 修改街道办
	 * 
	 */
	@RequestMapping(value = "/amendSubOffice", method = RequestMethod.POST)
	public void amendSubOffice(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> backMap = new HashMap<String, Object>();
		String districtNumber = request.getParameter("districtNumber").trim();// 行政区编号
		String districtName = request.getParameter("districtName").trim();// 行政区名称
		String subofficeNumber = request.getParameter("subofficeNumber").trim();// 街道办编号
		String subofficeName = request.getParameter("subofficeName").trim();// 街道办名称
		String id = request.getParameter("id").trim();// 街道办名称
		String nameOld = request.getParameter("nameOld").trim();// 街道办名称

		Integer type = 2;// 1行政区，2街道 3.小区
		List<Area> list = areaService.getListByNameOrNumById(subofficeName, subofficeNumber, Integer.parseInt(id),
				type);

		// 判断该行政区下是否包含街道
		List<Area> listInclude = areaService.getListByName(subofficeName, 2);

		if (list.size() > 0) {
			backMap.put("flag", "3");
			backMap.put("message", "该名称已存在！");
		} else {
			if (listInclude.size() > 0) {
				backMap.put("flag", "3");
				backMap.put("message", "该名称已存在！");
			} else {
				Area record = new Area();
				record.setDistrictNumber(districtNumber);// Integer.parseInt(districtNumber)
				record.setDistrictName(districtName);
				record.setParentSuboffice(districtNumber);// Integer.parseInt(districtNumber)
				record.setSubofficeNumber(subofficeNumber);// Integer.parseInt(subofficeNumber)
				record.setSubofficeName(subofficeName);
				record.setType(type);
				int updateSelective = areaService.updateName(record, nameOld);

				if (updateSelective > 0) {
					backMap.put("flag", "1");
					backMap.put("message", "操作成功！");
				} else {
					backMap.put("flag", "0");
					backMap.put("message", "操作失败！");
				}
			}
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 修改社区
	 * 
	 */
	@RequestMapping(value = "/amendCommunity", method = RequestMethod.POST)
	public void amendCommunity(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> backMap = new HashMap<String, Object>();
		String districtNumber = request.getParameter("districtNumber").trim();// 行政区编号
		String districtName = request.getParameter("districtName").trim();// 行政区名称
		String subofficeNumber = request.getParameter("subofficeNumber").trim();// 街道办编号
		String subofficeName = request.getParameter("subofficeName").trim();// 街道办名称
		String nameOld = request.getParameter("nameOld").trim();// 街道办名称
		String communityNumber = request.getParameter("communityNumber").trim();// 街道办编号
		String communityName = request.getParameter("communityName").trim();// 街道办名称
		String id = request.getParameter("id").trim();// 街道办名称

		Integer type = 3;// 1行政区，2街道 3.小区
		List<Area> list = areaService.getListByNameOrNumById(communityName, communityNumber, Integer.parseInt(id),
				type);

		// 判断该行政区下是否包含街道
		List<Area> listInclude = areaService.getListByName(subofficeName, 3);

		if (list.size() > 0) {
			backMap.put("flag", "3");
			backMap.put("message", "该名称已存在！");
		} else {
			if (listInclude.size() > 0) {
				backMap.put("flag", "3");
				backMap.put("message", "该名称已存在！");
			} else {
				Area record = new Area();
				record.setDistrictNumber(districtNumber);// Integer.parseInt(districtNumber)
				record.setDistrictName(districtName);
				record.setParentSuboffice(districtNumber);// Integer.parseInt(districtNumber)
				record.setSubofficeNumber(subofficeNumber);// Integer.parseInt(subofficeNumber)
				record.setSubofficeName(subofficeName);
				record.setCommunityNumber(communityNumber);// Integer.parseInt(communityNumber)
				record.setCommunityName(communityName);
				record.setType(type);
				int updateSelective = areaService.updateName(record, nameOld);

				if (updateSelective > 0) {
					backMap.put("flag", "1");
					backMap.put("message", "操作成功！");
				} else {
					backMap.put("flag", "0");
					backMap.put("message", "操作失败！");
				}
			}
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 回显数据
	 * 
	 */
	@RequestMapping(value = "/getSelectData", method = RequestMethod.POST)
	public void getSelectData(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> backMap = new HashMap<String, Object>();
		String id = request.getParameter("id").trim();// 行政区编号
		Area entity = areaService.selectByPrimaryKey(Integer.parseInt(id));
		Map<String, Object> map = new HashMap<String, Object>();
		if (entity.getType() == 1) {
			map.put("id", entity.getAreaId());// 序号
			map.put("districtId", entity.getDistrictNumber());
			map.put("districtName", entity.getDistrictName());
			map.put("type", entity.getType());
		} else if (entity.getType() == 2) {
			map.put("id", entity.getAreaId());// 序号
			map.put("districtId", entity.getDistrictNumber());
			map.put("districtName", entity.getDistrictName());
			map.put("subofficeId", entity.getSubofficeNumber());
			map.put("subofficeName", entity.getSubofficeName());
			map.put("type", entity.getType());
		} else if (entity.getType() == 3) {
			map.put("id", entity.getAreaId());// 序号
			map.put("districtId", entity.getDistrictNumber());
			map.put("districtName", entity.getDistrictName());
			map.put("subofficeId", entity.getSubofficeNumber());
			map.put("subofficeName", entity.getSubofficeName());
			map.put("communityNumber", entity.getCommunityNumber());
			map.put("communityName", entity.getCommunityName());
			map.put("type", entity.getType());
		}
		response.getWriter().write(ZsJsonUtil.map2Json(map));
	}

	/**
	 * 删除区域
	 */
	@RequestMapping(value = "/deleteById", method = RequestMethod.POST)
	public void deleteById(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> backMap = new HashMap<String, Object>();

		String typeDis = request.getParameter("type").trim();// 街道办编号
		String id = request.getParameter("id").trim();// areaId
		Area area = areaService.selectByPrimaryKey(Integer.parseInt(id));
		List<UserArea> user = null;
		List<Area> list = null;

		String areId = id;
		int areaId = Integer.parseInt(areId);
		user = userService.getUser(areaId); // 判断行政区是否分配人员
		// 判断删除的是否是哪一级
		if (typeDis.equals("1")) {
			// 删除行政区 ,判断是否有分配的街道
			String districtNumber = area.getDistrictNumber();// 行政区编号
			list = areaService.getSelectListByName(districtNumber, 2);
		} else if (typeDis.equals("2")) {
			// 删除街道
			String subofficeNumber = area.getSubofficeNumber();// 街道办编号
			list = areaService.getSelectListByName(subofficeNumber, 3);
		}

		if (typeDis.equals("3")) {
			int updateSelective = areaService.deleteByPrimaryKey(Integer.parseInt(id));

			if (updateSelective > 0) {
				backMap.put("flag", "1");// 删除成功
			} else {
				backMap.put("flag", "0");// 删除失败
			}
		} else {
			if (list.size() == 0) {
				if (user.size() == 0) {
					int updateSelective = areaService.deleteByPrimaryKey(Integer.parseInt(id));

					if (updateSelective > 0) {
						backMap.put("flag", "1");// 删除成功
					} else {
						backMap.put("flag", "0");// 删除失败
					}
				} else {
					backMap.put("flag", "4");// 已分配人员管理。
				}
			} else {
				if (typeDis.equals("1")) {
					backMap.put("flag", "3");// 行政区下有街道
				} else if (typeDis.equals("2")) {
					backMap.put("flag", "5");// 街道下有社区
				}
			}
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 获取行政区，街道，社区下拉列表
	 */
	@RequestMapping(value = "/getListArea", method = RequestMethod.POST)
	public void getListArea(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> backMap = new HashMap<String, Object>();
		String type = request.getParameter("type").trim();// 街道办编号
		Map<String, Object> mapNode = null;

		List<Map<String, Object>> listArea = new ArrayList<Map<String, Object>>();

		List<Area> list = null;
		// 判断删除的是否是哪一级
		if (type.equals("1")) {
			// 删除行政区 ,判断是否有分配的街道
			list = areaService.getListByType(1);
		} else if (type.equals("2")) {
			// 删除街道
			list = areaService.getListByType(2);
		} else if (type.equals("3")) {
			// 删除街道
			list = areaService.getListByType(3);
		}

		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				mapNode = new HashMap<String, Object>();
				mapNode.put("id", list.get(i).getAreaId());
				if (type.equals("1")) {
					// 行政区
					mapNode.put("text", list.get(i).getDistrictName());
				} else if (type.equals("2")) {
					// 街道
					mapNode.put("text", list.get(i).getSubofficeName());
				} else if (type.equals("3")) {
					// 社区
					mapNode.put("text", list.get(i).getCommunityName());
				}
				listArea.add(mapNode);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listArea));
	}

	/**
	 * 获取行政区，街道，社区下拉列表
	 */
	@RequestMapping(value = "/getAreaDownList", method = RequestMethod.POST)
	public void getAreaDownList(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> backMap = new HashMap<String, Object>();
		String type = request.getParameter("type").trim();// 街道办编号
		Map<String, Object> mapNode = null;

		List<Map<String, Object>> listArea = new ArrayList<Map<String, Object>>();

		List<Area> list = null;
		// 判断删除的是否是哪一级
		if (type.equals("1")) {

			// 获取行政区 ,判断是否有分配的街道
			list = areaService.getListByType(1);
		} else if (type.equals("2")) {
			String districtName = request.getParameter("districtName").trim();// 街道办编号
			// 获取街道
			list = areaService.getSelectListByName(districtName, 2);
		} else if (type.equals("3")) {
			String subOfficeSelName = request.getParameter("subOfficeSelName").trim();
			// 获取社区
			list = areaService.getSelectListByName(subOfficeSelName, 3);
		}

		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				mapNode = new HashMap<String, Object>();

				if (type.equals("1")) {
					// 行政区
					mapNode.put("id", list.get(i).getDistrictNumber());
					mapNode.put("text", list.get(i).getDistrictName());
					mapNode.put("areaId", list.get(i).getAreaId());// 此字段添加用户时需要主键id
				} else if (type.equals("2")) {
					// 街道
					mapNode.put("id", list.get(i).getSubofficeNumber());
					mapNode.put("text", list.get(i).getSubofficeName());
					mapNode.put("areaId", list.get(i).getAreaId());// 此字段添加用户时需要主键id
				} else if (type.equals("3")) {
					// 社区
					mapNode.put("id", list.get(i).getCommunityNumber());
					mapNode.put("text", list.get(i).getCommunityName());
					mapNode.put("areaId", list.get(i).getAreaId());// 此字段添加用户时需要主键id
				}
				listArea.add(mapNode);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listArea));
	}

	/**
	 * 获取树级菜单
	 */
	@RequestMapping(value = "/getListAreaTree", method = RequestMethod.POST)
	public void getListAreaTree(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> backMap = new HashMap<String, Object>();
		String type = request.getParameter("type").trim();// 街道办编号
		Map<String, Object> mapNode = null;

		List<Map<String, Object>> listDis = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> listSub = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> listComm = new ArrayList<Map<String, Object>>();

		List<Area> list = areaService.getAllList();
		// 判断删除的是否是哪一级
		if (type.equals("1")) {
			// 删除行政区 ,判断是否有分配的街道

		} else if (type.equals("2")) {
			// 删除街道
			list = areaService.getListByType(2);
		} else if (type.equals("3")) {
			// 删除街道
			list = areaService.getListByType(3);
		}
		if (list.size() > 0) {
			backMap.put("flag", "3");
			backMap.put("message", "该名称下已分配，请删除其下所有列表再删除！");
		} else {
			// 获取不同的行政区,街道，社区数量
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getType() == 3) {
					mapNode = new HashMap<String, Object>();
					mapNode.put("id", list.get(i).getAreaId());
					mapNode.put("text", list.get(i).getCommunityName());
					mapNode.put("year", list.get(i).getDistrictName());
					mapNode.put("month", list.get(i).getSubofficeName());
					listComm.add(mapNode);
				}
			}

			// 获取不同街道数量
			String subName = null;
			for (int i = 0; i < list.size(); i++) {
				if (!list.get(i).getSubofficeName().equals(subName) && list.get(i).getType() == 2) {
					// countMonth++;
					subName = list.get(i).getSubofficeName();

					mapNode = new HashMap<String, Object>();
					mapNode.put("id", list.get(i).getAreaId());
					mapNode.put("text", subName);
					mapNode.put("year", list.get(i).getDistrictName());

					// 添加改街道下的社区
					List<Map<String, Object>> listDateSub = new ArrayList<Map<String, Object>>();
					for (int j = 0; j < listComm.size(); j++) {
						if (listComm.get(j).get("month").equals(subName)) {
							listDateSub.add(listComm.get(j));
						}
					}

					mapNode.put("children", listDateSub);
					listSub.add(mapNode);
				}
			}

			// 获取行政区下的街道
			String disName = null;
			for (int i = 0; i < list.size(); i++) {
				if (!list.get(i).getDistrictName().equals(disName) && list.get(i).getType() == 1) {
					// countYear++;
					disName = list.get(i).getDistrictName();

					mapNode = new HashMap<String, Object>();
					mapNode.put("id", list.get(i).getAreaId());
					mapNode.put("text", disName);

					// 添加改年份下的月
					List<Map<String, Object>> listMonthSub = new ArrayList<Map<String, Object>>();
					for (int j = 0; j < listSub.size(); j++) {
						if (listSub.get(j).get("year").equals(disName)) {
							listMonthSub.add(listSub.get(j));
						}
					}

					mapNode.put("children", listMonthSub);
					listDis.add(mapNode);
				}
			}

		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listDis));
	}

	/**
	 * 根据不同管理员权限查询其下的管理区域列表
	 */
	@RequestMapping(value = "/getListAreaGrid", method = RequestMethod.POST)
	public void getListAreaGrid(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;

		// 判断权限Area
		HttpSession session = servletRequest.getSession();
		/*
		 * String area = (String) session.getAttribute(""); String type =
		 * (String) session.getAttribute("type");//type:1行政区，2街道 3.小区
		 */
		String cityCode = (String) session.getAttribute("cityCode");
		String area = "";
		String type = "+1";
		// 获取管辖区域
		List<Area> list = areaService.getSelectListByNames(area, Integer.parseInt(type), cityCode);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		// 根据权限查询所管辖区域
		JSONArray array = new JSONArray();// 此为对象的集合 getDepartment

		int count = 0;
		if (list != null && list.size() > 0) {
			count = list.size();
			for (int i = 0; i < list.size(); i++) {
				Area entity = list.get(i);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("index", i + 1);// 序号
				map.put("id", entity.getAreaId());// id
				map.put("districtNumber", entity.getDistrictNumber());
				map.put("districtName", entity.getDistrictName());
				map.put("subofficeNumber", entity.getSubofficeNumber());
				map.put("subofficeName", entity.getSubofficeName());
				map.put("communityNumber", entity.getCommunityNumber());
				map.put("communityName", entity.getCommunityName());
				map.put("type", entity.getType());
				map.put("delete", entity.getAreaId());
				listResult.add(map);
			}
		}

		JSONObject object_ = new JSONObject();// 此为数据和行号
		/*
		 * object_.put("rows", array); object_.put("total", count);
		 */
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));

	}

	/**
	 * 区域导入
	 */
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
	public void fileUpload(@RequestParam(value = "file") MultipartFile file, XssHttpServletRequestWrapper request,
			HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");
		/*
		 * Iterator iterator = file.getFileNames(); MultipartFile multifile =
		 * file.getFile((String) iterator.next());
		 */
		String path = request.getSession().getServletContext().getRealPath("upload");
		String fileName = file.getOriginalFilename();
		System.out.println(fileName);
		SimpleDateFormat appendIx_id = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		/* fileName.replace(".xls", appendIx_id+".xls"); */
		File targetFile = new File(path, fileName);
		// 设置文件保存的路径 提供给后面的解析使用
		request.getSession().setAttribute("fileNameSpare", fileName);
		request.getSession().setAttribute("filePathSpare", path);

		// 把文件下载下来
		FileUtils.copyInputStreamToFile(file.getInputStream(), new File(path, fileName));
		// FileUtils.copyInputStreamToFile(file.getInputStream(), new File(
		// path, fileName));
		/*
		 * SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		 * 
		 * MultipartHttpServletRequest multipartRequest =
		 * (MultipartHttpServletRequest) request; // 获取传入文件
		 * multipartRequest.setCharacterEncoding("utf-8");
		 * 
		 * List<MultipartFile> files = multipartRequest.getFiles("myFile");
		 * 
		 * SimpleDateFormat appendIx_id = new
		 * SimpleDateFormat("yyyyMMddHHmmssSSS");
		 * 
		 * 
		 * if(!files.isEmpty()&&files.size()>0){ for (MultipartFile file :
		 * files) { if(file.getSize() < 1) { continue; } String realPath =
		 * request.getSession().getServletContext() .getRealPath("/upload");
		 * 
		 * 
		 * String fileName = file.getOriginalFilename();
		 * 
		 * 
		 * FileUtils.copyInputStreamToFile(file.getInputStream(), new File(
		 * realPath, fileName));
		 * 
		 * }
		 * 
		 * }
		 */

		String realDir = servletRequest.getSession().getServletContext().getRealPath("");
		String contextpath = servletRequest.getContextPath();
		String basePath = servletRequest.getScheme() + "://" + servletRequest.getServerName() + ":"
				+ servletRequest.getServerPort() + contextpath + "/";

		Map<String, Object> backMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertItems = new ArrayList<Map<String, Object>>();// 更新
		Map<String, Object> addMap = new HashMap<String, Object>();
		String type = request.getParameter("type").trim();// 街道办编号
		try {
			// 对读取Excel表格标题测试
			InputStream is = new FileInputStream(targetFile);
			ExcelReader excelReader = new ExcelReader();
			String[] title = excelReader.readExcelTitle(is);
			System.out.println("获得Excel表格的标题:" + title);
			for (String s : title) {
				System.out.print(s + " ");
			}
			String depict = "";
			// 对读取Excel表格内容测试
			InputStream is2 = new FileInputStream(targetFile);
			Map<Integer, String> map = excelReader.readExcelContent(is2);
			System.out.println("获得Excel表格的内容:");
			String districtNames = "";
			String districtNumbers = "";
			String subofficeNumbers = "";
			String subofficeNames = "";
			String communityNumbers = "";
			String communityNames = "";
			String cityCodes = "";
			String districtNamesBack = "行政区 :";
			String subofficeNamesBack = "街道 :";
			String communityNamesBack = "社区 :";
			if (type.equals("1")) {
				for (int i = 1; i <= map.size(); i++) {
					String k = map.get(i);
					if (!"".equals(k) && null != k) {
						String[] data = k.split(",");

						addMap = new HashMap<String, Object>();
						addMap.put("districtNumber", data[0]);
						addMap.put("districtName", data[1]);
						addMap.put("type", data[6]);
						addMap.put("cityCode", data[9]);
						if (!"1".equals(data[6])) {
							depict = "文件中区域类型与页面中您选择的类型不符，请核对xls文件中的区域类型。“1”代表行政区，“2”代表街道，“3”代表社区。";
						} else if (!cityCode.equals(data[9])) {
							depict = "抱歉！城市不相同，无法导入行，请联系管理员或修改城市编码";
						}
						if (i != map.size()) {
							districtNames += data[1] + ",";
							districtNumbers += data[0].replace(".0", "") + ",";
							cityCodes += data[9].replace(".0", "") + ",";
						} else {
							districtNames += data[1];
							districtNumbers += data[0].replace(".0", "");
							cityCodes += data[9].replace(".0", "");
						}
					} else {
						depict = "xls文件中存在内容为空的数据，请检查文件内容，特别注意内容顶部或底部。";
					}
					insertItems.add(addMap);
				}
			} else if (type.equals("2")) {
				for (int i = 1; i <= map.size(); i++) {
					String k = map.get(i);
					if (!"".equals(k) && null != k) {
						String[] data = k.split(",");
						addMap = new HashMap<String, Object>();
						addMap.put("districtNumber", data[0]);
						addMap.put("districtName", data[1]);
						addMap.put("subofficeNumber", data[2]);
						addMap.put("subofficeName", data[3]);
						addMap.put("type", data[6]);
						addMap.put("cityCode", data[9]);
						if (!"2".equals(data[6])) {
							depict = "文件中区域类型与页面中您选择的类型不符，请核对xls文件中的区域类型。“1”代表行政区，“2”代表街道，“3”代表社区。";
						} else if (!cityCode.equals(data[9])) {
							depict = "抱歉！城市不相同，无法导入，请联系管理员或修改城市编码";
						}
						if (i != map.size()) {
							districtNames += data[1] + ",";
							districtNumbers += data[0].replace(".0", "") + ",";
							subofficeNames += data[3] + ",";
							subofficeNumbers += data[2].replace(".0", "") + ",";
							cityCodes += data[9].replace(".0", "") + ",";
						} else {
							districtNames += data[1];
							districtNumbers += data[0].replace(".0", "");
							subofficeNames += data[3];
							subofficeNumbers += data[2].replace(".0", "");
							cityCodes += data[9].replace(".0", "");
						}
					} else {
						depict = "xls文件中存在内容为空的数据，请检查文件内容，特别注意内容顶部或底部。";
					}
					insertItems.add(addMap);
				}
			} else if (type.equals("3")) {
				for (int i = 1; i <= map.size(); i++) {
					String k = map.get(i);
					if (!"".equals(k) && null != k) {
						String[] data = k.split(",");
						addMap = new HashMap<String, Object>();
						addMap.put("districtNumber", data[0]);
						addMap.put("districtName", data[1]);
						addMap.put("subofficeNumber", data[2]);
						addMap.put("subofficeName", data[3]);
						addMap.put("communityNumber", data[4]);
						addMap.put("communityName", data[5]);
						addMap.put("type", data[6]);
						addMap.put("totalCount", data[7]);
						addMap.put("everydayCount", data[8]);
						addMap.put("cityCode", data[9]);
						if (!"3".equals(data[6])) {
							depict = "文件中区域类型与页面中您选择的类型不符，请核对xls文件中的区域类型。“1”代表行政区，“2”代表街道，“3”代表社区。";
						} else if (!cityCode.equals(data[9])) {
							depict = "抱歉！城市不相同，无法导入，请联系管理员或修改城市编码";
						}
						if (i != map.size()) {
							districtNames += data[1] + ",";
							districtNumbers += data[0].replace(".0", "") + ",";
							subofficeNames += data[3] + ",";
							subofficeNumbers += data[2].replace(".0", "") + ",";
							communityNames += data[5] + ",";
							communityNumbers += data[4].replace(".0", "") + ",";
							cityCodes += data[9].replace(".0", "") + ",";
						} else {
							districtNames += data[1];
							districtNumbers += data[0].replace(".0", "");
							subofficeNames += data[3];
							subofficeNumbers = data[2].replace(".0", "");
							communityNames += data[5];
							communityNumbers += data[4].replace(".0", "");
							cityCodes += data[9].replace(".0", "");
						}
					} else {
						depict = "xls文件中存在内容为空的数据，请检查文件内容，特别注意内容顶部或底部。";
					}
					insertItems.add(addMap);
				}
			}
			// 判断用户选择的区域类型和文件中的区域类型是否一致
			if (depict == "" || "".equals(depict)) {
				// 根据不同传值查询 数据库是否包含所传信息 id+ name
				// 行政区是否存在
				List<Area> listDis = null;
				// 街道
				List<Area> listSub = null;
				// 社区
				List<Area> listCom = null;
				if (type.equals("1")) {
					// 行政区是否存在
					listDis = areaService.getListByNameOrNumInclude(districtNames, districtNumbers,
							Integer.parseInt(type), cityCode);
					if (listDis.size() > 0) {
						for (int i = 0; i < listDis.size(); i++) {
							if (i != listDis.size() - 1) {
								districtNamesBack += listDis.get(i).getDistrictName() + ",";
							} else {
								districtNamesBack += listDis.get(i).getDistrictName();
							}
						}
						backMap.put("flag", "0");
						backMap.put("message", "操作失败！编码与名称均不可重复，请删除以下已存在的数据" + districtNamesBack);
					} else {
						// 可以新增
						int ins = AreaJdbcUtil.insertTable(insertItems, Integer.parseInt(type));
						// 不可以新增 取出字段
						if (ins != -1) {
							backMap.put("flag", "1");
							backMap.put("message", "操作成功！");
						} else {
							backMap.put("flag", "0");
							backMap.put("message", "操作失败！");
						}
					}
				} else if (type.equals("2")) {
					// 行政区是否存在
					listDis = areaService.getListByNameOrNumInclude(districtNames, districtNumbers, 1, cityCode);
					// 街道
					/*
					 * listSub =
					 * areaService.getListByNameOrNumInclude(subofficeNames,
					 * subofficeNumbers, 2);
					 */
					if (-1 > 0) {
						if (-1 > 0) {
							for (int i = 0; i < listSub.size(); i++) {
								if (i != listSub.size() - 1) {
									subofficeNamesBack += listSub.get(i).getSubofficeName() + ",";
								} else {
									subofficeNamesBack += listSub.get(i).getSubofficeName();
								}
							}
						}
						backMap.put("flag", "0");
						backMap.put("message", "操作失败！编码与名称均不可重复，请删除以下已存在的数据" + subofficeNamesBack);
					} else {
						// 可以新增
						int ins = AreaJdbcUtil.insertTable(insertItems, Integer.parseInt(type));
						// 不可以新增 取出字段
						if (ins != -1) {
							backMap.put("flag", "1");
							backMap.put("message", "操作成功！");
						} else {
							backMap.put("flag", "0");
							backMap.put("message", "操作失败！");
						}
					}

				} else if (type.equals("3")) {
					// 行政区是否存在
					listDis = areaService.getListByNameOrNumInclude(districtNames, districtNumbers, 1, cityCode);
					// 街道
					listSub = areaService.getListByNameOrNumInclude(subofficeNames, subofficeNumbers, 2, cityCode);
					// 社区
					listCom = areaService.getListByNameOrNumInclude(communityNames, communityNumbers, 3, cityCode);

					if (listCom.size() > 0) {
						if (listCom.size() > 0) {
							for (int i = 0; i < listCom.size(); i++) {
								if (i != listCom.size() - 1) {
									communityNamesBack += listCom.get(i).getCommunityName() + ",";
								} else {
									communityNamesBack += listCom.get(i).getCommunityName();
								}
							}
						}
						backMap.put("flag", "0");
						backMap.put("message", "操作失败！编码与名称均不可重复，请删除以下已存在的数据" + communityNamesBack);

					} else {
						// 可以新增
						int ins = AreaJdbcUtil.insertTable(insertItems, Integer.parseInt(type));
						// 不可以新增 取出字段
						if (ins != -1) {
							backMap.put("flag", "1");
							backMap.put("message", "操作成功！");
						} else {
							backMap.put("flag", "0");
							backMap.put("message", "操作失败！");
						}
					}
				}
			} else {
				backMap.put("flag", "0");
				backMap.put("message", depict);
			}

			backMap.put("status", 1);
			System.out.println(insertItems + "--");
		} catch (FileNotFoundException e) {
			System.out.println("未找到指定路径的文件!");
			e.printStackTrace();
		}
		String map2Json = ZsJsonUtil.map2Json(backMap);
		response.getWriter().write(ZsJsonUtil.map2Json(backMap));
	}

	/**
	 * 获取社区下拉框
	 */
	@RequestMapping(value = "/ByRegion", method = RequestMethod.POST)
	public void queryByRegion(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		List<Area> list = areaService.queryByRegion();
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				Area entity = list.get(i);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("districtName", entity.getDistrictName());
				listResult.add(map);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	/**
	 * 条件查询（区域管理）行政区 街道
	 */
	@RequestMapping(value = "/queryGetArea", method = RequestMethod.POST)
	public void queryGetArea(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		String districtCode = request.getParameter("districtCode").trim();
		String subofficeCode = request.getParameter("subofficeCode").trim();
		String type = request.getParameter("type");
		// System.out.println(districtName+"----"+subofficeName);
		List<Area> list = areaService.queryAreaIst(districtCode, subofficeCode);
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				Area entity = list.get(i);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("index", i + 1);// 序号
				map.put("id", entity.getAreaId());// id
				map.put("districtNumber", entity.getDistrictNumber());
				map.put("districtName", entity.getDistrictName());
				map.put("subofficeNumber", entity.getSubofficeNumber());
				map.put("subofficeName", entity.getSubofficeName());
				map.put("communityNumber", entity.getCommunityNumber());
				map.put("communityName", entity.getCommunityName());
				map.put("type", entity.getType());
				map.put("delete", entity.getAreaId());
				listResult.add(map);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 查询街道下社区
	@RequestMapping(value = "/queryGetCommunityName", method = RequestMethod.POST)
	public void queryGetCommunityName(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		String subofficeNumber = request.getParameter("subofficeNumber");
		List<Area> list = areaService.selectCommunityName(subofficeNumber);
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				Area entity = list.get(i);
				String communityName = entity.getCommunityName();
				String communityNumber = entity.getCommunityNumber();
				map.put("communityName", communityName);
				map.put("communityNumber", communityNumber);
				listResult.add(map);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 修改回显数据
	@RequestMapping(value = "/addEcho", method = RequestMethod.POST)
	public void addEcho(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		String areaId = request.getParameter("areaId").trim();
		Area entity = areaService.addEcho(Integer.parseInt(areaId));
		if (entity != null) {
			map.put("districtName", entity.getDistrictName());// 行政区名字
			map.put("subofficeName", entity.getSubofficeName());// 街道名字
			map.put("communityName", entity.getCommunityName());// 社区名字
			map.put("districtNumber", entity.getDistrictNumber());// 行政区编码
			map.put("subofficeNumber", entity.getSubofficeNumber());// 街道编码
			map.put("communityNumber", entity.getCommunityNumber());// 社区编码
			map.put("type", entity.getType());// 类型
			listResult.add(map);
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 修改行政区。----
	@RequestMapping(value = "/updateDistrict", method = RequestMethod.POST)
	public void updateDistrict(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		String districtNumberUpdate = request.getParameter("districtNumberUpdate").trim();
		String districtNameUpdate = request.getParameter("districtNameUpdate").trim();
		String districtName = request.getParameter("districtName").trim();
		String districtNumber = request.getParameter("districtNumber").trim();
		if (districtNumberUpdate.equals("")) {
			districtNumberUpdate = null;
		}
		if (districtNameUpdate.equals("")) {
			districtNameUpdate = null;
		}
		List<Area> selectDistrictName = areaService.selectDistrictName(districtNameUpdate, cityCode);// 查询行政区名字是否存在
		List<Area> selectDistrictNumber = areaService.selectDistrictNumber(districtNumberUpdate, cityCode);// 查询行政区编码是否存在
		List<Area> list = areaService.selectSubofficeNumber(districtNumber);// 查询社区下面是否有街道
		if (districtNumber.equals(districtNumberUpdate) && !districtName.equals(districtNameUpdate)) {
			if (selectDistrictName.size() <= 0) { // 判断行政区名字是否重复
				int updateDistrict = areaService.updateDistrict(districtNumberUpdate, districtNameUpdate,
						districtNumber, cityCode);
				if (updateDistrict > 0) {
					map.put("back", 1);// 成功
				} else {
					map.put("back", 2); // 失败
				}
			} else {
				map.put("back", 3);// 名称重复
			}
		} else if (districtName.equals(districtNameUpdate) && !districtNumber.equals(districtNumberUpdate)) {
			if (selectDistrictNumber.size() <= 0) { // 判断编码是否重复
				String subofficeNumber = null;
				if (list.size() > 1) {
					subofficeNumber = list.get(1).getSubofficeNumber();
				} else {
					subofficeNumber = list.get(0).getSubofficeNumber();
				}
				if (subofficeNumber == null) {
					int updateDistrict = areaService.updateDistrict(districtNumberUpdate, districtNameUpdate,
							districtNumber, cityCode);
					if (updateDistrict > 0) {
						map.put("back", 1);// 成功
					} else {
						map.put("back", 2); // 失败
					}
				} else {
					map.put("back", 4);// 行政区下有街道
				}
			} else {
				map.put("back", 6);// 编码重复
			}
		} else if (districtName.equals(districtNameUpdate) && districtNumber.equals(districtNumberUpdate)) {
			map.put("back", 8);// 参数一样
		} else {
			Area area = null;
			if (list.size() > 1) {
				area = list.get(1);
			} else {
				area = list.get(0);
			}
			String subofficeNumber = area.getSubofficeNumber();
			if (subofficeNumber == null) {
				if (selectDistrictName.size() <= 0 && selectDistrictNumber.size() <= 0) { // 判断行政区名字是否重复
																							// 跟编号是否重复
					int updateDistrict = areaService.updateDistrict(districtNumberUpdate, districtNameUpdate,
							districtNumber, cityCode);
					if (updateDistrict > 0) {
						map.put("back", 1);// 成功
					} else {
						map.put("back", 2); // 失败
					}
				} else if (selectDistrictName.size() > 0) {
					map.put("back", 5);// 名称重复
				} else if (selectDistrictNumber.size() > 0) {
					map.put("back", 6);// 编码重复
				}
			} else {
				map.put("back", 4);// 行政区下有街道
			}
		}
		listResult.add(map);
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 修改街道----
	@RequestMapping(value = "/updateSuboffice", method = RequestMethod.POST)
	public void updateSuboffice(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		String subofficeNameUpdate = request.getParameter("subofficeNameUpdate");
		String subofficeNumberUpdate = request.getParameter("subofficeNumberUpdate");
		String subofficeName = request.getParameter("subofficeName");
		String subofficeNumber = request.getParameter("subofficeNumber");
		List<Area> selectSubofficeNames = areaService.selectSubofficeNames(subofficeNameUpdate, cityCode);// 判断名字是否重复
		List<Area> selectSubofficeNumbers = areaService.selectSubofficeNumbers(subofficeNumberUpdate, cityCode);// 判断编号是否重复
		List<Area> selectCommunityNumber = areaService.selectCommunityNumber(subofficeNumber, cityCode);// 判断该街道下是否有社区
		if (!subofficeNameUpdate.equals(subofficeName) && subofficeNumberUpdate.equals(subofficeNumber)) {
			if (selectSubofficeNames.size() <= 0) {
				int updateSuboffice = areaService.updateSuboffice(subofficeNumberUpdate, subofficeNameUpdate,
						subofficeNumber, cityCode);
				if (updateSuboffice > 0) {
					map.put("back", 1);// 成功
				} else {
					map.put("back", 2);// 失败
				}
			} else {
				map.put("back", 3);// 名称重复
			}
		} else if (subofficeNameUpdate.equals(subofficeName) && !subofficeNumberUpdate.equals(subofficeNumber)) {
			if (selectSubofficeNumbers.size() <= 0) {
				String CommunityNumber = null;
				String distar = null;
				if (selectCommunityNumber.size() > 1) {
					CommunityNumber = selectCommunityNumber.get(1).getCommunityNumber();
					distar = selectCommunityNumber.get(1).getDistrictNumber();
				} else {
					CommunityNumber = selectCommunityNumber.get(0).getCommunityNumber();
					distar = selectCommunityNumber.get(0).getDistrictNumber();
				}
				if (CommunityNumber == null && distar == null) {
					int updateSuboffice = areaService.updateSuboffice(subofficeNumberUpdate, subofficeNameUpdate,
							subofficeNumber, cityCode);
					if (updateSuboffice > 0) {
						map.put("back", 1);// 成功
					} else {
						map.put("back", 2);// 失败
					}
				} else if (CommunityNumber != null) {
					map.put("back", 4);// 街道下有社区
				} else if (distar != null) {
					map.put("back", 7);// 街道上有行政区
				}
			} else {
				map.put("back", 6);// 编码重复
			}
		} else if (subofficeNameUpdate.equals(subofficeName) && !subofficeNumberUpdate.equals(subofficeNumber)) {
			map.put("back", 8);// 参数一样
		} else {
			String CommunityNumber = null;
			String distar = null;
			if (selectCommunityNumber.size() > 1) {
				CommunityNumber = selectCommunityNumber.get(1).getCommunityNumber();
				distar = selectCommunityNumber.get(0).getDistrictNumber();
			} else {
				CommunityNumber = selectCommunityNumber.get(0).getCommunityNumber();
				distar = selectCommunityNumber.get(0).getDistrictNumber();
			}
			if (CommunityNumber == null && distar == null) {
				if (selectSubofficeNames.size() <= 0 && selectSubofficeNumbers.size() <= 0) {
					int updateSuboffice = areaService.updateSuboffice(subofficeNumberUpdate, subofficeNameUpdate,
							subofficeNumber, cityCode);
					if (updateSuboffice > 0) {
						map.put("back", 1);// 成功
					} else {
						map.put("back", 2);// 失败
					}
				} else if (selectSubofficeNames.size() > 0) {
					map.put("back", 5);// 名称重复
				} else if (selectSubofficeNumbers.size() > 0) {
					map.put("back", 6);// 编码重复
				}
			} else if (CommunityNumber != null) {
				map.put("back", 4);// 街道下有社区
			} else if (distar != null) {
				map.put("back", 7);// 街道上有行政区
			}
		}
		listResult.add(map);
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 修改社区----
	@RequestMapping(value = "/updateCommunity", method = RequestMethod.POST)
	public void updateCommunity(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		String communityNameUpdate = request.getParameter("communityNameUpdate");
		String communityNumberUpdate = request.getParameter("communityNumberUpdate");
		String communityName = request.getParameter("communityName");
		String communityNumber = request.getParameter("communityNumber");
		List<Area> selectCommunityNameUpdates = areaService.selectCommunityNameUpdates(communityNameUpdate, cityCode);// 查询社区名字
		List<Area> selectCommunityNumberUpdates = areaService.selectCommunityNumberUpdates(communityNumberUpdate,
				cityCode);// 查询社区编码
		List<Area> selectSub = areaService.selectSub(communityNumber);// 查询上是否有街道
		if (!communityNameUpdate.equals(communityName) && communityNumberUpdate.equals(communityNumber)) {
			if (selectCommunityNameUpdates.size() <= 0) {
				int updateCommunity = areaService.updateCommunity(communityNumberUpdate, communityNameUpdate,
						communityNumber, cityCode);
				if (updateCommunity > 0) {
					map.put("back", 1);// 成功
				} else {
					map.put("back", 2);// 失败
				}
			} else {
				map.put("back", 3);// 名称重复
			}

		} else if (communityNameUpdate.equals(communityName) && !communityNumberUpdate.equals(communityNumber)) {
			String sub = null;
			String dis = null;
			if (selectSub.size() > 1) {
				sub = selectSub.get(1).getSubofficeNumber();
				dis = selectSub.get(1).getDistrictNumber();
			} else {
				sub = selectSub.get(0).getSubofficeNumber();
				dis = selectSub.get(0).getDistrictNumber();
			}
			if (sub == null && dis == null) {
				if (selectCommunityNumberUpdates.size() <= 0) {
					int updateCommunity = areaService.updateCommunity(communityNumberUpdate, communityNameUpdate,
							communityNumber, cityCode);
					if (updateCommunity > 0) {
						map.put("back", 1);// 成功
					} else {
						map.put("back", 2);// 失败
					}
				} else {
					map.put("back", 6);// 编码重复
				}
			} else if (sub != null) {
				map.put("back", 4);// 社区上有街道
			} else if (dis != null) {
				map.put("back", 7);// 社区上有行政区
			}
		} else if (communityNameUpdate.equals(communityName) && communityNumberUpdate.equals(communityNumber)) {
			map.put("back", 8);
		} else {
			String sub = null;
			String dis = null;
			if (selectSub.size() > 1) {
				sub = selectSub.get(1).getSubofficeNumber();
				dis = selectSub.get(1).getDistrictNumber();
			} else {
				sub = selectSub.get(0).getSubofficeNumber();
				dis = selectSub.get(0).getDistrictNumber();
			}
			if (sub == null && dis == null) {
				if (selectCommunityNumberUpdates.size() <= 0 && selectCommunityNameUpdates.size() <= 0) {
					int updateCommunity = areaService.updateCommunity(communityNumberUpdate, communityNameUpdate,
							communityNumber, cityCode);
					if (updateCommunity > 0) {
						map.put("back", 1);// 成功
					} else {
						map.put("back", 2);// 失
					}
				} else if (selectCommunityNumberUpdates.size() > 0) {
					map.put("back", 6);// 编码重复
				} else if (selectCommunityNameUpdates.size() > 0) {
					map.put("back", 5);// 名称重复
				}
			} else if (sub != null) {
				map.put("back", 4);// 社区上有街道
			} else if (dis != null) {
				map.put("back", 7);// 社区上有行政区
			}
		}
		listResult.add(map);
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
}
