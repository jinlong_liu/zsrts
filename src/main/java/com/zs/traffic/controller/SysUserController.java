package com.zs.traffic.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.constant.ZsAuthDegree;
import com.zs.traffic.model.Area;
import com.zs.traffic.model.SysRole;
import com.zs.traffic.model.SysRoleAuth;
import com.zs.traffic.model.SysUserRole;
import com.zs.traffic.model.User;
import com.zs.traffic.model.UserArea;
import com.zs.traffic.service.AreaService;
import com.zs.traffic.service.RoleService;
import com.zs.traffic.service.SysRoleAuthService;
import com.zs.traffic.service.SysUserRoleService;
import com.zs.traffic.service.UserService;
import com.zs.utils.MD5Util;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;
@Controller
@RequestMapping("/")
public class SysUserController extends BaseController {
	
	@Autowired
	public UserService userService;
	@Autowired
	public RoleService roleService;
	@Autowired
	public SysRoleAuthService sysRoleAuthService;
	@Autowired
	public AreaService areaService;
	
	private final static Logger logger = LoggerFactory.getLogger(SysUserController.class);
	/**
	 * 查询用户信息
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/getUserList", method=RequestMethod.POST)
    public void getUserList(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
    	request.setCharacterEncoding("UTF-8");
    	
    	HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		
		Integer isSuperAdmin =  (Integer) session.getAttribute("isSuperAdmin");//获取用户是否为超级管理员
		if(isSuperAdmin == null){
			isSuperAdmin = 0 ;
		}
		String cityCode =  (String) session.getAttribute("cityCode");//获取用户城市编码
		
		Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
		String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限
		String userName = (String) session.getAttribute("userName");//获取用户名
		String  authDegree = "";
        String[] array = authDegrees.split(",");
		Boolean flag = true;
		for(String auth:array){
			if(auth.equals(ZsAuthDegree.PERSON_AUTH)){
				 flag = false;
			}
			if(auth.equals(ZsAuthDegree.CITY_AUTH)||auth.equals(ZsAuthDegree.FIRST_AUTH)||
					auth.equals(ZsAuthDegree.SECOND_AUTH)||auth.equals(ZsAuthDegree.THIRD_AUTH)||
					auth.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){
				authDegree = auth;
			}
		}
		System.out.println("=============================");
		System.out.println(authDegree);
		System.out.println("=============================");
		if(flag){//如果没有人员管理权限,直接返回
			return;
		}
    	//String  authDegree ="2";//  当前用户的权限  id
    	//Integer  userId = 43;
    	
    	List<Area> listArea = new ArrayList<Area>();
    	List<String> areaNumber = new ArrayList<String>();
    	if(authDegree.equals(ZsAuthDegree.FIRST_AUTH)){//一级负责人,4
    		listArea=areaService.getAreaByuserId(userId,1);//根据用户id和type获取当前登录用户的管辖区域
    		for(int i=0;i<listArea.size();i++){
    			areaNumber.add(listArea.get(i).getDistrictNumber());
    		}
    		
    	}else if(authDegree.equals(ZsAuthDegree.SECOND_AUTH)){//二级负责人,5
    		listArea=areaService.getAreaByuserId(userId,2);//根据用户id和type获取当前登录用户的管辖区域
    		for(int i=0;i<listArea.size();i++){
    			areaNumber.add(listArea.get(i).getSubofficeNumber());
    		}
    	}else if(authDegree.equals(ZsAuthDegree.THIRD_AUTH)){//三级负责人,6
    		listArea=areaService.getAreaByuserId(userId,3);//根据用户id和type获取当前登录用户的管辖区域
    		for(int i=0;i<listArea.size();i++){
    			areaNumber.add(listArea.get(i).getCommunityNumber());
    		}
    	}
    	
    	List<User> list = new ArrayList<User>();
    	
    	//List<User> list=userService.getUserList();//查询所有
    	if(authDegree.equals(ZsAuthDegree.CITY_AUTH)){//市级负责人,3
    		if(isSuperAdmin == 1){//市级负责人admin为超级管理员，查询所有
    			list=userService.getUserList(cityCode);//查询所有
				System.out.println("=========================");
				System.out.println("超级管理员");
    		}else{//市级负责人,查询区域下所有
				list=userService.getUserList(cityCode);//查询所有
				System.out.println("==========================");
				System.out.println("不是超级管理员");
    		}
    	}else if(authDegree.equals(ZsAuthDegree.FIRST_AUTH)){//一级负责人，查询所管辖的行政区下的人员,4
    		//根据用户type，管理区域编号和当前登录用户的权限等级查询出当前登录用户下的所有区域的人员信息
    		list=userService.getUserListByUserId(areaNumber,1,ZsAuthDegree.INTFIRST_AUTH,cityCode);
    	}else if(authDegree.equals(ZsAuthDegree.SECOND_AUTH)){//二级负责人，查询所管辖的所有街道人员,5
    		//根据用户type，管理区域编号和当前登录用户的权限等级查询出当前登录用户下的所有区域的人员信息
    		list=userService.getUserListByUserId(areaNumber,2,ZsAuthDegree.INTSECOND_AUTH,cityCode);
    	}else if(authDegree.equals(ZsAuthDegree.THIRD_AUTH)){//三级负责人，查询所管辖的所有社区人员,6
    		//根据用户type，管理区域编号和当前登录用户的权限等级查询出当前登录用户下的所有区域的人员信息
    		list=userService.getUserListByUserId(areaNumber,3,ZsAuthDegree.INTTHIRD_AUTH,cityCode);
    	}
    	//List<User> list=userService.getUserListByUserId(userId);
 //   	JSONArray array = new JSONArray();//此为对象的集合 getDepartment
    	List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
    	
    	StringBuffer aerStringOne=new StringBuffer(); //  所有 一 二 三  调查员
    	//StringBuffer aerStringTwo=new StringBuffer();// 二级  三级 
    	//StringBuffer aerStringThree=new StringBuffer();//  调查员
    	if (list!=null && list.size()>0) {
    		int orderNumber = 1;//排序
    		for (int i = 0; i < list.size(); i++) {
				System.out.println("=============================");
				System.out.println("i = " + i);
    			Map<String,Object>  map=new HashMap<String,Object>();
    			int authDegre=list.get(i).getAuthDegree();
    			if(authDegre != ZsAuthDegree.INTCITY_AUTH){
					System.out.println("===========================");
					System.out.println("不为市级");
    				if(list.get(i).getTypeInt()==null){
						System.out.println("跳出");
    					continue;
    				}
    			}
    			String uName=list.get(i).getUserName();
 //   			aerString.append(+"、");
    			//String uName=list.get(i).getUserName();
    			//Map<String,Object>  map=new HashMap<String,Object>();
    			if(i!=(list.size()-1)){
    				if(list.get(i).getUserName().equals(list.get(i+1).getUserName())){//查询的时候，根据用户名排序
    					if(list.get(i).getTypeInt()==null || list.get(i).getTypeInt()==1){
    						if(authDegre == ZsAuthDegree.INTCITY_AUTH){// 市级负责人,3
    	        				continue;
            	            }else{
            	            	if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
            	            }
    					}
    					/*if(list.get(i).getTypeInt()==1){//  区域表里    type=1  一级负责人   =2  二级  =3  三级加调查员
        				if(ZsAuthDegree.CITY_AUTH.equals(authDegree)){// 市级负责人,3
        					if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
        						aerStringOne.append(list.get(i).getDistrictName()+"，");
        					}
        	            }
        				}else */if(list.get(i).getTypeInt()==2){
        					if(ZsAuthDegree.CITY_AUTH.equals(authDegree)){// 市级负责人,3
        						if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName()+"，");
            					}
        	            	}else if(ZsAuthDegree.FIRST_AUTH.equals(authDegree)){//一级负责人,4
        	            		if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName()+"，");
            					}
        	            	}
        					
        				}else if(list.get(i).getTypeInt()==3){
        					if(ZsAuthDegree.CITY_AUTH.equals(authDegree)){// 市级负责人,3
        						if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
            						aerStringOne.append(list.get(i).getCommunityName()+"，");
            					}
        	            	}else if(ZsAuthDegree.FIRST_AUTH.equals(authDegree)){//一级负责人,4
        	            		if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
            						aerStringOne.append(list.get(i).getCommunityName()+"，");
            					}
        	            	}else if(ZsAuthDegree.SECOND_AUTH.equals(authDegree)){// 二级负责人,5
        	            		if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
            						aerStringOne.append(list.get(i).getCommunityName()+"，");
            					}
        	            	}else if(ZsAuthDegree.THIRD_AUTH.equals(authDegree)){// 三级负责人,6
        	            		if(list.get(i).getAuthDegree()==ZsAuthDegree.INTINVESTIGATOR_AUTH){//只能查询调查员,7
        	            			if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
                						aerStringOne.append(list.get(i).getDistrictName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
                						aerStringOne.append(list.get(i).getSubofficeName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
                						aerStringOne.append(list.get(i).getCommunityName()+"，");
                					}
        	            		}
        	            	}
        					
        				}
        			}else{
        				if(list.get(i).getTypeInt()==null || list.get(i).getTypeInt()==1){
							System.out.println("=========================");
							System.out.println("添加");
    						if(authDegre == ZsAuthDegree.INTCITY_AUTH){// 市级负责人,3
    							map.put("i", orderNumber++);
    	        				
    	        				map.put("userId", list.get(i).getUserId());//序号
    	        				map.put("userName", list.get(i).getUserName());// 用户名
    	        				map.put("realName", list.get(i).getRealName());//真实名字
    	        				map.put("roleName", list.get(i).getRoleName());// 角色名
    	        				
    	        				map.put("phone", list.get(i).getPhone());//手机号
    	        				
    	        				//System.out.println(aerStringOne);
    	        				map.put("area", "全部");// 区域
    	        				map.put("seeDetail","详情");
    	        				map.put("dele","删除");
    	        				map.put("revise","修改");
    	        				listResult.add(map);
    	        				continue;
            	            }else{
            	            	if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName());
            					}
            	            }
    					}
        			//这里 把上一个 区域 加入map
        				/*if(list.get(i).getTypeInt()==1){//  区域表里    type=1  一级负责人   =2  二级  =3  三级加调查员
        					
            				if(ZsAuthDegree.CITY_AUTH.equals(authDegree)){// 市级负责人,3
            					if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName());
            					}
            	            }
            				}else*/ if(list.get(i).getTypeInt()==2){
            					if(ZsAuthDegree.CITY_AUTH.equals(authDegree)){// 市级负责人,3
            						if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            							aerStringOne.append(list.get(i).getDistrictName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
                						aerStringOne.append(list.get(i).getSubofficeName());
                					}
            	                	
            	            	}else if(ZsAuthDegree.FIRST_AUTH.equals(authDegree)){// 市级负责人,4
            						if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            							aerStringOne.append(list.get(i).getDistrictName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
                						aerStringOne.append(list.get(i).getSubofficeName());
                					}
            	                	
            	            	}else if(ZsAuthDegree.SECOND_AUTH.equals(authDegree)){//一级负责人,5
            	            		if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            							aerStringOne.append(list.get(i).getDistrictName()+"，");
            							//aerStringOne.append(list.get(i).getSubofficeName());
            							//aerStringOne.append(list.get(i).getSubofficeName());
                					}
            						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
                						aerStringOne.append(list.get(i).getSubofficeName());
                					}
            	            	}
            					
            				}else if(list.get(i).getTypeInt()==3){
            					if(ZsAuthDegree.CITY_AUTH.equals(authDegree)){// 市级负责人,3
            						
            						if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
                						aerStringOne.append(list.get(i).getDistrictName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
                						aerStringOne.append(list.get(i).getSubofficeName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
                						aerStringOne.append(list.get(i).getCommunityName());
                					}
            						
            	            	}else if(ZsAuthDegree.FIRST_AUTH.equals(authDegree)){//一级负责人,4
            	            		
            	            		if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
                						aerStringOne.append(list.get(i).getDistrictName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
                						aerStringOne.append(list.get(i).getSubofficeName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
                						aerStringOne.append(list.get(i).getCommunityName());
                					}
            	                	
            	            	}else if(ZsAuthDegree.SECOND_AUTH.equals(authDegree)){// 二级负责人,5
            	            		
            	            		if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
                						aerStringOne.append(list.get(i).getDistrictName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
                						aerStringOne.append(list.get(i).getSubofficeName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
                						aerStringOne.append(list.get(i).getCommunityName());
                					}
            	                	
            	            	}else if(ZsAuthDegree.THIRD_AUTH.equals(authDegree)){// 三级负责人,6
            	            		if(list.get(i).getAuthDegree()==ZsAuthDegree.INTINVESTIGATOR_AUTH){// 三级负责人只查询调查员,7
            	            			if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
                    						aerStringOne.append(list.get(i).getDistrictName()+"，");
                    					}
                						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
                    						aerStringOne.append(list.get(i).getSubofficeName()+"，");
                    					}
                						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
                    						aerStringOne.append(list.get(i).getCommunityName());
                    					}
            	            		}
            	            	}
            				}
        				map.put("i", orderNumber++);
        				
        				map.put("userId", list.get(i).getUserId());//序号
        				map.put("userName", list.get(i).getUserName());// 用户名
        				map.put("realName", list.get(i).getRealName());//真实名字
        				map.put("roleName", list.get(i).getRoleName());// 角色名
        				
        				map.put("phone", list.get(i).getPhone());//手机号
        				
        				//System.out.println(aerStringOne);
        				String area = aerStringOne.toString();
        				map.put("area", area);// 区域
        				map.put("seeDetail","详情");
        				map.put("dele","删除");
        				map.put("revise","修改");
        				listResult.add(map);
        				aerStringOne.delete(0,aerStringOne.length()); 
        			}
    					
    			}else{
    				
        			//这里 把上一个 区域 加入map
    				if(list.get(i).getTypeInt()==null || list.get(i).getTypeInt()==1){//  区域表里    type=1  一级负责人   =2  二级  =3  三级加调查员
    						if(authDegre == ZsAuthDegree.INTCITY_AUTH){// 市级负责人,3
    							map.put("i", orderNumber++);
    	        				
    	        				map.put("userId", list.get(i).getUserId());//序号
    	        				map.put("userName", list.get(i).getUserName());// 用户名
    	        				map.put("realName", list.get(i).getRealName());//真实名字
    	        				map.put("roleName", list.get(i).getRoleName());// 角色名
    	        				
    	        				map.put("phone", list.get(i).getPhone());//手机号
    	        				
    	        				//System.out.println(aerStringOne);
    	        				map.put("area", "全部");// 区域
    	        				map.put("seeDetail","详情");
    	        				map.put("dele","删除");
    	        				map.put("revise","修改");
    	        				listResult.add(map);
    	        				continue;
            	            }else{
            	            	if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
            	            }
    					}
    					/*if(ZsAuthDegree.CITY_AUTH.equals(authDegree)){// 市级负责人,3
        					if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
        						aerStringOne.append(list.get(i).getDistrictName());
        					}
        	            }
        				}else*/ if(list.get(i).getTypeInt()==2){
        					if(ZsAuthDegree.CITY_AUTH.equals(authDegree)){// 市级负责人,3
        						if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
        							aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName());
            					}
        	            	}else if(ZsAuthDegree.FIRST_AUTH.equals(authDegree)){//一级负责人,4
        	            		if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
        							aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName());
            					}
        					
        				    }
        				}else if(list.get(i).getTypeInt()==3){
        					if(ZsAuthDegree.CITY_AUTH.equals(authDegree)){// 市级负责人,3
        						
        						if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
            						aerStringOne.append(list.get(i).getCommunityName());
            					}
        	            	}else if(ZsAuthDegree.FIRST_AUTH.equals(authDegree)){//一级负责人,4
        	            		
        	            		if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
            						aerStringOne.append(list.get(i).getCommunityName());
            					}
        	            	}else if(ZsAuthDegree.SECOND_AUTH.equals(authDegree)){// 二级负责人,5
        	            		
        	            		if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
            						aerStringOne.append(list.get(i).getDistrictName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
            						aerStringOne.append(list.get(i).getSubofficeName()+"，");
            					}
        						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
            						aerStringOne.append(list.get(i).getCommunityName());
            					}
        	            	}else if(ZsAuthDegree.THIRD_AUTH.equals(authDegree)){// 三级负责人,6
        	            		if(list.get(i).getAuthDegree()==ZsAuthDegree.INTINVESTIGATOR_AUTH){//三级负责人只查询调查员，7
        	            			if(aerStringOne.indexOf(list.get(i).getDistrictName())==-1){
                						aerStringOne.append(list.get(i).getDistrictName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getSubofficeName())==-1){
                						aerStringOne.append(list.get(i).getSubofficeName()+"，");
                					}
            						if(aerStringOne.indexOf(list.get(i).getCommunityName())==-1){
                						aerStringOne.append(list.get(i).getCommunityName());
                					}
        	            		}
        	            	}
        				}
    			
    				map.put("i", orderNumber++);
    				map.put("userId", list.get(i).getUserId());//序号
    				map.put("userName", list.get(i).getUserName());// 用户名
    				map.put("realName", list.get(i).getRealName());//真实名字
    				map.put("roleName", list.get(i).getRoleName());// 角色名
    				
    				map.put("phone", list.get(i).getPhone());//手机号
    				
    				//System.out.println(aerStringOne);
    				String area = aerStringOne.toString();
    				map.put("area", area);// 区域
    				map.put("seeDetail","详情");
    				map.put("dele","删除");
    				map.put("revise","修改");
    				listResult.add(map);
    				aerStringOne.delete(0,aerStringOne.length()); 
    			
    		}
    	
		  }
    	}
    	response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
    }
	
	
	/**
	 * 查询角色信息
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/loadRole", method=RequestMethod.POST)
    public void loadRole(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
    	request.setCharacterEncoding("UTF-8");
    	//先获取当前登录用户的id和权限
    	/*HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		
		//String area = (String) session.getAttribute("area");
		//String type = (String) session.getAttribute("type");//type:1行政区，2街道 3.小区  
		
		Integer authDegree = (Integer) session.getAttribute("authDegree");//获取当前登陆用户的角色的权限等级
		authDegree = 2;//测试用数据
		*/	
    	HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		
		Integer isSuperAdmin =  (Integer) session.getAttribute("isSuperAdmin");//获取用户是否为超级管理员
		if(isSuperAdmin == null){
			isSuperAdmin = 0 ;
		}

		
		Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
		String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限(1,2是用户管理和角色管理权限，3以后为相应角色权限)
		String userName = (String) session.getAttribute("userName");
		
		String[] array = authDegrees.split(",");
		Integer  authDegree = 0;
		Boolean flag = true;
		for(String auth:array){
			if(auth.equals(ZsAuthDegree.PERSON_AUTH)){
				 flag = false;
			}
			if(auth.equals(ZsAuthDegree.CITY_AUTH)||auth.equals(ZsAuthDegree.FIRST_AUTH)||
					auth.equals(ZsAuthDegree.SECOND_AUTH)||auth.equals(ZsAuthDegree.THIRD_AUTH)||
					auth.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){
				authDegree = Integer.parseInt(auth);
			}
		}
		
		if(flag){//如果没有人员管理权限,直接返回
			return;
		}
		
		//String roleId = request.getParameter("roleId");
		List<SysRole> list = new ArrayList<SysRole>();
		if(isSuperAdmin == 1){//市级负责人可以添加市级负责人，其他级别只能创建自己级别以下的
			authDegree = 2;
		}
		list=roleService.getSysRoleByAuthDegree(authDegree);//当前登录用户角色以下权限的角色名
		
		/*SysRole role = roleService.getSysRoleById(Integer.parseInt(roleId));//
		list=roleService.getSysRoleByAuthDegree(role.getAuthDegree());*/
    	
    	List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
    	//int count = 0;
    	int deptId=-1;
    	if (list!=null && list.size()>0) {
    		//count = list.size();
    		for (int i = 0; i < list.size(); i++) {
    			Map<String,Object>  map=new HashMap<String,Object>();
    			SysRole entity = list.get(i);
    			deptId=list.get(i).getRoleId();
        		map.put("id", entity.getRoleId());//序号
        		map.put("roleName", entity.getRoleName());
        		listResult.add(map);
    		}
		}
    	response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
    }
	
	/**
	 * 查询用户管辖区域
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	 @RequestMapping(value="/queryAreaByUserId")
	 public void queryAreaByUserId(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
	    	request.setCharacterEncoding("UTF-8");
	    	HttpServletRequest servletRequest = (HttpServletRequest) request;
			HttpServletResponse servletResponse = (HttpServletResponse) response;
			HttpSession session = servletRequest.getSession();
			Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
			String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限(1,2是用户管理和角色管理权限，3以后为相应角色权限)
			String userName = (String) session.getAttribute("userName");
			
			String cityCode =  (String) session.getAttribute("cityCode");//获取用户城市编码
			
			String[] array = authDegrees.split(",");
			Boolean flag = true;
			String authDegree = "";
			for(String auth:array){
				if(auth.equals(ZsAuthDegree.PERSON_AUTH)){
					 flag = false;
				}
				if(auth.equals(ZsAuthDegree.CITY_AUTH)||auth.equals(ZsAuthDegree.FIRST_AUTH)||
						auth.equals(ZsAuthDegree.SECOND_AUTH)||auth.equals(ZsAuthDegree.THIRD_AUTH)||
						auth.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){
					authDegree = auth;
				}
			}
			
			if(flag){//如果没有人员管理权限,直接返回
				return;
			}
			//userId = 1;//测试数据
			//authDegree = 2;//测试用数据，authDegree等于2的时候是市级管理员
			String type = request.getParameter("type").trim();//
			
			
			List<Area> list = new ArrayList<Area>();
	    	/*if(authDegree == 2){
	    		list = areaService.getAllListArea(Integer.parseInt(type));
	    	}else{
	    		list=areaService.getAreaByuserId(userId,Integer.parseInt(type));//获取当前登录用户的管辖区域
	    	}*/
			list = areaService.getAllListArea(Integer.parseInt(type),cityCode);
			
			List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
			
			for(int i = 0; i < list.size(); i++){
				if(list.get(i).getType()==1){	    		
		    	   Map<String,Object>  map=new HashMap<String,Object>();
		    	   map.put("type", 1);
		    	   //map.put("authDegree", authDegree);
		    	   map.put("id", list.get(i).getAreaId());
		           map.put("districtNumber", list.get(i).getDistrictNumber());
		           map.put("districtName", list.get(i).getDistrictName());
		           listResult.add(map);
		    	}
				if(list.get(i).getType()==2){	    		
			    	   Map<String,Object>  map=new HashMap<String,Object>();
			    	   map.put("type", 2);
			    	   map.put("id", list.get(i).getAreaId());
			           map.put("districtNumber", list.get(i).getDistrictNumber());
			           map.put("districtName", list.get(i).getDistrictName());
			           map.put("subofficeNumber", list.get(i).getSubofficeNumber());
			           map.put("subofficeName", list.get(i).getSubofficeName());
			           
			           listResult.add(map);
			    	}
				if(list.get(i).getType()==3){	    		
			    	   Map<String,Object>  map=new HashMap<String,Object>();
			    	   map.put("type", 3);
			    	   map.put("id", list.get(i).getAreaId());
			           map.put("districtNumber", list.get(i).getDistrictNumber());
			           map.put("districtName", list.get(i).getDistrictName());
			           map.put("subofficeNumber", list.get(i).getSubofficeNumber());
			           map.put("subofficeName", list.get(i).getSubofficeName());
			           map.put("communityNumber", list.get(i).getCommunityNumber());
			           map.put("communityName", list.get(i).getCommunityName());
			           
			           listResult.add(map);
			    	}
				
			}
		
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 }
	 
	 
	 
	    /**
		 * 根据角色下拉查询出对应的下拉项
		 * @param request
		 * @param response
		 * @throws IOException
		 */
		 @RequestMapping(value="/queryAreaByRole")
		 public void queryAreaByRole(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
		    	request.setCharacterEncoding("UTF-8");
		    	HttpServletRequest servletRequest = (HttpServletRequest) request;
				HttpServletResponse servletResponse = (HttpServletResponse) response;
				HttpSession session = servletRequest.getSession();
				Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
				String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限(1,2是用户管理和角色管理权限，3以后为相应角色权限)
				String userName = (String) session.getAttribute("userName");
				
				String cityCode =  (String) session.getAttribute("cityCode");//获取用户城市编码
				
				String[] array = authDegrees.split(",");
				Boolean flag = true;
				String authDegree = "";
				for(String auth:array){
					if(auth.equals(ZsAuthDegree.PERSON_AUTH)){
						 flag = false;
					}
					if(auth.equals(ZsAuthDegree.CITY_AUTH)||auth.equals(ZsAuthDegree.FIRST_AUTH)||
							auth.equals(ZsAuthDegree.SECOND_AUTH)||auth.equals(ZsAuthDegree.THIRD_AUTH)||
							auth.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){
						authDegree = auth;
					}
				}
				
				if(flag){//如果没有人员管理权限,直接返回
					return;
				}
				//userId = 1;//测试数据
				//authDegree = 2;//测试用数据，authDegree等于2的时候是市级管理员
				String type = request.getParameter("type").trim();//
				
				
				List<Area> list = new ArrayList<Area>();
		    	/*if(authDegree == 2){
		    		list = areaService.getAllListArea(Integer.parseInt(type));
		    	}else{
		    		list=areaService.getAreaByuserId(userId,Integer.parseInt(type));//获取当前登录用户的管辖区域
		    	}*/
				if (ZsAuthDegree.CITY_AUTH.equals(authDegree)){
					list = areaService.getAllListArea(Integer.parseInt(type),cityCode);
				}else{
					list = areaService.queryAreaByRole(userId,Integer.parseInt(authDegree),Integer.parseInt(type),cityCode);
				}
				
				List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
				
				for(int i = 0; i < list.size(); i++){
					if(list.get(i).getType()==1){	    		
			    	   Map<String,Object>  map=new HashMap<String,Object>();
			    	   map.put("type", 1);
			    	   //map.put("authDegree", authDegree);
			    	   map.put("id", list.get(i).getAreaId());
			           map.put("districtNumber", list.get(i).getDistrictNumber());
			           map.put("districtName", list.get(i).getDistrictName());
			           listResult.add(map);
			    	}
					if(list.get(i).getType()==2){	    		
				    	   Map<String,Object>  map=new HashMap<String,Object>();
				    	   map.put("type", 2);
				    	   map.put("id", list.get(i).getAreaId());
				           map.put("districtNumber", list.get(i).getDistrictNumber());
				           map.put("districtName", list.get(i).getDistrictName());
				           map.put("subofficeNumber", list.get(i).getSubofficeNumber());
				           map.put("subofficeName", list.get(i).getSubofficeName());
				           
				           listResult.add(map);
				    	}
					if(list.get(i).getType()==3){	    		
				    	   Map<String,Object>  map=new HashMap<String,Object>();
				    	   map.put("type", 3);
				    	   map.put("id", list.get(i).getAreaId());
				           map.put("districtNumber", list.get(i).getDistrictNumber());
				           map.put("districtName", list.get(i).getDistrictName());
				           map.put("subofficeNumber", list.get(i).getSubofficeNumber());
				           map.put("subofficeName", list.get(i).getSubofficeName());
				           map.put("communityNumber", list.get(i).getCommunityNumber());
				           map.put("communityName", list.get(i).getCommunityName());
				           
				           listResult.add(map);
				    	}
					
				}
			
				response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
		 }
		 
	 
	 
	 /**
		 * 根据id查询用户详细信息
		 * @param request
		 * @param response
		 * @throws IOException
		 */
		 @RequestMapping(value="/queryUserByUserId")
		 public void queryUserByUserId(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
			 	System.out.println("====================");
			 	System.out.println("SysUserController.queryUserByUserId");
		    	request.setCharacterEncoding("UTF-8");
		    	HttpServletRequest servletRequest = (HttpServletRequest) request;
				HttpServletResponse servletResponse = (HttpServletResponse) response;
				HttpSession session = servletRequest.getSession();
				
				String cityCode =  (String) session.getAttribute("cityCode");//获取用户城市编码
				
		    	List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
		    	
				String userId = request.getParameter("id").trim();//用户id
			
				List<User> list=userService.getUserInfoByuserId(Integer.parseInt(userId));//查询用户信息
				//System.out.println(list.get(0).getAreaId());
			 	System.out.println("===========");
			 	System.out.println(list.size());
				if(list.size()>0){
					if(list.get(0).getTypeInt() == null){
						if(list.get(0).getAuthDegree() == ZsAuthDegree.INTCITY_AUTH){
//							List<Area> listArea = areaService.getAllListArea(1,cityCode);
//							for(Area area:listArea){
								Map<String,Object>  map=new HashMap<String,Object>();
								 map.put("id",userId);
								 map.put("userName",list.get(0).getUserName());
								 map.put("userPassword",list.get(0).getUserPassword());
								 map.put("realName",list.get(0).getRealName());
								 
								 map.put("phone", list.get(0).getPhone());
								 System.out.println(list.get(0));
								 map.put("roleId",list.get(0).getRoleId());
								 map.put("roleName",list.get(0).getRoleName());
								 map.put("remark",list.get(0).getRemark());
								 map.put("auth", list.get(0).getAuthDegree());
								 
								  map.put("type", 1);
								  // System.out.println(list.get(i).getAreaId());
//								   map.put("areaId", area.getAreaId());
//						           map.put("districtNumber", area.getDistrictNumber());
//						           map.put("districtName", area.getDistrictName());
						           listResult.add(map);
//							}
							System.out.println("==============return first=================");
							response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
							return;
						}
					}
					 int type = list.get(0).getTypeInt();
					 
					for(int i = 0; i < list.size(); i++){
						Map<String,Object>  map=new HashMap<String,Object>();
						 map.put("id",userId);
						 map.put("userName",list.get(0).getUserName());
						 map.put("userPassword",list.get(0).getUserPassword());
						 map.put("realName",list.get(0).getRealName());
						 
						 map.put("phone", list.get(0).getPhone());
						 
						 map.put("roleId",list.get(0).getRoleId());
						 map.put("roleName",list.get(0).getRoleName());
						 map.put("remark",list.get(0).getRemark());
						 map.put("auth", list.get(0).getAuthDegree());
						if(type==1){
						   map.put("type", 1);
						  // System.out.println(list.get(i).getAreaId());
						   map.put("areaId", list.get(i).getAreaId());
				           map.put("districtNumber", list.get(i).getDistrictNumber());
				           map.put("districtName", list.get(i).getDistrictName());
				           listResult.add(map);
				    	}
						if(type==2){
							   map.put("type", 2);
							   map.put("areaId", list.get(i).getAreaId());
					           map.put("districtNumber", list.get(i).getDistrictNumber());
					           map.put("districtName", list.get(i).getDistrictName());
					           map.put("subofficeNumber", list.get(i).getSubofficeNumber());
					           map.put("subofficeName", list.get(i).getSubofficeName());
					           
					           listResult.add(map);
					    	}
						if(type==3){	
							   map.put("type", 3);
					    	   map.put("areaId", list.get(i).getAreaId());
					           map.put("districtNumber", list.get(i).getDistrictNumber());
					           map.put("districtName", list.get(i).getDistrictName());
					           map.put("subofficeNumber", list.get(i).getSubofficeNumber());
					           map.put("subofficeName", list.get(i).getSubofficeName());
					           map.put("communityNumber", list.get(i).getCommunityNumber());
					           map.put("communityName", list.get(i).getCommunityName());
					           
					           listResult.add(map);
					    	}
						
					}
				
				}
			 System.out.println("==============return end=================");
				response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
				
				
		
		 }
	
	/**
	 * 查询角色对应的权限等级
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/queryAuthFlag", method=RequestMethod.POST)
    public void queryAuthFlag(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
    	request.setCharacterEncoding("UTF-8");
    	Map<String,Object>  map=new HashMap<String,Object>();
    	//String userName = request.getParameter("userName").trim();
    	String roleId = request.getParameter("roleId").trim();
    	Integer roleI = Integer.parseInt(roleId);//
    	SysRoleAuth sysRoleAuth=sysRoleAuthService.getSysRoleAuthByRoleId(roleI);
    	
    	if(sysRoleAuth!=null){
    		map.put("sysAuthId", sysRoleAuth.getSysAuthId());//序号
    		map.put("sysAuthDegree", sysRoleAuth.getAuthDegree());
    		
    	}
    	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    }
	
	
	
	/**
	 * 添加用户
	 * @param request
	 * @param response
	 * @throws Exception 
	 */
	@RequestMapping(value="/addUser", method=RequestMethod.POST)
    public void addUser(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
   	    request.setCharacterEncoding("UTF-8");
   	    HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
		String cityCode =  (String) session.getAttribute("cityCode");//获取城市编码
		String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限
		String[] array = authDegrees.split(",");
		
		Boolean flag = true;
		for(String auth:array){
			if(auth.equals(ZsAuthDegree.PERSON_AUTH)){
				 flag = false;
			}
			
		}
		
		if(flag){//如果没有人员管理权限,直接返回
			return;
		}
		
		
		//Integer authDegree = Integer.parseInt(array[array.length-1]);
		String operator = (String) session.getAttribute("userName");//创建人为当前登录人
   	    
    	List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
    	Map<String,Object>  map=new HashMap<String,Object>();
    	User model = new User();
    	String userName = request.getParameter("userName").trim();//用户名称
    	
    	/*if(userName.equals("admin")){//不能添加admin
    		map.put("errNo", "-2");
    		map.put("result", "该用户名已经存在");
    		
    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    		return;
    	}*/
    	
    	String userPassword = request.getParameter("userPassword").trim();//密码
    	
    	String realName = request.getParameter("realName").trim();//姓名
    	
    	String phone = request.getParameter("phone");
    	
    	String roleId = request.getParameter("roleId").trim();//角色名
    	String area = request.getParameter("area").trim();//区域
    	String remark = request.getParameter("remark");//备注
		System.out.println("=====================================");
		System.out.println("roleId = " + roleId);

    	model.setUserName(userName);

    	MD5Util md5=new MD5Util();//密码加密
    	if(userPassword!="" ||userPassword.equals("")){
    		model.setUserPassword(md5.getStringMD5String("123456"));//默认密码为123456
    	}else{
    		model.setUserPassword(md5.getStringMD5String(userPassword));
    	}

    	model.setRealName(realName);

    	model.setPhone(phone);

    	model.setRemark(remark);
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		model.setCreateDate(sdf.format(new Date()));
    	model.setOperator(operator);

    	model.setCityCode(cityCode);

    	User user = userService.selectByUserName(userName);

		if(user != null){
			map.put("errNo", "-2");
    		map.put("result", "该用户名已经存在");
    		//listResult.add(map);
    		//response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    		return;
		}
		if(roleId.equals("2")){
			model.setIsSuperAdmin(0);
		}else {
			model.setIsSuperAdmin(1);
		}
    	int ret = userService.insertUser(model,roleId,area);
    	if(ret>0){
    		map.put("errNo", "0");
    		map.put("result", "保存成功");

    	}else{
    		map.put("errNo", "-1");
    		map.put("message", "系统异常！");
    	}

    	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());

    }
	
	/**
	 * 修改用户
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/updateSysUser", method=RequestMethod.POST)
    public void updateSysUser(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
	    	List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
	    	Map<String,Object>  map=new HashMap<String,Object>();
	    	User model = new User();
	    	String id = request.getParameter("id").trim();
	    	String userName = request.getParameter("userName").trim();//用户名称
	    	
	    	String userPassword = request.getParameter("userPassword").trim();//密码
	    	
	    	String realName = request.getParameter("realName").trim();//姓名
	    	
	    	String phone = request.getParameter("phone");//电话号码
	    	
	    	String roleId = request.getParameter("roleId").trim();//角色名
	    	String area = request.getParameter("area").trim();//区域
	    	String remark = request.getParameter("remark");//备注
	    	
	    	model.setUserId(Integer.parseInt(id));
	    	model.setUserName(userName);
	    	MD5Util md5=new MD5Util();//密码加密
	    	
	    	model.setRealName(realName);
	    	model.setRemark(remark);
	    	
	    	model.setPhone(phone);
	    	
	    	User sysUser = userService.selectByPrimaryKey(Integer.parseInt(id));
	    	
	    	if(userPassword=="" ||userPassword.equals("")){//密码若不输入，则默认为123456
	    		model.setUserPassword(md5.getStringMD5String("123456"));
	    	}else{
	    		if(userPassword == sysUser.getUserPassword() || userPassword.equals(sysUser.getUserPassword())){
	    			model.setUserPassword(userPassword);
	    		}else{
	    			model.setUserPassword(md5.getStringMD5String(userPassword));
	    		}
	    	}
	    	
	    	Integer isSuperAdmin = sysUser.getIsSuperAdmin();
	    	if(isSuperAdmin == null){
	    		isSuperAdmin = 0;
	    	}
	    	
	    	if(isSuperAdmin == 1){//admin不能修改自己的用户名,不能修改系统超级管理员
	    		map.put("errNo", "-2");
	    		map.put("result", "不能修改系统管理员");
	    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	    		return;
	    	}
	    	User user = userService.selectByUserName(userName);
	    	if(user == null){
	    		System.out.println("可以修改！");
	    	}else if(user.getUserId() != Integer.parseInt(id) ){//admin不能修改自己的用户名,不能修改系统超级管理员
	    		map.put("errNo", "-3");
	    		map.put("result", "用户名已存在！");
	    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	    		return;
	    	}
	    	int ret = userService.updateSysUserByPrimaryKey(model,roleId,area);//通过主键id修改
	    	if(ret>0){
	    		map.put("errNo", "0");
	    		map.put("result", "修改成功");
	    		
	    	}else{
	    		map.put("errNo", "-1");
	    		map.put("message", "系统异常！");
	    	}
	    	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	}
	/**
	 * 删除用户
	 * @param request
	 * @param response
	 * @throws Exception 
	 */
	@RequestMapping(value="/deleteUser", method=RequestMethod.POST)
    public void deleteUser(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
    	request.setCharacterEncoding("UTF-8");
    	
    	/*HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		
		Integer isSuperAdmin =  (Integer) session.getAttribute("isSuperAdmin");//获取用户是否为超级管理员
		if(isSuperAdmin == null){
			isSuperAdmin = 0 ;
		}*/
    	
    	String id = request.getParameter("id");//id
    	List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
    	Map<String,Object>  map=new HashMap<String,Object>();
    	
    	User sysUser = userService.selectByPrimaryKey(Integer.parseInt(id));
    	int ret =0;
    	Integer isSuperAdmin = sysUser.getIsSuperAdmin();
    	if(isSuperAdmin == null){
    		isSuperAdmin = 0;
    	}
    	if(isSuperAdmin == 1){//admin不能修改自己的用户名,超级管理员
    		ret = -2;
    	}else{
    		ret = userService.deleteSysUserByUserId(Integer.parseInt(id));//通过主键id删除
    	}

    	if(ret>0){
    		map.put("errNo", "0");
    		map.put("result", "删除成功");
    	}else if(ret ==-2){
    		map.put("errNo", "-2");
    		map.put("result", "不能删除系统管理员");
    	}else{
    		map.put("errNo", "-1");
    		map.put("message", "系统异常！");
    	}
	
    	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    }
	
	
	/**
	 * 登录
	 * @param request
	 * @param response
	 * @throws Exception 
	 */
	@RequestMapping(value="/loginSystem", method=RequestMethod.POST)
   public void loginSystem(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
   	request.setCharacterEncoding("UTF-8");
   	HttpServletRequest servletRequest = (HttpServletRequest) request;
	HttpServletResponse servletResponse = (HttpServletResponse) response;
   	Map<String, Object> json = new HashMap<String, Object>();
   	//String paramters = request.getParameter("paramters").trim();//用户名
    //	JSONObject params=JSONObject.fromObject(paramters);
   	String username = request.getParameter("username").toString().trim();//用户名
   	String password = request.getParameter("password").toString().trim();//密码
   	HttpSession session = request.getSession();
   	//用户名不能为空
   	if ("".equals(username)) {
   		json.put("errNo", "1");
   		json.put("result", "用户名不能为空！");
   		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
   		return;
		}
   	//密码不能为空
   	if ("".equals(password)) {
   		json.put("errNo", "2");
   		json.put("result", "密码不能为空！");
   		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
   		return;
		}
   	
   	//判断用户是否存在
   	User user = userService.selectByUserName(username);//根据用户名查询出用户信息
   	if(user == null){
   		json.put("errNo", "3");
   		json.put("result", "用户名不存在！");
   		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
   		return;
   	}
   	System.out.println("-------------------------------------");
   	System.out.println(password);
   	System.out.println(user.getUserPassword());
    if(!password.equals(user.getUserPassword())){
    	json.put("errNo", "4");
   		json.put("result", "密码不正确！");
   		response.getWriter().write(ZsJsonUtil.map2Json(json).toString());
   		return;
    }
   	
   	//根据userId获取用户管理信息
    List<User> list=userService.getUserInfosByuserId(user.getUserId());//查询用户信息
    Integer isSuperAdmin = list.get(0).getIsSuperAdmin();//是否为超级管理员
    String cityCode = list.get(0).getCityCode();//城市编号
    Integer userId = list.get(0).getUserId();//用户id
    String userName = list.get(0).getUserName();//用户名
    Integer roleId =  list.get(0).getRoleId();//角色id
    String roleName = list.get(0).getRoleName();//角色名
    Integer type = list.get(0).getTypeInt();//区域type
   	String authDegree="";//权限等级
   	//String area = "";//区域
   	for (int i = 0; i < list.size(); i++) {
   		if(i==0){
   			authDegree = list.get(i).getAuthDegree()+",";
   			/*if(type == 1){
				area = list.get(i).getDistrictName()+",";
			}else if(type == 2){
				area = list.get(i).getSubofficeName()+",";
			}else if(type == 3){
				area = list.get(i).getCommunityName()+",";
			}*/
   		}else{
   			//System.out.println(list.get(i).getUserName()+list.get(i).getAuthDegree());
   			Boolean flagAuthDegree = true;
   			//Boolean flagArea = true;
   			
   			String[] degree = authDegree.split(",");
   			//String[] areas = area.split(",");
   			for(int j=0;j<degree.length;j++){
   				if(degree[j].equals(list.get(i).getAuthDegree().toString())){
   					flagAuthDegree = false;
   				}
   			}
   			/*for(int k=0;k<areas.length;k++){
   				if(type == 1){
   					if(areas[k].equals(list.get(i).getDistrictName())){
   						flagArea = false;
   					}
   				}
   				if(type == 2){
   					if(areas[k].equals(list.get(i).getSubofficeName())){
   						flagArea = false;
   					}
   				}
   				if(type == 3){
   					if(areas[k].equals(list.get(i).getCommunityName())){
   						flagArea = false;
   					}
   				}
   			}*/
   			if(flagAuthDegree){
   				authDegree += list.get(i).getAuthDegree()+",";
   			}
   			/*if(flagArea){
   				if(type == 1){
   					area += list.get(i).getDistrictName()+",";
   				}else if(type == 2){
   					area += list.get(i).getSubofficeName()+",";
   				}else if(type == 3){
   					area += list.get(i).getCommunityName()+",";
   				}
   				
   			}*/
   		}
		
   	}
   	System.out.println("权限："+authDegree);
    // 	System.out.println("区域："+area);
	//成功后将用户、角色、权限、区域信息放入session	
   	
   	request.getSession().setAttribute("isSuperAdmin", isSuperAdmin);
   	request.getSession().setAttribute("cityCode", cityCode);
   	
    request.getSession().setAttribute("userId", userId);
   	request.getSession().setAttribute("userName", userName);
   	request.getSession().setAttribute("roleId", roleId);
   	request.getSession().setAttribute("roleName", roleName);
   	request.getSession().setAttribute("type", type);
   	request.getSession().setAttribute("authDegree", authDegree);
   	//request.getSession().setAttribute("area", area);
	json.put("errNo", "0");
	json.put("result", "登录成功！");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	logger.info("系统用户登录时间"+sdf.format(new Date())+", 系统用户"+userName+"登录成功！");
   	response.getWriter().write(ZsJsonUtil.map2Json(json).toString());	 
   }
	

	/**
	 * 注销
	 * @param request
	 * @param response
	 * @throws Exception 
	 */
	@RequestMapping(value="/OutSystem", method=RequestMethod.POST)
   public void OutSystem(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
		
   	request.setCharacterEncoding("UTF-8");
   	HttpServletRequest servletRequest = (HttpServletRequest) request;
	HttpServletResponse servletResponse = (HttpServletResponse) response;
   	Map<String, Object> map = new HashMap<String, Object>();
   
   	HttpSession session = request.getSession();
   	
	request.getSession().removeAttribute("isSuperAdmin");
   	request.getSession().removeAttribute("cityCode");
   	
    request.getSession().removeAttribute("userId");
   	request.getSession().removeAttribute("userName");
   	request.getSession().removeAttribute("roleId");
   	request.getSession().removeAttribute("roleName");
   	request.getSession().removeAttribute("type");
   	request.getSession().removeAttribute("authDegree");
   	
   	map.put("flag", "1");
   	map.put("message", "退出成功！");
	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());	  
   	
	}
	
	
	/**
	 * 修改密码
	 * @param request
	 * @param response
	 * @throws Exception 
	 */
	@RequestMapping(value="/modifyPassword", method=RequestMethod.POST)
   public void modifyPassword(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
		
   	request.setCharacterEncoding("UTF-8");
   	HttpServletRequest servletRequest = (HttpServletRequest) request;
	HttpServletResponse servletResponse = (HttpServletResponse) response;
   	Map<String, Object> map = new HashMap<String, Object>();
   
   	HttpSession session = request.getSession();
   	
   	Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
   	
   	String password = request.getParameter("password");
   	String oldpassword = request.getParameter("oldpassword");//原密码
   
   	User user = userService.selectByPrimaryKey(userId);
   	
   	if(!oldpassword.equals(user.getUserPassword())){
   		map.put("errNo", "-2");
   		map.put("message", "请确认原密码！");
   		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());	
   		return;
   	}
   	
   	User model = new User();
   
   	model.setUserId(userId);
   	model.setUserPassword(password);
   	
   	int ret = userService.updateByPrimaryKeySelective(model);
   	if(ret>0){
   		map.put("errNo", "0");
   		map.put("message", "密码修改成功！");
   	}else{
   		map.put("errNo", "-1");
		map.put("message", "系统异常！");
   	}
   	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());	 
   	
	}

}
