package com.zs.traffic.controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.constant.ZsAuthDegree;
import com.zs.traffic.model.Area;
import com.zs.traffic.model.SysRole;
import com.zs.traffic.model.SysRoleAuth;
import com.zs.traffic.model.SysUserRole;
import com.zs.traffic.model.User;
import com.zs.traffic.model.UserArea;
import com.zs.traffic.service.AreaService;
import com.zs.traffic.service.RoleService;
import com.zs.traffic.service.StatisticsParticularService;
import com.zs.traffic.service.SysRoleAuthService;
import com.zs.traffic.service.SysUserRoleService;
import com.zs.traffic.service.UserService;
import com.zs.utils.MD5Util;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;
@Controller
@RequestMapping("/")
public class QuestionnaireStatisticsController extends BaseController {
	
	@Autowired
	public UserService userService;
	@Autowired
	public RoleService roleService;
	@Autowired
	public SysRoleAuthService sysRoleAuthService;
	@Autowired
	public AreaService areaService;
	@Autowired
	public StatisticsParticularService statisticsParticularService; 
	
	/**
	 * 查询用户信息
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/getQuestionnaireList", method=RequestMethod.POST)
    public void getQuestionnaireList(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
    	request.setCharacterEncoding("UTF-8");
    	
    	HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
		String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限(1,2,8,9是用户管理和角色管理权限，3以后为相应角色权限)
		
		String cityCode =  (String) session.getAttribute("cityCode");//获取用户城市编码
		
		String userName = (String) session.getAttribute("userName");//获取用户名
		String  authDegree = "";
        String[] array = authDegrees.split(",");
		
		for(String auth:array){
			if(auth.equals(ZsAuthDegree.CITY_AUTH)||auth.equals(ZsAuthDegree.FIRST_AUTH)||
					auth.equals(ZsAuthDegree.SECOND_AUTH)||auth.equals(ZsAuthDegree.THIRD_AUTH)||
					auth.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){
			/*if(auth.equals("3")||auth.equals("4")||auth.equals("5")||auth.equals("6")||auth.equals("7")){*/
				authDegree = auth;
			}
		}
    	//String  authDegree ="4";//  当前用户的权限  id
    	//Integer  userId = 44;
			List<Area> listArea = new ArrayList<Area>();
			List<String> areaNumber = new ArrayList<String>();
			if(authDegree.equals(ZsAuthDegree.FIRST_AUTH)){//一级负责人,4
				listArea=areaService.getAreaByuserId(userId,1);//根据用户id和type获取当前登录用户的管辖区域
				for(int i=0;i<listArea.size();i++){
					areaNumber.add(listArea.get(i).getDistrictNumber());
				}
				
			}else if(authDegree.equals(ZsAuthDegree.SECOND_AUTH)){//二级负责人,5
				listArea=areaService.getAreaByuserId(userId,2);//根据用户id和type获取当前登录用户的管辖区域
				for(int i=0;i<listArea.size();i++){
					areaNumber.add(listArea.get(i).getSubofficeNumber());
				}
			}else if(authDegree.equals(ZsAuthDegree.THIRD_AUTH) || authDegree.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){//三级负责人和调查员,6,7
				listArea=areaService.getAreaByuserId(userId,3);//根据用户id和type获取当前登录用户的管辖区域
				for(int i=0;i<listArea.size();i++){
					areaNumber.add(listArea.get(i).getCommunityNumber());
				}
			}
    	List<Area> list = new ArrayList<Area>();
    	List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
    	//List<User> list=userService.getUserList();//查询所有
    	if(authDegree.equals(ZsAuthDegree.CITY_AUTH)){//市级负责人，查询所有,3
    		list=areaService.getAreaList(areaNumber,ZsAuthDegree.INTCITY_AUTH,cityCode);//查询所有
    	}else if(authDegree.equals(ZsAuthDegree.FIRST_AUTH)){//一级负责人，查询所管辖的行政区下的人员
    		//根据用户权限，和管理区域 查询出当前登录用户下的所有社区的问卷统计信息,4
    		list=areaService.getAreaList(areaNumber,ZsAuthDegree.INTFIRST_AUTH,cityCode);
    	}else if(authDegree.equals(ZsAuthDegree.SECOND_AUTH)){//二级负责人，查询所管辖的所有街道人员,5
    		list=areaService.getAreaList(areaNumber,ZsAuthDegree.INTSECOND_AUTH,cityCode);
    	}else if(authDegree.equals(ZsAuthDegree.THIRD_AUTH)){//三级负责人，查询所管辖的所有社区人员,6
    		
    		list=areaService.getAreaList(areaNumber,ZsAuthDegree.INTTHIRD_AUTH,cityCode);
    	}else if(authDegree.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){//调查员，7
    	    list=areaService.getAreaList(areaNumber,ZsAuthDegree.INTINVESTIGATOR_AUTH,cityCode);
    	}

    	if(list!=null && list.size()>0) {
    		for (int i = 0; i < list.size(); i++) {
    			Map<String,Object>  map=new HashMap<String,Object>();
    			map.put("i", i+1);
    			map.put("districtName", list.get(i).getDistrictName());
    			map.put("subofficeName", list.get(i).getSubofficeName());
    			map.put("communityName", list.get(i).getCommunityName());
    			map.put("totalCount", list.get(i).getTotalCount());
    			
    			//int followCount = statisticsParticularService.selectFollowCount(list.get(i).getAreaId());
    			//int submitCount = statisticsParticularService.selectubmitCount(list.get(i).getAreaId());
    			//int notCompleteCount = statisticsParticularService.selectNotCompleteCount(list.get(i).getAreaId());
    			int followCount = 0;
    			int submitCount = 0;
    			int notCompleteCount = 0;
    			if(list.get(i).getFollowCount()!=null){
    				followCount = list.get(i).getFollowCount();
    				//System.out.println(followCount);
    			}
    			if(list.get(i).getSubmitCount()!=null){
    				submitCount = list.get(i).getSubmitCount();
        			//System.out.println(submitCount);
    			}
    			if(list.get(i).getNotCompleteCount()!=null){
    				notCompleteCount = list.get(i).getNotCompleteCount();
        			//System.out.println(notCompleteCount);
    			}
    			
    			map.put("followCount", followCount);//关注数
    			map.put("submitCount", submitCount);//完成提交数
    			map.put("notCompleteCount", notCompleteCount);//未完成提交数
    			
    			int total=1;
    			if(list.get(i).getTotalCount()!=null && list.get(i).getTotalCount()>0){
    				total = list.get(i).getTotalCount();
    			}
    			double percentageOfCompletion = (double)submitCount/total*100;
    			DecimalFormat dft  = new DecimalFormat("######0.00"); //格式化double保留1位小数  
    			
    			
    			map.put("percentageOfCompletion", dft.format(percentageOfCompletion)+"%");
    			listResult.add(map);
    		}
    	}
    	
    	response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
    
	
	}
	
	
	/**
	 * 查询区域统计信息下拉
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	 @RequestMapping(value="/queryAreaStatistics")
	 public void queryAreaStatistics(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
	    	request.setCharacterEncoding("UTF-8");
	    	HttpServletRequest servletRequest = (HttpServletRequest) request;
			HttpServletResponse servletResponse = (HttpServletResponse) response;
			HttpSession session = servletRequest.getSession();
			Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
			String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限(1,2,8,9是用户管理和角色管理权限，3以后为相应角色权限)
			
			Integer isSuperAdmin =  (Integer) session.getAttribute("isSuperAdmin");//获取用户是否为超级管理员
			if(isSuperAdmin == null){
				isSuperAdmin = 0 ;
			}
			String cityCode =  (String) session.getAttribute("cityCode");//获取用户城市编码
			
			String userName = (String) session.getAttribute("userName");//获取用户名
			String  authDegree = "";
	        String[] array = authDegrees.split(",");
			
			for(String auth:array){
				if(auth.equals(ZsAuthDegree.CITY_AUTH)||auth.equals(ZsAuthDegree.FIRST_AUTH)||
						auth.equals(ZsAuthDegree.SECOND_AUTH)||auth.equals(ZsAuthDegree.THIRD_AUTH)||
						auth.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){
				/*if(auth.equals("3")||auth.equals("4")||auth.equals("5")||auth.equals("6")||auth.equals("7")){*/
					authDegree = auth;
				}
			}
			
			//userId = 1;//测试数据
			//authDegree = 2;//测试用数据，authDegree等于2的时候是市级管理员
			
			List<Area> list = new ArrayList<Area>();
	    	if(authDegree.equals(ZsAuthDegree.CITY_AUTH)){
	    		//list = areaService.getAllListArea(1,cityCode);
	    		list = areaService.queryAreaByRole(userId,Integer.parseInt(authDegree),1,cityCode);
	    	}else if(authDegree.equals(ZsAuthDegree.FIRST_AUTH)){
	    		list=areaService.getAreaByuserId(userId,1);//获取当前登录用户的管辖区域
	    	}else if(authDegree.equals(ZsAuthDegree.SECOND_AUTH)){
	    		list=areaService.getAreaByuserId(userId,2);//获取当前登录用户的管辖区域
	    	}else if(authDegree.equals(ZsAuthDegree.THIRD_AUTH)){
	    		list=areaService.getAreaByuserId(userId,3);//获取当前登录用户的管辖区域
	    	}else if(authDegree.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){
	    		list=areaService.getAreaByuserId(userId,3);//获取当前登录用户的管辖区域
	    	}
			
			List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
			
			for(int i = 0; i < list.size(); i++){
				if(list.get(i).getType()==1){	    		
		    	   Map<String,Object>  map=new HashMap<String,Object>();
		    	   map.put("type", 1);
		    	   map.put("id", list.get(i).getAreaId());
		           map.put("districtNumber", list.get(i).getDistrictNumber());
		           map.put("districtName", list.get(i).getDistrictName());
		           listResult.add(map);
		    	}
				if(list.get(i).getType()==2){	    		
			    	   Map<String,Object>  map=new HashMap<String,Object>();
			    	   map.put("type", 2);
			    	   map.put("id", list.get(i).getAreaId());
			           map.put("districtNumber", list.get(i).getDistrictNumber());
			           map.put("districtName", list.get(i).getDistrictName());
			           map.put("subofficeNumber", list.get(i).getSubofficeNumber());
			           map.put("subofficeName", list.get(i).getSubofficeName());
			           listResult.add(map);
			    	}
				if(list.get(i).getType()==3){	    		
			    	   Map<String,Object>  map=new HashMap<String,Object>();
			    	   map.put("type", 3);
			    	   map.put("id", list.get(i).getAreaId());
			           map.put("districtNumber", list.get(i).getDistrictNumber());
			           map.put("districtName", list.get(i).getDistrictName());
			           map.put("subofficeNumber", list.get(i).getSubofficeNumber());
			           map.put("subofficeName", list.get(i).getSubofficeName());
			           map.put("communityNumber", list.get(i).getCommunityNumber());
			           map.put("communityName", list.get(i).getCommunityName());
			           listResult.add(map);
			    	}
				
			}
		
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 }
	
	
	
	
	   /**
		 * 查询用户信息
		 * @param request
		 * @param response
		 * @throws IOException
		 */
		@RequestMapping(value="/queryByareaNumber", method=RequestMethod.POST)
	    public void  queryByareaNumber(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
	    	request.setCharacterEncoding("UTF-8");
	    	
	    	HttpServletRequest servletRequest = (HttpServletRequest) request;
			HttpServletResponse servletResponse = (HttpServletResponse) response;
			HttpSession session = servletRequest.getSession();
			Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
			String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限(1,2,8,9是用户管理和角色管理权限，3以后为相应角色权限)
			
			String cityCode =  (String) session.getAttribute("cityCode");//获取用户城市编码
			
			String userName = (String) session.getAttribute("userName");//获取用户名
			String  authDegree = "";
	        String[] array = authDegrees.split(",");
			
			for(String auth:array){
				if(auth.equals(ZsAuthDegree.CITY_AUTH)||auth.equals(ZsAuthDegree.FIRST_AUTH)||
						auth.equals(ZsAuthDegree.SECOND_AUTH)||auth.equals(ZsAuthDegree.THIRD_AUTH)||
						auth.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){
				/*if(auth.equals("3")||auth.equals("4")||auth.equals("5")||auth.equals("6")||auth.equals("7")){*/
					authDegree = auth;
				}
			}
	    	//String  authDegree ="4";//  当前用户的权限  id
	    	//Integer  userId = 44;
			String districtNumber = request.getParameter("districtNumber").trim();
			String subofficeNumber = request.getParameter("subofficeNumber").trim();
			List<Area> listArea = new ArrayList<Area>();
			List<String> areaNumber = new ArrayList<String>();
			int ret = 0;
			if(!subofficeNumber.isEmpty()){
				areaNumber.add(subofficeNumber);
				ret = 1;
			}else if(!districtNumber.isEmpty() && (authDegree.equals("3")||authDegree.equals("4"))){//行政区
				areaNumber.add(districtNumber);
			}else{
				if(authDegree.equals(ZsAuthDegree.CITY_AUTH)){//市级负责人
					//listArea=areaService.getAreaByuserId(userId,1);//根据用户id和type获取当前登录用户的管辖区域
					 listArea = areaService.getAllListArea(1,cityCode);
					for(int i=0;i<listArea.size();i++){
						areaNumber.add(listArea.get(i).getDistrictNumber());
						
					}
					
				}else if(authDegree.equals(ZsAuthDegree.FIRST_AUTH)){//一级负责人
					listArea=areaService.getAreaByuserId(userId,1);//根据用户id和type获取当前登录用户的管辖区域
					for(int i=0;i<listArea.size();i++){
						areaNumber.add(listArea.get(i).getDistrictNumber());
						
					}
					
				}else if(authDegree.equals(ZsAuthDegree.SECOND_AUTH)){//二级负责人
					listArea=areaService.getAreaByuserId(userId,2);//根据用户id和type获取当前登录用户的管辖区域
					for(int i=0;i<listArea.size();i++){
						areaNumber.add(listArea.get(i).getSubofficeNumber());
					}
				}else if(authDegree.equals(ZsAuthDegree.THIRD_AUTH) || authDegree.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){//三级负责人和调查员
					listArea=areaService.getAreaByuserId(userId,3);//根据用户id和type获取当前登录用户的管辖区域
					for(int i=0;i<listArea.size();i++){
						areaNumber.add(listArea.get(i).getCommunityNumber());
					}
				}
			}
			
	    	List<Area> list = new ArrayList<Area>();
	    	List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
	    	//List<User> list=userService.getUserList();//查询所有
	    	if(ret == 1){
	    		list=areaService.queryByareaNumber(areaNumber,5,cityCode);
	    	}else{
	    		if(authDegree.equals(ZsAuthDegree.CITY_AUTH)){//市级负责人，查询所有
	    			list=areaService.queryByareaNumber(areaNumber,ZsAuthDegree.INTCITY_AUTH,cityCode);//查询所有
	    		}else if(authDegree.equals(ZsAuthDegree.FIRST_AUTH)){//一级负责人，查询所管辖的行政区下的人员
	    			//根据用户权限，和管理区域 查询出当前登录用户下的所有社区的问卷统计信息
	    			list=areaService.queryByareaNumber(areaNumber,ZsAuthDegree.INTFIRST_AUTH,cityCode);
	    		}else if(authDegree.equals(ZsAuthDegree.SECOND_AUTH)){//二级负责人，查询所管辖的所有街道人员
	    			list=areaService.queryByareaNumber(areaNumber,ZsAuthDegree.INTSECOND_AUTH,cityCode);
	    		}else if(authDegree.equals(ZsAuthDegree.THIRD_AUTH)){//三级负责人，查询所管辖的所有社区人员
	    			
	    			list=areaService.queryByareaNumber(areaNumber,ZsAuthDegree.INTTHIRD_AUTH,cityCode);
	    		}else if(authDegree.equals(ZsAuthDegree.INVESTIGATOR_AUTH)){
	    			list=areaService.queryByareaNumber(areaNumber,ZsAuthDegree.INTINVESTIGATOR_AUTH,cityCode);
	    		}
	    	}

	    	if(list!=null && list.size()>0) {
	    		for (int i = 0; i < list.size(); i++) {
	    			Map<String,Object>  map=new HashMap<String,Object>();
	    			map.put("i", i+1);
	    			map.put("authDegree", authDegree);
	    			map.put("districtName", list.get(i).getDistrictName());
	    			map.put("subofficeName", list.get(i).getSubofficeName());
	    			map.put("communityName", list.get(i).getCommunityName());
	    			map.put("totalCount", list.get(i).getTotalCount());
	    			
	    			//int followCount = statisticsParticularService.selectFollowCount(list.get(i).getAreaId());
	    			//int submitCount = statisticsParticularService.selectubmitCount(list.get(i).getAreaId());
	    			//int notCompleteCount = statisticsParticularService.selectNotCompleteCount(list.get(i).getAreaId());
	    			
	    			int followCount = 0;
	    			int submitCount = 0;
	    			int notCompleteCount = 0;
	    			if(list.get(i).getFollowCount()!=null){
	    				followCount = list.get(i).getFollowCount();
	    				//System.out.println(followCount);
	    			}
	    			if(list.get(i).getSubmitCount()!=null){
	    				submitCount = list.get(i).getSubmitCount();
	        			//System.out.println(submitCount);
	    			}
	    			if(list.get(i).getNotCompleteCount()!=null){
	    				notCompleteCount = list.get(i).getNotCompleteCount();
	        			//System.out.println(notCompleteCount);
	    			}
	    			
	    			int total=1;
	    			if(list.get(i).getTotalCount()!=null && list.get(i).getTotalCount()>0){
	    				total = list.get(i).getTotalCount();
	    			}
	    			
	    			double percentageOfCompletion = (double)submitCount/total*100;
	    			DecimalFormat dft  = new DecimalFormat("######0.00"); //格式化double保留1位小数  
	    			
	    			map.put("percentageOfCompletion", dft.format(percentageOfCompletion)+"%");//完成率
	    			
	    			map.put("followCount", followCount);//关注数
	    			map.put("submitCount", submitCount);//完成提交数
	    			map.put("notCompleteCount", notCompleteCount);//未完成提交数
	    			listResult.add(map);
	    		}
	    	}
	    	
	    	response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	    
		
		}
	
	
	
	
	
	
	
	
 }
