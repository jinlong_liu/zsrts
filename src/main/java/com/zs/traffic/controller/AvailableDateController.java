package com.zs.traffic.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.traffic.model.Auth;
import com.zs.traffic.service.AuthService;
import com.zs.utils.XssHttpServletRequestWrapper;


/**
 * @description 获取可查询的数据的日期列表
 * 
 * @author renxw
 * 
 * @date 2016-06-18
 *
 */

@Controller
@RequestMapping("/")
public class AvailableDateController extends BaseController {
	
	@Autowired
	public AuthService auService;
	
	@RequestMapping(value="/getTest", method=RequestMethod.GET)
    public void getTest(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
    	request.setCharacterEncoding("UTF-8");
    	
    	List<Auth> list = auService.getAuthList();
    	System.out.println(list.get(0).getName()+"----"+list.get(1).getName());
    	List<Map<String, Object>> listYear = new ArrayList<Map<String, Object>>();
    	response.getWriter().write("1111");	  
    }
	
	
}
