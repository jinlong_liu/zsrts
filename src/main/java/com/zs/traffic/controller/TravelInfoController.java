package com.zs.traffic.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.traffic.model.TravelInformation;
import com.zs.traffic.service.TravelInformationService;
import com.zs.utils.InverseGeocoding;
import com.zs.utils.VerificationUtil;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;

@Controller
@RequestMapping("/app")
public class TravelInfoController extends BaseController{
	
	@Autowired
	public TravelInformationService service;
	
	/**
	 * @description APP 出行信息提交或修改
	 * (目前APP批量修改调用该方法)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-03-31
	 */
	@RequestMapping(value="saveOrUpdateTravelInfo",method=RequestMethod.POST)
	public void saveOrUpdateTravelInfo(XssHttpServletRequestWrapper request,HttpServletResponse response)throws Exception{
		request.setCharacterEncoding("UTF-8");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String cityCode = request.getParameter("cityCode");//必传字段，不能为空
		String status = request.getParameter("status");
		String dayDate = request.getParameter("dayDate");//获取 页面传过来的日期 
		String travelInfos = request.getParameter("travelInfo");
		
		if(id == null || cityCode == null || status == null || dayDate == null ||
				travelInfos == null){
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		id = id.trim();
		cityCode = cityCode.trim();
		status = status.trim();
		dayDate = dayDate.trim();
		travelInfos = travelInfos.trim();
		if(id.isEmpty() || cityCode.isEmpty() || status.isEmpty() || dayDate.isEmpty() ||
				travelInfos.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(id.length()>11 || cityCode.length()>255 || status.length()>11){
			map.put("errNo", "-7");
	 		map.put("result", "参数长度超出范围");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(VerificationUtil.isNumeric(id) || VerificationUtil.isNumeric(status)){
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return;
		}
		if(VerificationUtil.isValidDateDay(dayDate)){
			map.put("errNo", "-5");
	 		map.put("result", "日期类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		String[] travel = travelInfos.split(",,");
		int ret = 0;
		
		String key = getProperties().getProperty("key");//获取调用地图的key
		
		//service.deleteByAppuserIdStatus(appuserId, sdfDate.parse(dayDate));//添加出行信息前先删除
		
        if(status=="2" || status.equals("2")){//当status=2证明当天无出行记录，备注不能空
        	TravelInformation model= new TravelInformation(); 
			String remark = travel[0];
			model.setAppuserId(appuserId);
			model.setCurrentDate(sdfDate.parse(dayDate));
			model.setRemark(remark);
			model.setStatus(Integer.parseInt(status));
			model.setCityCode(cityCode);
			ret = service.insertSelective(model);//保存到数据库
		}else{
			for(String travelInfomation : travel){
				try {
				TravelInformation model= new TravelInformation(); 
				String[] travelInfo = travelInfomation.split(" ");
				//添加对应字段
				String startDate = dayDate+" "+travelInfo[0];//对时间进行处理
				String endDate = dayDate+" "+travelInfo[3];//对时间进行处理
				
				model.setStartDate(sdf.parse(startDate));
				model.setEndDate(sdf.parse(endDate));
				model.setStartAddress(travelInfo[1]);
				model.setEndAddress(travelInfo[4]);
				model.setStartGps("("+travelInfo[2]+")");
				model.setEndGps("("+travelInfo[5]+")");
				model.setTripObjective(travelInfo[6]);//出行目的
				model.setTripMode(travelInfo[7]);//出行方式
				
				if (travelInfo.length > 8) {
					if(!travelInfo[8].equals("null")){
						model.setIsSendChildren(Integer.parseInt(travelInfo[8]));//是否接送小孩
					}
				}
				if (travelInfo.length > 9){
					if(!travelInfo[9].equals("null")){
						model.setSendAdress(travelInfo[9]);
					}
				}
				
				if (travelInfo.length > 10){
					if(!travelInfo[10].equals("null")){
						String sendDate = dayDate+" "+travelInfo[10];//对时间进行处理
						model.setSendDate(sdf.parse(sendDate));
					}
				}
				
				if (travelInfo.length > 11) {
					if(!travelInfo[11].equals("null")){
						model.setTravelTime(Integer.parseInt(travelInfo[11]));//出行时长
					}
				}
				/*if (travelInfo.length > 12) {
	                if(!travelInfo[12].equals("null")){
	                	model.setDistance(Double.parseDouble(travelInfo[12]));//出行距离
	                }
				}*/
				
				
				/*if(!(travelInfo[6].equals("2")||travelInfo[6].equals("6"))){//出行目的可选是否接送小孩(2=上学，6=回程)
					model.setIsSendChildren(Integer.parseInt(travelInfo[10]));
					if(!travelInfo[10].equals("0")){//是否为接送小孩
						model.setSendAdress(travelInfo[11]);
						String sendDate = dayDate+" "+travelInfo[12];//对时间进行处理
						model.setSendDate(sdf.parse(sendDate));
					}
				}*/
				
				model.setCurrentDate(sdfDate.parse(dayDate));//添加当天日期
				model.setStatus(Integer.parseInt(status));//默认提交后改为1
				model.setAppuserId(appuserId);//登录用户id
				model.setCityCode(cityCode);
				
				if(travelInfo.length > 12){
					if(travelInfo[12].equals("")){
						
						String startGps = travelInfo[2];
						String[] starGpsA = startGps.split(",");
						String lngA = starGpsA[0];
						String latA = starGpsA[1];
						
						List<String> startAddressList = InverseGeocoding.addressPois(lngA,latA,key);
						
						if(startAddressList.size()>2){
							model.setStartFormatAddress(startAddressList.get(1));
						}
					}else{	
						model.setStartFormatAddress(travelInfo[12]);
					}
				}
				if(travelInfo.length > 13){
					if(travelInfo[13].equals("")){
						String endGps = travelInfo[5];
						String[] endGpsB = endGps.split(",");
						String lngB = endGpsB[0];
						String latB = endGpsB[1];
						
						List<String> endAddressList = InverseGeocoding.addressPois(lngB,latB,key);
						
						if(endAddressList.size()>2){
							model.setEndFormatAddress(endAddressList.get(1));
						}
					}else{
						model.setEndFormatAddress(travelInfo[13]);
					}
				}
				
				if(travelInfo.length > 14){
					model.setId(Integer.parseInt(travelInfo[14]));
				}
				if(travelInfo.length > 15){
					model.setRanking(Integer.parseInt(travelInfo[15]));
				}
				
				/*if (travelInfo.length > 16) {
	                if(!travelInfo[12].equals("null")){
	                	model.setDistance(Double.parseDouble(travelInfo[16]));//出行距离
	                }else{
	                model.setDistance(null);
	                }
				}*/
				model.setDistance(null);//距离默认为null，不让修改
				
				//service.insertSelective(model);//保存到数据库
				service.updateByPrimaryKeySelective(model);
				ret++;
			} catch (Exception e) {
				map.put("errNo", "-6");
		 		map.put("result", "出行信息参数类型格式不正确");
		 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
		 		return;
			}
			}
			
		}
		
		if(ret>0){
			map.put("errNo", "0");
			map.put("result", "成功!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}else{
			map.put("errNo", "-1");
			map.put("result", "修改失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		//必填字段，出行描述(status=0,对应开始出行。status=1，对应结束出行)
		/*String status = request.getParameter("status");
		
		String startAddress = request.getParameter("startAddress");//开始地址
		String endAddress = request.getParameter("endAddress");//结束地址
		String startGps = request.getParameter("startGps");//开始GPS
		String endGps = request.getParameter("endGps");//结束GPS
		String tripMode = request.getParameter("tripMode");//出行描述
        */		
		/*//开始出行时
		if(status=="0" || status.equals("0")){
			//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			//model.setStartDate(sdf.format(new Date()));
			model.setStartDate(new Date());
			model.setStartAddress(startAddress);
			model.setStartGps(startGps);
			model.setAppuserId(appuserId);
			if(!tripMode.isEmpty()){
				model.setTripMode(tripMode);
			}
			int ret = service.insert(model);
			if(ret>0){
				map.put("errNo", "0");
				map.put("result", "成功!");
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				return;
			}else{
				map.put("errNo", "-1");
				map.put("result", "服务器异常!");
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				return;
			}
			
		}
		
		//结束出行
		if(status=="1" || status.equals("1")){
			//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			//model.setStartDate(sdf.format(new Date()));
			model.setEndDate(new Date());
			model.setEndAddress(endAddress);
			model.setEndGps(endGps);
			
			//判断，结束出行时，出行描述必填
			TravelInformation travelInformation = service.selectByAppuserId(appuserId);
			if(travelInformation==null){
				map.put("errNo", "-1");
				map.put("result", "服务器异常!");
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				return;
			}
			if(travelInformation.getTripMode().isEmpty()){
				if(tripMode.isEmpty()){
					map.put("errNo", "-2");
					map.put("result", "请填写出行方式!");
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
					return;
				}
				model.setTripMode(tripMode);
			}
			model.setId(travelInformation.getId());
			
			int retu = service.updateByPrimaryKeySelective(model);
			if(retu>0){
				map.put("errNo", "0");
				map.put("result", "成功!");
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				return;
			}else{
				map.put("errNo", "-1");
				map.put("result", "服务器异常!");
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				return;
			}
			
		}*/
		

	}
	
	
	
	/**
	 * @description APP查询当天的出行信息
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-03-31
	 */
	@RequestMapping(value="queryTravelInfo",method=RequestMethod.POST)
	public void queryTravelInfo(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
		
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String dayDate = request.getParameter("dayDate");//获取 页面传过来的日期
		
		//假数据，测试用
		//dayDate= "2017-04-21";
		//Integer appuserId=18;
		if(id == null || dayDate == null){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		id = id.trim();
		dayDate = dayDate.trim();
		if(id.isEmpty() || dayDate.isEmpty()){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		if(id.length()>11){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-6");
	 		map.put("result", "参数长度超出范围");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		if(VerificationUtil.isNumeric(id)){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		if(VerificationUtil.isValidDateDay(dayDate)){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-5");
	 		map.put("result", "日期类型参数格式不正确");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm");//往页面传值时，要此格式
		Date todayDate = sdf.parse(dayDate);
		/*Calendar calendar = new GregorianCalendar(); 
	    calendar.setTime(todayDate); 
	    calendar.add(calendar.DATE,1);//把日期往后增加一天.整数往后推,负数往前移动 
		Date tomorrowDate = calendar.getTime(); //这个时间就是日期往后推一天的结果
*/		
		List<TravelInformation> list = service.selectByAppuserId(appuserId,todayDate);
		
		if(list.size() > 0){//判断是否有提交过的数据
			boolean flag = false;
			for(int i = 0; i < list.size(); i++){
				if(list.get(i).getStatus() != 0){
					flag = true;
					break;
				}
			}
			if(flag){
				for(int i = 0; i < list.size(); i++){//status 1和2不能同时有
					if(list.get(i).getStatus() == 1){//修改后只能看到修改后的数据，status=1
						Map<String, Object> map = new HashMap<String, Object>();
						
						map.put("errNo", "0");
						map.put("id", list.get(i).getId());
						map.put("appuserId", list.get(i).getAppuserId());//登录用户id
						map.put("startDate", sdfDate.format(list.get(i).getStartDate()));
						map.put("endDate", sdfDate.format(list.get(i).getEndDate()));
						map.put("startAddress", list.get(i).getStartAddress());
						map.put("endAddress", list.get(i).getEndAddress());
						map.put("startGps", list.get(i).getStartGps());
						map.put("endGps", list.get(i).getEndGps());
						map.put("tripMode", list.get(i).getTripMode());
						map.put("tripObjective", list.get(i).getTripObjective());
						map.put("status", list.get(i).getStatus());
						map.put("currentDate", list.get(i).getCurrentDate());
						map.put("remark", list.get(i).getRemark());
						map.put("distance", list.get(i).getDistance());//距离
						map.put("travelTime", list.get(i).getTravelTime());//时长
						map.put("isSendChildren", list.get(i).getIsSendChildren());//是否接送小孩
						
						map.put("cityCode", list.get(i).getCityCode());
						
						//System.out.println(list.get(i).getIsSendChildren());
						
						map.put("sendAdress", list.get(i).getSendAdress());//接送小孩地址
						//System.out.println(list.get(i).getSendDate());
						if(list.get(i).getSendDate()==null){
							map.put("sendDate", null);//接送小孩时间
						}else{
							map.put("sendDate", sdfDate.format(list.get(i).getSendDate()));//接送小孩时间
						}
						map.put("startFormatAddress", list.get(i).getStartFormatAddress());//开始短地址
						map.put("endFormatAddress", list.get(i).getEndFormatAddress());//结束短地址
						//map.put("tripGps", list.get(i).getTripGps());//该次出行所有经纬度
						map.put("ranking", list.get(i).getRanking());//该用户当天第几条出行信息
						
						listResult.add(map);
					}
					if(list.get(i).getStatus() == 2){//修改后只能看到修改后的数据，status=2没有出行记录
						Map<String, Object> map = new HashMap<String, Object>();
						
						map.put("errNo", "0");
						map.put("id", list.get(i).getId());
						map.put("appuserId", list.get(i).getAppuserId());//登录用户id
						map.put("status", list.get(i).getStatus());
						map.put("currentDate", list.get(i).getCurrentDate());
						map.put("remark", list.get(i).getRemark());
						map.put("cityCode", list.get(i).getCityCode());
						listResult.add(map);
					}
				    }
				 }else{
					for(int i = 0; i < list.size(); i++){
						if(list.get(i).getStatus() == 0){//没修改前只能看到分析出来的数据，status=0
						Map<String, Object> map = new HashMap<String, Object>();
						
						map.put("errNo", "0");
						map.put("id", list.get(i).getId());
						map.put("appuserId", list.get(i).getAppuserId());//登录用户id
						map.put("startDate", sdfDate.format(list.get(i).getStartDate()));
						map.put("endDate", sdfDate.format(list.get(i).getEndDate()));
						map.put("startAddress", list.get(i).getStartAddress());
						map.put("endAddress", list.get(i).getEndAddress());
						map.put("startGps", list.get(i).getStartGps());
						map.put("endGps", list.get(i).getEndGps());
						map.put("tripMode", list.get(i).getTripMode());
						map.put("tripObjective", list.get(i).getTripObjective());
						map.put("status", list.get(i).getStatus());
						map.put("currentDate", list.get(i).getCurrentDate());
						map.put("remark", list.get(i).getRemark());
						map.put("distance", list.get(i).getDistance());//距离
						map.put("travelTime", list.get(i).getTravelTime());//时长
						map.put("isSendChildren", list.get(i).getIsSendChildren());//是否接送小孩
						
						map.put("cityCode", list.get(i).getCityCode());
						
						map.put("sendAdress", list.get(i).getSendAdress());//接送小孩地址
						//System.out.println(list.get(i).getSendDate());
						if(list.get(i).getSendDate()==null){
							map.put("sendDate", null);//接送小孩时间
						}else{
							map.put("sendDate", sdfDate.format(list.get(i).getSendDate()));//接送小孩时间
						}
						
						map.put("startFormatAddress", list.get(i).getStartFormatAddress());//开始短地址
						map.put("endFormatAddress", list.get(i).getEndFormatAddress());//结束短地址
						map.put("tripGps", list.get(i).getTripGps());//该次出行所有经纬度
						map.put("ranking", list.get(i).getRanking());//该用户当天第几条出行信息
						
						listResult.add(map);
					}
				}
			}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
        }
	}
	
	/**
	 * @description APP查询出行信息
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-06-24
	 */
	@RequestMapping(value="queryTravelInfoNear",method=RequestMethod.POST)
	public void queryTravelInfoNear(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
		
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String dayDate = request.getParameter("dayDate");//获取 页面传过来的日期
		
		if(id == null || dayDate == null){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		id = id.trim();
		dayDate = dayDate.trim();
		if(id.isEmpty() || dayDate.isEmpty()){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		if(id.length()>11){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-6");
	 		map.put("result", "参数长度超出范围");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		if(VerificationUtil.isNumeric(id)){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		if(VerificationUtil.isValidDateDay(dayDate)){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-5");
	 		map.put("result", "日期类型参数格式不正确");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm");//往页面传值时，要此格式
		Date todayDate = sdf.parse(dayDate);
		/*
		Calendar calendar = new GregorianCalendar(); 
	    calendar.setTime(todayDate); 
	    calendar.add(calendar.DATE,1);//把日期往后增加一天.整数往后推,负数往前移动 
		Date tomorrowDate = calendar.getTime(); //这个时间就是日期往后推一天的结果
        */
		List<TravelInformation> list = service.selectByAppuserIdNear(appuserId,todayDate);
		
		for(int i = 0; i < list.size(); i++){//status 1
				Map<String, Object> map = new HashMap<String, Object>();
				
				map.put("errNo", "0");
				map.put("id", list.get(i).getId());
				map.put("appuserId", list.get(i).getAppuserId());//登录用户id
				map.put("startDate", sdfDate.format(list.get(i).getStartDate()));
				map.put("endDate", sdfDate.format(list.get(i).getEndDate()));
				map.put("startAddress", list.get(i).getStartAddress());
				map.put("endAddress", list.get(i).getEndAddress());
				map.put("startGps", list.get(i).getStartGps());
				map.put("endGps", list.get(i).getEndGps());
				map.put("tripMode", list.get(i).getTripMode());
				map.put("tripObjective", list.get(i).getTripObjective());
				map.put("status", list.get(i).getStatus());
				map.put("currentDate", sdf.format(list.get(i).getCurrentDate()));
				map.put("remark", list.get(i).getRemark());
				map.put("distance", list.get(i).getDistance());//距离
				map.put("travelTime", list.get(i).getTravelTime());//时长
				map.put("isSendChildren", list.get(i).getIsSendChildren());//是否接送小孩
				map.put("sendAdress", list.get(i).getSendAdress());//接送小孩地址
				if(list.get(i).getSendDate()==null){
					map.put("sendDate", null);//接送小孩时间
				}else{
					map.put("sendDate", sdfDate.format(list.get(i).getSendDate()));//接送小孩时间
				}
				map.put("startFormatAddress", list.get(i).getStartFormatAddress());//开始短地址
				map.put("endFormatAddress", list.get(i).getEndFormatAddress());//结束短地址
				map.put("ranking", list.get(i).getRanking());//该用户当天第几条出行信息
				listResult.add(map);
			}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
	
	
	/**
	 * @description APP当天出行信息统计
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-05-06
	 */
	@RequestMapping(value="travelInfoStatistics",method=RequestMethod.POST)
	public void travelInfoStatistics(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		String id = request.getParameter("appuserId");//必传字段，不能为空
		//String userName = request.getParameter("userName");//必传字段，不能为空
		String cityCode = request.getParameter("cityCode");
		if(id == null || cityCode == null){
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		id = id.trim();
		cityCode = cityCode.trim();
		if(id.isEmpty() || cityCode.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return;
		}
		if(id.length()>11 || cityCode.length()>255){
			map.put("errNo", "-5");
	 		map.put("result", "参数长度超出范围");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(VerificationUtil.isNumeric(id)){
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		
		Date date = new Date();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dayDate = sdf.format(date);//获取 页面传过来的日期
		
		//dayDate = "2017-05-06";//测试数据
		
		Date todayDate = sdf.parse(dayDate);
		
		List<TravelInformation> list = service.selectByAppuserId(appuserId,todayDate);//获取当前登录用户的出行信息
		
		int status=-1;
		
		int count = 0;//统计出行次数
		int travelTime = 0;//平均出行耗时
		int maxTravelTime = 0;//最大出行耗时
		Double travelDistance = 0.0;//平均出行距离
		Double maxTravelDistance = 0.0;//最长出行距离
		//String averageTravelDistance = "";//平均出行距离格式化
		//DecimalFormat df  = new DecimalFormat("######0.0"); //格式化double保留1位小数  
		
		int travelTimeCount = 0;//个人出行总时长
		Double travelDistanceCount = 0.0;//个人出行总距离
		
		if(list.size() > 0){//判断是否有提交过的数据
			boolean flag = false;
			for(int i = 0; i < list.size(); i++){//若为true证明已经提交过
				if(list.get(i).getStatus() != 0){
					flag = true;
				}
				if(list.get(i).getStatus()==0){
					status = 0;
				}
				else if(list.get(i).getStatus()==1){
					status = 1;
				}else if(list.get(i).getStatus()==2){
					status = 2;
				}
			}
			if(flag){
				//int travelTimeCount = 0;//个人出行总时长
				//Double travelDistanceCount = 0.0;//个人出行总距离
				for(TravelInformation model:list){//提交过的数据
					if(model.getStatus()==1){
						if(model.getTravelTime() != null){
							if(model.getTravelTime() > maxTravelTime){
								maxTravelTime = model.getTravelTime();
							}
						}else{
							model.setTravelTime(0);
						}
						if(model.getDistance() != null){
							
							if(model.getDistance() > maxTravelDistance){
								maxTravelDistance = model.getDistance();
							}
						}else{
							model.setDistance(0.0);
						}
						travelTimeCount = travelTimeCount + model.getTravelTime();
						travelDistanceCount = travelDistanceCount + model.getDistance();
						count++;
					}
				}
				if(count>0){
					travelTime = travelTimeCount/count;//整除
					travelDistance = travelDistanceCount/count;	
				}else{
					travelTime = 0;
					travelDistance = 0.0;
				}
				//averageTravelDistance = df.format(travelDistance);
			}else{                                                //没有提交过
				//int travelTimeCount = 0;//个人出行总时长
				//Double travelDistanceCount = 0.0;//个人出行总距离
				for(TravelInformation model:list){//提交过的数据
					if(model.getStatus()==0){
						if(model.getTravelTime() != null){
							if(model.getTravelTime() > maxTravelTime){
								maxTravelTime = model.getTravelTime();
							}
						}else{
							model.setTravelTime(0);
						}
						
						if(model.getDistance() != null){
							if(model.getDistance() > maxTravelDistance){
								maxTravelDistance = model.getDistance();
							}
						}else{
							model.setDistance(0.0);
						}
						travelTimeCount = travelTimeCount + model.getTravelTime();
						travelDistanceCount = travelDistanceCount + model.getDistance();
						count++;
					}
				}
				
               if(count>0){
            	   travelTime = travelTimeCount/count;//整除
   				   travelDistance = travelDistanceCount/count;	
				}else{
					travelTime = 0;
					travelDistance = 0.0;
				}
				
				//averageTravelDistance = df.format(travelDistance);
			}
		}
		//查询总量，全市平均出行次数，耗时，出行距离
		double avgCounts = 0.0;//全市平均出行次数
		TravelInformation travelInformation = service.selectTotalNumber(todayDate,cityCode);
		
		//System.out.println(travelInformation.getTotalNumber());//总的出行次数
		//System.out.println(travelInformation.getTotalAvgTripTime());//全市平均出行耗时
		//System.out.println(travelInformation.getTotalAvgDis());//全市平均出行距离
		
		List<TravelInformation> listTotal = service.selectByDate(todayDate,cityCode);//查询出当天所有的信息
		
		int timeConsumingRanking = 1;//出行耗时排名
		int distanceRanking = 1;//出行距离排名
		int tripsRanking = 1;//出行次数排名
		double perTrips = 0.0;//出行次数百分比
		
		for(TravelInformation travelInfo:listTotal){
			if( travelInfo.getAppuserId() != appuserId){
				if(travelTime<travelInfo.getAvgTripTime()){//出行耗时排名
					timeConsumingRanking++;
				}
				if(travelDistance<travelInfo.getAvgDis()){
					distanceRanking++;
				}
				if(count<travelInfo.getTrips()){
					tripsRanking++;
				}
			}
		}
		
		if(listTotal.size()>0){
			avgCounts = (double)travelInformation.getTotalNumber()/listTotal.size();
			perTrips = (double)(listTotal.size()-tripsRanking)/listTotal.size()*100;
		}else{
			avgCounts = 0;
			perTrips = 0;
		}
		
		List<TravelInformation> modeList = service.selectTripByAppuserId(appuserId, todayDate);//查询出当前登陆人的出行方式和次数
		/*if(modeList.size()>0){
			System.out.println(modeList.get(0).getTripMode()+modeList.get(0).getTripsMode());
		}*/
		
		List<TravelInformation> objectiveList = service.selectTripObjectiveByAppuserId(appuserId, todayDate);//查询出当前登陆人的出行目的和次数
		/*if(objectiveList.size()>0){
			System.out.println(objectiveList.get(0).getTripObjective()+objectiveList.get(0).getTripsObjective());
		}*/
		
		DecimalFormat df  = new DecimalFormat("######0.0"); //格式化double保留1位小数  
		DecimalFormat dft  = new DecimalFormat("######0.00"); //格式化double保留1位小数  
		
		map.put("errNo", "0");
		map.put("status", status);//0，未提交过，1，提交过并有出行信息，2提交备注，无出行信息
		
		map.put("travelTime", travelTime);//每天平均出行耗时
		map.put("maxTravelTime", maxTravelTime);//每天最久一次出行耗时
		map.put("totalAvgTripTime", df.format(travelInformation.getTotalAvgTripTime()));//全市参与者平均出行耗时
		map.put("timeConsumingRanking", timeConsumingRanking);//当天出行耗时排名
		
		map.put("travelDistance", df.format(travelDistance));//每天平均出行距离
		map.put("maxTravelDistance", df.format(maxTravelDistance));//每天最长一次出行距离
		map.put("totalAvgDis", df.format(travelInformation.getTotalAvgDis()));//全市每天平均出行距离
		map.put("distanceRanking", distanceRanking);//当天出行距离排名
		
		map.put("count", count);//每天出行次数
		map.put("avgCounts", df.format(avgCounts));//全市每天所有人的平均出行次数
		map.put("perTrips", dft.format(perTrips));//每天出行次数排名百分比
		
		map.put("modeList", modeList);//出行方式饼图信息
		map.put("objectiveList", objectiveList);//出行目的饼图信息
		
		map.put("travelTimeCount", df.format(travelTimeCount/60.0));//个人当天出行总时长
		map.put("travelDistanceCount", df.format(travelDistanceCount));//个人当天出行总距离
		
		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	}
	
	/**
	 * @description APP 出行信息修改
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-06-22
	 */
	@RequestMapping(value="updateTravelInfo",method=RequestMethod.POST)
	public void updateTravelInfo(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		String tid = request.getParameter("id");//必传字段，不能为空
		String tappuserId = request.getParameter("appuserId");
		
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String startAddress = request.getParameter("startAddress");
		String endAddress = request.getParameter("endAddress");
		String startGps = request.getParameter("startGps");
		String endGps = request.getParameter("endGps");
		String tripMode = request.getParameter("tripMode");//出行方式
		String tripObjective = request.getParameter("tripObjective");//出行目的
		String dayDate = request.getParameter("dayDate");//获取 页面传过来的日期 
		String status = request.getParameter("status");
		String remark = request.getParameter("remark");
		//String distance = request.getParameter("distance");
		String tripTime = request.getParameter("tripTime");//出行时长
		//String isSendChildren = request.getParameter("isSendChildren");
		//String sendAdress = request.getParameter("sendAdress");
		//String sendDate = request.getParameter("sendDate");
		String cityCode = request.getParameter("cityCode");
		
		String startFormatAddress = request.getParameter("startFormatAddress");//开始地址，短地址
		String endFormatAddress = request.getParameter("endFormatAddress");//结束地址，短地址
		
		String ranking = request.getParameter("ranking");//当天第几条出行信息
		
		if(tid == null || tappuserId == null || startDate == null || endDate == null ||
				startAddress == null || endAddress == null || startGps == null || endGps == null || 
				tripMode == null ||tripObjective == null || dayDate == null || status == null ||
				tripTime == null || cityCode == null || startFormatAddress == null || endFormatAddress== null||
				ranking == null){
			map.put("errNo", "-2");
			map.put("result", "缺少参数");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		tid = tid.trim();
		tappuserId = tappuserId.trim();
		startDate = startDate.trim();
		endDate = endDate.trim();
		startAddress = startAddress.trim();
		endAddress = endAddress.trim();
		startGps = startGps.trim();
		endGps = endGps.trim();
		tripMode = tripMode.trim();
		tripObjective = tripObjective.trim();
		dayDate = dayDate.trim();
		status = status.trim();
		tripTime = tripTime.trim();
		cityCode = cityCode.trim();
		startFormatAddress = startFormatAddress.trim();
		startFormatAddress = startFormatAddress.trim();
		ranking = ranking.trim();
		if(tid.isEmpty() || tappuserId.isEmpty() || startDate.isEmpty() || endDate.isEmpty() ||
				startAddress.isEmpty() || endAddress.isEmpty() || startGps.isEmpty() || endGps.isEmpty() ||
				tripMode.isEmpty() || tripObjective.isEmpty() || dayDate.isEmpty() || status.isEmpty() || 
				tripTime.isEmpty() || cityCode.isEmpty() || ranking.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(tid.length()>11 || tappuserId.length()>11 || startAddress.length()>255 || endAddress.length()>255 || 
				startGps.length()>50 || endGps.length()>50 || tripMode.length()>200 ||tripObjective.length()>255 || 
				status.length()>11 ||tripTime.length()>11 || cityCode.length()>255 || startFormatAddress.length()>255 || 
				endFormatAddress.length()>255||ranking.length()>11){
			map.put("errNo", "-7");
			map.put("result", "参数长度超出范围");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		if(VerificationUtil.isNumeric(tid) || VerificationUtil.isNumeric(tappuserId) || VerificationUtil.isNumeric(ranking)
				|| VerificationUtil.isNumeric(status) || VerificationUtil.isNumeric(tripTime)){
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		
		startDate = dayDate+" "+startDate;//对时间进行处理
		endDate = dayDate+" "+endDate;//对时间进行处理
		if(VerificationUtil.isValidDateDay(dayDate) || VerificationUtil.isValidDateMinute(startDate) ||
				VerificationUtil.isValidDateMinute(endDate)){
			map.put("errNo", "-5");
	 		map.put("result", "日期类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		TravelInformation model= new TravelInformation(); 
		Integer id = Integer.parseInt(tid);//转成Integer类型
		model.setId(id);
		Integer appuserId = Integer.parseInt(tappuserId);//转成Integer类型
		model.setAppuserId(appuserId);
		
		model.setCurrentDate(sdfDate.parse(dayDate));
		model.setRemark(remark);
		model.setStatus(Integer.parseInt(status));
		
		if(sdf.parse(startDate).after(sdf.parse(endDate))){
			map.put("errNo", "-6");
	 		map.put("result", "开始时间必须小于结束时间");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
			
		model.setStartDate(sdf.parse(startDate));
		model.setEndDate(sdf.parse(endDate));
		
		model.setStartAddress(startAddress);
		model.setEndAddress(endAddress);
		model.setStartGps("("+startGps+")");
		model.setEndGps("("+endGps+")");
		model.setTripMode(tripMode);//出行方式
		model.setTripObjective(tripObjective);//出行目的
		/*if(!isSendChildren.isEmpty()){
			model.setIsSendChildren(Integer.parseInt(isSendChildren));//是否接送小孩
		}*/
		//model.setSendAdress(sendAdress);
		//sendDate = dayDate+" "+sendDate;//对时间进行处理
		/*if(!sendDate.isEmpty()){
			model.setSendDate(sdf.parse(sendDate));
		}*/
		model.setTravelTime(Integer.parseInt(tripTime));//出行时长
		/*if(!distance.isEmpty()){
			model.setDistance(Double.parseDouble(distance));//出行距离
		}*/
	   /*if(distance.equals("null")){
			model.setDistance(null);//出行距离
		}*/
	   model.setDistance(null);////距离默认为null，不让修改
	   model.setCityCode(cityCode);
		
		String key = getProperties().getProperty("key");
			if(startFormatAddress.equals("")){
				String[] starGpsA = startGps.split(",");
				String lngA = starGpsA[0];
				String latA = starGpsA[1];
				
				List<String> startAddressList = InverseGeocoding.addressPois(lngA,latA,key);
				
				if(startAddressList.size()>2){
					model.setStartFormatAddress(startAddressList.get(1));
				}
			}else{
				model.setStartFormatAddress(startFormatAddress);
			}
		
			if(endFormatAddress.equals("")){
				String[] endGpsB = endGps.split(",");
				String lngB = endGpsB[0];
				String latB = endGpsB[1];
				
				List<String> endAddressList = InverseGeocoding.addressPois(lngB,latB,key);
				
				if(endAddressList.size()>2){
					model.setEndFormatAddress(endAddressList.get(1));
				}
			}else{
				model.setEndFormatAddress(endFormatAddress);
			}
		
		model.setRanking(Integer.parseInt(ranking));
		
		int ret = service.updateByPrimaryKeySelective(model);//保存到数据库
		
		if(ret>0){
			map.put("errNo", "0");
			map.put("result", "成功!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}else{
			map.put("errNo", "-1");
			map.put("result", "修改失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}

	}
	
	/**
	 * @description APP 出行信息轨迹点
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-06-22
	 */
	@RequestMapping(value="queryTraGps",method=RequestMethod.POST)
	public void queryTraGps(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		
		List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
		
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String dayDate = request.getParameter("dayDate");//获取 页面传过来的日期
		
		if(id == null || dayDate == null){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		id = id.trim();
		dayDate = dayDate.trim();
		if(id.isEmpty() || dayDate.isEmpty()){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		if(id.length()>11){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-6");
	 		map.put("result", "参数长度超出范围");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		if(VerificationUtil.isNumeric(id)){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		if(VerificationUtil.isValidDateDay(dayDate)){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("errNo", "-5");
	 		map.put("result", "日期类型参数格式不正确");
	 		listResult.add(map);
	 		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 		return; 
		}
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		//String day = sdfDate.format(date);
		Date date = sdfDate.parse(dayDate);
		
		List<TravelInformation> list= service.selectTraGps(appuserId,date); 
		
		if(list.size()>0){
			for(int i = 0; i < list.size(); i++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("errNo", "0");
				map.put("ranking", list.get(i).getRanking());
				map.put("tripGps", list.get(i).getTripGps());
				
				listResult.add(map);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
	
	
	/**
	 * @description APP 推送
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-07-29
	 */
	@RequestMapping(value="queryPush",method=RequestMethod.POST)
	public void queryPush(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		String id = request.getParameter("appuserId");//必传字段，不能为空
		
		if(id == null){
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		id = id.trim();
		if(id.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return;
		}
		if(id.length()>11){
			map.put("errNo", "-5");
	 		map.put("result", "参数长度超出范围");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return;
		}
		if(VerificationUtil.isNumeric(id)){
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		Date date = new Date();
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		String day = sdfDate.format(date);
		Date dayDate = sdfDate.parse(day);
		
		List<TravelInformation> list= service.selectPush(appuserId,dayDate); 
		
		if(list.size()>0){
		    map.put("errNo", "0");
			map.put("result", "请填写出行信息");
		}else{
			map.put("errNo", "-1");
	 		map.put("result", "查询失败，请稍候再试");
		}
		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	}
	
	/**
	 * @description APP 出行信息添加(弃用)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-07-07
	 */
	@RequestMapping(value="insertTravelInfo",method=RequestMethod.POST)
	public void insertTravelInfo(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		//HttpServletRequest servletRequest = (HttpServletRequest) request;
		//HttpServletResponse servletResponse = (HttpServletResponse) response;
		//HttpSession session = servletRequest.getSession();
	
		String tappuserId = request.getParameter("appuserId").trim();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String startGps = request.getParameter("startGps");
		String endGps = request.getParameter("endGps");
		String tripGps = request.getParameter("tripGps");
		//String tripMode = request.getParameter("tripMode");//出行方式
		//String tripObjective = request.getParameter("tripObjective");//出行目的
		String dayDate = request.getParameter("dayDate");//获取 页面传过来的日期 
		//String remark = request.getParameter("remark");
		String distance = request.getParameter("distance");
		String tripTime = request.getParameter("tripTime");//出行时长
		String cityCode = request.getParameter("cityCode");
		
		String ranking = request.getParameter("ranking");//当天第几条出行信息
		
		TravelInformation model= new TravelInformation(); 
	
		if(!tappuserId.isEmpty()){
			Integer appuserId = Integer.parseInt(tappuserId);//转成Integer类型
			model.setAppuserId(appuserId);
		}
		if(!dayDate.isEmpty()){
			model.setCurrentDate(sdfDate.parse(dayDate));
		}
		//model.setRemark(remark);
		
		//startDate = dayDate+" "+startDate;//对时间进行处理
		if(!startDate.isEmpty()){
			model.setStartDate(sdf.parse(startDate));
		}
		//endDate = dayDate+" "+endDate;//对时间进行处理
		if(!endDate.isEmpty()){
			model.setEndDate(sdf.parse(endDate));
		}
		
		model.setStartGps("("+startGps+")");
		model.setEndGps("("+endGps+")");
		model.setTripGps(tripGps);
		//model.setTripMode(tripMode);//出行方式
		//model.setTripObjective(tripObjective);//出行目的
		
		if(!tripTime.isEmpty()){
			model.setTravelTime(Integer.parseInt(tripTime));//出行时长
		}
		if(!distance.isEmpty()){
			model.setDistance(Double.parseDouble(distance));//出行距离
		}
		model.setCityCode(cityCode);
		
		
		String key = getProperties().getProperty("key");
		String[] starGpsA = startGps.split(",");
		String lngA = starGpsA[0];
		String latA = starGpsA[1];
		
		List<String> startAddressList = InverseGeocoding.addressPois(lngA,latA,key);
		
		if(startAddressList.size()>0){
			model.setStartAddress(startAddressList.get(0));
		}
		if(startAddressList.size()>1){
			model.setStartFormatAddress(startAddressList.get(1));
		}
				
			
		String[] endGpsB = endGps.split(",");
		String lngB = endGpsB[0];
		String latB = endGpsB[1];
		
		List<String> endAddressList = InverseGeocoding.addressPois(lngB,latB,key);
		
		if(endAddressList.size()>0){
			model.setEndAddress(endAddressList.get(0));
		}
		if(endAddressList.size()>1){
			model.setEndFormatAddress(endAddressList.get(1));
		}
				
		model.setStatus(0);	
		
		model.setRanking(Integer.parseInt(ranking));
		//当第一条出行信息进来时，先清空当天出行信息
		if("1".equals(ranking)){
			service.deleteByAppuserId(Integer.parseInt(tappuserId),sdfDate.parse(dayDate));
		}
		
		int ret = service.insertSelective(model);//保存到数据库
		
		//查询当天出行信息状态为1的是否修改过
		List<TravelInformation> listRank = service.selectByRanking(Integer.parseInt(tappuserId),sdfDate.parse(dayDate),Integer.parseInt(ranking));
		if(listRank.size()<1){
			service.deleteByRank(Integer.parseInt(tappuserId),sdfDate.parse(dayDate),Integer.parseInt(ranking));
			model.setStatus(1);//分析出数据，状态为1
			service.insertSelective(model);//保存到数据库
		}
		
		if(ret>0){
			map.put("errNo", "0");
			map.put("result", "成功!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}else{
			map.put("errNo", "-1");
			map.put("result", "保存失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}

	}
	//
	
	
	
}		

	

