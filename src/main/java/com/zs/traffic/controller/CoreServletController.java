package com.zs.traffic.controller;

import com.zs.traffic.model.CityCodeUrl;
import com.zs.traffic.model.StatisticsParticular;
import com.zs.traffic.service.CityCodeUrlService;
import com.zs.traffic.service.StatisticsParticularService;
import com.zs.traffic.service.WechartInfoService;
import com.zs.traffic.wx.model.event.SNSUserInfo;
import com.zs.traffic.wx.model.event.WeixinOauth2Token;
import com.zs.traffic.wx.model.resp.Article;
import com.zs.traffic.wx.model.resp.NewsMessage;
import com.zs.traffic.wx.model.resp.TextMessage;
import com.zs.utils.AdvancedUtil;
import com.zs.utils.MessageUtil;
import com.zs.utils.SignUtil;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"/"})
public class CoreServletController
  extends BaseController
{
  private static final long serialVersionUID = 4440739483644821986L;
  @Autowired
  public WechartInfoService wechartInfoService;
  @Autowired
  public StatisticsParticularService statisticsParticularService;
  @Autowired
  public CityCodeUrlService cityCodeUrlService;
  public static final String appID = getProperties().getProperty("appID");
  public static final String appScret = getProperties().getProperty("appScret");
  public static final String ctiyCodes = getProperties().getProperty("ctiyCodes");
  
  private final static Logger logger = LoggerFactory.getLogger(AppUserController.class);
  
  @RequestMapping({"/getXml"})
  public void getXml(HttpServletRequest request, HttpServletResponse response)
    throws IOException{
    String signature = request.getParameter("signature");
    
    String timestamp = request.getParameter("timestamp");
    
    String nonce = request.getParameter("nonce");
    
    String echostr = request.getParameter("echostr");
    System.out.println("输出验证接口码"+echostr);
    if ((echostr != null) && (echostr != "")){
      PrintWriter out = response.getWriter();
      if (SignUtil.checkSignature(signature, timestamp, nonce)) {
        out.print(echostr);
      }
      out.close();
      out = null;
    }else{
      request.setCharacterEncoding("UTF-8");
      response.setCharacterEncoding("UTF-8");
      
      String respXml = null;
      try{
        Map<String, String> requestMap = MessageUtil.parseXml(request);
        
        String qrscene_code = (String)requestMap.get("EventKey");
        logger.debug("输出二维码参数"+qrscene_code);
        String titleName = "";
        String ctiyCode = "";
        String isSurveyor = "";
        if ((qrscene_code != null) && (!"".equals(qrscene_code))){
          String codeLong = qrscene_code.replace("qrscene_", "");
          if (codeLong.indexOf(";") != -1){
            String[] codes = codeLong.split(";");
            for (int i = 0; i < codes.length; i++){
              ctiyCode = codes[0];
              titleName = codes[1];
              isSurveyor = codes[2];
            }

          }
        }
        
        if(titleName!=null&&titleName!=""&&!"".equals(titleName)){       	
        	 String bj= titleName.substring(0,4);
        	 if("3704".equals(bj)){
             	ctiyCode = "0632";
             }
             if("3713".equals(bj)){
             	ctiyCode = "0539";
             }
        }
       
       
        
        String fromUserName = (String)requestMap.get("FromUserName");
  	    System.out.println("该用户的openId为："+fromUserName);

        String toUserName = (String)requestMap.get("ToUserName");
        
        String msgType = (String)requestMap.get("MsgType");
        System.out.println("加载......");
        TextMessage textMessage = new TextMessage();
        textMessage.setToUserName(fromUserName);
        textMessage.setFromUserName(toUserName);
        textMessage.setCreateTime(new Date().getTime());
        textMessage.setMsgType("text");
        if (msgType.equals("event")){
          String eventType = (String)requestMap.get("Event");
          if (eventType.equals("subscribe")){
        	  
        	  String cityWord = "";
        	  String cityUrl = "";
        	  CityCodeUrl cityCodeUrl = cityCodeUrlService.selectByCityCode(ctiyCode,"0");
        	  if(cityCodeUrl!=null){
        		  cityWord = cityCodeUrl.getExtendedFieldTwo();
        		  cityUrl = cityCodeUrl.getCityUrl();
        		  logger.debug("输出查询到的文字"+cityWord);
        	  }
        	  
              if ((qrscene_code == null) || ("".equals(qrscene_code))){
            	textMessage.setContent("您好，欢迎关注“交通视野”，请您扫描指定二维码关注后点击菜单栏“问卷填写”填写问卷。此次关注无法进行问卷填写。");            
            	}else{
            	//查询数据库是否存在此openId
    			List<StatisticsParticular> stp = statisticsParticularService.selectBySubmitterForList(fromUserName);
              if (stp.size()==0){
                StatisticsParticular statisticsParticular = new StatisticsParticular();
                statisticsParticular.setSubmitter(fromUserName);
                
                statisticsParticular.setTitleName(titleName);
                statisticsParticular.setIsSurveyor(isSurveyor);
                statisticsParticular.setCtiyCode(ctiyCode);
                statisticsParticular.setSubmitStage(Integer.valueOf(0));
                
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String sj = df.format(new Date());
                statisticsParticular.setFollowDate(sj);
                this.statisticsParticularService.insertSelective(statisticsParticular);
                System.out.println("第一次关注用户保存唯一标识openId");
                //动态推送消息
                textMessage.setContent(cityWord); 
                }else{
            	  StatisticsParticular spis = statisticsParticularService.selectBySubmitterIs(fromUserName);
            	  if(spis!=null){
            		  if(titleName=="public"||"public".equals(titleName)||"0".equals(isSurveyor)){
                          textMessage.setContent("您好，欢迎关注“交通视野”，您是该公众的调查员，无法填写公众问卷或普通问卷。身份信息无法修改，您仍是社区调查员身份。"); 

            		  }else{
            		 //将管理员修改为普通
            		  //spis.setTitleName(titleName);
            		  spis.setSubmitStage(7);
            		  spis.setIsSurveyor("99");
            		  statisticsParticularService.updateByPrimaryKeySelective(spis);
            		  //将未完成问卷强制改变
            		  StatisticsParticular statis = statisticsParticularService.selectBySubmitter(fromUserName);
                      if(statis!=null){
                    	  statis.setSubmitStage(7);
                    	  statisticsParticularService.updateByPrimaryKeySelective(statis);
                      }
            		  
            		  //新增管理员信息
            		  StatisticsParticular statisticsParticular = new StatisticsParticular();
                      statisticsParticular.setSubmitter(fromUserName);
                      
                      statisticsParticular.setTitleName(titleName);
                      statisticsParticular.setIsSurveyor("1");
                      statisticsParticular.setCtiyCode(ctiyCode);
                      statisticsParticular.setSubmitStage(Integer.valueOf(0));
                      
                      SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                      String sj = df.format(new Date());
                      statisticsParticular.setFollowDate(sj);
                      this.statisticsParticularService.insertSelective(statisticsParticular);
                      textMessage.setContent(cityWord); 
            		  }
            		  }else{
            			  //如果是普通用户扫描调查员二维码
            		/*	  if(isSurveyor=="1"||"1".equals(isSurveyor)){
            				  for(int i=0;i<stp.size();i++){
            					  stp.get(i).setSubmitStage(7);
            					  stp.get(i).setIsSurveyor("99");
            				  }
            				  //新增调查员信息
            				  StatisticsParticular statisticsParticular = new StatisticsParticular();
                              statisticsParticular.setSubmitter(fromUserName);
                              
                              statisticsParticular.setTitleName(titleName);
                              statisticsParticular.setIsSurveyor("1");
                              statisticsParticular.setCtiyCode(ctiyCode);
                              statisticsParticular.setSubmitStage(Integer.valueOf(0));
                              
                              SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                              String sj = df.format(new Date());
                              statisticsParticular.setFollowDate(sj);
                              this.statisticsParticularService.insertSelective(statisticsParticular);
            				  textMessage.setContent("您好，欢迎关注“交通视野”，您已经从普通用户升级为调查员。可以点击菜单栏“问卷填写”填写问卷。"); 

            			  }else{*/
                              textMessage.setContent("您好，欢迎关注“交通视野”，您已经关注过该公众号，如果是二次填写问卷则视为无效。若问卷还处于未完成状态，可以点击菜单栏“问卷填写”填写问卷。"); 

            			  //}   
            	  }
            	  System.out.println("该用户关注过此公众号，存在openId唯一标识！");
              }
            }
            respXml = MessageUtil.messageToXml(textMessage);
          }
          else if (!eventType.equals("unsubscribe")){
            if (eventType.equals("CLICK")){
              String eventKey = (String)requestMap.get("EventKey");
              if (eventKey.equals("oschina")){
                Article article = new Article();
                article.setTitle("zushikeji");
                article.setDescription("?????");
                article.setPicUrl("");
                article.setUrl("http://m.oschina.net");
                List<Article> articleList = new ArrayList();
                articleList.add(article);
                
                NewsMessage newsMessage = new NewsMessage();
                newsMessage.setToUserName(fromUserName);
                newsMessage.setFromUserName(toUserName);
                newsMessage.setCreateTime(new Date().getTime());
                newsMessage.setMsgType("news");
                newsMessage.setArticleCount(articleList.size());
                newsMessage.setArticles(articleList);
                respXml = MessageUtil.messageToXml(newsMessage);
              }
              else if (eventKey.equals("iteye")){
                textMessage.setContent("iteye");
                respXml = MessageUtil.messageToXml(textMessage);
              }
            }else{
            	textMessage.setContent("欢迎回来!");
            	respXml = MessageUtil.messageToXml(textMessage);
            }
            
          }
        }else{
          textMessage.setContent("感谢您关注“交通视野”，点击下方菜单进入填写问卷。");
          respXml = MessageUtil.messageToXml(textMessage);
        }
      }
      catch (Exception e){
        e.printStackTrace();
      }
      System.out.println("处理微信服务器消息成功！");
      
      PrintWriter out = response.getWriter();
      out.print(respXml);
      out.close();
    }
  }
  
  @RequestMapping({"/getOpenId"})
  public void getOpenId(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
  {
    request.setCharacterEncoding("gb2312");
    response.setCharacterEncoding("gb2312");
    
    String code = request.getParameter("code");
    System.out.print("获取" + code);
    
    String openId = "";
    if (code == null)
    {
      RequestDispatcher rd = request.getRequestDispatcher("/wx/false403.jsp");
      rd.forward(request, response);
    }
    if (!"authdeny".equals(code))
    {
      WeixinOauth2Token weixinOauth2Token = AdvancedUtil.getOauth2AccessToken(appID, appScret, code);
      
      String accessToken = weixinOauth2Token.getAccessToken();
      if (accessToken == null)
      {
        RequestDispatcher rd = request.getRequestDispatcher("/wx/false403.jsp");
        rd.forward(request, response);
      }
      openId = weixinOauth2Token.getOpenId();
      
      SNSUserInfo snsUserInfo = AdvancedUtil.getSNSUserInfo(accessToken, openId);
      
      HttpSession session = request.getSession();
      
      session.setAttribute("openId", openId);
      System.out.println("openId保存到session中");
      System.out.println("获取城市" + ctiyCodes);
      
      request.setAttribute("snsUserInfo", snsUserInfo);
    }
    request.getRequestDispatcher("/index.jsp?openId=" + openId + "&ctiyCodes=" + ctiyCodes).forward(request, response);
  }
}
