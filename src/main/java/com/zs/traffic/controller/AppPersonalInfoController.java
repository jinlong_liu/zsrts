package com.zs.traffic.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.traffic.model.AppFamilyInformation;
import com.zs.traffic.model.SysAppuser;
import com.zs.traffic.service.AppFamilyInformationService;
import com.zs.traffic.service.SysAppuserService;
import com.zs.utils.TxtUtil;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;

@Controller
@RequestMapping("/app")
public class AppPersonalInfoController extends BaseController{
	
	@Autowired
	public AppFamilyInformationService service;
	@Autowired
	public SysAppuserService sysAppuserService;
	
	/**
	 * @description App端个人信息提交  (版本更新后弃用)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-03-31
	 */
	@RequestMapping(value="/insertPersonalInfo",method=RequestMethod.POST)
	public void insertPersonalInfo(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception{
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		//Integer appuserId =  (Integer) session.getAttribute("appuserId");
		//String userName =  (String) session.getAttribute("userName");//账户电话号码,保存文件是文件名用
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String userName = request.getParameter("userName");//必传字段，不能为空
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		
		String cityCode = request.getParameter("cityCode");//必传字段，不能为空
		
		//String familyInfos = request.getParameter("familyInfo").trim();
		String stag = request.getParameter("stag").trim();
		
		/*String[] family = familyInfos.split(",");
		int ret = 0;
		for(String familyInfo : family){
			AppFamilyInformation model = new AppFamilyInformation();//对应的实体
			String[] familyInfomation = familyInfo.split(" ");
			//System.out.println(familyInfomation);
			String sex = familyInfomation[0];
			String age = familyInfomation[1];
			String isPermanentAddress  = familyInfomation[2];
			String profession = familyInfomation[3];
			String income = familyInfomation[4];
			String icCard = familyInfomation[5];
			String phone = familyInfomation[6];
			String companyAddress = familyInfomation[7];
			model.setSex(Integer.parseInt(sex));
			model.setAge(Integer.parseInt(age));
			model.setIsPermanentAddress(Integer.parseInt(isPermanentAddress));
			model.setProfession(Integer.parseInt(profession));
			model.setIncome(income);
			model.setIcCard(icCard);
			model.setPhone(phone);
			model.setCompanyAddress(companyAddress);
			
			model.setAppuserId(appuserId);
			service.insertSelective(model);
			ret++;
		}
*/
		String sex = request.getParameter("sex").trim();
		String age = request.getParameter("age").trim();
		String isPermanentAddress = request.getParameter("isPermanentAddress").trim();//是否常住
		String income = request.getParameter("income").trim();
		String profession = request.getParameter("profession").trim();
		//String icCard = request.getParameter("icCard").trim();
		String companyAddress = request.getParameter("companyAddress").trim();
		
		AppFamilyInformation model = new AppFamilyInformation();//对应的实体
		
		if(!sex.isEmpty()){
			Integer se = Integer.parseInt(sex);
			model.setSex(se);
		}
		if(!age.isEmpty()){
			Integer ag = Integer.parseInt(age);
			model.setAge(ag);
		}
		if(!isPermanentAddress.isEmpty()){
			Integer isPermanentAddres = Integer.parseInt(isPermanentAddress);
			model.setIsPermanentAddress(isPermanentAddres);
		}
		if(!income.isEmpty()){
			model.setIncome(income);
		}
		if(!profession.isEmpty()){
			Integer professio = Integer.parseInt(profession);
			model.setProfession(professio);
		}
		/*if(!icCard.isEmpty()){
			model.setIcCard(icCard);
		}*/
		if(!companyAddress.isEmpty()){
			model.setCompanyAddress(companyAddress);
		}
		
		model.setAppuserId(appuserId);
		int ret = service.insertSelective(model);
		if(ret>0){
			SysAppuser appUser = new SysAppuser();//对应的实体类
         	if(!stag.isEmpty()){
     			Integer sta = Integer.parseInt(stag);//显示提交步骤
     			appUser.setStag(sta);
     		}
         	appUser.setAppuserId(appuserId);
         	sysAppuserService.updateByPrimaryKeySelective(appUser);//记录提交步骤
			//AppFamilyInformation appFamilyInformation = service.selectByAppuserId(appuserId);
	 	    //map.put("appFamilyInformation", familyInfos);
         	map.put("appuserId", appuserId);
	 	    map.put("sex", sex);//登录用户id
			map.put("age", age);
			map.put("isPermanentAddress", isPermanentAddress);
			map.put("profession", profession);
			map.put("income", income);
			//map.put("icCard", icCard);
			//map.put("phone", list.get(i).getPhone());
			map.put("companyAddress", companyAddress);
	 		String filePathAndName = getProperties().getProperty("filePathDetail")+"Base/"+cityCode+"/"+userName+"base.txt";
	 		String fileContent = ZsJsonUtil.map2Json(map).toString();
	 		//map.remove("appFamilyInformation");
	 		map.clear();
	 		TxtUtil.newFile(filePathAndName,fileContent);
	        map.put("errNo", "0");
	    	map.put("result", "保存成功！");
	    	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	    	return;
	    }else{
	        map.put("errNo", "-1");
	    	map.put("result", "保存失败，请稍候再试");
	    	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	    	return;
	    }

	}
	
	
	/**
	 * @description App端个人信息修改  (版本更新后弃用)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-03-31
	 */
	@RequestMapping(value="/updatePersonalInfo",method=RequestMethod.POST)
	public void updatePersonalInfo(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception{
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		//Integer appuserId =  (Integer) session.getAttribute("appuserId");
		//String userName =  (String) session.getAttribute("userName");//账户电话号码,保存文件是文件名用
		String id = request.getParameter("appuserId");//必传字段，不能为空
		String userName = request.getParameter("userName");//必传字段，不能为空
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		
		String cityCode = request.getParameter("cityCode");//必传字段，不能为空
		
		/*String familyInfos = request.getParameter("familyInfo").trim();
		
		int retu = service.deleteByAppUserId(appuserId);//修改前先删除
		if(retu<=0){
			map.put("errNo", "-1");
    		map.put("result", "服务器异常");
    		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
    		return;
		}
		
		String[] family = familyInfos.split(",");
		int ret = 0;
		for(String familyInfo : family){
			AppFamilyInformation model = new AppFamilyInformation();//对应的实体
			String[] familyInfomation = familyInfo.split(" ");
			//System.out.println(familyInfomation);
			String sex = familyInfomation[0];
			String age = familyInfomation[1];
			String isPermanentAddress  = familyInfomation[2];
			String profession = familyInfomation[3];
			String income = familyInfomation[4];
			String icCard = familyInfomation[5];
			String phone = familyInfomation[6];
			String companyAddress = familyInfomation[7];
			//String familyId = familyInfomation[8];
			model.setSex(Integer.parseInt(sex));
			model.setAge(Integer.parseInt(age));
			model.setIsPermanentAddress(Integer.parseInt(isPermanentAddress));
			model.setProfession(Integer.parseInt(profession));
			model.setIncome(income);
			model.setIcCard(icCard);
			model.setPhone(phone);
			model.setCompanyAddress(companyAddress);
			
			model.setAppuserId(appuserId);
			//model.setFamilyId(Integer.parseInt(familyId));
			service.insertSelective(model);//在保存
			ret++;
		}*/
		
		String familyId = request.getParameter("familyId").trim();//必传id
		String sex = request.getParameter("sex").trim();
		String age = request.getParameter("age").trim();
		String isPermanentAddress = request.getParameter("isPermanentAddress").trim();//是否常住
		String income = request.getParameter("income").trim();
		String profession = request.getParameter("profession").trim();
		//String icCard = request.getParameter("icCard").trim();
		String companyAddress = request.getParameter("companyAddress").trim();
		
		AppFamilyInformation model = new AppFamilyInformation();//对应的实体
		
		model.setFamilyId(Integer.parseInt(familyId));
		if(!sex.isEmpty()){
			Integer se = Integer.parseInt(sex);
			model.setSex(se);
		}
		if(!age.isEmpty()){
			Integer ag = Integer.parseInt(age);
			model.setAge(ag);
		}
		if(!isPermanentAddress.isEmpty()){
			Integer isPermanentAddres = Integer.parseInt(isPermanentAddress);
			model.setIsPermanentAddress(isPermanentAddres);
		}
		if(!income.isEmpty()){
			model.setIncome(income);
		}
		if(!profession.isEmpty()){
			Integer professio = Integer.parseInt(profession);
			model.setProfession(professio);
		}
		/*if(!icCard.isEmpty()){
			model.setIcCard(icCard);
		}*/
		if(!companyAddress.isEmpty()){
			model.setCompanyAddress(companyAddress);
		}
		//AppFamilyInformation appFamilyInformation = service.selectByAppuserId(appuserId);
		//model.setAppuserId(appuserId);
		int ret = service.updateByPrimaryKeySelective(model);
		if(ret>0){
	 	    //map.put("appFamilyInformation", familyInfos);
			map.put("appuserId", appuserId);
			map.put("sex", sex);
			map.put("age", age);
			map.put("isPermanentAddress", isPermanentAddress);
			map.put("profession", profession);
			map.put("income", income);
			//map.put("icCard", icCard);
			//map.put("phone", list.get(i).getPhone());
			map.put("companyAddress", companyAddress);
	 		String filePathAndName = getProperties().getProperty("filePathDetail")+"Base/"+cityCode+"/"+userName+"base.txt";
	 		String fileContent = ZsJsonUtil.map2Json(map).toString();
	 		//map.remove("appFamilyInformation");
	 		map.clear();
	 		TxtUtil.newFile(filePathAndName,fileContent);
	        map.put("errNo", "0");
	    	map.put("result", "修改成功！");
	    	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	    	return;
	    }else{
	        map.put("errNo", "-1");
	    	map.put("result", "修改失败，请稍候再试");
	    	response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	    	return;
	    }
	}
	
	/**
	 * @description  个人信息查询  (版本更新后弃用)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author 		wangzf
	 * @date 		2017-03-31
	 */
	@RequestMapping(value="/queryPersonalInfo",method=RequestMethod.POST)
	public void queryPersonalInfo(XssHttpServletRequestWrapper request,HttpServletResponse response) throws Exception{
		request.setCharacterEncoding("UTF-8");
		List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
		
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		//Integer appuserId =  (Integer) session.getAttribute("appuserId");
		String id = request.getParameter("appuserId");//必传字段，不能为空
		//String userName = request.getParameter("userName");//必传字段，不能为空
		Integer appuserId = Integer.parseInt(id);//转成Integer类型
		
		List<AppFamilyInformation> list = service.selectByAppuserId(appuserId);
		//AppFamilyInformation appFamilyInformation = service.selectByAppuserId(appuserId);
		if(list.size() > 0){
			for(int i = 0; i < list.size(); i++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("familyId", list.get(i).getFamilyId());
        		map.put("sex", list.get(i).getSex());
    			map.put("age", list.get(i).getAge());
    			map.put("isPermanentAddress", list.get(i).getIsPermanentAddress());
    			map.put("profession", list.get(i).getProfession());
    			map.put("income", list.get(i).getIncome());
    			//map.put("icCard", list.get(i).getIcCard());
    			//map.put("phone", list.get(i).getPhone());
    			map.put("companyAddress", list.get(i).getCompanyAddress());
    			listResult.add(map);
			}
        }
		 response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
	
	

}
