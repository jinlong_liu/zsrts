package com.zs.traffic.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zs.traffic.model.RoleGrade;
import com.zs.traffic.model.SysRole;
import com.zs.traffic.service.RoleGradeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;


import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.SourceVersion;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zs.traffic.model.*;
import com.zs.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/")
public class RoleGradeController extends BaseController {

    @Autowired
    public RoleGradeService roleService;

    /**
     * 查询用户信息
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value="/getGradeRoleList", method=RequestMethod.POST)
    public void getGradeUserList(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        System.out.println("##################################################################################"+request);
        List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();

        HttpServletRequest servletRequest = (HttpServletRequest) request;

        HttpServletResponse servletResponse = (HttpServletResponse) response;

//        HttpSession session = servletRequest.getSession();
//
//        Integer userId = (Integer) session.getAttribute("userId");//获取用户id

//        List<RoleGrade> list = roleService.getGradeRoleList();

        int count = 0;
        int deptId = -1;
//        if (list!=null && list.size()>0) {
//            count = list.size();
//            for (int i = 0; i < list.size(); i++) {
//
//                Map<String,Object>  map=new HashMap<String,Object>();
//                RoleGrade entity = list.get(i);
//
//                map.put("i", i+1);
//                map.put("studentnumber", entity.getSnumber());//序号
//                map.put("roleName", entity.getRolegradeName());
//                map.put("chinese", entity.getChinese());
//                map.put("math", entity.getMath());
//                map.put("english", entity.getEnglish());
//                map.put("dele","删除");
//                map.put("revise","修改");
//                listResult.add(map);
//            }


        Map<String, Object> map = new HashMap<String, Object>();
//                RoleGrade entity = list.get(1);
        map.put("number", "205100");//序号
        map.put("roleName", "60");
        map.put("chinese", "20");
        map.put("math", "50");
        map.put("english", "70");
        listResult.add(map);

        response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));

    }


    /**
     * 添加角色
     * @param request
     * @param response
     * @throws Exception
     */
//    @RequestMapping(value="/addRole", method=RequestMethod.POST)
//    public void addRole(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
//        request.setCharacterEncoding("UTF-8");
//        List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
//
//        HttpServletRequest servletRequest = (HttpServletRequest) request;
//        HttpServletResponse servletResponse = (HttpServletResponse) response;
//        HttpSession session = servletRequest.getSession();
//        Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
//        String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限
//        String operator =  (String) session.getAttribute("userName");//获取当前登录胡名
//        String[] array = authDegrees.split(",");
//
//        Boolean flag = true;
//        for(String auth:array){
//            if(auth.equals(ZsAuthDegree.ROLE_AUTH)){
//                flag = false;
//                break;
//            }
//        }
//
//        //String  authDegree = array[array.length-1];//判断当前登录用户是否是超级管理员
//        if(flag){//没有角色管理权限返回
//            return;
//        }
//
//        String roleName = request.getParameter("roleName").trim();//角色名称
//        String remark = request.getParameter("remark");//备注
//        String describeActor=request.getParameter("describeActor");//  角色描述
//        //String operator = request.getParameter("operator");//创建者
//        // 权限关联表
//        String resvice = request.getParameter("resvice");//权限
//        String peo = request.getParameter("peo");//是否给予人员管理权限
//        String actor = request.getParameter("actor");//是否给予角色管理
//
//        String area = request.getParameter("area");//是否给予区域管理
//        String param = request.getParameter("param");//是否给予参数管理
//
//        String examine = request.getParameter("examine");//是否给予参数管理
//        System.out.println("=================================");
//        System.out.println("peo = " + peo);
//        System.out.println("actor = " + actor);
//        System.out.println("area = " + area);
//        System.out.println("param = " + param);
//        System.out.println("examine = " + examine);
//        System.out.println("================================");
//        System.out.println(roleName);
//        System.out.println(remark);
//        System.out.println(describeActor);
//        if(ZsAuthDegree.INVESTIGATOR_AUTH.equals(resvice)){// 7
//            peo="0";
//            actor="0";
//            area="0";
//            param="0";
//            examine="0";
//        }
//        if(ZsAuthDegree.SECOND_AUTH.equals(resvice)||ZsAuthDegree.THIRD_AUTH.equals(resvice)){//5,6
//            actor="0";
//            area="0";
//            param="0";
//        }
//        if(ZsAuthDegree.FIRST_AUTH.equals(resvice)){//4
//            actor="0";
//            area="0";
//        }
//
////    	if(remark.isEmpty()){
////    		remark="无备注";
////    	}
////    	if(describe.isEmpty()){
////    		describe="无角色描述";
////    	}
//        // 先插入角色
//        //获取当前时间
//        Date date=new Date();
//        DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String createDate=format.format(date);
//        // 先查询角色名是否存在
//        int count=roleService.getIsRoleName(roleName);
//        System.out.println("=========================================");
//        System.out.println(count);
//        System.out.println("=========================================");
//        if (count>=1) {
//            Map<String,Object>  map=new HashMap<String,Object>();
//            map.put("flag", "2");
//            listResult.add(map);
//            response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
//            return;
//        }
//        // 插入数据
//        SysRole sysRole=new SysRole();
//        sysRole.setRoleName(roleName);
//
//
////    	sysRole.setRemark(remark);
////    	sysRole.setCreateDate(createDate);
////    	sysRole.setDescribeActor(describeActor);
////
////    	sysRole.setOperator(operator);
//
//        int k=roleService.insert(sysRole);
//
//        // 插入关联表
//        // 根据角色名查id
//        List<SysRole> list=roleService.getRoleListByName(roleName);
//
//
//
////    	int sysRoleId=list.get(0).getRoleId();
//
//
//        if (ZsAuthDegree.CITY_AUTH.equals(resvice)||ZsAuthDegree.FIRST_AUTH.equals(resvice)
//                ||ZsAuthDegree.SECOND_AUTH.equals(resvice)||ZsAuthDegree.THIRD_AUTH.equals(resvice)
//                ||ZsAuthDegree.INVESTIGATOR_AUTH.equals(resvice)){
//            SysAuthorization sysAuthorization = roleService.getAuthByDegree(Integer.parseInt(resvice));
//
//            int sysAuthId=sysAuthorization.getAuthId();
//
//            SysRoleAuth sysRoleAuth=new SysRoleAuth();
//            //int sysAuthId=Integer.parseInt(resvice);
//
//
////        	sysRoleAuth.setSysRoleId(sysRoleId);
//
//
//
//            sysRoleAuth.setSysAuthId(sysAuthId);
//            int resviceReturn=roleService.insertAuthRole(sysRoleAuth);
//        }
//        Map<String,Object>  map=new HashMap<String,Object>();
//        if (k>0) {
//            map.put("flag", "1");
//            listResult.add(map);
//        }else {
//            map.put("flag", "0");
//            listResult.add(map);
//        }
//        response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
//
//    }


//    /**
//     * 删除角色
//     * @param request
//     * @param response
//     * @throws Exception
//     */
//    @RequestMapping(value="/deleteGradeRole", method=RequestMethod.POST)
//    public void deleteRole(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
//        request.setCharacterEncoding("UTF-8");
//        System.out.println("sjkdfhskadhfjksdahfjksdahfsjdahfsfgdgdaghfjksda");
//        String id = request.getParameter("id");//id
//        List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
//        Map<String,Object>  map=new HashMap<String,Object>();
//
//        HttpServletRequest servletRequest = (HttpServletRequest) request;
//        HttpServletResponse servletResponse = (HttpServletResponse) response;
//        HttpSession session = servletRequest.getSession();
//        Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
////		String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限
////		String[] array = authDegrees.split(",");
////		Boolean flags = true;
////		for(String auth:array){
////			if(auth.equals(ZsAuthDegree.ROLE_AUTH)){
////				 flags = false;
////				 break;
////			}
////		}
////		//String  authDegree = array[array.length-1];//判断当前登录用户是否是超级管理员
////		if(flags){//没有角色管理权限返回
////			return;
////		}
//
////        Integer flag=0;
////        int rolUser=roleService.selectGradeRoleUser(Integer.parseInt(id));
////        if (rolUser>0) {
////            map.put("flag", "0");
////            flag=0;
////            listResult.add(map);
////        }else {
////            // 先删除角色权限关联表
//////			int rolAuto=roleService.deleteRoleAuto(Integer.parseInt(id));
////            // 删除角色
////            int rol=roleService.deleteGradeRole(Integer.parseInt(id));
////            map.put("flag", "1");
////            flag=1;
////            listResult.add(map);
////        }
//
//        response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
//    }




    /**
     * 修改时先查询角色信息
     * @param request
     * @param response
     * @throws Exception
     */
//    @RequestMapping(value="/selectAndReviceRole", method=RequestMethod.POST)
//    public void selectAndReviceRole(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
//        request.setCharacterEncoding("UTF-8");
//        String id = request.getParameter("id");//id
//        //System.out.println("fhsdjhfsuidhfsdhfjlskdhgfjklsfdhgjklsfdhgljkgdfgfd         "+id);
//        List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
//        Map<String,Object>  map=new HashMap<String,Object>();
//
//        HttpServletRequest servletRequest = (HttpServletRequest) request;
//        HttpServletResponse servletResponse = (HttpServletResponse) response;
//        HttpSession session = servletRequest.getSession();
//        Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
//        String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限
//        String[] array = authDegrees.split(",");
//        Boolean flag = true;
//        for(String auth:array){
//            if(auth.equals(ZsAuthDegree.ROLE_AUTH)){
//                flag = false;
//                break;
//            }
//        }
//
//        if(flag){//没有角色管理权限返回
//            return;
//        }
//
//        //查询角色名
//        List<SysRole> list=roleService.getRoleListByid(Integer.parseInt(id));
//
//
//
////    	map.put("id", list.get(0).getRoleId());
////    	map.put("roleName", list.get(0).getRoleName());
////    	map.put("remark", list.get(0).getRemark());
////    	map.put("describeActor", list.get(0).getDescribeActor());
//
//
//
//
//
//        // 查询权限  根据  角色id 关联表 查询出权限
//        List<SysAuthorization> listAuth=roleService.getAuthListByid(Integer.parseInt(id));
//        // 查询关联表id
//        List<SysRoleAuth> sysRoleAuth=roleService.getSysRoleAuthByid(Integer.parseInt(id));
//
//        map.put("actor","无");
//        map.put("peopol", "无");
//        map.put("area", "无");
//        map.put("param", "无");
//        map.put("examine", "无");
//        for(int i=0 ;i<listAuth.size();i++){
//            if(listAuth.get(i).getauthDegree()==1){
//                map.remove("actor");
//                map.put("peopol",  listAuth.get(i).getauthDegree());
//                map.put("peopolId", sysRoleAuth.get(0).getId());
//            }else if(listAuth.get(i).getauthDegree()==2){
//                map.remove("actor");
//                map.put("actor",  listAuth.get(i).getauthDegree());
//                map.put("actorId", sysRoleAuth.get(0).getId());
//            }else if(listAuth.get(i).getauthDegree()==8){
//                map.remove("area");
//                map.put("area",  listAuth.get(i).getauthDegree());
//                map.put("areaId", sysRoleAuth.get(0).getId());
//            }else if(listAuth.get(i).getauthDegree()==9){
//                map.remove("param");
//                map.put("param",  listAuth.get(i).getauthDegree());
//                map.put("paramId", sysRoleAuth.get(0).getId());
//            }else if(listAuth.get(i).getauthDegree()==10){
//                map.remove("examine");
//                map.put("examine",  listAuth.get(i).getauthDegree());
//                map.put("examineId", sysRoleAuth.get(0).getId());
//            }else{
//                map.put("auth",  listAuth.get(i).getauthDegree());
//                map.put("authId", sysRoleAuth.get(0).getId());
//            }
//        }
//
//    	/*if(listAuth.size()==1){
//    		map.put("auth", listAuth.get(0).getAuthId());
//    		map.put("authId", sysRoleAuth.get(0).getId());
//    		map.put("actor","无");
//    		map.put("peopol", "无");
//
//    		map.put("area", "无");
//    		map.put("param", "无");
//
//    	}else if(listAuth.size()==2){
//    		for(int i=0 ;i<listAuth.size();i++){
//    			int a=listAuth.get(i).getAuthId();
//    			if(listAuth.get(i).getAuthId()==1){
//    				map.put("peopol",  listAuth.get(i).getAuthId());
//    				map.put("peopolId", sysRoleAuth.get(0).getId());
//    				map.put("actor","无");
//    			}else
//    			if(listAuth.get(i).getAuthId()==2){
//    				map.put("actor",  listAuth.get(i).getAuthId());
//    				map.put("actorId", sysRoleAuth.get(0).getId());
//    				map.put("peopol", "无");
//    			}else{
//    				map.put("auth",  listAuth.get(i).getAuthId());
//    				map.put("authId", sysRoleAuth.get(0).getId());
//    			}
//    		}
//    	}else if(listAuth.size()>2){
//    		for(int i=0 ;i<listAuth.size();i++){
//    			if(listAuth.get(i).getAuthId()==1){
//    				map.put("peopol",  listAuth.get(i).getAuthId());
//    				map.put("peopolId", sysRoleAuth.get(0).getId());
//    			}else
//    			if(listAuth.get(i).getAuthId()==2){
//    				map.put("actor",  listAuth.get(i).getAuthId());
//    				map.put("actorId", sysRoleAuth.get(0).getId());
//    			}else{
//    				map.put("auth",  listAuth.get(i).getAuthId());
//    				map.put("authId", sysRoleAuth.get(0).getId());
//    			}
//    		}
//    	}*/
//        listResult.add(map);
//        response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
//    }





    /**
     * 修改角色保存
     * @param request
     * @param response
     * @throws Exception
     */
//    @RequestMapping(value="/reviseRole", method=RequestMethod.POST)
//    public void reviseRole(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception{
//        request.setCharacterEncoding("UTF-8");
//        List<Map<String,Object>> listResult=new ArrayList<Map<String,Object>>();
//
//        HttpServletRequest servletRequest = (HttpServletRequest) request;
//        HttpServletResponse servletResponse = (HttpServletResponse) response;
//        HttpSession session = servletRequest.getSession();
//        Integer userId =  (Integer) session.getAttribute("userId");//获取用户id
//        String authDegrees =  (String) session.getAttribute("authDegree");//获取用户权限
//        String operator =  (String) session.getAttribute("userName");//获取当前登录胡名
//        String[] array = authDegrees.split(",");
//        Boolean flag = true;
//        for(String auth:array){
//            if(auth.equals(ZsAuthDegree.ROLE_AUTH)){
//                flag = false;
//                break;
//            }
//        }
//
//        if(flag){//没有角色管理权限返回
//            return;
//        }
//
//        String roleName = request.getParameter("roleName").trim();//角色名称
//        String remark = request.getParameter("remark");
//        String describeActor=request.getParameter("describeActor");
//        String roleId = request.getParameter("id");//需要修改的id
//        //String operator = request.getParameter("operator");//创建者
//        // 权限关联表
//        String resvice = request.getParameter("resvice");//权限
//        String peo = request.getParameter("peo");//是否给予人员管理权限
//        String actor = request.getParameter("actor");//是否给予角色管理
//
//        String area = request.getParameter("area");//是否给予区域管理
//        String param = request.getParameter("param");//是否给予参数管理
//
//        String examine = request.getParameter("examine");//是否给予参数管理
//
//        // 先插入角色
//        //获取当前时间
//        Date date=new Date();
//        DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String createDate=format.format(date);
//        // 先查询角色名是否存在
//        // 判断角色名是否修改
//        if(!"1".equals(roleName)){
//            int count=roleService.getIsRoleName(roleName);
//            if (count>=1) {
//                Map<String,Object>  map=new HashMap<String,Object>();
//                map.put("flag", "2");
//                listResult.add(map);
//                response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
//                return;
//            }
//        }else{
//            roleName=null;
//        }
//        if(ZsAuthDegree.INVESTIGATOR_AUTH.equals(resvice)){
//            peo="0";
//            actor="0";
//            area="0";
//            param="0";
//            examine="0";
//        }
//        if(ZsAuthDegree.THIRD_AUTH.equals(resvice)||ZsAuthDegree.SECOND_AUTH.equals(resvice)){
//            actor="0";
//            area="0";
//            param="0";
//        }
//        if(ZsAuthDegree.FIRST_AUTH.equals(resvice)){
//            actor="0";
//            area="0";
//        }
//        // 修改插入数据
//        SysRole sysRole=new SysRole();
//
//
//        /** gdsfgsd*/
////    	sysRole.setRoleId(Integer.parseInt(roleId));
////    	sysRole.setRoleName(roleName);
////    	sysRole.setRemark(remark);
////    	sysRole.setCreateDate(createDate);
////    	sysRole.setDescribeActor(describeActor);
////    	sysRole.setOperator(operator);
//
//
//        int k=roleService.updateRole(sysRole);
//        // 插入关联表
//        // 先删除角色权限关联表
//        int rolAuto=roleService.deleteRoleAuto(Integer.parseInt(roleId));
//        int sysRoleId=Integer.parseInt(roleId);
//        if(ZsAuthDegree.PERSON_AUTH.equals(peo)){
//            SysAuthorization sysAuthorization = roleService.getAuthByDegree(Integer.parseInt(peo));
//
//            int sysAuthId=sysAuthorization.getAuthId();
//            //int sysAuthId=Integer.parseInt(peo);
//            SysRoleAuth sysRoleAuth=new SysRoleAuth();
//            sysRoleAuth.setSysRoleId(sysRoleId);
//            sysRoleAuth.setSysAuthId(sysAuthId);
//            int peoReturn=roleService.insertAuthRole(sysRoleAuth);
//        }
//        if(ZsAuthDegree.ROLE_AUTH.equals(actor)){
//            SysAuthorization sysAuthorization = roleService.getAuthByDegree(Integer.parseInt(actor));
//
//            int sysAuthId=sysAuthorization.getAuthId();
//            //int sysAuthId=Integer.parseInt(actor);
//            SysRoleAuth sysRoleAuth=new SysRoleAuth();
//            sysRoleAuth.setSysRoleId(sysRoleId);
//            sysRoleAuth.setSysAuthId(sysAuthId);
//            int actorReturn=roleService.insertAuthRole(sysRoleAuth);
//        }
//
//        //添加区域管理权限
//        if(ZsAuthDegree.REGION_AUTH.equals(area)){
//            SysAuthorization sysAuthorization = roleService.getAuthByDegree(Integer.parseInt(area));
//
//            int sysAuthId=sysAuthorization.getAuthId();
//            //int sysAuthId=Integer.parseInt(area);
//            SysRoleAuth sysRoleAuth=new SysRoleAuth();
//            sysRoleAuth.setSysRoleId(sysRoleId);
//            sysRoleAuth.setSysAuthId(sysAuthId);
//            int areaReturn=roleService.insertAuthRole(sysRoleAuth);
//        }
//        //添加参数管理权限
//        if(ZsAuthDegree.PARAMETER_AUTH.equals(param)){
//            SysAuthorization sysAuthorization = roleService.getAuthByDegree(Integer.parseInt(param));
//
//            int sysAuthId=sysAuthorization.getAuthId();
//            //int sysAuthId=Integer.parseInt(param);
//            SysRoleAuth sysRoleAuth=new SysRoleAuth();
//            sysRoleAuth.setSysRoleId(sysRoleId);
//            sysRoleAuth.setSysAuthId(sysAuthId);
//            int paramReturn=roleService.insertAuthRole(sysRoleAuth);
//        }
//
//        //添加审核管理权限
//        if(ZsAuthDegree.TO_EXAMINE_AUTH.equals(examine)){
//            SysAuthorization sysAuthorization = roleService.getAuthByDegree(Integer.parseInt(examine));
//
//            int sysAuthId=sysAuthorization.getAuthId();
//            SysRoleAuth sysRoleAuth=new SysRoleAuth();
//            sysRoleAuth.setSysRoleId(sysRoleId);
//            sysRoleAuth.setSysAuthId(sysAuthId);
//            int examineReturn=roleService.insertAuthRole(sysRoleAuth);
//        }
//
//        if (ZsAuthDegree.CITY_AUTH.equals(resvice)||ZsAuthDegree.FIRST_AUTH.equals(resvice)
//                ||ZsAuthDegree.SECOND_AUTH.equals(resvice)||ZsAuthDegree.THIRD_AUTH.equals(resvice)
//                ||ZsAuthDegree.INVESTIGATOR_AUTH.equals(resvice)){
//            SysAuthorization sysAuthorization = roleService.getAuthByDegree(Integer.parseInt(resvice));
//
//            int sysAuthId=sysAuthorization.getAuthId();
//            SysRoleAuth sysRoleAuth=new SysRoleAuth();
//            //int sysAuthId=Integer.parseInt(resvice);
//            sysRoleAuth.setSysRoleId(sysRoleId);
//            sysRoleAuth.setSysAuthId(sysAuthId);
//            int resviceReturn=roleService.insertAuthRole(sysRoleAuth);
//        }
//
//        Map<String,Object>  map=new HashMap<String,Object>();
//        if (k>0) {
//            map.put("flag", "1");
//            listResult.add(map);
//        }else {
//            map.put("flag", "0");
//            listResult.add(map);
//        }
//        response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
//    }
}
