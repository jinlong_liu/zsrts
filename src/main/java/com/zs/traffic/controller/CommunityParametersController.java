package com.zs.traffic.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.zs.traffic.model.Area;
import com.zs.traffic.service.AreaService;
import com.zs.traffic.service.ParametersChangeService;
import com.zs.utils.ExcelReader;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;

/**
 * @description 参数设置
 * 
 * @author anTL
 * 
 * @date 2017-04-17
 *
 */

@Controller
@RequestMapping("/")
public class CommunityParametersController extends BaseController {
	@Autowired
	public ParametersChangeService auService;
	@Autowired
	public AreaService areaService;

	/**
	 * 查询所有参数
	 * 
	 */
	@RequestMapping(value = "/getAreaList", method = RequestMethod.POST)
	public void getAeraList(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		List<Area> list = auService.getAreaList(cityCode);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		/* System.out.println(list.get(0).getCommunityName()); */
		int count = 0;
		if (list != null && list.size() > 0) {
			count = list.size();
			for (int j = 0; j < count; j++) {
				int number = j + 1;
				Area entity = list.get(j);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("number", number); // 序号
				map.put("areaId", entity.getAreaId());
				map.put("districtName", entity.getDistrictName()); // 行政区
				map.put("subofficeName", entity.getSubofficeName()); // 街道
				map.put("communityName", entity.getCommunityName()); // 社区
				map.put("totalCount", entity.getTotalCount()); // 指标数
				map.put("everyCount", entity.getEverydayCount()); // 每天完成度
				listResult.add(map);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	/**
	 * 指标跟提交数量 一起设置
	 * 
	 */
	@RequestMapping(value = "/updateParameter", method = RequestMethod.POST)
	public void updateParameter(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		String everTotal = request.getParameter("everTotal").trim();// 获取指标的参数
		String everyDayTotal = request.getParameter("everyDayTotal").trim();// 获取提交参数
		String ids = request.getParameter("ids").trim();// 获取id数组
		String[] split = ids.split(",");// 以逗号切分字符串
		Map<String, Object> backMap = new HashMap<String, Object>();
		int areaId = 0;
		int updateSelective = 0;
		int update = 0;
		int sel = split.length;
		for (int i = 0; i < split.length; i++) {
			String id = split[i];
			areaId = Integer.parseInt(id);
			// System.out.println(areaId);
			Area record = new Area();
			record.setTotalCount(Integer.parseInt(everTotal));
			record.setEverydayCount(Integer.parseInt(everyDayTotal));
			updateSelective = auService.updateParameter(record, areaId);
			update = updateSelective + update;
		}
		if (update == sel) {
			backMap.put("flag", "1");
			backMap.put("message", "修改成功!");
		} else {
			backMap.put("flag", "0");
			backMap.put("message", "只修改成功了" + update + "条信息");
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 设置提交数量
	 * 
	 */
	@RequestMapping(value = "/updateEveryday", method = RequestMethod.POST)
	public void updateEveryday(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		String Total = request.getParameter("everyDayTotal").trim();
		String ids = request.getParameter("ids").trim();
		Map<String, Object> backMap = new HashMap<String, Object>();
		String[] split = ids.split(",");// 字符串按逗号切分
		int updateSelective = 0;
		int update = 0;
		int sel = split.length;
		for (String string : split) {
			int everydayCount = Integer.parseInt(Total);
			int areaId = Integer.parseInt(string);
			// System.out.println(Total+"---"+areaId);
			updateSelective = auService.updateEveryday(everydayCount, areaId);
			update = updateSelective + update;
			// System.out.println(update);
		}
		if (update == sel) {
			backMap.put("flag", "1");

			backMap.put("message", "修改成功!");
		} else {
			backMap.put("flag", "0");
			backMap.put("message", "只修改成功了" + update + "条信息");
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 设置指标数量
	 * 
	 */
	@RequestMapping(value = "/updateTotal", method = RequestMethod.POST)
	public void updateTotal(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		String everTotal = request.getParameter("everTotal").trim();
		String ids = request.getParameter("ids").trim();
		Map<String, Object> backMap = new HashMap<String, Object>();
		String[] split = ids.split(",");// 字符串按逗号切分
		int updateSelective = 0;
		int update = 0;
		int sel = split.length;
		for (String string : split) {
			// System.out.println(everTotal+"----"+string);
			int totalCount = Integer.parseInt(everTotal);
			int areaId = Integer.parseInt(string);
			updateSelective = auService.updateTotal(totalCount, areaId);
			update = updateSelective + update;
			// System.out.println(update);
		}
		if (update == sel) {
			backMap.put("flag", "1");

			backMap.put("message", "修改成功!");
		} else {
			backMap.put("flag", "0");
			backMap.put("message", "只修改成功了" + update + "条信息");
		}
		response.getWriter().write(ZsJsonUtil.map2Json(backMap).toString());
	}

	/**
	 * 导入参数
	 */
	@RequestMapping(value = "/fileUploa", method = RequestMethod.POST)
	public void fileUploa(@RequestParam(value = "file") MultipartFile file, XssHttpServletRequestWrapper request,
			HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		/*
		 * Iterator iterator = file.getFileNames(); MultipartFile multifile =
		 * file.getFile((String) iterator.next());
		 */
		String path = request.getSession().getServletContext().getRealPath("upload");
		String fileName = file.getOriginalFilename();
		SimpleDateFormat appendIx_id = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		/* fileName.replace(".xls", appendIx_id+".xls"); */
		File targetFile = new File(path, fileName);
		// 设置文件保存的路径 提供给后面的解析使用
		request.getSession().setAttribute("fileNameSpare", fileName);
		request.getSession().setAttribute("filePathSpare", path);
		FileUtils.copyInputStreamToFile(file.getInputStream(), new File(path, fileName));
		/*
		 * SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		 * MultipartHttpServletRequest multipartRequest =
		 * (MultipartHttpServletRequest) request; // 获取传入文件
		 * multipartRequest.setCharacterEncoding("utf-8"); List<MultipartFile>
		 * files = multipartRequest.getFiles("myFile"); SimpleDateFormat
		 * appendIx_id = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		 * if(!files.isEmpty()&&files.size()>0){ for (MultipartFile file :
		 * files) { if(file.getSize() < 1) { continue; } String realPath =
		 * request.getSession().getServletContext() .getRealPath("/upload");
		 * String fileName = file.getOriginalFilename();
		 * FileUtils.copyInputStreamToFile(file.getInputStream(), new File(
		 * realPath, fileName)); }
		 * 
		 * }
		 */
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		String realDir = servletRequest.getSession().getServletContext().getRealPath("");
		String contextpath = servletRequest.getContextPath();
		String basePath = servletRequest.getScheme() + "://" + servletRequest.getServerName() + ":"
				+ servletRequest.getServerPort() + contextpath + "/";
		/*
		 * System.out.println("basePath    "+basePath); System.out.println(
		 * "realDir    "+realDir); System.out.println("contextpath    "
		 * +contextpath);
		 */
		Map<String, Object> backMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertItems = new ArrayList<Map<String, Object>>();// 更新
		Map<String, Object> addMap = new HashMap<String, Object>();
		String type = request.getParameter("type").trim();// 街道办编号
		try {
			// 对读取Excel表格标题测试
			InputStream is = new FileInputStream(targetFile);
			ExcelReader excelReader = new ExcelReader();
			String[] title = excelReader.readExcelTitle(is);
			// System.out.println("获得Excel表格的标题:");
			for (String s : title) {
				// System.out.print(s + " ");
			}
			// 对读取Excel表格内容测试
			InputStream is2 = new FileInputStream(targetFile);
			Map<Integer, String> map = excelReader.readExcelContent(is2);
			// System.out.println("获得Excel表格的内容:");
			String communityNumbers = "";
			String communityNames = "";
			String totalCounts = "";
			String everydayCounts = "";
			if (type.equals("2")) {
				for (int i = 1; i <= map.size(); i++) {
					String k = map.get(i);
					String[] data = k.split(",");
					addMap = new HashMap<String, Object>();
					addMap.put("totalCount", data[7]);
					addMap.put("communityNumber", data[4]);
					addMap.put("communityName", data[5]);
					if (i != map.size()) {
						communityNames += data[5] + ",";
						totalCounts += data[7] + ",";
						communityNumbers += data[4].replace(".0", "") + ",";
					} else {
						communityNames += data[5];
						totalCounts += data[7];
						communityNumbers += data[4].replace(".0", "");
					}
					insertItems.add(addMap);
				}
			} else if (type.equals("3")) {
				for (int i = 1; i <= map.size(); i++) {
					String k = map.get(i);
					String[] data = k.split(",");

					addMap = new HashMap<String, Object>();
					addMap.put("communityName", data[5]);
					addMap.put("everydayCount", data[8]);
					addMap.put("communityNumber", data[4]);
					if (i != map.size()) {
						communityNames += data[5] + ",";
						everydayCounts += data[8] + ",";
						communityNumbers += data[4].replace(".0", "") + ",";
					} else {
						communityNames += data[5];
						everydayCounts += data[8];
						communityNumbers += data[4].replace(".0", "");
					}
					insertItems.add(addMap);
				}
			} else if (type.equals("1")) {
				for (int i = 1; i <= map.size(); i++) {
					String k = map.get(i);
					String[] data = k.split(",");
					addMap = new HashMap<String, Object>();
					addMap.put("communityName", data[5]);
					addMap.put("communityNumber", data[4]);
					addMap.put("totalCount", data[7]);
					addMap.put("everydayCount", data[8]);
					if (i != map.size()) {
						communityNames += data[5] + ",";
						totalCounts += data[7] + ",";
						everydayCounts += data[8] + ",";
						communityNumbers += data[4].replace(".0", "") + ",";

					} else {
						communityNumbers += data[4].replace(".0", "");
						communityNames += data[5];
						totalCounts += data[7];
						everydayCounts += data[8];

					}
					insertItems.add(addMap);
				}
			}
			String community = "";
			if (type.equals("2")) {
				int update = 0;
				for (int i = 0; i < insertItems.size(); i++) {
					Map<String, Object> map2 = insertItems.get(i);
					String communityNumber = (String) map2.get("communityNumber");
					String communityName = (String) map2.get("communityName");
					String object2 = (String) map2.get("totalCount");
					String b = object2.replaceAll(".0*$", "");
					int totalCount = Integer.parseInt(b);
					int count = auService.updateTotalCount(totalCount, communityNumber, cityCode);
					if (count == 0) {
						communityName.trim();
						community = community + communityName + ",";
					}
					update = count + update;// 更新成功的数据量
					int size = insertItems.size();// 在表格里共查出来的数据量
					if (update == size) {
						backMap.put("flag", "1");
						backMap.put("message", "全部更新成功共" + update + "条数据。");
					} else {
						backMap.put("flag", "0");
						backMap.put("message", "在Excel里面共拿到" + size + "条数据，更新成功了" + update + "条" + "，其中：" + community
								+ "没有更新成功,请先添加此社区。");
					}
				}
			} else if (type.equals("3")) {
				/* List<Area> areaList = auService.getAreaList(); */
				int update = 0;
				for (int i = 0; i < insertItems.size(); i++) {
					Map<String, Object> map2 = insertItems.get(i);
					String communityName = (String) map2.get("communityName");
					String communityNumber = (String) map2.get("communityNumber");
					String object = (String) map2.get("everydayCount");
					String a = object.replaceAll(".0*$", "");
					int everydayCount = Integer.parseInt(a);
					int count = auService.updateEverydayCount(everydayCount, communityNumber, cityCode);
					if (count == 0) {
						communityName.trim();
						community = community + communityName + ",";
					}
					update = count + update;// 更新成功的数据量
					int size = insertItems.size();// 在表格里共查出来的数据量
					if (update == size) {
						backMap.put("flag", "1");
						backMap.put("message", "全部更新成功共" + update + "条数据。");
					} else {
						backMap.put("flag", "0");
						backMap.put("message", "在Excel里面共拿到" + size + "条数据，更新成功了" + update + "条" + "，其中：" + community
								+ "没有更新成功,请先添加此社区。");
					}
				}
			}
			backMap.put("status", 1);
			// System.out.println(insertItems+"--");
		} catch (FileNotFoundException e) {
			// System.out.println("未找到指定路径的文件!");
			backMap.put("flag", "1");
			backMap.put("message", "未找到指定路径的文件!");
			e.printStackTrace();
		}
		String map2Json = ZsJsonUtil.map2Json(backMap);
		response.getWriter().write(ZsJsonUtil.map2Json(backMap));
	}

	/**
	 * 条件查询（参数设置） 行政区 街道
	 */
	@RequestMapping(value = "/queryGetparameter", method = RequestMethod.POST)
	public void queryGetparameter(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		String districtNumber = request.getParameter("districtNumber").trim();
		String subofficeNumber = request.getParameter("subofficeNumber").trim();
		String type = request.getParameter("type");
		// System.out.println(districtName+"----"+subofficeName);
		List<Area> list = areaService.queryListParameter(districtNumber, subofficeNumber);
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				int number = i + 1;
				Area entity = list.get(i);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("number", number);
				map.put("areaId", entity.getAreaId());
				map.put("type", entity.getType());
				map.put("districtName", entity.getDistrictName());
				map.put("subofficeName", entity.getSubofficeName());
				map.put("communityName", entity.getCommunityName());
				map.put("totalCount", entity.getTotalCount());
				map.put("everyCount", entity.getEverydayCount());
				listResult.add(map);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
}
