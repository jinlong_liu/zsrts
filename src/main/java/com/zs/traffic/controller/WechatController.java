package com.zs.traffic.controller;

import com.zs.utils.XssHttpServletRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/")
public class WechatController {

    @RequestMapping("/wx")
    @ResponseBody
    public String Wechat(XssHttpServletRequestWrapper request, HttpServletResponse response){
        System.out.println("request = " + request);
        System.out.println("response = " + response);
        return "index";
    }
}
