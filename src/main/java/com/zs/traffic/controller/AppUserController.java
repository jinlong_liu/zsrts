package com.zs.traffic.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.zs.traffic.model.AppLogInfo;
import com.zs.traffic.model.AppSms;
import com.zs.traffic.model.Charge;
import com.zs.traffic.model.CityCodeUrl;
import com.zs.traffic.model.RegisterNumber;
import com.zs.traffic.model.SysAppuser;
import com.zs.traffic.service.AppLogInfoService;
import com.zs.traffic.service.AppMsmService;
import com.zs.traffic.service.ChargeService;
import com.zs.traffic.service.CityCodeUrlService;
import com.zs.traffic.service.RegisterNumberService;
import com.zs.traffic.service.RechargeableCardService;
import com.zs.traffic.service.SysAppuserService;
import com.zs.utils.MD5Util;
import com.zs.utils.TxtUtil;
import com.zs.utils.VerificationUtil;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;
import com.zs.utils.randomNumber;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

@Controller
@RequestMapping("/app")
public class AppUserController extends BaseController {

	@Autowired
	public SysAppuserService service;
	@Autowired
	public RegisterNumberService registerNumberService;
	@Autowired
	public ChargeService chargeService;

	@Autowired
	public RechargeableCardService rechargeableCardService;

	@Autowired
	public AppMsmService appMsmService;

	@Autowired
	public CityCodeUrlService cityService;
	
	@Autowired
	public AppLogInfoService appLogInfoService;//记录登陆日志

	public static final String REGEX_MOBILE = "^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
	// public static final String PASSWORD = "/^[0-9A-Za-z]{6,}$/";

	private final static Logger logger = LoggerFactory.getLogger(AppUserController.class);
	
	/**
	 * @description APP注册
	 * @param 用户信息
	 * @return
	 * @throws Exception
	 * @author wangzf
	 * @date 2017-03-31
	 */
	@RequestMapping(value = "/register")
	public void register(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();

		String userName = request.getParameter("userName");// 用户名
		String userPassword = request.getParameter("userPassword");// 密码
		String cityCode = request.getParameter("cityCode");// 城市编码
		String rechargeType = request.getParameter("rechargeType");// 充值类型	
		String verificationCode = request.getParameter("verificationCode");// 验证码

		String mobilePhoneModel = request.getParameter("mobilePhoneModel");//手机型号
		String mobilePhoneManufacturers = request.getParameter("mobilePhoneManufacturers");//APP版本
		String operatingSystemVersion = request.getParameter("operatingSystemVersion");//手机操作系统
		
		if(userName==null|| userPassword==null || cityCode==null || rechargeType==null ||
				verificationCode==null){
			map.put("errNo", "-5");
	 		map.put("result", "缺少参数,请更新至最新版本");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		//不为null，去掉2边空格
		userName = userName.trim();
		userPassword = userPassword.trim();
		cityCode = cityCode.trim();
		rechargeType = rechargeType.trim();
		verificationCode = verificationCode.trim();
		
		if(cityCode.isEmpty() || rechargeType.isEmpty() || verificationCode.isEmpty()){
			map.put("errNo", "-12");
	 		map.put("result", "参数有空值,请更新至最新版本");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(userName.length()>20 || userPassword.length()>200 || cityCode.length()>255 || 
				rechargeType.length()>11 ||verificationCode.length()>255){
			map.put("errNo", "-16");
	 		map.put("result", "参数长度超出范围！");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(VerificationUtil.isNumeric(rechargeType)){
			map.put("errNo", "-13");
	 		map.put("result", "数字类型参数格式不正确，请更新至最新版本");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		
		if(userName.isEmpty()){
			map.put("errNo", "-14");
	 		map.put("result", "用户名不能为空");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(userPassword.isEmpty()){
			map.put("errNo", "-15");
	 		map.put("result", "密码不能为空");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		System.out.println(getProperties().getProperty("ipJudge"));
	
		int status = 99;
		String verificationCodeSelect = "";
		// 验证码是否有效
		SimpleDateFormat sdfVfc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar beforeTime = Calendar.getInstance();
		beforeTime.add(Calendar.MINUTE, -5);// 5分钟之前的时间
		Date sendTime = beforeTime.getTime();
		String time = sdfVfc.format(sendTime);
		List<AppSms> AppSmsList = appMsmService.selectByUserNameTime(userName, cityCode, sendTime);
		if (AppSmsList.size() > 0) {
			status = AppSmsList.get(0).getStatus();
			verificationCodeSelect = String.valueOf(AppSmsList.get(0).getVerificationCode());
		} else {
			map.put("errNo", "-11");
			map.put("result", "验证码超时，无效！");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		if (verificationCodeSelect.equals(verificationCode)) {
			if (status == 1) {
				map.put("errNo", "-9");
				map.put("result", "验证码不可重复使用！");
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				return;
			}
		}
		if (!verificationCodeSelect.equals(verificationCode)) {
			map.put("errNo", "-10");
			map.put("result", "验证码错误！");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}

		String ip = null;
		if ("1".equals(getProperties().getProperty("ipJudge"))) {
			HttpServletRequest servletRequest = (HttpServletRequest) request;

			if (request.getHeader("x-forwarded-for") == null) {
				ip = request.getRemoteAddr();
			} else {
				ip = request.getHeader("x-forwarded-for");
			}

			List<SysAppuser> listAppUser = service.selectByOccupation(ip);// 通过ip查询是否已经注册过了
			if (listAppUser.size() > 0) {
				map.put("errNo", "-6");
				map.put("result", "您已经注册过了!");
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				return;
			}
		}

		/*
		 * if ("".equals(rechargeType)) { map.put("errNo", "-6");
		 * map.put("result", "APP版本异常!");
		 * response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
		 * return; }
		 */
		int chargeType = Integer.parseInt(rechargeType);
		// 校验手机号
		/*
		 * if(!Pattern.matches(REGEX_MOBILE, userName)){ map.put("errNo", "-2");
		 * map.put("result", "用户名格式错误！");
		 * response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
		 * return; }
		 */
		Date date = new Date();
		SimpleDateFormat tsdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dayDate = tsdf.format(date);

		Date todayDate = tsdf.parse(dayDate);

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(todayDate);
		calendar.add(calendar.DATE, 1);// 把日期往后增加一天.整数往后推,负数往前移动
		Date tomorrowDate = calendar.getTime(); // 这个时间就是日期往后推一天的结果

		String today = sdf.format(todayDate);
		String tomorrowDay = sdf.format(tomorrowDate);

		// int number = service.selectQuantity(today,tomorrowDay);//查询出每天的注册量

		/*
		 * if(number>ZsConstant.NUMBER){//每天注册量限制 map.put("errNo", "-5");
		 * map.put("result", "今天注册量以达到上限！");
		 * response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
		 * return; }
		 */

		//
		RegisterNumber registNumber = registerNumberService.selectByRechargeType(chargeType);

		int total = chargeService.selectTotal(chargeType);

		if (chargeType == 1) {// 20元话费的每天加限制
			int todayNumber = chargeService.selectTodayNumber(chargeType, today, tomorrowDay);
			if (todayNumber > registNumber.getRegisterNumDay()) {
				map.put("errNo", "-7");
				map.put("result", "当天注册量已达上限!");
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				return;
			}
		}

		if (total > registNumber.getRegisterNumTotal()) {
			map.put("errNo", "-8");
			map.put("result", "注册量已达上限!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}

		// 用户名不能为空
		if ("".equals(userName)) {
			map.put("errNo", "-2");
			map.put("result", "用户名格式错误！");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		// 密码不能为空
		if ("".equals(userPassword)) {
			map.put("errNo", "-3");
			map.put("result", "密码不能为空！");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		// 账号验证是否已经注册过
		SysAppuser appUser = service.selectByUserName(userName);
		if (appUser != null) {
			map.put("errNo", "-4");
			map.put("result", "此帐号已经注册过了!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}

		SysAppuser model = new SysAppuser();// 对应的实体类
		model.setUserName(userName);
		// MD5Util md5=new MD5Util();
		model.setUserPassword(userPassword);

		model.setCreateDate(sdf.format(new Date()));
		model.setStag(0);// 查看问卷完成进度，默认第一次进来时填1

		model.setOccupation(ip);

		model.setCityCode(cityCode);
		int ret = service.insertSelective(model);

		SysAppuser appUserFirst = service.selectByUserName(userName);

		if (AppSmsList.size() > 0) {
			AppSms appSms = AppSmsList.get(0);
			if (appSms != null) {
				appSms.setStatus(1);
				appMsmService.updateByPrimaryKeySelective(appSms);
			}
		}

		// 注册成功后向话费信息表加数据
		Charge charge = new Charge();
		charge.setAppuserId(appUserFirst.getAppuserId());
		charge.setChargeType(chargeType);
		charge.setExtended1(sdf.format(new Date()));

		int retu = chargeService.insertSelective(charge);
		if (retu < 0) {
			map.put("errNo", "-1");
			map.put("result", "注册失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}

		if (ret > 0) {
			//request.getSession().setAttribute("appuserId", appUserFirst.getAppuserId());
			// request.getSession().setAttribute("userName",
			// appUser1.getUserName());
			map.put("errNo", "0");
			map.put("result", "注册成功!");
			
			AppLogInfo record = new AppLogInfo();
			
			record.setAppuserId(appUserFirst.getAppuserId());
			  
			if(mobilePhoneModel == null || mobilePhoneModel.trim().isEmpty() || mobilePhoneModel.trim().length() >255){
				record.setMobilePhoneModel("未知");
			}else{
				record.setMobilePhoneModel(mobilePhoneModel);
			}
			if(mobilePhoneManufacturers == null || mobilePhoneManufacturers.trim().isEmpty() || mobilePhoneManufacturers.trim().length() >255){
				record.setMobilePhoneManufacturers("APP版本:未知");
			}else{
				record.setMobilePhoneManufacturers("APP版本:"+mobilePhoneManufacturers);
			}
			if(operatingSystemVersion == null || operatingSystemVersion.trim().isEmpty() || operatingSystemVersion.trim().length() > 255){
				record.setOperatingSystemVersion("未知");
			}else{
				record.setOperatingSystemVersion(operatingSystemVersion);
			}
			record.setCityCode(cityCode);
			record.setLoginTime(new Date());
			record.setRemark("注册");
			
			appLogInfoService.insertSelective(record);//登录成功，记录日志保存到数据库
			
			map.put("appuserId", appUserFirst.getAppuserId());// 向前台扔一个app用户id
			map.put("userName", userName);
			map.put("cityCode", cityCode);
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		} else {
			map.put("errNo", "-1");
			map.put("result", "注册失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}

	}

	/**
	 * @description 登录
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author wangzf
	 * @date 2017-03-31
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public void login(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		String userName = request.getParameter("userName");// 姓名
		String userPassword = request.getParameter("userPassword");// 密码 
		
		String mobilePhoneModel = request.getParameter("mobilePhoneModel");//手机型号
		String mobilePhoneManufacturers = request.getParameter("mobilePhoneManufacturers");//APP版本
		String operatingSystemVersion = request.getParameter("operatingSystemVersion");//手机操作系统
		
		//HttpSession session = request.getSession();
		
		if(userName==null|| userPassword==null){
			map.put("errNo", "-5");
	 		map.put("result", "缺少参数,请更新至最新版本");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		userName = userName.trim();
		userPassword = userPassword.trim();
		if(userName.length()>20 || userPassword.length()>200){
			map.put("errNo", "-6");
	 		map.put("result", "参数长度超出范围！");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		// 用户名不能为空
		if ("".equals(userName) || userName.isEmpty()) {
			map.put("errNo", "-2");
			map.put("result", "用户名不能为空！");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		// 密码不能为空
		if ("".equals(userPassword) || userPassword.isEmpty()) {
			map.put("errNo", "-3");
			map.put("result", "密码不能为空！");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		// 账号校验
		SysAppuser appUser = service.selectByUserName(userName);
		if (appUser == null) {
			map.put("errNo", "-4");
			map.put("result", "此账号不存在!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		// 密码校验
		Boolean flag = userPassword.equals(appUser.getUserPassword());// 密码在前台js加密
		if (!flag) {
			map.put("errNo", "-1");
			map.put("result", "密码错误!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		//request.getSession().setAttribute("appuserId", appUser.getAppuserId());
		// request.getSession().setAttribute("userName", appUser.getUserName());

		map.put("errNo", "0");
		map.put("result", "登录成功");
		
		AppLogInfo record = new AppLogInfo();
		
		record.setAppuserId(appUser.getAppuserId());
		 
		if(mobilePhoneModel == null || mobilePhoneModel.trim().isEmpty() || mobilePhoneModel.trim().length() >255){
			record.setMobilePhoneModel("未知");
		}else{
			record.setMobilePhoneModel(mobilePhoneModel);
		}
		if(mobilePhoneManufacturers == null || mobilePhoneManufacturers.trim().isEmpty() || mobilePhoneManufacturers.trim().length() >255){
			record.setMobilePhoneManufacturers("APP版本:未知");
		}else{
			record.setMobilePhoneManufacturers("APP版本:"+mobilePhoneManufacturers);
		}
		if(operatingSystemVersion == null || operatingSystemVersion.trim().isEmpty() || operatingSystemVersion.trim().length() > 255){
			record.setOperatingSystemVersion("未知");
		}else{
			record.setOperatingSystemVersion(operatingSystemVersion);
		}
		record.setCityCode(appUser.getCityCode());
		record.setLoginTime(new Date());
		record.setRemark("登录");
		
		appLogInfoService.insertSelective(record);//登录成功，记录日志保存到数据库
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		logger.info("APP用户登录时间"+sdf.format(new Date())+", APP用户"+userName+"登录成功！");
		
		map.put("stag", appUser.getStag());// 每次登陆返回一个值给前台，验证填写进度
		map.put("appuserId", appUser.getAppuserId());// 向前台扔一个app用户id
		map.put("userName", userName);
		map.put("cityCode", appUser.getCityCode());
		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
		return;
	}

	/**
	 * @description APP端家庭信息提交和修改  (版本更新后弃用)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author wangzf
	 * @date 2017-03-31
	 */
	@RequestMapping(value = "/updateFamilyInfo", method = RequestMethod.POST)
	public void updateFamilyInfo(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		SysAppuser model = new SysAppuser();// 对应的实体类

		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		// Integer appuserId = (Integer)
		// session.getAttribute("appuserId");//数据不从session中取，从前台直接传过来

		String id = request.getParameter("appuserId");// 必传字段，不能为空
		String userName = request.getParameter("userName");// 必传字段，不能为空
		// String cityCode = request.getParameter("cityCode");// 必传字段，不能为空
		Integer appuserId = Integer.parseInt(id);// 转成Integer类型

		// String age = request.getParameter("age").trim();
		// String sex = request.getParameter("sex").trim();
		// String Occupation = request.getParameter("Occupation").trim();
		String familyAddress = request.getParameter("familyAddress").trim();
		String stag = request.getParameter("stag").trim();
		String electricBikes = request.getParameter("electricBikes").trim();// 电动车数量
		String carCount = request.getParameter("carCount").trim();// 汽车数量
		String homesCount = request.getParameter("homesCount").trim();// 家庭成员数量
		// byte[] jiema= remark.getBytes("UTF-8") ; //解码
		// String bianma = new String(jiema,"UTF-8");//测试String编码类型为UTF-8
		/*
		 * if(!age.isEmpty()){ Integer ag = Integer.parseInt(age);
		 * model.setAge(ag); } if(!sex.isEmpty()){ Integer se =
		 * Integer.parseInt(sex); model.setSex(se); } if(!Occupation.isEmpty()){
		 * Integer Occupatio = Integer.parseInt(Occupation);
		 * model.setOccupation(Occupatio); }
		 */
		if (!familyAddress.isEmpty()) {
			model.setFamilyAddress(familyAddress);
		}
		if (!stag.isEmpty()) {
			Integer sta = Integer.parseInt(stag);
			model.setStag(sta);
		}
		if (!electricBikes.isEmpty()) {// 电动车数量,不填默认为0
			Integer electricBike = Integer.parseInt(electricBikes);
			model.setElectricBikes(electricBike);
		}

		if (!carCount.isEmpty()) {// 汽车数量,不填默认为0
			Integer carCoun = Integer.parseInt(carCount);
			model.setCarCount(carCoun);
		}
		if (!homesCount.isEmpty()) {// 家庭成员
			Integer homesCoun = Integer.parseInt(homesCount);
			model.setHomesCount(homesCoun);
		}

		model.setAppuserId(appuserId);
		int ret = service.updateByPrimaryKeySelective(model);
		if (ret > 0) {
			SysAppuser appUser = service.selectByPrimaryKey(appuserId);
			// map.put("appUser", appUser);
			map.put("appuserId", appUser.getAppuserId());
			map.put("userName", appUser.getUserName());
			map.put("userPassword", appUser.getUserPassword());
			map.put("createDate", appUser.getCreateDate());
			map.put("electricBikes", appUser.getElectricBikes());
			// map.put("age", appUser.getAge());
			// map.put("sex", appUser.getSex());
			// map.put("Occupation", appUser.getOccupation());
			map.put("familyAddress", appUser.getFamilyAddress());
			map.put("homesCount", appUser.getHomesCount());
			map.put("carCount", appUser.getCarCount());
			map.put("stag", appUser.getStag());

			map.put("cityCode", appUser.getCityCode());

			String filePathAndName = getProperties().getProperty("filePathDetail") + "Family/" + appUser.getCityCode()
					+ "/" + appUser.getUserName() + "family.txt";
			String fileContent = ZsJsonUtil.map2Json(map).toString();
			map.clear();
			TxtUtil.newFile(filePathAndName, fileContent);
			map.put("errNo", "0");
			map.put("result", "保存成功!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		} else {
			map.put("errNo", "-1");
			map.put("result", "保存失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
	}

	/**
	 * @description APP端家庭信息查看   (版本更新后弃用)
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author wangzf
	 * @date 2017-03-31
	 */
	@RequestMapping(value = "/queryFamilyInfoById", method = RequestMethod.POST)
	public void queryFamilyInfoById(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();

		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		// Integer appuserId = (Integer)
		// session.getAttribute("appuserId");//数据不从session中取，从前台直接传过来
		String id = request.getParameter("appuserId");// 必传字段，不能为空
		String userName = request.getParameter("userName");// 必传字段，不能为空
		// String cityCode = request.getParameter("cityCode");// 必传字段，不能为空
		Integer appuserId = Integer.parseInt(id);// 转成Integer类型

		SysAppuser appUser = service.selectByPrimaryKey(appuserId);

		if (appUser != null) {
			map.put("appuserId", appUser.getAppuserId());
			map.put("userName", appUser.getUserName());
			map.put("userPassword", appUser.getUserPassword());
			map.put("createDate", appUser.getCreateDate());
			map.put("electricBikes", appUser.getElectricBikes());
			// map.put("age", appUser.getAge());
			// map.put("sex", appUser.getSex());
			// map.put("Occupation", appUser.getOccupation());
			map.put("familyAddress", appUser.getFamilyAddress());
			map.put("homesCount", appUser.getHomesCount());
			map.put("carCount", appUser.getCarCount());
			map.put("stag", appUser.getStag());
			map.put("cityCode", appUser.getCityCode());
		}

		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	}

	/**
	 * @description APP端密码修改
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author wangzf
	 * @date 2017-04-27
	 */
	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	public void updatePassword(XssHttpServletRequestWrapper request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		SysAppuser model = new SysAppuser();// 对应的实体类

		String id = request.getParameter("appuserId");// 必传字段，不能为空
		
		String oldUserPassword = request.getParameter("oldUserPassword");// 原密码
		String userPassword = request.getParameter("userPassword");// 新密码
		
		if(id == null || oldUserPassword == null || userPassword == null){
			map.put("errNo", "-5");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		id = id.trim();
		oldUserPassword = oldUserPassword.trim();
		userPassword = userPassword.trim();
		if(id.isEmpty() || oldUserPassword.isEmpty() || userPassword.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(id.length()>11 || oldUserPassword.length() >200 || userPassword.length() > 200){
			map.put("errNo", "-7");
	 		map.put("result", "参数长度超出范围！");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(VerificationUtil.isNumeric(id)){
			map.put("errNo", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		Integer appuserId = Integer.parseInt(id);// 转成Integer类型

		SysAppuser appUser = service.selectByPrimaryKey(appuserId);
		if(appUser == null){
			map.put("errNo", "-6");
	 		map.put("result", "请核对该用户是否存在");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		Boolean flag = oldUserPassword.equals(appUser.getUserPassword());// 密码在前台js加密
		if (!flag) {// 对原密码进行校验
			map.put("errNo", "-2");
			map.put("result", "请确认原密码");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}

		model.setUserPassword(userPassword);
		model.setAppuserId(appuserId);
		int ret = service.updateByPrimaryKeySelective(model);
		if (ret > 0) {
			// map.put("appUser", appUser);
			map.put("appuserId", appUser.getAppuserId());
			map.put("userName", appUser.getUserName());
			map.put("userPassword", appUser.getUserPassword());
			map.put("createDate", appUser.getCreateDate());
			map.put("electricBikes", appUser.getElectricBikes());
			// map.put("age", appUser.getAge());
			// map.put("sex", appUser.getSex());
			// map.put("Occupation", appUser.getOccupation());
			map.put("familyAddress", appUser.getFamilyAddress());
			map.put("homesCount", appUser.getHomesCount());
			map.put("carCount", appUser.getCarCount());
			map.put("stag", appUser.getStag());
			map.put("cityCode", appUser.getCityCode());

			String filePathAndName = getProperties().getProperty("filePathDetail") + "Family/" + appUser.getCityCode()
					+ "/" + appUser.getUserName() + "family.txt";
			String fileContent = ZsJsonUtil.map2Json(map).toString();
			map.clear();
			TxtUtil.newFile(filePathAndName, fileContent);
			map.put("errNo", "0");
			map.put("result", "密码修改成功!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		} else {
			map.put("errNo", "-1");
			map.put("result", "密码修改失败，请稍候再试!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
	}

	/**
	 * @description 查询符合送话费条件的list
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author wanglijain
	 * @date 2017-05-18
	 */
	@RequestMapping(value = "/selectListForWinning", method = RequestMethod.POST)
	public void selectListForWinning(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		String id = request.getParameter("appuserId");// 必传字段，不能为空
		//String userName = request.getParameter("userName");// 必传字段，不能为空
		String timing = request.getParameter("timing");// 定时器调用接口
		String cityCode = request.getParameter("cityCode");// 必传字段，不能为空
		
		if(id == null || cityCode == null){
			map.put("num", "-2");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		id = id.trim();
		cityCode = cityCode.trim();
		if(id.isEmpty() || cityCode.isEmpty()){
			map.put("num", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return;
		}
		if(id.length()>11 || cityCode.length() > 255){
			map.put("errNo", "-5");
	 		map.put("result", "参数长度超出范围！");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(VerificationUtil.isNumeric(id)){
			map.put("num", "-4");
	 		map.put("result", "数字类型参数格式不正确");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		Integer appuserId = Integer.parseInt(id);// 转成Integer类型
		// 根据appId查询人的状态
		Charge charge = chargeService.selectByPrimaryKey(appuserId);
		
		CityCodeUrl cityCodeUrl = cityService.selectByCityCode(cityCode, "app");
		
		if (charge != null && cityCodeUrl != null) {
			int state = charge.getIsChargeStatus();
			String extended = charge.getExtended2();
			Integer timeSign = charge.getRegisteredQuantity();
			int type = charge.getChargeType();
			if (type != 3) {
				// 等于1符合送话费条件
				if (timeSign == 2 && timing != null && !"".equals(timing)) {
					map.put("num", "99");
					map.put("result", "定时器请求不再返回消息。");
					map.put("phone", cityCodeUrl.getCityUrl());
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				} else if (state == 1) {
					if (timing != null || !"".equals(timing)) {
						// Charge record = new Charge();
						charge.setRegisteredQuantity(2);
						int success = chargeService.updateByPrimaryKeySelective(charge);
					}
					map.put("num", "1");
					map.put("result", "成功了");
					map.put("phone", cityCodeUrl.getCityUrl());
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				} else if (state == 0 && "1".equals(extended)) {
					map.put("num", "2");
					map.put("result", "抱歉，还有一次获取话费奖励机会，继续加油！");
					map.put("phone", cityCodeUrl.getCityUrl());
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());

				} else if (state == 0 && "2".equals(extended)) {
					map.put("num", "3");
					map.put("result", "彻底没机会了");
					map.put("phone", cityCodeUrl.getCityUrl());
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				} else {
					map.put("num", "4");
					map.put("result", "审核中");
					map.put("phone", cityCodeUrl.getCityUrl());
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				}
			} else {
				if (timeSign == 2 && timing != null && !"".equals(timing)) {
					map.put("num", "99");
					map.put("result", "定时器请求不再返回消息。");
					map.put("phone", cityCodeUrl.getCityUrl());
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				} else if (state == 1) {
					// Charge record = new Charge();
					charge.setRegisteredQuantity(2);
					int success = chargeService.updateByPrimaryKeySelective(charge);
					map.put("num", "1");
					map.put("result", "成功了");
					map.put("phone", cityCodeUrl.getCityUrl());
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				} else if (state == 0) {
					map.put("num", "3");
					map.put("result", "彻底没机会了");
					map.put("phone", cityCodeUrl.getCityUrl());
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				} else {
					map.put("num", "4");
					map.put("result", "审核中");
					map.put("phone", cityCodeUrl.getCityUrl());
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				}

			}

		} else {
			map.put("num", "-1");
			map.put("result", "出现错误");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
		}
	}

	// app发送短信验证码
	@RequestMapping(value = "/MessageAuthentication", method = RequestMethod.POST)
	public void MessageAuthentication(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> map = new HashMap<String, Object>();
		Date requerstDate = new Date();// 请求时间
		String userName = request.getParameter("userName");// 手机号码
		String cityCode = request.getParameter("cityCode");// 城市编码
		if(userName == null || cityCode == null){
			map.put("errNo", "-2");
	 		map.put("result", "缺少参数");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		userName = userName.trim();
		cityCode = cityCode.trim();
		if(userName.isEmpty() || cityCode.isEmpty()){
			map.put("errNo", "-3");
	 		map.put("result", "参数有空值");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		if(userName.length() > 20 || cityCode.length()>255){
			map.put("errNo", "-4");
	 		map.put("result", "参数长度超出范围！");
	 		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	 		return; 
		}
		int randNumType = randomNumber.getRandNum(1, 999999);// 获取验证码
		String randNum = String.format("%06d", randNumType);// 字符串格式化验证码必须为6位不够了往前面补0
		String Account = getProperties().getProperty("Account");// 账号
		String Password = getProperties().getProperty("Password");// 密码
		String UserID = getProperties().getProperty("UserID");// 密码
		SysAppuser appUser = service.selectByUserName(userName);
		String signatureContent = "";
		List<CityCodeUrl> signature = cityService.getSignature(cityCode);
		if (signature != null && signature.size() > 0) {
			signatureContent = signature.get(0).getSignatureContent();
		} else {
			// 如果城市没有配置则返回提示,
			map.put("errNo", "5");
			map.put("result", "该城市暂未开通，暂时不能注册!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		if (appUser != null) {
			map.put("errNo", "4");
			map.put("result", "此帐号已经注册过了!");
			response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			return;
		}
		List<AppSms> list = appMsmService.queryRequestTime(userName);
		Date requestTime = null;
		if (list != null && list.size() > 0) {
			requestTime = list.get(0).getRequestTime();
			long l = requerstDate.getTime() - requestTime.getTime();
			long day = l / (24 * 60 * 60 * 1000);
			long hour = (l / (60 * 60 * 1000) - day * 24);
			long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
			long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
			if (day == 0 && hour == 0 && min == 0) {
				if (s >= 60) {
					String ruslt = "";
					String jobId = "";
					String url = "http://www.mxtong.net.cn/GateWay/Services.asmx/DirectSend?";
					HttpClient httpClient = new HttpClient();
					httpClient.getParams().setBooleanParameter("http.protocol.expect-continue", false);
					PostMethod getMethod = new PostMethod(url);
					String Content = java.net.URLEncoder
							.encode("您好，您本次注册的短信验证码是：" + randNum + "，验证码5分钟内有效，请尽快完成注册。" + signatureContent, "UTF-8");
					/*
					 * SimpleDateFormat df = new SimpleDateFormat(
					 * "yyyy-MM-dd HH:mm:ss");
					 */
					NameValuePair[] data = { new NameValuePair("UserID", UserID), new NameValuePair("Account", Account),
							new NameValuePair("Password", Password), new NameValuePair("Phones", userName),
							new NameValuePair("SendType", "1"), new NameValuePair("SendTime", ""),
							new NameValuePair("PostFixNumber", ""), new NameValuePair("Content", Content) };
					getMethod.setRequestBody(data);
					getMethod.addRequestHeader("Connection", "close");
					try {
						int statusCode = httpClient.executeMethod(getMethod);
						if (statusCode != HttpStatus.SC_OK) {
							System.err.println("Method failed: " + getMethod.getStatusLine());
						}
						byte[] responseBody = getMethod.getResponseBody();
						String str = new String(responseBody, "GBK");
						if (str.contains("GBK")) {
							str = str.replaceAll("GBK", "utf-8");
						}
						int beginPoint = str.indexOf("<RetCode>");
						int endPoint = str.indexOf("</RetCode>");
						int begJob = str.indexOf("<JobID>");
						int endJob = str.indexOf("</JobID>");
						jobId = str.substring(begJob + 7, endJob);
						ruslt = str.substring(beginPoint + 9, endPoint);
						// return getMethod.getResponseBodyAsString();
					} catch (HttpException e) {
						System.out.println(1);
					} catch (IOException e) {
						System.out.println(2);
					} finally {
						getMethod.releaseConnection();
					}
					int isSusses = -1;
					if (ruslt.equals("Sucess")) {
						isSusses = 1;
					} else {
						isSusses = 0;
					}
					Date senddate = new Date();
					AppSms record = new AppSms();
					record.setUserName(userName);// 手机号码
					record.setCityCode(cityCode);// 城市编码
					record.setJobId(jobId);
					record.setRequestTime(requerstDate);// 请求时间
					record.setVerificationCode(randNum);// 验证码
					record.setSendTime(senddate);// 发送短信时间
					record.setIsSuccess(isSusses);
					int addMsm = appMsmService.insertSelective(record);
					if (addMsm == 1) {
						map.put("errNo", "1");// 成功
						map.put("result", "成功");
						response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
					} else {
						map.put("errNo", "2");// 失败
						response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
					}
				} else {
					map.put("errNo", "3");
					map.put("result", "抱歉，在一分钟内不能重复提交");
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				}
			} else {
				String ruslt = "";
				String jobId = "";
				String url = "http://www.mxtong.net.cn/GateWay/Services.asmx/DirectSend?";
				HttpClient httpClient = new HttpClient();
				httpClient.getParams().setBooleanParameter("http.protocol.expect-continue", false);
				PostMethod getMethod = new PostMethod(url);
				String Content = java.net.URLEncoder
						.encode("您好，您本次注册的短信验证码是：" + randNum + "，验证码5分钟内有效，请尽快完成注册。" + signatureContent, "UTF-8");
				/*
				 * SimpleDateFormat df = new SimpleDateFormat(
				 * "yyyy-MM-dd HH:mm:ss");
				 */
				NameValuePair[] data = { new NameValuePair("UserID", UserID), new NameValuePair("Account", Account),
						new NameValuePair("Password", Password), new NameValuePair("Phones", userName),
						new NameValuePair("SendType", "1"), new NameValuePair("SendTime", ""),
						new NameValuePair("PostFixNumber", ""), new NameValuePair("Content", Content) };
				getMethod.setRequestBody(data);
				getMethod.addRequestHeader("Connection", "close");
				try {
					int statusCode = httpClient.executeMethod(getMethod);
					if (statusCode != HttpStatus.SC_OK) {
						System.err.println("Method failed: " + getMethod.getStatusLine());
					}
					byte[] responseBody = getMethod.getResponseBody();
					String str = new String(responseBody, "GBK");
					if (str.contains("GBK")) {
						str = str.replaceAll("GBK", "utf-8");
					}
					int beginPoint = str.indexOf("<RetCode>");
					int endPoint = str.indexOf("</RetCode>");
					int begJob = str.indexOf("<JobID>");
					int endJob = str.indexOf("</JobID>");
					jobId = str.substring(begJob + 7, endJob);
					ruslt = str.substring(beginPoint + 9, endPoint);
					// return getMethod.getResponseBodyAsString();
				} catch (HttpException e) {
					System.out.println(1);
				} catch (IOException e) {
					System.out.println(2);
				} finally {
					getMethod.releaseConnection();
				}
				int isSusses = -1;
				if (ruslt.equals("Sucess")) {
					isSusses = 1;
				} else {
					isSusses = 0;
				}
				Date senddate = new Date();
				AppSms record = new AppSms();
				record.setUserName(userName);// 手机号码
				record.setCityCode(cityCode);// 城市编码
				record.setJobId(jobId);
				record.setRequestTime(requerstDate);// 请求时间
				record.setVerificationCode(randNum);// 验证码
				record.setSendTime(senddate);// 发送短信时间
				record.setIsSuccess(isSusses);
				int addMsm = appMsmService.insertSelective(record);
				if (addMsm == 1) {
					map.put("errNo", "1");// 成功
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				} else {
					map.put("errNo", "2");// 失败
					response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
				}
			}
		} else {
			String ruslt = "";
			String jobId = "";
			String url = "http://www.mxtong.net.cn/GateWay/Services.asmx/DirectSend?";
			HttpClient httpClient = new HttpClient();
			httpClient.getParams().setBooleanParameter("http.protocol.expect-continue", false);
			PostMethod getMethod = new PostMethod(url);
			String Content = java.net.URLEncoder
					.encode("您好，您本次注册的短信验证码是：" + randNum + "，验证码5分钟内有效，请尽快完成注册。" + signatureContent, "UTF-8");
			/*
			 * SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"
			 * );
			 */
			NameValuePair[] data = { new NameValuePair("UserID", UserID), new NameValuePair("Account", Account),
					new NameValuePair("Password", Password), new NameValuePair("Phones", userName),
					new NameValuePair("SendType", "1"), new NameValuePair("SendTime", ""),
					new NameValuePair("PostFixNumber", ""), new NameValuePair("Content", Content) };
			getMethod.setRequestBody(data);
			getMethod.addRequestHeader("Connection", "close");
			try {
				int statusCode = httpClient.executeMethod(getMethod);
				if (statusCode != HttpStatus.SC_OK) {
					System.err.println("Method failed: " + getMethod.getStatusLine());
				}
				byte[] responseBody = getMethod.getResponseBody();
				String str = new String(responseBody, "GBK");
				if (str.contains("GBK")) {
					str = str.replaceAll("GBK", "utf-8");
				}
				int beginPoint = str.indexOf("<RetCode>");
				int endPoint = str.indexOf("</RetCode>");
				int begJob = str.indexOf("<JobID>");
				int endJob = str.indexOf("</JobID>");
				jobId = str.substring(begJob + 7, endJob);
				ruslt = str.substring(beginPoint + 9, endPoint);
				// return getMethod.getResponseBodyAsString();
			} catch (HttpException e) {
				System.out.println(1);
			} catch (IOException e) {
				System.out.println(2);
			} finally {
				getMethod.releaseConnection();
			}
			int isSusses = -1;
			if (ruslt.equals("Sucess")) {
				isSusses = 1;
			} else {
				isSusses = 0;
			}
			Date senddate = new Date();
			AppSms record = new AppSms();
			record.setUserName(userName);// 手机号码
			record.setCityCode(cityCode);// 城市编码
			record.setJobId(jobId);// jobId
			record.setRequestTime(requerstDate);// 请求时间
			record.setVerificationCode(randNum);// 验证码
			record.setSendTime(senddate);// 发送短信时间
			record.setIsSuccess(isSusses);// 是否成功标识
			int addMsm = appMsmService.insertSelective(record);
			if (addMsm == 1) {
				map.put("errNo", "1");// 成功
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			} else {
				map.put("errNo", "2");// 失败
				response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
			}
		}
	}
	
}
