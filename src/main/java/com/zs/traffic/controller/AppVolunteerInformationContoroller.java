package com.zs.traffic.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.SourceVersion;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zs.traffic.model.*;
import com.zs.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.traffic.service.AppFamilyInformationService;
import com.zs.traffic.service.AppVolunteerInformationService;
import com.zs.traffic.service.ChargeService;
import com.zs.traffic.service.TrajectoryDateService;
import com.zs.traffic.service.TravelInformationService;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description 查询志愿者信息
 *
 * @author Antl
 *
 * @date 2016-06-18
 *
 */
@Controller
@RequestMapping("/")
public class AppVolunteerInformationContoroller extends BaseController {
	@Autowired
	public AppVolunteerInformationService auService;
	@Autowired
	public AppFamilyInformationService aaService;
	@Autowired
	public TravelInformationService acService;
	@Autowired
	public TrajectoryDateService avService;
	@Autowired
	public ChargeService asService;



	/**
	 * 查询志愿者信息
	 *
	 */
	@RequestMapping(value = "/getListVolunteers", method = RequestMethod.POST)
	public void getListVolunteers(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
//		System.out.println("##################################################################################"+request);

		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
//		System.out.println("##################################################################################"+servletRequest);
		// 判断权限Area
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");

		/* if(type.equals("2")){ */
		List<AppFamilyInformation> list = aaService.getListAppFamilyInformation(cityCode);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		int count = 0;
		// JSONArray array = new JSONArray(); 此为对象的集合 getDepartment
		if (list != null && list.size() > 0) {
			count = list.size();
			for (int j = 0; j < count; j++) {
				int number = j + 1;
				AppFamilyInformation entity = list.get(j);
				Integer status = entity.getIsChargeStatus();// 审核状态
				Integer sexType = entity.getSex();// 性别
				Integer age = entity.getAge();// 年龄
				Integer chargeType = entity.getChargeType();// 冲花费的类型
				Integer profession = entity.getProfession();// 职业
				Integer isPermanentAddress = entity.getIsPermanentAddress();// 是否常住本地
				Integer appuserId = entity.getAppuserId();// 用户id
				String createDate = entity.getCreateDate();// 注册时间
				Integer isTravelType = entity.getStatus();// 有无出行
				String isTravel = "无";
				if (isTravelType != null) {
					if (isTravelType == 1 || isTravelType == 2) {
						isTravel = "有";
					}
				}
				String isLocal = "";// 定义是否常住本地
				String appFormatWork = "";// 定义职业
				String sex = "";// 定义性别
				String atdus = "";// 定义审核状态
				String costType = "";// 定义用户的类型
				// 格式化工作
				if (profession != null) {
					appFormatWork = FormattingUtil.AppFormatWork(profession);
				}
				// 格式化是否常住本地
				if (isPermanentAddress != null) {
					if (isPermanentAddress == 0) {
						isLocal = "常住";
					} else if (isPermanentAddress == 1) {
						isLocal = "暂住";
					}
				}
				// 格式化性别
				if (sexType != null) {
					if (sexType == 0) {
						sex = "男";
					} else if (sexType == 1) {
						sex = "女";
					}

				}
				// 判断审核状态
				if (status != null) {
					if (status == -1) {
						atdus = "未审核";
					} else if (status == 0) {
						atdus = "未通过";
					} else if (status == 1) {
						atdus = "通过";
					} else {
						atdus = "未审核";
					}
				} else {
					atdus = "未审核";
				}
				// 判断话费充值类型
				if (chargeType != null) {
					if (chargeType == 1) {
						costType = "送20元";
					} else if (chargeType == 2) {
						costType = "送30元";
					} else if (chargeType == 3) {
						costType = "送50元";
					}
				}
				createDate = createDate.replace(".0", "");
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("isTravel", isTravel);// 是否有出行信息
				map.put("createDate", createDate);// 注册时间
				map.put("number", number);
				map.put("userName", entity.getUserName());
				map.put("sex", sex);
				map.put("userId", appuserId);
				map.put("age", age);
				map.put("appFormatWork", appFormatWork);
				map.put("isLocal", isLocal);
				map.put("atdus", atdus);
				map.put("costType", costType);
//				System.out.println("11111111111111111111111111111111111111111111############################################################################################################################");
				System.out.println(map);
				listResult.add(map);
			}
		}

//		System.out.println("2222222222222222222222222222222222222222222222222############################################################################################################################");
		System.out.println(listResult);


		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
	/**
	 * 删除用户
	 *
	 */
	@RequestMapping(value = "/deleteAPPUser", method = RequestMethod.POST)
	public void deleteAPPUser(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException{
		String id = request.getParameter("id");//id

		Map<String,Object>  map=new HashMap<String,Object>();
		int ret =0;
		ret = auService.deleteByPrimaryKey(Integer.parseInt(id));//通过主键id删除
		if(ret>0){
			map.put("errNo", "0");
			map.put("result", "删除成功");
		}
		else{
			map.put("errNo", "-1");
			map.put("message", "删除失败");
		}
		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	}


	/**
	 * 一键解绑用户
	 *
	 */
	@RequestMapping(value = "/deleteUserAllInformation", method = RequestMethod.POST)
	public void deleteUserAllInformation(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException{
		String start_date = request.getParameter("start_date");//startdate
		String end_date = request.getParameter("end_date");//enddate
		Map<String,Object>  map=new HashMap<String,Object>();
		int ret =0;
		List<Integer> array = auService.findDeleteUserAllInformation(start_date,end_date);//通过主键id删除
		for(int i=0;i<array.size();i++){
			//System.out.println("dsfhsdakjfhsjkldahgjklashfgjklash"+array.get(i));
			//deleteUserAllCharge(array.get(i));
			//auService.deleteUserAllCharge(array.get(i));
			auService.unbindByUserId(array.get(i));
			//auService.deleteUserThirdParty(array.get(i));

		}
		//ret = auService.deleteUserAllInformation(start_date,end_date);//通过主键id删除
		ret = auService.deleteUserAllInformation(start_date,end_date);//通过主键id删除
		if(ret>0){
			map.put("errNo", "0");
			map.put("result", "删除成功");
		}
		else{
			map.put("errNo", "-1");
			map.put("message", "删除失败");
		}
		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	}


	/**
	 * 单独解绑用户
	 *
	 */
	@RequestMapping(value = "/unbindByUserId", method = RequestMethod.POST)
	public void unbindByUserId(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException{
		String id = request.getParameter("id");//id

		Map<String,Object>  map=new HashMap<String,Object>();
		int ret =0;
		ret = auService.unbindByUserId(Integer.parseInt(id));//通过主键id删除
		if(ret>0){
			map.put("errNo", "0");
			map.put("result", "解绑成功");
		}
		else{
			map.put("errNo", "-1");
			map.put("message", "解绑失败");
		}
		response.getWriter().write(ZsJsonUtil.map2Json(map).toString());
	}

	@RequestMapping(value = "/findDeleteUserAllInformation", method = RequestMethod.POST)
	public List<Integer> findDeleteUserAllInformation(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException{
		String start_date = request.getParameter("start_date");//startdate
		String end_date = request.getParameter("end_date");//enddate
		Map<String,Object>  map=new HashMap<String,Object>();

		List<Integer> array = auService.findDeleteUserAllInformation(start_date,end_date);//通过主键id删除
		for(int i=0;i<array.size();i++){
			System.out.println(array.get(i));
			//deleteUserAllCharge(array.get(i));
			//auService.deleteUserAllCharge(array.get(i));
			auService.unbindByUserId(array.get(i));
		}

		System.out.println("controller"+array);
		return array;
	}

	public void deleteUserAllCharge(Integer appuser_id)
			throws IOException{
		int ret =0;
		ret = auService.deleteUserAllCharge(appuser_id);//通过主键id删除
	}
	/**
	 * 用户微信公众号注册
	 */
	@RequestMapping(value = "/registerWXUser", method = RequestMethod.POST)
	public void registerWXUser(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException{
		request.setCharacterEncoding("UTF-8");
		Map<String, Object> result = new HashMap<String, Object>();
		//获得注册信息如下------>建议增加username
		//注册时间
		String createDate = FormattingUtil.dateToString(new Date());
		//家庭地址
		String username = request.getParameter("user_name");
		//家庭地址
		String family_address = request.getParameter("family_address");
		//mac地址
		String mac_address = request.getParameter("mac_address").toUpperCase();
		//电动车
		String electric_bikes = request.getParameter("electric_bikes");
		//汽车
		String car_count = request.getParameter("car_count");
		//用户身份，比如户主
		String identity = request.getParameter("identity");
		//性别
		String sex = request.getParameter("sex");
		//年龄
		String age = request.getParameter("age");
		//薪水
		String salary = request.getParameter("salary");
		//职业
		String  occupation = request.getParameter("occupation");
		//是否常驻
		String isPermanentAddress = request.getParameter("isPermanentAddress");
		//手机号
		String telphone = request.getParameter("telphone");
		//openid---> 用于绑定用户与微信的关系，建表建议放在第三方表中
		String openid = request.getParameter("openId");
		//获得注册信息结束------>

		int ret = auService.register(username,family_address,mac_address,electric_bikes,
				car_count,identity,sex,age,salary,occupation,isPermanentAddress,telphone,openid);

		System.out.println("openid："+openid+"family_address："+family_address);

		result.put("result", ret);
		response.getWriter().write(ZsJsonUtil.map2Json(result).toString());

	}

	/**
	 * 查询审核结果
	 */
	@RequestMapping(value = "/getCheckResult", method = RequestMethod.POST)
	public void getCheckResult(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException{
		request.setCharacterEncoding("UTF-8");
		String openid = request.getParameter("openid").trim();// 获取openId，然后找到appuser的id，然后找到审核结果

		int ret = auService.getCheckResult(openid);

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", ret);
		response.getWriter().write(ZsJsonUtil.map2Json(result).toString());
	}
	/**
	 * 查询轨迹信息
	 */
	@RequestMapping(value = "/getTrajectory", method = RequestMethod.POST)
	public void getTrajectory(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException{
		request.setCharacterEncoding("UTF-8");
		String openid = request.getParameter("openid").trim();// 获取openId，然后找到appuser的id，然后找到轨迹
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();

		listResult = auService.getTrajectory(openid);

		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	/**
	 * 完善轨迹信息
	 */
	@RequestMapping(value = "/updateTrajectory", method = RequestMethod.POST)
	public void updateTrajectory(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException{
		request.setCharacterEncoding("UTF-8");
		//轨迹的id，用于唯一更新
		String id =  request.getParameter("id");
		//开始时间   精确到时分秒的时间
		String startDate = request.getParameter("startDate");
		//结束时间
		String endDate = request.getParameter("endDate");
		//开始地址
		String startAddress = request.getParameter("startAddress");
		//结束地址
		String endAddress = request.getParameter("endAddress");
		//出行方式
		String tripMode = request.getParameter("tripMode");
		//出行目的
		String tripObjective = request.getParameter("tripObjective");

		//获取字段结束,开始执行service
		int ret = auService.updateTrajectory(id,startDate,endDate,startAddress,endAddress,tripMode,tripObjective);

		//结果返回至前端：如果修改成功了  返回1
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", ret);
		response.getWriter().write(ZsJsonUtil.map2Json(result).toString());
	}

	/**
	 * 获取志愿者家庭住址
	 *
	 */
	@RequestMapping(value = "/getListHomeAddress", method = RequestMethod.POST)
	public void getListHomeAddress(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		String userId = request.getParameter("userId").trim();// 获取Id
		int appuserId = Integer.parseInt(userId);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		List<SysAppuser> list = auService.getListHomeAddress(appuserId);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list != null && list.size() > 0) {
			int count = list.size();
			for (int i = 0; i < count; i++) {
				SysAppuser entity = list.get(i);
				String familyAddress = entity.getFamilyAddress();// 家庭地址
				Integer electricBikes = entity.getElectricBikes();// 电动车数量
				Integer carCount = entity.getCarCount();// 汽车数量
				Integer homesCount = entity.getHomesCount();// 家庭成员数量
				if (familyAddress != null) {
					String[] splitAddress = familyAddress.split("\\(");
					if (splitAddress.length > 2) {
						familyAddress = splitAddress[0] + "(" + splitAddress[1];
						map.put("homeAddress", familyAddress);
					} else {
						familyAddress = splitAddress[0];
						map.put("homeAddress", familyAddress);
					}
				} else {
					map.put("homeAddress", "暂无家庭信息");
				}
				if (electricBikes != null) {
					map.put("electricBikes", electricBikes);
				} else {
					map.put("electricBikes", "");
				}
				if (carCount != null) {
					map.put("carCount", carCount);
				} else {
					map.put("carCount", "");
				}
				if (homesCount != null) {
					map.put("homesCount", homesCount);
				} else {
					map.put("homesCount", "");
				}
			}
		} else {
			map.put("homesCount", "系统异常");
		}
		listResult.add(map);
		System.out.println("========================");
		System.out.println(map.toString());
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
	/**
	 * 获取志愿者个人信息
	 *
	 */
	@RequestMapping(value = "/getListPersonal", method = RequestMethod.POST)
	public void getListPersonal(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		System.out.println("****************************************************");
		request.setCharacterEncoding("UTF-8");
		String userId = request.getParameter("userId").trim();// 获取Id
		int appuserId = Integer.parseInt(userId);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		AppFamilyInformation personal = aaService.getListPersonal(appuserId);
		if (personal != null) {
			Integer age = personal.getAge();// 年龄
			String Income = personal.getIncome();// 月收入
			Integer sexType = personal.getSex();// 性别
			Integer Address = personal.getIsPermanentAddress();// 是否常驻本地
			String companyAddress = personal.getCompanyAddress();// 公司地址
			Integer occ = personal.getProfession();// 职业
			String sex = "";
			sex = String.valueOf(sexType);// 将性别类型int转换为String
			sex = FormattingUtil.FormatSex(sex); // 格式化性别
			String formatWork = "";
			String oc = "";
			if (occ != null) {
				int occa = occ - 1;
				//int occa = occ ;
				oc = String.valueOf(occa);// 将工作类型int转换为String
				formatWork = FormattingUtil.FormatWork(oc);// 格式化工作
			} else if (occ == null) {
				oc = String.valueOf(occ);// 将工作类型int转换为String
				formatWork = FormattingUtil.FormatWork(oc);// 格式化工作
				// System.out.println(formatWork);
			}
			// 格式化月收入
			String MonthlyIncome = "";
			if (Income != null) {
				if (Income.equals("1")) {
					MonthlyIncome = "2000元以下";
				} else if (Income.equals("2")) {
					MonthlyIncome = "2000-3000元";
				} else if (Income.equals("3")) {
					MonthlyIncome = "3000-5000元";
				} else if (Income.equals("4")) {
					MonthlyIncome = "5000-8000元";
				} else if (Income.equals("5")) {
					MonthlyIncome = "8000-10000元";
				} else if (Income.equals("6")) {
					MonthlyIncome = "10000元以上";
				} else {
					MonthlyIncome = "无稳定收入";
				}
			}
			String isPermanentAddress = "";
			if (Address != null) {
				if (Address == 0) {
					isPermanentAddress = "常住";
				} else if (Address == 1) {
					isPermanentAddress = "暂住";
				} else {
					isPermanentAddress = "其他";
				}
			}
			if(companyAddress!=null){
				String[] splitAddress = companyAddress.split("\\(");

				if (splitAddress.length > 2) {
					companyAddress = splitAddress[0] + "(" + splitAddress[1];
				} else {
					companyAddress = splitAddress[0];
				}
			} else {
				companyAddress = "";
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("age", age); // 年龄
			map.put("companyAddress", companyAddress); // 公司地址
			map.put("MonthlyIncome", MonthlyIncome); // 月收入
			map.put("sex", sex); // 性别
			map.put("isPermanentAddress", isPermanentAddress); // 是否常住本地
			map.put("formatWork", formatWork); // 工作
			listResult.add(map);
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
		}
	}

	/*	*//**
	 * 获取志愿者出行信息
	 *
	 * @throws ParseException
	 *
	 */
	/*
	 * @RequestMapping(value = "/getTravelInformation", method =
	 * RequestMethod.POST) public void
	 * getTravelInformation(XssHttpServletRequestWrapper request,
	 * HttpServletResponse response) throws IOException, ParseException {
	 * request.setCharacterEncoding("UTF-8"); List<Map<String,Object>>
	 * listResult=new ArrayList<Map<String,Object>>(); String userId =
	 * request.getParameter("userId").trim();//获取Id int appuserId =
	 * Integer.parseInt(userId); Date date = new Date(); SimpleDateFormat
	 * timeConversion =new SimpleDateFormat("yyyy-MM-dd"); String format =
	 * timeConversion.format(date); Date parse = timeConversion.parse(format);
	 *
	 * List<TravelInformation> list = acService.getTravelInformation(appuserId);
	 * if (list != null && list.size() > 0) { int count = list.size(); for (int
	 * i = 0; i < count; i++) { Map<String, Object> map = new HashMap<String,
	 * Object>(); int number = i+1; TravelInformation entity = list.get(i); Date
	 * start = entity.getStartDate();//出发时间 Date end =
	 * entity.getEndDate();//结束时间 String startAddress =
	 * entity.getStartAddress();//出发地点 String endAddress =
	 * entity.getEndAddress();//结束地点 String tripMode =
	 * entity.getTripMode();//出行方式 String tripObjective =
	 * entity.getTripObjective();//出行目的 map.put("number", number);//序号
	 * map.put("startAddress", startAddress);//出发地点 map.put("endAddress",
	 * endAddress);//结束地点
	 *
	 * if(start !=null){ SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
	 * //时间格式转换 String starDate=sdf.format(start);//时间转化格式 map.put("starDate",
	 * starDate);//出发时间 } if(end !=null){ SimpleDateFormat sdf=new
	 * SimpleDateFormat("HH:mm:ss"); //时间格式转换 String
	 * endDate=sdf.format(end);//时间转化格式
	 *
	 * map.put("endDate", endDate);//结束时间
	 *
	 * } int length = tripMode.length();//出行方式的长度 if(length>=3){ String
	 * getSignInfo = tripMode.substring(tripMode.indexOf(":") +
	 * 1);//获取开始截取的位置，之后截取冒号后面的所有内容 map.put("tripMode", getSignInfo);//出行方式
	 *
	 *
	 * }else{ String tripModeo = FormattingUtil.tripMode(tripMode);
	 *
	 * map.put("tripMode", tripModeo);//出行方式
	 *
	 * } int lengtho = tripObjective.length(); if(lengtho>=2){ String
	 * getSignInfo = tripObjective.substring(tripObjective.indexOf(":") +
	 * 1);//获取开始截取的位置，之后截取冒号后面的所有内容
	 *
	 * map.put("tripObjective", getSignInfo);//出行目的
	 *
	 * }else{ String objective = FormattingUtil.tripObjective(tripObjective);
	 *
	 * map.put("tripObjective", objective);//出行目的
	 *
	 * } listResult.add(map); }
	 * response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	 *
	 *
	 * } }
	 */
	// 查询App出行记录
	@RequestMapping(value = "/getTravel", method = RequestMethod.POST)
	public void getTravelIn(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ParseException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		String userId = request.getParameter("userId").trim();
		int appuserId = Integer.parseInt(userId);
		SimpleDateFormat date_type = new SimpleDateFormat("yyyy-MM-dd");
		List<TravelInformation> list = acService.getTravelConditions(appuserId);
		if (list != null && list.size() > 0) {
			int count = list.size();
			for (int i = 0; i < count; i++) {
				TravelInformation entity = list.get(i);
				if (entity.getStatus() == 1) {
					Map<String, Object> map = new HashMap<String, Object>();
					int number = i + 1;
					Date start = entity.getStartDate();// 出发时间
					Date end = entity.getEndDate();// 结束时间
					String startAddress = entity.getStartAddress();// 出发地点
					String endAddress = entity.getEndAddress();// 结束地点
					String tripMode = entity.getTripMode();// 出行方式
					String tripObjective = entity.getTripObjective();// 出行目的
					map.put("number", number);// 序号
					map.put("startAddress", startAddress);// 出发地点
					map.put("endAddress", endAddress);// 结束地点
					map.put("status", entity.getStatus());// 状态
					map.put("note", entity.getRemark());// 备注
					map.put("id", entity.getId());
					if (start != null) {
						SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); // 时间格式转换
						String starDate = sdf.format(start);// 时间转化格式
						map.put("starDate", starDate);// 出发时间
					}
					if (end != null) {
						SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); // 时间格式转换
						String endDate = sdf.format(end);// 时间转化格式

						map.put("endDate", endDate);// 结束时间

					}
					if(tripMode!=null){
						int length = tripMode.length();// 出行方式的长度
						if (length >= 3) {
							String getSignInfo = tripMode.substring(tripMode.indexOf(":") + 1);// 获取开始截取的位置，之后截取冒号后面的所有内容
							map.put("tripMode", getSignInfo);// 出行方式

						} else {
							String tripModeo = FormattingUtil.tripMode(tripMode);

							map.put("tripMode", tripModeo);// 出行方式

						}
					}else {
						map.put("tripMode", "未填写");// 出行方式
					}
					if(tripObjective != null){
						int lengtho = tripObjective.length();
						if (lengtho >= 2) {
							String getSignInfo = tripObjective.substring(tripObjective.indexOf(":") + 1);// 获取开始截取的位置，之后截取冒号后面的所有内容

							map.put("tripObjective", getSignInfo);// 出行目的

						} else {
							String objective = FormattingUtil.tripObjective(tripObjective);

							map.put("tripObjective", objective);// 出行目的

						}
					}else {
						map.put("tripObjective", "未填写");// 出行目的
					}
					listResult.add(map);
				} /*
				 * else if(entity.getStatus()==2){ Map<String, Object> map =
				 * new HashMap<String, Object>();
				 * map.put("status",entity.getStatus());//状态 map.put("note",
				 * entity.getRemark());//备注 listResult.add(map); }
				 */
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));

	}

	/**
	 * 查询状态
	 */
	@RequestMapping(value = "/getTravelStatus", method = RequestMethod.POST)
	public void getTravelStatus(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ParseException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		String userId = request.getParameter("userId").trim();
		int appuserId = Integer.parseInt(userId);
		System.out.println("=======================================");
		List<TravelInformation> list = acService.getTravelConditions(appuserId);
		System.out.println("=======================================");
		if (list != null && list.size() > 0) {
			int count = list.size();
			for (int i = 0; i < count; i++) {
				TravelInformation entity = list.get(i);
				if (entity.getStatus() == 2) {
					map.put("status", entity.getStatus());// 状态
					map.put("note", entity.getRemark());// 备注

				} else if (entity.getStatus() == 1) {
					map.put("status", 1);// 状态
					map.put("note", "");// 备注
				} else {
					map.put("status", 0);// 状态
					map.put("note", "");// 备注
				}

			}
		} else {
			map.put("status", "");// 状态
			map.put("note", "");// 备注
		}
		listResult.add(map);
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));

	}

	/**
	 * 获取志愿者轨迹
	 *
	 * @throws ParseException
	 * @throws SQLException
	 *
	 */
	@RequestMapping(value = "/gettrajectory", method = RequestMethod.POST)
	public void gettrajectory(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ParseException, SQLException {
		request.setCharacterEncoding("UTF-8");
		String userId = request.getParameter("userId").trim();// 获取Id
		int appuserId = Integer.parseInt(userId);// 将id转化为int类型
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		List<String> travelTIme = aaService.getTravelTIme(Integer.parseInt(userId));
		try{
			travelTIme.get(0);
		} catch (NullPointerException e){
			System.out.println("没有出行轨迹");
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("lon", 1); // 没有轨迹点(表不存在)
			listResult.add(map);
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
			return;
		}
		System.out.println("======================================");
		System.out.println("travelTIme.get(0) = " + travelTIme.get(0));
		System.out.println("travelTIme.get(1) = " + travelTIme.get(1));
		System.out.println("======================================");
		String stime = travelTIme.get(0);// 获取开始时间
		String etime = travelTIme.get(1);// 获取结束时间
		/*
		 * String stime="2017-04-23 10:15:00"; String etime=
		 * "2017-04-23 10:22:00";
		 */
		Date sstime = null;
		Date eetime = null;
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 转换时间对象
		try {
			sstime = sf.parse(stime);// 转换时间
			eetime = sf.parse(etime);
			/*
			 * System.out.println(s); System.out.println(aa);
			 */} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(sstime);
		System.out.println(eetime);
		SimpleDateFormat sfa = new SimpleDateFormat("yyyy_MM_dd");
		String format = sfa.format(sstime);
		System.out.println("========================");
		System.out.println(format);
		System.out.println("========================");
		// System.out.println(format);
		String tableName = "trajectory_" + format;// 获取表名
		Date endDate = eetime;
		Date startDate = sstime;
		/* String upDate =""; */
		SimpleDateFormat af = new SimpleDateFormat("yyyy-MM-dd");
		String data = af.format(sstime);
		String starTime = "12:00:00";
		String starTime_point = data + " " + starTime;
		Date point = sf.parse(starTime_point);// 定义的12点整参数
		SimpleDateFormat bf = new SimpleDateFormat("yyyyMMddHHmmss");
		String compare_stime = bf.format(sstime).trim();
		String compare_time = bf.format(point).trim();
		int to = compare_stime.compareTo(compare_time);
		Connection conn = JdbcUtil.getConnection();
		ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);//判断表是否存在
		if(!rs.next()){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("lon", 1); // 没有轨迹点(表不存在)
			listResult.add(map);
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
			return;
		}
		int compareTo = 0;
		if (to < 0) {// 根据to的大小判断是上午还是下午
			String compare_etime = bf.format(endDate).trim();
			String compare_atime = bf.format(point).trim();
			compareTo = compare_etime.compareTo(compare_atime);
		}
		if (compareTo <= 0) {// 判断时间段调用不同的方法
			List<TrajectoryDate> list = avService.gettrajectory(appuserId, startDate, endDate, tableName);
			// System.out.println(list);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					TrajectoryDate entity = list.get(i);
					String trajectoryGps = entity.getTrajectoryGps();
					if (trajectoryGps != null) {
						String[] split = trajectoryGps.split(";");// 字符串按逗号切分
						for (String string : split) {
							// System.out.println(string);
							String[] splita = string.split(",");// 字符串按逗号切分
							Map<String, Object> map = new HashMap<String, Object>();
							String lon = splita[0];
							String lat = splita[1];
							System.out.println("一整天的点");
							map.put("lon", lon);
							map.put("lat", lat);
							map.put("name", 3);// 一天整天的点
							listResult.add(map);
						}
					}
					// System.out.println("gps:"+trajectoryGps);
				}
			} else {
				if (listResult.size() < 1) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("lon", 1); // 没有轨迹点
					listResult.add(map);
				}
			}
		} else {
			List<TrajectoryDate> list = avService.getTrajectoryMorning(appuserId, startDate, point, tableName);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					TrajectoryDate entity = list.get(i);
					String trajectoryGps = entity.getTrajectoryGps();
					if (trajectoryGps != null) {
						String[] split = trajectoryGps.split(";");// 字符串按逗号切分
						for (String string : split) {
							// System.out.println(string);
							String[] splita = string.split(",");// 字符串按逗号切分
							Map<String, Object> map = new HashMap<String, Object>();
							String lon = splita[0];
							String lat = splita[1];
							map.put("lon", lon);
							map.put("lat", lat);
							map.put("name", 1);// 上午的轨迹点
							listResult.add(map);
						}
					}
				}
			} /*else {
				if (listResult.size() < 1) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("lon", 2); // 没有轨迹点
					listResult.add(map);
				}
			}*/
			List<TrajectoryDate> listTrajectoryDate = avService.getTrajectoryAfternoon(appuserId, point, endDate,
					tableName);
			if (listTrajectoryDate != null && listTrajectoryDate.size() > 0) {
				for (int i = 0; i < listTrajectoryDate.size(); i++) {
					TrajectoryDate entity = listTrajectoryDate.get(i);
					String trajectoryGps = entity.getTrajectoryGps();
					if (trajectoryGps != null) {
						String[] split = trajectoryGps.split(";");// 字符串按逗号切分
						for (String string : split) {
							// System.out.println(string);
							String[] splita = string.split(",");// 字符串按逗号切分

							Map<String, Object> map = new HashMap<String, Object>();
							String lon = splita[0];
							String lat = splita[1];
							System.out.println(lon+","+lat);
							map.put("lon", lon);
							map.put("lat", lat);
							map.put("name", 2);// 下午的轨迹点
							listResult.add(map);
							/*
							 * System.out.println("经度:"+lon);
							 * System.out.println("纬度:"+lat);
							 */
						}
					}
					// System.out.println("gps:"+trajectoryGps);
				}
			} else {
				// renxw, 2017-06-16修改
				if (listResult.size() < 1) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("lon", 3); // 没有轨迹点
					listResult.add(map);
				}
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	/**
	 * 获取志愿者轨迹（app使用）
	 *
	 * @throws ParseException
	 * @throws SQLException
	 *
	 */
	@RequestMapping(value = "/app/gettrajectory_app", method = RequestMethod.POST)
	public void gettrajectory_app(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ParseException, SQLException {
		request.setCharacterEncoding("UTF-8");
		String userId = request.getParameter("userId").trim();// 获取Id
		int appuserId = Integer.parseInt(userId);// 将id转化为int类型
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		String stime = request.getParameter("stime");// 获取开始时间
		String etime = request.getParameter("etime");// 获取结束时间
		/*
		 * String stime="2017-04-23 10:15:00"; String etime=
		 * "2017-04-23 10:22:00";
		 */
		Date sstime = null;
		Date eetime = null;
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 转换时间对象
		try {
			sstime = sf.parse(stime);// 转换时间
			eetime = sf.parse(etime);
			/*
			 * System.out.println(s); System.out.println(aa);
			 */} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleDateFormat sfa = new SimpleDateFormat("yyyy_MM_dd");
		String format = sfa.format(sstime);
		// System.out.println(format);
		String tableName = "trajectory_" + format;// 获取表名
		Date endDate = eetime;
		Date startDate = sstime;
		/* String upDate =""; */
		SimpleDateFormat af = new SimpleDateFormat("yyyy-MM-dd");
		String data = af.format(sstime);
		String starTime = "12:00:00";
		String starTime_point = data + " " + starTime;
		Date point = sf.parse(starTime_point);// 定义的12点整参数
		SimpleDateFormat bf = new SimpleDateFormat("yyyyMMddHHmmss");
		String compare_stime = bf.format(sstime).trim();
		String compare_time = bf.format(point).trim();
		int to = compare_stime.compareTo(compare_time);
		int compareTo = 0;
		if (to < 0) {// 根据to的大小判断是上午还是下午
			String compare_etime = bf.format(endDate).trim();
			String compare_atime = bf.format(point).trim();
			compareTo = compare_etime.compareTo(compare_atime);
		}
		if (compareTo <= 0) {// 判断时间段调用不同的方法
			List<TrajectoryDate> list = avService.gettrajectory(appuserId, startDate, endDate, tableName);
			// System.out.println(list);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					TrajectoryDate entity = list.get(i);
					String trajectoryGps = entity.getTrajectoryGps();
					if (trajectoryGps != null) {
						String[] split = trajectoryGps.split(";");// 字符串按分号切分
						for (String string : split) {
							// System.out.println(string);
							String[] splita = string.split(",");// 字符串按逗号切分
							Map<String, Object> map = new HashMap<String, Object>();
							String lon = splita[1];
							String lat = splita[2];
							map.put("lon", lon);
							map.put("lat", lat);
							map.put("name", 3);// 一天整天的点
							listResult.add(map);
						}
					}
					// System.out.println("gps:"+trajectoryGps);
				}
			} else {
				if (listResult.size() < 1) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("lon", 1); // 没有轨迹点
					listResult.add(map);
				}
			}
		} else {
			List<TrajectoryDate> list = avService.getTrajectoryMorning(appuserId, startDate, point, tableName);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					TrajectoryDate entity = list.get(i);
					String trajectoryGps = entity.getTrajectoryGps();
					if (trajectoryGps != null) {
						String[] split = trajectoryGps.split(";");// 字符串按逗号切分
						for (String string : split) {
							String[] splita = string.split(",");// 字符串按逗号切分
							Map<String, Object> map = new HashMap<String, Object>();
							String lon = splita[1];
							String lat = splita[2];
							map.put("lon", lon);
							map.put("lat", lat);
							map.put("name", 1);// 上午的轨迹点
							listResult.add(map);

						}
					}
					// System.out.println("gps:"+trajectoryGps);
				}
			} else {
				if (listResult.size() < 1) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("lon", 2); // 没有轨迹点
					listResult.add(map);
				}
			}

			List<TrajectoryDate> listTrajectoryDate = avService.getTrajectoryAfternoon(appuserId, point, endDate,
					tableName);
			if (listTrajectoryDate != null && listTrajectoryDate.size() > 0) {
				for (int i = 0; i < listTrajectoryDate.size(); i++) {
					TrajectoryDate entity = listTrajectoryDate.get(i);

					String trajectoryGps = entity.getTrajectoryGps();
					if (trajectoryGps != null) {
						String[] split = trajectoryGps.split(";");// 字符串按逗号切分
						for (String string : split) {
							// System.out.println(string);
							String[] splita = string.split(",");// 字符串按逗号切分
							Map<String, Object> map = new HashMap<String, Object>();
							String lon = splita[1];
							String lat = splita[2];
							map.put("lon", lon);
							map.put("lat", lat);
							map.put("name", 2);// 下午的轨迹点
							listResult.add(map);
						}
					}
					// System.out.println("gps:"+trajectoryGps);
				}
			} else {
				// renxw, 2017-06-16修改
				if (listResult.size() < 1) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("lon", 3); // 没有轨迹点
					listResult.add(map);
				}
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));

	}

	// 查询审核状态
	@RequestMapping(value = "/selectAppaudit", method = RequestMethod.POST)
	public void selectAppaudit(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		String userId = request.getParameter("userId").trim();
		Charge charge = asService.selectAppaudit(Integer.parseInt(userId));
		String adtus = "";
		if (charge != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			Integer isChargeStatus = charge.getIsChargeStatus();
			if (isChargeStatus == -1) {
				adtus = "未审核";
			} else if (isChargeStatus == 1) {
				adtus = "通过";
			} else if (isChargeStatus == 0) {
				adtus = "不通过";
			}
			map.put("adtus", adtus);
			listResult.add(map);
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("adtus", "");
			listResult.add(map);
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 更新审核状态
	@RequestMapping(value = "/udateStatus", method = RequestMethod.POST)
	public void udateStatus(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		Integer isSuperAdmin = (Integer) session.getAttribute("isSuperAdmin");// 获取用户是否为超级管理员
		if (isSuperAdmin == null) {
			isSuperAdmin = 0;
		}
		Integer userId = (Integer) session.getAttribute("userId");// 获取用户id
		String authDegrees = (String) session.getAttribute("authDegree");// 获取用户权限
		String userName = (String) session.getAttribute("userName");// 获取用户名
		String authDegree = "";
		String[] array = null;
		String appuserId = request.getParameter("userId").trim();
		String status = request.getParameter("audit").trim();
		if (isSuperAdmin == 1) {
			Map<String, Object> map = new HashMap<String, Object>();
			Charge entity = asService.selectAppaudit(Integer.parseInt(appuserId));
			if (entity == null) {
				map.put("back", 3);
				listResult.add(map);
				response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
				return;
			}
			String chance = entity.getExtended2();
			String opportunity = "";
			if (chance == null) {
				opportunity = "1";
			} else if (chance.equals("1")) {
				opportunity = "2";
			} else if (chance.equals("2")) {
				opportunity = "2";
			}
			int back = asService.Appaudit(Integer.parseInt(appuserId), Integer.parseInt(status), opportunity);

			if (back > 0) {
				map.put("back", 1);
				listResult.add(map);
			} else {
				map.put("back", 0);
				listResult.add(map);
			}
		} else {
			Charge entity = asService.selectAppaudit(Integer.parseInt(appuserId));
			Map<String, Object> map = new HashMap<String, Object>();
			if (entity == null) {
				map.put("back", 3);
				listResult.add(map);
				response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
				return;
			}
			String opportunity = "";
			String chance = entity.getExtended2();
			if (chance == null) {
				opportunity = "1";
			} else if (chance.equals("1")) {
				opportunity = "2";
			} else if (chance.equals("2")) {
				opportunity = "2";
			}
			if (entity.getExtended2() == null || entity.getExtended2().equals("1")) {
				int back = asService.Appaudit(Integer.parseInt(appuserId), Integer.parseInt(status), opportunity);

				if (back > 0) {
					map.put("back", 1);
					listResult.add(map);
				} else {
					map.put("back", 0);
					listResult.add(map);
				}
			} else {

				map.put("back", 2);
				listResult.add(map);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 条件查询
	@RequestMapping(value = "/ConditionsQuery", method = RequestMethod.POST)
	public void ConditionsQuery(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		// 判断权限Area
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");
		String sex_type = request.getParameter("sex");// 性别
		String starAge_type = request.getParameter("starAge");// 起始年龄
		String endAge_type = request.getParameter("endAge");// 结束年龄
		String userName = request.getParameter("userName");// 用户名
		String Islocal = request.getParameter("isLocal");// 是否常住
		String work_type = request.getParameter("work");// 职业
		int sex_pata = -1;
		int starAge = -1;
		int endAge = -1;
		int local = -1;
		int work = -1;
		if (!sex_type.equals("")) {
			sex_type.trim();
			sex_pata = Integer.parseInt(sex_type);
		}
		if (!starAge_type.equals("")) {
			starAge_type.trim();
			starAge = Integer.parseInt(starAge_type);
		}
		if (!endAge_type.equals("")) {
			endAge_type.trim();
			endAge = Integer.parseInt(endAge_type);
		}
		if (!Islocal.equals("")) {
			Islocal.trim();
			local = Integer.parseInt(Islocal);
		}
		if (!work_type.equals("")) {
			work_type.trim();
			work = Integer.parseInt(work_type);
		}
		if (userName.equals("")) {
			userName = null;
		}
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();

		List<AppFamilyInformation> list = aaService.selectAppuser(starAge, endAge, work, sex_pata, local, userName,
				cityCode);
		int count = 0;
		// JSONArray array = new JSONArray(); 此为对象的集合 getDepartment
		if (list != null && list.size() > 0) {
			count = list.size();
			for (int j = 0; j < count; j++) {
				int number = j + 1;
				AppFamilyInformation entity = list.get(j);
				Integer status = entity.getIsChargeStatus();// 审核状态
				Integer sexType = entity.getSex();// 性别
				Integer age = entity.getAge();// 年龄
				Integer chargeType = entity.getChargeType();// 冲花费的类型
				Integer profession = entity.getProfession();// 职业
				Integer isPermanentAddress = entity.getIsPermanentAddress();// 是否常住本地
				Integer appuserId = entity.getAppuserId();
				String createDate = entity.getCreateDate();
				Integer isTravelType = entity.getStatus();// 有无出行
				String isTravel = "无";
				if (isTravelType != null) {
					if (isTravelType == 1 || isTravelType == 2) {
						isTravel = "有";
					}
				}
				String isLocal = "";// 定义是否常住本地
				String appFormatWork = "";// 定义职业
				String sex = "";// 定义性别
				String atdus = "";// 定义审核状态
				String costType = "";// 定义用户的类型
				// 格式化工作
				if (profession != null) {
					appFormatWork = FormattingUtil.AppFormatWork(profession);

				}
				// 格式化是否常住本地
				if (isPermanentAddress != null) {
					if (isPermanentAddress == 0) {
						isLocal = "常住";
					} else if (isPermanentAddress == 1) {
						isLocal = "暂住";
					}

				}
				// 格式化性别
				if (sexType != null) {
					if (sexType == 0) {
						sex = "男";
					} else if (sexType == 1) {
						sex = "女";
					}
				}
				// 判断审核状态
				if (status != null) {
					if (status == -1) {
						atdus = "未审核";
					} else if (status == 0) {
						atdus = "未通过";
					} else if (status == 1) {
						atdus = "通过";
					} else {
						atdus = "未审核";
					}
				} else {
					atdus = "未审核";
				}
				// 判断话费充值类型
				if (chargeType != null) {
					if (chargeType == 1) {
						costType = "送20元";
					} else if (chargeType == 2) {
						costType = "送30元";
					} else if (chargeType == 3) {
						costType = "送50元";
					}
				}
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("isTravel", isTravel);
				map.put("createDate", createDate);
				map.put("number", number);
				map.put("userName", entity.getUserName());
				map.put("sex", sex);
				map.put("userId", appuserId);
				map.put("age", age);
				map.put("appFormatWork", appFormatWork);
				map.put("isLocal", isLocal);
				map.put("atdus", atdus);
				map.put("costType", costType);
				listResult.add(map);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	/**
	 * 用户统计
	 *
	 */
	@RequestMapping(value = "/appUserStatistical", method = RequestMethod.POST)
	public void appUserStatistical(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		// 判断权限Area
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");
		String sex_type = request.getParameter("sex");// 性别
		String starAge_type = request.getParameter("starAge");// 起始年龄
		String endAge_type = request.getParameter("endAge");// 结束年龄
		String userName = request.getParameter("userName");// 用户名
		String Islocal = request.getParameter("isLocal");// 是否常住
		String work_type = request.getParameter("work");// 职业
		System.out.println("cityCode = " + cityCode);
		System.out.println("sex_type = " + sex_type);
		System.out.println("starAge_type = " + starAge_type);
		System.out.println("endAge_type = " + endAge_type);
		System.out.println("userName = " + userName);
		System.out.println("Islocal = " + Islocal);
		System.out.println("work_type = " + work_type);
		int sex_pata = -1;
		int starAge = -1;
		int endAge = -1;
		int local = -1;
		int work = -1;
		if (null == sex_type) {
			sex_pata = -1;
		} else {
			if (sex_type.trim().equals("")) {
				sex_pata = -1;
			} else {
				sex_pata = Integer.parseInt(sex_type.trim());
			}
		}
		if (null == starAge_type) {
			starAge = -1;
		} else {
			if (starAge_type.trim().equals("")) {
				starAge = -1;
			} else {
				starAge = Integer.parseInt(starAge_type.trim());
			}
		}
		if (null == endAge_type) {
			endAge = -1;
		} else {
			if (endAge_type.trim().equals("")) {
				endAge = -1;
			} else {
				endAge = Integer.parseInt(endAge_type.trim());
			}
		}

		if (null == Islocal) {
			local = -1;
		} else {
			if (Islocal.trim().equals("")) {
				local = -1;
			} else {
				local = Integer.parseInt(Islocal.trim());
			}
		}
		if (null == work_type) {
			work = -1;
		} else {
			if (work_type.trim().equals("")) {
				work = -1;
			} else {
				work = Integer.parseInt(work_type.trim());
			}
		}
		if (null != userName) {
			if (userName.equals("")) {
				userName = null;
			}
		}
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		int appUserTotal = 0;// 用户信息总量
		int noAudit = 0;// 未审核
		int yesAudit = 0;// 审核通过
		int noneAudit = 0;// 审核未通过的
		int twentyNoAudit = 0;// 20元未审核
		int twentyYesAudit = 0;// 20元审核通过的
		int twentyNoneAudit = 0;// 20元审核通过的
		int thirtyNoAudit = 0;// 30元未审核的
		int thirtyYesAudit = 0;// 30元审核通过的
		int thirtyNoneAudit = 0;// 30元审核通过的
		int fiftyNoAudit = 0;// 50元未审核的
		int fiftyYesAudit = 0;// 50元审核通过的
		int fiftyNoneAudit = 0;// 50元审核通过的

		List<AppFamilyInformation> list = aaService.selectAppuser(starAge, endAge, work, sex_pata, local, userName,
				cityCode);
		System.out.println(list.size());
		int count = 0;
		// JSONArray array = new JSONArray(); 此为对象的集合 getDepartment
		if (list != null && list.size() > 0) {
			appUserTotal = count = list.size();
			for (int j = 0; j < count; j++) {
				int number = j + 1;
				AppFamilyInformation entity = list.get(j);
				Integer status = entity.getIsChargeStatus();// 审核状态
				Integer chargeType = entity.getChargeType();// 冲花费的类型
				if (null == status) {
					status = -1;
				}
				if (null == chargeType) {
					chargeType = 1;
				}
				if (status == 0) {
					// atdus="未通过";
					noneAudit++;
					if (1 == chargeType) {
						twentyNoneAudit++;
					} else if (2 == chargeType) {
						thirtyNoneAudit++;
					} else if (3 == chargeType) {
						fiftyNoneAudit++;
					}

				} else if (status == 1) {
					// atdus="通过";
					yesAudit++;
					if (1 == chargeType) {
						twentyYesAudit++;
					} else if (2 == chargeType) {
						thirtyYesAudit++;
					} else if (3 == chargeType) {
						fiftyYesAudit++;
					}
				} else {
					// atdus="未审核";
					noAudit++;
					if (1 == chargeType) {
						twentyNoAudit++;
					} else if (2 == chargeType) {
						thirtyNoAudit++;
					} else if (3 == chargeType) {
						fiftyNoAudit++;
					}
				}
			}
		}
		map.put("appUserTotal", appUserTotal); // 用户信息总量
		map.put("noAudit", noAudit); // 未审核
		map.put("yesAudit", yesAudit); // 审核通过
		map.put("noneAudit", noneAudit); // 审核未通过的
		map.put("twentyNoAudit", twentyNoAudit); // 20元未审核
		map.put("twentyYesAudit", twentyYesAudit); // 20元审核通过的
		map.put("twentyNoneAudit", twentyNoneAudit); // 20元审核未通过的
		map.put("thirtyNoAudit", thirtyNoAudit); // 30元未审核的
		map.put("thirtyYesAudit", thirtyYesAudit); // 30元审核通过的
		map.put("thirtyNoneAudit", thirtyNoneAudit); // 30元审核未通过的
		map.put("fiftyNoAudit", fiftyNoAudit); // 50元未审核的
		map.put("fiftyYesAudit", fiftyYesAudit); // 50元审核通过的
		map.put("fiftyNoneAudit", fiftyNoneAudit); // 50元审核未通过的
		listResult.add(map);
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	// 用户详情
	@RequestMapping(value = "/detailsInformation ", method = RequestMethod.POST)
	public void detailsInformation(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException, ParseException {
		request.setCharacterEncoding("UTF-8");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		String id = request.getParameter("id").trim();

		TravelInformation entity = acService.selectByPrimaryKey(Integer.parseInt(id));
		if (entity != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			Date start = entity.getStartDate();// 出发时间
			Date end = entity.getEndDate();// 结束时间
			String startAddress = entity.getStartAddress();// 出发地点
			String endAddress = entity.getEndAddress();// 结束地点
			String tripMode = entity.getTripMode();// 出行方式
			String tripObjective = entity.getTripObjective();// 出行目的
			map.put("startAddress", startAddress);// 出发地点
			map.put("endAddress", endAddress);// 结束地点
			Integer isSendChildren = entity.getIsSendChildren();// 是否接送小孩
			String sendAdress = entity.getSendAdress();// 接送小孩地址
			String address = null;
			if (sendAdress != null) {
				String[] splitAddress = sendAdress.split("\\(");
				if (splitAddress.length > 2) {
					address = splitAddress[0] + "(" + splitAddress[1];
				} else {
					address = splitAddress[0];
				}
			} else {
				address = "";
			}
			Date sendDate = entity.getSendDate();// 接送小孩时间
			String sendTime = null;
			if (sendDate != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); // 时间格式转换
				sendTime = sdf.format(sendDate);// 时间转化格式
			} else {
				sendTime = "";
			}
			if (isSendChildren != null) {
				if (isSendChildren == 0) {
					map.put("children", "否");
				} else if (isSendChildren == 1) {
					map.put("children", "接小孩");
					map.put("sendTime", "接小孩时间：" + sendTime);
					map.put("address", "接小孩地址：" + address);
				} else if (isSendChildren == 2) {
					map.put("children", "送小孩");
					map.put("sendTime", "送小孩时间：" + sendTime);
					map.put("address", "送小孩地址：" + address);
				}
			} else {
				map.put("children", "否");
			}
			if (start != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); // 时间格式转换
				String starDate = sdf.format(start);// 时间转化格式
				map.put("starDate", starDate);// 出发时间
			}
			if (end != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); // 时间格式转换
				String endDate = sdf.format(end);// 时间转化格式
				map.put("endDate", endDate);// 结束时间
			}
			if(tripMode!=null){
				int length = tripMode.length();// 出行方式的长度
				if (length >= 3) {
					String getSignInfo = tripMode.substring(tripMode.indexOf(":") + 1);// 获取开始截取的位置，之后截取冒号后面的所有内容
					map.put("tripMode", getSignInfo);// 出行方式
				} else {
					String tripModeo = FormattingUtil.tripMode(tripMode);
					map.put("tripMode", tripModeo);// 出行方式
				}
			}else {
				map.put("tripMode", "未填写");// 出行方式
			}
			if(tripObjective != null){
				int lengtho = tripObjective.length();
				if (lengtho >= 2) {
					String getSignInfo = tripObjective.substring(tripObjective.indexOf(":") + 1);// 获取开始截取的位置，之后截取冒号后面的所有内容
					map.put("tripObjective", getSignInfo);// 出行目的
				} else {
					String objective = FormattingUtil.tripObjective(tripObjective);
					map.put("tripObjective", objective);// 出行目的
				}
			}else {
				map.put("tripObjective", "未填写");// 出行目的
			}
			listResult.add(map);

			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
		}
	}
	//修改调查员用户信息
	@RequestMapping(value = "/changeInformation ", method = RequestMethod.POST)
	public void changeInformation(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		String userId = request.getParameter("userId");
		String username = request.getParameter("username");
		String age = request.getParameter("age");
		String sex = request.getParameter("sex");
		String isLocal = request.getParameter("isLocal");
		String address = request.getParameter("address");
		String car = request.getParameter("car");
		String bike = request.getParameter("bike");
		String number = request.getParameter("number");
		String income = request.getParameter("income");
		String profession = request.getParameter("profession");
		String macAddress = request.getParameter("macAddress");
		System.out.println("==================");
		System.out.println("username = " + username);
		System.out.println("age = " + age);
		System.out.println("sex = " + sex);
		System.out.println("isLocal = " + isLocal);
		System.out.println("address = " + address);
		System.out.println("car = " + car);
		System.out.println("bike = " + bike);
		System.out.println("number = " + number);
		System.out.println("income = " + income);
		System.out.println("profession = " + profession);
		System.out.println("macAddress = " + macAddress);
		int result = auService.changeVolunteerInformation(userId, username, age, sex, isLocal, address, car, "1", number, income, profession, macAddress);
		//System.out.println("rjgreqtoqohsjkdahfgjklahgjklhhjkl"+result);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Map<String,Object> map = new HashMap<>();
		map.put("result",result);
		listResult.add(map);
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));

	}
	@RequestMapping(value = "/getMacAddress", method = RequestMethod.POST)
	public void getMacAddress(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException{
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		String userId = request.getParameter("userId");
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Map<String,Object> map = new HashMap<>();
		String macAddress = auService.getMacAddress(userId);
		map.put("macAddress",macAddress);
		listResult.add(map);
		System.out.println("========================================================================");
		System.out.println(macAddress);
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
}
