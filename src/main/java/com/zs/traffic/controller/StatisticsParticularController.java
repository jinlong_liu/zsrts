package com.zs.traffic.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.traffic.model.Area;
import com.zs.traffic.model.StatisticsParticular;
import com.zs.traffic.service.AreaService;
import com.zs.traffic.service.StatisticsParticularService;
import com.zs.utils.XssHttpServletRequestWrapper;
import com.zs.utils.ZsJsonUtil;

/**
 * @description 查询问卷详情
 * 
 * @author Antl
 * 
 * @date 2017-04-18
 *
 */

@Controller
@RequestMapping("/")
public class StatisticsParticularController extends BaseController {
	@Autowired
	public StatisticsParticularService aaService;
	@Autowired
	public AreaService areaService;
	/**
	 * 查询问卷详情
	 * 
	 */
	@RequestMapping(value = "/getListStatisticsParticular", method = RequestMethod.POST)
	public void getTest(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		// 判断登录人权限人员权限
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		Integer isSuperAdmin = (Integer) session.getAttribute("isSuperAdmin");// 获取用户是否为超级管理员
		if (isSuperAdmin == null) {
			isSuperAdmin = 0;
		}
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		Integer userId = (Integer) session.getAttribute("userId");// 获取用户id
		String authDegrees = (String) session.getAttribute("authDegree");// 获取用户权限
		String userName = (String) session.getAttribute("userName");// 获取用户名z
		String offsetType = request.getParameter("offset");// 获取分页参数
		String limitType = request.getParameter("limit");// 获取分页参数
		int limit = Integer.parseInt(limitType);
		int offset = Integer.parseInt(offsetType);
		String authDegree = "";
		String[] array = null;
		if (authDegrees != null) {
			array = authDegrees.split(",");
			Boolean flag = true;
			for (String auth : array) {
				if (auth.equals("3") || auth.equals("4") || auth.equals("5") || auth.equals("6") || auth.equals("7")) {
					authDegree = auth;
				}
			}
		}
		List<Area> listArea = new ArrayList<Area>();
		List<String> areaNumber = new ArrayList<String>();
		if (authDegree.equals("4")) {// 一级负责人
			listArea = areaService.getAreaByuserId(userId, 1);// 根据用户id和type获取当前登录用户的管辖区域
			for (int i = 0; i < listArea.size(); i++) {
				areaNumber.add(listArea.get(i).getDistrictNumber());
			}

		} else if (authDegree.equals("5")) {// 二级负责人
			listArea = areaService.getAreaByuserId(userId, 2);// 根据用户id和type获取当前登录用户的管辖区域
			for (int i = 0; i < listArea.size(); i++) {
				areaNumber.add(listArea.get(i).getSubofficeNumber());
			}
		} else if (authDegree.equals("6")) {// 三级负责人
			listArea = areaService.getAreaByuserId(userId, 3);// 根据用户id和type获取当前登录用户的管辖区域
			for (int i = 0; i < listArea.size(); i++) {
				areaNumber.add(listArea.get(i).getCommunityNumber());
			}
		}
		if (isSuperAdmin == 1) {
			List<StatisticsParticular> list = aaService.getStatisticsParticularList(cityCode, limit, offset);
			List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
			int totall = aaService.selecttotalAll(cityCode);// 问卷总条数
			int count = 0;
			int index = 1;
			if (list != null && list.size() > 0) {
				count = list.size();
				for (int j = 0; j < count; j++) {
					int number = index++;
					String date = list.get(j).getSubmitDate();
					/*
					 * String dates=null; if(date !=null){ SimpleDateFormat
					 * sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //时间格式转换
					 * String sDate=sdf.format(date); dates = sDate;//时间格式转换 }
					 */
					StatisticsParticular entity = list.get(j);
					String travelTotal = entity.getExtendedFieldTwo();
					String total = "";
					int pate = 0;
					String length = entity.getExtendedFieldOne();
					Map<String, Object> map = new HashMap<String, Object>();
					if (length == null || length.equals("")) {
						map.put("travelTotal", "暂无");
					} else {
						pate = Integer.parseInt(length);
						// System.out.println(
						// entity.getAreaId()+"----"+dates+"---"+number);
						if (travelTotal != null && !travelTotal.equals("")) {
							String[] split = travelTotal.split(",");

							for (int i = 0; i < split.length; i++) {
								String[] travel_total = split[i].split(":");
								if (travel_total[1].equals("99")) {
									total = total + "0，";
								} else {
									total = total + travel_total[1] + "，";

								}
							}
							total = total.substring(0, total.length() - 1);
							map.put("travelTotal", total);
						} else {
							map.put("travelTotal", "暂无");
						}
					}
					if (entity.getExtendedFieldOne() != null && !entity.getExtendedFieldOne().equals("")) {
						map.put("familyMembers", entity.getExtendedFieldOne()); // 家庭成员数量
					} else {
						map.put("familyMembers", "暂无"); // 家庭成员数量
					}
					if (null == entity.getStatus() || -1 == entity.getStatus()) {
						map.put("status", "未审核");
					} else if (0 == entity.getStatus()) {
						map.put("status", "未通过");
					} else if (1 == entity.getStatus()) {
						map.put("status", "通过");
					}
					String positionDeviate = entity.getPositionDeviate();
					String deviation = null;
					boolean flag = false;
					if (positionDeviate != null) {
						for (int i = 0; i < positionDeviate.length(); i++) {
							// 获取相应的字符
							char bb = positionDeviate.charAt(i);
							if (bb == '.') {
								flag = true;
								break;
							}
						}
						if (flag) {
							String[] splitaOne = positionDeviate.split("\\.");

							String deviationType = splitaOne[0];
							int parseInt = Integer.parseInt(deviationType);
							if (parseInt >= 1000) {
								deviation = "异常";
							} else {
								deviation = "正常";
							}

						} else {
							int parseInt = Integer.parseInt(positionDeviate);
							if (parseInt >= 1000) {
								deviation = "异常";
							} else {
								deviation = "正常";
							}
						}
					} else {
						deviation = "";
					}
					map.put("deviation", deviation);// 偏差
					map.put("number", number); // 序号
					map.put("familyId", entity.getFamilyId()); // 家庭编号
					map.put("districtName", entity.getDistrictName()); // 行政区
					map.put("subofficeName", entity.getSubofficeName()); // 街道
					map.put("communityName", entity.getCommunityName()); // 社区
					map.put("submitDate", date); // 时间
					map.put("id", entity.getId());
					listResult.add(map);
				}
			}
			Map<String, Object> mapResult = new HashMap<String, Object>();
			mapResult.put("total", totall);
			mapResult.put("rows", listResult);
			response.getWriter().write(ZsJsonUtil.map2Json(mapResult).toString());
		} else if (authDegree.equals("3")) {// 市级管理员
			List<StatisticsParticular> list = aaService.getStatisticsParticularList(cityCode, limit, offset);
			int totall = aaService.selecttotalAll(cityCode);// 问卷总条数
			List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
			int count = 0;
			// JSONArray array = new JSONArray(); 此为对象的集合 getDepartment

			if (list != null && list.size() > 0) {
				count = list.size();
				for (int j = 0; j < count; j++) {
					int number = j + 1;
					String date = list.get(j).getSubmitDate();
					/*
					 * String dates=null; if(date !=null){ SimpleDateFormat
					 * sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //时间格式转换
					 * String sDate=sdf.format(date); dates = sDate;//时间格式转换 }
					 */
					StatisticsParticular entity = list.get(j);
					String travelTotal = entity.getExtendedFieldTwo();
					String total = "";
					int pate = 0;
					String length = entity.getExtendedFieldOne();
					Map<String, Object> map = new HashMap<String, Object>();
					if (length == null || length.equals("")) {
						map.put("travelTotal", "暂无");
					} else {
						pate = Integer.parseInt(length);
						// System.out.println(
						// entity.getAreaId()+"----"+dates+"---"+number);
						if (travelTotal != null && !travelTotal.equals("")) {
							String[] split = travelTotal.split(",");

							for (int i = 0; i < split.length; i++) {
								String[] travel_total = split[i].split(":");
								if (travel_total[1].equals("99")) {
									total = total + "0，";
								} else {
									total = total + travel_total[1] + "，";

								}
							}
							total = total.substring(0, total.length() - 1);
							map.put("travelTotal", total);
						} else {
							map.put("travelTotal", "暂无");
						}
					}
					if (entity.getExtendedFieldOne() != null && !entity.getExtendedFieldOne().equals("")) {
						map.put("familyMembers", entity.getExtendedFieldOne()); // 家庭成员数量
					} else {
						map.put("familyMembers", "暂无"); // 家庭成员数量
					}
					if (null == entity.getStatus() || -1 == entity.getStatus()) {
						map.put("status", "未审核");
					} else if (0 == entity.getStatus()) {
						map.put("status", "未通过");
					} else if (1 == entity.getStatus()) {
						map.put("status", "通过");
					}

					map.put("number", number); // 序号
					map.put("familyId", entity.getFamilyId()); // 家庭编号
					map.put("districtName", entity.getDistrictName()); // 行政区
					map.put("subofficeName", entity.getSubofficeName()); // 街道
					map.put("communityName", entity.getCommunityName()); // 社区
					map.put("submitDate", date); // 时间
					map.put("id", entity.getId());
					listResult.add(map);
				}
			}
			Map<String, Object> mapResult = new HashMap<String, Object>();
			mapResult.put("total", totall);
			mapResult.put("rows", listResult);
			response.getWriter().write(ZsJsonUtil.map2Json(mapResult).toString());
		} else if (authDegree.equals("4")) {// 一级负责人
			List<StatisticsParticular> list = aaService.getQuestionnaire(areaNumber, 1, cityCode, limit, offset);
			int totall = aaService.getQuestionnaireTotal(areaNumber, cityCode);
			List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
			int count = 0;
			// JSONArray array = new JSONArray(); 此为对象的集合 getDepartment
			if (list != null && list.size() > 0) {
				count = list.size();
				for (int j = 0; j < count; j++) {
					int number = j + 1;
					String date = list.get(j).getSubmitDate();
					/*
					 * String dates=null; if(date !=null){ SimpleDateFormat
					 * sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //时间格式转换
					 * String sDate=sdf.format(date); dates = sDate;//时间格式转换 }
					 */
					StatisticsParticular entity = list.get(j);
					String travelTotal = entity.getExtendedFieldTwo();
					String total = "";
					int pate = 0;
					String length = entity.getExtendedFieldOne();
					Map<String, Object> map = new HashMap<String, Object>();
					if (length == null || length.equals("")) {
						map.put("travelTotal", "暂无");
					} else {
						pate = Integer.parseInt(length);
						// System.out.println(
						// entity.getAreaId()+"----"+dates+"---"+number);
						if (travelTotal != null && !travelTotal.equals("")) {
							String[] split = travelTotal.split(",");
							/* if(pate==split.length){ */
							for (int i = 0; i < split.length; i++) {
								String[] travel_total = split[i].split(":");
								if (travel_total[1].equals("99")) {
									total = total + "0，";
								} else {
									total = total + travel_total[1] + "，";

								}
							}
							total = total.substring(0, total.length() - 1);
							map.put("travelTotal", total);
						} else {
							map.put("travelTotal", "暂无");
						}
					}
					if (entity.getExtendedFieldOne() != null && !entity.getExtendedFieldOne().equals("")) {
						map.put("familyMembers", entity.getExtendedFieldOne()); // 家庭成员数量
					} else {
						map.put("familyMembers", "暂无"); // 家庭成员数量
					}
					map.put("number", number); // 序号
					map.put("familyId", entity.getFamilyId()); // 家庭编号
					map.put("districtName", entity.getDistrictName()); // 行政区
					map.put("subofficeName", entity.getSubofficeName()); // 街道
					map.put("communityName", entity.getCommunityName()); // 社区
					map.put("submitDate", date); // 时间
					map.put("id", entity.getId());
					if (null == entity.getStatus() || -1 == entity.getStatus()) {
						map.put("status", "未审核");
					} else if (0 == entity.getStatus()) {
						map.put("status", "未通过");
					} else if (1 == entity.getStatus()) {
						map.put("status", "通过");
					}
					listResult.add(map);
				}
			}
			Map<String, Object> mapResult = new HashMap<String, Object>();
			mapResult.put("total", totall);
			mapResult.put("rows", listResult);
			response.getWriter().write(ZsJsonUtil.map2Json(mapResult).toString());
		} else if (authDegree.equals("5")) {// 二级负责人
			List<StatisticsParticular> list = aaService.getTwoQuestionnaire(areaNumber, 2, cityCode, limit, offset);
			int totall = aaService.getTwoQuestionnaireTotal(areaNumber, cityCode);
			List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
			int count = 0;
			// JSONArray array = new JSONArray(); 此为对象的集合 getDepartment
			if (list != null && list.size() > 0) {
				count = list.size();
				for (int j = 0; j < count; j++) {
					int number = j + 1;
					String date = list.get(j).getSubmitDate();
					StatisticsParticular entity = list.get(j);
					String travelTotal = entity.getExtendedFieldTwo();
					String total = "";
					int pate = 0;
					String length = entity.getExtendedFieldOne();
					Map<String, Object> map = new HashMap<String, Object>();
					if (length == null || length.equals("")) {
						map.put("travelTotal", "暂无");
					} else {
						pate = Integer.parseInt(length);
						// System.out.println(
						// entity.getAreaId()+"----"+dates+"---"+number);
						if (travelTotal != null && !travelTotal.equals("")) {
							String[] split = travelTotal.split(",");
							/* if(pate==split.length){ */
							for (int i = 0; i < split.length; i++) {
								String[] travel_total = split[i].split(":");
								if (travel_total[1].equals("99")) {
									total = total + "0，";
								} else {
									total = total + travel_total[1] + "，";

								}
							}
							total = total.substring(0, total.length() - 1);
							map.put("travelTotal", total);
						} else {
							map.put("travelTotal", "暂无");
						}
					}
					if (entity.getExtendedFieldOne() != null && !entity.getExtendedFieldOne().equals("")) {
						map.put("familyMembers", entity.getExtendedFieldOne()); // 家庭成员数量
					} else {
						map.put("familyMembers", "暂无"); // 家庭成员数量
					}
					map.put("number", number); // 序号
					map.put("familyId", entity.getFamilyId()); // 家庭编号
					map.put("districtName", entity.getDistrictName()); // 行政区
					map.put("subofficeName", entity.getSubofficeName()); // 街道
					map.put("communityName", entity.getCommunityName()); // 社区
					map.put("submitDate", date); // 时间
					map.put("id", entity.getId());
					if (null == entity.getStatus() || -1 == entity.getStatus()) {
						map.put("status", "未审核");
					} else if (0 == entity.getStatus()) {
						map.put("status", "未通过");
					} else if (1 == entity.getStatus()) {
						map.put("status", "通过");
					}
					listResult.add(map);
				}
			}
			Map<String, Object> mapResult = new HashMap<String, Object>();
			mapResult.put("total", totall);
			mapResult.put("rows", listResult);
			response.getWriter().write(ZsJsonUtil.map2Json(mapResult).toString());
		} else if (authDegree.equals("6")) {// 三级负责人
			List<StatisticsParticular> list = aaService.getSanQuestionnaire(areaNumber, 2, cityCode, limit, offset);
			int totall = aaService.getSanQuestionnaireTotal(areaNumber, cityCode);
			List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
			int count = 0;
			// JSONArray array = new JSONArray(); 此为对象的集合 getDepartment
			if (list != null && list.size() > 0) {
				count = list.size();
				for (int j = 0; j < count; j++) {
					int number = j + 1;
					String date = list.get(j).getSubmitDate();
					/*
					 * String dates=null; if(date !=null){ SimpleDateFormat
					 * sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //时间格式转换
					 * String sDate=sdf.format(date); dates = sDate;//时间格式转换 }
					 */
					StatisticsParticular entity = list.get(j);
					String travelTotal = entity.getExtendedFieldTwo();
					String total = "";
					int pate = 0;
					String length = entity.getExtendedFieldOne();
					Map<String, Object> map = new HashMap<String, Object>();
					if (length == null || length.equals("")) {
						map.put("travelTotal", "暂无");
					} else {
						pate = Integer.parseInt(length);
						// System.out.println(
						// entity.getAreaId()+"----"+dates+"---"+number);
						if (travelTotal != null && !travelTotal.equals("")) {
							String[] split = travelTotal.split(",");
							/* if(pate==split.length){ */
							for (int i = 0; i < split.length; i++) {
								String[] travel_total = split[i].split(":");
								if (travel_total[1].equals("99")) {
									total = total + "0，";
								} else {
									total = total + travel_total[1] + "，";
								}
							}
							total = total.substring(0, total.length() - 1);
							map.put("travelTotal", total);
						} else {
							map.put("travelTotal", "暂无");
						}
					}
					if (entity.getExtendedFieldOne() != null && !entity.getExtendedFieldOne().equals("")) {
						map.put("familyMembers", entity.getExtendedFieldOne()); // 家庭成员数量
					} else {
						map.put("familyMembers", "暂无"); // 家庭成员数量
					}
					map.put("number", number); // 序号
					map.put("familyId", entity.getFamilyId()); // 家庭编号
					map.put("districtName", entity.getDistrictName()); // 行政区
					map.put("subofficeName", entity.getSubofficeName()); // 街道
					map.put("communityName", entity.getCommunityName()); // 社区
					map.put("submitDate", date); // 时间
					map.put("id", entity.getId());
					if (null == entity.getStatus() || -1 == entity.getStatus()) {
						map.put("status", "未审核");
					} else if (0 == entity.getStatus()) {
						map.put("status", "未通过");
					} else if (1 == entity.getStatus()) {
						map.put("status", "通过");
					}
					listResult.add(map);
				}
			}
			Map<String, Object> mapResult = new HashMap<String, Object>();
			mapResult.put("total", totall);
			mapResult.put("rows", listResult);
			response.getWriter().write(ZsJsonUtil.map2Json(mapResult).toString());
		} else {
			System.out.println("没有权限");
		}

	}

	/**
	 * 条件查询问卷
	 * 
	 */
	@RequestMapping(value = "/getConditions", method = RequestMethod.POST)
	public void getConditions(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		String offsetType = request.getParameter("offset");// 获取分页参数
		String limitType = request.getParameter("limit");// 获取分页参数
		int limit = Integer.parseInt(limitType);
		int offset = Integer.parseInt(offsetType);
		String districtNumber = request.getParameter("districtNumber").trim();
		String subofficeNumber = request.getParameter("subofficeNumber").trim();
		String communityNumber = request.getParameter("communityNumber").trim();
		String familyId = request.getParameter("familyId").trim();
		if (districtNumber.equals("")) {
			districtNumber = null;
		}
		if (subofficeNumber.equals("")) {
			subofficeNumber = null;
		}
		if (familyId.equals("")) {
			familyId = null;
		}
		if (communityNumber.equals("")) {
			communityNumber = null;
		}
		List<StatisticsParticular> list = aaService.selectContent(districtNumber, subofficeNumber, familyId,
				communityNumber, cityCode, limit, offset);
		int totall = aaService.selectContentTotal(districtNumber, subofficeNumber, familyId, communityNumber, cityCode);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		int count = 0;
		// JSONArray array = new JSONArray(); 此为对象的集合 getDepartment
		if (list != null && list.size() > 0) {
			count = list.size();
			for (int j = 0; j < count; j++) {
				int number = j + 1;
				String date = list.get(j).getSubmitDate();
				String dates = null;
				/*
				 * if(date !=null){ SimpleDateFormat sdf=new SimpleDateFormat(
				 * "yyyy-MM-dd HH:mm:ss"); //时间格式转换 String
				 * sDate=sdf.format(date); dates = sDate;//时间格式转换 }
				 */
				StatisticsParticular entity = list.get(j);
				String travelTotal = entity.getExtendedFieldTwo();
				String total = "";
				int pate = 0;
				String length = entity.getExtendedFieldOne();
				Map<String, Object> map = new HashMap<String, Object>();
				if (length == null || length.equals("")) {
					map.put("travelTotal", "暂无");
				} else {
					pate = Integer.parseInt(length);
					// System.out.println(
					// entity.getAreaId()+"----"+dates+"---"+number);
					if (travelTotal != null && !travelTotal.equals("")) {
						String[] split = travelTotal.split(",");
						/* if(pate==split.length){ */
						for (int i = 0; i < split.length; i++) {
							String[] travel_total = split[i].split(":");
							if (travel_total[1].equals("99")) {
								total = total + "0，";
							} else {
								total = total + travel_total[1] + "，";

							}
						}
						total = total.substring(0, total.length() - 1);
						map.put("travelTotal", total);
					} else {
						map.put("travelTotal", "暂无");
					}
				}
				if (entity.getExtendedFieldOne() != null && !entity.getExtendedFieldOne().equals("")) {
					map.put("familyMembers", entity.getExtendedFieldOne()); // 家庭成员数量
				} else {
					map.put("familyMembers", "暂无"); // 家庭成员数量
				}
				String positionDeviate = entity.getPositionDeviate();
				String deviation = null;
				boolean flag = false;
				if (positionDeviate != null) {
					for (int i = 0; i < positionDeviate.length(); i++) {
						// 获取相应的字符
						char bb = positionDeviate.charAt(i);
						if (bb == '.') {
							flag = true;
							break;
						}
					}
					if (flag) {
						String[] splitaOne = positionDeviate.split("\\.");

						String deviationType = splitaOne[0];
						int parseInt = Integer.parseInt(deviationType);
						if (parseInt >= 1000) {
							deviation = "异常";
						} else {
							deviation = "正常";
						}

					} else {
						int parseInt = Integer.parseInt(positionDeviate);
						if (parseInt >= 1000) {
							deviation = "异常";
						} else {
							deviation = "正常";
						}
					}
				} else {
					deviation = "";
				}
				map.put("deviation", deviation);// 偏差
				map.put("number", number); // 序号
				map.put("familyId", entity.getFamilyId()); // 家庭编号
				map.put("districtName", entity.getDistrictName()); // 行政区
				map.put("subofficeName", entity.getSubofficeName()); // 街道
				map.put("communityName", entity.getCommunityName()); // 社区
				map.put("submitDate", date); // 时间
				map.put("id", entity.getId());
				if (null == entity.getStatus() || -1 == entity.getStatus()) {
					map.put("status", "未审核");
				} else if (0 == entity.getStatus()) {
					map.put("status", "未通过");
				} else if (1 == entity.getStatus()) {
					map.put("status", "通过");
				}
				listResult.add(map);
			}
		}
		Map<String, Object> mapResult = new HashMap<String, Object>();
		mapResult.put("total", totall);
		mapResult.put("rows", listResult);
		response.getWriter().write(ZsJsonUtil.map2Json(mapResult).toString());

	}

	/**
	 * 
	 * 查询公众问卷
	 */
	@RequestMapping(value = "/getPublicQuestionnaire", method = RequestMethod.POST)
	public void getPublicQuestionnaire(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		String offsetType = request.getParameter("offset");// 获取分页参数
		String limitType = request.getParameter("limit");// 获取分页参数
		int limit = Integer.parseInt(limitType);
		int offset = Integer.parseInt(offsetType);
		List<StatisticsParticular> list = aaService.getPublicQuestionnaire(cityCode, limit, offset);
		int totall = aaService.getPublicQuestionnaireTotal(cityCode);
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		if (list != null && list.size() > 0) {
			for (int j = 0; j < list.size(); j++) {
				int number = j + 1;
				String date = list.get(j).getSubmitDate();
				/*
				 * String dates=null; if(date !=null){ SimpleDateFormat sdf=new
				 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //时间格式转换 String
				 * sDate=sdf.format(date); dates = sDate;//时间格式转换 }
				 */
				StatisticsParticular entity = list.get(j);
				String travelTotal = entity.getExtendedFieldTwo();
				String total = "";
				int pate = 0;
				String length = entity.getExtendedFieldOne();
				Map<String, Object> map = new HashMap<String, Object>();
				if (length == null || length.equals("")) {
					map.put("travelTotal", "暂无");
				} else {
					pate = Integer.parseInt(length);
					// System.out.println(
					// entity.getAreaId()+"----"+dates+"---"+number);
					if (travelTotal != null && !travelTotal.equals("")) {
						String[] split = travelTotal.split(",");

						for (int i = 0; i < split.length; i++) {
							String[] travel_total = split[i].split(":");
							if (travel_total[1].equals("99")) {
								total = total + "0，";
							} else {
								total = total + travel_total[1] + "，";

							}
						}
						total = total.substring(0, total.length() - 1);
						map.put("travelTotal", total);
					} else {
						map.put("travelTotal", "暂无");
					}
				}
				if (entity.getExtendedFieldOne() != null && !entity.getExtendedFieldOne().equals("")) {
					map.put("familyMembers", entity.getExtendedFieldOne()); // 家庭成员数量
				} else {
					map.put("familyMembers", "暂无"); // 家庭成员数量
				}
				if (null == entity.getStatus() || -1 == entity.getStatus()) {
					map.put("status", "未审核");
				} else if (0 == entity.getStatus()) {
					map.put("status", "未通过");
				} else if (1 == entity.getStatus()) {
					map.put("status", "通过");
				}
				String positionDeviate = entity.getPositionDeviate();
				String deviation = null;
				boolean flag = false;
				if (positionDeviate != null) {
					for (int i = 0; i < positionDeviate.length(); i++) {
						// 获取相应的字符
						char bb = positionDeviate.charAt(i);
						if (bb == '.') {
							flag = true;
							break;
						}
					}
					if (flag) {
						String[] splitaOne = positionDeviate.split("\\.");

						String deviationType = splitaOne[0];
						int parseInt = Integer.parseInt(deviationType);
						if (parseInt >= 1000) {
							deviation = "异常";
						} else {
							deviation = "正常";
						}

					} else {
						int parseInt = Integer.parseInt(positionDeviate);
						if (parseInt >= 1000) {
							deviation = "异常";
						} else {
							deviation = "正常";
						}
					}
				} else {
					deviation = "";
				}
				map.put("deviation", deviation);// 偏差

				map.put("number", number);
				map.put("familyId", entity.getFamilyId());
				map.put("districtName", entity.getExtendedFieldThree());// 行政区
				map.put("address", entity.getAddress());
				map.put("submitDate", date);
				map.put("id", entity.getId());
				listResult.add(map);
			}
		}
		Map<String, Object> mapResult = new HashMap<String, Object>();
		mapResult.put("total", totall);
		mapResult.put("rows", listResult);
		response.getWriter().write(ZsJsonUtil.map2Json(mapResult).toString());
	}

	/**
	 * 
	 * 查询公众行政区下拉框
	 */
	@RequestMapping(value = "/selectDis", method = RequestMethod.POST)
	public void selectDis(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		List<StatisticsParticular> list = aaService.selectDis(cityCode);
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				String distric = "";
				StatisticsParticular entity = list.get(i);
				Map<String, Object> map = new HashMap<String, Object>();
				if(entity!=null){
					distric = entity.getExtendedFieldThree();
				}
				if(distric!=null && !distric.equals("")){
					map.put("distric", distric);
				}
				listResult.add(map);
			}
		}
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}

	/**
	 * 
	 * 条件查询公众问卷
	 */
	@RequestMapping(value = "/selectPublic", method = RequestMethod.POST)
	public void selectPublic(XssHttpServletRequestWrapper request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		String district = request.getParameter("district");
		String offsetType = request.getParameter("offset");// 获取分页参数
		String limitType = request.getParameter("limit");// 获取分页参数
		int limit = Integer.parseInt(limitType);
		int offset = Integer.parseInt(offsetType);
		if (district != null) {
			district = district.trim();
			if (district.equals("请选择")) {
				district = null;
			}
		}
		List<StatisticsParticular> list = aaService.selectPublic(cityCode, district, limit, offset);
		int totall = aaService.selectPublicTotal(cityCode, district);
		if (list != null && list.size() > 0) {
			for (int j = 0; j < list.size(); j++) {
				int number = j + 1;
				String date = list.get(j).getSubmitDate();
				/*
				 * String dates=null; if(date !=null){ SimpleDateFormat sdf=new
				 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //时间格式转换 String
				 * sDate=sdf.format(date); dates = sDate;//时间格式转换 }
				 */
				StatisticsParticular entity = list.get(j);
				String travelTotal = entity.getExtendedFieldTwo();
				String total = "";
				int pate = 0;
				String length = entity.getExtendedFieldOne();
				Map<String, Object> map = new HashMap<String, Object>();
				if (length == null || length.equals("")) {
					map.put("travelTotal", "暂无");
				} else {
					pate = Integer.parseInt(length);
					// System.out.println(
					// entity.getAreaId()+"----"+dates+"---"+number);
					if (travelTotal != null && !travelTotal.equals("")) {
						String[] split = travelTotal.split(",");

						for (int i = 0; i < split.length; i++) {
							String[] travel_total = split[i].split(":");
							if (travel_total[1].equals("99")) {
								total = total + "0，";
							} else {
								total = total + travel_total[1] + "，";

							}
						}
						total = total.substring(0, total.length() - 1);
						map.put("travelTotal", total);
					} else {
						map.put("travelTotal", "暂无");
					}
				}
				if (entity.getExtendedFieldOne() != null && !entity.getExtendedFieldOne().equals("")) {
					map.put("familyMembers", entity.getExtendedFieldOne()); // 家庭成员数量
				} else {
					map.put("familyMembers", "暂无"); // 家庭成员数量
				}
				if (null == entity.getStatus() || -1 == entity.getStatus()) {
					map.put("status", "未审核");
				} else if (0 == entity.getStatus()) {
					map.put("status", "未通过");
				} else if (1 == entity.getStatus()) {
					map.put("status", "通过");
				}
				String positionDeviate = entity.getPositionDeviate();
				String deviation = null;
				boolean flag = false;
				if (positionDeviate != null) {
					for (int i = 0; i < positionDeviate.length(); i++) {
						// 获取相应的字符
						char bb = positionDeviate.charAt(i);
						if (bb == '.') {
							flag = true;
							break;
						}
					}
					if (flag) {
						String[] splitaOne = positionDeviate.split("\\.");

						String deviationType = splitaOne[0];
						int parseInt = Integer.parseInt(deviationType);
						if (parseInt >= 1000) {
							deviation = "异常";
						} else {
							deviation = "正常";
						}

					} else {
						int parseInt = Integer.parseInt(positionDeviate);
						if (parseInt >= 1000) {
							deviation = "异常";
						} else {
							deviation = "正常";
						}
					}
				} else {
					deviation = "";
				}
				map.put("deviation", deviation);// 偏差
				map.put("number", number);
				map.put("familyId", entity.getFamilyId());
				map.put("districtName", entity.getExtendedFieldThree());// 行政区
				map.put("address", entity.getAddress());
				map.put("submitDate", date);
				listResult.add(map);
			}
		}
		Map<String, Object> mapResult = new HashMap<String, Object>();
		mapResult.put("total", totall);
		mapResult.put("rows",listResult);
		response.getWriter().write(ZsJsonUtil.map2Json(mapResult).toString());
	}

	// 数量统计
	@RequestMapping(value = "/statisticsNumber", method = RequestMethod.POST)
	public void statisticsNumber(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		// 判断登录人权限人员权限
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		Integer isSuperAdmin = (Integer) session.getAttribute("isSuperAdmin");// 获取用户是否为超级管理员
		if (isSuperAdmin == null) {
			isSuperAdmin = 0;
		}
		Integer userId = (Integer) session.getAttribute("userId");// 获取用户id
		String authDegrees = (String) session.getAttribute("authDegree");
		// 获取用户权限
		/*
		 * String userName = (String) session.getAttribute("userName");//获取用户名z
		 */ String authDegree = "";
		String[] array = null;
		if (authDegrees != null) {
			array = authDegrees.split(",");
			Boolean flag = true;
			for (String auth : array) {
				if (auth.equals("3") || auth.equals("4") || auth.equals("5") || auth.equals("6") || auth.equals("7")) {
					authDegree = auth;
				}
			}
		}
		List<Area> listArea = new ArrayList<Area>();
		List<String> areaNumber = new ArrayList<String>();
		if (authDegree.equals("4")) {// 一级负责人
			listArea = areaService.getAreaByuserId(userId, 1);// 根据用户id和type获取当前登录用户的管辖区域
			for (int i = 0; i < listArea.size(); i++) {
				areaNumber.add(listArea.get(i).getDistrictNumber());
			}
		} else if (authDegree.equals("5")) {// 二级负责人
			listArea = areaService.getAreaByuserId(userId, 2);// 根据用户id和type获取当前登录用户的管辖区域
			for (int i = 0; i < listArea.size(); i++) {
				areaNumber.add(listArea.get(i).getSubofficeNumber());
			}
		} else if (authDegree.equals("6")) {// 三级负责人
			listArea = areaService.getAreaByuserId(userId, 3);// 根据用户id和type获取当前登录用户的管辖区域
			for (int i = 0; i < listArea.size(); i++) {
				areaNumber.add(listArea.get(i).getCommunityNumber());
			}
		}
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		if (isSuperAdmin == 1 || authDegree.equals("3")) {
			int totalQuestionnaire = aaService.selecttotalAll(cityCode);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("totalQuestionnaire", totalQuestionnaire);// 问卷的总条数
			int ResidentsTotal = aaService.selectResidentsQuestionnaire(cityCode);// 居民填写的
			int investigatorsTotal = totalQuestionnaire - ResidentsTotal;
			map.put("ResidentsTotal", ResidentsTotal);// 居民填写的问卷数量
			map.put("investigatorsTotal", investigatorsTotal);// 志愿者填写的问卷数量
			int ReviewNot = aaService.selectReviewNot(cityCode);// 未审核数量
			int ReviewYes = aaService.selectReviewYes(cityCode);// 通过审核数量
			int ReviewPass = totalQuestionnaire - (ReviewNot + ReviewYes);
			map.put("ReviewNot", ReviewNot);// 未审核
			map.put("ReviewYes", ReviewYes);// 已通过
			map.put("ReviewPass", ReviewPass);// 未通过审核
			int completed = aaService.selectStageTotal(cityCode);// 问卷填写完成的数量
			int noCompleted = totalQuestionnaire - completed;// 未完成
			map.put("completed", completed);// 已完成
			map.put("noCompleted", noCompleted);// 未完成
			int deviation = 0;
			int deviationOn = 0;
			List<StatisticsParticular> statistics = aaService.getStatistics(cityCode);
			if (statistics != null && statistics.size() > 0) {
				for (int i = 0; i < statistics.size(); i++) {
					StatisticsParticular entity = statistics.get(i);
					String positionDeviate = entity.getPositionDeviate();
					boolean flag = false;
					if (positionDeviate != null) {
						for (int k = 0; k < positionDeviate.length(); k++) {
							// 获取相应的字符
							char bb = positionDeviate.charAt(k);
							if (bb == '.') {
								flag = true;
								break;
							}
						}
						if (flag) {
							String[] splitaOne = positionDeviate.split("\\.");

							String deviationType = splitaOne[0];
							int parseInt = Integer.parseInt(deviationType);
							if (parseInt >= 1000) {
								deviationOn++;
							} else {
								deviation++;
							}

						} else {
							int parseInt = Integer.parseInt(positionDeviate);
							if (parseInt >= 1000) {
								deviationOn++;
							} else {
								deviation++;
							}
						}
					}
				}
			}
			int numberPass = 0;
			int flagNumber = 0;
			int number = 0;
			List<StatisticsParticular> list = aaService.selectTravelTotal(cityCode);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					number = i + 1;
					boolean flagrPass = false;
					StatisticsParticular entity = list.get(i);
					String Traveltotal = entity.getExtendedFieldTwo();
					if (Traveltotal != null && !Traveltotal.equals("")) {
						String[] split = Traveltotal.split(",");
						for (int j = 0; j < split.length; j++) {
							String[] travel_total = split[j].split(":");
							int parseInt = Integer.parseInt(travel_total[1]);
							if (parseInt < 2 || parseInt == 99) {
								flagrPass = true;
								break;
							}
						}
						if (flagrPass) {
							numberPass++;
						}

					}
					if (Traveltotal == null || Traveltotal.equals("")) {
						flagNumber++;
					}
				}
			}
			map.put("deviation", deviation);// 无偏差
			map.put("deviationOn", deviationOn);// 有偏差
			map.put("nowring", numberPass);
			map.put("wring", number - numberPass - flagNumber);
			listResult.add(map);
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
		} else if (authDegree.equals("4")) {// 一级管理员
			int totalQuestionnaire = aaService.selectOneTotal(areaNumber, cityCode);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("totalQuestionnaire", totalQuestionnaire);// 问卷的总条数
			int selectResidentsOne = aaService.selectResidentsOne(areaNumber, cityCode);// 居民填写的
			int investigatorsTotal = totalQuestionnaire - selectResidentsOne;// 志愿者填写的
			map.put("ResidentsTotal", selectResidentsOne);// 居民填写的问卷数量
			map.put("investigatorsTotal", investigatorsTotal);// 志愿者填写的问卷数量
			int ReviewNot = aaService.selectReviewNotOne(areaNumber, cityCode);// 未审核数量
			int ReviewYes = aaService.selectReviewYesOne(areaNumber, cityCode);// 通过审核数量
			int ReviewPass = totalQuestionnaire - (ReviewNot + ReviewYes);// 未通过审核数量
			map.put("ReviewNot", ReviewNot);// 未审核
			map.put("ReviewYes", ReviewYes);// 已通过
			map.put("ReviewPass", ReviewPass);// 未通过审核
			int completed = aaService.selectStageTotalOne(areaNumber, cityCode);// 问卷填写完成的数量
			int noCompleted = totalQuestionnaire - completed;// 未完成
			map.put("completed", completed);// 已完成
			map.put("noCompleted", noCompleted);// 未完成
			/*
			 * int deviation =0; int deviationOn=0; List<StatisticsParticular>
			 * statistics = aaService.getStatisticsOne(areaNumber,cityCode);
			 * if(statistics!=null && statistics.size()>0){ for (int i = 0; i <
			 * statistics.size(); i++) { StatisticsParticular entity =
			 * statistics.get(i); String positionDeviate =
			 * entity.getPositionDeviate(); boolean flag = false;
			 * if(positionDeviate!=null){ for(int k =0;
			 * k<positionDeviate.length();k++){ //获取相应的字符 char bb =
			 * positionDeviate.charAt(k); if(bb == '.'){ flag=true; break; } }
			 * if(flag){ String[] splitaOne = positionDeviate.split("\\.");
			 * 
			 * String deviationType=splitaOne[0]; int parseInt =
			 * Integer.parseInt(deviationType); if(parseInt>=1000){
			 * deviationOn++; }else{ deviation++; }
			 * 
			 * }else{ int parseInt = Integer.parseInt(positionDeviate);
			 * if(parseInt>=1000){ deviationOn++; }else{ deviation++; } } } } }
			 */
			int numberPass = 0;
			int flagNumber = 0;
			int number = 0;
			List<StatisticsParticular> list = aaService.getStatisticsOne(areaNumber, cityCode);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					number = i + 1;
					boolean flagrPass = false;
					StatisticsParticular entity = list.get(i);
					String Traveltotal = entity.getExtendedFieldTwo();
					if (Traveltotal != null && !Traveltotal.equals("")) {
						String[] split = Traveltotal.split(",");
						for (int j = 0; j < split.length; j++) {
							String[] travel_total = split[j].split(":");
							int parseInt = Integer.parseInt(travel_total[1]);
							if (parseInt < 2 || parseInt == 99) {
								flagrPass = true;
								break;
							}
						}
						if (flagrPass) {
							numberPass++;
						}
					}
					if (Traveltotal == null || Traveltotal.equals("")) {
						flagNumber++;
					}
				}
			}
			/*
			 * map.put("deviation", deviation);//无偏差 map.put("deviationOn",
			 * deviationOn);//有偏差
			 */
			map.put("nowring", numberPass);
			map.put("wring", number - numberPass - flagNumber);
			listResult.add(map);
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
		} else if (authDegree.equals("5")) {// 二级级管理员
			int totalQuestionnaire = aaService.selectTwoTotal(areaNumber, cityCode);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("totalQuestionnaire", totalQuestionnaire);// 问卷的总条数
			int selectResidentsOne = aaService.selectResidentsTwo(areaNumber, cityCode);// 居民填写的
			int investigatorsTotal = totalQuestionnaire - selectResidentsOne;// 志愿者填写的
			map.put("ResidentsTotal", selectResidentsOne);// 居民填写的问卷数量
			map.put("investigatorsTotal", investigatorsTotal);// 志愿者填写的问卷数量
			int ReviewNot = aaService.selectReviewNotTwo(areaNumber, cityCode);// 未审核数量
			int ReviewYes = aaService.selectReviewYesTwo(areaNumber, cityCode);// 通过审核数量
			int ReviewPass = totalQuestionnaire - (ReviewNot + ReviewYes);// 未通过审核数量
			map.put("ReviewNot", ReviewNot);// 未审核
			map.put("ReviewYes", ReviewYes);// 已通过
			map.put("ReviewPass", ReviewPass);// 未通过审核
			int completed = aaService.selectStageTotalTwo(areaNumber, cityCode);// 问卷填写完成的数量
			int noCompleted = totalQuestionnaire - completed;// 未完成
			map.put("completed", completed);// 已完成
			map.put("noCompleted", noCompleted);// 未完成
			/*
			 * int deviation =0; int deviationOn=0; List<StatisticsParticular>
			 * statistics = aaService.getStatisticsTwo(areaNumber,cityCode);
			 * if(statistics!=null && statistics.size()>0){ for (int i = 0; i <
			 * statistics.size(); i++) { StatisticsParticular entity =
			 * statistics.get(i); String positionDeviate =
			 * entity.getPositionDeviate(); boolean flag = false;
			 * if(positionDeviate!=null){ for(int k =0;
			 * k<positionDeviate.length();k++){ //获取相应的字符 char bb =
			 * positionDeviate.charAt(k); if(bb == '.'){ flag=true; break; } }
			 * if(flag){ String[] splitaOne = positionDeviate.split("\\.");
			 * 
			 * String deviationType=splitaOne[0]; int parseInt =
			 * Integer.parseInt(deviationType); if(parseInt>=1000){
			 * deviationOn++; }else{ deviation++; }
			 * 
			 * }else{ int parseInt = Integer.parseInt(positionDeviate);
			 * if(parseInt>=1000){ deviationOn++; }else{ deviation++; } } } } }
			 */
			int numberPass = 0;
			int flagNumber = 0;
			int number = 0;
			List<StatisticsParticular> list = aaService.getStatisticsTwo(areaNumber, cityCode);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					number = i + 1;
					boolean flagrPass = false;
					StatisticsParticular entity = list.get(i);
					String Traveltotal = entity.getExtendedFieldTwo();
					if (Traveltotal != null && !Traveltotal.equals("")) {
						String[] split = Traveltotal.split(",");
						for (int j = 0; j < split.length; j++) {
							String[] travel_total = split[j].split(":");
							int parseInt = Integer.parseInt(travel_total[1]);
							if (parseInt < 2 || parseInt == 99) {
								flagrPass = true;
								break;
							}
						}
						if (flagrPass) {
							numberPass++;
						}

					}
					if (Traveltotal == null || Traveltotal.equals("")) {
						flagNumber++;
					}
				}
			}
			/*
			 * map.put("deviation", deviation);//无偏差 map.put("deviationOn",
			 * deviationOn);//有偏差
			 */ map.put("nowring", numberPass);
			map.put("wring", number - numberPass - flagNumber);
			listResult.add(map);
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
		} else if (authDegree.equals("6")) {// 三级管理员
			int totalQuestionnaire = aaService.selectThreeTotal(areaNumber, cityCode);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("totalQuestionnaire", totalQuestionnaire);// 问卷的总条数
			int selectResidentsOne = aaService.selectResidentsThree(areaNumber, cityCode);// 居民填写的
			int investigatorsTotal = totalQuestionnaire - selectResidentsOne;// 志愿者填写的
			map.put("ResidentsTotal", selectResidentsOne);// 居民填写的问卷数量
			map.put("investigatorsTotal", investigatorsTotal);// 志愿者填写的问卷数量
			int ReviewNot = aaService.selectReviewNotThree(areaNumber, cityCode);// 未审核数量
			int ReviewYes = aaService.selectReviewYesThree(areaNumber, cityCode);// 通过审核数量
			int ReviewPass = totalQuestionnaire - (ReviewNot + ReviewYes);// 未通过审核数量
			map.put("ReviewNot", ReviewNot);// 未审核
			map.put("ReviewYes", ReviewYes);// 已通过
			map.put("ReviewPass", ReviewPass);// 未通过审核
			int completed = aaService.selectStageTotalThree(areaNumber, cityCode);// 问卷填写完成的数量
			int noCompleted = totalQuestionnaire - completed;// 未完成
			map.put("completed", completed);// 已完成
			map.put("noCompleted", noCompleted);// 未完成
			/*
			 * int deviation =0; int deviationOn=0; List<StatisticsParticular>
			 * statistics = aaService.getStatisticsThree(areaNumber,cityCode);
			 * if(statistics!=null && statistics.size()>0){ for (int i = 0; i <
			 * statistics.size(); i++) { StatisticsParticular entity =
			 * statistics.get(i); String positionDeviate =
			 * entity.getPositionDeviate(); boolean flag = false;
			 * if(positionDeviate!=null){ for(int k =0;
			 * k<positionDeviate.length();k++){ //获取相应的字符 char bb =
			 * positionDeviate.charAt(k); if(bb == '.'){ flag=true; break; } }
			 * if(flag){ String[] splitaOne = positionDeviate.split("\\.");
			 * 
			 * String deviationType=splitaOne[0]; int parseInt =
			 * Integer.parseInt(deviationType); if(parseInt>=1000){
			 * deviationOn++; }else{ deviation++; }
			 * 
			 * }else{ int parseInt = Integer.parseInt(positionDeviate);
			 * if(parseInt>=1000){ deviationOn++; }else{ deviation++; } } } } }
			 */
			int numberPass = 0;
			int flagNumber = 0;
			int number = 0;
			List<StatisticsParticular> list = aaService.getStatisticsThree(areaNumber, cityCode);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					number = i + 1;
					boolean flagrPass = false;
					StatisticsParticular entity = list.get(i);
					String Traveltotal = entity.getExtendedFieldTwo();
					if (Traveltotal != null && !Traveltotal.equals("")) {
						String[] split = Traveltotal.split(",");
						for (int j = 0; j < split.length; j++) {
							String[] travel_total = split[j].split(":");
							int parseInt = Integer.parseInt(travel_total[1]);
							if (parseInt < 2 || parseInt == 99) {
								flagrPass = true;
								break;
							}
						}
						if (flagrPass) {
							numberPass++;
						}

					}
					if (Traveltotal == null || Traveltotal.equals("")) {
						flagNumber++;
					}
				}
			}
			/*
			 * map.put("deviation", deviation);//无偏差 map.put("deviationOn",
			 * deviationOn);//有偏差
			 */
			map.put("nowring", numberPass);
			map.put("wring", number - numberPass - flagNumber);
			listResult.add(map);
			response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
		} else {
			System.out.println("权限不足");
		}

	}

	// 条件查询 统计
	@RequestMapping(value = "/selectConditions", method = RequestMethod.POST)
	public void selectConditions(XssHttpServletRequestWrapper request, HttpServletResponse response)
			throws IOException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		HttpSession session = servletRequest.getSession();
		String cityCode = (String) session.getAttribute("cityCode");// 获取城市编码
		List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		String districtNumber = request.getParameter("districtNumber").trim();
		String subofficeNumber = request.getParameter("subofficeNumber").trim();
		String familyId = request.getParameter("familyId").trim();
		String communityNumber = request.getParameter("communityNumber").trim();
		if (districtNumber.equals("")) {
			districtNumber = null;
		}
		if (subofficeNumber.equals("")) {
			subofficeNumber = null;
		}
		if (familyId.equals("")) {
			familyId = null;
		}
		if (communityNumber.equals("")) {
			communityNumber = null;
		}
		int totalQuestionnaire = aaService.selectConditions(districtNumber, subofficeNumber, familyId, communityNumber,
				cityCode);// 总数
		int ResidentsTotal = aaService.selectConditionsResidents(districtNumber, subofficeNumber, familyId,
				communityNumber, cityCode);// 居民填写的数量
		int investigatorsTotal = totalQuestionnaire - ResidentsTotal;// 志愿者填写的问卷
		int ReviewNot = aaService.selectCondditionNotaudit(districtNumber, subofficeNumber, familyId, communityNumber,
				cityCode);// 未审核
		int ReviewYes = aaService.selectCondditioYesaudit(districtNumber, subofficeNumber, familyId, communityNumber,
				cityCode);// 通过审核
		int ReviewPass = totalQuestionnaire - (ReviewNot + ReviewYes);// 不通过
		int completed = aaService.selectCondditionStageTotal(districtNumber, subofficeNumber, familyId, communityNumber,
				cityCode);// 问卷填写完成
		int noCompleted = totalQuestionnaire - completed;// 问卷未完成
		int deviation = 0;
		int deviationOn = 0;
		List<StatisticsParticular> statistics = aaService.selectDeviation(districtNumber, subofficeNumber, familyId,
				communityNumber, cityCode);
		if (statistics != null && statistics.size() > 0) {
			for (int i = 0; i < statistics.size(); i++) {
				StatisticsParticular entity = statistics.get(i);
				String positionDeviate = entity.getPositionDeviate();
				boolean flag = false;

				if (positionDeviate != null) {
					for (int k = 0; k < positionDeviate.length(); k++) {
						// 获取相应的字符
						char bb = positionDeviate.charAt(k);
						if (bb == '.') {
							flag = true;
							break;
						}
					}
					if (flag) {
						String[] splitaOne = positionDeviate.split("\\.");

						String deviationType = splitaOne[0];
						int parseInt = Integer.parseInt(deviationType);
						if (parseInt >= 1000) {
							deviationOn++;
						} else {
							deviation++;
						}
					} else {
						int parseInt = Integer.parseInt(positionDeviate);
						if (parseInt >= 1000) {
							deviationOn++;
						} else {
							deviation++;
						}
					}
				}
			}
		}
		List<StatisticsParticular> list = aaService.selectTotal(districtNumber, subofficeNumber, familyId,
				communityNumber, cityCode);
		int numberPass = 0;
		int flagNumber = 0;
		int number = 0;
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				number = i + 1;
				boolean flagrPass = false;
				StatisticsParticular entity = list.get(i);
				String Traveltotal = entity.getExtendedFieldTwo();
				if (Traveltotal != null && !Traveltotal.equals("")) {
					String[] split = Traveltotal.split(",");
					for (int j = 0; j < split.length; j++) {
						String[] travel_total = split[j].split(":");
						int parseInt = Integer.parseInt(travel_total[1]);
						if (parseInt < 2 || parseInt == 99) {
							flagrPass = true;
							break;
						}
					}
					if (flagrPass) {
						numberPass = numberPass + 1;
					}

				}
				if (Traveltotal == null || Traveltotal.equals("")) {
					flagNumber++;
				}
			}
		}
		map.put("deviation", deviation);// 无偏差
		map.put("deviationOn", deviationOn);// 有偏差
		map.put("totalQuestionnaire", totalQuestionnaire);// 问卷的总条数
		map.put("ResidentsTotal", ResidentsTotal);// 居民填写的问卷数量
		map.put("investigatorsTotal", investigatorsTotal);// 志愿者填写的问卷数量
		map.put("ReviewNot", ReviewNot);// 未审核
		map.put("ReviewYes", ReviewYes);// 已通过
		map.put("ReviewPass", ReviewPass);// 未通过审核
		map.put("completed", completed);// 已完成
		map.put("noCompleted", noCompleted);// 未完成
		map.put("nowring", numberPass);
		map.put("wring", number - numberPass - flagNumber);
		listResult.add(map);
		response.getWriter().write(ZsJsonUtil.listMap2Json(listResult));
	}
}
