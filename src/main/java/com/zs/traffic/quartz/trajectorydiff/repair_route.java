/**
 * Copyright (C), 2015-2020, XXX有限公司
 * FileName: newroute
 * Author:   dellxh
 * Date:     2020/10/16 19:58
 * Description: INput  ID and TIME  OUPUT TRACTORY
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zs.traffic.quartz.trajectorydiff;

import com.zs.traffic.quartz.trajectorydiff.dao.OutdoorLocationDao;
import com.zs.traffic.quartz.trajectorydiff.entity.Gps;
import com.zs.traffic.quartz.trajectorydiff.entity.Main;
import com.zs.traffic.quartz.trajectorydiff.entity.Trajectory;
import net.sf.json.JSONObject;
import org.apache.xmlbeans.impl.jam.provider.JamClassPopulator;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 〈一句话功能简述〉<br> 
 * 〈INput  ID and TIME  OUPUT TRACTORY〉
 *
 * @author dellxh
 * @create 2020/10/16
 * @since 1.0.0
 */
public class repair_route {
    private static final String BAIDU_APP_KEY = "zGXRgza5WxGuiGGWSvmxYX2M";
    public static void main(String[] args)  {
        start();
    }

    public static void start(){

        Scanner scanner = new Scanner(System.in);//建立Scanner对象，通过System.in来读取键盘输入数据
        System.out.println("请输入你的ID：");
        String ID1 = scanner.nextLine();
        int ID =Integer.valueOf(ID1);
        //=scanner.nextInt();
        //String ID = scanner.nextLine();//字符串输入语句名字name
        System.out.println("请输入年份：");
        String year = scanner.nextLine();//字符串输入语句爱好favor
        System.out.println("请输入月份：");
        String month = scanner.nextLine();//字符串输入语句爱好favor
        System.out.println("请输入日期：");
        String day = scanner.nextLine();//字符串输入语句爱好favor
        if (month.length() == 1) {
            month = "0" + month;
        }
        if (day.length() == 1) {
            day = "0" + day;
        }
        String s = "trajectory"+"_" + year + "_" + month + "_" + day;

        OutdoorLocationDao outdoorLocationDao = new OutdoorLocationDao();
        List<Gps> gpsList = outdoorLocationDao.searchselfInGps(ID,s);
        List<Trajectory> trajectoryList = Main.getTrajectory(gpsList);

        int num=0;

        for(Trajectory trajectory : trajectoryList){
//            String[] split = trajectory.getStart_gps().split(",");
//            String start_address = getAddress(split[0], split[1]);
//            String[] split2 =  trajectory.getEnd_gps().split(",");
//            String end_address = getAddress(split2[0], split2[1]);
            String start_address = trajectory.getStart_address();
            String end_address = trajectory.getEnd_address();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
            outdoorLocationDao.insert(trajectory.getAppid(),trajectory.getStartTime(),trajectory.getEndime(),start_address,end_address,trajectory.getStart_gps(),trajectory.getEnd_gps(),0,df.format(new Date()),trajectory.getRanking());
            num++;
        }
        System.out.println("num  :   "+ --num);

        int[] infor = outdoorLocationDao.Information();
        for(int i=0;i<infor.length;i++){
            int min = outdoorLocationDao.findMin(infor[i]);
            //System.out.println(min);
            if (min!=1){
                // System.out.println("deleMinid :  "+ infor[i]);
                outdoorLocationDao.delMin(infor[i]);
                outdoorLocationDao.updateMin(infor[i],min);
            }

            int max = outdoorLocationDao.findMax(infor[i]);
            int rankingMax = outdoorLocationDao.rankingMax(infor[i]);
            if(max!=rankingMax){
                outdoorLocationDao.delMax(infor[i],rankingMax);
                // System.out.println("appid:  "+infor[i]   +  "    deleMAX:   "+ rankingMax);
                outdoorLocationDao.updateMax(rankingMax,infor[i],max);
            }

        }

    }

    public static String getAddress(String lng,String lat){
        String res;
        String address=null ;
        try {
            URL resjson = new URL("http://api.map.baidu.com/geocoder/v2/?callback=renderReverse&location="+lat+","+lng+"&output=json&pois=1&ak="+BAIDU_APP_KEY);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    resjson.openStream(), StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder("");
            while ((res = in.readLine()) != null) {
                sb.append(res.trim());
            }
            in.close();
            String str = sb.toString();
            String subStr = str.substring(str.indexOf('(')+1, str.indexOf("})")+1);
            JSONObject jsonObj = JSONObject.fromObject(subStr);
//获取匹配到的中文地址
            address=(String) jsonObj.getJSONObject("result").get("formatted_address");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return address;
    }
}
