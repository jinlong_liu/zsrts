package com.zs.traffic.quartz.trajectorydiff.entity;

import java.util.Date;

public class Trajectory {

    private int appid;

    private Date startTime;

    private Date endime;

    private String start_gps;

    private String end_gps;

    private int ranking;

    private String start_address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String end_address;

    private String address;

    public String getEnd_address() {
        return end_address;
    }

    public void setEnd_address(String end_address) {
        this.end_address = end_address;
    }

    public String getStart_address() {
        return start_address;
    }

    public void setStart_address(String start_address) {
        this.start_address = start_address;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public int getAppid() {
        return appid;
    }

    public void setAppid(int appid) {
        this.appid = appid;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndime() {
        return endime;
    }

    public void setEndime(Date endime) {
        this.endime = endime;
    }

    public String getStart_gps() {
        return start_gps;
    }

    public void setStart_gps(String start_gps) {
        this.start_gps = start_gps;
    }

    public String getEnd_gps() {
        return end_gps;
    }

    public void setEnd_gps(String end_gps) {
        this.end_gps = end_gps;
    }
}