package com.zs.traffic.quartz.trajectorydiff.dao;


import org.apache.commons.dbcp2.BasicDataSource;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;//main/java/config/druid.properties

public class JDBCUtil {
    /**
     * 连接池
     */
    //static DataSource dataSource = null;
    private static BasicDataSource datasource = new BasicDataSource();


    /**
     * 静态代码块，用来初始化连接池

     static {
     try {
     //1.通过类加载器(任意一个即可，因为运行期间获取的类加载器都是Application ClassLoader)将properties文件变成输入流
     // name需要改成自己的，或者按找我这个目录 src->config->druid.properties
     InputStream is = JDBCUtil.class.getClassLoader().getResourceAsStream("main/java/config/druid.properties");
     Properties properties = new Properties();
     // 加载输入流
     properties.load(is);
     //2.创建指定参数的数据库连接池
     dataSource = DruidDataSourceFactory.createDataSource(properties);
     System.out.println("创建指定参数的数据库连接池");
     } catch (Exception e) {

     e.printStackTrace();
     System.out.println("创建指定参数的数据库连接池error  ERROR");
     }
     }*/

    static{
        try {
//            URL conf = JDBCUtil.class.getClassLoader().getResource("conf/jdbc.properties");
//            FileInputStream fis = new FileInputStream(conf.getFile());
//            FileInputStream fis = new FileInputStream("/usr/local/tomcat/jdbc.properties");
            FileInputStream fis = new FileInputStream("E:\\learning\\MIDEA\\zsrts\\src\\main\\resources\\conf\\jdbc.properties");
            //创建Properties对象
            Properties pro = new Properties();
            //从流中加载数据
            pro.load(fis);
            //关闭流
            fis.close();
            //从Properties对象中根据键读取值
            String driverClass = pro.getProperty("jdbc_driverClassName");
            System.out.println(driverClass);
            String url = pro.getProperty("jdbc_url");
            String username = pro.getProperty("jdbc_username");
            String password = pro.getProperty("jdbc_password");
            //数据库连接信息配置
            datasource.setDriverClassName(driverClass);
            datasource.setUrl(url);
            datasource.setUsername(username);
            datasource.setPassword(password);
            //对象连接池中的连接数量配置,可选的
//        datasource.setInitialSize(10);
            datasource.setMaxIdle(5);
            datasource.setMinIdle(1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 获取当前连接池
     * @return
     */
    public static DataSource getDataSource() {
        return  datasource;
    }

    /**
     * 获取Connection
     * @return Connection
     */
    public static Connection getConnection(){
        Connection connection = null;
        try {
            // 建立Connection
            connection = datasource.getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }


    /**
     * 获取Connection，并开启事务
     * @return Connection
     */
    public static Connection getConnectionWithTransaction(){
        Connection connection = null;
        try {
            connection = datasource.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }
}
