package com.zs.traffic.quartz.trajectorydiff.dao;



import com.zs.traffic.quartz.trajectorydiff.entity.Gps;
import com.zs.traffic.quartz.trajectorydiff.entity.Trajectory;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
//import org.jmqtt.utils.JDBCUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;

import javax.lang.model.SourceVersion;
import java.sql.SQLException;
import java.util.*;

public class OutdoorLocationDao {
    QueryRunner queryRunner;

    private static final Logger logger = LoggerFactory.getLogger(OutdoorLocationDao.class); //获取logger实例

    public int[] selectUserIdByCharge() {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            //编写sql语句
            String sql = "SELECT distinct appuser_id FROM charge ;";
            int num = 0;
            //设置占位符
            //执行语句
            if (queryRunner.query(sql, new MapListHandler()) != null) {
                //System.out.println(queryRunner.query(sql, new ColumnListHandler(),params));;
                //Object number = String.valueOf(queryRunner.query(sql, new MapHandler()));
                List<Map<String, Object>> list = queryRunner.query(sql, new MapListHandler());
                int[] array = new int[list.size()];
                for (Map<String, Object> map : list) {
                    for (String key : map.keySet()) {
                        //System.out.print(key + "–>" + map.get(key) + " ");
                        array[num] = (int) map.get(key);
                        num++;
                    }
                }
                for (int i = 0; i < array.length; i++) {
                    //System.out.println("array  :   " + array[i]);
                }

                return array;
            } else {
                //logger.error("查不到mac地址对应的Userid，检查是否绑定手环");
                return null;
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //logger.error("根据mac查询id出错原因："+e);
            return null;
        }
    }

    public int[] selectCheckOut() {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            //编写sql语句
            String sql = "select appuser_id from charge where is_charge_status = 0;";
            int num = 0;
            //设置占位符
            //执行语句
            if (queryRunner.query(sql, new MapListHandler()) != null) {
                //System.out.println(queryRunner.query(sql, new ColumnListHandler(),params));;
                //Object number = String.valueOf(queryRunner.query(sql, new MapHandler()));
                List<Map<String, Object>> list = queryRunner.query(sql, new MapListHandler());
                int[] array = new int[list.size()];
                for (Map<String, Object> map : list) {
                    for (String key : map.keySet()) {
                        //System.out.print(key + "–>" + map.get(key) + " ");
                        array[num] = (int) map.get(key);
                        num++;
                    }
                }
                for (int i = 0; i < array.length; i++) {
                    System.out.println("array  :   " + array[i]);
                }

                return array;
            } else {
                //logger.error("查不到mac地址对应的Userid，检查是否绑定手环");
                return null;
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //logger.error("根据mac查询id出错原因："+e);
            return null;
        }
    }


    public int[] selectUserIdByInformation() {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            //编写sql语句
            String sql = "SELECT distinct appuser_id from travel_information where  status = 1;";
            int num = 0;
            //设置占位符
            //Object[] params = {};
            //执行语句
            if (queryRunner.query(sql, new MapListHandler()) != null) {
                //System.out.println(queryRunner.query(sql, new ColumnListHandler(),params));;
                List<Map<String, Object>> list = queryRunner.query(sql, new MapListHandler());
                int[] array = new int[list.size()];
                for (Map<String, Object> map : list) {
                    for (String key : map.keySet()) {
                        //System.out.print(key + "–>" + map.get(key) + " ");
                        array[num] = (int) map.get(key);
                        num++;
                    }
                }
                for (int i = 0; i < array.length; i++) {
                    //.out.println("array  :   " + array[i]);
                }
                return array;
            } else {
                //logger.error("查不到mac地址对应的Userid，检查是否绑定手环");
                return null;
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //logger.error("根据mac查询id出错原因："+e);
            return null;
        }
    }


    public int[] selectUserIdByAll() {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            //编写sql语句
            Calendar now = Calendar.getInstance();
           // System.out.println("年: " + now.get(Calendar.YEAR));
           // System.out.println("月: " + (now.get(Calendar.MONTH) + 1) + "");
           // System.out.println("日: " + now.get(Calendar.DAY_OF_MONTH));
            int month = (now.get(Calendar.MONTH) + 1);
            int day = now.get(Calendar.DAY_OF_MONTH);
            String months = String.valueOf(now.get(Calendar.MONTH) + 1);
            String days = String.valueOf(now.get(Calendar.DAY_OF_MONTH));
            if (months.length() == 1) {
                months = "0" + months;
            }
            if (days.length() == 1) {
                days = "0" + days;
            }

            String s = "_" + now.get(Calendar.YEAR) + "_" + months + "_" + days;
            //String s = "_" + now.get(Calendar.YEAR) + "_" + months + "_" + 14;
          //  System.out.println(s);
            String sql = "SELECT distinct appuser_id FROM trajectory" + s + " where trajectory_gps != '0,0';";
           // System.out.println(" selectUserIdByAll:    " + sql);
            int num = 0;
            //设置占位符
            //Object[] params = {};
            //执行语句
            if (queryRunner.query(sql, new MapListHandler()) != null) {
                //System.out.println(queryRunner.query(sql, new ColumnListHandler(),params));;
                List<Map<String, Object>> list = queryRunner.query(sql, new MapListHandler());
                int[] array = new int[list.size()];
                for (Map<String, Object> map : list) {
                    for (String key : map.keySet()) {
                       // System.out.print(key + "–>" + map.get(key) + " ");
                        array[num] = (int) map.get(key);
                        num++;
                    }
                }
                for (int i = 0; i < array.length; i++) {
                 //   System.out.println("array  :   " + array[i]);
                }
                return array;
            } else {
                //logger.error("查不到mac地址对应的Userid，检查是否绑定手环");
                return null;
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //logger.error("根据mac查询id出错原因："+e);
            return null;
        }
    }


    public List<Gps> searchInGps(int id) {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        List<Gps> gpsList = new ArrayList<>();
        try {
            //编写sql语句
            Calendar now = Calendar.getInstance();
            //System.out.println("年: " + now.get(Calendar.YEAR));
            //System.out.println("月: " + (now.get(Calendar.MONTH) + 1) + "");
            //System.out.println("日: " + now.get(Calendar.DAY_OF_MONTH));
            int month = (now.get(Calendar.MONTH) + 1);
            int day = now.get(Calendar.DAY_OF_MONTH);
            String months = String.valueOf(now.get(Calendar.MONTH) + 1);
            String days = String.valueOf(now.get(Calendar.DAY_OF_MONTH));
            if (months.length() == 1) {
                months = "0" + months;
            }
            if (days.length() == 1) {
                days = "0" + days;
            }

            String s = "_" + now.get(Calendar.YEAR) + "_" + months + "_" + days;
            //String s = "_" + now.get(Calendar.YEAR) + "_" + months + "_" + 14;
            //System.out.println(s);
            String sql = "SELECT * FROM trajectory" + s + " where appuser_id =? and trajectory_gps != '0,0' Order By start_date;";
            //设置占位符
            Object[] params = {id};
            //执行语句
            if (queryRunner.query(sql, new MapListHandler(),params) != null) {
                //System.out.println(queryRunner.query(sql, new ColumnListHandler(),params));;
                List<Map<String, Object>> list = queryRunner.query(sql, new MapListHandler(),params);
               // System.out.println(list);
                int[] appid = new int[list.size()];
                Date[] time = new Date[list.size()];
//                String[] Start_address = new String[list.size()];
//                String[] End_address = new String[list.size()];
                String[] address = new String[list.size()];
                String[] split = new String[list.size()];
                int num = 0;
                for (Map<String, Object> map : list) {
                    for (String key : map.keySet()) {
                        Gps gps = new Gps();
                        if (key.equals("appuser_id")) {
                            appid[num] = (int) map.get(key);
                            //System.out.println("address:    "+ num);
                            //System.out.println(appid[num]);
                            //gps.setAppid((Integer) map.get(key));
                            //System.out.println("appod::::::"+gps.getAppid());
                        }
                        if (key.equals("start_date")) {
                            time[num] = (Date) map.get(key);
                            //System.out.println(time[num]);
                            //gps.setTime((Date) map.get(key));
                            //System.out.println("date:    "+ gps.getTime());
                        }
                        if (key.equals("trajectory_gps")) {
                            split[num] = ((String) map.get(key));
                        }
                        if (key.equals("address")) {
                            //System.out.println("address:    "+ map.get(key));
                            //System.out.println("address:    "+ num);
                            address[num] = ((String) map.get(key));
                            gps.setAppid(appid[num]);
                            gps.setTime(time[num]);
                            String[] sp = split[num].split(",");
                            gps.setLat(sp[1]);
                            //System.out.println(sp[1]);
                            gps.setLog(sp[0]);
                            gps.setAddress(address[num]);
                            //System.out.println(gps.getAppid());
                            num++;
                            //System.out.println("lat: "+ gps.getLat()   + "   log:  "+ gps.getLog());
                            gpsList.add(gps);
                        }
                       // System.out.println(key);
                       // System.out.println(key + "–>" + map.get(key) + " ");
                    }
                }

                return gpsList;
            } else {
                //logger.error("查不到mac地址对应的Userid，检查是否绑定手环");
                return null;
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //logger.error("根据mac查询id出错原因："+e);
            return null;
        }
    }

    public List<Gps> searchselfInGps(int id,String table ) {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        List<Gps> gpsList = new ArrayList<>();
        try {
            //编写sql语句
            Calendar now = Calendar.getInstance();
            //System.out.println("年: " + now.get(Calendar.YEAR));
            //System.out.println("月: " + (now.get(Calendar.MONTH) + 1) + "");
            //System.out.println("日: " + now.get(Calendar.DAY_OF_MONTH));
            //String s = "_" + now.get(Calendar.YEAR) + "_" + months + "_" + 14;
            //System.out.println(s);
            String sql = "SELECT * FROM "+ table+ " where appuser_id = ? and trajectory_gps != '0,0' Order By start_date;";
            //设置占位符
            Object[] params = {id};
            //执行语句
            if (queryRunner.query(sql, new MapListHandler(),params) != null) {
                //System.out.println(queryRunner.query(sql, new ColumnListHandler(),params));;
                List<Map<String, Object>> list = queryRunner.query(sql, new MapListHandler(),params);
                // System.out.println(list);
                //int[] appid = new int[list.size()];
                Date[] time = new Date[list.size()];
                String[] address = new String[list.size()];
                String[] split = new String[list.size()];
                int num = 0;
                for (Map<String, Object> map : list) {
                    for (String key : map.keySet()) {
                        Gps gps = new Gps();
//                        if (key.equals("appuser_id")) {
//                            appid[num] = (int) map.get(key);
//                            //System.out.println(appid[num]);
//                            //gps.setAppid((Integer) map.get(key));
//                            //System.out.println("appod::::::"+gps.getAppid());
//                        }
                        if (key.equals("start_date")) {
                            time[num] = (Date) map.get(key);
                            //System.out.println(time[num]);
                            //gps.setTime((Date) map.get(key));
                            //System.out.println("date:    "+ gps.getTime());
                        }

                        if (key.equals("trajectory_gps")) {
                            split[num] = ((String) map.get(key));
                        }
                        if (key.equals("address")) {
                            //System.out.println("address:    "+ map.get(key));
                            System.out.println("address:    "+ num);
                            address[num] = ((String) map.get(key));
                            //gps.setAppid(appid[num]);
                            gps.setTime(time[num]);
                            String[] sp = split[num].split(",");
                            gps.setLat(sp[1]);
                            //System.out.println(sp[1]);
                            gps.setLog(sp[0]);
                            gps.setAddress(address[num]);
                            //System.out.println(gps.getAppid());
                            num++;
                            //System.out.println("lat: "+ gps.getLat()   + "   log:  "+ gps.getLog());
                            gpsList.add(gps);
                            System.out.println(gpsList.get(num-1));
                        }
                        // System.out.println(key);
                        // System.out.println(key + "–>" + map.get(key) + " ");
                    }
                }
                return gpsList;
            } else {
                //logger.error("查不到mac地址对应的Userid，检查是否绑定手环");
                return null;
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //logger.error("根据mac查询id出错原因："+e);
            return null;
        }
    }

    public void delBefore(int id) {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            String sql = "delete from travel_information where appuser_id = ? and  id >= 0 and status = 0";
            queryRunner.update(sql, id);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void delCheckOut(int appid){
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            String sql = "delete from charge where is_charge_status = 0 and id>=0 and appuser_id = ?";
            queryRunner.update(sql,appid);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void delCheckOutInformation(int appid){
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            String sql = "delete from travel_information where appuser_id = ? and status = 1 and id>=0;";
            queryRunner.update(sql,appid);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void insert(int appuser_id, Date start_date, Date end_date, String start_address, String end_address, String start_gps, String end_gps, int status, String date, int ranking) {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            String sql = "insert into travel_information (appuser_id,start_date,end_date,start_address,end_address,start_gps,end_gps,status,date,ranking ) values (?,?,?,?,?,?,?,?,?,?)";
            Object[] params = {appuser_id, start_date, end_date, start_address, end_address, start_gps, end_gps, status, date, ranking};
            queryRunner.update(sql, params);
        } catch (Exception e) {
            System.out.println(e);
        }
    }



    public void delMin(int appuserid){
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            String sql = "delete from travel_information where  appuser_id = ? and ranking=1 and id >=0";
            queryRunner.update(sql,appuserid);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void delMax(int appuserid,int rankingMax){
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            String sql = "delete from travel_information where  appuser_id = ?  and id >=0 and ranking = ?";
            queryRunner.update(sql,appuserid,rankingMax);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void updateMin(int appuser_id,int ranking){
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            String sql = "update travel_information set ranking = 1 where appuser_id = ? and ranking = ? and  id>=0";
            Object[] params = {appuser_id,ranking};
            queryRunner.update(sql, params);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void updateMax(int rankMax,int appuser_id,int ranking){
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            String sql = "update travel_information set ranking = ? where  appuser_id = ? and ranking = ? and id>=0";
            Object[] params = {rankMax,appuser_id,ranking};
            queryRunner.update(sql, params);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public int findMin(int appid){
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try{
            String sql = "select ranking from travel_information where  start_date = (select min(start_date) from travel_information where appuser_id = ?)";
            Object[] params = {appid};
            if(queryRunner.query(sql,new ScalarHandler<>(), params)!=null){
                int ranking = (int) queryRunner.query(sql, new ScalarHandler<>(), params);
               return ranking;
            }
        }catch (Exception e){
            System.out.println(e);
        }
        return 0;
    }


   public int findMax(int appid){
       queryRunner = new QueryRunner(JDBCUtil.getDataSource());
       try{
           String sql = "select ranking from travel_information where end_date = (select max(end_date) from travel_information where appuser_id = ?)";
           Object[] params = {appid};
           if(queryRunner.query(sql,new ScalarHandler<>(), params)!=null){
               int ranking = (int) queryRunner.query(sql, new ScalarHandler<>(), params);
               return ranking;
           }
       }catch (Exception e){
           System.out.println(e);
       }
       return 0;
   }

   public int rankingMax(int appid){
       queryRunner = new QueryRunner(JDBCUtil.getDataSource());
       try{
           String sql = "select max(ranking) from travel_information where appuser_id = ?";
           Object[] params = {appid};
           if(queryRunner.query(sql,new ScalarHandler<>(), params)!=null){
               int ranking = (int) queryRunner.query(sql, new ScalarHandler<>(), params);
               return ranking;
           }
       }catch (Exception e){
           System.out.println(e);
       }
       return 0;
   }



    public int[] Information() {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            //编写sql语句
            String sql = "SELECT distinct appuser_id FROM travel_information ";
            int num = 0;
            //设置占位符
            //执行语句
            if (queryRunner.query(sql, new MapListHandler()) != null) {
                //System.out.println(queryRunner.query(sql, new ColumnListHandler(),params));;
                //Object number = String.valueOf(queryRunner.query(sql, new MapHandler()));
                List<Map<String, Object>> list = queryRunner.query(sql, new MapListHandler());
                int[] array = new int[list.size()];
                for (Map<String, Object> map : list) {
                    for (String key : map.keySet()) {
                       // System.out.print(key + "–>" + map.get(key) + " ");
                        array[num] = (int) map.get(key);
                        num++;
                    }
                }
                for (int i = 0; i < array.length; i++) {
                    //System.out.println("array  :   " + array[i]);
                }

                return array;
            } else {
                //logger.error("查不到mac地址对应的Userid，检查是否绑定手环");
                return null;
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //logger.error("根据mac查询id出错原因："+e);
            return null;
        }
    }


    public List<Trajectory> check(int appid) {
        queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        List<Trajectory> trajectoryList = new ArrayList<>();
        try {
            String sql = "SELECT * from travel_information where appuser_id = ?;";
            if (queryRunner.query(sql, new MapListHandler(),appid) != null) {
                List<Map<String, Object>> list = queryRunner.query(sql, new MapListHandler(),appid);
               // System.out.println(list);
                int[] app_id = new int[list.size()];
                Date[] time = new Date[list.size()];
                Date[] time2 = new Date[list.size()];
                int[] rankingAll = new int[list.size()] ;
                int num = 0;
                for (Map<String, Object> map : list) {
                    for (String key : map.keySet()) {
                        Trajectory trajectory = new Trajectory();
                        if (key.equals("appuser_id")) {
                            app_id[num] = (int) map.get(key);
                            //System.out.println(app_id[num]);
                            //gps.setAppid((Integer) map.get(key));
                            //System.out.println("appod::::::"+gps.getAppid());
                        }
                        if (key.equals("start_date")) {
                            time[num] = (Date) map.get(key);
                            //System.out.println(time[num]);
                            //gps.setTime((Date) map.get(key));
                            //System.out.println("date:    "+ gps.getTime());
                        }
                        if (key.equals("end_date")) {
                            time2[num] = (Date) map.get(key);
                            //System.out.println(time2[num]);
                            //gps.setTime((Date) map.get(key));
                            //System.out.println("date:    "+ gps.getTime());
                        }
                        if (key.equals("ranking")) {
                            rankingAll[num] = (int) map.get(key);
                            trajectory.setAppid(app_id[num]);
                            trajectory.setStartTime(time[num]);
                            trajectory.setEndime(time2[num]);
                            trajectory.setRanking(rankingAll[num]);
                            num++;
                            //System.out.println("lat: "+ gps.getLat()   + "   log:  "+ gps.getLog());
                            trajectoryList.add(trajectory);
                        }

                       // System.out.println(key);
                        //System.out.println(key + "–>" + map.get(key) + " ");
                    }
                }
                return trajectoryList;
            }
        }catch (Exception e){
            System.out.println(e);
        }
        return null;
    }

}
