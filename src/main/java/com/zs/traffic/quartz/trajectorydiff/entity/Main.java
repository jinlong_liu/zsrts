package com.zs.traffic.quartz.trajectorydiff.entity;

import com.zs.traffic.quartz.trajectorydiff.dao.OutdoorLocationDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Main {


    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    public static void main(String[] args) {
        Main main = new Main();
        OutdoorLocationDao outdoorLocationDao = new OutdoorLocationDao();
        main.getTrajectory(outdoorLocationDao.searchInGps(11));

    }
    /**
     * 轨迹划分算法
     *
     * @param gpsList gps点的集合
     * @return 轨迹的集合
     */
    public static List<Trajectory> getTrajectory(List<Gps> gpsList) {
        Gps[] gpsArray = gpsList.toArray(new Gps[gpsList.size()]);
        List<Trajectory> trajectoryList = new ArrayList<>();

        Gps[] gps = gpsList.toArray(new Gps[gpsList.size()]);
        List<Gps> gpsArray1 = new ArrayList<>();
        for(int i = 0;i<gps.length;i++){
            if (!gps[i].getLat().equals(0)) {
                gpsArray1.add(gps[i]);
            }
        }
        gpsArray = gpsArray1.toArray(new Gps[gpsArray1.size()]);
        trajectoryList = new ArrayList<>();

        if (gpsArray.length!=0) {
            String pointA[];
            String pointB[];
            ArrayList[] result = new ArrayList[gpsArray.length];
            ArrayList[] AroundPointsID = new ArrayList[gpsArray.length];  //定义一个数组，用于保存i小于300范围内的所有点ID
            int m = 0;
            int nexterrorpoint = 0;
            int arrestpoint;
            int errorpoint;
            int index = 0;
            ArrayList<Integer> routecount = new ArrayList<>();
            routecount.add(0);
            List<Integer> tempPoint = new ArrayList<>();

            //判断任何两点之间的时间差是否大于1个小时，如果大于的话，则保存轨迹点的编号到异常点集合中，直至遍历完所有点。作用是使用异常轨迹点把轨迹分开讨论，避免新旧数据的交叉对停留点的影响

            //System.out.println(gpsArray[0].getLat());
            for ( m = 0; m < gpsList.size() - 1; m++) {
                long t = 0;
                Date d1 = gpsArray[m].getTime();
                Date d2 = gpsArray[m + 1].getTime();
                long diff = d2.getTime()-d1.getTime();
                t = diff/1000;
                if (t > 1800) {
                    routecount.add(m);
                }
            }
            routecount.add(m);
            //System.out.println(routecount);

            //对异常点集合进行分析，对两个异常点之间的数据进行分析，通过以每个轨迹点为圆心，将距离圆心300米以内的点保存到不同的数据集合中。

            for (m = 0; m < routecount.size() - 1; m++) {
                errorpoint = routecount.get(m);
                if(errorpoint!=0){
                    errorpoint=errorpoint+1;
                }
                nexterrorpoint = routecount.get(m + 1);
                arrestpoint = 0;
                for (int i = errorpoint; i < nexterrorpoint; i++) {
                    AroundPointsID[i] = new ArrayList();
                    pointA = new String[]{gpsArray[i].getLog(), gpsArray[i].getLat()};
                    for (int j = i + 1; j < nexterrorpoint; j++) {
                        pointB = new String[]{gpsArray[j].getLog(), gpsArray[j].getLat()};///arrayList1.get(j), arrayList2.get(j)
                        double dist = getDistance(Double.parseDouble(pointA[0]), Double.parseDouble(pointA[1]), Double.parseDouble(pointB[0]), Double.parseDouble(pointB[1]));
                        if (dist < 100) {
                            AroundPointsID[i].add(j);
                        } else {
                            break;
                        }
                    }
                    for (int j = i; j >= errorpoint; j--) {
                        pointB = new String[]{gpsArray[j].getLog(), gpsArray[j].getLat()};
                        double dist = getDistance(Double.parseDouble(pointA[0]), Double.parseDouble(pointA[1]), Double.parseDouble(pointB[0]), Double.parseDouble(pointB[1]));
                        if (dist < 100) {
                            AroundPointsID[i].add(j);
                        } else {
                            break;
                        }
                    }
                }

                //对都有某个轨迹点的两个集合进行点的个数比较，直到确定出包含该轨迹点点数最多的集合，若该集合点的个数大于5个（即在该点聚集时间大于等于30分钟），则将该点视为停留点

                tempPoint = AroundPointsID[errorpoint];

                for (int k = errorpoint + 1; k < nexterrorpoint; k++) {
                    if (isArrayContainObj(tempPoint, k)) {
                        if (tempPoint.size() < AroundPointsID[k].size()) {
                            tempPoint = AroundPointsID[k];
                        }
                    } else {
                        if (tempPoint.size() > 3) {
                            result[arrestpoint] = (ArrayList) tempPoint;
                            arrestpoint++;
                        }
                        if (k < nexterrorpoint - 1) {
                            tempPoint = AroundPointsID[(k + 1)];
                        }
                    }
                }
                System.out.println(tempPoint);
                //将每个轨迹段的信息进行输出。

                List<Integer> temp = new ArrayList<Integer>();
                List<Integer> temp2 = new ArrayList<Integer>();
                Trajectory trajectory1 = new Trajectory();
                String stime;
                String etime;
                long timediff;
                Double length=0.0;
                //System.out.println(gpsArray[errorpoint].getTime());
                //System.out.println(gpsArray[nexterrorpoint].getTime());
                if(arrestpoint==0 && gpsArray[errorpoint].getTime() != gpsArray[nexterrorpoint].getTime()){
                    String start_gps = gpsArray[errorpoint].getLog() + "," + gpsArray[errorpoint].getLat();
                    String end_gps = gpsArray[nexterrorpoint].getLog() + "," + gpsArray[nexterrorpoint].getLat();
                    for(int i=errorpoint; i < nexterrorpoint;i++){
                        pointA = new String[]{gpsArray[i].getLog(), gpsArray[i].getLat()};
                        pointB = new String[]{gpsArray[i+1].getLog(), gpsArray[i+1].getLat()};
                        double dist = getDistance(Double.parseDouble(pointA[0]), Double.parseDouble(pointA[1]),
                                Double.parseDouble(pointB[0]), Double.parseDouble(pointB[1]));
                        length=length+dist;
                    }
                    stime=dateToString(gpsArray[errorpoint].getTime(),"yyyy-MM-dd HH:mm:ss");
                    etime=dateToString(gpsArray[nexterrorpoint].getTime(),"yyyy-MM-dd HH:mm:ss");
                    timediff=(gpsArray[nexterrorpoint].getTime().getTime()-gpsArray[errorpoint].getTime().getTime())/1000;
                    String Timediff = timediff/3600+":"+timediff%3600/60+":"+timediff%3600%60;
                    //System.out.println("4");
                    String Start_address=gpsArray[errorpoint].getAddress();
                    //System.out.println(Start_address);
                    String End_address = gpsArray[nexterrorpoint].getAddress();
                    //System.out.println(Timediff);
                    System.out.println("1111起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime+ "      " + "轨迹时间:" + Timediff + "      " + "轨迹长度:" + length);
                    index ++;
                    trajectory1 = new Trajectory();
                    trajectory1.setAppid(gpsArray[0].getAppid());
                    trajectory1.setStart_gps(start_gps);
                    trajectory1.setStartTime(gpsArray[errorpoint].getTime());
                    trajectory1.setEnd_gps(end_gps);
                    trajectory1.setEndime(gpsArray[nexterrorpoint].getTime());
                    trajectory1.setRanking(index);
                    trajectory1.setStart_address(Start_address);
                    trajectory1.setEnd_address(End_address);
                    trajectoryList.add(trajectory1);
                }else if (arrestpoint!=0){
                    temp = result[0];
                    String start_gps = gpsArray[errorpoint].getLog() + "," + gpsArray[errorpoint].getLat();
                    String end_gps = gpsArray[Collections.max(temp)].getLog() + "," + gpsArray[Collections.max(temp)].getLat();
                    if(gpsArray[0].getTime()!= gpsArray[Collections.min(temp)].getTime()){
                        length = 0.0;
                        stime=dateToString(gpsArray[errorpoint].getTime(),"yyyy-MM-dd HH:mm:ss");
                        etime=dateToString(gpsArray[Collections.max(temp)].getTime(),"yyyy-MM-dd HH:mm:ss");
                        timediff=(gpsArray[Collections.max(temp)].getTime().getTime()-gpsArray[errorpoint].getTime().getTime())/1000;
                        String Timediff = timediff/3600+":"+timediff%3600/60+":"+timediff%3600%60;
                        for(int i=errorpoint; i < Collections.max(temp); i++){
                            pointA = new String[]{gpsArray[i].getLog(), gpsArray[i].getLat()};
                            pointB = new String[]{gpsArray[i+1].getLog(), gpsArray[i+1].getLat()};
                            double dist = getDistance(Double.parseDouble(pointA[0]), Double.parseDouble(pointA[1]), Double.parseDouble(pointB[0]), Double.parseDouble(pointB[1]));
                            length=length+dist;
                        }
                        //System.out.println("3");
                        String Start_address=gpsArray[errorpoint].getAddress();
                        String End_address =gpsArray[Collections.max(temp)].getAddress();
                        //System.out.println(Timediff);
                        System.out.println("2222起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime+ "      " + "轨迹时间:" + Timediff + "      " + "轨迹长度:" + length );
                        index ++;
                        trajectory1 = new Trajectory();
                        trajectory1.setAppid(gpsArray[0].getAppid());
                        trajectory1.setStart_gps(start_gps);
                        trajectory1.setStartTime(gpsArray[errorpoint].getTime());
                        trajectory1.setEnd_gps(end_gps);
                        trajectory1.setEndime(gpsArray[Collections.max(temp)].getTime());
                        trajectory1.setRanking(index);
                        trajectory1.setStart_address(Start_address);
                        trajectory1.setEnd_address(End_address);
                        trajectoryList.add(trajectory1);
                    }
                    for (int count = 0; count < arrestpoint - 1; count++) {
                        length = 0.0;
                        temp = result[count];
                        temp2 = result[count + 1];
                        start_gps = gpsArray[Collections.max(temp)].getLog() + "," + gpsArray[Collections.max(temp)].getLat();
                        end_gps = gpsArray[Collections.max(temp2)].getLog() + "," + gpsArray[Collections.max(temp2)].getLat();
                        //double distance = getDistance(Double.parseDouble(gpsArray[Collections.max(temp)].getLog()), Double.parseDouble(gpsArray[Collections.max(temp)].getLat()), Double.parseDouble(gpsArray[Collections.max(temp2)].getLog()), Double.parseDouble(gpsArray[Collections.max(temp2)].getLat()));
                        //if (distance > 100) {
                        stime = dateToString(gpsArray[Collections.max(temp)].getTime(), "yyyy-MM-dd HH:mm:ss");
                        etime = dateToString(gpsArray[Collections.max(temp2)].getTime(), "yyyy-MM-dd HH:mm:ss");
                        timediff = (gpsArray[Collections.max(temp2)].getTime().getTime() - gpsArray[Collections.max(temp)].getTime().getTime()) / 1000;
                        String Timediff = timediff / 3600 + ":" + timediff % 3600 / 60 + ":" + timediff % 3600 % 60;
                        for (int i = Collections.max(temp); i < Collections.max(temp2); i++) {
                            pointA = new String[]{gpsArray[i].getLog(), gpsArray[i].getLat()};
                            pointB = new String[]{gpsArray[i + 1].getLog(), gpsArray[i + 1].getLat()};
                            double dist = getDistance(Double.parseDouble(pointA[0]), Double.parseDouble(pointA[1]), Double.parseDouble(pointB[0]), Double.parseDouble(pointB[1]));
                            length = length + dist;
                        }
                        //System.out.println("2");
                        String Start_address=gpsArray[Collections.max(temp)].getAddress();
                        String End_address = gpsArray[Collections.max(temp2)].getAddress();
                        //System.out.println(Timediff);
                        System.out.println("3333起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime + "      " + "轨迹时间:" + Timediff + "      " + "轨迹长度:" + length);
                        index++;
                        trajectory1 = new Trajectory();
                        trajectory1.setAppid(gpsArray[0].getAppid());
                        trajectory1.setStart_gps(start_gps);
                        trajectory1.setStartTime(gpsArray[Collections.max(temp)].getTime());
                        trajectory1.setEnd_gps(end_gps);
                        trajectory1.setEndime(gpsArray[Collections.max(temp2)].getTime());
                        trajectory1.setRanking(index);
                        trajectory1.setStart_address(Start_address);
                        trajectory1.setEnd_address(End_address);
                        trajectoryList.add(trajectory1);
                        temp.clear();
                        //}
                    }
                    length = 0.0;
                    temp = result[arrestpoint - 1];
                    start_gps = gpsArray[Collections.max(temp)].getLog() + "," + gpsArray[Collections.max(temp)].getLat();
                    end_gps = gpsArray[nexterrorpoint].getLog() + "," + gpsArray[nexterrorpoint].getLat();
                    if(gpsArray[nexterrorpoint].getTime()!= gpsArray[Collections.max(temp)].getTime()){
                        stime=dateToString(gpsArray[Collections.max(temp)].getTime(),"yyyy-MM-dd HH:mm:ss");
                        etime=dateToString(gpsArray[nexterrorpoint].getTime(),"yyyy-MM-dd HH:mm:ss");
                        timediff=(gpsArray[nexterrorpoint].getTime().getTime()-gpsArray[Collections.max(temp)].getTime().getTime())/1000;
                        String Timediff = timediff/3600+":"+timediff%3600/60+":"+timediff%3600%60;
                        for(int i=Collections.max(temp); i < nexterrorpoint; i++){
                            pointA = new String[]{gpsArray[i].getLog(), gpsArray[i].getLat()};
                            pointB = new String[]{gpsArray[i+1].getLog(), gpsArray[i+1].getLat()};
                            double dist = getDistance(Double.parseDouble(pointA[0]), Double.parseDouble(pointA[1]), Double.parseDouble(pointB[0]), Double.parseDouble(pointB[1]));
                            length=length+dist;
                        }
                        //System.out.println("111111111111");
                        String Start_address=gpsArray[Collections.max(temp)].getAddress();
                        String End_address = gpsArray[nexterrorpoint].getAddress();
                        //System.out.println(Timediff);
                        System.out.println("4444起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime+ "      " + "轨迹时间:" + Timediff + "      " + "轨迹长度:" + length );
                        index ++;
                        trajectory1 = new Trajectory();
                        trajectory1.setAppid(gpsArray[0].getAppid());
                        trajectory1.setStart_gps(start_gps);
                        trajectory1.setStartTime(gpsArray[Collections.max(temp)].getTime());
                        trajectory1.setEnd_gps(end_gps);
                        trajectory1.setEndime(gpsArray[nexterrorpoint].getTime());
                        trajectory1.setRanking(index);
                        trajectory1.setStart_address(Start_address);
                        trajectory1.setEnd_address(End_address);
                        trajectoryList.add(trajectory1);
                        temp.clear();
                    }
                }
            }
        }
        return trajectoryList;
    }

//    public static List<Trajectory> getTrajectory(List<Gps> gpsList) {
//        Gps[] gpsArray = gpsList.toArray(new Gps[gpsList.size()]);
//        List<Trajectory> trajectoryList = new ArrayList<>();
//        int g;
//
//        if (gpsArray.length!=0) {
//            String pointA[];
//            String pointB[];
//            ArrayList[] result = new ArrayList[gpsArray.length];
//            ArrayList[] AroundPointsID = new ArrayList[gpsArray.length];  //定义一个数组，用于保存i小于300范围内的所有点ID
//            ArrayList<Integer> routecount = new ArrayList<>();
//            routecount.add(0);
//            List<Integer> tempPoint = new ArrayList<>();
//
//            //判断任何两点之间的时间差是否大于1个小时，如果大于的话，则保存轨迹点的编号到异常点集合中，直至遍历完所有点。作用是使用异常轨迹点把轨迹分开讨论，避免新旧数据的交叉对停留点的影响
//
//            for (g = 0; g < gpsList.size() - 1; g++) {
//
//                //计算时间差的方法
//                Date d1 = gpsArray[g].getTime();
//                Date d2 = gpsArray[g + 1].getTime();
//                long diff = d2.getTime()-d1.getTime();
//                long t = diff/1000;
//                if (t > 1800) {
//                    routecount.add(g);
//                }
//            }
//            routecount.add(g);
//
//            int count = 0;  //记录轨迹段的个数
//
//            //对异常点集合进行分析，对两个异常点之间的数据进行分析，通过以每个轨迹点为圆心，将距离圆心300米以内的点保存到不同的数据集合中。
//            for (g = 0; g < routecount.size() -1; g++) {
//                int s = routecount.get(g);
//                if(s!=0){
//                    s=s+1;
//                }
//                int z = routecount.get(g + 1);
//                int y = 0;
//                for (int i = s; i < z; i++) {
//                    AroundPointsID[i] = new ArrayList();
//                    pointA = new String[]{gpsArray[i].getLog(), gpsArray[i].getLat()};
//                    for (int j = i + 1; j <= z; j++) {
//                        pointB = new String[]{gpsArray[j].getLog(), gpsArray[j].getLat()};///arrayList1.get(j), arrayList2.get(j)
//                        double dist = Double.parseDouble(getDistance(pointA[1], pointA[0], pointB[1], pointB[0]));
//                        if (dist < 100) {
//                            AroundPointsID[i].add(j);
//                        } else
//                            break;
//                    }
//                    for (int j = i; j >= s; j--) {
//                        pointB = new String[]{gpsArray[j].getLog(), gpsArray[j].getLat()};
//                        double dist = Double.parseDouble(getDistance(pointA[1], pointA[0], pointB[1], pointB[0]));
//                        if (dist < 100) {
//                            AroundPointsID[i].add(j);
//                        } else
//                            break;
//                    }
//                }
//
//                //对都有某个轨迹点的两个集合进行点的个数比较，直到确定出包含该轨迹点点数最多的集合，若该集合点的个数大于5个（即在该点聚集时间大于等于30分钟），则将该点视为停留点
//
//                tempPoint = AroundPointsID[s];
//
//                for (int k = (s + 1); k < z; k++) {
//                    if (isArrayContainObj(tempPoint, k)) {
//                        if (tempPoint.size() < AroundPointsID[k].size()) {
//                            tempPoint = AroundPointsID[k];
//                        }
//                    } else {
//                        if (tempPoint.size() > 3) {
//                            result[y] = (ArrayList) tempPoint;
//                            y++;
//                        }
//                        if (k < z ) {
//                            tempPoint = AroundPointsID[(k + 1)];
//                        }
//                    }
//                }
//
//                //将每个轨迹段的信息进行输出。
//
//                List<Integer> temp = new ArrayList<Integer>();
//                List<Integer> temp2 = new ArrayList<Integer>();
//                Trajectory trajectory1 = new Trajectory();
//                String stime;
//                String etime;
//                if (y == 1) {    //只有一个停留点的情况
//                    temp = result[0];
//                    String start_gps = gpsArray[s].getLog() + "," + gpsArray[s].getLat();
//                    String end_gps = gpsArray[Collections.max(temp)].getLog() + "," + gpsArray[Collections.max(temp)].getLat();  //arrayList1.get(Collections.max(temp)) + "," + arrayList2.get(Collections.max(temp));
//                    if(gpsArray[0].getLat()!= gpsArray[Collections.min(temp)].getLat()){
//                        stime=dateToString(gpsArray[s].getTime(),"HH:mm:ss");
//                        etime=dateToString(gpsArray[Collections.max(temp)].getTime(),"HH:mm:ss");
//                        System.out.println("起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime);
//                        LOGGER.info("起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime);
//                        count++;
//                        trajectory1 = new Trajectory();
//                        trajectory1.setAppid(gpsArray[0].getAppid());
//                        trajectory1.setStart_gps(start_gps);
//                        trajectory1.setStartTime(gpsArray[s].getTime());
//                        trajectory1.setEnd_gps(end_gps);
//                        trajectory1.setEndime(gpsArray[Collections.max(temp)].getTime());
//                        trajectory1.setRanking(count);
//                        trajectoryList.add(trajectory1);
//
//                        start_gps = gpsArray[Collections.max(temp)].getLog() + "," + gpsArray[Collections.max(temp)].getLat();
//                        end_gps = gpsArray[z].getLog() + "," + gpsArray[z].getLat();
//                        stime=dateToString(gpsArray[Collections.max(temp)].getTime(),"HH:mm:ss");
//                        etime=dateToString(gpsArray[z].getTime(),"HH:mm:ss");
//                        System.out.println("起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime);
//                        count ++;
//                        trajectory1 = new Trajectory();
//                        trajectory1.setAppid(gpsArray[0].getAppid());
//                        trajectory1.setStart_gps(start_gps);
//                        trajectory1.setStartTime(gpsArray[Collections.max(temp)].getTime());
//                        trajectory1.setEnd_gps(end_gps);
//                        trajectory1.setEndime(gpsArray[z].getTime());
//                        trajectory1.setRanking(count);
//                        trajectoryList.add(trajectory1);
//                        temp.clear();
//                    }
//                } else if (y > 1) {
//                    temp = result[0];
//                    String start_gps = gpsArray[s].getLog() + "," + gpsArray[s].getLat();
//                    String end_gps = gpsArray[Collections.max(temp)].getLog() + "," + gpsArray[Collections.max(temp)].getLat();
//                    if(gpsArray[0].getLat()!= gpsArray[Collections.min(temp)].getLat()){
//                        stime=dateToString(gpsArray[s].getTime(),"HH:mm:ss");
//                        etime=dateToString(gpsArray[Collections.max(temp)].getTime(),"HH:mm:ss");
//                        System.out.println("起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime);
//                        count ++;
//                        trajectory1 = new Trajectory();
//                        trajectory1.setAppid(gpsArray[0].getAppid());
//                        trajectory1.setStart_gps(start_gps);
//                        trajectory1.setStartTime(gpsArray[s].getTime());
//                        trajectory1.setEnd_gps(end_gps);
//                        trajectory1.setEndime(gpsArray[Collections.max(temp)].getTime());
//                        trajectory1.setRanking(count);
//                        trajectoryList.add(trajectory1);
//                    }
//                    for (int num = 0; num < y - 1; num++) {
//                        temp = result[num];
//                        temp2 = result[num + 1];
//                        start_gps = gpsArray[Collections.max(temp)].getLog() + "," + gpsArray[Collections.max(temp)].getLat();
//                        end_gps = gpsArray[Collections.max(temp2)].getLog() + "," + gpsArray[Collections.max(temp2)].getLat();
//                        stime=dateToString(gpsArray[Collections.max(temp)].getTime(),"HH:mm:ss");
//                        etime=dateToString(gpsArray[Collections.max(temp2)].getTime(),"HH:mm:ss");
//                        System.out.println("起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime);
//                        count ++;
//                        trajectory1 = new Trajectory();
//                        trajectory1.setAppid(gpsArray[0].getAppid());
//                        trajectory1.setStart_gps(start_gps);
//                        trajectory1.setStartTime(gpsArray[Collections.max(temp)].getTime());
//                        trajectory1.setEnd_gps(end_gps);
//                        trajectory1.setEndime(gpsArray[Collections.max(temp2)].getTime());
//                        trajectory1.setRanking(count);
//                        trajectoryList.add(trajectory1);
//                        temp.clear();
//                        //temp2.clear();
//                    }
//                    temp = result[y - 1];
//                    start_gps = gpsArray[Collections.max(temp)].getLog() + "," + gpsArray[Collections.max(temp)].getLat();
//                    end_gps = gpsArray[z].getLog() + "," + gpsArray[z].getLat();
//                    if(gpsArray[z].getLat()!= gpsArray[Collections.min(temp)].getLat()){
//                        stime=dateToString(gpsArray[Collections.max(temp)].getTime(),"HH:mm:ss");
//                        etime=dateToString(gpsArray[z].getTime(),"HH:mm:ss");
//                        System.out.println("起始坐标:" + start_gps + "," + stime + "      " + "终止坐标:" + end_gps + "," + etime);
//                        count ++;
//                        trajectory1 = new Trajectory();
//                        trajectory1.setAppid(gpsArray[0].getAppid());
//                        trajectory1.setStart_gps(start_gps);
//                        trajectory1.setStartTime(gpsArray[Collections.max(temp)].getTime());
//                        trajectory1.setEnd_gps(end_gps);
//                        trajectory1.setEndime(gpsArray[z].getTime());
//                        trajectory1.setRanking(count);
//                        trajectoryList.add(trajectory1);
//                        temp.clear();
//                    }
//                }
//            }
//        }
//        return trajectoryList;
//    }


    //判断某个轨迹点是否存在该集合中

    private static boolean isArrayContainObj(List<Integer> tempPoint, int obj) {
        boolean b = false;
        for (int i = 0; i < tempPoint.size(); i++) {
            if (tempPoint.get(i)==obj) {
                b = true;
                break;
            }
        }
        return b;
    }

    private static double EARTH_RADIUS = 6378.137;
    private static double rad(double d) { //将经纬度转换为弧度
        return d * Math.PI / 180.0;
    }

    //计算点的距离差

//    public static String getDistance(String lat1Str, String lng1Str, String lat2Str, String lng2Str) {
//        Double lat1 = Double.parseDouble(lat1Str);
//        Double lng1 = Double.parseDouble(lng1Str);
//        Double lat2 = Double.parseDouble(lat2Str);
//        Double lng2 = Double.parseDouble(lng2Str);
//        double radLat1 = rad(lat1);
//        double radLat2 = rad(lat2);
//        double difference = radLat1 - radLat2;
//        double mdifference = rad(lng1) - rad(lng2);
//        double distance = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(difference / 2), 2)
//                + Math.cos(radLat1) * Math.cos(radLat2)
//                * Math.pow(Math.sin(mdifference / 2), 2)));
//        distance = distance * EARTH_RADIUS;
//        distance = 1000*Math.round(distance * 10000) / 10000;
//        String distanceStr = distance+"";
//        distanceStr = distanceStr.
//                substring(0, distanceStr.indexOf("."));
//        return distanceStr;
//    }
    static double DEF_PI = 3.14169; // PI59265359;
    static double DEF_2PI= 6.28318530712; // 2*PI
    static double DEF_PI180= 0.01745329252; // PI/180.0
    static double DEF_R =6370693.5; // radius of earth
    public static double getDistance(double lon1, double lat1, double lon2, double lat2)
    {
        double ew1, ns1, ew2, ns2;
        double dx, dy, dew;
        double distance;
        // 角度转换为弧度
        ew1 = lon1 * DEF_PI180;
        ns1 = lat1 * DEF_PI180;
        ew2 = lon2 * DEF_PI180;
        ns2 = lat2 * DEF_PI180;
        // 经度差
        dew = ew1 - ew2;
        // 若跨东经和西经180 度，进行调整
        if (dew > DEF_PI){
            dew = DEF_2PI - dew;}
        else if (dew < -DEF_PI){
            dew = DEF_2PI + dew;}
        dx = DEF_R * Math.cos(ns1) * dew; // 东西方向长度(在纬度圈上的投影长度)
        dy = DEF_R * (ns1 - ns2); // 南北方向长度(在经度圈上的投影长度)
        // 勾股定理求斜边长
        distance = Math.sqrt(dx * dx + dy * dy);
        //System.out.println();
        return distance;
    }

    //将Date类数据转化成String类数据

    public static String dateToString(Date date,String format) {
        SimpleDateFormat sformat = new SimpleDateFormat(format);//日期格式
        String tiem = sformat.format(date);
        return tiem;
    }

    //将数据源读入，形成点的列表

    public List<Gps> getDateSource() {
        List<Gps> gpsList = new ArrayList<>();
        try {
            File file = new File("src\\route.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String str = "";
            while ((str = bufferedReader.readLine()) != null) {
                Gps gps = new Gps();
                gps.setAppid(1);
                gps.setLat(str.substring(32, 41));
                gps.setLog(str.substring(15, 25));
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                //SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                Date time = formatter.parse(str.substring(50, 69));
                gps.setTime(time);
                gpsList.add(gps);
            }
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return gpsList;
    }

}
