package com.zs.constant;

public class ZsAuthDegree {
	/**
	 * parameters key
	 */
	public static final String PERSON_AUTH = "1";//人员管理权限
	public static final int INTPERSON_AUTH = 1;//人员管理权限
	
	public static final String ROLE_AUTH = "2";//角色管理权限
	public static final int INTROLE_AUTH = 2;//角色管理权限
	
	public static final String CITY_AUTH = "3";//市级负责人
	public static final int INTCITY_AUTH = 3;//市级负责人
	
	public static final String FIRST_AUTH = "4";//一级负责人
	public static final int INTFIRST_AUTH = 4;//一级负责人
	
	public static final String SECOND_AUTH = "5";//二级负责人
	public static final int INTSECOND_AUTH = 5;//二级负责人
	
	public static final String THIRD_AUTH = "6";//三级负责人
	public static final int INTTHIRD_AUTH = 6;//三级负责人
	
	public static final String INVESTIGATOR_AUTH = "7";//调查员
	public static final int INTINVESTIGATOR_AUTH = 7;//调查员
	
	public static final String REGION_AUTH = "8";//区域管理权限
	public static final int INTREGION_AUTH = 8;//区域管理权限
	
	public static final String PARAMETER_AUTH = "9";//参数管理权限
	public static final int INTPARAMETER_AUTH = 9;//参数管理权限
	
	public static final String TO_EXAMINE_AUTH = "10";//审核管理权限
	public static final int INTTO_EXAMINE_AUTH = 10;//审核管理权限

}
