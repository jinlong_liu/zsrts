package com.zs.constant;

/**
 * @class_name ZsErrorCode
 * 
 * @author liliang
 * 
 * @date 2016.3.17
 * 
 * @description
 * 		define constant class, obtain parameters error number and so on
 * 
 * @version
 *
 */
public final class ZsErrorCode {
	
	/**
	 * Parameters error code from B/S client, from 1XXXX start
	 */
	
	// parameter of the line number error
	public final static Integer LINE_NO_ERROR = 10000;
	
	
	/**
	 * Server internal error code, from 2XXXX start
	 */
	
}
