package com.zs.constant;


/**
 * @class_name ZsErrorDesc
 * 
 * @author liliang
 * 
 * @description
 * 
 * @date
 *
 * @version 1.0
 */
public class ZsErrorDesc {
	
	public final static String LINE_NO_ERROR_DESC = "line no error";

}
