package com.zs.constant;

public class ZsConstant {
	/**
	 * parameters key
	 */
	public static final String PARA_KEY = "parameters";
	
	public static final int TRAJECTORY_DISTANCE = 300;//轨迹分析容差值

	public static final int NUMBER = 100;//APP每日注册量最大值
	
	public static final int THREADNUMBER = 8;//设置线程池中线程数量
	
	public static final int TRAJECTORY_BACKWARD = 10;//轨迹分析向后对比10个点
	
	public static final int TRAJECTORY_CORRECT = 5;//分析的10个点中，正确个数最小值
	
	public static final int TRAJECTORY_STOPS = 240;//停留点聚合点数最小值
	
	public static final long TRAJECTORY_TIMES = 1800000;//超过半个小时未传数据的
	
	public static final long TRAJECTORY_STOPS_TIMES = 1200000;//在一个点超过20分钟的算作一个停留点
	
	public static final long TRAJECTORY_CLOSE_TIMES = 30*60*1000;//关闭时间超过5分钟


}
