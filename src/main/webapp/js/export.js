    function exportTableData(fileName,tableId,div,treeDate,dateTree) {
        var tableString = '<div id="exportText" style="visibility:hidden"><table cellspacing="0" class="pb" id="tableData" >';  
        var frozenColumns = $('#'+tableId).datagrid("options").frozenColumns;  // 得到frozenColumns对象  
        var columns = $('#'+tableId).datagrid("options").columns;    // 得到columns对象  
        var nameList = new Array();
        // 载入title  
        if (typeof columns != 'undefined' && columns != '') { 
        	var variety="";
        	tableString += '\n<thead><tr>';  
            $(columns).each(function (index) {
                if (typeof frozenColumns != 'undefined' && typeof frozenColumns[index] != 'undefined') {  
                    for (var i = 0; i < frozenColumns[index].length; ++i) {  
                        if (!frozenColumns[index][i].hidden) {  
                            tableString += '\n<th width="' + frozenColumns[index][i].width + '"';  
                            if (typeof frozenColumns[index][i].rowspan != 'undefined' && frozenColumns[index][i].rowspan > 1) {  
                                tableString += ' rowspan="' + frozenColumns[index][i].rowspan + '"';  
                            }  
                            if (typeof frozenColumns[index][i].colspan != 'undefined' && frozenColumns[index][i].colspan > 1) {  
                                tableString += ' colspan="' + frozenColumns[index][i].colspan + '"';  
                                
                            }  
                            if (typeof frozenColumns[index][i].field != 'undefined' && frozenColumns[index][i].field != '') {  
                                nameList.push(frozenColumns[index][i]);  
                            }  
                            tableString += '>' + frozenColumns[0][i].title + '</th>';  
                        }  
                    }  
                }  
                
                for (var i = 0; i < columns[index].length; ++i) {

                    if (!columns[index][i].hidden) {

                    	if (typeof columns[index][i].field != 'undefined' && columns[index][i].field != '' && columns[index][i].title!='变化比例' && columns[index][i].title!='变化幅度') {  
                    		
	                        tableString += '\n<th width="' + columns[index][i].width + '"';  
	                        if (typeof columns[index][i].rowspan != 'undefined' && columns[index][i].rowspan > 1) {
	                        	
	                            tableString += ' rowspan="' + columns[index][i].rowspan + '"';
	                        }  
	                        if (typeof columns[index][i].colspan != 'undefined' && columns[index][i].colspan > 1) {  
	                            tableString += ' colspan="' + columns[index][i].colspan + '"'; 
	                        }  
	                        if (typeof columns[index][i].field != 'undefined' && columns[index][i].field != '') {
	                        	nameList.push(columns[index][i]);  
	                        }
	                        if(index==1 && i==0){
	                        	tableString += '>' + columns[index][i].title + '(' + dateTree + ')'+'</th>';
	                        }else if(index==1 && i==1){
	                        	tableString += '>' + columns[index][i].title + '(' + treeDate + ')'+ '</th>';
	                        }else{
	                        	tableString += '>' + columns[index][i].title + '</th>';
	                        }

                        }else{
                        	variety=columns[index][i];
                        }
                    }
                 }
          });  

            nameList.push(variety);
            tableString += '\n<th width="200" >' + '变化比例' + '</th>';
            tableString += '\n</tr></thead>';
        }       

        // 载入内容  
        var rows = $('#'+tableId).datagrid("getRows"); // 这段代码是获取当前页的所有行  
//        var slice=nameList.slice(0,4);
//  	  var slice2=nameList.slice(4,5);
//  	  var slice3=nameList.slice(5,7);
// 	      var concat=slice.concat(slice3).concat(slice2);
        for (var i = 0; i < rows.length; ++i) {  
            tableString += '\n<tr>';  
            for (var j = 0; j < nameList.length; ++j) {  
          	    
                var e = nameList[j].field.lastIndexOf('_0');  
                tableString += '\n<td';  
                if (nameList[j].align != 'undefined' && nameList[j].align != '') { 
                    tableString += ' style="text-align:' + nameList[j].align + ';"';
                     
                }  
                tableString += '>';  
                if (e + 2 == nameList[j].field.length) {
                    tableString += rows[i][nameList[j].field.substring(0, e)];
                } 
                if(j == 5 && fileName.indexOf("站点延误") != -1){
	                tableString += ((rows[i][nameList[5].field])*100).toFixed(2)+"%";
                }else if(j == 3 && fileName.indexOf("出行费用-区域分布") != -1){
                	tableString +=(rows[i][nameList[3].field]).toFixed(2);
                }else if(j == 4 && fileName.indexOf("出行费用-区域分布") != -1){
                	tableString +=(rows[i][nameList[4].field]).toFixed(2);
                }else if(j == 3 && fileName.indexOf("出行距离-区域分布") != -1){
                	tableString +=(rows[i][nameList[3].field]).toFixed(2);
                }else if(j == 4 && fileName.indexOf("出行距离-区域分布") != -1){
                	tableString +=(rows[i][nameList[4].field]).toFixed(2);
                }else if(j == 3 && fileName.indexOf("出行时长-区域分布") != -1){
                	tableString +=(rows[i][nameList[3].field]).toFixed(2);
                }else if(j == 4 && fileName.indexOf("出行时长-区域分布") != -1){
                	tableString +=(rows[i][nameList[4].field]).toFixed(2);
                }else{
                    tableString += rows[i][nameList[j].field];
                }
                	tableString += '</td>';  
            }  
            tableString += '\n</tr>';  
        }  
        tableString += '\n</table></div>';
    
        //写入jsp文件
        $('div#exportText').remove();
        
        $('#'+div).append(tableString);
        
       /* alert(tableString);
        
        //获取table序号
        var tab=document.getElementById("tableData"); 
        //获取行数 
        var rows=tab.rows; 
        //遍历行 
        for(var i=0;i<rows.length;i++){
        	//遍历表格列 
        	for(var j=0;j<rows[i].cells.length;j++) {
        		//打印某行某列的值 
        		//alert("第"+(i+1)+"行，第"+(j+1)+"列的值是:"+rows[i].cells[j].innerHTML+" ");
        	} 
        }*/
        
        //导出excel
        //fileName+=" "+formatDate();
        $('#tableData').tableExport({type:'excel',escape:'false',filename:fileName});
        }
  function exportTableDataFu(fileName,tableId,div,treeDate,dateTree) {
        var tableString = '<div id="exportText" style="visibility:hidden"><table cellspacing="0" class="pb" id="tableData" >';  
        var frozenColumns = $('#'+tableId).datagrid("options").frozenColumns;  // 得到frozenColumns对象  
        var columns = $('#'+tableId).datagrid("options").columns;    // 得到columns对象  
        var nameList = new Array();
        // 载入title  
        if (typeof columns != 'undefined' && columns != '') { 
        	tableString += '\n<thead><tr>';  
            $(columns).each(function (index) {
                if (typeof frozenColumns != 'undefined' && typeof frozenColumns[index] != 'undefined') {  
                    for (var i = 0; i < frozenColumns[index].length; ++i) {  
                        if (!frozenColumns[index][i].hidden) {  
                            tableString += '\n<th width="' + frozenColumns[index][i].width + '"';  
                            if (typeof frozenColumns[index][i].rowspan != 'undefined' && frozenColumns[index][i].rowspan > 1) {  
                                tableString += ' rowspan="' + frozenColumns[index][i].rowspan + '"';  
                            }  
                            if (typeof frozenColumns[index][i].colspan != 'undefined' && frozenColumns[index][i].colspan > 1) {  
                                tableString += ' colspan="' + frozenColumns[index][i].colspan + '"';  
                                
                            }  
                            if (typeof frozenColumns[index][i].field != 'undefined' && frozenColumns[index][i].field != '') {  
                                nameList.push(frozenColumns[index][i]);  
                            }  
                            tableString += '>' + frozenColumns[0][i].title + '</th>';
                           
                        }  
                    }  
                }  
                
                for (var i = 0; i < columns[index].length; ++i) {

                    if (!columns[index][i].hidden) {
                    	if (typeof columns[index][i].field != 'undefined' && columns[index][i].field != '') {  
                    		
	                        tableString += '\n<th width="' + columns[index][i].width + '"';  
	                        if (typeof columns[index][i].rowspan != 'undefined' && columns[index][i].rowspan > 1) {
	                        	
	                            tableString += ' rowspan="' + columns[index][i].rowspan + '"';
	                        }  
	                        if (typeof columns[index][i].colspan != 'undefined' && columns[index][i].colspan > 1) {  
	                            tableString += ' colspan="' + columns[index][i].colspan + '"'; 
	                        }  
	                        if (typeof columns[index][i].field != 'undefined' && columns[index][i].field != '') {
	                        	nameList.push(columns[index][i]);  
	                        }if(index==1 && i==0){
	                        	tableString += '>' + columns[index][i].title + '(' + dateTree + ')'+'</th>';
	                        }else if(index==1 && i==1){
	                        	tableString += '>' + columns[index][i].title + '(' + dateTree + ')'+ '</th>';
	                        }else if(index==1 && i==2){
	                        	tableString += '>' + columns[index][i].title + '(' + treeDate + ')'+ '</th>';
	                        }else if(index==1 && i==3){
	                        	tableString += '>' + columns[index][i].title + '(' + treeDate + ')'+ '</th>';
	                        }else if(index==1 && i==4){
	                        	tableString += '>' + columns[index][i].title + '(' + "变化比例" + ')'+ '</th>';
	                        }else if(index==1 && i==5){
	                        	tableString += '>' + columns[index][i].title + '(' + "变化比例" + ')'+ '</th>';
	                        }else{
	                        	tableString += '>' + columns[index][i].title + '</th>';
	                        }
                        }
                    }
                }  
            });  
            tableString += '\n</tr></thead>';
           // alert(tableString);
        }       

        // 载入内容  
        var rows = $('#'+tableId).datagrid("getRows"); // 这段代码是获取当前页的所有行
        for (var i = 0; i < rows.length; ++i) {
            tableString += '\n<tr>';  
            for (var j = 0; j < nameList.length; ++j) {
                var e = nameList[j].field.lastIndexOf('_0');  
                tableString += '\n<td';  
                if (nameList[j].align != 'undefined' && nameList[j].align != '') { 
                    tableString += ' style="text-align:' + nameList[j].align + ';"';
                }  
                tableString += '>';  
                if (e + 2 == nameList[j].field.length) {
                    tableString += rows[i][nameList[j].field.substring(0, e)];  
                }
                 if (fileName.indexOf("出行费用-样本分布") != -1 ||fileName.indexOf("出行距离-样本分布") != -1 || fileName.indexOf("出行时长-样本分布") != -1) {
                	 if(j == 3){
   	                	tableString += ((rows[i][nameList[3].field])*100).toFixed(2)+"%";
   	                }else{
   	                    tableString += rows[i][nameList[j].field];
   	                }
				}else if (fileName.indexOf("出行费用-区域分布") != -1 || fileName.indexOf("出行距离-区域分布") != -1 || fileName.indexOf("出行时长-区域分布") != -1) {
	               	   if(j == 3){
	               		   //alert((rows[i][nameList[3].field]).toString().substring(0,5));
		                	tableString +=(rows[i][nameList[3].field]).toFixed(2);
		                }else{
		                    tableString += rows[i][nameList[j].field];
		                }
				}else if (fileName.indexOf("出行费用-线路站点分布") != -1 || fileName.indexOf("出行距离-线路站点分布") != -1 || fileName.indexOf("出行时长-线路站点分布") != -1 || fileName.indexOf("出行时长-等待时间") != -1 || fileName.indexOf("出行时长-换乘时间") != -1) {
	               	   if(j == 6){
		                	tableString +=(rows[i][nameList[6].field]).toFixed(2);
		                }else if(j == 8){
		                	tableString +=(rows[i][nameList[8].field]).toFixed(2);
		                }else{
		                    tableString += rows[i][nameList[j].field];
		               }
				}else{
	                tableString += rows[i][nameList[j].field];
				}
                    tableString += '</td>';
             }  
            tableString += '\n</tr>';  
         }  
        
        tableString += '\n</table></div>';
        //写入jsp文件
        //$('#exportText').empty();
        //renxw 2016-11-04
        $('div#exportText').remove();
        $('#'+div).append(tableString);
        //导出excel
       // fileName+=" "+formatDate();
        $('#tableData').tableExport({type:'excel',escape:'false',filename:fileName});
    }
  
  
    /**
     * 按钮调用时使用
     * @param flag
     * @return
     */
    function writeHiddenVal(flag){
    	if(flag){
    		$('#isYearQuery').val("true");
    	}else{
    		$('#isYearQuery').val("false");
    	}
    }
    
    /**
     * 按钮调用时使用
     * @param flag
     * @return
     */
    function writeTimeHiddenVal(flag){
    	if(flag=="1"){
    		alert(2);
    		$('#isYearQuery').val("1");
    		alert(3);
    	}else if(flag=="2"){
    		$('#isYearQuery').val("2");
    	}else if(flag=="3"){
    		$('#isYearQuery').val("3");
    	}else if(flag=="4"){
    		$('#isYearQuery').val("4");
    	}else if(flag=="5"){
    		$('#isYearQuery').val("5");
    	}else if(flag=="6"){
    		$('#isYearQuery').val("6");
    	}else if(flag=="7"){
    		$('#isYearQuery').val("7");
    	}else if(flag=="8"){
    		$('#isYearQuery').val("8");
    	}else if(flag=="9"){
    		$('#isYearQuery').val("9");
    	}else if(flag=="10"){
    		$('#isYearQuery').val("10");
    	}
    	var flag2=$('#isYearQuery').val();
    	
    }
    /**
     * 初始化时使用
     * @return
     */
    function writeHtml(){
    	var str = '<input type="hidden" id="isYearQuery" />';
    	$('#queryTable').append(str);
    }
    /**
     * 格式化日期方法
     * @return
     */
    function formatDate(){
        var date = new Date();
        return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
    }
      