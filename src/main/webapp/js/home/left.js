/* style和script必须写在body里，否则easyui父页面不能加载 */
$(document).ready(function() {

	/**
	 * 滑动/展开
	 */
	$("ul.expmenu li > div.header").click(function() {
		var arrow = $(this).find("span.arrow");
		if (arrow.hasClass("up")) {
			arrow.removeClass("up");
			arrow.addClass("down");
		} else if (arrow.hasClass("down")) {
			arrow.removeClass("down");
			arrow.addClass("up");
		}

		$(this).parent().find("ul.menu_e").slideToggle();
	});
});

/**
 * 收缩、展开菜单
 */
function showMenu(obj, n) {
	var Nav = obj.parentNode;
	var BName, HName, t;

	if (!Nav.id) {
		BName = Nav.getElementsByTagName("ol");
		HName = Nav.getElementsByTagName("h2");
		t = 2;
	} else {
		BName = document.getElementById(Nav.id).getElementsByTagName("span");
		HName = document.getElementById(Nav.id).getElementsByTagName(".header");
		t = 1;
	}
	for ( var i = 0; i < HName.length; i++) {
		HName[i].innerHTML = HName[i].innerHTML.replace("-", "+");
		HName[i].className = "";
	}
	obj.className = "h" + t;
	for ( var i = 0; i < BName.length; i++) {
		if (i != n) {
			BName[i].className = "no";
			obj.innerHTML = obj.innerHTML.replace("-", "+");
		}
	}
	if (BName[n].className == "no") {
		BName[n].className = "";
		obj.innerHTML = obj.innerHTML.replace("+", "-");
	} else {
		BName[n].className = "no";
		obj.innerHTML = obj.innerHTML.replace("-", "+");
	}
}

// 三级菜单切换
function setcolor(obj, index) {
	var Obj = $(obj).parent().children("a");
	for ( var i = 0; i < Obj.length; i++) {
		if (i == index) {
			$(obj).attr("style", "background-color:#27A4E6;");// #3096EA
		} else {
			$(Obj[i]).attr("style", "");
		}
	}
}
function mouseover(status) {
		 if(status==1){
			 document.getElementById("span1").style.backgroundImage = "url('./images/questionnaireclick.png')";  
		 }
		 if(status==2){
			 document.getElementById("span2").style.backgroundImage = "url('./images/Statisticalclick.png')";  
		 }
		 if(status==3){
			 document.getElementById("span3").style.backgroundImage = "url('./images/userlilck.png')";  
		 }
		 if(status==4){
			 document.getElementById("span4").style.backgroundImage = "url('./images/managerclick.png')";  
		 }
		 if(status==5){
			 document.getElementById("span5").style.backgroundImage = "url('./images/areaclick.png')";  
		 }
		 if(status==6){
			 document.getElementById("span6").style.backgroundImage = "url('./images/parametersclick.png')";  
		 }
		 if(status==7){
			 document.getElementById("span7").style.backgroundImage = "url('./images/updatePosswordclick.png')";  
		 }
		 if(status==8){
			 document.getElementById("span8").style.backgroundImage = "url('./images/codeclick.png')";  
		 }
		 if(status==9){
			 document.getElementById("span9").style.backgroundImage = "url('./images/usermanagerclick.png')";  
		 }
		 if(status==10){
			 document.getElementById("span10").style.backgroundImage = "url('./images/statisticalqueryclick.png')";  
		 }
}
function mouseout(status){
	 if(status==1){
		 document.getElementById("span1").style.backgroundImage = "url('./images/questionnaireDefault .png')";  
	 }
	 if(status==2){
		 document.getElementById("span2").style.backgroundImage = "url('./images/QuestionnaireStatistical.png')";  
	 }
	 if(status==3){
		 document.getElementById("span3").style.backgroundImage = "url('./images/Usermanagement.png')";  
	 }
	 if(status==4){
		 document.getElementById("span4").style.backgroundImage = "url('./images/Rolemanagement.png')";  
	 }
	 if(status==5){
		 document.getElementById("span5").style.backgroundImage = "url('./images/Regionalmanagement.png')";  
	 }
	 if(status==6){
		 document.getElementById("span6").style.backgroundImage = "url('./images/ParameterSettings.png')";  
	 }
	 if(status==7){
		 document.getElementById("span7").style.backgroundImage = "url('./images/Changepassword.png')";  
	 }
	 if(status==8){
		 document.getElementById("span8").style.backgroundImage = "url('./images/code.png')";  
	 }
	 if(status==9){
		 document.getElementById("span9").style.backgroundImage = "url('./images/uesrQuery.png')";  
	 }
	 if(status==10){
		 document.getElementById("span10").style.backgroundImage = "url('./images/Statisticalquery.png')";  
	 }
	 
}