/**
 * 
 *@file_name:  navigation.js
 *@description:
 *@author:  guoyuchuan
 *@date:  2016.08.13
 *@version:  1.0, by 2.0
 *@modify: 2017.04.13, by lirenbo
 **
 */

$(document).ready(function(){
//	$('#user_but').click(function(){
//		console.log("查看用户信息");
//	});
	$('#exit_but').click(function(){
		//alert('即将出退出该系统');
		logout();
	});
	
	
	
 
});

$(function() {
	// 数据
	var treeData = [ {
		text : "客流分析",
		iconCls : "icon-menu",
		children : [ {
			text : "线路",
			iconCls : "icon-menu",
			children : [ {
				text : "上车量",
				iconCls : "icon-menu",
				attributes : {
					url : "line_boarding_sta.jsp"
				}
			}, {
				text : "下车量",
				iconCls : "icon-menu",
				attributes : {
					// url:"aborading_old.jsp"
					url : "line_alighting_sta.jsp"
				}
			}, {
				text : "换乘量",
				iconCls : "icon-menu",
				attributes : {
					url : "line_transfer_sta.jsp"
				}
			}, {
				text : "速度",
				iconCls : "icon-menu",
				attributes : {
					url : "line_speed_sta.jsp"
				}
			} ]
		}, {
			text : "线网",
			iconCls : "icon-manage",
			children : [ {
				text : "客流点热力图",
				iconCls : "icon-menu",
				attributes : {
					url : "heat_map.jsp"
				}
			}, {
				text : "OD期望线",
				iconCls : "icon-menu",
				attributes : {
					url : ""
				}
			} ]
		} ]
	}, {
		text : "网络评估",
		iconCls : "icon-manage",
		children : [ {
			text : "网络评估",
			iconCls : "icon-menu",
			attributes : {
				url : ""
			}
		} ]
	}, {
		text : "营运评估",
		iconCls : "icon-manage",
		children : [ {
			text : "营运评估",
			iconCls : "icon-menu",
			attributes : {
				url : ""
			}
		} ]
	}, {
		text : "异动监测",
		iconCls : "icon-manage",
		children : [ {
			text : "异动监测",
			iconCls : "icon-menu",
			attributes : {
				url : ""
			}
		} ]

	} ];

	// 实例化树菜单
//	$("#tree").tree({
//		data : treeData,
//		lines : true,
//		onClick : function(node) {
//			if (node.attributes) {
//				openTab(node.text, node.attributes.url, node.iconCls);
//			}
//		}
//	});

	// 新增Tab
//	function openTab(text, url, iconCls) {
//		if ($("#tabs").tabs('exists', text)) {
//			$("#tabs").tabs('select', text);
//		} else {
//			var content = "<iframe frameborder='0' scrolling='auto' style='width:100%;height:100%' src="
//					+ url + "></iframe>";
//			$("#tabs").tabs('add', {
//				title : text,
//				closable : true,
//				content : content,
//				iconCls : iconCls,
//			});
//		}
//	}
});

function logout() {
	if (confirm("您确定要注销吗？")) {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : root + "/services/OutSystem",
			async : false,
			success : function(result) {
				var flag = result.flag;
				if (flag == "1") {
					window.location.href = root + "/login";
				}

			}
		});
	}
}

var root = root();
function root() {
	var pathName = window.location.pathname;
	var projectName = pathName
			.substring(0, pathName.substr(1).indexOf('/') + 1);
	return projectName;
}


