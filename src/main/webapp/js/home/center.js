/**
 * @author: guoyuchuan
 */
var centerTabs;
var tabsMenu;

$(function() {
	

	tabsMenu = $('#tabsMenu').menu(
			{
				onClick : function(item) {
					var curTabTitle = $(this).data('tabTitle');
					var type = $(item.target).attr('type');

					if (type === 'refresh') {
						refreshTab(curTabTitle);
						return;
					}

					if (type === 'close') {
						var t = centerTabs.tabs('getTab', curTabTitle);
						if (t.panel('options').closable) {
							centerTabs.tabs('close', curTabTitle);
						}
						return;
					}

					var allTabs = centerTabs.tabs('tabs');
					var closeTabsTitle = [];

					$.each(allTabs, function() {
						var opt = $(this).panel('options');
						if (opt.closable && opt.title != curTabTitle
								&& type === 'closeOther') {
							closeTabsTitle.push(opt.title);
						} else if (opt.closable && type === 'closeAll') {
							closeTabsTitle.push(opt.title);
						}
					});

					for ( var i = 0; i < closeTabsTitle.length; i++) {
						centerTabs.tabs('close', closeTabsTitle[i]);
					}
				}
			});

	centerTabs = $('#centerTabs').tabs({
		fit : true,
		border : false,
		onContextMenu : function(e, title) {
			e.preventDefault();
			tabsMenu.menu('show', {
				left : e.pageX,
				top : e.pageY
			}).data('tabTitle', title);
		}
	});
	
});


/* 打开Table页面 */
function addTab(cname, curl) {
	if (centerTabs.tabs('exists', cname)) {
		centerTabs.tabs('select', cname);
	} else {
		if (curl && curl.length > 0) {
			/*
			 * if (curl.indexOf('!druid.do') < 0) {/*数据源监控页面不需要开启等待提示
			 * $.messager.progress({ text : '页面加载中....', interval : 100 });
			 * window.setTimeout(function() { try {
			 * $.messager.progress('close'); } catch (e) { } }, 5000); }
			 */
			centerTabs
					.tabs(
							'add',
							{
								title : cname,
								closable : true,
								content : '<iframe src="'
										+ curl
										+ '" frameborder="0" style="border:0;width:100%;height:99.4%;"></iframe>',
								tools : [ {
									/* iconCls : 'icon-mini-refresh', */
									handler : function() {
										refreshTab(cname);
									}
								} ]
							});
		} else {
			centerTabs
					.tabs(
							'add',
							{
								title : cname,
								closable : true,
								/* iconCls : ciconCls, */
								content : '<iframe src="error/err.jsp" frameborder="0" style="border:0;width:100%;height:99.4%;"></iframe>',
								tools : [ {
									/* iconCls : 'icon-mini-refresh', */
									handler : function() {
										refreshTab(cname);
									}
								} ]
							});
		}
	}
}

function refreshTab(title) {
	var tab = centerTabs.tabs('getTab', title);
	centerTabs.tabs('update', {
		tab : tab,
		options : tab.panel('options')
	});
}

// top 5
function rowStyle(index, row) {
	return 'height:46px;';
}


// 判断浏览区是否支持canvas
function isSupportCanvas() {
	var elem = document.createElement('canvas');
	return !!(elem.getContext && elem.getContext('2d'));
}

function mouseOver(status){
	if(status == 1){
		document.getElementById("img1").src="./images/parameterSetingMover.png";   
	}
	
	if(status == 2){
		document.getElementById("img2").src="./images/pegionalManagerMover.png";   
	}
	if(status == 3){
		document.getElementById("img3").src="./images/questionnaireInquiryMover.png";   
	}
	if(status == 4){
		document.getElementById("img4").src="./images/questionnaireStatisticsMover.png";   
	}
}
function mouseOut(status){
	if(status == 1){
		document.getElementById("img1").src="./images/parameterSeting.png";   
	}
	if(status == 2){
		document.getElementById("img2").src="./images/pegionalManager.png";   
	}
	if(status == 3){
		document.getElementById("img3").src="./images/questionnaireInquiry.png";   
	}
	if(status == 4){
		document.getElementById("img4").src="./images/questionnaireStatistics.png";   
	}
   
}


