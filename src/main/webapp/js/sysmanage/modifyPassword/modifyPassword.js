function modifyPwd() {
	
	
	
	var oldpassword = $("#oldpassword").val().trim();
	var oldpasswordNull = $("#oldpassword").val();
	var password = $("#password").val().trim();
	var passwordNull = $("#password").val();
	var conpassword = $("#con_password").val().trim();
	var conpasswordNull = $("#con_password").val();
	
	var reg = /^(?!_)^[0-9a-zA-Z_]{6,}$/;
	  if (!reg.test(oldpassword)) {
		  alert("密码格式不正确");
		  return;
	  }
	  if (!reg.test(password)) {
		  alert("新密码格式不正确");
		  return;
	  }
	  if (!reg.test(conpassword)) {
		  alert("确认密码格式不正确");
		  return;
	  }
	
	//alert("密码验证");
	if(oldpassword != oldpasswordNull){
		alert("原密码不能有空格！");
		return;
	}
	if(password != passwordNull){
		alert("新密码不能有空格！");
		return;
	}
	if(conpassword != conpasswordNull){
		alert("确认密码和新密码不一致！");
		return;
	}
	
	if (oldpassword == "") {
		alert("请输入原密码！");
		return;
	}
	if (password == "") {
		alert("请输入新密码！");
		return;
	}
	if (conpassword == "") {
		alert("请输入确认密码！");
		return;
	}
	if (oldpassword.length<6) {
		alert("密码长度不能低于6位！");
		return;
	}
	if(password.length<6){
    	alert("密码长度不能低于6位！");
    	return;
    }
	if(conpassword.length<6){
    	alert("密码长度不能低于6位！");
    	return;
    }
	if(oldpassword == password){
		alert("新密码与原密码一致！");
    	return;
	}
	if(password != conpassword){
		alert("密码与确认密码不一致");
    	return;
	}
	//var data = {"password":hex_md5(password)};  
	//data=null;hex_md5(password)

	var dialog=art.dialog({
	    title: '提示',//弹出框 的标题 
	    content: '是否确定修改？',//弹出框中显示的内容 
	    ok: function () {
	    	$.post(root+"/services/modifyPassword", {"password":hex_md5(password),
	    		"oldpassword":hex_md5(oldpassword)},
				function(data){
	    	    var obj = eval('(' + data + ')');//将data转成JSON对象
	    		
	    		if(obj.errNo==0){
	    			
	    			sweetAlert("", "密码修改成功！", "success");
	    			
	    			/*swal(
	    					{   title : " ",
								text : "密码修改成功!",
								type : "success",
								showCancelButton : true,
								confirmButtonColor : "#DD6B55",
								//confirmButtonText : "Yes",
								//cancelButtonText : "No, cancel plx!",
								closeOnConfirm : true,
								closeOnCancel : true
	    					},
	    					function(isConfirm) {
									if (isConfirm) {
										location.reload();
									} else {
										alert('22222');
									}
								}); */

	    			
	    			
	    			//art.dialog.tips('密码修改成功！', 1.5);
					//$('#myModal').modal('hide');
					//search();
					//location.reload();
	    		}else if(obj.errNo==-2){
	    			art.dialog.tips('原密码不正确！', 1.5);
	    		}else{
					art.dialog.tips('系统异常！', 1.5);
				}
	    		
	    		
	    				/*//var  resu=null;
	    				var obj = eval('(' + data + ')');//将data转成JSON对象
	    				
	    			
						if(obj.errNo==0){
							art.dialog.tips('添加成功！', 1.5);
							$('#myModal').modal('hide');
							//search();
							location.reload();
						}else if(obj.errNo==-2){
							art.dialog.tips('角色名已存在！', 1.5);
						}else{
							art.dialog.tips('系统异常', 1.5);
						}*/
					   });
	    },
	    okVal:'确定',
	    cancel: function () { 
	    },
	   
	    fixed:true,
	    resize:true
	});
}

/*document.onkeydown = function(e) {
	var theEvent = window.event || e;
	var code = theEvent.keyCode || theEvent.which;
	if (code == 13) {
		$("#login").click();
	}
};*/

