var updateIdPubllic=null;
var updateRolename=null;
var peopolId=null; // 人员管理id
var actorId=null;	// 角色管理id
var authId=null;	//  普通权限id

var peopolOld=null; // 人员管理
var actorOld=null;	// 角色管理
var authOld=null;	//  普通权限
var status = 1;

function search(){
	
	 document.getElementById("view").style.display = "none";//隐藏图表显示框
	//初始化
	$('#table').bootstrapTable('destroy');
	
	 // $(".loaderTip,.bgBox").css("display", "block");
	
	// radioVal = encodeURI(encodeURI(radioVal));    
	$("#table").bootstrapTable({
		url : root+'/services/getQuestionnaireList',  
		method:'post',
		contentType:'application/x-www-form-urlencoded;charset=UTF-8',
		striped:true,
		pagination:true,
		pageSize:10,
		pageNumber:1,
		height:411,
		pageList:[10,20,30],
		silent: true,
		sortStable:true,
		sortName:'time',
		sortOrder:'desc',
		sidePagination: "client",
		responseHandler:function(res){
			return{
				number:res.number,
				stoptype:res.stoptype
			};
		},
		/*queryParams:{
			 districtNumber:$('#districtName').val(),
			 subofficeNumber:$('#subofficeName').val()
		},
		queryParams:function(params){
			return {
				 districtNumber:$('#districtName').val(),
				 subofficeNumber:$('#subofficeName').val()

           };
		},*/
		formatLoadingMessage:"请稍等,正在加载中。。。。。。。",
		columns:[
		   
		    {
				field:'i',
				title:'序号',
				width:100,
				valign:'middle',
				align:'center'
			},
			{
				field:"districtName",
				title:'行政区',
				width:150,
				valign:'middle',
				align:'center'
			},
			{
				field:'subofficeName',
				title:'街道',
				width:250,
				valign:'middle',
				align:'center'
			},
			{
				field:'communityName',
				title:'社区',
				width:200,
				valign:'middle',
				align:'center'
			},
			{
				field:'totalCount',
				title:'指标数',
				width:100,
				valign:'middle',
				align:'center'
			},
			{
				field:'followCount',
				title:'关注数',
				width:100,
				valign:'middle',
				align:'center'
			},
			
			{
				field:'submitCount',
				title:'问卷提交数',
				width:150,
				valign:'middle',
				align:'center',
				
			},
			
			{
				field:'notCompleteCount',
				title:'问卷未完成数',
				width:200,
				valign:'middle',
				align:'center',
				
			},
			
			{
				field:'percentageOfCompletion',
				title:'问卷完成率',
				width:150,
				valign:'middle',
				align:'center',
				
			},
			]
			
	});
	
	
}
$().ready(function(){
	//页面加载
	loadDistrictNamedistrictName();
	search();
	 
});

/*加载提示框*/
$(function() {
    $(document).ajaxStart(function () { $(".loaderTip,.bgBox").css("display", "block"); });  
    $(document).ajaxStop(function () { $(".loaderTip,.bgBox").css("display", "none"); });

    $(".pager a").click(function() {
        var Is = $(this).attr("disabled");
        if (Is != true) {
            $(".loaderTip,.bgBox").css("display", "block");
        }
    });
});


//加载页面按钮
function loadDistrictNamedistrictName(){
	//查询对应的角色的权限
	$.ajax({
		type : "POST",
		dataType : "json",
		async : false,
		url : root+"/services/queryAreaStatistics",
		success : function(result) {
			
			var obj = eval(result);
			
			//var select = "";
			if(obj[0].type == 1){ 
				/*$("#are").empty();
				select =  "<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>	"+				  
				"<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea(2)' > "+
				"</select> "+
				"<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>	 "+
				"<select id='subofficeName' name='type'  class='js-example-disabled-results' style='width:200px' > "+
				"</select> "+
				" <button type='button' style='border:none;' onclick='searchByareaNumber()'> "+
				"    查    询"+
				"</button> ";
				
				document.getElementById("are").innerHTML = document
				.getElementById("are").innerHTML+select;*/
				
				//document.getElementById("view5").style.cssText ="margin-left:50px";
				
				for ( var i = 0; i < obj.length; i++) {
					$("#districtName").append(
							"<option value='" + obj[i].districtNumber + "'>"
							+ obj[i].districtName + "</option>");
					
					
				}
			 }
			
			
			if(obj[0].type == 2){ 
				
				$("#districtName").empty();
				
				/*select =  "<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>	"+				  
				"<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea(2)' > "+
				"</select> "+
				"<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>	 "+
				"<select id='subofficeName' name='type'  class='js-example-disabled-results' style='width:200px' > "+
				"</select> "+
				" <button type='button' style='border:none;' onclick='searchByareaNumber()'> "+
				"    查    询"+
				"</button> ";
				
				document.getElementById("are").innerHTML = document
				.getElementById("are").innerHTML+select;*/
				
				//document.getElementById("view5").style.cssText ="margin-left:50px";
				
				for ( var j = 0; j < obj.length; j++) {
					
					if(j==0){
						$("#districtName").append(
								"<option value='" + obj[j].districtNumber + "'>"
								+ obj[j].districtName + "</option>");
						$("#subofficeName").append(
								"<option value='" + obj[j].subofficeNumber + "'>"
								+ obj[j].subofficeName + "</option>");
					}else if(obj[j].districtNumber== obj[0].districtNumber){
						$("#subofficeName").append(
								"<option value='" + obj[j].subofficeNumber + "'>"
								+ obj[j].subofficeName + "</option>");
					}
					
					
				}
			 }
			if(obj[0].type == 3){
				document.getElementById("view3").style.display = "none";
				document.getElementById("view4").style.display = "none";
				//document.getElementById("view5").style.cssText ="margin-left:650px";
				
				//document.getElementById("are").style.display = "block";
			}
			
		},
		error : function(result) {
			alert("系统错误");
		}
	});
}

/**
 * 点击行政区加载街道
 */
function diArea() {
		var districtNumber = $('#districtName').val();
		var districtName = $('#districtName').find("option:selected").text();
		
		/*if (districtNumber == null) {
			$("#subOfficeSel").append("<option value=''>请选择</option>");
		}*/
		if(districtNumber == ''){//行政区为空，清空社区下拉并添加一个空
			$("#subofficeName").empty();
			$("#subofficeName").append(
					"<option value='" + " " + "'>"
					+ "请选择" + "</option>");
		}
		if (districtNumber != '') {//行政区不为空，清空社区下拉并添加
			$.ajax({
				url : root + "/services/queryAreaByUserId",
				type : "post",
				async : false,
				data : {
					type : 2,
					/*districtName : districtName*/
				},
				success : function(result) {
					
					$("#subofficeName").empty();
					
					var obj = eval(result);
					
					$("#subofficeName").append(
							"<option value='" + " " + "'>"
							+ "请选择" + "</option>");
					
					for ( var i = 0; i < obj.length; i++) {
						if(obj[i].districtName == districtName){
							
							$("#subofficeName").append(
									"<option value='" + obj[i].subofficeNumber + "'>"
									+ obj[i].subofficeName + "</option>");
						}
						
					}
					/*var obj = eval("(" + result + ")");
					if (obj.length == 0) {
						$("#subOfficeSel").append(
								"<option value='" + ' ' + "'>" + " "
										+ "</option>");
					} else {
						$("#subOfficeSel").append("<option value=''>请选择</option>");
						for ( var i = 0; i < obj.length; i++) {
							$("#subOfficeSel").append(
									"<option value='" + obj[i].areaId + "'>"
											+ obj[i].text + "</option>");
						}
					}*/
				},
				error : function(result) {
					alert("系统错误");
				}
			});

		}
	
}

function searchByareaNumber(){
	
	 var districtNumber = $('#districtName').val();
	 var subofficeNumber = $('#subofficeName').val();
	
	 $('#table').bootstrapTable('destroy');
	 $('#view').empty();
	
	 document.getElementById("view").style.display = "none";
	 //document.getElementById("are").style.display = "block";
	 
	 //$(".loaderTip,.bgBox").css("display", "block");
	 
	 $("#table").bootstrapTable({
			url : root+'/services/queryByareaNumber?districtNumber='+districtNumber+'&subofficeNumber='+subofficeNumber,  
			method:'post',
			contentType:'application/x-www-form-urlencoded;charset=UTF-8',
			striped:true,
			pagination:true,
			pageSize:10,
			pageNumber:1,
			height:411,
			pageList:[10,20,30],
			silent: true,
			sortStable:true,
			sortName:'time',
			sortOrder:'desc',
			sidePagination: "client",
			responseHandler:function(res){
				return{
					number:res.number,
					stoptype:res.stoptype
				};
			},
			/*queryParams:{
				 districtNumber:$('#districtName').val(),
				 subofficeNumber:$('#subofficeName').val()
			},
			queryParams:function(params){
				return {
					 districtNumber:$('#districtName').val(),
					 subofficeNumber:$('#subofficeName').val()

	           };
			},*/
			formatLoadingMessage:"请稍等,正在加载中。。。。。。。",
			columns:[
			   
			    {
					field:'i',
					title:'序号',
					width:100,
					valign:'middle',
					align:'center'
				},
				{
					field:"districtName",
					title:'行政区',
					width:150,
					valign:'middle',
					align:'center'
				},
				{
					field:'subofficeName',
					title:'街道',
					width:250,
					valign:'middle',
					align:'center'
				},
				{
					field:'communityName',
					title:'社区',
					width:200,
					valign:'middle',
					align:'center'
				},
				{
					field:'totalCount',
					title:'指标数',
					width:100,
					valign:'middle',
					align:'center'
				},
				{
					field:'followCount',
					title:'关注数',
					width:100,
					valign:'middle',
					align:'center'
				},
				
				{
					field:'submitCount',
					title:'问卷提交数',
					width:150,
					valign:'middle',
					align:'center',
					
				},
				
				{
					field:'notCompleteCount',
					title:'问卷未完成数',
					width:200,
					valign:'middle',
					align:'center',
					
				},
				
				{
					field:'percentageOfCompletion',
					title:'问卷完成率',
					width:150,
					valign:'middle',
					align:'center',
					
				},
				]
				
		});
	 
}

function viewHistogram(){
	
	 $('#table').bootstrapTable('destroy');
	 $('#view').empty();
	
	 document.getElementById("view").style.display = "block";
	 
	 // $(".loaderTip,.bgBox").css("display", "block");//加载中
	 
	 //document.getElementById("are").style.display = "none";
	 //document.getElementById("are").style.display = "block";
	 var districtNumber = $('#districtName').val().trim();
	 var subofficeNumber = $('#subofficeName').val().trim();	 
	 
	 var chartVehicle = echarts.init(document.getElementById("view"));//初始化
	 
	 var dataArea = [];//区域
	 var dataTotalCount = [];//指标数
	 var dataFollowCount = [];//关注数
	 var dataSubmitCount = [];//提交数
	 var dataNotCompleteCount = [];//提交数
	 
	 var totalCount = 0;
     var followCount = 0;
	 var submitCount = 0;
	 var notCompleteCount = 0;  
	 
	 $.ajax({
			type : "post",
			data : {
				districtNumber:districtNumber,
				subofficeNumber:subofficeNumber
			},
			async : false,
			url : root+'/services/queryByareaNumber',
			success : function(res) {
				
				var obj = eval( "(" + res + ")" );
				var authDegree = obj[0].authDegree;
				for(var i=0;i<obj.length;i++){
					
					if(authDegree=="3" || authDegree=="4"){//行政区柱状图数据
						
						if(districtNumber==''){
							
							if(i==obj.length-1){
								dataArea.push(obj[i].districtName);
								totalCount = totalCount+obj[i].totalCount;
								followCount = followCount+obj[i].followCount;
								submitCount = submitCount+obj[i].submitCount;
								notCompleteCount = notCompleteCount+obj[i].notCompleteCount;
								
								dataTotalCount.push(totalCount);//指标数
								dataFollowCount.push(followCount);//关注数
								dataSubmitCount.push(submitCount);//提交数
								
								dataNotCompleteCount.push(notCompleteCount);//未完成指提交数   
								
								totalCount = 0;
								followCount = 0;
								submitCount = 0;
								notCompleteCount = 0;
							}else{
								if(obj[i].districtName == obj[i+1].districtName){
									totalCount = totalCount+obj[i].totalCount;
									followCount = followCount+obj[i].followCount;
									submitCount = submitCount+obj[i].submitCount;
									notCompleteCount = notCompleteCount+obj[i].notCompleteCount;
								}else{
									
									totalCount = totalCount+obj[i].totalCount;
									followCount = followCount+obj[i].followCount;
									submitCount = submitCount+obj[i].submitCount;
									notCompleteCount = notCompleteCount+obj[i].notCompleteCount;
									
									dataArea.push(obj[i].districtName);
									dataTotalCount.push(totalCount);//指标数
									dataFollowCount.push(followCount);//关注数
									dataSubmitCount.push(submitCount);//提交数

									dataNotCompleteCount.push(notCompleteCount);//未完成指提交数   
									
									totalCount = 0;
									followCount = 0;
									submitCount = 0;
									notCompleteCount = 0;
								}
							}
							
						}
						
						if(districtNumber!=''&& subofficeNumber==''){//生成街道报表
							
							if(i==obj.length-1){
								dataArea.push(obj[i].subofficeName);
								
								totalCount = totalCount+obj[i].totalCount;
								followCount = followCount+obj[i].followCount;
								submitCount = submitCount+obj[i].submitCount;
								notCompleteCount = notCompleteCount+obj[i].notCompleteCount;
								
								dataTotalCount.push(totalCount);//指标数
								dataFollowCount.push(followCount);//关注数
								dataSubmitCount.push(submitCount);//提交数
								dataNotCompleteCount.push(notCompleteCount);//未完成指提交数   
								
								totalCount = 0;
								followCount = 0;
								submitCount = 0;
								notCompleteCount = 0;
							}else{
								if(obj[i].subofficeName == obj[i+1].subofficeName){
									totalCount = totalCount+obj[i].totalCount;
									followCount = followCount+obj[i].followCount;
									submitCount = submitCount+obj[i].submitCount;
									notCompleteCount = notCompleteCount+obj[i].notCompleteCount;
								}else{
									totalCount = totalCount+obj[i].totalCount;
									followCount = followCount+obj[i].followCount;
									submitCount = submitCount+obj[i].submitCount;
									notCompleteCount = notCompleteCount+obj[i].notCompleteCount;
									
									dataArea.push(obj[i].subofficeName);
									dataTotalCount.push(totalCount);//指标数
									dataFollowCount.push(followCount);//关注数
									dataSubmitCount.push(submitCount);//提交数
									dataNotCompleteCount.push(notCompleteCount);//未完成指提交数   
									
									totalCount = 0;
									followCount = 0;
									submitCount = 0;
									notCompleteCount = 0;
								}
							}
							
						}
						if(districtNumber!='' && subofficeNumber!=''){//生成社区报表
							dataArea.push(obj[i].communityName);
							dataTotalCount.push(totalCount+obj[i].totalCount);//指标数
							dataFollowCount.push(followCount+obj[i].followCount);//关注数
							dataSubmitCount.push(submitCount+obj[i].submitCount);//提交数
							dataNotCompleteCount.push(notCompleteCount+obj[i].notCompleteCount);//未完成指提交数   
						}
						
						
					}//权限3、4
					
					
					if(authDegree=="5"){//柱状图数据
						
						if(subofficeNumber==''){//生成街道报表
							if(i==obj.length-1){
								dataArea.push(obj[i].subofficeName);
								
								totalCount = totalCount+obj[i].totalCount;
								followCount = followCount+obj[i].followCount;
								submitCount = submitCount+obj[i].submitCount;
								notCompleteCount = notCompleteCount+obj[i].notCompleteCount;
								
								dataTotalCount.push(totalCount);//指标数
								dataFollowCount.push(followCount);//关注数
								dataSubmitCount.push(submitCount);//提交数
								dataNotCompleteCount.push(notCompleteCount);//未完成指提交数   
								
								totalCount = 0;
								followCount = 0;
								submitCount = 0;
								notCompleteCount = 0;
							}else{
								if(obj[i].subofficeName == obj[i+1].subofficeName){
									totalCount = totalCount+obj[i].totalCount;
									followCount = followCount+obj[i].followCount;
									submitCount = submitCount+obj[i].submitCount;
									notCompleteCount = notCompleteCount+obj[i].notCompleteCount;
								}else{
									totalCount = totalCount+obj[i].totalCount;
									followCount = followCount+obj[i].followCount;
									submitCount = submitCount+obj[i].submitCount;
									notCompleteCount = notCompleteCount+obj[i].notCompleteCount;
									
									dataArea.push(obj[i].subofficeName);
									dataTotalCount.push(totalCount);//指标数
									dataFollowCount.push(followCount);//关注数
									dataSubmitCount.push(submitCount);//提交数
									dataNotCompleteCount.push(notCompleteCount);//未完成指提交数   
									
									totalCount = 0;
									followCount = 0;
									submitCount = 0;
									notCompleteCount = 0;
								}
							}
							
						}
						if(districtNumber!=''&& subofficeNumber!=''){//生成社区报表
							if(districtNumber!='' && subofficeNumber!=''){//生成社区报表
								dataArea.push(obj[i].communityName);
								dataTotalCount.push(totalCount+obj[i].totalCount);//指标数
								dataFollowCount.push(followCount+obj[i].followCount);//关注数
								dataSubmitCount.push(submitCount+obj[i].submitCount);//提交数
								dataNotCompleteCount.push(notCompleteCount+obj[i].notCompleteCount);//未完成指提交数   
							}
						}
						
						
					}//权限5
					
                    if(authDegree=="6" || authDegree=="7"){//柱状图数据
                    	    //生成社区报表
							dataArea.push(obj[i].communityName);
							dataTotalCount.push(totalCount+obj[i].totalCount);//指标数
							dataFollowCount.push(followCount+obj[i].followCount);//关注数
							dataSubmitCount.push(submitCount+obj[i].submitCount);//提交数
							dataNotCompleteCount.push(notCompleteCount+obj[i].notCompleteCount);//未完成指提交数   
							
						
					}//权限 6、7 
					
				}
				
			},
		error : function(res) {console.log("获取不到数据.");}
		});
	
	
	 //加载
	var option = {
			    tooltip : {
			        trigger: 'axis',
			        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
			            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
			        }
			    },
			    grid: {
			        left: '5%',
			        right: '5%',
			        bottom: '2%',
			        containLabel: true
			    },
			    xAxis : [
			        {
			            type : 'category',
			            data : dataArea,
			            show: true, 
			            name:'区域'
			        }
			    ],
			    yAxis : [
			        {
			        	type : 'value',
			        	show: true, 
			        	name:'数量'
			        }
			    ],
			    series : [
			        {
			            name:'指标数',
			            type:'bar',
			            data:dataTotalCount,
			            itemStyle: {   
			                //通常情况下： 
			                normal:{  
			                	color: ['#36A867'],
			                },  
			            }
			            
			        },  
			        {
			            name:'关注数',
			            type:'bar',
			            data:dataFollowCount,
			            itemStyle: {   
			                //通常情况下： 
			                normal:{  
			                	color: ['#3f88a4'],
			                },  
			            }
			        },
			        {
			            name:'提交数',
			            type:'bar',
			            data:dataSubmitCount,
			            itemStyle: {   
			                //通常情况下： 
			                normal:{  
			                	color: ['#96ce3e'],
			                },  
			            }
			        },
			        {
			            name:'未完成提交数',
			            type:'bar',
			            data: dataNotCompleteCount,
			            itemStyle: {   
			                //通常情况下： 
			                normal:{  
			                	color: ['#2d774c'],
			                },  
			            }
			        },
			       
			      
			    ]
			};
	 
	 //放入相应的容器，在页面显示
	 chartVehicle.setOption(option);
	 
	
}

//点击查询根据不同状态走不同方法
function view(){
	if(status==1){
		searchByareaNumber();
	}
    if(status==2){
    	viewHistogram();
	}
	
}
//选择表格
function view1(){
	$('#view2').removeClass("active");
	$('#view1').addClass("active");
	status=1;
	searchByareaNumber();
}

//选择柱状图
function view2(){
	$('#view1').removeClass("active");
	$('#view2').addClass("active");
	status=2;
	viewHistogram();
}

