var _url = "";

// 添加用户信息
function addUser() {
	$("#userIdDiv").empty();
	var html = "<input name='userId' id='userId' style='width: 202px;height: 22px;border:1px #b3b3b3 solid;' />";
	$("#userIdDiv").append(html);
	$("#userName").val("");
	$("#remark").val("");
	getUserDeartment("userdateTree");
	$('#dlg').dialog('open').dialog('center').dialog('setTitle', '添加用户');
	$("#roleId").find("option[value='1']").attr("selected", true);
	_url = root + "/services/addUser";
}
// 修改用户信息
function editUser() {
	var row = $('#dg').datagrid('getSelected');
	if (row) {
		$('#dlg').dialog('open').dialog('center').dialog('setTitle', '修改用户');
		$('#fm').form('load', row);
		// roleId编辑选中
		if (row.roleId == "管理员") {
			$("#roleId").find("option[value='0']").attr("selected", true);
		} else {
			$("#roleId").find("option[value='1']").attr("selected", true);
		}
		// 替换userIdDiv
		$("#userIdDiv").empty();
		$("#userIdDiv").append(row.userId);
		_url = root + '/services/editUser?id=' + row.id;
	} else {
		alert("请选择一条记录操作！");
	}

}
// 保存用户信息
function saveUser() {
	var userId = $("#userId").val();
	if (userId == "") {
		alert("用户名不能为空！");
		return;
	}
	var userName = $("#userName").val();
	if (userName == "") {
		alert("姓名不能为空！");
		return;
	}
	$('#fm').form('submit', {
		url : _url,
		onSubmit : function() {
			return $(this).form('validate');
		},
		success : function(result) {
			var res = eval('(' + result + ')');
			if (res.flag == "1") {// 操作成功
				alert(res.message);
				$('#dlg').dialog('close'); // close the dialog
				$('#dg').datagrid('reload'); // reload the user data
			} else if (res.flag == "2") {
				alert(res.message);
			} else {
				alert(res.message);
			}
		}
	});
}
// 删除用户信息
function destroyUser() {
	var row = $('#dg').datagrid('getSelected');
	if (!row) {
		alert("请选择一条记录操作！");
		return;
	}
	if (row.userId == "admin") {
		alert("系统管理员不能删除！");
		return;
	}
	if (row) {
		$.messager.confirm('删除用户', '您确定要删除该用户吗?', function(r) {
			if (r) {
				$.post(root + '/services/delUser', {
					id : row.id
				}, function(result) {
					if (result.flag == "1") {// 操作成功
						alert(result.message);
						$('#dg').datagrid('reload'); // reload the user data
					} else {
						alert(result.message);
					}
				}, 'json');
			}
		});
	}
}

//获取部门
function getUserDeartment(id){
	//alert(id);
	//设置只能选择日期的节点，不能选择年和月
	$('#'+id).combotree({
	    //获取数据URL  
	    url : root+'/services/getDepartment',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
    },
	    onSelect : function(node) {
	    	if(node.text=='运输局'||node.text=='总公司'){
	    		alert("请选择其他部门!");
	    		$('#'+id).tree("unselect");
	    	}
	    	/*if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
	    		getLine(node.text);
	    	}*/
	    }
	});
}
//获取角色
function getRole(id){
	//alert(id);
	//设置只能选择日期的节点，不能选择年和月
	$('#'+id).combotree({
	    //获取数据URL  
	    url : root+'/services/getRoleList',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
    },
	    onSelect : function(node) {
	    	/*if(node.text=='运输局'||node.text=='总公司'){
	    		alert("请选择其他部门!");
	    		$('#'+id).tree("unselect");
	    	}*/
	    	/*if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
	    		getLine(node.text);
	    	}*/
	    }
	});
}
