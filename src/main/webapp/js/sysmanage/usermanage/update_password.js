	

	/**
	*页面初始化事件
	*/
	$(function(){
		
	});
	
	 /**
	 *查询按钮事件
	 */
	 function save(){
		var originalPwd=$('#originalPwd').val();
		if(originalPwd=="") {
		 	alert("请输入原密码！");
		 	return;
		}
		var newPwd=$('#newPwd').val();
		if(newPwd=="") {
		 	alert("请输入新密码！");
		 	return;
		}
		if(newPwd.length<6) {
		 	alert("新密码不能少于6位字符！");
		 	return;
		}
		var confirmPwd=$('#confirmPwd').val();
		if(confirmPwd=="") {
		 	alert("请输入确认密码！");
		 	return;
		}
		if(confirmPwd.length<6) {
		 	alert("确认密码不能少于6位字符！");
		 	return;
		}
		if(newPwd!=confirmPwd) {
		 	alert("新密码和确认密码不一致！");
		 	return;
		}
		
		var url =root+ "/services/updatePwd?originalPwd="+hex_md5(originalPwd)
				+"&newPwd="+hex_md5(newPwd)+"&confirmPwd="+hex_md5(confirmPwd);
		 $.ajax({
			type : "POST",
			dataType : "json",
			url : url,
			async : false,
			success : function(result) {
				var flag=result.flag;
				if(flag=="success"){
					alert(result.message);
					window.location.reload();
				}else if(flag=="fail"){
					alert(result.message);
				}
				
			}
		});
	 	
	 }
	 function closeTab(){
		 window.parent.$('#centerTabs').tabs('close','修改密码');
    }
