
function search() {

	$('#table').bootstrapTable('destroy');
	/* radioVal = encodeURI(encodeURI(radioVal)); */
	var area = "阿迪";
	var type = "1";
	$("#table")
			.bootstrapTable(
					{
						url : root + '/services/getListAreaGrid?area=' + area
								+ "&type=" + type,
						method : 'post',
						contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
						striped : true,
						pagination : true,
						pageSize : 10,
						pageNumber : 1,
						height : 550,
						pageList : [ 10, 20, 30 ],
						silent : true,
						sortStable : true,
						sortName : 'time',
						sortOrder : 'desc',
						sidePagination : "client",
						/*
						 * responseHandler:function(res){ return{
						 * number:res.number, stoptype:res.stoptype }; },
						 */

						/*
						 * queryParams:{ radioVal:radioVal },
						 */
						/*
						 * queryParams:function(params){ return {
						 * radioVal:radioVal, }; },
						 */
						formatLoadingMessage : "请稍等,正在加载中。。。。。。。",
						columns : [
						/*
						 * { checkbox:true, valign:'middle', align:'center' },
						 */
						{
							field : 'index',
							title : '序号',
							width : 85,
							valign : 'middle',
							align : 'center'
						}, {
							field : "districtNumber",
							title : '行政区编号',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							field : "districtName",
							title : '行政区',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'subofficeNumber',
							title : '街道编号',
							width : 100,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'subofficeName',
							title : '街道',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'communityNumber',
							title : '社区编号',
							width : 100,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'communityName',
							title : '社区',
							width : 200,
							valign : 'middle',
							align : 'center'
						},
						/*
						 * { field:'operator', title:'操作者', width:200,
						 * valign:'middle', align:'center' },
						 */
						/*
						 * { field: 'operate', title: '操作', align: 'center',
						 * width:200, events: operateEvents, formatter:
						 * operateFormatter }
						 */

						{
							field : 'operate',
							title : '操作',
							align : 'center',
							formatter : operateFormatter,
							width : 100
						}

						/*
						 * { field:'revise', title:'修改', width:200,
						 * valign:'middle', align:'center',
						 * formatter:function(value,row,index){ return "<a
						 * href='reviseRole.jsp?lineId="+row.id+"'>"+row.revise+"</a>"; } }, {
						 * field:'delete', title:'删除', width:200,
						 * valign:'middle', align:'center',
						 * formatter:function(value,row,index){ return "<a
						 * href='javascript:void(0);'
						 * onclick='deleteRole("+row.id+")' >"+row.dele+"</a>"; } }
						 */
						]

					});
}
/** 每行后面显示操作的列 */
function operateFormatter(value, row, index) {

	return '<div style="line-height:32px;"><a href=\"javascript:void(0);\"  onclick=\"deleteArea(' + row.id
			+ ',' + row.type + ')\">' + '<img   src="../../images/delete.png"/>'
			+ '</a>&nbsp<a href=\"javascript:void(0);\" onclick=\"editArea('
			+ row.type +','+row.id+ ')\">' + '<img  src="../../images/update.png"/></a></div>';
	


}

$().ready(function() {
	 var radioVal=$('input:radio[name="stop_info_radio"]:checked').val(); 
	 loadDistrictNamedistrictName();
	 search();
	
	  $(".stop_info_radio").change(function(){
	 radioVal=$('input:radio[name="stop_info_radio"]:checked').val();
	  search(radioVal); });
	  
	  $('#myModal').on('hidden.bs.modal', function () {
		  var $browsers = $("input[name=chSelect]"); 
		  $browsers.prop("checked",false);
	
	  });
	 

});

function loadDistrictNamedistrictName(){
	//获取行政区下拉框
	$.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/queryAreaStatistics",
		success : function(result) {
			var obj = eval(result);
				for ( var i = 0; i < obj.length; i++) {
					$("#districtName").append(
							"<option value='"+obj[i].districtNumber+"'>"
							+ obj[i].districtName + "</option>");
				}
		},
		error : function(result) {
			alert("系统错误");
		}
	});
}

/**
 * 点击行政区加载街道
 */
function dArea() {
		var districtNumber = $('#districtName').val();
		var districtName = $('#districtName').find("option:selected").text();
		if(districtNumber == ''){//行政区为空，清空社区下拉并添加一个空
			$("#subofficeName").empty();
			$("#subofficeName").append(
					"<option value='" + "" + "'>"
					+ "请选择" + "</option>");
		}
		if (districtNumber != '') {//行政区不为空，清空社区下拉并添加
			$.ajax({
				url : root + "/services/queryAreaByUserId",
				type : "post",
				data : {
					type : 2,
					districtName : districtName
				},
				success : function(result) {
					$("#subofficeName").empty();
					var obj = eval(result);
					$("#subofficeName").append(
							"<option value='" + "" + "'>"
							+ "请选择" + "</option>");
					for ( var i = 0; i < obj.length; i++) {
						if(obj[i].districtName == districtName){
							$("#subofficeName").append(
									"<option value='" + obj[i].subofficeNumber + "'>"
									+ obj[i].subofficeName + "</option>");
						}
					}
				},
				error : function(result) {
					alert("系统错误");
				}
			});
		}
}
function queryAreaa() {
	var districtName= $('#districtName').find("option:selected").text();
	var subofficeName = $('#subofficeName').find("option:selected").text();
	var districtCode= $('#districtName').val();
	var subofficeCode = $('#subofficeName').val();
	var type = "";
	 $('#table').bootstrapTable('destroy');
     if(districtName=='请选择' && subofficeName=='请选择'){
    	 search();
    	 return;
     }
	 $("#table").bootstrapTable({
		 	url : root+'/services/queryGetArea?districtCode='+districtCode+'&subofficeCode='+subofficeCode+'&type='+type,  
			method:'post',
			contentType:'application/x-www-form-urlencoded;charset=UTF-8',
			striped:true,
			pagination:true,
			pageSize:10,
			pageNumber:1,
			height:550,
			pageList:[10,20,30],
			silent: true,
			sortStable:true,
			sortName:'time',
			sortOrder:'desc',
			sidePagination: "client",
			responseHandler:function(res){
				return{
					number:res.number,
					stoptype:res.stoptype
				};
			},
			/*queryParams:{
				 districtNumber:$('#districtName').val(),
				 subofficeNumber:$('#subofficeName').val()
			},
			queryParams:function(params){
				return {
					 districtNumber:$('#districtName').val(),
					 subofficeNumber:$('#subofficeName').val()

	           };
			},*/
			formatLoadingMessage : "请稍等,正在加载中。。。。。。。",
			columns : [
			/*
			 * { checkbox:true, valign:'middle', align:'center' },
			 */
			{
				field : 'index',
				title : '序号',
				width : 85,
				valign : 'middle',
				align : 'center'
			}, {
				field : "districtNumber",
				title : '行政区编号',
				width : 120,
				valign : 'middle',
				align : 'center'
			}, {
				field : "districtName",
				title : '行政区',
				width : 200,
				valign : 'middle',
				align : 'center'
			}, {
				field : 'subofficeNumber',
				title : '街道编号',
				width : 100,
				valign : 'middle',
				align : 'center'
			}, {
				field : 'subofficeName',
				title : '街道',
				width : 200,
				valign : 'middle',
				align : 'center'
			}, {
				field : 'communityNumber',
				title : '社区编号',
				width : 100,
				valign : 'middle',
				align : 'center'
			}, {
				field : 'communityName',
				title : '社区',
				width : 200,
				valign : 'middle',
				align : 'center'
			},
			/*
			 * { field:'operator', title:'操作者', width:200,
			 * valign:'middle', align:'center' },
			 */
			/*
			 * { field: 'operate', title: '操作', align: 'center',
			 * width:200, events: operateEvents, formatter:
			 * operateFormatter }
			 */

			{
				field : 'operate',
				title : '操作',
				align : 'center',
				formatter : operateFormatter,
				width : 100
			}
				]
		});
}	
function deleteArea(a, type) {
	var id = a;
	var dialog=art.dialog({
	    title: '提示',//弹出框 的标题 
	    content: '是否确定删除？',//弹出框中显示的内容 
	    ok: function () {
	    	$.post(root + "/services/deleteById", { 'id' : id,
	    		'type' : type},
					   function(data){
	    				var  resu=null;
			    		for(var key in data)  {
			    		    console.log("属性：" + key + ",值："+ data[key]);  
			    		    resu=data[9];
			    		}  
						if(resu==3){
							sweetAlert("No", "该行政区下已分配街道，请删除其下所有列表再删除！", "warning");
						}else if(resu==4){
							sweetAlert("No", "该社区已分配人员，请删除分配的人员再删除！", "warning");
						}else if(resu==1){
							sweetAlert("Yes", "删除成功！", "success");
							search();
						}else if(resu==5){
							sweetAlert("No", "该街道下已分配社区，请删除其下所有列表再删除！", "warning");
						}else if(resu==0){
							sweetAlert("No", "删除失败", "error");
						}
					   });
	    },
	    okVal:'确定',
	    cancel: function () {
	    },
	    fixed:true,
	    resize:true
	});
}
// 新增角色
$(function() {
});
function addArea(){
	$("#testdiv").empty();
    $('#myModal').modal('show');
}
//新增区域模态框
function selectOnchang() {
	var str = $('input[name="chSelect"]:checked ').val();
	/* var testdiv = $("type").select2("val"); */
	// 清空div
	$("#testdiv").empty();
	var select = "";
	if (str == 1) {
		select = "  </br><label id='laberLeftTwo'>行政区 &nbsp;   ：</label><input id='districtNameType' type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>行政区编码  ：</label><input id='districtNumber' type='text' class='form-control select2' style='width:200px' /></br>";
	} else if (str == 2) {
	
		select = "</br><label id='laberLeftTwo'>行政区  &nbsp;  ：</label><select id='districtNameSel'   class='form-control select2' name='districtNameSel'  style='width:200px' ><option value=''>请选择</option>"
				+ "</select></br>";
		select = select
				+ " <label id='laberLeftTwo'>街道   &nbsp;&nbsp; ：</label><input id='subofficeNameType'  type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>街道编码：</label><input id='ss'  type='text' class='form-control select2' style='width:200px' /></br>";
		selectDis(1);
	} else if (str == 3) {
		
		select = "</br><label id='laberLeftTwo'>行政区&nbsp;    ：</label><select id='districtNameSel'   class='form-control select2' name='districtNameSel'  style='width:200px' onclick='diArea(2)'><option value=''>请选择</option>"
				+ "</select></br>";
		select = select
				+ "<label id='laberLeftTwo'>街道   &nbsp;&nbsp; ：</label><select id='subOfficeSel'   class='form-control select2' name='subOfficeSel'  style='width:200px' onclick='diSub()'><option value=''>请选择</option>"
				+ "</select></br>";
		select = select
				+ " <label id='laberLeftTwo'>社区  &nbsp;&nbsp; ：</label><input id='communityNameType' type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>社区编码 ：</label><input id='communityNumber' type='text' class='form-control select2' style='width:200px' /></br>";
		selectDis(1);
	}
	document.getElementById("testdiv").innerHTML = document
			.getElementById("testdiv").innerHTML
			+ select;
}
function addsave(){
	var str = $('input[name="chSelect"]:checked ').val();
	if(str==1){
		var districtNumber = $('#districtNumber').val();
		var districtName = $('#districtNameType').val();
		var reg = /['";{}()+\-*\/!%#@?？]/;
		if(reg.test(districtName)){
			sweetAlert("No", "行政区名称不能包含特殊符号", "warning");
			return;
		}
		
		if(districtName==''){
			sweetAlert("No", "行政区名字不能为空", "warning");
			return;
		}
		if(districtNumber==''){
			sweetAlert("No", "行政区编码不能为空", "warning");
			return;
		}
		if(!(/(^[0-9]\d*$)/.test(districtNumber))){
			sweetAlert("No", "行政区编码不能为汉字、英文字母或小数点", "warning");
			return;
		}
		$.ajax({
			url : root+"/services/insertDistract",
			dataType : "json",
			type : "post",
			data : {
			districtNumber:districtNumber,
			districtName:districtName
		    },
			success : function(result) {

				if (result.flag == 1) {
					sweetAlert("Yes", result.message, "success");
					$('#myModal').modal('hide');
					$("#testdiv").empty();
					$('#checkSave').val(0);
					search();
					loadDistrictNamedistrictName();
					// 清空数据
				} else if (result.flag == 0) {
					/*alert(result.message);
	*/			} else if (result.flag == 3) {
		         sweetAlert("No", result.message, "error");
				}
			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}else if(str==2){
		var districtNumber = $('#districtNameSel').find("option:selected")
		.val();
        var districtName = $('#districtNameSel').find("option:selected")
		.text();
        var subofficeNumber = $('#ss').val();
        var subofficeName = $('#subofficeNameType').val();
        var reg = /['";{}()+\-*\/!%#@?？]/;
		if(reg.test(subofficeName)){
			sweetAlert("No", "街道名称不能包含特殊符号", "warning");
			return;
		}
        if(districtName=='请选择'){
        	sweetAlert("No", "行政区名字不能为空", "warning");
			return;
        }
        if(subofficeNumber==''){
        	sweetAlert("No", "街道编码不能为空", "warning");
        	return;	
        }
        if(subofficeName==''){
        	sweetAlert("No", "街道名字不能为空", "warning");
			return;	
		}
        if(!(/(^[0-9]\d*$)/.test(subofficeNumber))){
			sweetAlert("No", "街道编码不能为汉字或英文字母或小数点", "warning");
			return;
		}
        $.ajax({
			url : root+"/services/insertSubOffice",
			dataType : "json",
			type : "post",
			data : {
        	districtName : districtName,
			districtNumber : districtNumber,
			subofficeNumber : subofficeNumber,
			subofficeName : subofficeName
		    },
			success : function(result) {

				if (result.flag == 1) {
					sweetAlert("Yes", result.message, "success");
					$('#myModal').modal('hide');
					$("#testdiv").empty();
					$('#checkSave').val(0);
					search();
					loadDistrictNamedistrictName();
					// 清空数据
				} else if (result.flag == 0) {
					/*alert(result.message);
	*/			} else if (result.flag == 3) {
					sweetAlert("No", result.message, "warning");
				}
			},
			error : function(result) {
				sweetAlert("No", "系统错误", "error");
			}
		});
	}else if(str==3){
		var districtNumber = $('#districtNameSel').find("option:selected")
		.val();
        var districtName = $('#districtNameSel').find("option:selected")
		.text();
        var subofficeNumber = $('#subOfficeSel').find("option:selected")
		.val();
        var subofficeName = $('#subOfficeSel').find("option:selected")
		.text();
        var communityNumber = $('#communityNumber').val();
        var communityName = $('#communityNameType').val();
        var reg = /['";{}()+\-*\/!%#@?？]/;
		if(reg.test(communityName)){
			sweetAlert("No", "社区名称不能包含特殊符号", "warning");
			return;
		}
        if(districtName=='请选择'){
        	sweetAlert("No", "行政区名字不能为空", "warning");
			return;
        }
        if(subofficeName=='请选择'){
        	sweetAlert("No", "街道名字不能为空", "warning");
        	return;
        }
        if(communityName==''){
        	sweetAlert("No", "社区名字不能为空", "warning");
        	return;
        }
        if(communityNumber==''){
        	sweetAlert("No", "社区编码不能为空", "warning");
        	return;
        }
        if(!(/(^[0-9]\d*$)/.test(communityNumber))){
			sweetAlert("No", "社区编码不能为汉字、英文字母或小数点", "warning");
			return;
		}
        $.ajax({
			url : root+"/services/insertCommunity",
			dataType : "json",
			type : "post",
			data : {
        	districtName : districtName,
			districtNumber : districtNumber,
			subofficeNumber : subofficeNumber,
			subofficeName : subofficeName,
			communityName : communityName,
			communityNumber : communityNumber
		    },
			success : function(result) {
				if (result.flag == 1) {
					sweetAlert("Yes", result.message, "success");
					$('#myModal').modal('hide');
					$("#testdiv").empty();
					$('#checkSave').val(0);
					search();
					loadDistrictNamedistrictName();
					// 清空数据
				} else if (result.flag == 0) {
					/*alert(result.message);*/	
			    } else if (result.flag == 3) {
		        sweetAlert("No", result.message, "error");
				}
			},
			error : function(result) {
				sweetAlert("No", "系统错误", "error");
			}
		});
	}
}
/**
 * 点击行政区加载街道
 */
function diArea(flag) {
/*	var type = $('input[name="chSelect"]:checked ').val();
	if (flag == 2) {
		$("#subOfficeSel").empty();
		var districtNumber = $('#districtNameSel').find("option:selected")
				.val();
		var districtName = $('#districtNameSel').find("option:selected").text();
		if (districtNumber.trim() == '') {
			$("#subOfficeSel").append("<option value=''>请选择</option>");
		}
		if (districtNumber.trim() != '') {
			$.ajax({
				url : root + "/services/getAreaDownList",
				type : "post",
				data : {
					type : 2,
					districtName : districtName
				},
				success : function(result) {
					var obj = eval("(" + result + ")");
					if (obj.length == 0) {
						$("#subOfficeSel").append(
								"<option value='" + ' ' + "'>" + " "
										+ "</option>");
					} else {

						for ( var i = 0; i < obj.length; i++) {
							$("#subOfficeSel").append(
									"<option value='" + obj[i].id + "'>"
											+ obj[i].text + "</option>");
						}
					}
				},
				error : function(result) {
					alert("系统错误");
				}
			});

		}
	}*/
	var districtNumber = $('#districtNameSel').val();
	var districtName = $('#districtNameSel').find("option:selected").text();
	
	if(districtNumber == ''){//行政区为空，清空社区下拉并添加一个空
		$("#subOfficeSel").empty();
		$("#subOfficeSel").append(
				"<option value='" + " " + "'>"
				+ "请选择" + "</option>");
	}
	if (districtNumber != '') {//行政区不为空，清空社区下拉并添加
		$.ajax({
			url : root + "/services/queryAreaByUserId",
			type : "post",
			data : {
				type : 2,
				districtName : districtName
			},
			success : function(result) {
				
				$("#subOfficeSel").empty();
				
				var obj = eval(result);
				
				$("#subOfficeSel").append(
						"<option value='" + " " + "'>"
						+ "请选择" + "</option>");
				
				for ( var i = 0; i < obj.length; i++) {
					if(obj[i].districtName == districtName){
						
						$("#subOfficeSel").append(
								"<option value='" + obj[i].subofficeNumber + "'>"
								+ obj[i].subofficeName + "</option>");
					}
					
				}
				/*var obj = eval("(" + result + ")");
				if (obj.length == 0) {
					$("#subOfficeSel").append(
							"<option value='" + ' ' + "'>" + " "
									+ "</option>");
				} else {
					$("#subOfficeSel").append("<option value=''>请选择</option>");
					for ( var i = 0; i < obj.length; i++) {
						$("#subOfficeSel").append(
								"<option value='" + obj[i].areaId + "'>"
										+ obj[i].text + "</option>");
					}
				}*/
			},
			error : function(result) {
				alert("系统错误");
			}
		});

	}

}

function diSub() {

}

function selectDis(type) {
	
	/*$.ajax({
		url : root + "/services/getAreaDownList",
		type : "post",
		data : {
			type : type
		},
		success : function(result) {

			var obj = eval("(" + result + ")");
			for ( var i = 0; i < obj.length; i++) {
				$("#districtNameSel").append(
						"<option value='" + obj[i].id + "'>" + obj[i].text
								+ "</option>");
			}
		},
		error : function(result) {
			alert("系统错误");
		}
	});*/
	
	$.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/queryAreaStatistics",
		success : function(result) {
			
			var obj = eval(result);
			
		
				for ( var i = 0; i < obj.length; i++) {
					$("#districtNameSel").append(
							"<option value='" + obj[i].districtNumber + "'>"
							+ obj[i].districtName + "</option>");
				}
		},
		error : function(result) {
			alert("系统错误");
		}
	});
}

/**
 * 保存 、
 */
/*function save() {
	var type = $('input[name="chSelect"]:checked ').val();
	var checkSave = $('#checkSave').val();
	alert(checkSave);
	var url = "";
	var data = {

	};
	if (checkSave == 0) {
		if (type == 1) {
			url = "/services/insertDistract";
			var districtNumber = $('#districtNumber').val();
			var districtName = $('#districtName').text();
			var re = /^[1-9]+[0-9]*]*$/; 
			if(isNaN(districtNumber)){
				alert("行政区编码请输入数字！");
				return;
			}
			data = {
				districtName : districtName,
				districtNumber : districtNumber,
				type : type
			};
		} else if (type == 2) {
			url = "/services/insertSubOffice";
			var districtNumber = $('#districtNameSel').find("option:selected")
					.val();
			var districtName = $('#districtNameSel').find("option:selected")
					.text();
			var subofficeNumber = $('#ss').val();
			var subofficeName = $('#subofficeName').val();
			if(isNaN(subofficeNumber)){
				alert("街道编码请输入数字！");
				return;
			}
			 alert(subofficeName+"--"+subofficeNumber); 
			data = {
				districtName : districtName,
				districtNumber : districtNumber,
				type : type,
				subofficeNumber : subofficeNumber,
				subofficeName : subofficeName
			};
		} else if (type == 3) {
			url = "/services/insertCommunity";
			var districtNumber = $('#districtNameSel').find("option:selected")
					.val();
			var districtName = $('#districtNameSel').find("option:selected")
					.text();
			var subofficeNumber = $('#subOfficeSel').find("option:selected")
					.val();
			var subofficeName = $('#subOfficeSel').find("option:selected")
					.text();
			var communityNumber = $('#communityNumber').val();
			var communityName = $('#communityName').val();
			if(isNaN(communityNumber)){
				alert("社区编码请输入数字！");
				return;
			}
			data = {
				districtName : districtName,
				districtNumber : districtNumber,
				type : type,
				subofficeNumber : subofficeNumber,
				subofficeName : subofficeName,
				communityName : communityName,
				communityNumber : communityNumber
			};
		}
	} else {
		if (type == 1) {
			url = "/services/amendDistract";
			var districtNumber = $('#districtNumber').val();
			var districtName = $('#districtName').val();
			var id = $('#id').val();
			var oldName=$("#oldName").val();
			data = {
					id:id,
					nameOld:oldName,
				districtName : districtName,
				districtNumber : districtNumber,
				type : type
			};
		} else if (type == 2) {
			url = "/services/amendSubOffice";
			var districtNumber = $('#districtNameSel').find("option:selected")
					.val();
			var districtName = $('#districtNameSel').find("option:selected")
					.text();
			var subofficeNumber = $('#ss').val();
			var subofficeName = $('#subofficeName').val();
			 alert(subofficeName+"--"+subofficeNumber); 
			var id = $('#id').val();
			var oldName=$("#oldName").val();
			data = {
					id:id,
					nameOld:oldName,
				districtName : districtName,
				districtNumber : districtNumber,
				type : type,
				subofficeNumber : subofficeNumber,
				subofficeName : subofficeName
			};
		} else if (type == 3) {
			url = "/services/amendCommunity";
			var districtNumber = $('#districtNameSel').find("option:selected")
					.val();
			var districtName = $('#districtNameSel').find("option:selected")
					.text();
			var subofficeNumber = $('#subOfficeSel').find("option:selected")
					.val();
			var subofficeName = $('#subOfficeSel').find("option:selected")
					.text();
			var communityNumber = $('#communityNumber').val();
			var communityName = $('#communityName').val();
			var id = $('#id').val();
			var oldName=$("#oldName").val();
			data = {
				id:id,
				nameOld:oldName,
				districtName : districtName,
				districtNumber : districtNumber,
				type : type,
				subofficeNumber : subofficeNumber,
				subofficeName : subofficeName,
				communityName : communityName,
				communityNumber : communityNumber
			};
		}
	}

	submitSave(url, data);
}
function submitSave(url, data) {
	$.ajax({
		url : root + url,
		dataType : "json",
		type : "post",
		data : data,
		success : function(result) {

			if (result.flag == 1) {
				alert(result.message);
				$('#myModal').modal('hide');
				$("#testdiv").empty();
				$('#checkSave').val(0);
				search();
				// 清空数据
			} else if (result.flag == 0) {
				alert(result.message);
			} else if (result.flag == 3) {
				alert(result.message);
			}
		},
		error : function(result) {
			alert("系统错误");
		}
	});
}*/
var districtName="";
var subofficeName="";
var communityName="";
var districtNumber="";
var subofficeNumber="";
var communityNumber="";
var type = "";
/** 区域编辑 */
function editArea(trc,areaId) {
	type=trc;
	$.ajax({
		url : root + "/services/addEcho",
		type : "post",
		data : {
			areaId : areaId
		},
		async:false,
		success : function(result) {
		obj = eval(result);
		districtName=obj[0].districtName;
		subofficeName=obj[0].subofficeName;
		communityName=obj[0].communityName;
		districtNumber=obj[0].districtNumber;
		subofficeNumber=obj[0].subofficeNumber;
		communityNumber=obj[0].communityNumber;
		},
		error : function(result) {
			alert("系统错误");
		}
	});
	$('#myModall').modal('show');
	$("#upatediv").empty();
	
	if (type == 1) {
		select = "  </br><label id='laberLeftTwo'>行政区 &nbsp;   ：</label><input id='districtNameUpdate' value='"+districtName+"' type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>行政区编码  ：</label><input id='districtNumberUpdate'  value='"+districtNumber+"' type='text' class='form-control select2' style='width:200px' /></br>";
	}else if(type == 2){
		select = "  </br><label id='laberLeftTwo'>行政区 &nbsp;   ：</label><input id='districtNumberUpdate' disabled= 'true' value='"+districtName+"' type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>行政区编码  ：</label><input id='districtNumberUpdate' disabled= 'true'value='"+districtNumber+"' type='text' class='form-control select2' style='width:200px' /></br>";
		select= select + "<label id='laberLeftTwo'>街道 &nbsp;   ：</label><input id='subofficeNameUpdate' type='text' value='"+subofficeName+"' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>街道编码  ：</label><input id='subofficeNumberUpdate' value='"+subofficeNumber+"' type='text' class='form-control select2' style='width:200px' /></br>";
	}else if(type == 3){
		select = "  </br><label id='laberLeftTwo'>行政区 &nbsp;   ：</label><input id='districtNumberUpdate' disabled= 'true'  value='"+districtName+"'type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>行政区编码  ：</label><input id='districtNumberUpdate' disabled= 'true' value='"+districtNumber+"' type='text' class='form-control select2' style='width:200px' /></br>";
		select= select + "<label id='laberLeftTwo'>街道 &nbsp;   ：</label><input id='subofficeNameUpdate'  disabled= 'true' type='text'value='"+subofficeName+"' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>街道编码  ：</label><input id='subofficeNumberUpdate' disabled= 'true' type='text' value='"+subofficeNumber+"' class='form-control select2' style='width:200px' /></br>"
		+"<label id='laberLeftTwo'>社区 &nbsp;   ：</label><input id='communityNameUpdate' type='text' value='"+communityName+"' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>社区编码  ：</label><input id='communityNumberUpdate' type='text' value='"+communityNumber+"' class='form-control select2' style='width:200px' /></br>";
	}
	document.getElementById("upatediv").innerHTML = document
	.getElementById("upatediv").innerHTML
	+ select;
}

function saveUpdate() {
	if(type==1){
		var districtNumberUpdate = $('#districtNumberUpdate').val();
		var districtNameUpdate = $('#districtNameUpdate').val();
		if(isNaN(districtNumberUpdate)){
			sweetAlert("No", "行政区编码不能为汉字或英文字母", "warning");
			return;
		}
		if(districtNameUpdate==''){
			sweetAlert("No", "行政区名称不能改为空", "warning");
			return;
		}
		if(districtNumberUpdate==''){
			sweetAlert("No", "行政区编码不能改为空", "warning");
			return;
		}
		$.ajax({
			url : root + "/services/updateDistrict",
			type : "post",
			data : {
			districtNumberUpdate : districtNumberUpdate,
			districtNameUpdate:districtNameUpdate,
			districtName:districtName,
			districtNumber:districtNumber
			},
			success : function(result) {
				var obj = eval(result);
			    var	back = obj[0].back;
				if(back==1){
					$('#myModall').modal('hide');
				sweetAlert("Yes", "修改成功 ", "success");
				search();
				}else if(back==2){
					sweetAlert("No", "系统错误修改失败，请重新修改", "error");
				}else if(back==3){
					sweetAlert("No", "行政区名称重复", "error");
				}else if(back==4){
					sweetAlert("No", "行政区下有街道，不能修改编码", "error");
				}else if(back==5){
					sweetAlert("No", "行政区名称重复", "error");
				}else if(back==6){
					sweetAlert("No", "行政区编码重复", "error");
				}else if(back==8){
					$('#myModall').modal('hide');
					sweetAlert("No", "修改成功 ", "success");
					search();
				}else{
					sweetAlert("No", "系统错误", "error");
				}
			}
		});
		
	}else if(type==2){
		var subofficeNameUpdate = $('#subofficeNameUpdate').val();
		var subofficeNumberUpdate = $('#subofficeNumberUpdate').val();
		if(isNaN(subofficeNumberUpdate)){
			sweetAlert("No", "街道编码不能为汉字或英文字母", "warning");
			return;
		}
		if(subofficeNameUpdate==''){
			sweetAlert("No", "街道名称不能改为空", "warning");
			return;
		}
		if(subofficeNumberUpdate==''){
			
			sweetAlert("No", "街道编码不能改为空", "warning");
			return;
			}
			$.ajax({
				url : root + "/services/updateSuboffice",
				type : "post",
				data : {
				subofficeNameUpdate:subofficeNameUpdate,
				subofficeNumberUpdate:subofficeNumberUpdate,
				subofficeName:subofficeName,
				subofficeNumber:subofficeNumber
				},
				success : function(result) {
					var obj = eval(result);
				    var	back = obj[0].back;
					if(back==1){
						$('#myModall').modal('hide');
					sweetAlert("Yes", "修改成功 ", "success");
					search();
					}else if(back==2){
						sweetAlert("No", "系统错误修改失败，请重新修改", "error");
					}else if(back==3){
						sweetAlert("No", "街道名称重复", "error");
					}else if(back==4){
						sweetAlert("No", "街道下有社区，不能修改编码", "error");
					}else if(back==5){
						sweetAlert("No", "街道名称重复", "error");
					}else if(back==6){
						sweetAlert("No", "街道编码重复", "error");
					}else if(back=7){
						sweetAlert("No", "街道上有行政区，不能修改编码", "error");
					}else if(back==8){
						$('#myModall').modal('hide');
						sweetAlert("Yes", "修改成功 ", "success");
						search();
					}else{
						sweetAlert("No", "系统错误", "error");
					}
				}
			});
			
		}else if(type==3){
			var communityNameUpdate = $('#communityNameUpdate').val();
			var communityNumberUpdate = $('#communityNumberUpdate').val();
			if(isNaN(communityNumberUpdate)){
				sweetAlert("No", "社区编码不能为汉字或英文字母", "warning");
				return;
			}
			if(communityNameUpdate==''){
				sweetAlert("No", "社区名称不能改为空", "warning");
				return;
			}
			if(communityNumberUpdate==''){
				sweetAlert("No", "社区编码不能改为空", "warning");
				return;
			}
			$.ajax({
				url : root + "/services/updateCommunity",
				type : "post",
				data : {
				communityNameUpdate:communityNameUpdate,
				communityNumberUpdate:communityNumberUpdate,
				communityName:communityName,
				communityNumber:communityNumber
				},
				success : function(result) {
					var obj = eval(result);
				    var	back = obj[0].back;
					if(back==1){
						$('#myModall').modal('hide');
					sweetAlert("Yes", "修改成功 ", "success");
					search();
					}else if(back==2){
						sweetAlert("No", "系统错误修改失败，请重新修改", "error");
					}else if(back==3){
						sweetAlert("No", "社区名称重复", "error");
					}else if(back==4){
						sweetAlert("No", "社区上街道，不能修改编码", "error");
					}else if(back==5){
						sweetAlert("No", "社区名称重复", "error");
					}else if(back==6){
						sweetAlert("No", "社区编码重复", "error");
					}else if(back=7){
						sweetAlert("No", "社区上有行政区，不能修改编码", "error");
					}else if(back==8){
						$('#myModall').modal('hide');
						sweetAlert("Yes", "修改成功 ", "success");
						search();
					}else{
						sweetAlert("No", "系统错误", "error");
					}
				}
			});
		}
	}


// 修改回显数据
/*function selectOnchangBack(type) {
	alert(type);
	var str = type;
	 var testdiv = $("type").select2("val"); 
	// 清空div
	$("#updatee").empty();
	var select = "";
	if (str == 1) {
		select = "  </br><label id='laberLeftTwo'>行政区 &nbsp;   ：</label><input id='districtName' type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>行政区编码  ：</label><input id='districtNumber' type='text' class='form-control select2' style='width:200px' /></br>";
	} else if (str == 2) {
		select = "</br><label id='laberLeftTwo'>行政区  &nbsp;  ：</label><select id='districtNameSel'   class='distant form-control select2' name='districtNameSel'  style='width:200px' ><option value='0'>请选择</option>"
				+ "</select></br>";
		select = select
				+ " <label id='laberLeftTwo'>街道   &nbsp;&nbsp; ：</label><input id='subofficeName'  type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>街道编码：</label><input id='ss'  type='text' class='form-control select2' style='width:200px' /></br>";
		selectDis(1);
	} else if (str == 3) {
		select = "</br><label id='laberLeftTwo'>行政区&nbsp;    ：</label><select id='districtNameSel'  class='distant form-control select2'   name='districtNameSel'  style='width:200px' onclick='diArea(2)'><option value=''>请选择</option>"
				+ "</select></br>";
		select = select
				+ "<label id='laberLeftTwo'>街道   &nbsp;&nbsp; ：</label><select id='subOfficeSel'   class='form-control select2' name='subOfficeSel'  style='width:200px' onclick='diSub()'><option value=''>请选择</option>"
				+ "</select></br>";
		select = select
				+ " <label id='laberLeftTwo'>小区  &nbsp;&nbsp; ：</label><input id='communityName' type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>小区编码 ：</label><input id='communityNumber' type='text' class='form-control select2' style='width:200px' /></br>";
		selectDis(1);
	}
	document.getElementById("testdiv").innerHTML = document
			.getElementById("testdiv").innerHTML
			+ select;

}

*/
function ajaxFileUpload()  
{  
	/*document.getElementById("formid").submit();*/
      var type=$('input:radio:checked').val(); 
      if(isNaN(type)){
    	  //alert("请选择导入类型！");
    	  swal("请选择导入类型！", "", "warning");
    	  return;
      }
      var excelPath = $("#file").val();
      if(excelPath == null || excelPath == ''){
    	  swal("请选择要上传的Excel文件！", "", "warning");
          //alert("请选择要上传的Excel文件");
          return;
      }else{
    	  var fileExtend = excelPath.substring(excelPath.lastIndexOf('.')).toLowerCase();
    	  if(fileExtend != '.xls'){
    		  swal("请文件格式需为'.xls'格式！", "", "warning");
    		  //alert("文件格式需为'.xls'格式");
    		  return;
    	  }
      }
      $.ajaxFileUpload({ 
             url:root+'/services/fileUpload?type='+type,             //需要链接到服务器地址  
             secureuri:false,  
             fileElementId:'file',  
             dataType: 'json',  
             type : "post",//服务器返回的格式，可以是json  
             success: function (data,status)             //相当于java中try语句块的用法  
             {     
            	 var result = data;
            	 var flag = result.flag;
                 //alert(result.message); data是从服务器返回来的值 
            	 if(flag==0){
            		 swal("操作失败", result.message, "error");
            	 }else{
            		 swal("操作成功", result.message, "success");
            	 }
            
             },  
             error: function (data, status, e)             //相当于java中catch语句块的用法  
             {  
                 alert("系统错误");
             }  
           }  
         );  
}  
