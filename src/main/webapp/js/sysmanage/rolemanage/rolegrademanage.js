// var updateIdPubllic=null;
// var updateRolename=null;
// var peopolId=null; // 人员管理id
// var actorId=null;	// 角色管理id
// var authId=null;	//  普通权限id
//
// var peopolOld=null; // 人员管理
// var actorOld=null;	// 角色管理
// var authOld=null;	//  普通权限
// function search(){
//     $('#table').bootstrapTable('destroy');
//     /*	 radioVal = encodeURI(encodeURI(radioVal));    */
//     $("#table").bootstrapTable({
//         url : root+'/services/getRoleList',
//         method:'post',
//         contentType:'application/x-www-form-urlencoded;charset=UTF-8',
//         striped:true,
//         pagination:true,
//         pageSize:10,
//         pageNumber:1,
//         height:450,
//         pageList:[10,20,30],
//         silent: true,
//         sortStable:true,
//         sortName:'time',
//         sortOrder:'desc',
//         sidePagination: "client",
//         /*responseHandler:function(res){
//             return{
//                 number:res.number,
//                 stoptype:res.stoptype
//             };
//         },*/
//
//         /*queryParams:{
//             radioVal:radioVal
//         },*/
//         /*queryParams:function(params){
//             return {
//                radioVal:radioVal,
//
//            };
//         },*/
//         formatLoadingMessage:"请稍等,正在加载中。。。。。。。",
//         columns:[
//             /*
//             {
//                 checkbox:true,
//                 valign:'middle',
//                 align:'center'
//             },
//             */
//             {
//                 field:'i',
//                 title:'number',
//                 width:100,
//                 valign:'middle',
//                 align:'center'
//             },
//             {
//                 field:'studentnumber',
//                 title:'studentnumber',
//                 width:100,
//                 valign:'middle',
//                 align:'center'
//             },
//             {
//                 field:"roleName",
//                 title:'rolename',
//                 width:100,
//                 valign:'middle',
//                 align:'center'
//             },
//             {
//                 field:'math',
//                 title:'math',
//                 width:200,
//                 valign:'middle',
//                 align:'center'
//             },
//             {
//                 field:'chinese',
//                 title:'chinese',
//                 width:180,
//                 valign:'middle',
//                 align:'center'
//             },
//             {
//                 field:'english',
//                 title:'english',
//                 width:200,
//                 valign:'middle',
//                 align:'center'
//             },
//             {
//                 field:'revise',
//                 title:'operate',
//                 width:200,
//                 valign:'middle',
//                 align:'center',
//                 formatter:function(value,row,index){
//                     return "<a href='javascript:void(0);' onclick='updateGradeRole("+row.id+")'><img  src='../../images/update.png'/></a>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='deleteGradeRole("+row.id+")' ><img  src='../../images/delete.png'/></a>";
//                 }
//             },
//
//         ]
//
//     });
// }
// $().ready(function(){
//     /*	var radioVal=$('input:radio[name="stop_info_radio"]:checked').val();       href='reviseRole.jsp?id="+row.id+"'*/
//     search();
//
//     $('#myModal').on('hidden.bs.modal', function () {
//
//         $("#Checkbox").prop("checked",false);
//         $("#CheckboxR").prop("checked",false);
//         $("#CheckboxA").prop("checked",false);
//         $("#CheckboxC").prop("checked",false);
//         $("#CheckboxE").prop("checked",false);
//
//         $("input[name='RadioOptionss']").prop("checked",false);
//
//         document.getElementById("DivHiden").style.display = "block";
//
//     })
//     $('#update').on('hidden.bs.modal', function () {
//         document.getElementById("DivHidenT").style.display = "block";
//         document.getElementById("lable4").style.display = "inline";
//         document.getElementById("lable5").style.display = "inline";
//         document.getElementById("lable6").style.display = "inline";
//
//         $("#CheckUpUser").prop("checked",false);
//         $("#CheckUpActor").prop("checked",false);
//         $("#CheckUpA").prop("checked",false);
//         $("#CheckUpC").prop("checked",false);
//         $("#CheckUpE").prop("checked",false);
//
//         $("input[name='upRole']").prop("checked",false);
//
//         document.getElementById("DivHidenT").style.display = "block";
//     })
//
//     /*	$(".stop_info_radio").change(function(){
//         radioVal=$('input:radio[name="stop_info_radio"]:checked').val();
//         search(radioVal);
//     });
//     */
//
// });
//
// //删除角色
// function deleteGradeRole(id) {
//     var dialog=art.dialog({
//         title: '提示',//弹出框 的标题
//         content: 'sure？',//弹出框中显示的内容
//         ok: function () {
//             $.post(root+"/services/deleteGradeRole", { id: id},
//                 function(data){
//                     var  resu=null;
//                     for(var key in data)  {
//                         console.log("属性：" + key + ",值："+ data[key]);
//                         resu=data[10];
//                     }
//                     if(resu==0){
//                         art.dialog.tips('该角色正在使用，无法删除！', 1.5);
//                     }else{
//                         art.dialog.tips('删除成功！', 1.5);
//                         search();
//                     }
//                 });
//         },
//         okVal:'确定',
//         cancel: function () {
//         },
//         fixed:true,
//         resize:true
//     });
// }
//
//
// //新增角色
// function save() {
//
//     var roleName=$('[name="roleName"]').val();
//
//     var reg = /^[\u4E00-\u9FA5a-zA-Z0-9_]{3,20}$/;
//
//     if (!reg.test(roleName)) {
//         alert("角色名格式不正确");
//         return;
//     }
//     // 获取 单选
//     var resvice="";
//     var radios=document.getElementsByName("RadioOptionss");
//     for(var i=0;i<radios.length;i++)
//     {
//         if(radios[i].checked==true)
//         {
//             resvice=radios[i].value;
//         }
//     }
//     if (resvice == "") {
//         alert("权限不能为空！");
//         return;
//     }
//     // 获取人员管理
//     var  peo =null;
//     var radiosPoe=document.getElementsByName("Checkbox");
//     for(var i=0;i<radiosPoe.length;i++){
//         if(radiosPoe[i].checked==true){
//             peo=radiosPoe[i].value;
//         }
//     }
//
//
//     // 获取角色管理
//     var  actor =null;
//     var radiosAct=document.getElementsByName("CheckboxR");
//     for(var i=0;i<radiosAct.length;i++)
//     {
//         if(radiosAct[i].checked==true)
//         {
//             actor=radiosAct[i].value;
//         }
//     }
//
//     // 获取区域管理
//     var  area =null;
//     var radiosAct=document.getElementsByName("CheckboxA");
//     for(var i=0;i<radiosAct.length;i++)
//     {
//         if(radiosAct[i].checked==true)
//         {
//             area=radiosAct[i].value;
//         }
//     }
//
//     // 获取参数管理
//     var  param =null;
//     var radiosAct=document.getElementsByName("CheckboxC");
//     for(var i=0;i<radiosAct.length;i++)
//     {
//         if(radiosAct[i].checked==true)
//         {
//             param=radiosAct[i].value;
//         }
//     }
//
//     // 获取审核管理
//     var  examine =null;
//     var radiosAct=document.getElementsByName("CheckboxE");
//     for(var i=0;i<radiosAct.length;i++)
//     {
//         if(radiosAct[i].checked==true)
//         {
//             examine=radiosAct[i].value;
//         }
//     }
//
//     // 获取备注
//     var remark=document.getElementById("remark").value;
//     var describeActor=document.getElementById("describeActor").value;
//     if (roleName == "") {
//         alert("角色名不能为空！");
//         return;
//     }
//     var dialog=art.dialog({
//         title: '提示',//弹出框 的标题
//         content: '是否确定添加？',//弹出框中显示的内容
//         ok: function () {
//             $.post(root+"/services/addRole", {roleName:roleName,
//                     resvice:resvice,
//                     peo:peo,
//                     actor:actor,
//                     area: area,
//                     param:param,
//                     examine:examine,
//                     describeActor:describeActor,
//                     remark:remark,},
//                 function(data){
//                     var  resu=null;
//                     for(var key in data)  {
//                         console.log("属性：" + key + ",值："+ data[key]);
//                         resu=data[10];
//                     }
//                     if(resu==1){
//                         art.dialog.tips('添加成功！', 1.5);
//                         $('#myModal').modal('hide');
//                         search();
//                         $("#roleName").val("");
//                         $("#describeActor").val("");
//                         $("#remark").val("");
//
//                         //location.reload();
//                     }else{
//                         art.dialog.tips('角色名已存在', 1.5);
//                     }
//                 });
//         },
//         okVal:'确定',
//         cancel: function () {
//         },
//         fixed:true,
//         resize:true
//     });
// }
// //  单选显示隐藏
// function show(obj) {
//     if (obj.id == "inlineRadioCity") {
//         document.getElementById("DivHiden").style.display = "block";
//         document.getElementById("lable1").style.display = "inline";
//         document.getElementById("lable2").style.display = "inline";
//         document.getElementById("lable3").style.display = "inline";
//
//     }else if(obj.id == "inlineRadioOne" ){
//         document.getElementById("DivHiden").style.display = "block";
//         document.getElementById("lable1").style.display = "none";
//         document.getElementById("lable2").style.display = "none";
//         document.getElementById("lable3").style.display = "inline";
//
//     }else if(obj.id == "inlineRadioTwo"|| obj.id == "inlineRadioThree"){
//         document.getElementById("DivHiden").style.display = "block";
//         document.getElementById("lable1").style.display = "none";
//         document.getElementById("lable2").style.display = "none";
//         document.getElementById("lable3").style.display = "none";
//
//     }
//     else {
//         document.getElementById("DivHiden").style.display = "none";
//     }
// }
//
// //单选显示隐藏   修改
// function showUp(obj) {
//     if(obj.id=="uproleCity"){
//         document.getElementById("DivHidenT").style.display = "block";
//         document.getElementById("lable4").style.display = "inline";
//         document.getElementById("lable5").style.display = "inline";
//         document.getElementById("lable6").style.display = "inline";
//     }else if (obj.id == "uproleOne") {
//         document.getElementById("DivHidenT").style.display = "block";
//         document.getElementById("lable4").style.display = "none";
//         document.getElementById("lable5").style.display = "none";
//         document.getElementById("lable6").style.display = "inline";
//     }else if (obj.id == "uproleTwo"|| obj.id == "uproleTree" ) {
//         document.getElementById("DivHidenT").style.display = "block";
//         document.getElementById("lable4").style.display = "none";
//         document.getElementById("lable5").style.display = "none";
//         document.getElementById("lable6").style.display = "none";
//     }else {
//         document.getElementById("DivHidenT").style.display = "none";
//     }
// }
//
// //修改  角色信息
// function updateGradeRole(id){
//
//     $.ajax({
//         url:root+'/services/selectAndReviceRole',
//         type:'post',
//         data:{
//             id:id,
//         },
//         success:function(data){
//             $.each($.parseJSON(data),function(i,n){
//                 var upDtaId=n.id;
//                 updateIdPubllic=upDtaId;
//                 var roleName=n.roleName;
//                 updateRolename=roleName;
//                 var remarkUp=n.remark;
//                 var describeActor=n.describeActor;
//                 var peopol=n.peopol; // 人员管理
//                 var actor=n.actor;	// 角色管理
//
//                 var area = n.area;//区域管理
//                 var param = n.param;//参数管理
//
//                 var examine = n.examine;//审核管理权限
//
//                 var auth=n.auth;	//  普通权限
//                 //向模态框中传值
//                 $('#roleNameUp').val(roleName);
//                 $('#remarkUp').val(remarkUp);
//                 $('#describeActorUp').val(describeActor);
//                 //	    debugger;
//                 if(peopol==1){
//                     $("#CheckUpUser").prop("checked",true);
//                 }
//                 if(actor==2){
//                     //    	$("#CheckUp2").is(":checked") = false
//                     $("#CheckUpActor").prop("checked",true);
//                 }
//                 if(area==8){
//                     $("#CheckUpA").prop("checked",true);
//                 }
//                 if(param==9){
//                     $("#CheckUpC").prop("checked",true);
//                 }
//
//                 if(examine==10){
//                     $("#CheckUpE").prop("checked",true);
//                 }
//                 if(auth==3){
//                     document.getElementById('uproleCity').checked = true;
//                     //document.getElementById("DivHidenT").style.display = "none";
//                 }
//                 if(auth==4){
//                     document.getElementById('uproleOne').checked = true;
//
//                     document.getElementById("lable4").style.display = "none";
//                     document.getElementById("lable5").style.display = "none";
//
//                 }
//                 if(auth==5){
//                     document.getElementById('uproleTwo').checked = true;
//                     document.getElementById("lable4").style.display = "none";
//                     document.getElementById("lable5").style.display = "none";
//                     document.getElementById("lable6").style.display = "none";
//
//                 }
//                 if(auth==6){
//                     document.getElementById('uproleTree').checked = true;
//                     document.getElementById("lable4").style.display = "none";
//                     document.getElementById("lable5").style.display = "none";
//                     document.getElementById("lable6").style.display = "none";
//
//                 }
//                 if(auth==7){
//                     document.getElementById('uproleSur').checked = true;
//                     document.getElementById("DivHidenT").style.display = "none";
//                 }
//             });
//         }
//
//     });
//     $('#update').modal('show')
// }
//
// //  修改 角色  保存
// function updateRolenew() {
//     var roleName=$('[name="roleNameUp"]').val();
//
//     var reg = /^[\u4E00-\u9FA5a-zA-Z0-9_]{3,20}$/;
//
//     if (!reg.test(roleName)) {
//         alert("角色名格式不正确");
//         return;
//     }
//     // 判断是否修改角色名
//     if(updateRolename==roleName){
//         roleName=1;
//     }
//     // 获取 单选
//     var resvice="";
//     var radios=document.getElementsByName("upRole");
//     for(var i=0;i<radios.length;i++)
//     {
//         if(radios[i].checked==true)
//         {
//             resvice=radios[i].value;
//         }
//     }
//     if (resvice == "") {
//         alert("权限不能为空！");
//         return;
//     }
//     // 获取人员管理
//     var  peo =null;
//     var radiosPoe=document.getElementsByName("CheckUpUser");
//     for(var i=0;i<radiosPoe.length;i++)
//     {
//         if(radiosPoe[i].checked==true)
//         {
//             peo=radiosPoe[i].value;
//         }
//     }
//     // 获取角色管理
//     var  actor =null;
//     var radiosAct=document.getElementsByName("CheckUpActor");
//     for(var i=0;i<radiosAct.length;i++)
//     {
//         if(radiosAct[i].checked==true)
//         {
//             actor=radiosAct[i].value;
//         }
//     }
//
//     // 获取区域管理
//     var  area =null;
//     var radiosAct=document.getElementsByName("CheckUpA");
//     for(var i=0;i<radiosAct.length;i++)
//     {
//         if(radiosAct[i].checked==true)
//         {
//             area=radiosAct[i].value;
//         }
//     }
//
//     // 获取参数管理
//     var  param =null;
//     var radiosAct=document.getElementsByName("CheckUpC");
//     for(var i=0;i<radiosAct.length;i++)
//     {
//         if(radiosAct[i].checked==true)
//         {
//             param=radiosAct[i].value;
//         }
//     }
//
//     // 获取审核管理
//     var  examine =null;
//     var radiosAct=document.getElementsByName("CheckUpE");
//     for(var i=0;i<radiosAct.length;i++)
//     {
//         if(radiosAct[i].checked==true)
//         {
//             examine=radiosAct[i].value;
//         }
//     }
//     // 获取备注
//     var remark=document.getElementById("remarkUp").value;
//     var describeActor=document.getElementById("describeActorUp").value;
//     if (roleName == "") {
//         alert("角色名不能为空！");
//         return;
//     }
//     var dialog=art.dialog({
//         title: '提示',//弹出框 的标题
//         content: '是否确定修改？',//弹出框中显示的内容
//         ok: function () {
//             $.post(root+"/services/reviseRole", {roleName:roleName,id:updateIdPubllic,
//                     resvice:resvice,
//                     peo:peo,
//                     actor:actor,
//                     area:area,
//                     param:param,
//                     examine:examine,
//                     remark:remark,
//                     describeActor:describeActor
//                 },
//                 function(data){
//                     var  resu=null;
//                     for(var key in data)  {
//                         console.log("属性：" + key + ",值："+ data[key]);
//                         resu=data[10];
//                     }
//                     if(resu==1){
//                         art.dialog.tips('修改成功！', 1.5);
//                         $('#update').modal('hide');
//                         search();
//
//                         //location.reload();
//                     }else{
//                         art.dialog.tips('角色名已存在', 1.5);
//                     }
//                 });
//         },
//         okVal:'确定',
//         cancel: function () {
//         },
//         fixed:true,
//         resize:true
//     });
// }