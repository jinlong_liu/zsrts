
function search(){
	var number =[];
	var people = [];
	
	 var sex = $('#sex').find("option:selected").val();
	 var work = $('#work').find("option:selected").val();
	 var isLocal = $('#isLocal').find("option:selected").val();
	 var starAge = $('#starAge').val();
	 var endAge = $('#endAge').val();
	 
	 var toDate = $('#toDate').val();
	
		if(isNaN(starAge)){
			sweetAlert("No", "年龄请输入数字！", "warning");
			return;
		}
		if(isNaN(endAge)){
			sweetAlert("No", "年龄请输入数字！", "warning");
			return;
		}
	
		/*if(Number(endAge)<Number(starAge)){
			sweetAlert("NO", "结束年龄不能小于开始年龄！", "warning");
			return;
		}*/
        
	
	$.ajax({
		url : root + "/services/selectSex",
		type : "post",
		//async : false,
		data : {
			sex : sex,
			work : work,
			isLocal : isLocal,
			starAge : starAge,
			endAge : endAge,
			toDate:toDate
		},
		success : function(result) {
		var avg = 0;//平均数
		var count = 0;//总次数
		var pCount = 0;//总人数
		obj = eval(result);
		//alert(obj.length);
		if(obj.length>0){
			var total = 0;
			for ( var i = 0; i < obj.length; i++) {
				if(i==0){
					total = obj[i].total;
				}else if(obj[i].total > obj[i-1].total){
					total = obj[i].total;
				}
				count = count + obj[i].total*obj[i].personNumber;
				pCount = pCount + obj[i].personNumber;
			}
			avg = count/pCount;
			
			//alert(avg.toFixed(2));
			var k = 0;
			for(var j=0;j<=total;j++){
				if(obj[k].total == j){
					number.push(obj[k].personNumber);
					k++;
				}else{
					number.push(0);
				}
				//alert(manNumber);
				people.push(j);
				
			}
		}

		document.getElementById("avg").innerHTML = "<label>平均出行次数:</label>"+avg.toFixed(2);
		
		var chartVehicle = echarts.init(document.getElementById("view"));//初始化
		option = {
			    tooltip : {
			        trigger: 'axis'
			    },
			    grid: {
			        left: '10%',
			        right: '10%',
			        bottom: '2%',
			        containLabel: true
			    },
			    /*legend: {
			        data:['邮件营销','联盟广告','视频广告','直接访问','搜索引擎']
			    },*/
			    toolbox: {
			        show : true,
			        feature : {
			            mark : {show: true},
			           // dataView : {show: true, readOnly: false},
			            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    xAxis : [
			        {
			            type : 'category',
			            name: "出行次数",
			            boundaryGap : false,
			            data :people
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value',
			            name: "出行人数"
			        }
			    ],
			    series : [
			        {
			            name:'出行人数',
			            type:'line',
			            smooth:true,
			            stack: '总量',
			            data:number,
			            itemStyle: {   
			                //通常情况下： 
			                normal:{  
			                	color: ['#06b370'],
			                },  
			            }
			        },
			      
			      /*  {
			            name:'女',
			            type:'line',
			            stack: '总量',
			            data:wumanNumber
			        }*/
			     
			    ]
			};
		chartVehicle.setOption(option);                    
		
		},
		error : function(result) {
			alert("系统错误");
		}
	});

	
	
}
$().ready(function(){
	//页面加载
	search();
	
});

/*加载提示框*/
$(function() {
    $(document).ajaxStart(function () { $(".loaderTip,.bgBox").css("display", "block"); });  
    $(document).ajaxStop(function () { $(".loaderTip,.bgBox").css("display", "none"); });

    $(".pager a").click(function() {
        var Is = $(this).attr("disabled");
        if (Is != true) {
            $(".loaderTip,.bgBox").css("display", "block");
        }
    });
});


