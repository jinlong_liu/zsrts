

/**
 * ***************************************************************
 * 
 * @file_name 	zscommon.js
 * 
 * @description	通用js函数封装
 * 
 * @author 		liang.li 
 * 
 * @version		1.0
 * 
 * @date 		2016.05.14
 * 
 * 
 * ***************************************************************
 */

/**
 * @description
 * @parameters
 * 			url: url
 * 			para: parameters
 * 			success: retture value
 * 			for example: 
 * 			var test = function(result) {

				if (result) {
					var para1 = result.para1;
					var para2 = result.para2;
				};
			};
 */
var ajax = function(url, para, ret) {
	
	$.ajax({
		type : "POST",
		url : url,
		data : para,
		dataType : "json",
		contentType : "application/x-www-form-urlencoded;charset=utf-8",
		async : false,
		cache : false,
		success : ret,
		error : function(data, status, e) {
			if (status == 'error')
				alert(data.statusText);
		}
	});
};
