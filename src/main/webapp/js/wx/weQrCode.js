$().ready(function() {
	search();
	getSelect();
});
/* 页面的全部查询 */
function search() {
	$('#table').bootstrapTable('destroy');
	$("#table").bootstrapTable({
		url : root + '/services/getAreaCodeList',
		method : 'post',
		contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
		striped : true,// 隔行变色
		pagination : true,// 分页
		pageSize : 10,
		pageNumber : 1,
		height : 530,
		pageList : [ 10, 20, 30, 40 ],
		silent : true, // 是否显示对话框.该属性为True时,该对话框不再显示脚本错误
		sortStable : true, // 排序
		sortName : 'time',
		sortOrder : 'desc',
		sidePagination : "client",
		formatLoadingMessage : "请稍等,正在加载中。。。。。。。",
		columns : [ {
			checkbox : true,
			maintainSelected : true
		},{
			field : "area",
			title : '行政区',
			width : 200,
			valign : 'middle',
			align : 'center'
		}, {
			field : "street",
			title : '街道',
			width : 200,
			valign : 'middle',
			align : 'center'
		}, {
			field : 'community',
			title : '社区',
			width : 200,
			valign : 'middle',
			align : 'center'
		}, ]

	});
}
function showLoader() {
	$("body").mLoading("show");
}
function hideLoader() {
	$("body").mLoading("hide");
}

/* 生成二维码 */
var num = 0;
function createQr() {
	var buffer = new StringBuffer();
	$.map($("#table").bootstrapTable('getSelections'), function(row) {
		buffer.Append(row.id);
		buffer.Append(",");
	});
	// 获取二维码类型
	var weradioValue = "";
	var wecode = document.getElementsByName("optionsRadiosinline");
	for (var g = 0; g < wecode.length; g++) {
		if (wecode[g].checked == true) {
			weradioValue = wecode[g].value;
		}
	}
	var weChechboxValue = "";
	if ($('#inlineCheckbox').is(':checked')) {
		weChechboxValue = 1;
	}
	if ((buffer.ToString() == "" || buffer.ToString().length < 0)
			&& weradioValue != 1) {
		swal("请您先选取需要生成二维码的社区！", "", "error");
	} else {

		var data = {
			'buffer' : buffer.ToString(),
			'weradioValue' : weradioValue,
			'weChechboxValue' : weChechboxValue
		};
		$
				.ajax({
					type : "POST",
					dataType : "json",
					beforeSend : showLoader,
					url : root + "/services/getWeQrCode",
					data : data,
					complete : hideLoader,
					success : function(result) {
						var res = result.flag;
						var message = result.message;
						if (res == 1) {
							swal("", "二维码生成成功！", "success");
						    //var url="http://16820215zl.iask.in/zsrts/weQrCode/"+message+".zip";
							var url = "http://47.92.86.63/zsrts/weQrCode/"
									+ message + ".zip";
							window.location.href = url;

						} else {
							if (num < 3) {
								num++;
								swal("很遗憾，出现了错误。正在尝试解决错误，点击确定重新生成。", "第" + num
										+ "次", "error");
								$
										.ajax({
											type : "POST",
											dataType : "json",
											beforeSend : showLoader,
											url : root
													+ "/services/getWeQrCode",
											data : data,
											complete : hideLoader,
											success : function(result) {
												/*swal("", "二维码生成成功！", "success");
												var url = "http://16820215zl.iask.in/zsrts/weQrCode/"
														+ message + ".zip";*/
												var url = "http://47.92.86.63/zsrts/weQrCode/"
													+ message + ".zip";
												window.location.href = url;
											}
										});
							} else {
								swal("很遗憾，错误无法解决。请您联系管理员！", "", "error");
							}

						}
					}
				});
	}
}
/* 获取选中的单选框 */
function getIdSelections() {
	return $.map($("#table").bootstrapTable('getSelections'), function(row) {
		if ($('#table').attr("checked", true)) {
			//alert(row.id);
		}
		return row.id
	});
}
/* 利用数组生成StringBuffer */
function StringBuffer() {
	this.__strings__ = [];
};
StringBuffer.prototype.Append = function(str) {
	this.__strings__.push(str);
	return this;
};
// 格式化字符串
StringBuffer.prototype.AppendFormat = function(str) {
	for (var i = 1; i < arguments.length; i++) {
		var parent = "\\{" + (i - 1) + "\\}";
		var reg = new RegExp(parent, "g")
		str = str.replace(reg, arguments[i]);
	}
	this.__strings__.push(str);
	return this;
}
StringBuffer.prototype.ToString = function() {
	return this.__strings__.join('');
};
StringBuffer.prototype.clear = function() {
	this.__strings__ = [];
}
StringBuffer.prototype.size = function() {
	return this.__strings__.length;
}

/* 查询行政区 */
function getSelect(type) {
	$.ajax({
		type : "POST",
		dataType : "json",
		url : root + "/services/getDistrictSelect",
		data : type,
		success : function(result) {
			var obj = eval(result);
					
			if (obj.length != 0) {
				for (var i = 0; i < obj.length; i++) {
					//alert(obj[i].id);	
					$("#districtSelect").append(
							"<option value='" + obj[i].id + "'>" + obj[i].text
									+ "</option>");
				}
			} else {
				$("#subOfficeSelect").append("<option value=''>请选择</option>");
			}

		}
	});
}
/* 查询街道 */
function getStreet() {
	var obj = document.getElementById('subOfficeSelect');
	obj.options.length = 0;
	$("#subOfficeSelect").append("<option value=''>请选择</option>");
	var areaId = $('#districtSelect').val();
	//alert(areaId);
	var districtSelectName = $('#districtSelect').find("option:selected")
			.text();
	if (areaId.trim() == '') {
		$("#subOfficeSelect").append("<option value=''>请选择</option>");
	} else {
		var data = {
			'districtSelectName' : districtSelectName,
			'areaId' : areaId
		};
		$.ajax({
			type : "POST",
			dataType : "json",
			url : root + "/services/getStreet",
			data : data,
			success : function(result) {
				var obj = eval(result);
				if (obj.length != 0) {
					for (var i = 0; i < obj.length; i++) {
						$("#subOfficeSelect").append(
								"<option value='" + obj[i].id + "'>"
										+ obj[i].text + "</option>");
					}
				} else {

				}

			}
		});
	}
}
/* 查询社区 */
function getCommunity() {
	var obj = document.getElementById('communitySelect');
	obj.options.length = 0;
	$("#communitySelect").append("<option value=''>请选择</option>");
	var streetId = $('#subOfficeSelect').val();
	var subOfficeSelectName = $('#subOfficeSelect').find("option:selected")
			.text();
	if (streetId.trim() == '') {
		$("#subOfficeSelectName").append("<option value=''>请选择</option>");
	} else {
		var data = {
			'subOfficeSelectName' : subOfficeSelectName
		};
		$.ajax({
			type : "POST",
			dataType : "json",
			url : root + "/services/getCommunity",
			data : data,
			success : function(result) {
				var obj = eval(result);
				if (obj.length != 0) {
					for (var i = 0; i < obj.length; i++) {
						$("#communitySelect").append(
								"<option value='" + obj[i].id + "'>"
										+ obj[i].text + "</option>");
					}
				} else {

				}

			}
		});
	}
}
/* 条件查询 */
function getListForQr() {
	var districtSelectName = $('#districtSelect').find("option:selected")
			.text();
	var districtSelectId = $('#districtSelect').val();
	//alert(districtSelectId);
	var subOfficeSelectName = $('#subOfficeSelect').find("option:selected")
			.text();
	var subOfficeSelectId = $('#subOfficeSelect').val();
	//alert(subOfficeSelectId);
	var communitySelectName = $('#communitySelect').find("option:selected")
			.text();
	if (districtSelectName == "请选择") {
		districtSelectName = '';
	}
	if (subOfficeSelectName == "请选择") {
		subOfficeSelectName = '';
	}
	if (communitySelectName == "请选择") {
		communitySelectName = '';
	}
	$('#table').bootstrapTable('destroy');
	$("#table")
			.bootstrapTable(
					{
						url : root
								+ '/services/getListForQr?districtSelectName='
								+ districtSelectName + '&subOfficeSelectName='
								+ subOfficeSelectName + '&communitySelectName='
								+ communitySelectName+'&districtSelectId='
								+districtSelectId+'&subOfficeSelectId='
								+subOfficeSelectId,
						method : 'post',
						contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
						striped : true,// 隔行变色
						pagination : true,// 分页
						pageSize : 10,
						pageNumber : 1,
						height : 530,
						pageList : [ 10, 20, 30, 40 ],
						silent : true, // 是否显示对话框.该属性为True时,该对话框不再显示脚本错误
						sortStable : true, // 排序
						sortName : 'time',
						sortOrder : 'desc',
						sidePagination : "client",
						formatLoadingMessage : "请稍等,正在加载中。。。。。。。",
						columns : [ {
							checkbox : true,
							maintainSelected : true
						},
						/*
						 * { field:'id', title:'areaId', width:0,
						 * valign:'middle', align:'center' },
						 */
						{
							field : "area",
							title : '行政区',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							field : "street",
							title : '街道',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'community',
							title : '社区',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, ]

					});
}
/* 如果是公众二维码，隐藏调查员框 */
function hiddenOrblock() {
	var wecodeValue = "";
	var wecode = document.getElementsByName("optionsRadiosinline");
	for (var g = 0; g < wecode.length; g++) {
		if (wecode[g].checked == true) {
			wecodeValue = wecode[g].value;
			// alert(trafficValue);
		}
	}
	if (wecodeValue == 1) {
		$('#surveyor').css('display', 'none');
		$('#surveyorone').css('display', 'none');
		$('#surveyortwo').css('display', 'none');
		$('#cx').css('display', 'none');
	} else {
		$('#surveyor').css('display', 'block');
		$('#surveyorone').css('display', 'block');
		$('#surveyortwo').css('display', 'block');
		$('#cx').css('display', 'block');
	}
}