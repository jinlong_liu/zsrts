var marker = null;
	 //map.setZoomAndCenter(14);
		//加载地图
		var map = new AMap.Map('container', {
			resizeEnable : true,
			zoom : 13,

		});
		//AMap.moveCamera(CameraUpdateFactory.zoomTo(zoom))
		////初始化加载地图时，若center及level属性缺省，地图默认显示用户当前城市范围
		var map = new AMap.Map('mapContainer', {
			resizeEnable : true
		});
		//地图中添加地图操作ToolBar插件
		map.plugin([ 'AMap.ToolBar' ], function() {
			//设置地位标记为自定义标记
			var toolBar = new AMap.ToolBar();
			map.addControl(toolBar);
		});

		//加载地图，调用浏览器定位服务  定位当前位置并获取经纬度
		var map, geolocation;
		map = new AMap.Map('container', {
			resizeEnable : true
		});
		map.plugin('AMap.Geolocation', function() {
			geolocation = new AMap.Geolocation({
				enableHighAccuracy : true,//是否使用高精度定位，默认:true
				timeout : 10000, //超过10秒后停止定位，默认：无穷大
				buttonOffset : new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
				zoomToAccuracy : true, //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
				showCircle: false,      //去掉圆形区域
				buttonPosition : 'RB'
			});
			geolocation.getCityInfo(function(status, result) {
			    if (status == 'complete') {
			    	//alert(result.citycode);
			    }
			    });
			map.addControl(geolocation);
			geolocation.getCurrentPosition();
			AMap.event.addListener(geolocation, 'complete', onComplete);//返回定位信息
			AMap.event.addListener(geolocation, 'error', onError); //返回定位出错信息
		});
		
		//解析定位结果
		function onComplete(data) {
			var str = [ '定位成功' ];
			 map.setZoomAndCenter(14, [data.position.getLng(), data.position.getLat()]);
			str.push('经度：' + data.position.getLng());
			str.push('纬度：' + data.position.getLat());
			if (data.accuracy) {
				str.push('精度：' + data.accuracy + ' 米');
			}//如为IP精确定位结果则没有精度信息
			str.push('是否经过偏移：' + (data.isConverted ? '是' : '否'));
			//alert(str);
			document.getElementById("lnglat").value = data.position.getLng()
					+ "," + data.position.getLat();
			
			//alert(data.addressComponent.district);
			document.getElementById("district").value = data.addressComponent.district;
			
			
			//将定位的位置写入文本框
			AMap.plugin('AMap.Geocoder', function() {
				var geocoder = new AMap.Geocoder({

				});
				var lnglat = data.position.getLng() + ","
						+ data.position.getLat();
				var input = document.getElementById('returnInput');
				geocoder.getAddress(lnglat, function(status, result) {
					if (status == 'complete') {
						input.value = result.regeocode.formattedAddress
					
						//alert(result.regeocode.formattedAddress);
						message.innerHTML = ''
					} else {
						message.innerHTML = '无法获取地址'
					}
				});
			});
		}
		//解析定位错误信息
		function onError(data) {
			document.getElementById('returnInput').innerHTML = '定位失败';
		}

		//输入联想并提供经纬度
		AMap.plugin([ 'AMap.Autocomplete', 'AMap.PlaceSearch' ], function() {
			var windowsArr = [];
			marker = [];
			var autoOptions = {

				input : "returnInput"//使用联想输入的input的id
					
			};
			
			autocomplete = new AMap.Autocomplete(autoOptions);
			var placeSearch = new AMap.PlaceSearch({

				map : map
			})
			AMap.event.addListener(autocomplete, "select", function(m) {
				//TODO 针对选中的poi实现自己的功能
				//placeSearch.search(e.poi.name)
				//alert(m.poi.name);
				AMap
				.plugin(
						'AMap.Geocoder',
						function() {
							var geocoder = new AMap.Geocoder(
									{

									});
							marker = new AMap.Marker({
								map : map,
								bubble : true
							})
							var input = document
									.getElementById('returnInput');
							lo();
							function lo(e) {
								var address = m.poi.name;
								//alert(address);
								geocoder
										.getLocation(
												address,
												function(
														status,
														result) {
													//alert(address);
													if (status == 'complete'
															&& result.geocodes.length) {
														marker
																.setPosition(result.geocodes[0].location);
														map
																.setCenter(marker
																		.getPosition())
														document
																.getElementById("lnglat").value = result.geocodes[0].location;
														document
																.getElementById('message').innerHTML = ''
													}
												})
							}
						});
			});
		});
		
		//绑定blur事件支持用户手动输入
		/* $('#returnInput')
				.bind(
						"click keyup change mouseover  mouseout",
						function() {
							
							map.remove(marker);
							AMap
									.plugin(
											'AMap.Geocoder',
											function() {
												var geocoder = new AMap.Geocoder(
														{

														});
												marker = new AMap.Marker({
													map : map,
													bubble : true
												})
												var input = document
														.getElementById('returnInput');
												lo();
												function lo(e) {
													var address = input.value;
													geocoder
															.getLocation(
																	address,
																	function(
																			status,
																			result) {
																		//alert(address);
																		if (status == 'complete'
																				&& result.geocodes.length) {
																			marker
																					.setPosition(result.geocodes[0].location);
																			map
																					.setCenter(marker
																							.getPosition())
																			document
																					.getElementById("lnglat").value = result.geocodes[0].location;
																			document
																					.getElementById('message').innerHTML = ''
																		}
																	})
												}
											});
						}); */
		
		//添加默认城市  点击事件完成地址与经纬度带入
		AMap.plugin('AMap.Geocoder', function() {
			var geocoder = new AMap.Geocoder({

			});
			marker = new AMap.Marker({
				map : map,
				bubble : true
			})
			//将获取的地址写到文本框
			var input = document.getElementById('returnInput');
			var message = document.getElementById('message');
			map.on('click', function(e) {
				marker.setPosition(e.lnglat);
				geocoder.getAddress(e.lnglat, function(status, result) {
					if (status == 'complete') {
						input.value = result.regeocode.formattedAddress;
					/* 	var para=document.createElement("p");
						para.appendChild("1234");
						$('#topforno').appendChild(para); */
						message.innerHTML = ''
					} else {
						message.innerHTML = '无法获取地址'
					}
				})
			})
			input.onchange = function(e) {
				var address = input.value;
				geocoder.getLocation(address, function(status, result) {
					if (status == 'complete' && result.geocodes.length) {
						marker.setPosition(result.geocodes[0].location);
						map.setCenter(marker.getPosition())
						message.innerHTML = ''
					} else {
						message.innerHTML = '无法获取位置'
					}
				})
			}
			//为地图注册click事件获取鼠标点击出的经纬度坐标
			var clickEventListener = map.on('click', function(e) {
				document.getElementById("lnglat").value = e.lnglat.getLng()
						+ ',' + e.lnglat.getLat()
			});
		});