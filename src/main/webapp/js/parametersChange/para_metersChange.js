var ids="";
function search() {
	$('#table').bootstrapTable('destroy');
	/* radioVal = encodeURI(encodeURI(radioVal)); */
	$("#table")
			.bootstrapTable(
					{
						url : root + '/services/getAreaList',
						method : 'post',
						contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
						striped : true,
						pagination : true,
						pageSize : 10,
						pageNumber : 1,
						height : 550,
						pageList : [ 10, 20, 30 ],
						silent : true,
						sortStable : true,
						sortName : 'time',
						sortOrder : 'desc',
						sidePagination : "client",
						formatLoadingMessage : "请稍等,正在加载中。。。。。。。",
						columns : [
						
						  { checkbox: true,
				            maintainSelected: false 
				            
				           },
				           /*{
								field : 'areaId',
								title : 'id',
								width : 50,
								valign : 'middle',
								align : 'center'
	                             
							},*/
						{
							field : 'number',
							title : '序号',
							width : 120,
							valign : 'middle',
							align : 'center'
						},
						 {
							field : "districtName",
							title : '行政区',
							width : 200,
							valign : 'middle',
							align : 'center'
						},  {
							field : 'subofficeName',
							title : '街道',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							 
							field : 'communityName',
							title : '社区',
							width : 200,
							valign : 'middle',
							align : 'center'
						},{
							 
							field : 'totalCount',
							title : '指标数',
							width : 100,
							valign : 'middle',
							align : 'center'
						},{
							 
							field : 'everyCount',
							title : '提交数',
							width : 100,
							valign : 'middle',
							align : 'center'
						}
						/*
						 * { field:'operator', title:'操作者', width:200,
						 * valign:'middle', align:'center' },
						 */
						/*
						 * { field: 'operate', title: '操作', align: 'center',
						 * width:200, events: operateEvents, formatter:
						 * operateFormatter }
						 */

						/*{
							field : 'operate',
							title : '操作',
							align : 'center',
							formatter : operateFormatter,
							width : 100
						}*/

						/*
						 * { field:'revise', title:'修改', width:200,
						 * valign:'middle', align:'center',
						 * formatter:function(value,row,index){ return "<a
						 * href='reviseRole.jsp?lineId="+row.id+"'>"+row.revise+"</a>"; } }, {
						 * field:'delete', title:'删除', width:200,
						 * valign:'middle', align:'center',
						 * formatter:function(value,row,index){ return "<a
						 * href='javascript:void(0);'
						 * onclick='deleteRole("+row.id+")' >"+row.dele+"</a>"; } }
						 */
						]

					});
}
/** 每行后面显示操作的列 */
/*function operateFormatter(value, row, index) {

	return '<a href=\"javascript:void(0);\" onclick=\"deleteArea(' + row.id
			+ ',' + row.type + ')\">' + '<font color="#06b370">删除</font>'
			+ '</a>&nbsp<a href=\"javascript:void(0);\" onclick=\"editArea('
			+ row.id + ')\">' + '<font color="#06b370">修改</font></a>';
}
*/
$().ready(function() {
	 var radioVal=$('input:radio[name="stop_info_radio"]:checked').val(); 
	 loadDistrictNamedistrictName();
	 search();
	  $(".stop_info_radio").change(function(){
	 radioVal=$('input:radio[name="stop_info_radio"]:checked').val();
	  search(radioVal); });
	  
	  $('#myModal').on('hidden.bs.modal', function () {
		  var $browsers = $("input[name=chSelect]"); 
		  $browsers.prop("checked",false);
		  $("#testdiv").empty();
		  });
	 
});
/*加载提示框*/
$(function() {
    $(document).ajaxStart(function () { $(".loaderTip,.bgBox").css("display", "block"); });  
    $(document).ajaxStop(function () { $(".loaderTip,.bgBox").css("display", "none"); });

    $(".pager a").click(function() {
        var Is = $(this).attr("disabled");
        if (Is != true) {
            $(".loaderTip,.bgBox").css("display", "block");
        }
    });
});

	
//下拉框
function loadDistrictNamedistrictName(){
	//获取行政区下拉框
	$.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/queryAreaStatistics",
		success : function(result) {
			var obj = eval(result);
				for ( var i = 0; i < obj.length; i++) {
					$("#districtName").append(
							"<option value='" + obj[i].districtNumber + "'>"
							+ obj[i].districtName + "</option>");
				}
		/*	if(obj[0].type == 2){ 
				select =  "<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>	"+				  
				"<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea(2)' > "+
				"</select> "+
				"<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>	 "+
				"<select id='subofficeName' name='type'  class='js-example-disabled-results' style='width:200px' > "+
				"</select> "+
				" <button type='button' style='border:none;' onclick='searchByareaNumber()'> "+
				"    查    询"+
				"</button> ";
				
				document.getElementById("are").innerHTML = document
				.getElementById("are").innerHTML+select;
				
				//document.getElementById("view5").style.cssText ="margin-left:50px";
				
				for ( var j = 0; j < obj.length; j++) {
					
					if(j==0){
						$("#districtName").append(
								"<option value='" + obj[j].districtNumber + "'>"
								+ obj[j].districtName + "</option>");
						$("#subofficeName").append(
								"<option value='" + obj[j].subofficeNumber + "'>"
								+ obj[j].subofficeName + "</option>");
					}else if(obj[j].districtNumber== obj[0].districtNumber){
						$("#subofficeName").append(
								"<option value='" + obj[j].subofficeNumber + "'>"
								+ obj[j].subofficeName + "</option>");
					}
					
					
				}
			 }*/
		},
		error : function(result) {
			sweetAlert("", "系统错误", "warning");
		}
	});
}
/**
 * 点击行政区加载街道
 */
function dArea() {
		var districtNumber = $('#districtName').val();
		var districtName = $('#districtName').find("option:selected").text();
		/*if (districtNumber == null) {
			$("#subOfficeSel").append("<option value=''>请选择</option>");
		}*/
		if(districtNumber == ''){//行政区为空，清空社区下拉并添加一个空
			$("#subofficeName").empty();
			$("#subofficeName").append(
					"<option value='" + " " + "'>"
					+ "请选择" + "</option>");
		}
		if (districtNumber != '') {//行政区不为空，清空社区下拉并添加
			$.ajax({
				url : root + "/services/queryAreaByUserId",
				type : "post",
				data : {
					type : 2,
					districtName : districtName
				},
				success : function(result) {
					
					$("#subofficeName").empty();
					
					var obj = eval(result);
					
					$("#subofficeName").append(
							"<option value='" + " " + "'>"
							+ "请选择" + "</option>");
					
					for ( var i = 0; i < obj.length; i++) {
						if(obj[i].districtName == districtName){
							
							$("#subofficeName").append(
									"<option value='" + obj[i].subofficeNumber + "'>"
									+ obj[i].subofficeName + "</option>");
						}
						
					}
					/*var obj = eval("(" + result + ")");
					if (obj.length == 0) {
						$("#subOfficeSel").append(
								"<option value='" + ' ' + "'>" + " "
										+ "</option>");
					} else {
						$("#subOfficeSel").append("<option value=''>请选择</option>");
						for ( var i = 0; i < obj.length; i++) {
							$("#subOfficeSel").append(
									"<option value='" + obj[i].areaId + "'>"
											+ obj[i].text + "</option>");
						}
					}*/
				},
				error : function(result) {
					sweetAlert("", "系统错误", "warning");
				}
			});

		}
	
}

//条件查询
function queryAreaa() {
	var districtName= $('#districtName').find("option:selected").text();
	var subofficeName = $('#subofficeName').find("option:selected").text();
	var districtNumber= $('#districtName').find("option:selected").val();
	var subofficeNumber = $('#subofficeName').find("option:selected").val();
	var type = "";
	 $('#table').bootstrapTable('destroy');
     if(districtName=="请选择" && subofficeName=="请选择"){
    	 search(); 
    	 return;
     }
	 $("#table").bootstrapTable({
			url : root+'/services/queryGetparameter?districtNumber='+districtNumber+'&subofficeNumber='+subofficeNumber+'&type='+type,  
			method:'post',
			contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
			striped : true,
			pagination : true,
			pageSize : 10,
			pageNumber : 1,
			height : 550,
			pageList : [ 10, 20, 30 ],
			silent : true,
			sortStable : true,
			sortName : 'time',
			sortOrder : 'desc',
			sidePagination : "client",

			/*responseHandler:function(res){
				return{
					number:res.number,
					stoptype:res.stoptype
				};
			},*/
			/*queryParams:{
				 districtNumber:$('#districtName').val(),
				 subofficeNumber:$('#subofficeName').val()
			},
			queryParams:function(params){
				return {
					 districtNumber:$('#districtName').val(),
					 subofficeNumber:$('#subofficeName').val()

	           };
			},*/
			formatLoadingMessage:"请稍等,正在加载中。。。。。。。",
			columns:[
					 { checkbox: true,
				            maintainSelected: true 
				            
				           },
				           /*{
								field : 'areaId',
								title : 'id',
								width : 50,
								valign : 'middle',
								align : 'center'
	                             
							},*/
						{
							field : 'number',
							title : '序号',
							width : 120,
							valign : 'middle',
							align : 'center'
						},
						 {
							field : "districtName",
							title : '行政区',
							width : 200,
							valign : 'middle',
							align : 'center'
						},  {
							field : 'subofficeName',
							title : '街道',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							 
							field : 'communityName',
							title : '社区',
							width : 200,
							valign : 'middle',
							align : 'center'
						},{
							 
							field : 'totalCount',
							title : '指标数',
							width : 100,
							valign : 'middle',
							align : 'center'
						},{
							 
							field : 'everyCount',
							title : '提交数',
							width : 100,
							valign : 'middle',
							align : 'center'
						}
				]
				
		});
}	
	


/*利用数组生成StringBuffer*/

/*function StringBuffer() { 
	  this.__strings__ = []; 
	}; 
	StringBuffer.prototype.Append = function (str) { 
	  this.__strings__.push(str); 
	  return this; 
	}; 
	//格式化字符串 
	StringBuffer.prototype.AppendFormat = function (str) { 
	  for (var i = 1; i < arguments.length; i++) { 
	    var parent = "\\{" + (i - 1) + "\\}"; 
	    var reg = new RegExp(parent, "g") ;
	    str = str.replace(reg, arguments[i]); 
	  } 	  
	  this.__strings__.push(str); 
	  return this; 
	};
	StringBuffer.prototype.ToString = function () { 
	  return this.__strings__.join(''); 
	}; 
	StringBuffer.prototype.clear = function () { 
	  this.__strings__ = []; 
	}; 
	StringBuffer.prototype.size = function () { 
	  return this.__strings__.length; 
	};*/
	
//修改参数
function uodateTotal(){
    var id;
    
    $.map($("#table").bootstrapTable('getSelections'), function(row) {
	total = row.totalCount;
	everyday = row.everyCount;
	id = row.areaId;
	
	});
   
    if(id==undefined){
    	sweetAlert("", "请勾选要修改的选项", "warning");
    }else{
    	$('#myModal').modal('show');
    }

}
	

// 修改参数
$(function() {
	
});

function selectOnchang() {
	var str = $('input[name="chSelect"]:checked ').val();
	var total='';
	var everyday='';
	

	$.map($("#table").bootstrapTable('getSelections'), function(row) {
		if(row.totalCount!=null){
			total = row.totalCount;
		}
		if(row.everyCount!=null){
			everyday = row.everyCount;
		}
	});
		
		
	
	/* var testdiv = $("type").select2("val"); */
	// 清空div
	$("#testdiv").empty();
	var select = "";
	if (str == 1) {
		select = "  </br><label id='laberLeftTwo'>指标数 &nbsp;   ：</label><input id='everTotal' value='"+total+"'  " +
				"type='text' class='form-control select2' style='width:200px'/></label></br>";		 
	} else if (str == 2) {
		
		select = "  </br><label id='laberLeftTwo'>提交数 &nbsp;   ：</label><input id='everyDayTotal' value='"+everyday+"' " +
				"type='text' class='form-control select2' style='width:200px'/></label>";
	} else if(str == 3){
		select = "  </br><label id='laberLeftTwo'>指标数    ：</label><input id='everTotal1' value='"+total+"' type='text' " +
				"class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>提交数  ：</label><input " +
				"id='everyDayTotal2' value='"+everyday+"' type='text' class='form-control select2' style='width:200px' /></br>";
	}
	document.getElementById("testdiv").innerHTML = document
	.getElementById("testdiv").innerHTML
	+ select;
}
		 
		

/**
 * 保存 、
 */
function save() {
	var type = $('input[name="chSelect"]:checked ').val();
	var checkSave = $('#checkSave').val();
	
	var id = '';
	var totalCount ='';
	var everyCount ='';
	$.map($("#table").bootstrapTable('getSelections'), function(row) {
		//return row.areaId,
		// alert(row.areaId);
		totalCount=row.totalCount;
		everyCount=row.everyCount;
		id=row.areaId;
		ids=ids+id+',';
		
	});
	
	//alert(type);
	//alert(checkSave);
	var areaId="";
	var url = "";
	var parm = {
			
	};
	if (checkSave == 0) {
		if (type == 1) {
			url = "/services/updateTotal";
			var everTotal = $('#everTotal').val();
		/*	var areaId=id;*/
			/*alert(everyCount);
			
			alert(everTotal);*/
			var re = /^[1-9]+[0-9]*]*$/; 
			if(Number(everTotal)<=0){
				sweetAlert("", "指标数必须大于0", "warning");
				return;
			}
			if(Number(everTotal)>1000){
				sweetAlert("", "指标数量不能大于1000", "warning");
			
				return;
			}
			if(Number(everTotal)<Number(everyCount) || Number(everTotal) ==Number(everyCount)){
				sweetAlert("", "提交数量要小于指标数量，请重新输入", "warning");
				
				return;
			}
			if(isNaN(everTotal)){
				sweetAlert("", "指标数量请输入数字！", "warning");
		
				return;
			
			}
			
			parm = {
					
					everTotal : everTotal,
					ids: ids
			};
		} else if (type == 2) {
			url = "/services/updateEveryday";
			var everyDayTotal = $('#everyDayTotal').val();
			/*var areaId=id;*/
			/*alert(everyDayTotal);
			alert(totalCount);*/
			var re = /^[1-9]+[0-9]*]*$/;
			if(Number(everyDayTotal)<=0){
				sweetAlert("", "提交数量必须大于0", "warning");
		
				return;
			}
			if(Number(everyDayTotal)>1000){
				sweetAlert("", "提交数量不能大于1000", "warning");
			
				return;
			}
			if(Number(everyDayTotal)>Number(totalCount) || Number(everyDayTotal)==Number(totalCount)){
				sweetAlert("", "提交数量要小于指标数量，请重新输入", "warning");
		
				return;
			}
			if(isNaN(everyDayTotal)){
				sweetAlert("", "提交数量请输入数字", "warning");
				
				return;
			}
			/* alert(subofficeName+"--"+subofficeNumber); */
			parm = {
					everyDayTotal : everyDayTotal,
					ids: ids
					
					
			};
		} else if (type == 3) {
			url = "/services/updateParameter";
			var everTotal = $('#everTotal1').val();
			var everyDayTotal = $('#everyDayTotal2').val();
			/*var areaId=id;*/
			/*alert(everyDayTotal);
			alert(everTotal);*/
			if(Number(everyDayTotal)<=0){
				sweetAlert("", "提交数量必须大于0", "warning");
			
				return;
			}
			if(Number(everyDayTotal)>1000){
				sweetAlert("", "提交数不能大于1000", "warning");
			
				return;
			}
			if(Number(everTotal)<=0){
				sweetAlert("", "指标数必须大于0", "warning");
				
				return;
			}
			if(Number(everTotal)>1000){
				sweetAlert("", "指标数不能大于1000", "warning");
				
				return;
			}
			if(Number(everyDayTotal)>Number(everTotal) ||Number(everyDayTotal) ==Number(everTotal)){
				sweetAlert("", "提交数量要小于指标数量，请重新输入", "warning");
				
				return;
			}
			if(isNaN(everyDayTotal)){
				sweetAlert("", "提交数量请输入数字！", "warning");
				
				return;
			}
			if(isNaN(everTotal)){
				sweetAlert("", "指标数量请输入数字！", "warning");
				
				return;
			}
			
			parm = {
					everTotal : everTotal,
					everyDayTotal : everyDayTotal,
					ids:ids
					
			};
		}
	} 
	
	submitSave(url, parm);
}
function submitSave(url, parm) {
	$('#myModal').modal('hide');
	$.ajax({
		url : root + url,
		dataType : "json",
		type : "post",
		data : parm,
		success : function(result) {
		/*$(document).ready(function() {
		    $("#dataLoad").hide(); //页面加载完毕后即将DIV隐藏
		    $("#showLoadingDiv").click(function(){$("#dataLoad").show();}); //为指定按钮添加数据加载动态显示：即将DIV显示出来
		   });*/
		
		if (result.flag == 1) {
			// 清空数据
			
			
			$('#myModal').modal('hide');
			$("#testdiv").empty();
			$('#checkSave').val(0);
			location.reload();
			
			sweetAlert("", "修改成功", "success");
			//加载提示
			/*$(document).ready(function() {
			    $("#dataLoad").hide(); //页面加载完毕后即将DIV隐藏
			    $("#showLoadingDiv").click(function(){$("#dataLoad").show();}); //为指定按钮添加数据加载动态显示：即将DIV显示出来
			   });*/
		} else if (result.flag == 0) {
			sweetAlert("", +result.message+"", "warning");
			
		} else if (result.flag == 3) {
			sweetAlert("", +result.message+"", "warning");
		}
	},
	error : function(result) {
		sweetAlert("", "系统错误", "warning");
	}
	});
}

/** 区域编辑 */
/*function editArea(id) {
	var url = root + "/services/getSelectData";
	
	
	$.ajax({
		url : url,
		type : "post",
		data : {
		id : id
	},
	success : function(result) {
		// Json转化为对象
		var data = JSON.parse(result);
		$('#myModal').modal('show');
		$('#checkSave').val(1);
		if (data.type == 1) {
			selectOnchangBack(data.type);
			$("#id").val(data.id);
			$("#oldName").val(data.districtName);
			$("input[name='chSelect']").eq(0).attr("checked",
			"checked");
			$("#districtName").val(data.districtName);
			$("#districtNumber").val(data.districtId);
		} else if (data.type == 2) {
			selectOnchangBack(data.type);
			$("#id").val(data.id);
			$("#oldName").val(data.subofficeName);
			$("input[name='chSelect']").eq(1).attr("checked",
			"checked");
			var districtName = $('.distant')
			.find("option:selected").text();
			var a=$("#id").val();
			alert(a);
			setTimeout("$('#districtNameSel').val("+data.districtId+")",50);
			$("#subofficeName").val(data.subofficeName);
			$("#ss").val(data.subofficeId);
		} else if (data.type == 3) {
			$("#id").val(data.id);
			$("#oldName").val(data.communityName);
			$("input[name='chSelect']").eq(2).attr("checked",
			"checked");
			selectOnchangBack(data.type);
			setTimeout("$('#districtNameSel').val("+data.districtId+");diArea(2);",50);
			setTimeout("$('#subOfficeSel').val("+data.subofficeId+")",100);
			$("#districtNameSel").val(data.districtId).trigger(
			"change");
			$("#subOfficeSel").val(data.subofficeId).trigger(
			"change");
			$("#communityName").val(data.communityName);
			$("#communityNumber").val(data.communityNumber);
		}
	},
	error : function(result) {
		sweetAlert("", "修改失败！", "error");
	}
	});
	
}*/

// 修改回显数据
/*function selectOnchangBack(type) {
	alert(type);
	var str = type;
	 var testdiv = $("type").select2("val"); 
	// 清空div
	$("#testdiv").empty();
	var select = "";
	if (str == 1) {
		select = "  </br><label id='laberLeftTwo'>行政区 &nbsp;   ：</label><input id='districtName' type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>行政区编码  ：</label><input id='districtNumber' type='text' class='form-control select2' style='width:200px' /></br>";
	} else if (str == 2) {
		select = "</br><label id='laberLeftTwo'>行政区  &nbsp;  ：</label><select id='districtNameSel'   class='distant form-control select2' name='districtNameSel'  style='width:200px' ><option value='0'>请选择</option>"
			+ "</select></br>";
		select = select
		+ " <label id='laberLeftTwo'>街道   &nbsp;&nbsp; ：</label><input id='subofficeName'  type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>街道编码：</label><input id='ss'  type='text' class='form-control select2' style='width:200px' /></br>";
		selectDis(1);
	} else if (str == 3) {
		select = "</br><label id='laberLeftTwo'>行政区&nbsp;    ：</label><select id='districtNameSel'  class='distant form-control select2'   name='districtNameSel'  style='width:200px' onclick='diArea(2)'><option value=''>请选择</option>"
			+ "</select></br>";
		select = select
		+ "<label id='laberLeftTwo'>街道   &nbsp;&nbsp; ：</label><select id='subOfficeSel'   class='form-control select2' name='subOfficeSel'  style='width:200px' onclick='diSub()'><option value=''>请选择</option>"
		+ "</select></br>";
		select = select
		+ " <label id='laberLeftTwo'>小区  &nbsp;&nbsp; ：</label><input id='communityName' type='text' class='form-control select2' style='width:200px' /></br><label id='laberLeftTwo'>小区编码 ：</label><input id='communityNumber' type='text' class='form-control select2' style='width:200px' /></br>";
		selectDis(1);
	}
	document.getElementById("testdiv").innerHTML = document
	.getElementById("testdiv").innerHTML
	+ select;
	
}*/

//导入表格
function ajaxFileUpload()  
{  
	/*document.getElementById("formid").submit();*/
	var type=$('input:radio:checked').val(); 
//	alert(type);
	if(type == undefined ){
		sweetAlert("", "请选择导入的类型", "warning");
		return;
	}
	var excelPath = $("#file").val();
	if(excelPath == null || excelPath == ''){
		sweetAlert("", "请选择要上传的Excel文件", "warning");
		
		return;
	}else{
		var fileExtend = excelPath.substring(excelPath.lastIndexOf('.')).toLowerCase();
		if(fileExtend != '.xls'){
			sweetAlert("", "文件格式需为'.xls'格式", "warning");
			
			return;
		}
	}
	$.ajaxFileUpload({ 
		 
		url:root+'/services/fileUploa?type='+type,             //需要链接到服务器地址  
		secureuri:false,  
		fileElementId:'file',  
		dataType: 'json',  
		type : "post",//服务器返回的格式，可以是json  
		success: function (data,status)             //相当于java中try语句块的用法  
		{     
	    
	    $(".loaderTip,.bgBox").css("display", "block");
		var result = data;
		sweetAlert("", result.message, "warning");
		search();
		    //data是从服务器返回来的值     
		/*location.reload();*/
		},  
		error: function (data, status, e)             //相当于java中catch语句块的用法  
		{  
			sweetAlert("", "系统错误", "warning");
		}  
	  
	});  
}  






