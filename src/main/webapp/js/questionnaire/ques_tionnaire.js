var flag = false;
function search() {
	document.getElementById("tableTwo").style.display = "none";
$('#table').bootstrapTable('destroy');
/* radioVal = encodeURI(encodeURI(radioVal)); */
var userName = $("#userName").val();
var authDegree = $("#authDegree").val();
var auth = authDegree.split(",");
if (userName == 1) {
	flag = true;
	/*
 * for(var i=0;i<auth.length;i++){ if(auth[i] == "3"){ flag=true;
 * break; } }
 */
}
if (flag) {
	$("#table").bootstrapTable(
			{
				method : 'post',
				contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
				striped : true,
				cache : true,
				pagination : true,
				sidePagination : "server",
				pageSize : 10,
				pageNumber : 1,
				height : 550,
				align : 'center',
				pageList : [ 10, 20, 30 ],
				queryParams : function(params) {
					data = {
						offset : params.offset, // 第几页
						limit : params.limit,
					}
					return data;
				},
				url : root
						+ '/services/getListStatisticsParticular',
				escape : true,
				silent : true,
				sortStable : true,
				sortOrder : true,
				dataType : 'json',
				columns : [
						{
							// field: 'Number',//可不加
							title : '序号',// 标题 可不加
							valign : 'middle',
							formatter : function(value, row, index) {
								return index + 1;
							}
						},
						{
							field : "familyId",
							title : '家庭编号',
							width : 110,
							valign : 'middle',
							align : 'center',
							formatter : function(value, row, index) {
								return '<a href="javaScript:getDetail('
										+ row.id
										+ ');">'
										+ row.familyId + '</a>';
							}
						}, {
							field : 'districtName',
							title : '行政区',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'subofficeName',
							title : '街道',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'communityName',
							title : '社区',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'familyMembers',
							title : '家庭成员数',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'travelTotal',
							title : '出行次数',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'status',
							title : '审核状态',
							width : 110,
							valign : 'middle',
							align : 'center'

						}, {
							field : 'deviation',
							title : '位置偏差',
							width : 110,
							valign : 'middle',
							align : 'center'

						}, {
							field : 'submitDate',
							title : '时间',
							width : 110,
							align : 'center'

								}

						]

					});
} else {
	$("#table").bootstrapTable(
			{
				method : 'post',
				contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
				striped : true,
				cache : true,
				pagination : true,
				sidePagination : "server",
				pageSize : 10,
				pageNumber : 1,
				height : 550,
				align : 'center',
				pageList : [ 10, 20, 30 ],
				queryParams : function(params) {
					data = {
						offset : params.offset, // 第几页
						limit : params.limit,
					}
					return data;
				},
				url : root
						+ '/services/getListStatisticsParticular',
				escape : true,
				silent : true,
				sortStable : true,
				sortOrder : true,
				dataType : 'json',
				columns : [
						{
							// field: 'Number',//可不加
							title : '序号',// 标题 可不加
							valign : 'middle',
							formatter : function(value, row, index) {
								return index + 1;
							}
						},
						/*
						 * {
						 * 
						 * field:'number', title:'序号', width:110,
						 * valign:'middle', align:'center' },
						 */
						{
							field : "familyId",
							title : '家庭编号',
							width : 123,
							valign : 'middle',
							align : 'center',
							formatter : function(value, row, index) {
								return '<a href="javaScript:getDetail('
										+ row.id
										+ ');">'
										+ row.familyId + '</a>';
							}

						}, {
							field : 'districtName',
							title : '行政区',
							width : 123,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'subofficeName',
							title : '街道',
							width : 123,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'communityName',
							title : '社区',
							width : 123,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'familyMembers',
							title : '家庭成员数',
							width : 123,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'travelTotal',
							title : '出行次数',
							width : 123,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'status',
							title : '审核状态',
							width : 123,
							valign : 'middle',
							align : 'center'

						},
						/*
						 * { field:'deviation', title:'位置偏差',
						 * width:110, valign:'middle',
						 * align:'center'
						 *  },
						 */
						{
							field : 'submitDate',
							title : '时间',
							width : 123,
							align : 'center'

									}

							]

						});
	}
}

/* 加载提示框 */
$(function() {
	$(document).ajaxStart(function() {
		$(".loaderTip,.bgBox").css("display", "block");
});
$(document).ajaxStop(function() {
	$(".loaderTip,.bgBox").css("display", "none");
});
$(".pager a").click(function() {
var Is = $(this).attr("disabled");
if (Is != true) {
	$(".loaderTip,.bgBox").css("display", "block");
		}
	});
});
// 预加载
$().ready(function() {
	loadDistrictNamedistrictName();
	search();
	queryTotalQuestionnaire();
});

function loadDis() {
	$.ajax({
		type : "POST",
		dataType : "json",
		url : root + "/services/selectDis",
		success : function(result) {
			var obj = eval(result);
			$("#district").append(
				"<option value='" + " " + "'>" + "请选择" + "</option>");
		for (var i = 0; i < obj.length; i++) {
			$("#district").append(
					"<option value='" + obj[i].distric + "'>"
					+ obj[i].distric + "</option>");
			}
		}
	});
}
// 加载页面按钮
function loadDistrictNamedistrictName() {
	// 查询对应的角色的权限
		$.ajax({
			type : "POST",
	dataType : "json",
	async : false,
	url : root + "/services/queryAreaStatistics",
	success : function(result) {
		var obj = eval(result);
		if (obj[0].type == 1) {
			for (var i = 0; i < obj.length; i++) {
				$("#districtName").append(
						"<option value='" + obj[i].districtNumber
								+ "'>" + obj[i].districtName
								+ "</option>");
			}
		}
		if (obj[0].type == 2) {
			$("#districtName").empty();
			for (var j = 0; j < obj.length; j++) {
				if (j == 0) {
					$("#districtName").append(
							"<option value='"
									+ obj[j].districtNumber + "'>"
									+ obj[j].districtName
									+ "</option>");
					$("#subofficeName").append(
							"<option value='"
									+ obj[j].subofficeNumber + "'>"
									+ obj[j].subofficeName
									+ "</option>");
				} else if (obj[j].districtNumber == obj[0].districtNumber) {
					$("#subofficeName").append(
							"<option value='"
									+ obj[j].subofficeNumber + "'>"
									+ obj[j].subofficeName
									+ "</option>");
				}

			}
		}
		if (obj[0].type == 3) {
			$("#districtName").empty();
			$("#subofficeName").empty();
			for (var j = 0; j < obj.length; j++) {
				if (j == 0) {
					$("#districtName").append(
							"<option value='"
									+ obj[j].districtNumber + "'>"
									+ obj[j].districtName
									+ "</option>");
					$("#subofficeName").append(
							"<option value='"
									+ obj[j].subofficeNumber + "'>"
									+ obj[j].subofficeName
									+ "</option>");
					$("#communityName").append(
							"<option value='"
									+ obj[j].communityNumber + "'>"
									+ obj[j].communityName
									+ "</option>");
				} else if (obj[j].districtNumber == obj[0].districtNumber
						&& obj[j].subofficeNumber == obj[0].subofficeNumber) {
					$("#communityName").append(
							"<option value='"
									+ obj[j].communityNumber + "'>"
									+ obj[j].communityName
									+ "</option>");
				}
			}
		}
	},
	error : function(result) {
		alert("系统错误");
				}
			});
}
/**
 * 点击行政区加载街道
 */
function diArea() {
	var districtNumber = $('#districtName').val();
var districtName = $('#districtName').find("option:selected").text();
if (districtNumber == '' && districtName == '请选择') {// 行政区为空，清空社区下拉并添加一个空
$("#communityName").empty();
$("#communityName").append(
	"<option value='" + " " + "'>" + "请选择" + "</option>");

$("#subofficeName").empty();
$("#subofficeName").append(
	"<option value='" + " " + "'>" + "请选择" + "</option>");
}
if (districtNumber != '') {// 行政区不为空，清空社区下拉并添加
$("#communityName").empty();
$("#communityName").append(
	"<option value='" + " " + "'>" + "请选择" + "</option>");
$.ajax({
	url : root + "/services/queryAreaByUserId",
type : "post",
data : {
	type : 2
/* districtName : districtName */
},
success : function(result) {

	$("#communityName").empty();
	$("#subofficeName").empty();

	var obj = eval(result);
	$("#communityName").append(
			"<option value='" + " " + "'>" + "请选择" + "</option>");
	$("#subofficeName").append(
			"<option value='" + " " + "'>" + "请选择" + "</option>");

	for (var i = 0; i < obj.length; i++) {
		if (obj[i].districtName == districtName) {

			$("#subofficeName").append(
					"<option value='" + obj[i].subofficeNumber
							+ "'>" + obj[i].subofficeName
							+ "</option>");
		}

	}
},
error : function(result) {
	alert("系统错误");
			}
		});

	}
}
// 点击街道加载社区
function dicommunityName() {
	var subofficeNumber = $('#subofficeName').val();
var subofficeName = $('#subofficeName').find("option:selected").text();
/*
 * if (districtNumber == null) { $("#subOfficeSel").append("<option
 * value=''>请选择</option>"); }
 */
if (subofficeNumber == '') {// 行政区为空，清空社区下拉并添加一个空
$("#communityName").empty();
$("#communityName").append(
	"<option value='" + " " + "'>" + "请选择" + "</option>");
}
if (subofficeNumber != '') {// 行政区不为空，清空社区下拉并添加
$.ajax({
	url : root + "/services/queryGetCommunityName",
type : "post",
data : {
	subofficeNumber : subofficeNumber
/* districtName : districtName */
},
success : function(result) {

	$("#communityName").empty();

	var obj = eval(result);

	$("#communityName").append(
			"<option value='" + " " + "'>" + "请选择" + "</option>");

	for (var i = 0; i < obj.length; i++) {
		/* if(obj[i].districtName == districtName){ */

		$("#communityName").append(
				"<option value='" + obj[i].communityNumber + "'>"
						+ obj[i].communityName + "</option>");
		/* } */

	}
},
error : function(result) {
	alert("系统错误");
			}
		});

	}

}
/* 页面跳转 */

// 条件查询
function queryAreaa() {
	var communityNumber = $('#communityName').find("option:selected").val()
		.trim();
var districtNumber = $('#districtName').find("option:selected").val()
		.trim();
var subofficeNumber = $('#subofficeName').find("option:selected").val()
		.trim();
var familyId = $('#familyId').val().trim();
var type = "";
$('#table').bootstrapTable('destroy');
if (isNaN(familyId)) {
	alert("家庭编码只能输入数字");
	return;
}
if (districtNumber == '' && familyId == '') {
search();
$("#information").empty();
	queryTotalQuestionnaire();
	return;
}
		$.ajax({
			url : root + '/services/selectConditions?districtNumber='
			+ districtNumber + '&subofficeNumber='
			+ subofficeNumber + '&type=' + type + '&familyId='
			+ familyId + '&communityNumber=' + communityNumber,
	type : "POST",
	dataType : "json",
	success : function(result) {
		$("#information").empty();
		var obj = eval(result);
		var ul = document.createElement("ul");
		var litotalQuestionnaire = document.createElement("li");
		var liResidentsTotal = document.createElement("li");
		var liinvestigatorsTotal = document.createElement("li");
		var liReviewNot = document.createElement("li");
		var liReviewYes = document.createElement("li");
		var liReviewPass = document.createElement("li");
		var licompleted = document.createElement("li");
		var linoCompleted = document.createElement("li");
		var linowring = document.createElement("li");
		var liwring = document.createElement("li");
		var lideviation = document.createElement("li");
		var lideviationOn = document.createElement("li");
		litotalQuestionnaire.style.cssText = "width:150px;margin-left:27px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liResidentsTotal.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liinvestigatorsTotal.style.cssText = "width:150px;margin-left:30px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liReviewNot.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liReviewYes.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liReviewPass.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		licompleted.style.cssText = "width:150px;margin-left:27px;font-size:14px;color:#474747;margin-top:5px;text-align:left;float:left;";
		linoCompleted.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		linowring.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		liwring.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		lideviation.style.cssText = "width:150px;margin-left:30px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		lideviationOn.style.cssText = "width:350px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		litotalQuestionnaire.innerText = "问卷总量："
				+ obj[0].totalQuestionnaire;
		liResidentsTotal.innerText = "居民填写："
				+ obj[0].ResidentsTotal;
		liinvestigatorsTotal.innerText = "调查员填写："
				+ obj[0].investigatorsTotal;
		liReviewNot.innerText = "未审核：" + obj[0].ReviewNot;
		liReviewYes.innerText = "审核通过：" + obj[0].ReviewYes;
		liReviewPass.innerText = "审核不通过：" + obj[0].ReviewPass;
		licompleted.innerText = "问卷填写完成：" + obj[0].completed;
		linoCompleted.innerText = "问卷填写未完成：" + obj[0].noCompleted;
		linowring.innerText = "未中奖：" + obj[0].nowring;
		liwring.innerText = "中奖：" + obj[0].wring;
		if (flag) {
			lideviationOn.innerText = "位置偏差：" + "正常："
					+ obj[0].deviation + "，" + "异常："
					+ obj[0].deviationOn;
			/* lideviation.innerText="无偏差："+obj[0].deviation; */
		}
		ul.appendChild(litotalQuestionnaire);
		ul.appendChild(liReviewNot);
		ul.appendChild(liReviewYes);
		ul.appendChild(liReviewPass);
		ul.appendChild(liResidentsTotal);
		ul.appendChild(liinvestigatorsTotal);
		ul.appendChild(licompleted);
		ul.appendChild(linoCompleted);
		ul.appendChild(linowring);
		ul.appendChild(liwring);
		ul.appendChild(lideviationOn);
		document.getElementById("information").appendChild(ul);
			}
		});

if (flag) {
	$("#table").bootstrapTable(
			{
				method : 'post',
				contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
				striped : true,
				cache : true,
				pagination : true,
				sidePagination : "server",
				pageSize : 10,
				pageNumber : 1,
				height : 550,
				align : 'center',
				pageList : [ 10, 20, 30 ],
				queryParams : function(params) {
					data = {
						offset : params.offset, // 第几页
						limit : params.limit,
					}
					return data;
				},
				url : root
						+ '/services/getConditions?districtNumber='
						+ districtNumber + '&subofficeNumber='
						+ subofficeNumber + '&type=' + type
						+ '&familyId=' + familyId
						+ '&communityNumber=' + communityNumber,
				escape : true,
				silent : true,
				sortStable : true,
				sortOrder : true,
				dataType : 'json',
				columns : [
						{
							// field: 'Number',//可不加
							title : '序号',// 标题 可不加
							valign : 'middle',
							formatter : function(value, row, index) {
								return index + 1;
							}
						},
						/*
						 * {
						 * 
						 * field:'number', title:'序号', width:110,
						 * valign:'middle', align:'center' },
						 */
						{
							field : "familyId",
							title : '家庭编号',
							width : 110,
							valign : 'middle',
							align : 'center',
							formatter : function(value, row, index) {
								return '<a href="javaScript:getDetail('
										+ row.id
										+ ');">'
										+ row.familyId + '</a>';
							}

						}, {
							field : 'districtName',
							title : '行政区',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'subofficeName',
							title : '街道',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'communityName',
							title : '社区',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'familyMembers',
							title : '家庭成员数',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'travelTotal',
							title : '出行次数',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'status',
							title : '审核状态',
							width : 110,
							valign : 'middle',
							align : 'center'

						}, {
							field : 'deviation',
							title : '位置偏差',
							width : 110,
							valign : 'middle',
							align : 'center'

						}, {
							field : 'submitDate',
							title : '时间',
							width : 110,
							align : 'center'

								}

						]

					});
} else {
	$("#table").bootstrapTable(
			{
				method : 'post',
				contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
				striped : true,
				cache : true,
				pagination : true,
				sidePagination : "server",
				pageSize : 10,
				pageNumber : 1,
				height : 550,
				align : 'center',
				pageList : [ 10, 20, 30 ],
				queryParams : function(params) {
					data = {
						offset : params.offset, // 第几页
						limit : params.limit,
					}
					return data;
				},
				url : root
						+ '/services/getConditions?districtNumber='
						+ districtNumber + '&subofficeNumber='
						+ subofficeNumber + '&type=' + type
						+ '&familyId=' + familyId
						+ '&communityNumber=' + communityNumber,
				escape : true,
				silent : true,
				sortStable : true,
				sortOrder : true,
				dataType : 'json',
				columns : [
						{
							// field: 'Number',//可不加
							title : '序号',// 标题 可不加
							valign : 'middle',
							formatter : function(value, row, index) {
								return index + 1;
							}
						},

						/*
						 * {
						 * 
						 * field:'number', title:'序号', width:110,
						 * valign:'middle', align:'center' },
						 */
						{
							field : "familyId",
							title : '家庭编号',
							width : 110,
							valign : 'middle',
							align : 'center',
							formatter : function(value, row, index) {
								return '<a href="javaScript:getDetail('
										+ row.id
										+ ');">'
										+ row.familyId + '</a>';
							}

						}, {
							field : 'districtName',
							title : '行政区',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'subofficeName',
							title : '街道',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'communityName',
							title : '社区',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'familyMembers',
							title : '家庭成员数',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'travelTotal',
							title : '出行次数',
							width : 110,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'status',
							title : '审核状态',
							width : 110,
							valign : 'middle',
							align : 'center'

						},
						/*
						 * { field:'deviation', title:'位置偏差',
						 * width:110, valign:'middle',
						 * align:'center'
						 *  },
						 */
						{
							field : 'submitDate',
							title : '时间',
							width : 110,
							align : 'center'

									}

							]

						});
	}
}
// 查询公众问卷
function query() {
	var type = $('input[name="chSelect"]:checked ').val();
if (type == 1) {
	document.getElementById("Conditions").style.display = "";
document.getElementById("information").style.display = "";
document.getElementById("imgOne").style.display = "";
document.getElementById("public").style.display = "none";
document.getElementById("tableOne").style.display = "block";
document.getElementById("tableTwo").style.display = "none";
/* search(); */
} else if (type == 2) {
	$("#district").empty();
loadDis();
document.getElementById("information").style.display = "none";
document.getElementById("imgOne").style.display = "none";
document.getElementById("public").style.display = "block";
document.getElementById("tableOne").style.display = "none";
document.getElementById("tableTwo").style.display = "block";
/*
 * $.ajax({ url : root+"/services/statisticsNumber", type : "POST",
 * dataType : "json", success : function(result) { var obj =
 * eval(result); var ul=document.createElement("ul"); var
 * litotalQuestionnaire=document.createElement("li"); var
 * liResidentsTotal=document.createElement("li"); var
 * liinvestigatorsTotal=document.createElement("li"); var
 * liReviewNot=document.createElement("li"); var
 * liReviewYes=document.createElement("li"); var
 * liReviewPass=document.createElement("li"); var
 * licompleted=document.createElement("li"); var
 * linoCompleted=document.createElement("li"); var
 * linowring=document.createElement("li"); var
 * liwring=document.createElement("li"); var
 * lideviation=document.createElement("li"); var
 * lideviationOn=document.createElement("li");
 * litotalQuestionnaire.style.cssText="width:150px;margin-left:27px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
 * liResidentsTotal.style.cssText="width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
 * liinvestigatorsTotal.style.cssText="width:150px;margin-left:30px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
 * liReviewNot.style.cssText="width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
 * liReviewYes.style.cssText="width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
 * liReviewPass.style.cssText="width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
 * licompleted.style.cssText="width:150px;margin-left:27px;font-size:14px;color:#474747;margin-top:5px;text-align:left;float:left;";
 * linoCompleted.style.cssText="width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
 * linowring.style.cssText="width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
 * liwring.style.cssText="width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
 * lideviation.style.cssText="width:150px;margin-left:30px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
 * lideviationOn.style.cssText="width:235px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
 * litotalQuestionnaire.innerText="问卷总量："+obj[0].totalQuestionnaire;
 * liResidentsTotal.innerText="居民填写："+obj[0].ResidentsTotal;
 * liinvestigatorsTotal.innerText="调查员填写："+obj[0].investigatorsTotal;
 * liReviewNot.innerText="未审核："+obj[0].ReviewNot;
 * liReviewYes.innerText="审核通过："+obj[0].ReviewYes;
 * liReviewPass.innerText="审核不通过："+obj[0].ReviewPass;
 * licompleted.innerText="问卷填写完成："+obj[0].completed;
 * linoCompleted.innerText="问卷填写未完成："+obj[0].noCompleted;
 * linowring.innerText="未中奖："+obj[0].nowring;
 * liwring.innerText="中奖："+obj[0].wring; if(flag){
 * lideviationOn.innerText="位置偏差："+"正常："+obj[0].deviation+"，"+"异常："+obj[0].deviationOn;
 * lideviation.innerText="无偏差："+obj[0].deviation; }
 * ul.appendChild(litotalQuestionnaire); ul.appendChild(liReviewNot);
 * ul.appendChild(liReviewYes); ul.appendChild(liReviewPass);
 * ul.appendChild(liResidentsTotal);
 * ul.appendChild(liinvestigatorsTotal); ul.appendChild(licompleted);
 * ul.appendChild(linoCompleted); ul.appendChild(linowring);
 * ul.appendChild(liwring); ul.appendChild(lideviationOn);
 * document.getElementById("information").appendChild(ul); } });
 */
if (flag) {
	document.getElementById("Conditions").style.display = "none";
$("#tablePublic").bootstrapTable(
				{
					method : 'post',
					contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
					striped : true,
					cache : true,
					pagination : true,
					sidePagination : "server",
					pageSize : 10,
					pageNumber : 1,
					height : 550,
					align : 'center',
					pageList : [ 10, 20, 30 ],
					queryParams : function(params) {
						data = {
							offset : params.offset, // 第几页
							limit : params.limit,
						}
						return data;
					},
					url : root + '/services/getPublicQuestionnaire',
					escape : true,
					silent : true,
					sortStable : true,
					sortOrder : true,
					dataType : 'json',
					columns : [
							{
								// field: 'Number',//可不加
								title : '序号',// 标题 可不加
								valign : 'middle',
								formatter : function(value, row,
										index) {
									return index + 1;
								}
							},

							/*
							 * {
							 * 
							 * field:'number', title:'序号',
							 * width:150, valign:'middle',
							 * align:'center' },
							 */
							{
								field : "familyId",
								title : '家庭编号',
								width : 150,
								valign : 'middle',
								align : 'center',
								formatter : function(value, row,
										index) {
									return '<a href=\"info_rmation.jsp?id='
											+ row.id
											+ '\">'
											+ row.familyId + '</a>';
								}

							}, {
								field : 'districtName',
								title : '行政区',
								width : 150,
								valign : 'middle',
								align : 'center'
							},
							/*
							 * { field:'subofficeName', title:'街道',
							 * width:110, valign:'middle',
							 * align:'center' }, {
							 * field:'communityName', title:'社区',
							 * width:110, valign:'middle',
							 * align:'center' },
							 */
							{
								field : 'familyMembers',
								title : '家庭成员数',
								width : 200,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'travelTotal',
								title : '出行次数',
								width : 200,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'status',
								title : '审核状态',
								width : 200,
								valign : 'middle',
								align : 'center'

							}, {
								field : 'deviation',
								title : '位置偏差',
								width : 200,
								valign : 'middle',
								align : 'center'

							}, {
								field : 'submitDate',
								title : '时间',
								width : 200,
								align : 'center'

								}

						]

					});
} else {
	document.getElementById("Conditions").style.display = "none";
$("#tablePublic").bootstrapTable(
				{
					method : 'post',
					contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
					striped : true,
					cache : true,
					pagination : true,
					sidePagination : "server",
					pageSize : 10,
					pageNumber : 1,
					height : 550,
					align : 'center',
					pageList : [ 10, 20, 30 ],
					queryParams : function(params) {
						data = {
							offset : params.offset, // 第几页
							limit : params.limit,
						}
						return data;
					},
					url : root + '/services/getPublicQuestionnaire',
					escape : true,
					silent : true,
					sortStable : true,
					sortOrder : true,
					dataType : 'json',
					columns : [
							{
								// field: 'Number',//可不加
								title : '序号',// 标题 可不加
								valign : 'middle',
								formatter : function(value, row,
										index) {
									return index + 1;
								}
							},
							/*
							 * {
							 * 
							 * field:'number', title:'序号',
							 * width:150, valign:'middle',
							 * align:'center' },
							 */
							{
								field : "familyId",
								title : '家庭编号',
								width : 150,
								valign : 'middle',
								align : 'center',
								formatter : function(value, row,
										index) {
									return '<a href=\"info_rmation.jsp?id='
											+ row.id
											+ '\">'
											+ row.familyId + '</a>';
								}

							}, {
								field : 'districtName',
								title : '行政区',
								width : 150,
								valign : 'middle',
								align : 'center'
							},
							/*
							 * { field:'subofficeName', title:'街道',
							 * width:110, valign:'middle',
							 * align:'center' }, {
							 * field:'communityName', title:'社区',
							 * width:110, valign:'middle',
							 * align:'center' },
							 */
							{
								field : 'familyMembers',
								title : '家庭成员数',
								width : 200,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'travelTotal',
								title : '出行次数',
								width : 200,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'status',
								title : '审核状态',
								width : 200,
								valign : 'middle',
								align : 'center'

							}, {
								field : 'submitDate',
								title : '时间',
								width : 200,
								align : 'center'

										}

								]

							});
		}
	}
}
function queryPublic() {
	var district = $('#district').find("option:selected").text();
if (flag) {
	$('#tablePublic').bootstrapTable('destroy');
document.getElementById("Conditions").style.display = "none";
$("#tablePublic").bootstrapTable(
			{
				method : 'post',
				contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
				striped : true,
				cache : true,
				pagination : true,
				sidePagination : "server",
				pageSize : 10,
				pageNumber : 1,
				height : 550,
				align : 'center',
				pageList : [ 10, 20, 30 ],
				queryParams : function(params) {
					data = {
						offset : params.offset, // 第几页
						limit : params.limit,
					}
					return data;
				},
				url : root + '/services/selectPublic?district='
						+ district,
				escape : true,
				silent : true,
				sortStable : true,
				sortOrder : true,
				dataType : 'json',
				columns : [
						{
							// field: 'Number',//可不加
							title : '序号',// 标题 可不加
							valign : 'middle',
							formatter : function(value, row, index) {
								return index + 1;
							}
						},
						/*
						 * { field:'number', title:'序号', width:150,
						 * valign:'middle', align:'center' },
						 */
						{
							field : "familyId",
							title : '家庭编号',
							width : 150,
							valign : 'middle',
							align : 'center',
							formatter : function(value, row, index) {
								return '<a href="javaScript:getDetail('
										+ row.id
										+ ');">'
										+ row.familyId + '</a>';
							}
						}, {
							field : 'districtName',
							title : '行政区',
							width : 150,
							valign : 'middle',
							align : 'center'
						},

						{
							field : 'familyMembers',
							title : '家庭成员数',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'travelTotal',
							title : '出行次数',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'status',
							title : '审核状态',
							width : 200,
							valign : 'middle',
							align : 'center'

						}, {
							field : 'deviation',
							title : '位置偏差',
							width : 200,
							valign : 'middle',
							align : 'center'

						}, {
							field : 'submitDate',
							title : '时间',
							width : 200,
							align : 'center'

								}

						]

					});
} else {
	$('#tablePublic').bootstrapTable('destroy');
document.getElementById("Conditions").style.display = "none";
$("#tablePublic").bootstrapTable(
			{
				method : 'post',
				contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
				striped : true,
				cache : true,
				pagination : true,
				sidePagination : "server",
				pageSize : 10,
				pageNumber : 1,
				height : 550,
				align : 'center',
				pageList : [ 10, 20, 30 ],
				queryParams : function(params) {
					data = {
						offset : params.offset, // 第几页
						limit : params.limit,
					}
					return data;
				},
				url : root + '/services/selectPublic?district='
						+ district,
				escape : true,
				silent : true,
				sortStable : true,
				sortOrder : true,
				dataType : 'json',
				columns : [
						{
							// field: 'Number',//可不加
							title : '序号',// 标题 可不加
							valign : 'middle',
							formatter : function(value, row, index) {
								return index + 1;
							}
						},
						{
							field : "familyId",
							title : '家庭编号',
							width : 150,
							valign : 'middle',
							align : 'center',
							formatter : function(value, row, index) {
								return '<a href=\"info_rmation.jsp?id='
										+ row.id
										+ '\">'
										+ row.familyId + '</a>';
							}
						}, {
							field : 'districtName',
							title : '行政区',
							width : 150,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'familyMembers',
							title : '家庭成员数',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'travelTotal',
							title : '出行次数',
							width : 200,
							valign : 'middle',
							align : 'center'
						}, {
							field : 'status',
							title : '审核状态',
							width : 200,
							valign : 'middle',
							align : 'center'

						}, {
							field : 'submitDate',
							title : '时间',
							width : 200,
							align : 'center'
									} ]
						});
	}
}
// 数量统计
function queryTotalQuestionnaire() {
	$
			.ajax({
				url : root + "/services/statisticsNumber",
	type : "POST",
	dataType : "json",
	success : function(result) {
		var obj = eval(result);
		var ul = document.createElement("ul");
		var litotalQuestionnaire = document.createElement("li");
		var liResidentsTotal = document.createElement("li");
		var liinvestigatorsTotal = document.createElement("li");
		var liReviewNot = document.createElement("li");
		var liReviewYes = document.createElement("li");
		var liReviewPass = document.createElement("li");
		var licompleted = document.createElement("li");
		var linoCompleted = document.createElement("li");
		var linowring = document.createElement("li");
		var liwring = document.createElement("li");
		var lideviation = document.createElement("li");
		var lideviationOn = document.createElement("li");
		litotalQuestionnaire.style.cssText = "width:150px;margin-left:27px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liResidentsTotal.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liinvestigatorsTotal.style.cssText = "width:150px;margin-left:30px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liReviewNot.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liReviewYes.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		liReviewPass.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:15px;text-align:left;float:left;";
		licompleted.style.cssText = "width:150px;margin-left:27px;font-size:14px;color:#474747;margin-top:5px;text-align:left;float:left;";
		linoCompleted.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		linowring.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		liwring.style.cssText = "width:150px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		lideviation.style.cssText = "width:150px;margin-left:30px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		lideviationOn.style.cssText = "width:350px;margin-left:0px;font-size:14px;color:#474747;margin-top:4px;text-align:left;float:left;";
		litotalQuestionnaire.innerText = "问卷总量："
				+ obj[0].totalQuestionnaire;
		liResidentsTotal.innerText = "居民填写："
				+ obj[0].ResidentsTotal;
		liinvestigatorsTotal.innerText = "调查员填写："
				+ obj[0].investigatorsTotal;
		liReviewNot.innerText = "未审核：" + obj[0].ReviewNot;
		liReviewYes.innerText = "审核通过：" + obj[0].ReviewYes;
		liReviewPass.innerText = "审核不通过：" + obj[0].ReviewPass;
		licompleted.innerText = "问卷填写完成：" + obj[0].completed;
		linoCompleted.innerText = "问卷填写未完成：" + obj[0].noCompleted;
		linowring.innerText = "未中奖：" + obj[0].nowring;
		liwring.innerText = "中奖：" + obj[0].wring;
		if (flag) {
			lideviationOn.innerText = "位置偏差：" + "正常："
					+ obj[0].deviation + "，" + "异常："
					+ obj[0].deviationOn;
			/*	lideviation.innerText="无偏差："+obj[0].deviation;*/
		}
		ul.appendChild(litotalQuestionnaire);
		ul.appendChild(liReviewNot);
		ul.appendChild(liReviewYes);
		ul.appendChild(liReviewPass);
		ul.appendChild(liResidentsTotal);
		ul.appendChild(liinvestigatorsTotal);
		ul.appendChild(licompleted);
		ul.appendChild(linoCompleted);
		ul.appendChild(linowring);
		ul.appendChild(liwring);
		ul.appendChild(lideviationOn);
		document.getElementById("information").appendChild(ul);
				}
			});
}

function getDetail(familyId) {
	var windowStatus = 'dialogWidth:260px;dialogHeight:180px;center:yes;status:0;';
//在模式窗口中打开的页面  
var winOption = "width='1300px',top=50,left=50,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,fullscreen=0";
var url = "info_rmation.jsp?id=" + familyId;
	window.open(url, window, winOption);

}
