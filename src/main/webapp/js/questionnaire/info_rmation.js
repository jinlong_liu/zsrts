
//审核状态模态框
function uodateTotal(){
	$('#myModal').modal('show');
	}
//审核状态保存
function save() {
	var audit = $('input[name="chSelect"]:checked ').val();
	var familyId = $('#familyId').val();
	if(audit==undefined){
		sweetAlert("NO", "选择审核状态后再确认", "error");
		return;
	} 
	$.ajax({
		url : root+"/services/questionnaireReview",
		type : "POST",
		dataType : "json",
		data:{
		familyId:familyId,
		audit:audit
	},
	   success : function(result) {
		var back = eval(result)[0].back;
		if(back==1){
			$('#myModal').modal('hide');
			art.dialog.tips('审核完成！', 1.5);
			location.reload();
		}else{
			sweetAlert("抱歉!!!", "系统错误审核失败，请重新点击审核", "error");
		}
		if(back=="没有权限"){
			$('#myModal').modal('hide');
			sweetAlert("抱歉!!!", "问卷已完成审核，如需更改请联系管理员", "error");
		}
	}		
	});
	
}	
$().ready(function() {
	var familyId = $('#familyId').val();
	var addres;
	$.ajax({
		url : root+"/services/getaudit",
		type : "POST",
		dataType : "json",
		data:{
		familyId:familyId
	},
	
	success : function(result) {
		var back = eval(result)[1].back;
		if(back=="未完成"){
			document.getElementById("auditButton").style.display = "none";//隐藏  
		}
		if(eval(result)[0].status!=""){
		var status = eval(result)[0].status;
		document.getElementById("quesr"). innerHTML = '审核状态：'+status; 
		}else{
	    document.getElementById("quesr"). innerHTML = '审核状态：'; 
		}
		if(eval(result)[1].back!=""){
			var back = eval(result)[1].back;
			document.getElementById("progress"). innerHTML = '填写进度：'+back;
		}else{
			document.getElementById("progress"). innerHTML = '填写进度：';
		}
	}		
	});
	//家庭基本信息展示
	$.ajax({
		url : root+"/services/getListfamily",
		type : "POST",
		dataType : "json",
		data:{
		familyId:familyId
	},
	success : function(result) {
		addres = eval(result)[0].homeAddress;
		var wid=document.body.clientWidth;//获取当前平米分辨率
		var divs=document.createElement("div");
		divs.style.cssText="width:"+(wid-25)+"px;height:60px; solid black;" +
		"margin-top:78px;border-radius: 5px;"; 
		var divHome=document.createElement("div");
		var divHomea=document.createElement("div");
		var imgNode = document.createElement('img');
		imgNode.setAttribute('src','../images/quan.png');
		divHomea.appendChild(imgNode);
		divHome.innerText="家庭基本信息";	
		divHome.style.cssText="width:120px;height:25px;margin-left:56px;" +
		"font-size:16px; color:#06b370;  ";
		divHomea.style.cssText="float:left;margin-top:4px;" +
		"margin-left:8px;" ;
		var divw=document.createElement("div");
		divw.style.cssText="width:1056;px;height:20px;margin-top:15px;" +
		"margin-left:56px;font-size:14px;" ;
		if(eval(result)[0].homeAddress!=1){
			divw.innerText="家庭住址："+eval(result)[0].homeAddress;	 
		}
		divs.appendChild(divHomea);
		divs.appendChild(divHome);
		divs.appendChild(divw);
		document.body.appendChild(divs);
		
		//车辆信息查询结果展示
		$.ajax({
			url : root+"/services/getListCar",
			type : "POST",
			dataType : "json",
			data:{
			familyId:familyId
		},
		success : function(result) {
			var total = eval(result).length;
			var wid=document.body.clientWidth;
			// alert(total);
			var hig=43*(total+1);
			var div=document.createElement("div");
			div.style.cssText="width:1056px;height:"+hig+"px;" +
			"margin-top:30px;margin-left:5px;";    	
			var divt=document.createElement("div");
			var divxy=document.createElement("div");
			var imgNode = document.createElement('img');
			imgNode.setAttribute('src','../images/quan.png');
			divxy.appendChild(imgNode);
			divt.innerText="车辆基本信息";	
			divt.style.cssText="width:120px;height:22px;margin-top:5px;" +
			"font-size:16px; color:#06b370;margin-left:50px; ";
			divxy.style.cssText="margin-top:3px;" +
			"margin-left:10px; 	 float:left; " ;
			div.appendChild(divxy);
			div.appendChild(divt);
			
			var disp = eval(result)[0].displacement;
			//alert(disp);
			//判断该家庭是否有车辆
			if(disp=="暂无车辆信息"){
				//alert(1111);
				var divc=document.createElement("div");
				divc.style.cssText="width:1056;px;height:40px;margin-top:15px;" +
				"margin-left:26px;font-size:16px; padding-left:2px;border:2px solid #f0f0f0;" +
				"line-height:35px;padding-left:18px;";
				divc.innerText="暂无车辆信息";	    
				div.appendChild(divc);
				document.body.appendChild(div);
			}else {
				
				for(var i=0;i<total;i++){
					var divw=document.createElement("div");
					if(i==0){
						divw.style.cssText="width:1056px;height:45px;margin-top:15px;" +
						"font-size:16px;margin-left:27px;" +
						"border:1px solid  #f0f0f0;margin-right:40px;line-height:180%;";
					}else {
						divw.style.cssText="width:1056px;height:45px;margin-top:0px;" +
						"font-size:16px;margin-left:27px;" +
						"border:1px solid  #f0f0f0;margin-right:40px;line-height:180%;";
					}
					var displacement=eval(result)[i].displacement;
					var mileage=eval(result)[i].mileage;
					var parkingSpace=eval(result)[i].parkingSpace;
					var VehiclepProperties=eval(result)[i].VehiclepProperties;
					var ul=document.createElement("ul");
					var li=document.createElement("li");
					var limileage=document.createElement("li");
					var liparkingSpace=document.createElement("li");
					var liVehiclepProperties=document.createElement("li");
					var liCar=document.createElement("li");
					li.style.cssText="float:left;margin-left:70px;font-size:14px;color:#474747;width:196px;margin-top:8px;text-align:left;";
					limileage.style.cssText="float:left;margin-left:2px;font-size:14px;color:#474747;width:200px;margin-top:8px;text-align:left;";
					liparkingSpace.style.cssText="float:left;margin-left:70px;font-size:14px;color:#474747;width:150px;margin-top:8px;overflow:hidden;text-align:left;";
					liVehiclepProperties.style.cssText="float:left;margin-left:70px;font-size:14px;color:#474747;width:150px;margin-top:8px;overflow:hidden;text-align:left;";
					liCar.style.cssText="float:left;margin-left:23px;font-size:14px;color:#474747;width:120px;margin-top:8px;overflow:hidden;text-align:left;;";
					
					liCar.innerText="第"+(i+1)+"辆车的信息";
					li.innerText="排量："+displacement;
					limileage.innerText="行驶里程："+mileage;
					liparkingSpace.innerText="是否有停车位："+parkingSpace;
					liVehiclepProperties.innerText="车辆属性："+VehiclepProperties;
					ul.appendChild(liCar);
					ul.appendChild(li);
					ul.appendChild(limileage);
					ul.appendChild(liparkingSpace);
					ul.appendChild(liVehiclepProperties);
					divw.appendChild(ul);
					div.appendChild(divw);
				}
				document.body.appendChild(div);//div放到页面	
			}
			
			//家庭成员基本信息结果展示
			$.ajax({
				url : root+"/services/getListHome",
				type : "POST",
				dataType : "json",
				data:{
				familyId:familyId
			},
			success : function(result) {
				var total = eval(result).length;
				var wid=document.body.clientWidth;
				var hi=document.body.clientHeight;
				var hig=80*total+(total-1)*50+80;
				var div=document.createElement("div");
				/*alert(hig);*/
				/*if(total==1){
					div.style.cssText="width:1070px;height:380px; margin-left:20px;" +
					"margin-top:30px;"; 
				}else{
					div.style.cssText="width:1070px;height:380px; margin-left:20px;" +
					"margin-top:30px;"; 
					
				}*/
				if(total<=3){
					div.style.cssText="width:1070px;height:380px; margin-left:20px;" +
					"margin-top:30px;"; 
				}else if(total>3 && total<=6){
					div.style.cssText="width:1070px;height:731px; margin-left:20px;" +
					"margin-top:30px;"; 
				}else if(total>6 && total<=9){
					div.style.cssText="width:1070px;height:1089px; margin-left:20px;" +
					"margin-top:30px;"; 
				}
				var divo=document.createElement("div");
				var divxo=document.createElement("div");
				var imgNode = document.createElement('img');
				imgNode.setAttribute('src','../images/quan.png');
				
				divxo.appendChild(imgNode);
				
				divo.innerText="家庭成员基本信息";	    	
				divo.style.cssText="width:140px;height:20px;margin-top:5px;margin-left:34px;" +
				"font-size:16px; color:#06b370;";
				divxo.style.cssText="margin-top:3px;" +
				"margin-left:10px; 	 float:left;" ;
				div.appendChild(divxo);
				div.appendChild(divo);
				
				
					var sex = eval(result)[0].sex;
							
							//判断该家庭是否有家庭成员
							if(sex=="暂无其他家庭成员"){
								  	
								for ( var int = 0; int < 1; int++) {
									//alert(mber);
									var divq=document.createElement("div");
									divq.style.cssText="float:left;margin-left:10px;width:345px;" +
									"height:345px;margin-top:10px;" ;
									divq.style.backgroundImage ='url(../images/bu.png)'; 
									div.appendChild(divq);	
								}
							}else {
				var b = total;
				for(var i=0;i<total;i++){
					
					var div2=document.createElement("div");
					var a = b--;
					
					if(a%3==0){
						div2.style.cssText="float:left;margin-left:10px;width:345px;" +
						"height:345px;margin-top:10px;" ;
						
						div2.style.backgroundImage ='url(../images/home2.png)';
						
					}else if(a%3==1){
						div2.style.cssText="float:left;margin-left:11px;width:345px;" +
						"height:345px;margin-top:10px;" ;
						
						div2.style.backgroundImage ='url(../images/home.png)';   
					}else if(a%3==2){
						div2.style.cssText="float:left;margin-left:10px;width:345px;" +
						"height:345px;margin-top:10px;" ;
						
						div2.style.backgroundImage ='url(../images/home3.png)'; 
					}
					/*  }else if(i==2){
							    	div2.style.cssText="float:left;margin-left:5px;width:345px;" +
									"height:351px;margin-top:3px;" ;
									
							        div2.style.backgroundImage ='url(../images/home3.png)'; 
							    }else if(i==3){
							    	div2.style.cssText="float:left;margin-left:10px;width:345px;" +
									"height:351px;margin-top:3px;" ;
									
							        div2.style.backgroundImage ='url(../images/home.png)'; 
							    }*/
					
					var sex=eval(result)[i].sex;//获取性别
					var age=eval(result)[i].age;//获取年龄
					var local=eval(result)[i].local;//获取居住情况
					var professional=eval(result)[i].professional;//获取职业
					var monthlyIncome=eval(result)[i].monthlyIncome;//获取月收入
					/*var busCardNumber=eval(result)[i].busCardNumber;//获取公交IC卡号
									var mobilePhoneNo=eval(result)[i].mobilePhoneNo;//获取手机号
					 */
					var unitAddress=eval(result)[i].unitAddress;//获取单位地址
					 var ul=document.createElement("ul");
					 var li=document.createElement("li");
					 var liage=document.createElement("li");
					 var lilocal=document.createElement("li");
					 var liprofessional=document.createElement("li");
					 var lir=document.createElement("li");
					 var limonthlyIncome=document.createElement("li");
					 var libusCardNumber=document.createElement("li");
					 var limobilePhoneNo=document.createElement("li");
					 var liunitAddress=document.createElement("li");
					 //判断工作，如果是学生的话就创建学校地址
					 if(professional=="学生"){
						 liunitAddress.innerText="学校地址："+unitAddress;
						 limonthlyIncome.innerText="月收入：";
					 }else if(professional=="退休"){
						 limonthlyIncome.innerText="月收入："+monthlyIncome;
						 liunitAddress.innerText="单位地址：";
					 }else{
						 liunitAddress.innerText="单位地址："+unitAddress;
						 limonthlyIncome.innerText="月收入："+monthlyIncome;
					 }
					 lir.style.cssText="margin-left:30px;font-size:18px;color:#FFFFFF;margin-top:15px;text-align:left;";
					 li.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:30px;text-align:left";
					 liage.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left;";
					 lilocal.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
					 liprofessional.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
					 limonthlyIncome.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
					 /*libusCardNumber.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
									limobilePhoneNo.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";*/
					 //alert(unitAddress);
					 //判断单位地址是否为空，如果为空就不显示。
					 if(unitAddress==undefined){
						 liunitAddress.style.cssText="display:none;margin-left:10px;font-size:16px;width:550px;margin-top:18px;text-align:left;";
					 }else{
						 liunitAddress.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left;";
					 }
					 lir.innerText="第"+(i+1)+"位成员的信息";
					 li.innerText="性别："+sex;
					 liage.innerText="年龄："+age+"岁";
					 lilocal.innerText="是否常住本地："+local;
					 liprofessional.innerText="职业："+professional;
					 
					 /*limobilePhoneNo.innerText="手机号："+mobilePhoneNo;*/
					 ul.appendChild(lir);
					 ul.appendChild(li);
					 ul.appendChild(liage);
					 ul.appendChild(lilocal);
					 ul.appendChild(liprofessional);
					 ul.appendChild(limonthlyIncome);
					 /*ul.appendChild(limobilePhoneNo);*/
					 ul.appendChild(liunitAddress);
					 div2.appendChild(ul);
					 div.appendChild(div2);
				}
				}
				
				var number=total%3;
				var mber=0;
				//alert(number);
				if(number==0){
					mber=number;
					//alert(mber);
				}else{
					mber=3-number;
					//alert(mber);
				}
				for ( var int = 0; int < mber; int++) {
					//alert(mber);
					var divq=document.createElement("div");
					divq.style.cssText="float:left;margin-left:10px;width:345px;" +
					"height:345px;margin-top:10px;" ;
					divq.style.backgroundImage ='url(../images/bu.png)'; 
					div.appendChild(divq);	
				}
				document.body.appendChild(div);//div放到页面
				//出行信息
				$.ajax({
					url :  root+"/services/getInformation",
					dataType : "json",
					type : "post",
					data:{
					familyId:familyId
				},
				success : function(res) {
					/* alert(addres);*/
					var total = eval(res).length;
					if(total<=3){
						document.getElementById("leftSide").style.height="1300px";
					}else if(total>3 && total<=6){
						document.getElementById("leftSide").style.height="1660px";
					}else if(total>6 && total<=9){
						document.getElementById("leftSide").style.height="2055px";
					}
					var wid=document.body.clientWidth;
				/*	var hig=128*total+(total-1)*50+80;*/
					var div=document.createElement("div");
					div.style.cssText="width:1070px; min-height:200px;  margin-left:20px;" +
					"margin-top:30px;";    	
					var divo=document.createElement("div");
					var divxo=document.createElement("div");
					var imgNode = document.createElement('img');
					imgNode.setAttribute('src','../images/quan.png');
					divxo.appendChild(imgNode);
					divo.innerText="出行信息";	    	
					divo.style.cssText="width:140px;height:20px;margin-top:5px;margin-left:34px;" +
					"font-size:16px; color:#06b370;";
					divxo.style.cssText="margin-top:3px;" +
					"margin-left:10px; 	 float:left;" ;
					div.appendChild(divxo);
					div.appendChild(divo);
					    	 name = eval(res)[0].name;
								//判断该家庭成员是否出行
								if(name==""){
									for ( var int = 0; int < 1; int++) {
										//alert(mber);
										var divq=document.createElement("div");
										divq.style.cssText="float:left;margin-left:10px;width:345px;" +
										"height:420px;margin-top:10px;" ;
										divq.style.backgroundImage ='url(../images/bu.png)'; 
										div.appendChild(divq);	
									}
								}else{
									var b = total;
									for(var i=0;i<total;i++){
										var div2=document.createElement("div");
										var a = b--;
										if(a%3==0){
											div2.style.cssText="float:left;margin-left:10px;width:345px;" +
											"height:420px;margin-top:10px;" ;
											
											div2.style.backgroundImage ='url(../images/home2.png)';
											
										}else if(a%3==1){
											div2.style.cssText="float:left;margin-left:11px;width:345px;" +
											"height:420px;margin-top:10px;" ;
											
											div2.style.backgroundImage ='url(../images/home.png)';   
										}else if(a%3==2){
											div2.style.cssText="float:left;margin-left:10px;width:345px;" +
											"height:420px;margin-top:10px;" ;
											
											div2.style.backgroundImage ='url(../images/home3.png)'; 
										}
										var ul=document.createElement("ul");
										var li=document.createElement("li");
										var liage=document.createElement("li");
										var lilocal=document.createElement("li");
										var liprofessional=document.createElement("li");
										var lir=document.createElement("li");
										var limonthlyIncome=document.createElement("li");
										var libusCardNumber=document.createElement("li");
										var limobilePhoneNo=document.createElement("li");
										var liunitAddress=document.createElement("li");
										var liunitchild=document.createElement("li");
										var liunitchildAddress=document.createElement("li");
										//判断是否接送小孩
										var noTravel = eval(res)[i].noTravel;
										if(noTravel=="备注"){
											lir.innerText="第"+eval(res)[i].name+"位成员的出行信息";
											li.innerText="未出行信息注明："+eval(res)[i].note;
										}else{
											lir.innerText="第"+eval(res)[i].name+"位成员的出行信息";
											/* li.innerText="第"+eval(res)[i].name+"次出行信息";*/
											liage.innerText="出发时间："+eval(res)[i].starTime;
											lilocal.innerText="到达时间："+eval(res)[i].endTime;
											liprofessional.innerText="出行目的："+eval(res)[i].purposee;
											limobilePhoneNo.innerText="出行方式："+eval(res)[i].transpor;
										if(eval(res)[i].child=="否"){
											liunitchild.innerText="是否接送小孩：  "+eval(res)[i].child; 
										}else if(eval(res)[i].child=="送小孩" || eval(res)[i].child=="接小孩"){
											liunitchild.innerText="是否接送小孩：  "+eval(res)[i].child
											liunitchildAddress.innerText=eval(res)[i].childAddress; 
										}else if(eval(res)[i].child==1){
											liunitchild.innerText="是否接送小孩：  "; 
										}
										//出发地点判断
										
									
										if(eval(res)[i].starAddress=="家"){
											liunitAddress.innerText="出发地点："+addres;
											
										}else{
											liunitAddress.innerText="出发地点："+eval(res)[i].starAddress;
											
										}
										//判断到达到达地点
										if(eval(res)[i].endAddress=="家"){
											
											limonthlyIncome.innerText="到达地点："+addres;
										}else{
											limonthlyIncome.innerText="到达地点："+eval(res)[i].endAddress;
											
										}
										}
										
										lir.style.cssText="margin-left:30px;font-size:18px;color:#FFFFFF;margin-top:15px;text-align:left;";
										li.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:30px;text-align:left";
										liage.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left;";
										lilocal.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
										liprofessional.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
										limonthlyIncome.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
										libusCardNumber.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
										limobilePhoneNo.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
										liunitchild.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
										liunitchildAddress.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
										liunitAddress.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
										ul.appendChild(lir);
										ul.appendChild(li);
										ul.appendChild(liage);
										ul.appendChild(liunitAddress);
										ul.appendChild(lilocal);
										ul.appendChild(limonthlyIncome);
										ul.appendChild(liprofessional);
										ul.appendChild(limobilePhoneNo);
										ul.appendChild(liunitchild);
										ul.appendChild(liunitchildAddress);
										div2.appendChild(ul);
										div.appendChild(div2);
									}
									}
								/*if(noTravel!="备注"){*/
								var number=total%3;
								var mber=0;
								//alert(number);
								if(number==0){
									mber=number;
									//alert(mber);
								}else{
									mber=3-number;
									//alert(mber);
								}
								for ( var int = 0; int < mber; int++) {
									//alert(mber);
									var divSupplement=document.createElement("div");
									divSupplement.style.cssText="float:left;margin-left:10px;width:345px;" +
									"height:420px;margin-top:10px;" ;
									divSupplement.style.backgroundImage ='url(../images/bu.png)'; 
									div.appendChild(divSupplement);	
								}
								/*}*/
								document.body.appendChild(div);//div放到页面	
								//通讯地址
								$.ajax({
									url : root+"/services/getListWinning",
									type : "POST",
									dataType : "json",
									data:{
									familyId:familyId
								},
								success : function(result) {	
									if(eval(result)[0].recipientAddress!="没有权限"){
										var wid=document.body.clientWidth;//获取当前平米分辨率
										//alert(eval(result)[0].homeAddress);
										var divs=document.createElement("div");
										divs.style.cssText="width:1070px;height:60px; solid black;" +
										"margin-top:-190px;border-radius: 5px;"; 
										var divHome=document.createElement("div");
										var divHomea=document.createElement("div");
										
										var imgNode = document.createElement('img');
										imgNode.setAttribute('src','../images/quan.png');
										divHomea.appendChild(imgNode);
										divHome.innerText="通讯地址";	
										divHome.style.cssText="width:120px;height:25px;float:left;margin-top:13px;" +
										"margin-left:11px;" +
										"font-size:16px; color:#06b370;  ";
										divHomea.style.cssText="float:left;margin-top:16px;" +
										"margin-left:10px;" ;
										
										var ul=document.createElement("ul");
										var liRecipientAddress=document.createElement("li");
										var liRecipientName=document.createElement("li");
										var liRecipientPhone=document.createElement("li");
										/*divRecipientName.style.cssText="width:200px;px;height:20px;margin-top:50px;" +
										"margin-left:-118px;font-size:14px;float:left" ;
										alert(total);
										if(total<=3){
											divRecipientAddress.style.cssText="width:600px;px;height:20px;margin-top:-106px;" +
											"margin-left:460px;font-size:14px;float:left" ;
										}else{
											divRecipientAddress.style.cssText="width:600px;px;height:20px;margin-top:-21px;" +
											"margin-left:460px;font-size:14px;float:left" ;
										}
										divRecipientPhone.style.cssText="width:260px;height:20px;margin-top:50px;" +
										"margin-left:-6px;font-size:14px;float:left" ;*/
										/*alert(eval(result)[0].recipientPhone);
							              alert(eval(result)[0].recipientAddress);
							              alert(eval(result)[0].recipientName);*/
										liRecipientPhone.style.cssText="float:left;margin-left:50px;font-size:14px;color:#474747;width:200px;margin-top:50px;text-align:left;";
										liRecipientAddress.style.cssText="float:left;margin-left:50px;font-size:14px;color:#474747;width:500px;margin-top:50px;text-align:left;";
										liRecipientName.style.cssText="float:left;margin-left: -123px;font-size:14px;color:#474747;width:200px;margin-top:50px;text-align:left;";
										
										liRecipientPhone.innerText="收件人电话："+eval(result)[0].recipientPhone;	 
										liRecipientAddress.innerText="收件人地址："+eval(result)[0].recipientAddress;	 
										liRecipientName.innerText="收件人姓名："+eval(result)[0].recipientName;
										
										ul.appendChild(liRecipientName)
										ul.appendChild(liRecipientPhone)
										ul.appendChild(liRecipientAddress)
										divs.appendChild(divHomea);
										divs.appendChild(divHome);
										divs.appendChild(ul);
										if(eval(result)[0].cityCode=='0371'){
										document.body.appendChild(divs);
										}
									}	
								}	
								});
				}
				});
				
			}
			});
		}
		});
	} 
	});  
});

									
								
							
							

						
						






















