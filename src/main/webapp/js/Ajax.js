//对ajax的封装
Nature.Ajax = function (ajax) {
    //最基础的一层封装
   
        //定义默认值
        var defaultInfo = {
            type: "GET",                        //访问方式：如果dataPata不为空，自动设置为POST；如果为空设置为GET。
            dataType: Nature.sendDataType,      //数据类型：JSON、JSONP、text。由配置信息来搞定，便于灵活设置
            cache: true,                        //是否缓存，默认缓存
            xhrFields: {
                //允许跨域访问时添加cookie。cors跨域的时候需要设置
                withCredentials: true
            },
            urlPata: {},//url后面的参数。一定会加在url后面，不会加到form里。
            formPata: {},//表单里的参数。如果dataType是JSON，一定加在form里，不会加在url后面；如果dataType是JSONP的话，只能加在url后面。

            //url:  //依靠上层指定

            //timeout: 2000,
            error: function() {
            },  //如果出错，停止加载动画，给出提示。也可以增加自己的处理程序

            success: function() {
            } //成功后显示debug信息。也可以增加自己的处理程序
        };

        //补全ajaxInfo
        if (typeof ajaxInfo.dataType == "undefined") {
            ajaxInfo.dataType = defaultInfo.dataType;
        }
        
        if (typeof ajaxInfo.formPata == "undefined") {
            ajaxInfo.type = "GET";
        } else {
            if (ajaxInfo.dataType == "JSON") {
                ajaxInfo.type = "POST";
            } else {    //get或者jsonp
                ajaxInfo.type = "POST";
            }
            ajaxInfo.data = ajaxInfo.formPata;

        }

        if (typeof ajaxInfo.cache == "undefined") {
            ajaxInfo.cache = defaultInfo.cache;
        }
   


        //处理URL
        if (typeof ajaxInfo.urlPata != "undefined") {
            var tmpUrlPara = "";
            var para = ajaxInfo.urlPata;
            for (var key in para) {
                tmpUrlPara += "&" + key + "=" + para[key];
            }

            if (ajaxInfo.url.indexOf('?') >= 0) {
                //原地址有参数，直接加
                ajaxInfo.url += tmpUrlPara;
            } else {
                //原地址没有参数，变成?再加
                ajaxInfo.url += tmpUrlPara.replace('&', '?');
            }
        }

        //开始执行ajax
        $.ajax({
            type: ajaxInfo.type,
            dataType: ajaxInfo.dataType,
            cache: ajaxInfo.cache,
            xhrFields: {
                //允许跨域访问时添加cookie
                withCredentials: true
            },
            url: ajaxInfo.url,  
            data: ajaxInfo.data,
            //timeout: 2000,
            error: function() { //访问失败，自动停止加载动画，并且给出提示
                alert("提交" + ajaxInfo.title + "的时候发生错误！");
                if (typeof top.spinStop != "undefined")
                    top.spinStop();
                if (typeof ajaxInfo.error == "function") ajaxInfo.error();
            },

            success: function (data) {
                if (typeof(parent.DebugSet) != "undefined")
                    parent.DebugSet(data.debug);		//调用显示调试信息的函数。
                
                if (typeof (ajaxInfo.ctrlId) == "undefined")
                    ajaxInfo.success(data);
                else {
                    ajaxInfo.success(ajaxInfo.ctrlId, data);
                }

            }
        });
};