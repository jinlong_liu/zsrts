    function exportTableData(fileName,tableId,div,fileId,fileNames) {  
    	//alert(1);
        var tableString = '<div id="exportText" style="visibility:hidden"><table cellspacing="0" class="pb" id="tableData" >';  
        var frozenColumns = $('#'+tableId).datagrid("options").frozenColumns;  // 得到frozenColumns对象  
        var columns = $('#'+tableId).datagrid("options").columns;    // 得到columns对象  
        var footer=$('#'+tableId).datagrid('getFooterRows');
        var data1 =eval(footer);
       // alert(fileName);
        var nameList = new Array();
       // alert(fileId);
        // 载入title  
        if (typeof columns != 'undefined' && columns != '') {  
            $(columns).each(function (index) {
                tableString += '\n<thead><tr>';  
                if (typeof frozenColumns != 'undefined' && typeof frozenColumns[index] != 'undefined') {  
                    for (var i = 0; i < frozenColumns[index].length; ++i) {  
                        if (!frozenColumns[index][i].hidden) {  
                            tableString += '\n<th width="' + frozenColumns[index][i].width + '"';  
                            if (typeof frozenColumns[index][i].rowspan != 'undefined' && frozenColumns[index][i].rowspan > 1) {  
                                tableString += ' rowspan="' + frozenColumns[index][i].rowspan + '"';  
                                
                            }  
                            if (typeof frozenColumns[index][i].colspan != 'undefined' && frozenColumns[index][i].colspan > 1) {  
                                tableString += ' colspan="' + frozenColumns[index][i].colspan + '"';  
                            }  
                            if (typeof frozenColumns[index][i].field != 'undefined' && frozenColumns[index][i].field != '') {  
                                nameList.push(frozenColumns[index][i]);  
                            }  
                            tableString += '>' + frozenColumns[0][i].title + '</th>';  
                        }  
                    }  
                }  
                for (var i = 0; i < columns[index].length; ++i) {  
                    if (!columns[index][i].hidden) {
                        tableString += '\n<th width="' + columns[index][i].width + '"';  
                        if (typeof columns[index][i].rowspan != 'undefined' && columns[index][i].rowspan > 1) {  
                            tableString += ' rowspan="' + columns[index][i].rowspan + '"';  
                          
                        }  
                        if (typeof columns[index][i].colspan != 'undefined' && columns[index][i].colspan > 1) {  
                            tableString += ' colspan="' + columns[index][i].colspan + '"';  
                        }  
                        if (typeof columns[index][i].field != 'undefined' && columns[index][i].field != '') {  
                            nameList.push(columns[index][i]);  
                        }  
                        tableString += '>' + columns[index][i].title + '</th>';
                       
                    }  
                }  
                tableString += '\n</tr></thead>';  
            });  
        }  
        // 载入内容  
        var rows = $('#'+tableId).datagrid("getRows"); // 这段代码是获取当前页的所有行  
        for (var i = 0; i < rows.length; ++i) {  
            tableString += '\n<tr>';  
            for (var j = 0; j < nameList.length; ++j) {  
                var e = nameList[j].field.lastIndexOf('_0');  
                e=-1;
                tableString += '\n<td';  
                if (nameList[j].align != 'undefined' && nameList[j].align != '') {  
                    tableString += ' style="text-align:' + nameList[j].align + ';"';  
                   
                }  
                tableString += '>';  
                if (e + 2 == nameList[j].field.length) {  
                    tableString += rows[i][nameList[j].field.substring(0, e)];  
                  
                }  
                else { 
                    tableString += rows[i][nameList[j].field];  
                    
                }
                tableString += '</td>'; 
            }  
            tableString += '\n</tr>';  
        }  
        //fileId: 1.登降量   2.换乘量 3.速度  4.区域OD  5.线路OD  6.出行费用   7.出行距离  8.出行时间 9 线路长度  10 线网长度
       // alert(tableString);
       if(fileId==1){
    	   if(fileName=="登降量站点分布"){
    	        tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].stopname+':</td>'+'<td  style="text-align:right">'
    	              +data1[0].boarding_count+'</td>'+'<td  style="text-align:right">'
    	              +data1[0].boarding_scale+'</td>'+'</td>'+'<td  style="text-align:right">'
    	              +data1[0].alighting_count+'</td>'+'<td  style="text-align:right">'
    	              +data1[0].alighting_scale+'</td>'+'<td  style="text-align:right">'
    	              +data1[0].count+'</td>\n</tr>';
    	        }else{
    	        	 tableString+='\n<tr>\n<td/><td  style="text-align:right">      '+data1[0].period+':</td>'+'<td  style="text-align:right">'
    	             +data1[0].boarding_count+'</td>'+'<td  style="text-align:right">'
    	             +data1[0].boarding_scale+'</td>'+'</td>'+'<td  style="text-align:right">'
    	             +data1[0].alighting_count+'</td>'+'<td  style="text-align:right">'
    	             +data1[0].alighting_scale+'</td>\n</tr>';
    	        }
       }else if(fileId==2){
    	   
    	   if(fileName=="换乘量站点分布"){
   	        tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].stopname+':</td>'+'<td  style="text-align:right">'
   	              +data1[0].boarding_count+'</td>'+'<td  style="text-align:right">'
   	              +data1[0].boarding_scale+'</td>'+'</td>'+'<td  style="text-align:right">'
   	              +data1[0].alighting_count+'</td>'+'<td  style="text-align:right">'
   	              +data1[0].alighting_scale+'</td>\n</tr>';
   	        }else{
   	        	 tableString+='\n<tr>\n<td/><td  style="text-align:right">      '+data1[0].period+':</td>'+'<td  style="text-align:right">'
   	             +data1[0].boarding_count+'</td>'+'<td  style="text-align:right">'
   	             +data1[0].boarding_scale+'</td>'+'</td>'+'<td  style="text-align:right">'
   	             +data1[0].alighting_count+'</td>'+'<td  style="text-align:right">'
   	             +data1[0].alighting_scale+'</td>\n</tr>';
   	        }
       }else if(fileId==3){
    	  
      	        	 tableString+='\n<tr>\n<td/><td  style="text-align:right">      '+data1[0].stopname+':</td><td/><td/><td  style="text-align:right">'
      	             +data1[0].speed+'</td>\n</tr>';
      	        
       }else if(fileId==4){
    	   if(fileName=="区域OD行政区期望线"){
     	        tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].region+':</td>'+'<td  style="text-align:right">'
     	              +data1[0].count+'</td>'+'<td  style="text-align:right">'
     	              +data1[0].scale+'</td>\n</tr>';
     	        }else{
     	        	// alert(data1[0].region);
     	        	tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].region+':</td>'+'<td  style="text-align:right">'
   	              +data1[0].count+'</td>'+'<td  style="text-align:right">'
   	              +data1[0].scale+'</td>\n</tr>';
     	        }
      }
      else if(fileId==6){
   	           if(fileName=="出行费用样本分布"){
    	        tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].ride+':</td>'+'<td  style="text-align:right">'
    	              +data1[0].time+'</td>'+'<td  style="text-align:right">'
    	              +data1[0].scale+'</td>\n</tr>';
    	        }else  if(fileName=="乘车费用样本分布"){
    	        	 tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].ride+':</td>'+'<td  style="text-align:right">'
   	              +data1[0].time+'</td>'+'<td  style="text-align:right">'
   	              +data1[0].scale+'</td>\n</tr>';
    	    	        }
   	           
   	           else  if(fileName=="出行费用区域分布(发生)"){
    	        	tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
  	              +data1[0].endName+'</td>'+'<td  style="text-align:right">'
  	              +data1[0].avg+'</td>\n</tr>';
    	        }
   	        else  if(fileName=="出行费用区域分布(吸引)"){
	        	tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
	              +data1[0].endName+'</td>'+'<td  style="text-align:right">'
	              +data1[0].avg+'</td>\n</tr>';
	        }
   	     else  if(fileName=="出行费用区域分布(合计)"){
	        	tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
             +data1[0].endName+'</td>'+'<td  style="text-align:right">'
             +data1[0].avg+'</td>\n</tr>';
	        }
   	  
     }
      else if(fileId==7){
	           if(fileName=="乘车距离样本分布"){
	        tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].ride+':</td>'+'<td  style="text-align:right">'
	              +data1[0].time+'</td>'+'<td  style="text-align:right">'
	              +data1[0].scale+'</td>\n</tr>';
	        }else  if(fileName=="出行距离样本分布"){
		        tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].ride+':</td>'+'<td  style="text-align:right">'
	              +data1[0].time+'</td>'+'<td  style="text-align:right">'
	              +data1[0].scale+'</td>\n</tr>';
	        }
	           
	           else  if(fileName=="出行距离区域分布(发生)"){
	        	tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
	              +data1[0].endName+'</td>'+'<td  style="text-align:right">'
	              +data1[0].avg+'</td>\n</tr>';
	        }
	           else  if(fileName=="出行距离区域分布(吸引)"){
		        	tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
		              +data1[0].endName+'</td>'+'<td  style="text-align:right">'
		              +data1[0].avg+'</td>\n</tr>';
		        }
	           else  if(fileName=="出行距离区域分布(合计)"){
		        	tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
		              +data1[0].endName+'</td>'+'<td  style="text-align:right">'
		              +data1[0].avg+'</td>\n</tr>';
		        }
      } else if(fileId==8){
		           if(fileName=="出行时长样本分布"){
		        tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].ride+':</td>'+'<td  style="text-align:right">'
		              +data1[0].time+'</td>'+'<td  style="text-align:right">'
		              +data1[0].scale+'</td>\n</tr>';
		        }else if(fileName=="乘车时长样本分布"){
			        tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].ride+':</td>'+'<td  style="text-align:right">'
		              +data1[0].time+'</td>'+'<td  style="text-align:right">'
		              +data1[0].scale+'</td>\n</tr>';
		        }
		           else  if(fileName=="出行时长区域分布(发生)"){
		        	tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
		              +data1[0].endName+'</td>'+'<td  style="text-align:right">'
		              +data1[0].avg+'</td>\n</tr>';
		        }  else  if(fileName=="出行时长区域分布(吸引)"){
		        	tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
			              +data1[0].endName+'</td>'+'<td  style="text-align:right">'
			              +data1[0].avg+'</td>\n</tr>';
			        }  else  if(fileName=="出行时长区域分布(合计)"){
			        	tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
				              +data1[0].endName+'</td>'+'<td  style="text-align:right">'
				              +data1[0].avg+'</td>\n</tr>';
				        }
      		}
      
      else if(fileId==9) 
      {
			 tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].length+':</td>'+'<td  style="text-align:right">'
		             +data1[0].count+'</td>'+'<td  style="text-align:right">'
		             +data1[0].scale+'</td>\n</tr>';
			
      }else if(fileId==10)
      {
    	  if(fileName=="行政区线网长度"){
    		 
    		  tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].name+':</td>'+
  		            '<td  style="text-align:right">'
  		             +data1[0].lineNetLength+'</td>\n</tr>'; 
    	  }else{
    		 
    		  tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
 		             +data1[0].streetName+'</td>'+'<td  style="text-align:right">'
 		             +data1[0].lineNetLength+'</td>\n</tr>';
    	  }
 	  
      }else if(fileId==11)
      {
    	  
    	  if(fileName=="行政区线网密度"){
     		 
    		  tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].name+':</td>'+
  		            '<td  style="text-align:right">'
  		             +data1[0].density+'</td>\n</tr>'; 
    	  }else{
    		 
    		  tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
 		             +data1[0].streetName+'</td>'+'<td  style="text-align:right">'
 		             +data1[0].density+'</td>\n</tr>';
    	  }
    	  
      }else if(fileId==12)
      {
    	  if(fileName=="行政区重复系数"){
      		// alert(fileName);
    		  tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].name+':</td>'+
  		            '<td  style="text-align:right">'
  		             +data1[0].repeatCoefficient+'</td>\n</tr>'; 
    	  }else{
    		  //alert(fileName);
    		  tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
 		             +data1[0].streetName+'</td>'+'<td  style="text-align:right">'
 		             +data1[0].repeatCoefficient+'</td>\n</tr>';
    	  }
    	  
      }else if(fileId==13){
    	  tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].space+':</td>'+'<td  style="text-align:right">'
		             +data1[0].count+'</td>'+'<td  style="text-align:right">'
		             +data1[0].scale+'</td>\n</tr>';
      }else if (fileId==14)
      {
    	  if(fileName=="行政区运力保有量"){
       		// alert(fileName);
     		  tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].name+':</td>'+
   		            '<td  style="text-align:right">'
   		             +data1[0].busPossessoveNum+'</td>\n</tr>'; 
     	  }else{
     		//  alert(fileName);
     		  tableString+='\n<tr>\n<td/><td/><td style="text-align:right">'
  		             +data1[0].streetName+'</td>'+'<td  style="text-align:right">'
  		             +data1[0].busPossessoveNum+'</td>\n</tr>';
     	  }
    	  
      }else if (fileId==15)
      {
    	  if(fileName=="行政区站点覆盖率"){
       		// alert(fileName);
     		  tableString+='\n<tr>\n<td/><td style="text-align:right">      '+data1[0].districtName+':</td>'+
   		            '<td  style="text-align:right">'
   		             +data1[0].stopCoverageRate+'</td>\n</tr>'; 
     	  }else{
     		//  alert(fileName);
     		  tableString+='\n<tr>\n<td/><td/><td style="text-align:right">      '
  		             +data1[0].streetName+'</td>'+'<td  style="text-align:right">'
  		             +data1[0].stopCoverageRate+'</td>\n</tr>';
     		 alert(tableString);
     	  }
    	  
      }
       
      //写入jsp文件
        //$('#exportText').empty();
        //renxw 2016-11-04
        $('div#exportText').remove();
        $('#'+div).append(tableString);
        //导出excel
        if(fileNames==" "){
        fileName+=" "+formatDate();
        }else{
        	fileName=fileNames;
        }
        $('#tableData').tableExport({type:'excel',escape:'false',filename:fileName});
    } 
    
    function writeHiddenVal(flag){
    	if(flag){
    		$('#isYearQuery').val("true");
    	}else{
    		$('#isYearQuery').val("false");
    	}
    }
    
    function writeHtml(){
    	var str = '<input type="hidden" id="isYearQuery" />';
    	$('#queryTable').append(str);
    }
    
    function formatDate(){
        var date = new Date();
        return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
    }
      