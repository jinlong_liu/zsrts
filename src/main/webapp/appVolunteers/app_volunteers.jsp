<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>用户信息</title>

<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript"
	src="<%=path%>/js/jquery//jquery-3.1.0.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
<!-- alert样式 -->
<link rel="stylesheet" type="text/css"
	href="<%=path%>/third_party/sweet-alert/sweet-alert.css" />
<script type="text/javascript"
	src="<%=path%>/third_party/sweet-alert/sweet-alert.min.js"></script>
<!-- 分页时多选弹出框插件插件 -->
<script type="text/javascript"
	src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
<script type="text/javascript"
	src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
<!-- import js -->

<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>

<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/appVolunteers/app_volunteers.css" />
<script type="text/javascript"
	src="<%=path%>/js/appVolunteers/app_volunteers.js"></script>

<base>
</head>
<body style="width: 100%">
	<div class="topBoder">
		<label id='userStyle' style="width: 52px; margin-left: 10px;display: none">用户:</label><input
			id='userName' class='js-example-disabled-results' name='type'
			style='width: 100px; height: 30px; margin-top: 10px;display: none'> <label
			id='userStyle' style="width: 36px; margin-left: 15px;">性别:</label> <select
			id='sex' name='type' class='js-example-disabled-results'
			style='width: 70px; height: 30px; margin-top: 10px;'
			onchange='diArea(2)'>
			<option value=''>请选择</option>
			<option value='0'>男</option>
			<option value='1'>女</option>
		</select> <label id='age' style="width: 38px; margin-left: 15px;">年龄:</label><input
			id='starAge' class='js-example-disabled-results' name='type'
			style='width: 50px; height: 30px; margin-top: 10px;'> <label
			id='age' style="width: 20px;">至:</label> <input id='endAge'
			class='js-example-disabled-results' name='type'
			style='width: 50px; height: 30px; margin-top: 10px;'> <label
			id='userStyle' style="width: 92px; margin-left: 16px;">是否常住本地:</label>
		<select id='isLocal' name='type' class='js-example-disabled-results'
			style='width: 70px; height: 30px; margin-top: 10px;'
			onchange='diArea(2)'>
			<option value=''>请选择</option>
			<option value='0'>是</option>
			<option value='1'>否</option>
		</select> <label id='userStyle' style="width: 39px; margin-left: 15px;">职业:</label>
		<select id='work' name='type' class='js-example-disabled-results'
			style='width: 142px; height: 30px; margin-top: 10px;'
			onchange='diArea(2)'>
			<option value=''>请选择</option>
			<option value='1'>企业公司员工</option>
			<option value='2'>政府及事业单位人</option>
			<option value='3'>学生</option>
			<option value='4'>退休</option>
			<option value='5'>个体经营或自由职业</option>
			<option value='6'>农林牧鱼业人员</option>
			<option value='7'>无业</option>
			<option value='8'>其他</option>
		</select>
		<button type='button'
			style='border-radius: 6px; border: none; width: 60px; background: #06b370; color: white; margin-top: 5px; margin-left: 24.5px; height: 30px;'
			onclick='queryUser()'>查 询</button>
	</div>

	<div class="photo">
		<img alt="" src="../images/quan.png">&nbsp; 统计信息
	</div>
	<div class="quest" id="information"></div>

	<div class="margin-top-table">
		<table id="table">
		</table>
	</div>
	<script type="text/javascript">
		function search() {
			$('#table').bootstrapTable('destroy');
			/* radioVal = encodeURI(encodeURI(radioVal)); */
			$("#table").bootstrapTable({
				url : root + '/services/getListVolunteers',
				method : 'post',
				contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
				striped : true,
				pagination : true,
				pageSize : 10,
				pageNumber : 1,
				height : 550,
				pageList : [ 10, 20, 30 ],
				silent : true,
				sortStable : true,
				search : true,
				showColumns : true,
				sortOrder : true,
				sidePagination : "client",

				formatLoadingMessage : "请稍等,正在加载中。。。。。。。",
				columns : [ {
					field : 'number',
					title : '序号',
					width : 85,
					valign : 'middle',
					align : 'center'
				}, {
					field : "userName",
					title : '用户',
					width : 220,
					valign : 'middle',
					align : 'center',
					formatter:function (value,row,index){
						if(value.length>12){
							return value.substring(0,6)+"...";
						}
						return value;
					}
					//style:
				}, {
					field : 'sex',
					title : '性别',
					width : 85,
					valign : 'middle',
					align : 'center'
				}, {
					field : 'age',
					title : '年龄',
					width : 85,
					valign : 'middle',
					align : 'center'
				}, {
					field : 'appFormatWork',
					title : '职业',
					width : 160,
					valign : 'middle',
					align : 'center'
				}, {
					field : 'isLocal',
					title : '是否常住本地',
					width : 200,
					valign : 'middle',
					align : 'center'
				}, {
					field : 'atdus',
					title : '审核状态',
					width : 135,
					valign : 'middle',
					align : 'center'
				}, /*{
					field : 'costType',
					title : '用户类型',
					width : 135,
					valign : 'middle',
					align : 'center'
				}, */{
					field : 'isTravel',
					title : '有无出行',
					width : 150,
					valign : 'middle',
					align : 'center'
				}, {
					field : 'createDate',
					title : '注册时间',
					width : 165,
					valign : 'middle',
					align : 'center'
				}, {
					field : 'operate',
					title : '操作',
					align : 'center',

					formatter : operateFormatter,
					width : 130
				}

				]
			});
		}

		function operateFormatter(value, row, index) {
			return '<div style="line-height:47px;"><a style="display: none" href=\"volunteerForDetails.jsp?id='
					+ row.userId
					+ '\" onclick=\"details('
					+ row.userId
					+ ')\">'
					+ '<font color="#06b370"><img  src="../images/xq.png"/></font>'
					+ '</a>&nbsp;<a href=\"travelInformation.jsp?id='
					+ row.userId
					+ '\" onclick=\"trajectory('
					+ row.userId
					+ ')\">'
					+ '<font color="#06b370"><img  src="../images/gj.png"/></font></a></div>';
		}
		$().ready(function() {

			/* var radioVal=$('input:radio[name="stop_info_radio"]:checked').val(); */
			search();
			queryAppUserTotal();

			/*
             * $(".stop_info_radio").change(function(){
             * radioVal=$('input:radio[name="stop_info_radio"]:checked').val();
             * search(radioVal); });
             */

		});
		function queryUser() {
			var sex = $('#sex').find("option:selected").val();
			var work = $('#work').find("option:selected").val();
			var isLocal = $('#isLocal').find("option:selected").val();
			var userName = $('#userName').val();
			var starAge = $('#starAge').val();
			var endAge = $('#endAge').val();
			var type = "";
			if (isNaN(starAge)) {
				sweetAlert("No", "年龄请输入数字！", "warning");
				return;
			}
			if (isNaN(endAge)) {
				sweetAlert("No", "年龄请输入数字！", "warning");
				return;
			}
			if (starAge != "" && endAge == "") {
				sweetAlert("No", "年龄段不能只填写一个！", "warning");
				return;
			}
			if (starAge == "" && endAge != "") {
				sweetAlert("No", "年龄段不能只填写一个！", "warning");
				return;
			}

			if (Number(endAge) < Number(starAge)) {
				sweetAlert("NO", "结束年龄不能小于开始年龄！", "warning");
				return;
			}

			if (userName == "" && work == "" && starAge == "" && endAge == ""
					&& sex == "" && isLocal == "") {
				search();
				queryAppUserTotal();
			} else {
				$('#table').bootstrapTable('destroy');
				/* radioVal = encodeURI(encodeURI(radioVal)); */
				$("#table").bootstrapTable(
						{
							url : root + '/services/ConditionsQuery?userName='
									+ userName + '&sex=' + sex + '&starAge='
									+ starAge + '&endAge=' + endAge
									+ '&isLocal=' + isLocal + '&work=' + work
									+ '&type=' + type,
							method : 'post',
							contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
							striped : true,
							pagination : true,
							pageSize : 10,
							pageNumber : 1,
							height : 550,
							pageList : [ 10, 20, 30 ],
							silent : true,
							sortStable : true,
							search : true,
							showColumns : true,
							sortOrder : true,
							sidePagination : "client",

							formatLoadingMessage : "请稍等,正在加载中。。。。。。。",
							columns : [ {
								field : 'number',
								title : '序号',
								width : 90,
								valign : 'middle',
								align : 'center'
							}, {
								field : "userName",
								title : '用户名',
								width : 220,
								valign : 'middle',
								align : 'center',
								formatter:function (value,row,index){

									if(value.length>12){
										return value.substring(0,6)+"...";
									}

									return value;


								}

							}, {
								field : 'sex',
								title : '性别',
								width : 85,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'age',
								title : '年龄',
								width : 85,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'appFormatWork',
								title : '职业',
								width : 160,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'isLocal',
								title : '是否常住本地',
								width : 200,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'atdus',
								title : '审核状态',
								width : 140,
								valign : 'middle',
								align : 'center'
							},/* {
								field : 'costType',
								title : '用户类型',
								width : 135,
								valign : 'middle',
								align : 'center'
							}, */{
								field : 'isTravel',
								title : '有无出行',
								width : 150,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'createDate',
								title : '注册时间',
								width : 180,
								valign : 'middle',
								align : 'center'
							}, {
								field : 'operate',
								title : '操作',
								align : 'center',
								formatter : operateFormatter,
								width : 130
							}

							]
						});
				$.ajax({
					url : root + '/services/appUserStatistical?userName='
							+ userName + '&sex=' + sex + '&starAge=' + starAge
							+ '&endAge=' + endAge + '&isLocal=' + isLocal
							+ '&work=' + work + '&type=' + type,
					type : "POST",
					dataType : "json",
					success : function(result) {
						$("#information").empty();
						var obj = eval(result);
						var ul = document.createElement("ul");
						var litotalAppUserTotal = document.createElement("li");
						// var litotalAppUserTwenty = document.createElement("li");
						// var litotalAppUserThirty = document.createElement("li");
						// var litotalAppUserFifty = document.createElement("li");

						litotalAppUserTotal.style.cssText = "width:350px;margin-left:30px;font-size:14px;color:#474747;margin-top:5px;text-align:left;float:left;";
						// litotalAppUserTwenty.style.cssText = "width:350px;margin-left:50px;font-size:14px;color:#474747;margin-top:5px;text-align:left;float:left;";
						// litotalAppUserThirty.style.cssText = "width:350px;margin-left:30px;font-size:14px;color:#474747;margin-top:10px;text-align:left;float:left;";
						// litotalAppUserFifty.style.cssText = "width:350px;margin-left:50px;font-size:14px;color:#474747;margin-top:10px;text-align:left;float:left;";

						litotalAppUserTotal.innerText = "测试总量："
								+ obj[0].appUserTotal + "，未审核："
								+ obj[0].noAudit + "，通过：" + obj[0].yesAudit
								+ "，未通过：" + obj[0].noneAudit;
						// litotalAppUserTwenty.innerText = "测试一天："
						// 		+ (parseInt(obj[0].twentyNoAudit)
						// 				+ parseInt(obj[0].twentyYesAudit) + parseInt(obj[0].twentyNoneAudit))
						// 		+ "，未审核：" + obj[0].twentyNoAudit + "，通过："
						// 		+ obj[0].twentyYesAudit + "，未通过："
						// 		+ obj[0].twentyNoneAudit;
						// litotalAppUserThirty.innerText = "测试两天："
						// 		+ (parseInt(obj[0].thirtyNoAudit)
						// 				+ parseInt(obj[0].thirtyYesAudit) + parseInt(obj[0].thirtyNoneAudit))
						// 		+ "      未审核：" + obj[0].thirtyNoAudit + "，通过："
						// 		+ obj[0].thirtyYesAudit + "，未通过："
						// 		+ obj[0].thirtyNoneAudit;
						// litotalAppUserFifty.innerText = "测试一周："
						// 		+ (parseInt(obj[0].fiftyNoAudit)
						// 				+ parseInt(obj[0].fiftyYesAudit) + parseInt(obj[0].fiftyNoneAudit))
						// 		+ "，未审核：" + obj[0].fiftyNoAudit + "，通过："
						// 		+ obj[0].fiftyYesAudit + "，未通过："
						// 		+ obj[0].fiftyNoneAudit;
						ul.appendChild(litotalAppUserTotal);
						// ul.appendChild(litotalAppUserTwenty);
						// ul.appendChild(litotalAppUserThirty);
						// ul.appendChild(litotalAppUserFifty);
						document.getElementById("information").appendChild(ul);
					}
				});
			}
		}

		function queryAppUserTotal() {
			$.ajax({
				url : root + "/services/appUserStatistical",
				type : "POST",
				dataType : "json",
				success : function(result) {
					$("#information").empty();
					var obj = eval(result);
					var ul = document.createElement("ul");
					var litotalAppUserTotal = document.createElement("li");
					// var litotalAppUserTwenty = document.createElement("li");
					// var litotalAppUserThirty = document.createElement("li");
					// var litotalAppUserFifty = document.createElement("li");

					litotalAppUserTotal.style.cssText = "width:350px;margin-left:30px;font-size:14px;color:#474747;margin-top:5px;text-align:left;float:left;";
					// litotalAppUserTwenty.style.cssText = "width:350px;margin-left:50px;font-size:14px;color:#474747;margin-top:5px;text-align:left;float:left;";
					// litotalAppUserThirty.style.cssText = "width:350px;margin-left:30px;font-size:14px;color:#474747;margin-top:10px;text-align:left;float:left;";
					// litotalAppUserFifty.style.cssText = "width:350px;margin-left:50px;font-size:14px;color:#474747;margin-top:10px;text-align:left;float:left;";

					litotalAppUserTotal.innerText = "测试总量："
							+ obj[0].appUserTotal + "，未审核：" + obj[0].noAudit
							+ "，通过：" + obj[0].yesAudit + "，未通过："
							+ obj[0].noneAudit;
					// litotalAppUserTwenty.innerText = "测试一天："
					// 		+ (parseInt(obj[0].twentyNoAudit)
					// 				+ parseInt(obj[0].twentyYesAudit) + parseInt(obj[0].twentyNoneAudit))
					// 		+ "，未审核：" + obj[0].twentyNoAudit + "，通过："
					// 		+ obj[0].twentyYesAudit + "，未通过："
					// 		+ obj[0].twentyNoneAudit;
					// litotalAppUserThirty.innerText = "测试两天："
					// 		+ (parseInt(obj[0].thirtyNoAudit)
					// 				+ parseInt(obj[0].thirtyYesAudit) + parseInt(obj[0].thirtyNoneAudit))
					// 		+ "，未审核：" + obj[0].thirtyNoAudit + "，通过："
					// 		+ obj[0].thirtyYesAudit + "，未通过："
					// 		+ obj[0].thirtyNoneAudit;
					// litotalAppUserFifty.innerText = "测试一周："
					// 		+ (parseInt(obj[0].fiftyNoAudit)
					// 				+ parseInt(obj[0].fiftyYesAudit) + parseInt(obj[0].fiftyNoneAudit))
					// 		+ "，未审核：" + obj[0].fiftyNoAudit + "，通过："
					// 		+ obj[0].fiftyYesAudit + "，未通过："
					// 		+ obj[0].fiftyNoneAudit;
					ul.appendChild(litotalAppUserTotal);
					// ul.appendChild(litotalAppUserTwenty);
					// ul.appendChild(litotalAppUserThirty);
					// ul.appendChild(litotalAppUserFifty);
					document.getElementById("information").appendChild(ul);
				}
			});
		}

	</script>
</body>
</html>
