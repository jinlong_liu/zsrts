<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
	String authDegree = request.getSession().getAttribute("authDegree").toString();
	String[] array = authDegree.split(",");
%>


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>出行信息</title>

	<jsp:include page="/js/inc.jsp"></jsp:include>
	<%-- <script type="text/javascript"
        src="<%=path%>/js/jquery//jquery-3.1.0.min.js"></script> --%>
	<link rel="stylesheet" type="text/css"
		  href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css"
		  href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />

	<link rel="stylesheet" type="text/css"
		  href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
	<link rel="stylesheet" type="text/css"
		  href="<%=path%>/third_party/sweet-alert/sweet-alert.css" />


	<!-- 分页时多选弹出框插件插件 -->
	<script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
	<script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
	<!-- import js -->

	<script type="text/javascript"
			src="<%=path%>/js/jquery/jquery-1.11.1.min.js"></script>
	<script type="text/javascript"
			src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<script type="text/javascript"
			src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>
	<script type="text/javascript"
			src="<%=path%>/third_party/sweet-alert/sweet-alert.min.js"></script>

	<!-- 时间控件     -->
	<link rel="stylesheet" type="text/css"
		  href="<%=path%>/third_party/bootstrap/datetimepicker/css/bootstrap-datetimepicker.css" />
	<script type="text/javascript"
			src="<%=path%>/third_party/bootstrap/datetimepicker/js/moment-with-locales.js"></script>
	<script type="text/javascript"
			src="<%=path%>/third_party/bootstrap/datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript"
			src="<%=path%>/third_party/bootstrap/locales/bootstrap-datepicker.zh-CN.js"></script>


	<link rel="stylesheet" type="text/css"
		  href="<%=path%>/css/appVolunteers/travelInformation.css" />
	<script type="text/javascript"
			src="<%=path%>/js/appVolunteers/travelInformation.js"></script>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport"
		  content="initial-scale=1.0, user-scalable=no, width=device-width">
	<title>折线</title>
	<link rel="stylesheet"
		  href="http://cache.amap.com/lbs/static/main1119.css" />
	<script
			src="http://webapi.amap.com/maps?v=1.3&key=4bf3569f2e3041ab00ec6bcf97d2778f&&plugin=AMap.ToolBar"></script>
	<script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
	<base>
</head>
<body style="overflow: hidden">
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	 aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width: 420px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">选择审核状态</h4>
			</div>
			<div id="selectDiv">
				</br> <label id="laberLeft" style="margin-left: 108px; width: 72px">选择状态：</label>
				<input type="radio" name="chSelect" value="1">&nbsp;通过
				&nbsp; &nbsp; <input type="radio" name="chSelect" value="0">&nbsp;不通过
				&nbsp; &nbsp;



				<!-- <input type="hidden" id="checkSave" value="0"> -->
				<!--  <input type="hidden" id="id" value=" ">
                                  <input type="hidden" id="oldName" value=" "> -->

			</div>

			<div id="testdiv"></div>
			<div class="modal-footer">
				<!-- 						<button type="button"  class="btn btn-primary" style="margin-right: 45%" onclick="save()" >确认审核</button>
                        <button type="button" class="btn btn-default" style="margin-right: 43%"data-dismiss="modal">取消审核</button> -->
				<button type="button" class="btn btn-primary" onclick="audute()">确认</button>
				<button type="button" class="btn btn-default"
						style="margin-right: 35%" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情模态框	 -->
<div class="modal fade" id="myModall" tabindex="-1" role="dialog"
	 aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width: 420px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<span>详细信息</span>
			</div>
			<div id="testdivOne" class="userDetails"></div>
			<div class="modal-footer">
				<!-- 						<button type="button"  class="btn btn-primary" style="margin-right: 45%" onclick="save()" >确认审核</button>
                        <button type="button" class="btn btn-default" style="margin-right: 43%"data-dismiss="modal">取消审核</button> -->

				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</div>
</div>

<input id="userId" value="${param.id}" type="hidden" />
<!-- 日期和时间：<input type="datetime-local" name="user_date" value="2015/03/27 10:41" id="user_date"/> -->
<%-- <nav class="navbar navbar-default" role="navigation" style="margin-left:20px;margin-top:10px;">
<div class="container-fluid" >

<div>
    <ul class="nav navbar-nav">
        <li id="back" ><a href="app_volunteers.jsp">返回</a></li>
        <li id="travel" style="margin-left: 18px;"><a href="#" onclick='viewHistogram()'>出行轨迹</a></li>
        <li id="records" class="active"><a href="#" onclick='searchByareaNumber()'>出行记录</a></li>
    </ul>
</div>
  <div id="adtus"  style=" width:200px; margin-left: 272px;margin-top: 15px; color:black"></div>
</div>
<c:forEach var="auth" items="<%=array  %>">
  <c:if test="${auth == 10}">
 <button type="button" class="btn btn-primary" id="auditButton"  style=" heigth:27px;  margin-left: 510px;
  margin-top: -42px; data-toggle="modal" data-target="#myModal" onclick="updateAutude()">
    审核
    <!-- 修改参数 -->
    </button>
        </c:if>
</c:forEach>
</nav> --%>
<!--  <div class="font-registeredStart">时间段：自</div>
                    <div class='input-group date' id='begin-datetimepicker'>
                        <input type='text' class="form-control" onclick="reviewBeginTime()"
                            id="begin-date">
                        <div class="input-group-addon" id="input-group-addon"></div>

                    </div>
                    <div class="font-registeredEnd">至</div>
                    <div class='input-group date' id='end-datetimepicker'>
                        <input type='text' class="form-control" onclick="reviewEndTime()"
                            id="end-date">
                        <div class="input-group-addon" id="input-group-addon"></div>

                    </div>
    -->
<div class="title">
	<a href="app_volunteers.jsp"> <img src="../images/rectangular.png">
	</a> &nbsp;
	<div class="radioButton">
		<input type="radio" name="chSelect" checked="checked"
			   onclick='viewHistogram()'>&nbsp;<label id='userStyle'>出行轨迹</label>
		<input type="radio" name="chSelect" style="margin-left: -176px"
			   onclick='searchByareaNumber()'>&nbsp;<label id='userStyle'>出行记录</label>
	</div>
	<div id="adtus"
		 style="width: 200px; margin-left: 272px; margin-top: -56px; color: black"></div>
	<%--	<c:forEach var="auth" items="<%=array  %>">--%>
	<%--	<c:if test="${auth == 10}">--%>
	<div>
		<button type="button" class="btn btn-primary" id="auditButton"
				style="heigth: 27px; margin-left: 420px; margin-top: -42px; background: #06b370;"
				modal" data-target="#myModal" onclick="updateAutude()">
		审核</button>
		<%--		</c:if>--%>
		<%--		</c:forEach>--%>
	</div>


	<div id="timeK">
		<div class='col-sm-6'
			 style="width: 260px; falot: left; margin-left: 660; margin-top: -43px;display: none">
			<div class="form-group" >
				<div class="font-registeredStart"
					 style="float: left; margin-top: -7px">时间：</div>
				<div class='input-group date' id='datetimepickertime'>
					<input type='text' class="form-control" name="time"
						   onblur="tomtk()" /> <span class="input-group-addon"> <span
						class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-primary"
				style="background: #06b370; margin-top: -43px;display: none"
				onclick="selectTravel()">查询</button>
	</div>
	<div class="row" id="appTime"
		 style="margin-left: 470px; margin-top: -43px; " >
		<div class='col-sm-6' style="width: 260px; margin-left: 25px;display: none">
			<div class="form-group">
				<div class="font-registeredStart"
					 style="float: left; margin-top: -8px">自：</div>
				<div class='input-group date' id='datetimepicker1'>
					<input type='text' class="form-control" name="stime"
						   onblur="BeginTime()" /> <span class="input-group-addon"> <span
						class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<div class='col-sm-6' style="width: 260px; display: none">
			<div class="form-group">

				<div class="font-registeredEnd"
					 style="float: left; margin-top: -8px">至：</div>
				<div class='input-group date' id='datetimepicker2'>
					<input type='text' class="form-control" name="etime"
						   onblur="EndTime()" /> <span class="input-group-addon"> <span
						class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-primary"
				style="background: #06b370;position: relative;left: 50px;top: 3px; display: none" onclick="query()"  >查询</button>
	</div>
	<div class="margin-top-table" style="width: 80%; height: 565px;">
		<table  id="table">
		</table>
	</div>

	<!--  地图显示 -->
	<div id="container"
		 style="width: 95%; height: 82%; margin-top: 68px; margin-left: 3.5px"></div>
	<!-- 时间加载框 -->
	<script type="text/javascript">
		$(document).ready(function() {
			query();
			// selectTravel();
		})

		function search() {
			var userId = $('#userId').val();
			$('#table').bootstrapTable('destroy');
			/* radioVal = encodeURI(encodeURI(radioVal)); */
			$("#table").bootstrapTable({
				url : root + '/services/getTravelInformation?userId=' + userId,
				method : 'post',
				contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
				striped : true,
				pagination : true,
				pageSize : 10,
				pageNumber : 1,
				height : 550,
				pageList : [ 10, 20, 30 ],
				silent : true,
				sortStable : true,
				search : true,
				showColumns : true,
				sortOrder : true,
				sidePagination : "client",
				formatLoadingMessage : "请稍等,正在加载中。。。。。。。",
				columns : [
					/*
                     * { checkbox:true, valign:'middle', align:'center' },
                     */
					{
						field : 'number',
						title : '序号',
						width : 120,
						valign : 'middle',
						align : 'center'
					}, {
						field : "startAddress",
						title : '出发地点',
						width : 120,
						valign : 'middle',
						align : 'center'

					}, {
						field : 'starDate',
						title : '出发时间',
						width : 130,
						valign : 'middle',
						align : 'center'
					}, {
						field : 'endAddress',
						title : '到达地点',
						width : 130,
						valign : 'middle',
						align : 'center'
					}, {
						field : 'endDate',
						title : '到达时间',
						width : 130,
						valign : 'middle',
						align : 'center'
					}, {
						field : 'tripMode',
						title : '出行方式',
						width : 130,
						valign : 'middle',
						align : 'center'
					}, {
						field : 'tripObjective',
						title : '出行目的',
						width : 130,
						valign : 'middle',
						align : 'center'
					}]

			});
		}
		// 预加载
		$().ready(function() {
			/* var radioVal=$('input:radio[name="stop_info_radio"]:checked').val(); */
			// search();
			selectAutude();
			viewHistogram();
			/*
             * $(".stop_info_radio").change(function(){
             * radioVal=$('input:radio[name="stop_info_radio"]:checked').val();
             * search(radioVal); });
             */
			$('#myModall').on('hidden.bs.modal', function() {

				$("#testdivOne").empty();
			});
		});
		// function BeginTime() {
		// 	var stime = $('input[name="stime"] ').val();
		// 	var etime = $('input[name="etime"] ').val();
		// 	if (stime == "") {
		// 		sweetAlert("No", "开始时间不能为空", "warning");
		// 		return;
		// 	}
		// }
		// function EndTime() {
		// 	var stime = $('input[name="stime"] ').val();
		// 	var etime = $('input[name="etime"] ').val();
		// 	/*
		// 	 * var starTime = stime.substring(start, end); var endTime =
		// 	 * etime.substring(start, end); alert(starTime); alert(endTime);
		// 	 */
		// 	if (etime == "") {
		// 		sweetAlert("No", "结束时间不能为空", "warning");
		// 		return;
		// 	}
		// 	if (stime > etime) {
		// 		sweetAlert("No", "开始时间不能大于结束时间", "warning");
		// 		return;
		// 	}
		// }
		function query() {
			var lineArr = [];// 放入地图坐标
			var center = [];// 放入中心坐标
			var star = [];// 放入开始坐标
			var end = [];// 放入结束坐标
			var morningLineArr = [];// 放入12点以前的坐标
			var afternoonLineArr = [];// 放入12点以后的坐标
			var userId = $('#userId').val();
			var stime = $('input[name="stime"] ').val();
			var etime = $('input[name="etime"] ').val();
			// if (stime == "") {
			// 	sweetAlert("", "开始时间不能为空", "warning");
			// 	return;
			// }
			// if (etime == "") {
			// 	sweetAlert("", "结束时间不能为空", "warning");
			// 	return;
			// }
			// if (stime > etime) {
			// 	sweetAlert("", "开始时间不能大于结束时间", "warning");
			// 	return;
			// }
			parm = {
				stime : stime,
				userId : userId,
				etime : etime
			};
			$.ajax({
				url : root + '/services/gettrajectory',
				dataType : "json",
				type : "post",
				data : parm,
				success : function(result) {
					obj = eval(result);
					if (obj[0].lon == 1) {
						sweetAlert("No", "这时间段没有出行轨迹", "warning");
						var map = new AMap.Map('container', {
							resizeEnable : true,
							zoom : 11,
							center : [ 113.65, 34.76 ]
						});
						return;
					} else if (obj[0].lon == 2 || obj[0].lon == 3) {
						sweetAlert("No", "这时间段没有出行轨迹", "warning");
						var map = new AMap.Map('container', {
							resizeEnable : true,
							zoom : 11,
							center : [ 113.65, 34.76 ]
						});
						return;
					}
					/*
                     * alert(obj.length); alert(obj[0].lon);
                     */
					center.push(obj[0].lon);
					center.push(obj[0].lat);
					star.push(obj[0].lon);
					star.push(obj[0].lat);
					end.push(obj[obj.length - 1].lon);
					end.push(obj[obj.length - 1].lat);
					for (var i = 0; i < obj.length; i++) {
						if (obj[i].name == 3) {
							var args = [];
							args.push(obj[i].lon);
							args.push(obj[i].lat);
							lineArr.push(args);
						}
						if (obj[i].name == 1) {
							var morning = [];
							morning.push(obj[i].lon);
							morning.push(obj[i].lat);
							morningLineArr.push(morning);
						}
						if (obj[i].name == 2) {
							var afternoon = [];
							afternoon.push(obj[i].lon);
							afternoon.push(obj[i].lat);
							afternoonLineArr.push(afternoon);
						}
					}
					// alert(center);
					var map = new AMap.Map('container', {
						resizeEnable : true,
						center : center,
						zoom : 20
					});
					var toolBar = new AMap.ToolBar({
						visible: true
					});

					map.addControl(toolBar);
					// 添加点标记起始位置，并使用自己的icon
					new AMap.Marker(
							{
								map : map,
								position : star,
								icon : new AMap.Icon(
										{
											size : new AMap.Size(50, 50), // 图标大小
											image : "http://webapi.amap.com/theme/v1.3/markers/n/start.png",
											imageOffset : new AMap.Pixel(0, -1)
										})
							});
					new AMap.Marker(
							{
								map : map,
								position : end,
								icon : new AMap.Icon(
										{
											size : new AMap.Size(50, 50), // 图标大小
											image : "http://webapi.amap.com/theme/v1.3/markers/n/end.png",
											imageOffset : new AMap.Pixel(0, -1)
										})
							});
					/* alert(lineArr); */
					if (obj[0].name == 3) {
						var polyline = new AMap.Polyline({
							path : lineArr, // 设置线覆盖物路径
							strokeColor : "#3366FF", // 线颜色
							strokeOpacity : 1, // 线透明度
							strokeWeight : 2, // 线宽
							strokeStyle : "solid", // 线样式
							strokeDasharray : [ 10, 5 ]
							// 补充线样式
						});
						polyline.setMap(map);
					} else {
						// 12点以前的线
						var polylineMorning = new AMap.Polyline({
							path : morningLineArr, // 设置线覆盖物路径
							strokeColor : "#EE4000", // 线颜色
							strokeOpacity : 1, // 线透明度
							strokeWeight : 4, // 线宽
							strokeStyle : "solid", // 线样式
							strokeDasharray : [ 10, 5 ]
							// 补充线样式
						});
						polylineMorning.setMap(map);
						// 12点以后的线
						var polylineAfternoon = new AMap.Polyline({
							path : afternoonLineArr, // 设置线覆盖物路径
							strokeColor : "#3366FF", // 线颜色
							strokeOpacity : 1, // 线透明度
							strokeWeight : 2, // 线宽
							strokeStyle : "solid", // 线样式
							strokeDasharray : [ 10, 5 ]
							// 补充线样式
						});
						polylineAfternoon.setMap(map);
					}

					//add by renxianwei 20170629
					//地图自动缩放到合适范围
					map.setFitView();
				}
			});
		}
		// 查询出行信息表格
		function selectTravel() {

			var time = $('input[name="time"] ').val();
			var userId = $('#userId').val();
			// if (time == "") {
			// 	sweetAlert("No", "时间不能为空", "warning");
			// 	return;
			// }
			// alert("1");
			$.ajax({
				url : root + '/services/getTravelStatus?userId=' + userId + '&time='
						+ time,
				type : "POST",
				dataType : "json",
				async : false,
				data : {
					userId : userId,
					time : time
				},
				success : function(result) {
					var obj = eval(result);
					var status = obj[0].status;
					if (status == 2) {
						sweetAlert("未出行备注", obj[0].note, "warning");
						return;
					} else if (status == '') {
						sweetAlert("No", "此用户暂未填写出行信息", "error");
					}
				}
			});
			$('#table').bootstrapTable('destroy');
			/* radioVal = encodeURI(encodeURI(radioVal)); */
			$("#table").bootstrapTable({
				url : root + '/services/getTravel?userId=' + userId + '&time=' + time,
				method : 'post',
				contentType : 'application/x-www-form-urlencoded;charset=UTF-8',
				striped : true,
				pagination : true,
				pageSize : 10,
				pageNumber : 1,
				height : 550,
				pageList : [ 10, 20, 30 ],
				silent : true,
				sortStable : true,
				search : true,
				showColumns : true,
				sortOrder : true,
				sidePagination : "client",
				formatLoadingMessage : "请稍等,正在加载中。。。。。。。",
				columns : [
					/*
                     * { checkbox:true, valign:'middle', align:'center' },
                     */
					{
						field : 'number',
						title : '序号',
						width : 120,
						valign : 'middle',
						align : 'center'
					}, {
						field : "startAddress",
						title : '出发地点',
						width : 120,
						valign : 'middle',
						align : 'center'

					}, {
						field : 'starDate',
						title : '出发时间',
						width : 130,
						valign : 'middle',
						align : 'center'
					}, {
						field : 'endAddress',
						title : '到达地点',
						width : 130,
						valign : 'middle',
						align : 'center'
					}, {
						field : 'endDate',
						title : '到达时间',
						width : 130,
						valign : 'middle',
						align : 'center'
					}, {
						field : 'tripMode',
						title : '出行方式',
						width : 130,
						valign : 'middle',
						align : 'center'
					}, {
						field : 'tripObjective',
						title : '出行目的',
						width : 130,
						valign : 'middle',
						align : 'center'
					}

					/*
                     * { field:'formatWork', title:'职业', width:300, valign:'middle',
                     * align:'center' },
                     */
				]

			});
		}
		function operateFormatter(value, row, index) {

			return '<div style="width: 100%; margin: 22px auto;"><a href=\"#;\" onclick=\"userDetails('
					+ row.id
					+ ')\">'
					+ '<font color="#06b370"><img  src="../images/xq.png" /></font>';
		}

		function searchByareaNumber() {
			$('#travel').removeClass("active");
			$('#records').addClass("active");
			/* $('#container').empty(); */
			document.getElementById("container").style.display = "none";
			document.getElementById("appTime").style.display = "none";
			document.getElementById("timeK").style.display = "";
			selectTravel();
		}
		function userDetails(id) {
			/*
             * var time = $('input[name="time"] ').val(); var userId =
             * $('#userId').val();
             */

			$('#myModall').modal('show');
			$.ajax({
				url : root + '/services/detailsInformation',
				type : "POST",
				dataType : "json",
				data : {
					id : id
				},
				success : function(result) {
					alert("操作");
					document.getElementById("testdivOne").innerHTML = "";
					var obj = eval(result);
					var ul = document.createElement("ul");
					var li = document.createElement("li");
					var listartAddress = document.createElement("li");
					var listarDate = document.createElement("li");
					var liendAddress = document.createElement("li");
					var liendDate = document.createElement("li");
					var litripMode = document.createElement("li");
					var litripObjective = document.createElement("li");
					var lichildren = document.createElement("li");
					var lisendTime = document.createElement("li");
					var liaddress = document.createElement("li");

					li.style.cssText = "margin-left:30px;font-size:18px;color:#FFFFFF;text-align:left;";
					listartAddress.style.cssText = "margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left;";
					listarDate.style.cssText = "margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:30px;text-align:left";
					liendAddress.style.cssText = "margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left;";
					liendDate.style.cssText = "margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
					litripMode.style.cssText = "margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
					litripObjective.style.cssText = "margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
					lichildren.style.cssText = "margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
					lisendTime.style.cssText = "margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";
					liaddress.style.cssText = "margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left";

					li.innerText = "详细出行信息";
					listartAddress.innerText = "出发地点：" + obj[0].startAddress;
					listarDate.innerText = "出发时间：" + obj[0].starDate;
					liendAddress.innerText = "到达地点：" + obj[0].endAddress;
					liendDate.innerText = "到达时间：" + obj[0].endDate;
					litripMode.innerText = "出行方式：" + obj[0].tripMode;
					litripObjective.innerText = "出行目的：" + obj[0].tripObjective;
					lichildren.innerText = "是否接送小孩：" + obj[0].children;
					if (obj[0].children != '否') {
						lisendTime.innerText = obj[0].sendTime;
						liaddress.innerText = obj[0].address;
					}
					ul.appendChild(li);
					ul.appendChild(listartAddress);
					ul.appendChild(listarDate);
					ul.appendChild(liendAddress);
					ul.appendChild(liendDate);
					ul.appendChild(litripMode);
					ul.appendChild(litripObjective);
					ul.appendChild(lichildren);
					ul.appendChild(liaddress);
					ul.appendChild(lisendTime);
					document.getElementById("testdivOne").appendChild(ul);
				}
			});
		}

		function viewHistogram() {
			$('#records').removeClass("active");
			$('#travel').addClass("active");
			$('#table').bootstrapTable('destroy');
			document.getElementById("container").style.display = "";
			document.getElementById("appTime").style.display = "";
			document.getElementById("timeK").style.display = "none";
			var map = new AMap.Map('container', {
				resizeEnable : true,
				zoom : 11,
				center : [ 120.57,30.03 ]
			});

			var toolBar = new AMap.ToolBar({
				visible: true
			});

			map.addControl(toolBar);
			query();
		}

		// 改变审核状态模态框
		function updateAutude() {
			$('#myModal').modal('show');

		}
		// 查询审核状态
		function selectAutude() {
			var userId = $('#userId').val();
			$.ajax({
				url : root + "/services/selectAppaudit",
				type : "POST",
				dataType : "json",
				data : {
					userId : userId
				},
				success : function(result) {
					var adtus = eval(result)[0].adtus;
					if (adtus != "") {
						document.getElementById("adtus").innerHTML = '审核状态：' + adtus;
					} else {
						document.getElementById("adtus").innerHTML = '审核状态：';
					}
				}
			});

		}
		// 审核状态更改保存
		function audute() {
			var userId = $('#userId').val();
			var audit = $('input[name="chSelect"]:checked ').val();
			$.ajax({
				url : root + "/services/udateStatus",
				type : "POST",
				dataType : "json",
				data : {
					userId : userId,
					audit : audit
				},
				success : function(result) {
					var back = eval(result)[0].back;
					if (back == 1) {
						$('#myModal').modal('hide');
						art.dialog.tips('审核完成！', 1.5);
						location.reload();
					} else if (back == 0) {
						sweetAlert("抱歉!!!", "审核失败，请重新点击审核", "error");
					} else if (back == 2) {
						$('#myModal').modal('hide');
						sweetAlert("抱歉!!!", "出行信息已完成两次审核,如还需再次审核，请联系管理员", "error");
					}else if (back == 3) {
						$('#myModal').modal('hide');
						sweetAlert("抱歉!!!", "审核信息未正常生成，无法审核，请联系管理员", "error");
					} else {
						$('#myModal').modal('hide');
						sweetAlert("抱歉!!!", "系统错误", "error");
					}
				}
			});
		}
		$(function () {
			var picker1 = $('#datetimepicker1').datetimepicker({
				format: 'YYYY-MM-DD HH:mm:ss',

				locale: moment.locale('zh-cn'),
				//minDate: '2016-7-1'
			});
			var picker3 = $('#datetimepickertime').datetimepicker({
				format: 'YYYY-MM-DD',
				locale: moment.locale('zh-cn'),
				//minDate: '2016-7-1'
			});
			var picker2 = $('#datetimepicker2').datetimepicker({
				format: 'YYYY-MM-DD HH:mm:ss',
				locale: moment.locale('zh-cn')
			});
		});

	</script>
</body>

</html>