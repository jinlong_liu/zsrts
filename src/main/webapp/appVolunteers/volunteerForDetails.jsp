<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>志愿者信息详情</title>
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript"
	src="<%=path%>/js/jquery//jquery-3.1.0.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />

<!-- 分页时多选弹出框插件插件 -->
<script type="text/javascript"
	src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
<script type="text/javascript"
	src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
<!-- import js -->

<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>

<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/appVolunteers/volunteerForDetails.css" />
<script type="text/javascript"
	src="<%=path%>/js/appVolunteers/volunteerForDetails.js"></script>

<base>

</head>
<body>
	<input id="userId" value="${param.id}" type="hidden" />
	<div class=title>
		<a href="app_volunteers.jsp"> <img src="../images/rectangular.png">
		</a> &nbsp; 志愿者信息详情
	</div>
	<div class=left></div>
	<script type="text/javascript">
		window.onload = function() {
			var userId = $('#userId').val();
			// 家庭基本信息显示
			$.ajax({
				url : root + "/services/getListHomeAddress",
				type : "POST",
				dataType : "json",
				data : {
					userId : userId
				},
				success : function(result) {
					var wid = document.body.clientWidth;// 获取当前平米分辨率
					/* alert(eval(result)); */
					var total = eval(result).length;

					var divs = document.createElement("div");
					divs.style.cssText = "width:" + (wid - 25)
							+ "px;height:60px; solid black;"
							+ "border-radius: 5px;";
					var divHome = document.createElement("div");
					var divHomea = document.createElement("div");
					var imgNode = document.createElement('img');
					imgNode.setAttribute('src', '../images/quan.png');
					divHomea.appendChild(imgNode);
					divHome.innerText = "家庭基本信息";
					divHome.style.cssText = "width:150px;height:25px;margin-left:56px;"
							+ "font-size:16px; color:#06b370;  ";
					divHomea.style.cssText = "float:left;margin-top:4px;"
							+ "margin-left:8px;";
					var divw = document.createElement("div");
					divw.style.cssText = "width:1056;px;height:20px;margin-top:15px;"
							+ "margin-left:39px;font-size:14px;";
					var ul = document.createElement("ul");
					var liHomeAddress = document.createElement("li");
					var liHomesCount = document.createElement("li");
					var liCarCount = document.createElement("li");
					var liElectricBikes = document.createElement("li");
					liHomeAddress.style.cssText = "float:left;margin-left:24px;font-size:14px;color:#474747;width:585px;margin-top:-2px;text-align:left;";
					liHomesCount.style.cssText = "float:left;margin-left:18px;font-size:14px;color:#474747;width:108px;margin-top:-2px;text-align:left;";
					liCarCount.style.cssText = "float:left;margin-left:24px;font-size:14px;color:#474747;width:128px;margin-top:-2px;overflow:hidden;text-align:left;";
					liElectricBikes.style.cssText = "float:left;margin-left:24px;font-size:14px;color:#474747;width:135px;margin-top:-2px;overflow:hidden;text-align:left;;";
					liHomeAddress.innerText = "家庭住址："
							+ eval(result)[0].homeAddress;
					liHomesCount.innerText = "家庭成员数："
							+ eval(result)[0].homesCount;
					liCarCount.innerText = "家庭汽车数量：" + eval(result)[0].carCount;
					liElectricBikes.innerText = "家庭电动车数量："
							+ eval(result)[0].electricBikes;
					ul.appendChild(liHomesCount);
					ul.appendChild(liCarCount);
					ul.appendChild(liElectricBikes);
					ul.appendChild(liHomeAddress);
					divs.appendChild(divHomea);
					divs.appendChild(divHome);
					divw.appendChild(ul);
					divs.appendChild(divw);
					document.body.appendChild(divs);
					// 个人基本信息结果展示
					$
							.ajax({
								url : root + "/services/getListPersonal",
								type : "POST",
								dataType : "json",
								data : {
									userId : userId
								},
								success : function(result) {
									var total = eval(result).length;
									var wid = document.body.clientWidth;
									var hig = 120 * total + (total - 1) * 50
											+ 80;
									var div = document.createElement("div");
									div.style.cssText = "width:1100px;height:"
											+ hig + "px;" + "margin-top:30px;";
									var divo = document.createElement("div");
									var divxo = document.createElement("div");
									var imgNode = document.createElement('img');
									imgNode.setAttribute('src',
											'../images/quan.png');
									divxo.appendChild(imgNode);
									divo.innerText = "个人基本信息";
									divo.style.cssText = "width:140px;height:20px;margin-top:5px;margin-left:55px;"
											+ "font-size:16px; color:#06b370;";
									divxo.style.cssText = "margin-top:3px;"
											+ "margin-left:10px; 	 float:left;";
									div.appendChild(divxo);
									div.appendChild(divo);
									var divground = document
											.createElement("div");
									divground.style.cssText = "float:left;margin-left:12px;width:345px;"
											+ "height:355px;margin-top:20px;";
									divground.style.backgroundImage = 'url(../images/home.png)';
									var divbackground = document
											.createElement("div");
									divbackground.style.cssText = "float:left;margin-left:8px;width:345px;"
											+ "height:355px;margin-top:20px;";
									// divbackground.style.backgroundImage = 'url(../images/bu.png)';
									var divRight = document
											.createElement("div");
									divRight.style.cssText = "float:left;margin-left:8px;width:345px;"
											+ "height:355px;margin-top:20px;";
									// divRight.style.backgroundImage = 'url(../images/bu.png)';

									var ul = document.createElement("ul");
									var liSex = document.createElement("li");
									var liage = document.createElement("li");
									var lilocal = document.createElement("li");
									var liprofessional = document
											.createElement("li");
									var liUnitAddress = document
											.createElement("li");
									var limonthlyIncome = document
											.createElement("li");
									var unitAddress = eval(result)[0].companyAddress;// 获取地址
									var professional = eval(result)[0].formatWork;// 获取工作性质
									var age = eval(result)[0].age;// 获取年龄
									var sex = eval(result)[0].sex;// 获取性别
									var MonthlyIncome = eval(result)[0].MonthlyIncome;// 获取月收入
									var isPermanentAddress = eval(result)[0].isPermanentAddress;// 获取是否常住本地
									liSex.innerText = "性别：" + sex;
									liage.innerText = "年龄：" + age;
									lilocal.innerText = "是否常住本地："
											+ isPermanentAddress;
									liprofessional.innerText = "职业："
											+ professional;
									if (professional == "学生") {
										limonthlyIncome.innerText = "月收入：";
										liUnitAddress.innerText = "学校地址："
												+ unitAddress;
									} else {
										limonthlyIncome.innerText = "月收入："
												+ MonthlyIncome;
										liUnitAddress.innerText = "单位地址："
												+ unitAddress;
									}
									liSex.style.cssText = "margin-left:30px;font-size:18px;color:#FFFFFF;margin-top:18px;text-align:left;";
									liage.style.cssText = "margin-left:30px;font-size:18px;color:#FFFFFF;margin-top:18px;text-align:left;";
									lilocal.style.cssText = "margin-left:30px;font-size:18px;color:#FFFFFF;margin-top:18px;text-align:left";
									liprofessional.style.cssText = "margin-left:30px;font-size:18px;color:#FFFFFF;margin-top:18px;text-align:left";
									limonthlyIncome.style.cssText = "margin-left:30px;font-size:18px;color:#FFFFFF;margin-top:18px;text-align:left";
									liUnitAddress.style.cssText = "margin-left:30px;font-size:18px;color:#FFFFFF;margin-top:18px;text-align:left";

									/*
									 * //alert(unitAddress);
									 * //判断单位地址是否为空，如果为空就不显示。
									 * if(unitAddress==undefined){
									 * liunitAddress.style.cssText="display:none;margin-left:10px;font-size:16px;width:550px;margin-top:18px;text-align:left;";
									 * }else{
									 * liunitAddress.style.cssText="margin-left:30px;font-size:14px;color:#FFFFFF;margin-top:15px;text-align:left;"; }
									 */
									ul.appendChild(liSex);
									ul.appendChild(liage);
									ul.appendChild(lilocal);
									ul.appendChild(liprofessional);
									ul.appendChild(limonthlyIncome);
									ul.appendChild(liUnitAddress);
									divground.appendChild(ul);
									div.appendChild(divground);
									div.appendChild(divbackground);
									div.appendChild(divRight);
									document.body.appendChild(div);//div放到页面
								}
							});
				}
			});
		}

	</script>
</body>
</html>







