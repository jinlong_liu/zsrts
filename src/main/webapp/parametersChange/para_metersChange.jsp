<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>参数设置</title>
<jsp:include page="/js/inc.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
<%-- <link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/table/fileinput.min.css" /> --%>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/third_party/maskLayer/DataCenter.css" />

<!-- 分页时多选弹出框插件插件 -->
<script type="text/javascript"
	src="<%=path%>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
<script type="text/javascript"
	src="<%=path%>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
<!-- import js -->

<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>
<!-- alert样式 -->
<link rel="stylesheet" type="text/css"
	href="<%=path%>/third_party/sweet-alert/sweet-alert.css" />
<script type="text/javascript"
	src="<%=path%>/third_party/sweet-alert/sweet-alert.min.js"></script>
<%-- <script type="text/javascript"
	src="<%=path%>/js/bootstrap/table/fileinput.js"></script> --%>

<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/parametersChange/para_metersChange.css" />
<script type="text/javascript"
	src="<%=path%>/js/parametersChange/para_metersChange.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/parametersChange/ajaxfileupload.js"></script>
<script type="text/javascript">
</script>

</head>
<body>
	<div class="bgBox">
		<div class="loaderTip">
			<img src="../images/loading.gif" />
			<div class="loaderTip_msg">数据正在加载中，请稍候...</div>
		</div>
	</div>


	<div class="infoBase">

		<%-- <c:if test="${sessionScope.adminFlag != 5 }">   --%>


		<form id="formid" style="width: 1100px;"
			post" enctype="multipart/form-data">
			<button type="button" style="margin-left: 5px; border: none;"
				modal" data-target="#myModal" onclick="uodateTotal()">
				<!-- class="btn btn-primary" -->
				<img src="../images/updatea.png">
				<!-- 修改参数 -->
			</button>
			<label id='userStyle' style="width: 4.5%; margin-left: 8%;">行政区:</label>
			<select id='districtName' name='type'
				class='js-example-disabled-results'
				style='width: 150px; height: 35px; margin-top: 10px;'
				onchange='dArea(2)'>
				<option value=''>请选择</option>
			</select> <label id='userStyle' style="width: 3%; margin-left: 1%;">街道:</label>
			<select id='subofficeName' name='type'
				class='js-example-disabled-results'
				style='width: 150px; height: 35px; margin-top: 10px;'>
				<option value=''>请选择</option>
			</select>
			<button type='button'
				style='border-radius: 6px; border: none; width: 80px; background: #06b370; color: white; margin-top: 5px; margin-left: 20px; height: 35px;'
				onclick='queryAreaa()'>查 询</button>
			<input id="input-6" name="input6" type="radio" value="2"
				style="margin-left: 5%"><span>指标数</span> <input id="input-6"
				name="input6" type="radio" value="3" style="margin-left: 30px">提交数


			<a href="javascript:;" class="file"
				style="border: none; width: 80px; height: 35px; top: 10px; background: url(../images/chos.png) no-repeat center; text-align: center;">
				<input id="file" name="file" type="file" style="width: 78px"
				multiple class="file">
			</a>
			<!-- <button type="button" class="btn btn-primary" >提交</button> -->
			<input type="button"
				style="border: none; background: url(../images/import.png) no-repeat center; text-align: center; padding: 3px 3px; width: 78px; height: 40px; margin-top: 0.7%; float: right"
				onclick="ajaxFileUpload()" />
		</form>

		<%--  </c:if>   --%>

		<div class="radiodiv" style="width: 80px">
			<!-- <div class="button"> -->
			<!-- <button  style=""type="button" style="border:none;class="btn btn-primary" data-toggle="modal"
				onclick="uodateTotal()"><img  src="../images/updatea.png"/> </button> -->


			<!-- 	</div> -->

			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">设置参数</h4>
						</div>
						<div id="selectDiv">
							</br> <label id="laberLeft">选择类型：</label> <input
								type="radio" name="chSelect" value="1" onclick="selectOnchang()">指标数
							&nbsp; &nbsp; <input type="radio" name="chSelect" value="2"
								onclick="selectOnchang()">提交数 &nbsp; &nbsp; <input
								type="radio" name="chSelect" value="3" onclick="selectOnchang()">全选
							&nbsp; &nbsp; <input type="hidden" id="checkSave" value="0">
							<!--  <input type="hidden" id="id" value=" ">
									  <input type="hidden" id="oldName" value=" "> -->

						</div>

						<div id="testdiv"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary"
								style="margin-right: 45%" onclick="save()">保存</button>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 加载提示框 -->
		<%-- <table width=100% height=100% border=0 align=center valign=middle >
							    <tr height=50%><td align=center>&nbsp;</td></tr>
							    <tr><td align=center><img src="<%=request.getContextPath()%>/XXXXX/loading-gif.gif"/></td></tr>
							    <tr><td align=center>数据载入中，请稍后......</td></tr>
							    <tr height=50%><td align=center>&nbsp;</td></tr>
							   </table> --%>
		<div class="margin-top-table"
			style="width: 1100px; height: 565px; margin-left: 5px">
			<table id="table"></table>
		</div>
	</div>
</body>
</html>
