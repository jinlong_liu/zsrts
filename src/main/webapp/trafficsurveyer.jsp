<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="initial-scale=1.0, user-scalable=no, width=device-width">
<title>出行调查APP下载</title>

<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<jsp:include page="/js/inc.jsp"></jsp:include>
<style type="text/css">
/*   span{
     display:block;
         }*/
p {
	white-space: normal;
}
</style>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						var sUserAgent = navigator.userAgent.toLowerCase();
						var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
						var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
						var bIsMidp = sUserAgent.match(/midp/i) == "midp";
						var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
						var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
						var bIsAndroid = sUserAgent.match(/android/i) == "android";
						var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
						var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
						if (!(bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7
								|| bIsUc || bIsAndroid || bIsCE || bIsWM)) {
							//alert("电脑");
							url = root + "/trafficsurveyer-pc.jsp";
							window.location.href = url;
						}
					});
</script>
</head>
<body style="background-color: #01B8AE;">
	<div>
		<br />
		<div class="qrcode-sign-top" style="width: 80%;margin-left: 10%;font-weight: bold;">
			<!-- <span>出行轨迹调查APP下载</span> -->
			<span>出行轨迹手机APP下载页面及使用说明</span>
		</div>
		<br />
		<div class="word-div-center">
			<!-- <p class="qrcode-p-big">活动规则</p> -->
		</div>
		<div class="word-div-margin">
			<p>
				<span class="qrcode-span-number">①</span><span
					class="qrcode-span-word">&nbsp;&nbsp;参与本项出行轨迹测试的居民可获得20元手机话费奖励。</span>
			</p>
			<p>
				<span class="qrcode-span-number">②</span><span
					class="qrcode-span-word">&nbsp;&nbsp;下载APP并注册完成后，需保持本软件1整天在后台运行，后台运行注意事项请阅读APP里的“使用帮助”。</span>
			</p>
			<p>
				<span class="qrcode-span-number">③</span><span
					class="qrcode-span-word">&nbsp;&nbsp;测试当天晚上睡觉前需补充当天一整天每次出行使用的交通方式和出行目的，补充完成后提交即完成测试。</span>
			</p>
			<p>
				<span class="qrcode-span-number">④</span><span
					class="qrcode-span-word">&nbsp;&nbsp;提交出行信息并经审核通过后，20元话费会在1小时之内充值到注册的手机账户中。</span>
			</p>
			<!-- <p>
				<span class="qrcode-span-number">⑤</span><span
					class="qrcode-span-word">提交出行信息并经审核通过后，20元话费会在1小时之内充值到注册的手机账户。</span>
			</p> -->

		</div>

        <br/><br/>
		<div class="qrcode-right-div-bak">

			<div class="qrcod-qr-zw"></div>

			<div class="right-color-bak">
				<div class="qrcod-qr-bak">
					<div class="div-boder-bak">
						<img src="/zsrts/images/wx/qrCode1.1.png" class="qrcode-img-bak" />
						<div class="qrcode-img-word-bak">
							<div style="width: 100%; height: 10px;font-size: 18px;margin-right: 50px;line-height: 80px;font-weight: bold;"><span>APP下载区</span></div>
							<!-- <textarea style="font-size: 14px;" class="qrcode-text" rows="2" disabled>APP下载区</textarea> -->
						</div>
						<div class="qrcode-a-one-bak">
							<a href="http://47.92.86.63/zsrts/app/trafficsurveyer1.1.apk"><img
								src="/zsrts/images/wx/app-qrcode-button.png" /></a>
						</div>

					</div>
				</div>

<!-- 				<div class="qrcod-qr-bak">
					<div class="div-boder-bak">
						<img src="/zsrts/images/wx/qrCode1.2.png" class="qrcode-img-bak" />
						<div class="qrcode-word-two-bak">
							<div style="width: 100%; height: 10px;"></div>
							<textarea class="qrcode-text" rows="2" disabled>参加一个工作日和一天周末测试APP下载区</textarea>
						</div>
						<div class="qrcode-a-two-bak">
							<a href="http://47.92.86.63/zsrts/app/trafficsurveyer1.2.apk"><img
								src="/zsrts/images/wx/app-qrcode-button.png" /></a>
						</div>

					</div>
				</div>

				<div class="qrcod-qr-bak">
					<div class="div-boder-bak">
						<img src="/zsrts/images/wx/qrCode1.3.png" class="qrcode-img-bak" />
						<div class="qrcode-word-three-bak">
							<div style="width: 100%; height: 10px;"></div>
							<textarea class="qrcode-text" rows="2" disabled>参加一周测试APP下载区</textarea>
						</div>
						<div class="qrcode-a-three-bak">
							<a href="http://47.92.86.63/zsrts/app/trafficsurveyer1.3.apk"><img
								src="/zsrts/images/wx/app-qrcode-button.png" /></a>
						</div>

					</div>
				</div> -->

			</div>
		</div>

	</div>
</body>
</html>