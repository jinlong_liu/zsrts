<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

String authDegree = request.getSession().getAttribute("authDegree").toString();
String[] array = authDegree.split(",");

%>

<link rel = "stylesheet" type = "text/css" href="<%=path%>/css/home/center.css" />
<style type="text/css">

</style>
<script type="text/javascript" src="<%=path%>/js/home/center.js"></script>

<div id="centerTabs" class="easyui-tabs"  fit="true" border="false" >
	<div title="首页" style="overflow:hidden; color:red;height:100%;" >
	   <div style="height:20%;"></div>
	   <div class="homeButton" style="width: 100%;">
		   <center>
			   <span style="font-size: 70px;color: #00a6c8">欢迎使用居民出行调查系统</span>
		   </center>
<%--	   <c:forEach var="auth" items="<%=array  %>">--%>
<%--	   <c:if test="${auth == 9}">	--%>
<%--	    <a href="javascript:addTab('参数设置', './parametersChange/para_metersChange.jsp')">--%>
<%--	     <img  alt="mouse" onmouseover="mouseOver(1);" onmouseout="mouseOut(1);" id="img1" src="./images/parameterSeting.png" >--%>
<%--	    </a>--%>
<%--	    </c:if>--%>
<%--	    <c:if test="${auth == 8}">	--%>
<%--	    <a href="javascript:addTab('轨迹审核管理', './appVolunteers/app_volunteers.jsp')" >--%>
<%--	     <img alt="mouse" onmouseover="mouseOver(2);" onmouseout="mouseOut(2);" id="img2"  src="./images/pegionalManager.png">--%>
<%--	    </a>--%>
<%--	    </c:if>--%>
<%--	    </c:forEach>--%>
<%--	   <!--  <img src="./images/pegionalManager.png" alt="mouse" onmouseover="alert('您的鼠标在图片上！')" />  -->--%>
<%--	    --%>
<%--	    <a href="javascript:addTab('问卷查询 ', './questionnaire/ques_tionnaire.jsp')">--%>
<%--	     <img alt="mouse" onmouseover="mouseOver(3);" onmouseout="mouseOut(3);" id="img3" src="./images/questionnaireInquiry.png">--%>
<%--	    </a>--%>
<%--	    --%>
<%--	    <a href="javascript:addTab('问卷统计', './sysmanage/questionnaireStatistics/questionnaireStatistics.jsp')">--%>
<%--	     <img alt="mouse" onmouseover="mouseOver(4);" onmouseout="mouseOut(4);" id="img4" src="./images/questionnaireStatistics.png">--%>
<%--	    </a>--%>
	  </div>
	   <div style="height:20%;"></div>
	</div>
</div>

<div id="tabsMenu" class="easyui-menu" style="width:150px;">
	<div id="tabupdate">刷新</div>
	<div class="menu-sep"></div>
	<div id="close">关闭</div>
	<div id="closeall">全部关闭</div>
	<div id="closeother">除此之外全部关闭</div>
	<div class="menu-sep"></div>
	<div id="closeright">当前页右侧全部关闭</div>
	<div id="closeleft">当前页左侧全部关闭</div>
	<div class="menu-sep"></div>
	<div id="exit">退出</div>
</div>