<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

String authDegree = request.getSession().getAttribute("authDegree").toString();
String[] array = authDegree.split(",");
 
%>
<html>
	<head>
		<title>导航菜单</title>
	</head>	
	<body>
		<script type="text/javascript" src="<%=path%>/js/home/left.js"></script>
		<ul class="expmenu">
			<li>
<%--				<div class="header">--%>
<%--					<span class="passengerflowanalyzeimage" ></span>--%>
<%--					<span class="label">系统管理</span>--%>
<%--					<span class="arrow down"></span>--%>
<%--				</div> --%>
				<div class="no">
					<ul class="menu_e" style="display:block;">
<%--					    <li  onclick="javascript:showMenu(this,0)" class="div-height">--%>
<%--							<a href="javascript:void(0)">--%>
<%--								<span class="i">+</span>--%>
<%--								<span class="stopflowimg"></span>--%>
<%--								<span class="label">统计查询</span>--%>
<%--							</a>--%>
<%--						</li>--%>
<%--						<ol class="no">--%>
<%--							<a  href="javascript:addTab('问卷查询 ', './questionnaire/ques_tionnaire.jsp')" onclick="setcolor(this,0);" onmouseover="mouseover(1);" onmouseout="mouseout(1);">--%>
<%--								<!-- <span class="img"></span> -->--%>
<%--								<span id="span1" class="img"   style="background-image: url('./images/questionnaireDefault .png');"></span>--%>
<%--								<span class="label">问卷查询</span>--%>
<%--							</a>--%>
<%--							<a  href="javascript:addTab('问卷统计', './sysmanage/questionnaireStatistics/questionnaireStatistics.jsp')" onclick="setcolor(this,1);" onmouseover="mouseover(2);" onmouseout="mouseout(2);">--%>
<%--								<span id="span2" class="img" style="background-image: url('./images/QuestionnaireStatistical.png');"></span>--%>
<%--								<span class="label">问卷统计</span>--%>
<%--							</a>--%>
<%--			  			</ol>--%>
						<li onclick="javascript:showMenu(this, 0)" class="div-height">
							<a href="javascript:void(0)">
								<span class="i">+</span>
								<span class="lineimg"></span>
								<span class="label">系统管理</span>
							</a>
						</li>
						<ol class="no">
						
						<c:forEach var="auth" items="<%=array  %>">
						
						 <c:if test="${auth == 1}">
							<a  href="javascript:addTab('平台用户管理', './sysmanage/usermanage/user.jsp')" onclick="setcolor(this,0);" onmouseover="mouseover(3);" onmouseout="mouseout(3);">
								<span id="span3" class="img" style="background-image: url('./images/Usermanagement.png');"></span>
								<span class="label">平台用户管理</span>
							</a>
						 </c:if>

						 <c:if test="${auth == 2}">
							<a  href="javascript:addTab('角色管理', './sysmanage/rolemanage/role_manage.jsp')" onclick="setcolor(this,1);"onmouseover="mouseover(4);" onmouseout="mouseout(4);">
								<span id="span4" class="img" style="background-image: url('./images/Rolemanagement.png');"></span>
								<span class="label">角色管理</span>
							</a>
						 </c:if>

<%--							<c:if test="${auth == 2}">--%>
<%--								<a  href="javascript:addTab('学生成绩管理', './sysmanage/rolemanage/rolegrademanage.jsp')" onclick="setcolor(this,1);"onmouseover="mouseover(4);" onmouseout="mouseout(4);">--%>
<%--									<span id="span5" class="img" style="background-image: url('./images/Rolemanagement.png');"></span>--%>
<%--									<span class="label">学生成绩管理</span>--%>
<%--								</a>--%>
<%--							 </c:if>--%>
<%--							<!--line_transfer_sta.jsp  -->--%>

<%--						 <c:if test="${auth == 8}">--%>
<%--							<a  href="javascript:addTab('区域管理', './sysmanage/areamanager/area_manage.jsp')" onclick="setcolor(this,2);"onmouseover="mouseover(5);" onmouseout="mouseout(5);">--%>

<%--								<span id="span5"  class="img" style="background-image: url('./images/Regionalmanagement.png');"></span>--%>
<%--								<span class="label">区域管理</span>--%>
<%--							</a>--%>
<%--						</c:if>--%>
<%--						<c:if test="${auth == 9}">		--%>
<%--							<a  href="javascript:addTab('参数设置', './parametersChange/para_metersChange.jsp')" onclick="setcolor(this,2);"onmouseover="mouseover(6);" onmouseout="mouseout(6);">--%>

<%--								<span id="span6" class="img" style="background-image: url('./images/ParameterSettings.png');"></span>--%>
<%--								<span class="label">参数设置</span>--%>
<%--							</a>--%>
<%--						</c:if>		--%>
							</c:forEach>
							<a  href="javascript:addTab('修改密码', './sysmanage/modifyPassword/modifyPassword.jsp')" onclick="setcolor(this,4);"onmouseover="mouseover(7);" onmouseout="mouseout(7);">
								<span id="span7" class="img" style="background-image: url('./images/Changepassword.png');"></span>
								<span class="label">修改密码</span>
							</a>
<%--							<a  href="javascript:addTab('生成二维码', './weQrCode.jsp')" onclick="setcolor(this,5);" onmouseover="mouseover(8);" onmouseout="mouseout(8);">--%>
<%--								<span id="span8" class="img" style="background-image: url('./images/code.png');"></span>--%>
<%--								<span class="label">生成二维码</span>--%>
<%--							</a>--%>
						
						</ol>
					</ul> 
				</div>
			</li>
			
			<li>

<%--				 <div class="header">--%>

<%--					<span class="networkassessimage" ></span>--%>
<%--					<span class="label">业务管理</span>--%>
<%--					<span class="arrow down"></span>--%>

<%--				</div>--%>


				<div class="no">
					<ul class="menu_e" style="display:block;">
						<li onclick="javascript:showMenu(this, 0)" class="div-height" >
							<a  href="javascript:void(0)">
								<span class="i">+</span>
								<span class="planbuildtargetimg"></span>
								<span class="label">业务管理</span>
							</a>
						</li>
						<ol class="no">
							<a  href="javascript:addTab('轨迹审核管理', './appVolunteers/app_volunteers.jsp')" onclick="setcolor(this,0);" onmouseover="mouseover(9);" onmouseout="mouseout(9);" >
								<span id="span9" class="img"style="background-image: url('./images/uesrQuery.png');" ></span>
								<span class="label">轨迹审核管理</span>
							</a>
							
<%--							 <a  href="javascript:addTab('统计查询', './appSampleStatistics/appSampleStatistics.jsp')" onclick="setcolor(this,1);" onmouseover="mouseover(10);" onmouseout="mouseout(10);" >--%>
<%--								<span id="span10" class="img" style="background-image: url('./images/Statisticalquery.png');"></span>--%>
<%--								<span class="label">统计查询</span>--%>
<%--							</a>--%>
							<a href="javascript:addTab('调查用户管理', './appVolunteers/appVolunteersManage.jsp')" onclick="setcolor(this,1)" onmouseover="mouseover(10)" onmouseout="mouseout(10)">
								<span id="span10" class="img" style="background-image: url('./images/Statisticalquery.png');"></span>
								<span class="label">调查用户管理</span>
							</a>
							
							
							
						</ol>
					</ul> 
				</div>
			</li>
			
		</ul>
	</body>
</html>