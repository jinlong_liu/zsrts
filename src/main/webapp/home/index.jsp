<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ include file="../common/taglibs.jsp"%>

<%
	String userName = request.getSession().getAttribute("userName").toString();
	String roleName = request.getSession().getAttribute("roleName").toString();
%>

<html>
<head>
	<title>居民出行调查系统</title>
	<!-- jquery库 -->
	<script type="text/javascript" src="${ctx}/third_party/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="${ctx}/common/common.js"></script>
	<script type="text/javascript" src="${ctx}/third_party/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${ctx}/third_party/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="${ctx}/js/home/index.js"></script>
	<!-- easyui相关库 -->
	<link rel = "stylesheet" type = "text/css" href = "${ctx}/third_party/easyui/themes/ui-cupertino/easyui.css" />
	<link rel = "stylesheet" type = "text/css" href = "${ctx}/third_party/easyui/themes/icon.css" />
	<link rel = "stylesheet" type = "text/css" href = "${ctx}/css/menu_styles.css" />
	<link rel = "stylesheet" type = "text/css" href = "${ctx}/css/main.css" />
	<link rel = "stylesheet" type = "text/css" href = "${ctx}/css/home/index.css" />
	<link rel = "stylesheet" type = "text/css" href = "${ctx}/css/home_styles.css" />

</head>

<body class="easyui-layout body" style="width: 100%">
<!-- navgation  end -->
<div data-options="region:'north'"  style="width:100%; height:82px;" class="navi-outer">
	<div class="logo-ico" style="background:url(./images/logo1.png)no-repeat 10px center;background-color:#00cd7f"></div>
	<div class="title-text" style="background:url(./images/logo.png)no-repeat 10px center"> </div>
	<div class="navi-right" >
		<div class="user-but" id="user_but" ></div>
		<div class="user-info">
			<div class="user-name">${userName }</div>
			<div class="user-rank">${roleName }</div>
		</div>
		<div class="exit-but" style="background:url(./images/exit.png)no-repeat center"id="exit_but" title="注销"></div>
	</div>
</div>

<div data-options="region:'south', split:'true'" style="height: 0px; width:1120px; background:'#D2E0F2'">
</div>

<div data-options = "region:'west', split:true, title:'导航菜单', href:'./home/left.jsp'"
	 class="div-left-div" >
</div>
<div data-options="region:'center', href:'./home/center.jsp'" >
</div>
</body>
</html>