<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
 <!-- 遮罩层 -->
   <link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet" media="screen" /> 
       <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript">
	
</script>
<title>居民出行调查</title>
</head>
<body>
	<div>
		<div class="one-carinfo"></div>
		<br />
		<div class="carinfo-margin">
			<div>
				<span class="spanw">1、车辆属性？：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx0" id="sx00"
						value="0"> <label for="sx00">家用</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx0" id="sx10"
						value="1"> <label for="sx10">政府</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx0" id="sx20"
						value="2"> <label for="sx20">军用</label>
				</div>
			</div>
			<br /> <span class="spanw">2、排量（升）：</span> <input type="number"
				class="inputw" id="showIdPl0" onblur="TextOnBlurShow(this)"/> <span id="checknum" class="tsText"></span><br />
			<br /> <span class="spanw">3、过去12个月行驶里程（公里）：</span><br /> <input
				type="number" class="inputwc" id="showIdGl0" onblur="TextOnBlurShow(this)"/><br />
			<br /> <span class="spanw">4、是否有停车位？：</span>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw0" id="cw00"
						value="0"> <label for="cw00">是</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw0" id="cw10"
						value="1"> <label for="cw10">否</label><br />
				</div>
			</div>
		</div>
	</div>
	<br />
	<br />
	<div>
		<div class="two-carinfo"></div>
		<br />
		<div class="carinfo-margin">
			<div>
				<span class="spanw">1、车辆属性？：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx1" id="sx01"
						value="0"> <label for="sx01">家用</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx1" id="sx11"
						value="1"> <label for="sx11">政府</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx1" id="sx21"
						value="2"> <label for="sx21">军用</label>
				</div>
			</div>
			<span class="spanw">2、排量（升）：</span> <input type="number" class="inputw"
				id="showIdPl1" onblur="TextOnBlurShow(this)" /> <span id="checknum" class="tsText"></span><br />
			<br /> <span class="spanw">3、过去12个月行驶里程（公里）：</span><br /> <input
				type="number" class="inputwc" id="showIdGl1" onblur="TextOnBlurShow(this)" /><br />
			<br /> <span class="spanw">4、是否有停车位？：</span>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw1" id="cw01"
						value="0"> <label for="cw01">是</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw1" id="cw11"
						value="1"> <label for="cw11">否</label>
				</div>
			</div>
		</div>
	</div>
	<br />
	<br />

	<div>
		<div class="three-carinfo"></div>
		<br />
		<div class="carinfo-margin">
			<div>
				<span class="spanw">1、车辆属性？：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx2" id="sx02"
						value="0"> <label for="sx02">家用</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx2" id="sx12"
						value="1"> <label for="sx12">政府</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx2" id="sx22"
						value="2"> <label for="sx22">军用</label>
				</div>
			</div>

			<span class="spanw">2、排量（升）：</span> <input type="number" class="inputw"
				id="showIdPl2" onblur="TextOnBlurShow(this)" /> <span id="checknum" class="tsText"></span><br />
			<br /> <span class="spanw">3、过去12个月行驶里程（公里）：</span><br /> <input
				type="number" class="inputwc" id="showIdGl2" onblur="TextOnBlurShow(this)" /><br />
			<br /> <span class="spanw">4、是否有停车位？：</span>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw2" id="cw02"
						value="0"> <label for="cw02">是</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw2" id="cw12"
						value="1"> <label for="cw12">否</label>
				</div>
			</div>

		</div>
	</div>
	</div>
	<br />
	<br />
	<br />

	<div style="text-align: center" id="scandal">
		<a href="javaScript:saveCarInfo()"><img
			src="../images/wx/next-page-green.png"
			style="width: 200px; height: 60px;" /></a>
	</div>

	<br />
	<br />
	<br />

</body>
</html>