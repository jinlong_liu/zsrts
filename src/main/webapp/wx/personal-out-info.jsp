<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
    <link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <link rel="stylesheet" href="<%=path%>/css/wx/magic-check.css"> 
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
<!-- 遮罩层 -->
<link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet"
	media="screen" />
<!-- 弹框 -->
<script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/sweetalert.css">
<!-- lghDialog -->
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/lhgdialog.css">
<script type="text/javascript"
	src="<%=path%>/js/wx/lhgcore.lhgdialog.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/wx/zDialog.js"></script>
<script type="text/javascript" src="<%=path%>/js/wx/zDrag.js"></script>
    <jsp:include page="/js/inc.jsp"></jsp:include>
     	<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/gaode-map.css">
    <script type="text/javascript">
    
    
    XBack = {};
	(function(XBack) {
		XBack.STATE = 'x - back';
		XBack.element;

		XBack.onPopState = function(event) {
			event.state === XBack.STATE && XBack.fire();
			XBack.record(XBack.STATE); //初始化事件时，push一下  
		};
		XBack.record = function(state) {
			history.pushState(state, null, location.href);
		};
		XBack.fire = function() {
			var event = document.createEvent('Events');
			event.initEvent(XBack.STATE, false, false);
			XBack.element.dispatchEvent(event);
		};
		XBack.listen = function(listener) {
			XBack.element.addEventListener(XBack.STATE, listener, false);
		};
		XBack.init = function() {
			XBack.element = document.createElement('span');
			window.addEventListener('popstate', XBack.onPopState);
			XBack.record(XBack.STATE);
		};

	})(XBack); // 可以当做js文件 引入这段

	XBack.init();
	XBack.listen(function() {
	});
    
    
    
    
    
    var weCityForBike = '<%=session.getAttribute("weCityCode")%>';
    //地图
    var weCity = '<%=session.getAttribute("weCityCode")%>';
    //alert(weCity);
    
	var personalNumForOut = 0;
$(document).ready(function(){
	
	$.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/wxgetPersonalList",
		data : "",
		async : false,
		success : function(result) {
			var obj = eval(result);
			var people = 1;
			var colorNum = 1;
			
			if(obj.length != 0){
				for(var i=0;i<obj.length;i++){
                    var two=obj[i];
                    var profession = "";
                    if(i==0){
                    if(two.profession==2){
                    	$('#outschoolorgosi').html("学校");
                    	$('#getschoolorgosi').html("学校");
                    }else{
                    	$('#outschoolorgosi').html("单位");
                    	$('#getschoolorgosi').html("单位");
                    }
                    
                    if(two.profession==3||two.profession==6||two.profession==7){
                    	$('#Company').css('display','none');
                    	$('#getCompany').css('display','none');
                    }
                                     
                    }
                    var sex = "";
                    if(two.sex==0){
                    	sex = "男";
                    }else{
                    	sex = "女";
                    }                
                    $("#personalTable").append("<tr id='tr"+i+"'><td style='width:33%;text-align:center;'>家庭成员"+ people +"</td><td style='width:33%;text-align:center;'>"+ sex +"</td><td style='width:33%;text-align:center;'>"+ two.age +"</td></tr><tr style='height:20px;'></tr>");
                    people++;
                }  
				for(var j=0;j<obj.length;j++){
					$("#tr"+colorNum).css("background-color","#F1F1F1");
					colorNum++;
					personalNumForOut++;
				}
				//$('#personalNumforPage').val(personalNum);
				$("#tr0").css("background-color","#E8C300");
            //swal("OK!", "成功！", "success");		
		}else{
			var url = root+"/wx/error-submit.jsp";
			window.location.href=url;		
		}		
		}
	});
	var myvaluefory = "昨天";
	var myvaluefort = "今天";
	var myvaluefornumy = "昨天第1次出行的出行信息";
	var muvaluefornumt = "今天第1次出行的出行信息";
	var muvalueforquestiony = "1.您昨天第一次出行的出发地点是？";
	var muvalueforquestiont = "1.您今天第一次出行的出发地点是？";
	var muvalueforsigny = "<span style='font-size:14px;'>如您昨天没有出行，填写没有出行的原因:</span>";
	var muvalueforsignt = "<span style='font-size:14px;'>如您今天没有出行，请填写没有出行的原因:</span>";
	var date = new Date();
	var hourForTorY = date.getHours();  
	if(hourForTorY<18){
		$('#greendforday0').html(myvaluefory);
		$('#small0').html(myvaluefornumy);
		$('#big0').html(muvalueforquestiony);
		$('#yellow').html(muvalueforsigny);
	}else{
		$('#greendforday0').html(myvaluefort);
		$('#small0').html(muvaluefornumt);
		$('#big0').html(muvalueforquestiont);
		$('#yellow').html(muvalueforsignt);
		
	}
	
	$.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/wxgetMetroSign",
		data : "",
		async : false,
		success : function(result) {
			var flag = result.flag;
			var message = result.message;
			if(flag=="1"){
				if(message==0){
					$('#metro00').css('display','none');
					$('#metro10').css('display','none');
					$('#metro20').css('display','none');
				}
			}
		}
	});	
	
	if(weCityForBike=='0539'){
		
		document.getElementById("bikeLy").innerHTML = "公租自行车";
	}
	
});
</script>
<style>

</style>
<title>居民出行调查</title>
</head>
<body>

<!-- 地址  -->
<div style="width:100%;height:100%;display:none;" id="adressforout" >
	<input id="numId" name="numId" value="${param.numId}" type="hidden" />
	<div id="container" tabindex="0"></div>
			<div class='panel'>
			<table width="100%" border="0">
				<tr style="width: 100%;" height="20%">
					<td width="15%">
						<input type="image" style="width: 100%;" src="../images/mapImageLeft.png" onclick="closemapforfamilyout()"/>
					</td>
					<td style="width: 60%;">
						<textarea id='returnInput' style="width: 100%;" value='点击地图获取位置/输入关键字获取位置'></textarea>
					</td>
					<td width="25%">
						<input type="image" id="btn" style="width: 100%;" src="../images/mapSubmit.png" onclick="submitmapforfamilyout()"/>
					</td>
				</tr>
			</table>
			<input placeholder="请输入关键字进行搜索" id="lnglat" hidden="hidden" type="text" />
			<div id='message'></div>
		</div>	
	<script type="text/javascript"
		src="http://webapi.amap.com/maps?v=1.3&key=3067ac7a693712711f75a87e43d6e5f9"></script>
     <script type="text/javascript" src="<%=path%>/js/wx/personalone.js"></script>
	<script type="text/javascript"
		src="http://webapi.amap.com/demos/js/liteToolbar.js"></script>
</div>


<input id="personalNumforPage" name="personalNumforPage" value="1" type="hidden"/>

<div class="greenTop">
<div class="greenBig">
<span class="greenTopSpan">&nbsp;1</span></div>
<div class="greenTopSmall">
<span class="greenTopSmallSpan">请填写您的出行信息</span></div></div>
<!-- 成员信息 -->
<div id="personalDiv" class="personalDiv">  
<center>
<table id="personalTable" class="personalTable">
<tr>
<td class="personalTd"><span style="font-size:18px;">家庭成员</span></td>
<td class="personalTdCenter"><span>性别</span></td>
<td class="personalTdCenter"><span>年龄</span></td>
</tr>
<tr class="zwTr"></tr>
</table>
</center>
</div>

<div class="tableLb"></div>
<!-- 出行信息 -->

<div class="personalXym" id="xym">
<!-- 标题头 -->
<br/>
<!-- 独立的大div，删除使用 -->
<div id="z0">
<div class="titleTopDiv" >
<div class="titleTopDivSpan" style="font-size:18px;">请填写您&nbsp<span id="greendforday0" style="font-size:25px;color:red;font-weight:900;font-family:Tahoma;"></span>&nbsp完整一天的出行信息</div>
<div class="titleTopDivTwo">
<span class="titleTopDivSpanTwo" id="small0"></span>
</div>
</div>
<br/><br/>
<!-- xinxikaishi -->
		<div class="carinfo-margin">
			<div style="width:92%;" id="cfdd0">
				<span class="spanw" id="big0" ></span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiolocations0" id="locations00"
						value="0" onclick="noneAddress(0)" > <label for="locations00">家</label>
				</div>
			</div>
			<div class="divBorder" id="Company">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiolocations0" id="locations10"
						value="1" onclick="noneAddress(0)"> <label for="locations10" id="outschoolorgosi"></label>
				</div>
			</div>
			<div class="divBorderNoheight">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiolocations0" id="locations20"
						value="2" onclick="blockAddress(0)"> <label for="locations20">其他地址</label>
				</div>
				
			<div class="qtAddress" id="txDiv0" onclick="mapforoutaddress(0,'out')">
             <textarea  disabled="disabled" rows="3" class="textareaAddress" id="tx0" name="tx0"></textarea>
             <a href="javascript:mapforoutaddress(0,'out')"><img src="../images/wx/postion.png"/></a>
             <input id="langt0" name="langt0" type="hidden"/>
             </div>
             
			</div>
			<br/><br/>
			
			<!-- chufashijian -->
			<div id="cfsj0">
				<span class="spanw">2.出发时间:</span>
			</div>
			<div class="outTimeDiv">
							<div class="divBorderTime">
				<div class="zwTimeDiv"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiooutTime0" id="outTime00"
						value="0"  onclick="checkRadio(0)"> <label for="outTime00">上午</label>
				</div>
			              </div>

			              				<div class="divBorderTime">
				<div class="zwTimeDiv"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiooutTime0" id="outTime10"
						value="1"  onclick="checkRadio(0)"> <label for="outTime10">下午</label>
				</div>
			                           </div>
			                           
			   <div class="divBorderTime">
	             <div class="timeLeft"><input type="number" id="timeHour0" name="timeHour0" class="timeInput" onfocus="forQuantum(0)" onblur="proofTime(0,this)"/></div>
	                     <div class="timeLine"><span>时</span></div>
			            </div>  

			   <div class="divBorderTime">
	             <div class="timeLeft"><input type="number" id="timeMinute0" name="timeMinute0"  class="timeInput" onfocus="forQuantum(0)" onblur="proofMinute(0,this)"/></div>             
			              <div class="timeLine"><span>分</span></div>
			    </div>		              
			</div>
			<br/><br/>
			<!-- 到达地点 -->
						<div id="dddd0">
				<span class="spanw">3.到达地点：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiogetlocations0" id="getlocations00"
						value="0" onclick="getnoneAddress(0)" > <label for="getlocations00">家</label>
				</div>
			</div>
			<div class="divBorder" id="getCompany">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiogetlocations0" id="getlocations10"
						value="1" onclick="getnoneAddress(0)"> <label for="getlocations10" id="getschoolorgosi"></label>
				</div>
			</div>
			<div class="divBorderNoheight">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiogetlocations0" id="getlocations20"
						value="2" onclick="getblockAddress(0)"> <label for="getlocations20">其他地址</label>
				</div>
				
			<div style="border:2px solid #E5E5E5;display:none;width:90%;margin-left:6%;" id="gettxDiv0" onclick="mapforoutaddress(0,'get')">
             <textarea  disabled="disabled" rows="3" class="textareaAddress" id="gettx0" name="gettx0"></textarea>
             <a href="javascript:mapforoutaddress(0,'get')"><img src="../images/wx/postion.png"/></a>
             <input id="getlangt0" name="getlangt0" type="hidden"/>
             </div>
			</div>
			<br/><br/>
			<!-- 到达时间 -->
			<div id="ddsj0">
				<span class="spanw">4.到达时间:</span>
			</div>
			<div class="outTimeDiv">
							<div class="divBorderTime">
				<div class="zwTimeDiv"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiogetTime0" id="getTime00"
						value="0"  onclick="getcheckRadio(0)"> <label for="getTime00">上午</label>
				</div>
			              </div>

			              				<div class="divBorderTime">
				<div class="zwTimeDiv"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiogetTime0" id="getTime10"
						value="1" onclick="getcheckRadio(0)"> <label for="getTime10">下午</label>
				</div>
			                           </div>
			                           
			   <div class="divBorderTime">
	             <div class="timeLeft"><input type="number" id="gettimeHour0" name="gettimeHour0" style="height:41px;width:80%;" onfocus="forGetQuantum(0)" onblur="proofGetTime(0,this)"/></div>
	                     <div class="timeLine"><span>时</span></div>
			            </div>  

			   <div class="divBorderTime">
	             <div class="timeLeft"><input type="number" id="gettimeMinute0" name="gettimeMinute0"  style="height:41px;width:80%;" onfocus="forGetQuantum(0)" onblur="proofGetMinute(0,this)"/></div>             
			              <div class="timeLine"><span>分</span></div>
			    </div>		              
			</div>
			<br/><br/>
			<!-- 出行目的 -->
						<div id="cxmd0">
				<span class="spanw">5.出行目的：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotrafficPurpose0" id="trafficPurpose00"
						value="0" onclick="qtcxmdtxforblock(0)"> <label for="trafficPurpose00">上班</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotrafficPurpose0" id="trafficPurpose10"
						value="1" onclick="qtcxmdtxforblock(0)"> <label for="trafficPurpose10">上学</label>
				</div>
			</div>
			
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotrafficPurpose0" id="trafficPurpose20"
						value="2" onclick="qtcxmdtxforblock(0)"> <label for="trafficPurpose20">工作外出&nbsp;(包括开会,公务等。)</label>
				</div>
			</div>
						<div style="width:90%;height:80px; border:1px solid #E6E6E6;">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotrafficPurpose0" id="trafficPurpose30"
						value="3" onclick="qtcxmdtxforblock(0)"> <label for="trafficPurpose30">生活类出行&nbsp;(包括:购物、休闲健身、外出就餐、看病、探访亲友、文化娱乐等。)</label>
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotrafficPurpose0" id="trafficPurpose40"
						value="4" onclick="qtcxmdtxforblock(0)"> <label for="trafficPurpose40">回家</label>
				</div>
			</div>
						<div style="width:90%;height:60px; border:1px solid #E6E6E6;">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotrafficPurpose0" id="trafficPurpose50"
						value="5" onclick="qtcxmdtxforblock(0)"> <label for="trafficPurpose50">回程&nbsp;(指完成一次行程后的返回过程，返回地不是家。)</label>
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotrafficPurpose0" id="trafficPurpose60"
						value="6" onclick="qtcxmdtxforblock(0)"> <label for="trafficPurpose60">接送小孩</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotrafficPurpose0" id="trafficPurpose70"
						value="7" onclick="qtcxmdtxforblock(0)"> <label for="trafficPurpose70">其他(注明)</label>
				</div>
			</div>		
			<div class="divBorder" style="display:none;" id="qtcxmdtxforblock0">
					<textarea class="qtjtfsText" id="qtcxmdtx0" name="qtcxmdtx0"></textarea>	
			</div>
			<br/>
						
             <!-- 温馨提示 -->			  
			  <!-- <div class="prompt" style="font-size:15px;">
			  <p>1.<span style="font-size:18px;color:red;">工作外出</span>包括开会,公务等。</p>
			  <p>2.<span style="font-size:18px;color:red;">回程</span>应该是指完成一次行程后的返回过程，返回地不是家。</p>
			  <p>3.<span style="font-size:18px;color:red;">生活类出行</span>包括:购物、休闲健身、外出就餐、看病、
			  探访亲友、文化娱乐等。</p>
			  </div><br/><br/>	 -->
			<!-- 交通方式 -->
			<div id="jtfs0">
				<span class="spanw"  >6.交通方式：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic00"
						value="0" onclick="qtjtfsttxforblock(0)"> <label for="traffic00">步行</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic10"
						value="1" onclick="qtjtfsttxforblock(0)"> <label for="traffic10">电动车</label>
				</div>
			</div>
			
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic20"
						value="2" onclick="qtjtfsttxforblock(0)"> <label id="bikeLy" for="traffic20">共享单车(如"小黄车")</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic30"
						value="3" onclick="qtjtfsttxforblock(0)"> <label for="traffic30">私人自行车</label>
				</div>
			</div>
						<div class="divBorder" id="metro00">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic40"
						value="4" onclick="qtjtfsttxforblock(0)"> <label for="traffic40">地铁</label>
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic50"
						value="5" onclick="qtjtfsttxforblock(0)"> <label for="traffic50">公交车</label>
				</div>
			</div>
						<div class="divBorder" id="metro10">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic60"
						value="6" onclick="qtjtfsttxforblock(0)"> <label for="traffic60">公交+地铁</label>
				</div>
			</div>
						<div class="divBorder" id="metro20">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic70"
						value="7" onclick="qtjtfsttxforblock(0)"> <label for="traffic70">自行车+地铁</label>
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic80"
						value="8" onclick="qtjtfsttxforblock(0)"> <label for="traffic80">驾驶私人小汽车</label>
				</div>
			</div>
									<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic90"
						value="9" onclick="qtjtfsttxforblock(0)"> <label for="traffic90">乘坐私人小汽车</label>
				</div>
			</div>
									<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic100"
						value="10" onclick="qtjtfsttxforblock(0)"> <label for="traffic100">班车</label>
				</div>
			</div>
									<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic110"
						value="11" onclick="qtjtfsttxforblock(0)"> <label for="traffic110">校车</label>
				</div>
			</div>
									<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic120"
						value="12" onclick="qtjtfsttxforblock(0)"> <label for="traffic120">出租车</label>
				</div>
			</div>
									<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic130"
						value="13" onclick="qtjtfsttxforblock(0)"> <label for="traffic130">网约车</label>
				</div>
			</div>
									<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic140"
						value="14" onclick="qtjtfsttxforblock(0)"> <label for="traffic140">驾驶单位小汽车</label>
				</div>
			</div>
			
									<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic150"
						value="15" onclick="qtjtfsttxforblock(0)"> <label for="traffic150">乘坐单位小汽车</label>
				</div>
			</div>
									<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic160"
						value="16" onclick="qtjtfsttxforblock(0)"> <label for="traffic160">摩托车</label>
				</div>
			</div>
									<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiotraffic0" id="traffic170"
						value="17" onclick="qtjtfsttxforblock(0)"> <label for="traffic170">其他交通方式(注明)</label>
				</div>
			</div>
			
			<div class="divBorder" style="display:none;" id="qtjtfsttxforblock0">
					<textarea class="qtjtfsText" id="qtjtfsttx0" name="qtjtfsttx0"></textarea>	
			</div>
			
			<br/><br/>
			
			
			
			<!-- 是否接送小孩 -->
			<div id="childenBlock0">			
			<div>
				<span class="spanw">7.在本次出行中是否顺路接送过小孩：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiojoinchilden0" id="joinchildenyes00"
						value="0" onclick="blockforjoinorgive(0)"> <label for="joinchildenyes00">接小孩</label>
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiojoinchilden0" id="givechildenyes00"
						value="1" onclick="blockforjoinorgive(0)"> <label for="givechildenyes00">送小孩</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiojoinchilden0" id="joinchildenno00"
						value="2" onclick="blockforjoinorgive(0)"> <label for="joinchildenno00">否</label>
				</div>
			</div>
			<br/><br/>
			</div>
			<!-- 接小孩 -->
			<div id="joinchildenblock0" style="display:none;">
			
			<div>
				<span class="spanw">您接小孩的地点：</span>
			</div>
			<div style="border:2px solid #E5E5E5;width:90%;" id="jointxDiv0" onclick="mapforoutaddress(0,'join')">
             <textarea  disabled="disabled" rows="3" class="textareaAddress" id="joinwheretx0" name="joinwheretx0"></textarea>
             <a href="javascript:mapforoutaddress(0,'join')"><img src="../images/wx/postion.png"/></a>
             <input id="joinlangt0" name="joinlangt0" type="hidden"/>
             </div>
             <br/>
             <div>
				<span class="spanw">您在以上地点接到小孩的时间：</span>
			</div>
             			<div class="outTimeDiv">
							<div class="divBorderTime">
				<div class="zwTimeDiv"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiojoinTime0" id="joinTime00"
						value="0"  onclick="joincheckRadio(0)"> <label for="joinTime00">上午</label>
				</div>
			              </div>

			              				<div class="divBorderTime">
				<div class="zwTimeDiv"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiojoinTime0" id="joinTime10"
						value="1" onclick="joincheckRadio(0)"> <label for="joinTime10">下午</label>
				</div>
			                           </div>
			                           
			   <div class="divBorderTime">
	             <div class="timeLeft"><input type="number" id="jointimeHour0" name="jointimeHour0" style="height:41px;width:80%;" onfocus="joinchildenonfocus(0)" onblur="joinchildenTime(0,this)"/></div>
	                     <div class="timeLine"><span>时</span></div>
			            </div>  

			   <div class="divBorderTime">
	             <div class="timeLeft"><input type="number" id="jointimeMinute0" name="jointimeMinute0"  style="height:41px;width:80%;" onfocus="joinchildenonfocus(0)" onblur="joinMinute(0,this)"/></div>             
			              <div class="timeLine"><span>分</span></div>
			    </div>		              
			</div>
             <br/><br/>
			</div>
			
			
			<!-- 送小孩 -->
			<div id="givechildenblock0" style="display:none;">
			
			<div>
				<span class="spanw">您送小孩到哪里：</span>
			</div>
			<div style="border:2px solid #E5E5E5;width:90%;" id="givetxDiv0" onclick="mapforoutaddress(0,'give')">
             <textarea  disabled="disabled" rows="3" class="textareaAddress" id="givewheretx0" name="givewheretx0"></textarea>
             <a href="javascript:mapforoutaddress(0,'give')"><img src="../images/wx/postion.png"/></a>
             <input id="givelangt0" name="givelangt0" type="hidden"/>
             </div>
             <br/>
             <div>
				<span class="spanw">您把小孩送到以上地点的时间：</span>
			</div>
             	<div class="outTimeDiv">
							<div class="divBorderTime">
				<div class="zwTimeDiv"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiogiveTime0" id="giveTime00"
						value="0"  onclick="givecheckRadio(0)"> <label for="giveTime00">上午</label>
				</div>
			              </div>

			              				<div class="divBorderTime">
				<div class="zwTimeDiv"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radiogiveTime0" id="giveTime10"
						value="1" onclick="givecheckRadio(0)"> <label for="giveTime10">下午</label>
				</div>
			                           </div>
			                           
			   <div class="divBorderTime">
	             <div class="timeLeft"><input type="number" id="givetimeHour0" name="givetimeHour0" style="height:41px;width:80%;" onfocus="givechildenonfocus(0)" onblur="givechildenTime(0,this)"/></div>
	                     <div class="timeLine"><span>时</span></div>
			            </div>  

			   <div class="divBorderTime">
	             <div class="timeLeft"><input type="number" id="givetimeMinute0" name="givetimeMinute0"  style="height:41px;width:80%;" onfocus="givechildenonfocus(0)" onblur="giveMinute(0,this)"/></div>             
			              <div class="timeLine"><span>分</span></div>
			    </div>		              
			</div>
             <br/><br/>
			</div>
			

       </div>
       </div>
       
</div>
               <!-- 填写未出行信息理由 -->
               <div class="personalXym">
               <div class="carinfo-margin">
               <!-- 添加下一个出行信息 -->
               <span><a href="javaScript:addOutInfoYm()"><div
						class="divCenter" style="margin-right: 4%;">
						<img src="../images/wx/add-out-info.png" style="width: 90%" />
					</div></a></span><br/><br/>
               <!-- 问号提示 -->
               <div class="whzwDiv"></div>		
                <div class="whImg">
                  <img src="../images/wx/interrogation.png"/>
                           </div>
                  <div class="whzwDivTwo">
             <a class="whzwa" href="javascript:void(0)" onclick="blockNoOutInfo(0)" id="yellow">
                     </a>
                                 </div>
                      <div class="whzwRight"></div>
                               <br/><br/>          
              <div style="display:none" id="textNoOutInfo0">
              <div class="noOutReason">
              <span id="signfornoout0" style="font-size:25px;"></span>
               </div>
                   <div >
                <textarea  class="noOuttx" id="noOuttx0" name="noOuttx0"></textarea>
                   </div>	
                     </div>
                     </div>	
                     </div>
   <!-- 下一步按钮 -->
               <div id="scandal" style="text-align:center;">
               <div style="width:100%;height:50px;background-color:#FFFFFF;">
               </div> 
               <div style="text-align:center;background-color:#FFFFFF;">
                  <a href="javaScript:getPersonalNumPage(personalNumForOut)"><img src="../images/wx/next-page-green.png" style="width:200px; height:60px;"/></a>
               </div>
               <div style="width:100%;height:50px;background-color:#FFFFFF;">
               </div> 
               </div>
</body>
</html>