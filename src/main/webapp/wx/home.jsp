<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/magic-check.css"> 
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
        <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
      <jsp:include page="/js/inc.jsp"></jsp:include>
<title>居民出行调查</title>
<style>
body{  
  background: url(../images/wx/wxWelcome.png) no-repeat;
  height:100%;
  width:100%;
  overflow: hidden;
  background-size:cover;
}
</style>
<script>

$(document).ready(function(){
	//var href = window.location.href; 
	//alert(href);
	href=window.location.href; 
	var openId = '${param.openId}';
	//alert(openId);
	
	/* $.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/wxgetCityForWord",
		data : "",
		async : false,
		success : function(result) {
			var flag = result.flag;
			var city = result.message;
			var extended = result.extended;
			if(flag=="1"){
				//alert(city);
				var construction = "<p>北京城建设计发展集团股份有限公司</p>";
				var extendedWord = "<p>"+extended+"</p>";
				$('#unitWord').html(extended);
				$('#company').html(construction);
               
			}else{
				swal("很遗憾，出现错误！请您重新尝试", "", "error");
			}
		}
	}); */
	
	
	var wd = document.body.scrollWidth;
    var hg = document.body.scrollHeight;
    var de = window.screen.deviceXDPI;
	//alert(hg);
	if(hg>600&&hg<700){
		$('#size-newline').css("font-size","12px");
	}else if(hg>700&&hg<800){
		$('#size-newline').css("font-size","18px");
	}else if(hg>800){
		$('#size-newline').css("font-size","18px");
	}else{
		$('#size-newline').css("font-size","11.5px");
	}
});

</script>
</head>
<body>
<input id="openId" name="openId" value="${param.openId}" type="hidden"/>
<div style="width:100%; height:40%;"></div>
<div style="text-align:left;margin-left:10%;margin-right:10%;" style="width:100%;height:60%;" id="size-newline">       
             <p style="text-align:center;font-size:18px;font-weight: bold;">致市民朋友的一封信:</p><br/>
             <p style="text-indent:2em;font-size:16px;">尊敬的市民朋友，感谢您参与本次居民出行入户调查！本调查不涉及任何个人隐私信息，
                         调查内容仅用于为我市相关规划、管理及政策制定提供参考依据。</p><br/>
                <div style="color:#FFB56B">
              <!--<p style="text-indent:2em;">参与本次调查的家庭均可获得一份精美礼品。 ，并参与抽大奖活动，
                           调查问卷填写质量高的市民将有机会获得大奖，
                           一等奖（2名）为价值1万元的礼品，二等奖（4名）为价值5千元的礼品，三等奖（12名）为2-3千元的礼品。 --></p><br/>
               </div>
<!--            <div style="text-align:right">
                               <p>郑州市综合交通规划研究中心 </p> 
                               <p>北京城建设计发展集团股份有限公司</p>
          </div><br/><br/> -->
               <!-- <div class="home"></div> -->
               <div style="text-align:right;font-size: 16px;margin-right: 5%;font-weight: bold;" id="unitWord" >
                               <p>郑州市综合交通规划研究中心 </p> 
                               <p>北京城建设计发展集团股份有限公司</p>
               </div>
               <div style="text-align:right;font-size: 10px;margin-right: 12%;" id="company">
                              <!--  <p>郑州市综合交通规划研究中心 </p> 
                               <p>北京城建设计发展集团股份有限公司</p> -->
               </div>
               <div style="height:2%;"></div>
               <div style="text-align:center;margin-top:20%;margin-bottom:5px;width:100%;height:10%;background-color:#FFFFFF;">
                  <a href="javaScript:signState()"><img src="../images/wx/startwrite.png" style="width:200px; height:60px;"/></a>
               </div>
               <br/><br/>
    <!-- </div> --> 
</body>
</html>