<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<title>居民出行调查 </title>
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
    <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
     <!-- 页面样式 -->
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/magic-check.css">
    <!-- 遮罩层 -->
   <link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet" media="screen" /> 
   <script type="text/javascript" src="<%=path%>/js/wx/zDialog.js"></script>
   <script type="text/javascript" src="<%=path%>/js/wx/zDrag.js"></script>
   <jsp:include page="/js/inc.jsp"></jsp:include>     
<script type="text/javascript">
$(document).ready(function(){
	
});
 </script>
</head>

<body style="background:#FFFFFF;">
<br/>
<div class="prizeInfoAll">
<div class="prizeInfoAllTwo">
<span style="color:#000000;font-size: 16px;margin-left: 10%;">您的手机号是本次抽大奖的唯一凭证，</span>
<span style="color:#FF0000;font-size: 20px;">如不留手机号仅能获得礼品，不能抽大奖，</span>
<span style="color:#000000;font-size: 16px;">中奖后将按您留的收件地址、收件人配送奖品。注意：不管是否留手机号都要</span>
<span style="color:#FF0000;font-size: 20px;">点击下面的“提交问卷”</span>
<!-- <span style="color:#000000;font-size: 16px;">回答最后1个问题，然后再</span>
<span style="color:#FF0000;font-size: 20px;" >点击“全部填完”</span> -->
<span style="color:#000000;font-size: 16px;">按钮。否则问卷无法提交，不能获得礼品。</span>
<!-- <span style="color:#FF0000;font-size: 20px;margin-left: 0%;">如不留手机号仅能获得礼品，无法参与抽奖</span>
<span style="color:#000000;font-size: 20px;margin-left: 0%;">，中奖后将按您留的收件地址配送奖品。</span> -->
<!-- <p class="prizeInfopBig">感谢您参与本次居民出行调查!留下您的家庭地址及联系人姓名、手机号可参与抽奖，您的手机号将作为后期抽大奖的唯一凭证，如不留手机号仅能获得礼品，无法参与抽奖，中奖后将按您留的收件地址配送奖品。
</p> -->
<br/>
<!-- <p class="prizeInfopSmall">
（收件地址请务必填写家庭地址，并写清楚楼号及门牌号）
</p> -->
<br/>
<div class="hint">

</div>


<br/><br/>
<p class="prizeTitle">地址:</p>
<textarea class="textzw" rows="2" name="text" id="text" onblur="checkNameForPrize()"></textarea>

<div class="prizeZw"></div>
<p class="prizeTitle">收件人:</p>
<input class="prizeInput" name="people" id="people" onblur="checkNameForPrize(this)"/>

<div class="prizeZw"></div>
<p class="prizeTitle">联系电话:</p>
<input type="number" class="prizeInput" name="telphone" id="telphone" onblur="TextOnBlurShowCellphoneForPrize(this)" />
</div>

<br/><br/>
        <div class="next-page-address" id="scandal">
        <a href="javaScript:saveRewardInfo()"><img src="../images/wx/all-write.png" style="width:200px; height:60px;"/></a>
        </div>

</div>
</body>
</html>