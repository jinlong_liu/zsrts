<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/magic-check.css">
<script type="text/javascript" src="<%=path%>/js/wx/zDialog.js"></script>
<script type="text/javascript" src="<%=path%>/js/wx/zDrag.js"></script>
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
    <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript">
var weCity = '<%=session.getAttribute("weCityCode")%>';
$(document).ready(function(){
	var lnglat = '${param.lnglat}';
});
</script>
<title>居民出行调查</title>
</head>
<body style="background: #FFFFFF;">
<input id="familyNum" value="${param.familyNum }" type="hidden" />
	<div style="line-height:85px;width:100%;height:80px;background-color:#75C89A;">
<div style="width:30%;Float:left;">
<span style="font-size:85px;color:#FFFFFF;font-weight:bold;">&nbsp;1</span></div>
<div style="width:70%;text-align:center;">
<span style="color:#FCFEFD;font-weight:bold;">请您填写个人信息</span></div></div><br/>
		<div class="carinfo-margin">
			<div>
				<span class="spanw">1、性别：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSet0" id="set00"
						value="0"> <label for="set00">男</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSet0" id="set10"
						value="1"> <label for="set10">女</label>
				</div>
			</div>
			<br /> <span class="spanw">2、年龄：</span> <input type="number"
				class="inputw" id="age0" onblur="TextOnBlurShow(this)" /> <span id="checknum0" class="tsText"></span><br />
			<br/><br /> 
			<span class="spanw">3、是否常住本地？</span>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioLive0" id="Live00"
						value="0"> <label for="Live00">1.常住（本地居住超过6个月）</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioLive0" id="Live10"
						value="1"> <label for="Live10">2.暂住（本地居住不足6个月）</label><br />
				</div>
			</div><br/>
			<span class="spanw">4、职业：</span>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession0" id="profession00"
						value="0" onclick="professionAddress(0)"> <label for="profession00">1.企业公司员工</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession0" id="profession10"
						value="1" onclick="professionAddress(0)"> <label for="profession10">2.政府及事业员工</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession0" id="profession20"
						value="2" onclick="professionAddress(0)"> <label for="profession20">3.学生</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession0" id="profession30"
						value="3" onclick="professionAddress(0)"> <label for="profession30">4.退休</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession0" id="profession40"
						value="4" onclick="professionAddress(0)"> <label for="profession40">5.个体经营户或自由职业者</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession0" id="profession50"
						value="5" onclick="professionAddress(0)"> <label for="profession50">6.农林牧渔业人员</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession0" id="profession60"
						value="6" onclick="professionAddress(0)"> <label for="profession60">7.无业</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession0" id="profession70"
						value="7" onclick="professionAddress(0)"> <label for="profession70">8.其他</label><br />
				</div>
			</div>
			<br/>
						<span class="spanw">5、月收入：</span>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome0" id="income00"
						value="0"> <label for="income00">1.无收入</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome0" id="income10"
						value="1"> <label for="income10">2.3000元以下</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome0" id="income20"
						value="2"> <label for="income20">3.3000-5000元</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome0" id="income30"
						value="3"> <label for="income30">4.5000-8000元</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome0" id="income40"
						value="4"> <label for="income40">5.8000-10000元</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome0" id="income50"
						value="5"> <label for="income50">6.10000元以上</label><br />
				</div>
			</div>
						<br /> <span class="spanw">6、公交IC卡（选填）：</span> <input type="text"
				class="inputw" id="ic0" onblur="TextOnBlurShowIc(this)" /> <span id="checknum" class="tsText"></span><br />
			<br/><br /> 
			
			
                <input name="checkTelphone" id="checkTelphone0" type="checkbox" value="0" onclick="hOrBforTelphone(0)"/>
				<span>如您想参加出行轨迹调查,成为志愿者请勾选。</span>
				<br/>
				<div id="hiddenBlockforTelphone0" style="display:none">
				<span class="spanw">手机号：</span> 
				<input type="text"
					class="inputw" id="cellphone0"
					onblur="TextOnBlurShowCellphone(this)" /> 				
					<span id="checknum" class="tsText"></span><br /> <br />
				<div wdith="100%">
					<div style="width: 95%;">
						<p style="text-indent: 2em;">如您想完成本调查后继续参加出行轨迹调查，请填写手机号，该手机号将作为报名参加出行轨迹调查的凭证，参加出行轨迹调查将获得更为丰厚的奖励。</p>
					</div>
				</div>
				</div>

<br/><br/>

<div id="unitAddress0" style="display:none;">
<div><span class="spanw">单位地址：</span></div><br/>
</div>
<div id="schoolAddress0" style="display:none;">
<div><span class="spanw">学校地址：</span></div><br/>
</div>
<div style="border:2px solid #E5E5E5;width:90%;display:none;" id="txDiv0" onclick="skipMapPersonal(0)">
<textarea  rows="3" class="textareaAddress" id="tx0" name="tx0"></textarea>
<a href="javascript:skipMapPersonal(0)"><img src="../images/wx/postion.png"/></a>
<input id="langt0" name="langt0" type="hidden"/>
</div>
	</div>
	<br />
<div style="line-height:85px;width:100%;height:80px;background-color:#75C89A;">
<div style="width:30%;Float:left;">
<span style="font-size:85px;color:#FFFFFF;font-weight:bold;">&nbsp;2</span></div>
<div style="width:70%;text-align:center;">
<span style="color:#FCFEFD;font-weight:bold;">家庭成员2</span></div></div><br/>
		<div class="carinfo-margin">
			<div>
				<span class="spanw">1、性别：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSet1" id="set01"
						value="0"> <label for="set01">男</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSet1" id="set11"
						value="1"> <label for="set11">女</label>
				</div>
			</div>
			<br /> <span class="spanw">2、年龄：</span> <input type="number"
				class="inputw" id="age1" onblur="TextOnBlurShowAge(this)" /> <span id="checknum1" class="tsText"></span><br />
			<br/><br /> 
			<span class="spanw">3、是否常住本地？</span>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioLive1" id="Live01"
						value="0"> <label for="Live01">1.常住（本地居住超过6个月）</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioLive1" id="Live11"
						value="1"> <label for="Live11">2.暂住（本地居住不足6个月）</label><br />
				</div>
			</div><br/>
			<span class="spanw">4、职业：</span>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession1" id="profession01"
						value="0" onclick="professionAddress(1)"> <label for="profession01">1.企业公司员工</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession1" id="profession11"
						value="1" onclick="professionAddress(1)"> <label for="profession11">2.政府及事业员工</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession1" id="profession21"
						value="2" onclick="professionAddress(1)"> <label for="profession21">3.学生</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession1" id="profession31"
						value="3" onclick="professionAddress(1)"> <label for="profession31">4.退休</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession1" id="profession41"
						value="4" onclick="professionAddress(1)"> <label for="profession41">5.个体经营户或自由职业者</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession1" id="profession51"
						value="5" onclick="professionAddress(1)"> <label for="profession51">6.农林牧渔业人员</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession1" id="profession61"
						value="6" onclick="professionAddress(1)"> <label for="profession61">7.无业</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioProfession1" id="profession71"
						value="7" onclick="professionAddress(1)"> <label for="profession71">8.其他</label><br />
				</div>
			</div>
			<br/>
						<span class="spanw">5、月收入：</span>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome1" id="income01"
						value="0"> <label for="income01">1.无收入</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome1" id="income11"
						value="1"> <label for="income11">2.3000元以下</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome1" id="income21"
						value="2"> <label for="income21">3.3000-5000元</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome1" id="income31"
						value="3"> <label for="income31">4.5000-8000元</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome1" id="income41"
						value="4"> <label for="income41">5.8000-10000元</label><br />
				</div>
			</div>
						<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioIncome1" id="income51"
						value="5"> <label for="income51">6.10000元以上</label><br />
				</div>
			</div>
						<br /> <span class="spanw">6、公交IC卡（选填）：</span> <input type="text"
				class="inputw" id="ic1" onblur="TextOnBlurShowIc(this)" /> <span id="checknum" class="tsText"></span><br />
			<br/><br /> 
			
			
               <input name="checkTelphone" id="checkTelphone1" type="checkbox" value="0" onclick="hOrBforTelphone(1)"/>
				<span>如您想参加出行轨迹调查,成为志愿者请勾选。</span>
				<br/>
				<div id="hiddenBlockforTelphone1" style="display:none">
				<span class="spanw">手机号：</span> 
				<input type="text"
					class="inputw" id="cellphone1"
					onblur="TextOnBlurShowCellphone(this)" /> 				
					<span id="checknum" class="tsText"></span><br /> <br />
				<div wdith="100%">
					<div style="width: 95%;">
						<p style="text-indent: 2em;">如您想完成本调查后继续参加出行轨迹调查，请填写手机号，该手机号将作为报名参加出行轨迹调查的凭证，参加出行轨迹调查将获得更为丰厚的奖励。</p>
					</div>
				</div>
				</div>


<br/><br/>

<div id="unitAddress1" style="display:none;">
<div><span class="spanw">单位地址：</span></div><br/>
</div>
<div id="schoolAddress1" style="display:none;">
<div><span class="spanw">学校地址：</span></div><br/>
</div>
<div style="border:2px solid #E5E5E5;width:90%;display:none;" id="txDiv1" onclick="skipMapPersonal(1)">
<textarea  rows="3" class="textareaAddress" id="tx1" name="tx1"></textarea>
<a href="javascript:skipMapPersonal(1)"><img src="../images/wx/postion.png"/></a>
<input id="langt1" name="langt1" type="hidden"/>
</div>

	</div>
	<br /><br/>
	<div style="text-align: center;float:center">
		<a href="javaScript:savePersonalInfo()"><img
			src="../images/wx/next-page-green.png"
			style="width: 200px; height: 60px;" /></a>
	</div>

	<br />
	<br />
	<br />


</body>
</html>