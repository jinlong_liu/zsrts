<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
    <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
<jsp:include page="/js/inc.jsp"></jsp:include>

    <link rel="stylesheet" href="http://cache.amap.com/lbs/static/main1119.css"/>
    <script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=3067ac7a693712711f75a87e43d6e5f9"></script>
    <script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>

<script type="text/javascript">
var messagelon = "";
var messagelat = "";
var messageslon = "";
var messageslat = "";
$(document).ready(function() {
$.ajax({
	type : "POST",
	dataType : "json",
	url : root+"/services/wxGetLnglat",
	data : "",
	async : false,
	success : function(result) {
		var flag = result.flag;
		messagelon = result.messagelon;
		messagelat = result.messagelat;
		messageslon = result.messageslon;
		messageslat = result.messageslat;
		if(flag==1){
			 //$('#sign').val("1");
			 //alert(messageslat);
		}else{
			swal("", "抱歉，公众号出现异常数据的存储，请等待。。。", "error");
		}			
	}
	});
	reckon();
});
function reckon(){
	   var map = new AMap.Map("container", {
	        resizeEnable: true,
	        zoom: 13
	    });
	    map.setFitView();
	    var lnglat = new AMap.LngLat(messagelon, messagelat);
	    var echolocation = lnglat.distance([messageslon, messageslat]);
	    //alert(echolocation);
		var data={
				'echolocation' : echolocation
				};
	    $.ajax({
	    	type : "POST",
	    	dataType : "json",
	    	url : root+"/services/wxSavePositionDeviate",
	    	data : data,
	    	async : false,
	    	success : function(result) {
	    		var flag = result.flag;
	    		if(flag==1){
	    			 $('#sign').val("1");
	    			 //alert("成功！");
	    		}else{
	    			swal("", "抱歉，公众号出现异常数据的存储，请等待。。。", "error");
	    		}			
	    	}
	    	});
}
</script>
<title>居民出行调查</title>
</head>
<body style="background: #FFFFFF;">

<input id="sign" value="0" type="hidden"/>
<!-- 地图隐藏 -->
<div style="display:none;">
<div id="container"></div>
</div>

<br/><br/>
<div class="divCenter">
<span class="spanw">您家里有几口人？</span>
</div>
 <div class="left-50-top">
     <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioFamilyNum" id="familyNum0" value="1"> 
    <label for="familyNum0">1人</label></div></div>
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioFamilyNum" id="familyNum1" value="2"> 
    <label for="familyNum1">2人</label><br/>
    </div></div>
    
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioFamilyNum" id="familyNum2" value="3"> 
    <label for="familyNum2">3人</label></div></div>
       
           <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioFamilyNum" id="familyNum3" value="4"> 
    <label for="familyNum3">4人</label></div></div>
    
        <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioFamilyNum" id="familyNum4" value="5"> 
    <label for="familyNum4">5人</label></div></div>
       
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioFamilyNum" id="familyNum5" value="6"> 
    <label for="familyNum5">5人以上</label></div></div>
 </div><br/>
 <!-- 家庭人数除您自家成员外,包括每周五天以上居住在您家的和调查当日临时居住在您家里的所有人。如保姆、同学、亲戚朋友等。 -->
 <div style="width:100%;">
   <div style="width:80%;margin-left:10%;">
  <p style="text-indent:2em;">注：家庭人数包括每周五天以上居住在您家里的所有人（也包括保姆、同学、亲戚朋友等），但不包括长期在外工作的家庭成员。</p>
  </div>
</div> 
<div style="height;10px;"></div>
<br/><br/>
               <div style="text-align:center">
                  <a href="javaScript:getFamilyInfo()"><img src="../images/wx/next-page-green.png" style="width:200px; height:60px;"/></a>
               </div>
<br/>
<div style="width:100%;">
   <div style="width:80%;margin-left:10%;">
  <p style="text-indent:2em;">接下来请先填写您的个人信息,然后依次填写您家庭其他成员的个人信息。</p>
  </div>
</div>
<br/>
</body>
</html>