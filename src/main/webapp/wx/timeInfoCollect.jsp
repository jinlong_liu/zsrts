<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<title>居民出行调查 </title>
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
    <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
     <!-- 页面样式 -->
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/magic-check.css">
    <!-- 遮罩层 -->
   <link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet" media="screen" /> 
   <script type="text/javascript" src="<%=path%>/js/wx/zDialog.js"></script>
   <script type="text/javascript" src="<%=path%>/js/wx/zDrag.js"></script>
   <jsp:include page="/js/inc.jsp"></jsp:include>     
<script type="text/javascript">
$(document).ready(function(){
	
	var familyId = '${param.family}';
	//alert(familyId);
	var data={
			'familyId' : familyId
			};
	$.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/wxgetPersonalRandomOutInfo",
		data : data,
		async : false,
		success : function(result) {
			
		}
		});
	
	
});
 </script>
</head>

<body style="background:#FFFFFF;">
<input id="openId" name="openId" value="${param.family}" type="hidden"/>
<br/>
<br/>
<br/>
<br/>
<div class="prizeInfoAll">
<div class="prizeInfoAllTwo">
<p class="prizeInfopBig">得到了ID</p><br/>
<p class="prizeInfopSmall">
</p>
<br/><br/>
<br/><br/>
        <div class="next-page-address" >
        <a href="javaScript:void(0)"><img src="../images/wx/timeInfo.png" style="width:200px; height:60px;"/></a>
        </div>

</div>
</body>
</html>