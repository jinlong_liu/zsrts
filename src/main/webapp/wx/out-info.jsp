<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
    <link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <link rel="stylesheet" href="<%=path%>/css/wx/magic-check.css"> 
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
        <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
      <jsp:include page="/js/inc.jsp"></jsp:include>
<title>居民出行调查</title>
<script type="text/javascript">

/*屏蔽手机返回键*/
XBack = {};
(function(XBack) {
	XBack.STATE = 'x - back';
	XBack.element;

	XBack.onPopState = function(event) {
		event.state === XBack.STATE && XBack.fire();
		XBack.record(XBack.STATE); //初始化事件时，push一下  
	};
	XBack.record = function(state) {
		history.pushState(state, null, location.href);
	};
	XBack.fire = function() {
		var event = document.createEvent('Events');
		event.initEvent(XBack.STATE, false, false);
		XBack.element.dispatchEvent(event);
	};
	XBack.listen = function(listener) {
		XBack.element.addEventListener(XBack.STATE, listener, false);
	};
	XBack.init = function() {
		XBack.element = document.createElement('span');
		window.addEventListener('popstate', XBack.onPopState);
		XBack.record(XBack.STATE);
	};

})(XBack); // 可以当做js文件 引入这段

XBack.init();
XBack.listen(function() {
});
</script>

</head>
<body>
<div style="width:100%; height:100%;background:#ecfff4;text-align:center;">
<img src="../images/wx/three-starwrite.png" class="fillPictrue" style="width:100%; height:60%;"/><br/><br/><br/>
<span style="font-size:25px;color:#75c89a">第三部分：出行信息</span><br/><br/><br/>
               <div style="text-align:center">
                  <a href="javaScript:goOutHint()"><img src="../images/wx/pi-startwrite.png" style="width:200px; height:60px;"/></a>
               </div>
</div>
</body>
</html>