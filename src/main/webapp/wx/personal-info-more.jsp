<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<!-- 遮罩层 -->
<link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet"
	media="screen" />
<!-- 弹框 -->
<script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/sweetalert.css">
<!-- lghDialog -->
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/lhgdialog.css">
<script type="text/javascript"
	src="<%=path%>/js/wx/lhgcore.lhgdialog.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/gaode-map.css">
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
<script type="text/javascript" src="<%=path%>/js/wx/zDialog.js"></script>
<script type="text/javascript" src="<%=path%>/js/wx/zDrag.js"></script>
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript">
	/*屏蔽手机返回键*/
	XBack = {};
	(function(XBack) {
		XBack.STATE = 'x - back';
		XBack.element;

		XBack.onPopState = function(event) {
			event.state === XBack.STATE && XBack.fire();
			XBack.record(XBack.STATE); //初始化事件时，push一下  
		};
		XBack.record = function(state) {
			history.pushState(state, null, location.href);
		};
		XBack.fire = function() {
			var event = document.createEvent('Events');
			event.initEvent(XBack.STATE, false, false);
			XBack.element.dispatchEvent(event);
		};
		XBack.listen = function(listener) {
			XBack.element.addEventListener(XBack.STATE, listener, false);
		};
		XBack.init = function() {
			XBack.element = document.createElement('span');
			window.addEventListener('popstate', XBack.onPopState);
			XBack.record(XBack.STATE);
		};

	})(XBack); // 可以当做js文件 引入这段

	XBack.init();
	XBack.listen(function() {
	});

	var weCity = '<%=session.getAttribute("weCityCode")%>';
	
	$(document)
			.ready(
					function() {
						/* 				var address = '${param.address}';
						 var lnglat = '${param.lnglat}';
						 var num = '${param.num}';
						 var height = window.screen.availHeight;
						 if (address != null && address != '' && address != undefined
						 && address != '点击地图获取位置/输入关键字获取位置') {
						 if (num == 2) {
						 $('#unit').val(address);
						 }
						 if (num == 3) {
						 $('#school').val(address);
						 }
						 } */
						var personalNum = 3;
						var num = 3;
						var zsPeople = 3;
						var familyNum = '${param.familyNum}';
						//alert(familyNum);
						if (familyNum == 6) {
							familyNum = 5;
						}
						if (familyNum > 3) {
							for (var i = 0; i < (familyNum - num); i++) {
								var djgPeople = zsPeople + 1;
								var createYm = document.createElement('div');
								createYm.innerHTML = "<div id='z"+personalNum+"'><div id='del"
										+ personalNum
										+ "' style='line-height:85px;width:100%;height:80px;background-color:#75C89A;'><div style='width:30%;Float:left;'><span id='big"+personalNum+"' style='font-size:85px;color:#FFFFFF;font-weight:bold;'>&nbsp;"
										+ djgPeople
										+ "</span></div><div style='width:70%;text-align:center;'><span id='small"+personalNum+"' style='color:#FCFEFD;font-weight:bold;'>第"
										+ djgPeople
										+ "位家庭成员</span></div></div><br/><div id='delm"+personalNum+"'><div class='carinfo-margin'><div id='xb"+personalNum+"'><span class='spanw'>1、性别：</span></div><div class='divBorder'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioSet"+personalNum+"' id='set0"+personalNum+"' value='0'> <label for='set0"+personalNum+"'>男</label></div></div><div class='divBorder'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioSet"+personalNum+"' id='set1"+personalNum+"' value='1'> <label for='set1"+personalNum+"'>女</label></div></div><br /> <span class='spanw'>2、年龄：</span> <input type='number' class='inputw' id='age"
										+ personalNum
										+ "' onblur='TextOnBlurShowAge(this,"+personalNum+")' /> <span id='checknum"+personalNum+"' class='tsText'></span><br /><br/><br /> <div id='cz"+personalNum+"'><span class='spanw'>3、是否常住本地？</span></div><div class='divBorder'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioLive"+personalNum+"' id='Live0"+personalNum+"' value='0'> <label for='Live0"+personalNum+"'>常住（本地居住超过6个月）</label></div></div><div class='divBorder'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioLive"+personalNum+"' id='Live1"+personalNum+"' value='1'> <label for='Live1"+personalNum+"'>暂住（本地居住不足6个月）</label><br /></div></div><br/> <div id='zy"+personalNum+"'><span class='spanw'>4、职业：</span></div><div class='divBorder' id='professionForNone0"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioProfession"
										+ personalNum
										+ "' id='profession0"
										+ personalNum
										+ "' value='0' onclick='professionAddress("
										+ personalNum
										+ ")'> <label for='profession0"+personalNum+"'>企业公司员工</label></div></div><div class='divBorder' id='professionForNone1"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioProfession"
										+ personalNum
										+ "' id='profession1"
										+ personalNum
										+ "' value='1' onclick='professionAddress("
										+ personalNum
										+ ")'> <label for='profession1"+personalNum+"'>政府及事业单位员工</label><br /></div></div><div class='divBorder' id='professionForNone2"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioProfession"
										+ personalNum
										+ "' id='profession2"
										+ personalNum
										+ "' value='2' onclick='professionAddress("
										+ personalNum
										+ ")'> <label for='profession2"+personalNum+"'>学生</label><br /></div></div><div class='divBorder' id='professionForNone3"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioProfession"
										+ personalNum
										+ "' id='profession3"
										+ personalNum
										+ "' value='3' onclick='professionAddress("
										+ personalNum
										+ ")'> <label for='profession3"+personalNum+"'>退休</label><br /></div></div><div class='divBorder' id='professionForNone4"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioProfession"
										+ personalNum
										+ "' id='profession4"
										+ personalNum
										+ "' value='4' onclick='professionAddress("
										+ personalNum
										+ ")'> <label for='profession4"+personalNum+"'>个体经营户或自由职业者</label><br /></div></div><div class='divBorder' id='professionForNone5"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioProfession"
										+ personalNum
										+ "' id='profession5"
										+ personalNum
										+ "' value='5' onclick='professionAddress("
										+ personalNum
										+ ")'> <label for='profession5"+personalNum+"'>农林牧渔业人员</label><br /></div></div><div class='divBorder' id='professionForNone6"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioProfession"
										+ personalNum
										+ "' id='profession6"
										+ personalNum
										+ "' value='6' onclick='professionAddress("
										+ personalNum
										+ ")'> <label for='profession6"+personalNum+"'>无业</label><br /></div></div><div class='divBorder' id='professionForNone7"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioProfession"
										+ personalNum
										+ "' id='profession7"
										+ personalNum
										+ "' value='7' onclick='professionAddress("
										+ personalNum
										+ ")'> <label for='profession7"+personalNum+"'>其他(注明)</label><br /></div></div> <div class='divBorder' style='display:none;' id='qtjtfsttxforblock"+personalNum+"'><textarea class='qtjtfsText' id='qtjtfsttx"+personalNum+"' name='qtjtfsttx"+personalNum+"'></textarea></div><br/><div id='includeforblock"+personalNum+"'><span class='spanw'>5、月收入：</span><div class='divBorder' id='IncomeForNone0"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioIncome"+personalNum+"' id='income0"+personalNum+"' value='0'> <label for='income0"+personalNum+"' class='sr0'>无收入</label></div></div><div class='divBorder' id='IncomeForNone1"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioIncome"+personalNum+"' id='income1"+personalNum+"' value='1'> <label for='income1"+personalNum+"' class='sr1'>3000元以下</label><br /></div></div><div class='divBorder' id='IncomeForNone2"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioIncome"+personalNum+"' id='income2"+personalNum+"' value='2'> <label for='income2"+personalNum+"' class='sr2'>3000-5000元</label><br /></div></div><div class='divBorder' id='IncomeForNone3"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioIncome"+personalNum+"' id='income3"+personalNum+"' value='3'> <label for='income3"+personalNum+"' class='sr3'>5000-8000元</label><br /></div></div><div class='divBorder' id='IncomeForNone4"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioIncome"+personalNum+"' id='income4"+personalNum+"' value='4'> <label for='income4"+personalNum+"' class='sr4'>8000-10000元</label><br /></div></div><div class='divBorder' id='IncomeForNone5"+personalNum+"'><div style='height: 15px;'></div><div class='propertyLeft'><input class='magic-radio' type='radio' name='radioIncome"+personalNum+"' id='income5"+personalNum+"' value='5'> <label for='income5"+personalNum+"' class='sr5'>10000元以上</label><br /></div></div></div><br/><div style='display:none;'><input name='checkTelphone' id='checkTelphone"
										+ personalNum
										+ "' type='checkbox' value='0' onclick='hOrBforTelphone("
										+ personalNum
										+ ")'/><span>如您想参加出行轨迹调查,成为志愿者请勾选。</span><br/><div id='hiddenBlockforTelphone"+personalNum+"' style='display:none'><span class='spanw'>APP登录：</span><input type='text' class='inputw' id='cellphone"+personalNum+"' disabled='disabled' value="+djgPeople+" /><span id='checknum' class='tsText'></span><br/> <br/><div wdith='100%'><div style='width: 95%;'><p style='text-indent: 2em;'>如您想完成本调查后继续参加出行轨迹调查，请填写手机号，该手机号将作为报名参加出行轨迹调查的凭证，参加出行轨迹调查将获得更为丰厚的奖励。</p></div></div></div><br/><br/></div> <div id='unitAddress"+personalNum+"' style='display:none;'><div><span class='spanw'>6、上班地址：</span></div></div><div id='schoolAddress"+personalNum+"' style='display:none;'><div><span class='spanw'>5、学校地址：</span></div><br/></div><div onclick='skipMapPersonal("
										+ personalNum
										+ ")' style='border:2px solid #E5E5E5;width:90%;display:none;' id='txDiv"
										+ personalNum
										+ "' ><textarea  disabled='disabled' rows='3' class='textareaAddress' id='tx"+personalNum+"' name='tx"+personalNum+"'></textarea><a href='javascript:skipMapPersonal("
										+ personalNum
										+ ")'><img src='../images/wx/postion.png'/></a><input id='langt"+personalNum+"' name='langt"+personalNum+"' type='hidden'/><br/></div><br/></div><br/></div>";
								document.getElementById("xym").appendChild(
										createYm);
								personalNum++;
								zsPeople++;
							}
						}
						
						var weCityCode = '<%=session.getAttribute("weCityCode")%>';
					    //alert(weCityCode);
					     if(weCityCode=='0371'){
					      $('.sr0').text("2000元以下");
					      $('.sr1').text("2000-3000元");
					     }else if(weCityCode=='0398'){
					    	 $('.sr0').text("1000元以下");
					 		$('.sr1').text("1000-3000元");
					 		$('.sr2').text("3000-5000元");
					 		$('.sr3').text("5000-10000元");
					 		$('.sr4').text("10000-20000元");
					 		$('.sr5').text("20000元以上");
					     }
						
					});
</script>
<title>居民出行调查</title>
</head>
<body style="background: #FFFFFF;">

	<div style="width: 100%; height: 100%; display: none;" id="adress">
		<input id="numId" name="numId" value="${param.numId}" type="hidden" />
		<div id="container" tabindex="0"></div>
		<div class='panel'>
			<table width="100%" border="0">
				<tr style="width: 100%;" height="20%">
					<td width="15%"><input type="image" style="width: 100%;"
						src="../images/mapImageLeft.png" onclick="closemapforfamily()" />
					</td>
					<td style="width: 60%;"><textarea id='returnInput'
							style="width: 100%;" value='点击地图获取位置/输入关键字获取位置'></textarea></td>
					<td width="25%"><input type="image" id="btn"
						style="width: 100%;" src="../images/mapSubmit.png"
						onclick="submitmapforfamily()" /></td>
				</tr>
			</table>
			<input placeholder="请输入关键字进行搜索" id="lnglat" hidden="hidden"
				type="text" />
			<div id='message'></div>
		</div>
		<script type="text/javascript"
			src="http://webapi.amap.com/maps?v=1.3&key=3067ac7a693712711f75a87e43d6e5f9"></script>
		<script type="text/javascript" src="<%=path%>/js/wx/personalone.js"></script>
		<script type="text/javascript"
			src="http://webapi.amap.com/demos/js/liteToolbar.js"></script>
	</div>


     <input id="familyNum" name="familyNum" value="${param.familyNum}" type="hidden"/>
	<div id="xym">
		<div id="z0">
			<div
				style="line-height: 85px; width: 100%; height: 80px; background-color: #75C89A;">
				<div style="width: 30%; Float: left;">
					<span id="big0"
						style="font-size: 85px; color: #FFFFFF; font-weight: bold;">&nbsp;1</span>
				</div>
				<div style="width: 70%; text-align: center;">
					<span id="small0" style="color: #FCFEFD; font-weight: bold;">请您填写个人信息</span>
				</div>
			</div>
			<br />
			<div class="carinfo-margin">
				<div id="xb0">
					<span class="spanw">1、性别：</span>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioSet0"
							id="set00" value="0"> <label for="set00">男</label>
					</div>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioSet0"
							id="set10" value="1"> <label for="set10">女</label>
					</div>
				</div>
				<br /> <span class="spanw">2、年龄：</span> <input type="number"
					class="inputw" id="age0" onblur="TextOnBlurShowAge(this,0)" /> <span
					id="checknum0" class="tsText"></span><br /> <br /> <br /> 
					<div id="cz0">
					<span
					class="spanw">3、是否常住本地？</span>
				</div>
				<div class="divBorder" >
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioLive0"
							id="Live00" value="0"> <label for="Live00">常住（本地居住超过6个月）</label>
					</div>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioLive0"
							id="Live10" value="1"> <label for="Live10">暂住（本地居住不足6个月）</label><br />
					</div>
				</div>
				<br /> 
				<div id="zy0">
				<span class="spanw">4、职业：</span>
				</div>
				<div class="divBorder" id="professionForNone00">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession0"
							id="profession00" value="0" onclick="professionAddress(0)">
						<label for="profession00">企业公司员工</label>
					</div>
				</div>
				<div class="divBorder" id="professionForNone10">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession0"
							id="profession10" value="1" onclick="professionAddress(0)">
						<label for="profession10">政府及事业单位员工</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone20">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession0"
							id="profession20" value="2" onclick="professionAddress(0)">
						<label for="profession20">学生</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone30">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession0"
							id="profession30" value="3" onclick="professionAddress(0)">
						<label for="profession30">退休</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone40">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession0"
							id="profession40" value="4" onclick="professionAddress(0)">
						<label for="profession40">个体经营户或自由职业者</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone50">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession0"
							id="profession50" value="5" onclick="professionAddress(0)">
						<label for="profession50">农林牧渔业人员</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone60">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession0"
							id="profession60" value="6" onclick="professionAddress(0)">
						<label for="profession60">无业</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone70">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession0"
							id="profession70" value="7" onclick="professionAddress(0)">
						<label for="profession70">其他(注明)</label><br />
					</div>
				</div>
				
				<div class="divBorder" style="display:none;" id="qtjtfsttxforblock0">
					<textarea class="qtjtfsText" id="qtjtfsttx0" name="qtjtfsttx0"></textarea>	
			</div>
				
				<br />
				<div id="includeforblock0">
					<span class="spanw">5、月收入：</span>
					<div class="divBorder"  id="IncomeForNone00">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome0"
								id="income00" value="0"> <label for="income00" class='sr0'>无收入</label>
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone10">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome0"
								id="income10" value="1"> <label for="income10" class='sr1'>3000元以下</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone20">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome0"
								id="income20" value="2"> <label for="income20" class='sr2'>3000-5000元</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone30">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome0"
								id="income30" value="3"> <label for="income30" class='sr3'>5000-8000元</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone40">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome0"
								id="income40" value="4"> <label for="income40" class='sr4'>8000-10000元</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone50">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome0"
								id="income50" value="5"> <label for="income50" class='sr5'>10000元以上</label><br />
						</div>
					</div>
				</div>
				<br />
				<div style="display: none;">
					<input name="checkTelphone" id="checkTelphone0" type="checkbox"
						value="0" onclick="hOrBforTelphone(0)" /> <span>如您想参加出行轨迹调查,成为志愿者请勾选。</span>
					<br />
					<div id="hiddenBlockforTelphone0" style="display: none">
						<span class="spanw">APP登录：</span> <input type="text"
							class="inputw" id="cellphone0" value="0" disabled="disabled" /> <span
							id="checknum" class="tsText"></span><br /> <br />
						<div wdith="100%">
							<div style="width: 95%;">
								<p style="text-indent: 2em;">如您想完成本调查后继续参加出行轨迹调查，请填写手机号，该手机号将作为报名参加出行轨迹调查的凭证，参加出行轨迹调查将获得更为丰厚的奖励。</p>
							</div>
						</div>
					</div>

					<br />
				</div>
				<div id="unitAddress0" style="display: none;">
					<div>
						<span class="spanw">6、上班地址：</span>
					</div>
				</div>
				<div id="schoolAddress0" style="display: none;">
					<div>
						<span class="spanw">5、学校地址：</span>
					</div>
					<br />
				</div>
				<div style="border: 2px solid #E5E5E5; width: 90%; display: none;"
					id="txDiv0" onclick="skipMapPersonal(0)">
					<textarea disabled="disabled" rows="3" class="textareaAddress"
						id="tx0" name="tx0" ></textarea>
					<a href="javascript:skipMapPersonal(0)"><img
						src="../images/wx/postion.png" /></a> <input id="langt0"
						name="langt0" type="hidden" /> <br />
				</div>
			</div>
			<br />
		</div>

		<!-- 第二个成员 -->
		<div id="z1">
			<div
				style="line-height: 85px; width: 100%; height: 80px; background-color: #75C89A;"
				id="two0">
				<div style="width: 30%; Float: left;">
					<span id="big1"
						style="font-size: 85px; color: #FFFFFF; font-weight: bold;">&nbsp;2</span>
				</div>
				<div style="width: 70%; text-align: center;">
					<span id="small1" style="color: #FCFEFD; font-weight: bold;">第2位家庭成员</span>
				</div>
			</div>
			<br />
			<div class="carinfo-margin" id="two1">
				<div id="xb1">
					<span class="spanw">1、性别：</span>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioSet1"
							id="set01" value="0"> <label for="set01">男</label>
					</div>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioSet1"
							id="set11" value="1"> <label for="set11">女</label>
					</div>
				</div>
				<br /> <span class="spanw">2、年龄：</span> <input type="number"
					class="inputw" id="age1" onblur="TextOnBlurShowAge(this,1)" /> <span
					id="checknum1" class="tsText"></span><br /> <br /> <br /> 
					<div id="cz1">
					<span
					class="spanw">3、是否常住本地？</span>
				    </div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioLive1"
							id="Live01" value="0"> <label for="Live01">常住（本地居住超过6个月）</label>
					</div>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioLive1"
							id="Live11" value="1"> <label for="Live11">暂住（本地居住不足6个月）</label><br />
					</div>
				</div>
				<br /> 
				<div id="zy1">
				<span class="spanw">4、职业：</span>
				</div>
				<div class="divBorder" id="professionForNone01">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession1"
							id="profession01" value="0" onclick="professionAddress(1)">
						<label for="profession01">企业公司员工</label>
					</div>
				</div>
				<div class="divBorder" id="professionForNone11">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession1"
							id="profession11" value="1" onclick="professionAddress(1)">
						<label for="profession11">政府及事业单位员工</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone21">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession1"
							id="profession21" value="2" onclick="professionAddress(1)">
						<label for="profession21">学生</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone31">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession1"
							id="profession31" value="3" onclick="professionAddress(1)">
						<label for="profession31">退休</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone41">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession1"
							id="profession41" value="4" onclick="professionAddress(1)">
						<label for="profession41">个体经营户或自由职业者</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone51">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession1"
							id="profession51" value="5" onclick="professionAddress(1)">
						<label for="profession51">农林牧渔业人员</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone61">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession1"
							id="profession61" value="6" onclick="professionAddress(1)">
						<label for="profession61">无业</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone71">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession1"
							id="profession71" value="7" onclick="professionAddress(1)">
						<label for="profession71">其他(注明)</label><br />
					</div>
				</div>
				
				<div class="divBorder" style="display:none;" id="qtjtfsttxforblock1">
					<textarea class="qtjtfsText" id="qtjtfsttx1" name="qtjtfsttx1"></textarea>	
			</div>
				
				<br />
				<div id="includeforblock1">
					<span class="spanw">5、月收入：</span>
					<div class="divBorder" id="IncomeForNone01">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome1"
								id="income01" value="0"> <label for="income01" class='sr0'>无收入</label>
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone11">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome1"
								id="income11" value="1"> <label for="income11" class='sr1'>3000元以下</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone21">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome1"
								id="income21" value="2"> <label for="income21" class='sr2'>3000-5000元</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone31">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome1"
								id="income31" value="3"> <label for="income31" class='sr3'>5000-8000元</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone41">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome1"
								id="income41" value="4"> <label for="income41" class='sr4'>8000-10000元</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone51">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome1"
								id="income51" value="5"> <label for="income51" class='sr5'>10000元以上</label><br />
						</div>
					</div>
				</div>
				<br />


				<div style="display: none;">
					<input name="checkTelphone" id="checkTelphone1" type="checkbox"
						value="0" onclick="hOrBforTelphone(1)" /> <span>如您想参加出行轨迹调查,成为志愿者请勾选。</span>
					<br />
					<div id="hiddenBlockforTelphone1" style="display: none">
						<span class="spanw">APP登录：</span> <input type="text"
							class="inputw" id="cellphone1" value="1" disabled="disabled" /> <span
							id="checknum" class="tsText"></span><br /> <br />
						<div wdith="100%">
							<div style="width: 95%;">
								<p style="text-indent: 2em;">如您想完成本调查后继续参加出行轨迹调查，请填写手机号，该手机号将作为报名参加出行轨迹调查的凭证，参加出行轨迹调查将获得更为丰厚的奖励。</p>
							</div>
						</div>
					</div>


					<br />
				</div>


				<div id="unitAddress1" style="display: none;">
					<div>
						<span class="spanw">6、上班地址：</span>
					</div>
				</div>
				<div id="schoolAddress1" style="display: none;">
					<div>
						<span class="spanw">5、学校地址：</span>
					</div>
					<br />
				</div>
				<div style="border: 2px solid #E5E5E5; width: 90%; display: none;"
					id="txDiv1" onclick="skipMapPersonal(1)">
					<textarea disabled="disabled" rows="3" class="textareaAddress"
						id="tx1" name="tx1" ></textarea>
					<a href="javascript:skipMapPersonal(1)"><img
						src="../images/wx/postion.png" /></a> <input id="langt1"
						name="langt1" type="hidden" /> <br />
				</div>

			</div>
			<br />
		</div>
		<!-- 第三个人员 -->
		<div id="z2">
			<div
				style="line-height: 85px; width: 100%; height: 80px; background-color: #75C89A;">
				<div style="width: 30%; Float: left;">
					<span id="big2"
						style="font-size: 85px; color: #FFFFFF; font-weight: bold;">&nbsp;3</span>
				</div>
				<div style="width: 70%; text-align: center;">
					<span id="small2" style="color: #FCFEFD; font-weight: bold;">第3位家庭成员</span>
				</div>
			</div>
			<br />
			<div class="carinfo-margin">
				<div id="xb2">
					<span class="spanw">1、性别：</span>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioSet2"
							id="set02" value="0"> <label for="set02">男</label>
					</div>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioSet2"
							id="set12" value="1"> <label for="set12">女</label>
					</div>
				</div>
				<br /> <span class="spanw">2、年龄：</span> <input type="number"
					class="inputw" id="age2" onblur="TextOnBlurShowAge(this,2)" /> <span
					id="checknum2" class="tsText"></span><br /> <br /> <br /> 
					<div id="cz2">
					<span
					class="spanw">3、是否常住本地？</span>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioLive2"
							id="Live02" value="0"> <label for="Live02">常住（本地居住超过6个月）</label>
					</div>
				</div>
				<div class="divBorder">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioLive2"
							id="Live12" value="1"> <label for="Live12">暂住（本地居住不足6个月）</label><br />
					</div>
				</div>
				<br /> 
				<div id="zy2">
				<span class="spanw">4、职业：</span>
				</div>
				<div class="divBorder" id="professionForNone02">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession2"
							id="profession02" value="0" onclick="professionAddress(2)">
						<label for="profession02">企业公司员工</label>
					</div>
				</div>
				<div class="divBorder" id="professionForNone12">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession2"
							id="profession12" value="1" onclick="professionAddress(2)">
						<label for="profession12">政府及事业单位员工</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone22">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession2"
							id="profession22" value="2" onclick="professionAddress(2)">
						<label for="profession22">学生</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone32">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession2"
							id="profession32" value="3" onclick="professionAddress(2)">
						<label for="profession32">退休</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone42">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession2"
							id="profession42" value="4" onclick="professionAddress(2)">
						<label for="profession42">个体经营户或自由职业者</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone52">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession2"
							id="profession52" value="5" onclick="professionAddress(2)">
						<label for="profession52">农林牧渔业人员</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone62">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession2"
							id="profession62" value="6" onclick="professionAddress(2)">
						<label for="profession62">无业</label><br />
					</div>
				</div>
				<div class="divBorder" id="professionForNone72">
					<div style="height: 15px;"></div>
					<div class="propertyLeft">
						<input class="magic-radio" type="radio" name="radioProfession2"
							id="profession72" value="7" onclick="professionAddress(2)">
						<label for="profession72">其他(注明)</label><br />
					</div>
				</div>
				
				<div class="divBorder" style="display:none;" id="qtjtfsttxforblock2">
					<textarea class="qtjtfsText" id="qtjtfsttx2" name="qtjtfsttx2"></textarea>	
			</div>
				
				<br />
				<div id="includeforblock2">
					<span class="spanw">5、月收入：</span>
					<div class="divBorder" id="IncomeForNone02">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome2"
								id="income02" value="0"> <label for="income02" class='sr0'>无收入</label>
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone12">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome2"
								id="income12" value="1"> <label for="income12" class='sr1'>3000元以下</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone22">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome2"
								id="income22" value="2"> <label for="income22" class='sr2'>3000-5000元</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone32">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome2"
								id="income32" value="3"> <label for="income32" class='sr3'>5000-8000元</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone42">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome2"
								id="income42" value="4"> <label for="income42" class='sr4'>8000-10000元</label><br />
						</div>
					</div>
					<div class="divBorder" id="IncomeForNone52">
						<div style="height: 15px;"></div>
						<div class="propertyLeft">
							<input class="magic-radio" type="radio" name="radioIncome2"
								id="income52" value="5"> <label for="income52" class='sr5'>10000元以上</label><br />
						</div>
					</div>
				</div>
				<br />

				<div style="display: none;">
					<input name="checkTelphone" id="checkTelphone2" type="checkbox"
						value="0" onclick="hOrBforTelphone(2)" /> <span>如您想参加出行轨迹调查,成为志愿者请勾选。</span>
					<br />
					<div id="hiddenBlockforTelphone2" style="display: none">


						<span class="spanw">APP登录：</span> <input type="text"
							class="inputw" id="cellphone2" disabled="disabled" value="2" />
						<span id="checknum" class="tsText"></span><br /> <br />
						<div wdith="100%">
							<div style="width: 95%;">
								<p style="text-indent: 2em;">如您想完成本调查后继续参加出行轨迹调查，请您记住账号与密码，这将作为报名参加出行轨迹调查的凭证，参加出行轨迹调查将获得更为丰厚的奖励。</p>
							</div>
						</div>
					</div>


					<br /> <br />
				</div>
				<div id="unitAddress2" style="display: none;">
					<div>
						<span class="spanw">6、上班地址：</span>
					</div>
				</div>
				<div id="schoolAddress2" style="display: none;">
					<div>
						<span class="spanw">5、学校地址：</span>
					</div>
					<br />
				</div>
				<div style="border: 2px solid #E5E5E5; width: 90%; display: none;"
					id="txDiv2" onclick="skipMapPersonal(2)">
					<textarea disabled="disabled" rows="3" class="textareaAddress"
						id="tx2" name="tx2" ></textarea>
					<a href="javascript:skipMapPersonal(2)"><img
						src="../images/wx/postion.png" /></a> <input id="langt2"
						name="langt2" type="hidden" /> <br />
				</div>

			</div>
			<br />
		</div>
	</div>

	<!-- 结束 -->
	<c:choose>
		<c:when test="${param.familyNum==6 }">
			<span><a href="javaScript:addPersonalYm()"><div
						class="divCenter" style="margin-right: 4%;">
						<img src="../images/wx/addFamily.png" style="width: 90%" />
					</div></a></span>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
	<br />

	<div style="text-align: center; float: center" id="scandal">
		<a href="javaScript:savePersonalInfo()"><img
			src="../images/wx/next-page-green.png"
			style="width: 200px; height: 60px;" /></a>
	</div>
	<br />
	<br />
	<br />


</body>
</html>