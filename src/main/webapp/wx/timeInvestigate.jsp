<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<title>居民出行调查 </title>
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
    <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
     <!-- 页面样式 -->
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/magic-check.css">
    <!-- 遮罩层 -->
   <link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet" media="screen" /> 
   <script type="text/javascript" src="<%=path%>/js/wx/zDialog.js"></script>
   <script type="text/javascript" src="<%=path%>/js/wx/zDrag.js"></script>
   <jsp:include page="/js/inc.jsp"></jsp:include>     
<script type="text/javascript">
 </script>
</head>

<body style="background:#FFFFFF;">
<input id="familyId" name="family" value="${param.family}" type="hidden"/>
<br/>
<br/>
<br/>
<br/>
<div class="prizeInfoAll">
<div class="prizeInfoAllTwo">
<p class="prizeInfopBig">您已完成居民出行入户调查,为进一步掌握我市市民的对不同交通方式的选择意愿,
为后期交通管理、政策制定提供参考依据,邀请您继续参与本次综合交通的时间价值调查专项。参与本次调查的市民
还将获得一份额外的奖励。</p><br/>
<p class="prizeInfopSmall">
</p>
<br/><br/>
<br/><br/>
        <div class="next-page-address" >
        <a href="javaScript:joinTimeInfoCollect()"><img src="../images/wx/timeInfo.png" style="width:200px; height:60px;"/></a>
        </div>

</div>
</body>
</html>