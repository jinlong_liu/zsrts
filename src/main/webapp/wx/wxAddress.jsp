<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<title>居民出行调查</title>
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
<!-- 弹框 -->
<script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/sweetalert.css">
<!-- 页面样式 -->
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
 <!-- 遮罩层 -->
<link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet" media="screen" /> 
<script type="text/javascript" src="<%=path%>/js/wx/zDialog.js"></script>
<script type="text/javascript" src="<%=path%>/js/wx/zDrag.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/gaode-map.css">
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript">
	$(document).ready(function() {
		//swal("这是一个信息提示框!");
		//swal("NO!", "弹出了一个错误提示框", "error");
		//swal("Good!", "弹出了一个操作成功的提示框", "success");

		/* 	swal(
		 {
		 title : "Are you sure?",
		 text : "You will not be able to recover this imaginary file!",
		 type : "warning",
		 showCancelButton : true,
		 confirmButtonColor : "#DD6B55",
		 confirmButtonText : "Yes, delete it!",
		 cancelButtonText : "No, cancel plx!",
		 closeOnConfirm : true,
		 closeOnCancel : true
		 },
		 function(isConfirm) {
		 if (isConfirm) {
		 alert('11111');
		 } else {
		 alert('22222');
		 }
		 }); */

		/* swal({   
			title: "Good!",   
			text: '自定义<span style="color:red">图片</span>、<a href="#">HTML内容</a>。<br/>5秒后自动关闭。',   
			imageUrl: "../images/thumbs-up.jpg",
			html: true,
			timer: 5000,   
			showConfirmButton: false
		});  */
		/* 	    var address = '${param.address}';
		 var lnglat = '${param.lnglat}';
		 if(address!=null&&address!=''&&address!=undefined&&address!='点击地图获取位置/输入关键字获取位置'){
		 $('#textId').val(address);
		 }   */
		 
		 
		 
		 
		 
	});
</script>
</head>
<body style="background: #75C89A;" id="scandal">
	<div style="width: 100%; height: 100%; display: none;" id="adress">
		<input id="numId" name="numId" value="${param.numId}" type="hidden" />

		<div id="container" tabindex="0"></div>
		<div class='panel'>
			<table width="100%" border="0">
				<tr style="width: 100%;" height="20%">
					<td width="15%"><input type="image" style="width: 100%;"
						src="../images/mapImageLeft.png" onclick="closemap()" /></td>
					<td style="width: 60%;"><textarea id='returnInput'
							style="width: 100%;" value='点击地图获取位置/输入关键字获取位置'></textarea></td>
					<td width="25%"><input type="image" id="btn"
						style="width: 100%;" src="../images/mapSubmit.png"
						onclick="submitmap()" /></td>
				</tr>
			</table>
			<input placeholder="请输入关键字进行搜索" id="lnglat" hidden="hidden"
				type="text" />
			<div id='message'></div>
		</div>
		<script type="text/javascript"
			src="http://webapi.amap.com/maps?v=1.3&key=3067ac7a693712711f75a87e43d6e5f9"></script>
		<script type="text/javascript" src="<%=path%>/js/wx/wxAddress.js"></script>
		<script type="text/javascript"
			src="http://webapi.amap.com/demos/js/liteToolbar.js"></script>

	</div>
	<input type="hidden" id="lnglat" name="lnglat" value="${param.lnglat}" />
	<div>
		<br />
		<br />
		<br />
		<div style="text-align: center;">
			<span class="writeAdress">请填写您的家庭住址</span>
		</div>
		<br />

		<div class="adressCenter" onclick="getAddress('1')">
			<div>
				<textarea disabled="disabled" rows="3" class="textareaAddress"
					id="textId" name="textName">
</textarea>
				<a href="javaScript:getAddress('1')"><img
					src="../images/wx/postion.png" /></a>
			</div>
		</div>
		<br />

		<!-- <div style="text-align:center;">
点击右侧进入地图页面选取地址
</div> -->

	</div>
	<br />
	<br />
	<br />
	<br />
	<br />
	<div class="next-page-address" >
		<a href="javaScript:pickAddress()"><img
			src="../images/wx/next-page.png" style="width: 200px; height: 60px;" /></a>
	</div>
	</div>
</body>
</html>