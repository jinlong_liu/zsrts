<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
 <!-- 遮罩层 -->
<link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet" media="screen" /> 
    <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript">
	function wxChangStage(){
		$.ajax({
			type : "POST",
			dataType : "json",
			url : root+"/services/wxChangStage",
			data : "",
			async : false,
			success : function(result) {
				var flag = result.flag;
				var message = result.message;
				//alert(flag);
				if(flag=="1"){
                  
						var url = root+"/wx/finish.jsp";
						window.location.href=url;	

				}else{
					swal("很遗憾，出现错误！请您重新尝试", "", "error");
				}
			}
		});
	}
</script>
<title>居民出行调查</title>
</head>
<body>
<div style="text-align: center;width: 100%;height: 100%;background-color: #FFFFFF; "> 
        <a href="javaScript:wxChangStage()"><img src="../images/wx/all-write.png" style="width:200px; height:60px;margin-top: 50%;"/></a>
</div>
</body>
</html>