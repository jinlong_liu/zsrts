<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/magic-check.css"> 
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
        <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
    <jsp:include page="/js/inc.jsp"></jsp:include>
<title>居民出行调查</title>
<style>
body{  
  background: url(../images/wx/wxWelcome.png) no-repeat;
  height:100%;
  width:100%;
  overflow: hidden;
  background-size:cover;
}
</style>
<script>

$(document).ready(function(){
	//var href = window.location.href; 
	//alert(href);
	href=window.location.href; 
	var openId = '${param.openId}';
	//alert(openId);
	
	/* $.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/wxgetCityForWord",
		data : "",
		async : false,
		success : function(result) {
			var flag = result.flag;
			var city = result.message;
			var extended = result.extended;
			if(flag=="1"){
				//alert(city);
				var construction = "<p>北京城建设计发展集团股份有限公司</p>";
				var extendedWord = "<p>"+extended+"</p>";
				$('#unitWord').html(extended);
				$('#company').html(construction);
               
			}else{
				swal("很遗憾，出现错误！请您重新尝试", "", "error");
			}
		}
	}); */
	
	
	var wd = document.body.scrollWidth;
    var hg = document.body.scrollHeight;
    var de = window.screen.deviceXDPI;
	//alert(hg);
	if(hg>600&&hg<700){
		$('#size-newline').css("font-size","12px");
	}else if(hg>700&&hg<800){
		$('#size-newline').css("font-size","18px");
	}else if(hg>800){
		$('#size-newline').css("font-size","18px");
	}else{
		$('#size-newline').css("font-size","11.5px");
	}
});

</script>
</head>
<body>
<input id="openId" name="openId" value="${param.openId}" type="hidden"/>
<div style="width:100%; height:33%;"></div>
<div style="text-align:left;margin-left:10%;margin-right:10%;" style="width:100%;height:60%;" id="size-newline">       
             <p style="text-align:center;font-size:18px;font-weight: bold;">致市民朋友的一封信:</p><br/>
             <p style="text-indent:2em;font-size:16px;">尊敬的市民朋友，感谢您参与本次调查！作为一名郴州市民您应该能深切体会到，近年来郴州市社会经济快速增长、城市变化日新月异，城市人口规模、机动车保有量大幅增长，城市不断繁荣的同时交通拥堵问题也不断恶化！</p><br/> 
                
              <p style="text-indent:2em;font-size:16px;">近年来，我市在交通建设方面投入了巨大的力量，并已经在城市道路建设、公共交通建设等方面取得了一定的成绩。然而，改善城市交通问题是一项复杂的系统工程，单纯靠交通基础设施建设无法从根本上解决城市交通拥堵问题。参照国内外大城市的科学发展经验，必须首先开展综合性的城市交通调查工作进而全面系统地掌握城市居民出行特征及城市交通运行特征，并在此基础上制定一系列交通发展政策及计划。我市于2017年10月启动了全市综合交通调查工作，共涉及10个专项调查，您即将参与的是本次调查的居民出行调查专项。该专项是对您家庭的基本信息及家庭内全部成员的一日完整出行信息进行调查。每一份用心填写的问卷都将为改善我市交通环境、促进我市城市发展贡献一份力量。再次对您的积极配合深表感谢！</p>
              
              <!-- <p style="text-indent:2em;font-size:16px;">本调查不涉及任何个人隐私信息，调查内容仅为我市轨道交通线网规划编制及
                           其他相关规划，管理及政策制定提供参考依据。感谢您的参与！</p> 
               <br/>-->
               
              <!-- <div class="home"></div> -->
               <div style="text-align:right;font-size: 18px;margin-right: 5%;font-weight: bold;" id="unitWord">
                               <p>郴州市综合交通调查办公室</p> 
                               <p>郴州市城市建设投资发展集团有限公司</p>
               </div>
              <!--  <div style="text-align:right;font-size: 10px;margin-right: 12%;" id="company"> -->
                          
               </div>
               <div style="height:2%;"></div>
               <div style="text-align:center;margin-bottom:5px;width:100%;height:10%;background-color:#FFFFFF;">
                  <a href="javaScript:signState()"><img src="../images/wx/startwrite.png" style="width:200px; height:60px;"/></a>
               </div>
               <br/><br/>
    <!-- </div> --> 
</body>
</html>