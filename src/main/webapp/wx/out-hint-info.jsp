<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
    <link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <link rel="stylesheet" href="<%=path%>/css/wx/magic-check.css"> 
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
        <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
      <jsp:include page="/js/inc.jsp"></jsp:include>
<title>居民出行调查</title>

  <script type="text/javascript">
  $(document).ready(function(){
  $.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/wxgetMetroSign",
		data : "",
		async : false,
		success : function(result) {
			var flag = result.flag;
			var prize = result.prize;
			var prizeWord = "";
			if(flag=="1"){
				if(prize==1){
					//prizeWord = "<span style='color: #FFFFFF;font-size:17px;'>出行为一次有明确目的的外出活动，包括上班、上学、回家、接送小孩、购物、外出就餐、车站接人等，也包括接送小孩后、就餐后、购物后回到工作单位或家里。</span><span style='color: #FFFFFF;font-size:20px;font-weight: bold;' >发生这些外出活动均需要在后面问卷中作为出行信息填写，否则无法获得礼品和参与抽大奖活动!</span>";
					prizeWord = "<span style='color: #FFFFFF;font-size:17px;'>出行为一次有明确目的的外出活动，包括上班、上学、回家、接送小孩、购物、外出就餐、车站接人等，也包括接送小孩后、就餐后、购物后回到工作单位或家里。</span><span style='color: #FFFFFF;font-size:20px;font-weight: bold;' >发生这些外出活动均需要在后面问卷中作为出行信息填写!</span>";

				}else{
					prizeWord = "<span style='color: #FFFFFF;font-size:17px;'>出行为一次有明确目的的外出活动，包括上班、上学、回家、接送小孩、购物、外出就餐、车站接人等，也包括接送小孩后、就餐后、购物后回到工作单位或家里。</span><span style='color: #FFFFFF;font-size:20px;font-weight: bold;' >发生这些外出活动均需要在后面问卷中作为出行信息填写!</span>";
				}
				//alert(prizeWord);
				document.getElementById('prizeWordId').innerHTML = prizeWord;
			}
		}
	});	
  });
  </script>
</head>
<body>
<div style="width:100%;height:100%;">
<div style="width:100%;height:70%;background-color:#75C89A;">
<div style="width:90%;margin-left:5%;font-size:13px;">
<br/><br/><p style="text-indent:2em;font-size:21px;font-weight:bold;color:red;">接下来请您家里的每一位成员(不含6岁以下的)分别填写自己一整天的全部出行信息。</p>
<br/>
<br/>

<div id="prizeWordId">

</div>
</div>
</div>
<div style="width:100%;height:30%;background-color:#FFFFFF;">
	<br/><br/><br/><div style="text-align: center;float:center">
		<a href="javaScript:getPersonalList()"><img
			src="../images/wx/next-page-green.png"
			style="width: 200px; height: 60px;" /></a>
	</div>
</div>
</div>
</body>
</html>