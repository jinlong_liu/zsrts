<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
 <!-- 遮罩层 -->
   <link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet" media="screen" /> 
       <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript">
$(document).ready(function(){
	 var weCityCode = '<%=session.getAttribute("weCityCode")%>';
	    //alert(weCityCode);
if(weCityCode=='0371'){
	
}else{
	$('.gl').text("3、过去12个月行驶里程（公里）：");
}
});
</script>
<title>居民出行调查</title>
</head>
<body style="background: #FFFFFF;">

	<input id="jymCarNum" value="${param.number }" type="hidden" />
	<input id="num" value="2" type="hidden">
	<div id="z0">
		<div class="one-carinfo"></div>
		<br />
		<div class="carinfo-margin">
			<div id="sx0">
				<span class="spanw">1、车辆属性：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx0" id="sx00"
						value="0"> <label for="sx00">私家车</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx0" id="sx10"
						value="1"> <label for="sx10">单位车</label>
				</div>
			</div>
			<br /> 
			
			
			
		<div id="pl0">
			<span class="spanw">2、排量（升）：</span> 
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCar0" id="car00"
						value="0"> <label for="car00">1.0以下</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCar0" id="car10"
						value="1"> <label for="car10">1.0(含1.0)-1.6</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCar0" id="car20"
						value="2"> <label for="car20">1.6(含1.6)-2.0(含2.0)</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCar0" id="car30"
						value="3"> <label for="car30">2.0以上</label>
				</div>
			</div>
				
				
				
				<br />
			<br /> <div class="spanw"><span class="gl">3、过去12个月行驶里程（万公里）：</span></div><br /> <input
				type="number" class="inputwc" id="showIdGl0"
				onblur="TextOnBlurShow(this)" /><br />
			<br /> 
			<div id="cw0">
			<span class="spanw">4、在居住地是否有固定停车位：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw0" id="cw00"
						value="0"> <label for="cw00">是</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw0" id="cw10"
						value="1"> <label for="cw10">否</label><br />
				</div>
			</div>
		</div>
	</div>
	<br />
	<br />
	<div id="z1">
		<div class="two-carinfo"></div>
		<br />
		<div class="carinfo-margin">
			<div id="sx1">
				<span class="spanw">1、车辆属性：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx1" id="sx01"
						value="0"> <label for="sx01">私家车</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioSx1" id="sx11"
						value="1"> <label for="sx11">单位车</label>
				</div>
			</div><br/>
			
			<div id="pl1">
			<span class="spanw">2、排量（升）：</span> 
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCar1" id="car01"
						value="0"> <label for="car01">1.0以下</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCar1" id="car11"
						value="1"> <label for="car11">1.0(含1.0)-1.6</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCar1" id="car21"
						value="2"> <label for="car21">1.6(含1.6)-2.0(含2.0)</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCar1" id="car31"
						value="3"> <label for="car31">2.0以上</label>
				</div>
			</div>
				
				<br />
			<br /> <div class="spanw"><span class="gl">3、过去12个月行驶里程（万公里）：</span></div><br /> <input
				type="number" class="inputwc" id="showIdGl1"
				onblur="TextOnBlurShow(this)" /><br />
			<br /> 
			<div id="cw1">
			<span class="spanw">4、在居住地是否有固定停车位：</span>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw1" id="cw01"
						value="0"> <label for="cw01">是</label>
				</div>
			</div>
			<div class="divBorder">
				<div style="height: 15px;"></div>
				<div class="propertyLeft">
					<input class="magic-radio" type="radio" name="radioCw1" id="cw11"
						value="1"> <label for="cw11">否</label>
				</div>
			</div>
		</div>
	</div>
	<br />
	<br />
    <br/>
	<div style="text-align: center" id="scandal">
		<a href="javaScript:saveCarInfo()"><img
			src="../images/wx/next-page-green.png"
			style="width: 200px; height: 60px;" /></a>
	</div>
	<br />
	<br />
	<br />
</body>
</html>