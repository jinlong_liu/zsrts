<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
        <link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/magic-check.css"> 
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
        <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
        <!-- 统一引入 -->
    <jsp:include page="/js/inc.jsp"></jsp:include>
    
     <!-- 遮罩层 -->
<link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet" media="screen" />    
    <style>
    body{
    background-color:#FFFFFF;
    }
    </style>
    <script type="text/javascript">
    

    /*屏蔽手机返回键*/
    XBack = {};
    (function(XBack) {
    	XBack.STATE = 'x - back';
    	XBack.element;

    	XBack.onPopState = function(event) {
    		event.state === XBack.STATE && XBack.fire();
    		XBack.record(XBack.STATE); //初始化事件时，push一下  
    	};
    	XBack.record = function(state) {
    		history.pushState(state, null, location.href);
    	};
    	XBack.fire = function() {
    		var event = document.createEvent('Events');
    		event.initEvent(XBack.STATE, false, false);
    		XBack.element.dispatchEvent(event);
    	};
    	XBack.listen = function(listener) {
    		XBack.element.addEventListener(XBack.STATE, listener, false);
    	};
    	XBack.init = function() {
    		XBack.element = document.createElement('span');
    		window.addEventListener('popstate', XBack.onPopState);
    		XBack.record(XBack.STATE);
    	};

    })(XBack); // 可以当做js文件 引入这段

    XBack.init();
    XBack.listen(function() {
    });

    
    
    
     var lnglat = "";
     var district = "";
	var gainGis = self.setInterval("gain()",50);
	jQuery('#activity_pane').showLoading();
function gain(){
    lnglat = $('#lnglat').val();
    district = $('#district').val();
    //alert(district);
    if(lnglat!=""&&district!=""){
		//alert("进入检查方法");
		window.clearInterval(gainGis);
		var data={
				'lnglat' : lnglat,
				'district' : district
		};
		//alert(lnglat);
	$.ajax({
		type : "POST",
		dataType : "json",
		url : root+"/services/wxSubmitLnglat",
		data : data,
		async : true,
		success : function(result) {
			var flag = result.flag;
			if(flag==1){
				 $('#sign').val("1");
				 jQuery('#activity_pane').hideLoading();  
			}			
		}
		});
		
	}
  } 
    
    
    
   
    </script>
<title>居民出行调查</title>
</head>
<body>

<input id="sign" value="0" type="hidden"/>

<!-- 隐藏地图，获取当前位置。 -->
		<div style="width: 100%; height: 100%; display: none;" id="adress">
		<div id="container" tabindex="0"></div>
		<div class='panel'>
			<table width="100%" border="0">
				<tr style="width: 100%;" height="20%">
					<td width="15%"><input type="image" style="width: 100%;"
						src="../images/mapImageLeft.png" onclick="closemap()" /></td>
					<td style="width: 60%;"><textarea id='returnInput'
							style="width: 100%;" value='点击地图获取位置/输入关键字获取位置'></textarea></td>
					<td width="25%"><input type="image" id="btn"
						style="width: 100%;" src="../images/mapSubmit.png"
						onclick="submitmap()" /></td>
				</tr>
			</table>
			<input placeholder="请输入关键字进行搜索" id="lnglat" hidden="hidden"
				type="text" />
				<input id="district" hidden="hidden"
				type="text" />
			<div id='message'></div>
		</div>
		<script type="text/javascript"
			src="http://webapi.amap.com/maps?v=1.3&key=3067ac7a693712711f75a87e43d6e5f9"></script>
		<script type="text/javascript" src="<%=path%>/js/wx/wxTjCar.js">
		</script>
		<script type="text/javascript"
			src="http://webapi.amap.com/demos/js/liteToolbar.js"></script>
	</div>



<br/><br/><br/>
<div class="divCenter">
<span class="spanw">您家有几辆小汽车？</span>
</div>
 <div class="left-50">
      <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioCarNum" id="carNum" value="99"> 
    <label for="carNum">0辆</label></div></div>
     <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioCarNum" id="carNum0" value="1"> 
    <label for="carNum0">1辆</label></div></div>
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioCarNum" id="carNum1" value="2"> 
    <label for="carNum1">2辆</label><br/>
    </div></div>
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioCarNum" id="carNum2" value="3"> 
    <label for="carNum2">3辆</label></div></div>
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioCarNum" id="carNum3" value="4"> 
    <label for="carNum3">3辆以上</label></div></div>
 </div><br/><br/><br/>
 </div>
               <div style="text-align:center">
                  <a href="javaScript:getCarNumber()"><img src="../images/wx/next-page-green.png" style="width:200px; height:60px;"/></a>
               </div>

</body>
</html>