<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
 <!-- 遮罩层 -->
<link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet" media="screen" /> 
    <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript">
	
</script>
<title>居民出行调查</title>
</head>
<body style="background: #FFFFFF;"> 
<br/><br/><br/>
<div class="divCenter">
<span class="spanw">您家有几辆电动车？</span>
</div>
 <div class="left-50" style="margin-top:2%;">
      <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioEleNum" id="eleNum" value="0" > 
    <label for="eleNum">0辆</label></div></div>
     <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioEleNum" id="eleNum0" value="1" > 
    <label for="eleNum0">1辆</label></div></div>
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioEleNum" id="elerNum1" value="2" > 
    <label for="elerNum1">2辆</label><br/>
    </div></div>
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioEleNum" id="eleNum2" value="3" > 
    <label for="eleNum2">3辆</label></div></div>
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="radioEleNum" id="eleNum3" value="4" > 
    <label for="eleNum3">3辆以上</label></div></div>
 </div>
 <br/>
 </div>
 <br/>
 <div id="hiddenele" style="display: none;">
 <div class="divCenter">
<span class="spanw">其中每天都使用的有几辆？</span>
</div>
 <div class="left-50" style="margin-top:2%;">
      <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="everydayEleNum" id="everydayNum" value="0" onclick="checkelenum();"> 
    <label for="everydayNum">0辆</label></div></div>
     <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="everydayEleNum" id="everydayNum0" value="1" onclick="checkelenum();"> 
    <label for="everydayNum0">1辆</label></div></div>
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="everydayEleNum" id="everydayNum1" value="2" onclick="checkelenum();"> 
    <label for="everydayNum1">2辆</label><br/>
    </div></div>
    <div class="divBorder">
    <div style="height:15px;">
    </div>
    <div class="propertyLeft">
    <input class="magic-radio" type="radio" name="everydayEleNum" id="everydayNum2" value="3" onclick="checkelenum();"> 
    <label for="everydayNum2">3辆</label></div></div>
 </div>
</div>

<!--  <div class="left-50">
    <div class="divBorder" id="addColor" >
    <div style="height:15px;" >
    </div>
    <div class="divCenter">
    <input type="radio" name="everydayEleNum" id="everydayNum" onclick="addColor()" value="99" style="display: none;" > 
    <label for="everydayNum" style="display:block;width:100%;">0辆</label></div></div>
 
     <div class="divBorder" id="addColor1">
    <div style="height:15px;">
    </div>
    <div class="divCenter">
    <input type="radio" name="everydayEleNum"  id="everydayNum0" value="1" style="display: none;width:900px" onclick="addColor()"> 
    <label for="everydayNum0" style="display:block;width:100%;">1辆</label></div></div>
    
    <div class="divBorder" id="addColor2">
    <div style="height:15px;">
    </div>
    <div class="divCenter">
    <input type="radio" name="everydayEleNum" id="everydayNum1" value="2" style="display: none;" onclick="addColor()"> 
    <label for="everydayNum1" style="display:block;width:100%;">2辆</label><br/>
    </div></div>
    <div class="divBorder" id="addColor3">
    <div style="height:15px;">
    </div>
    <div class="divCenter">
    <input type="radio" name="everydayEleNum" id="everydayNum2" value="3" style="display: none;" onclick="addColor()"> 
    <label for="everydayNum2" style="display:block;width:100%;">3辆</label></div></div>
 </div> -->
 
 
 <br/><br/><br/>
               <div style="text-align:center" id="scandal">
                  <a href="javaScript:getElectricInfo()"><img src="../images/wx/next-page-green.png" style="width:200px; height:60px;"/></a>
               </div>
<br/><br/><br/>
</body>
</html>