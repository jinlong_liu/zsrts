<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta name="viewport"
	content="initial-scale=1.0, user-scalable=no, width=device-width">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<link rel="stylesheet" href="<%=path%>/css/wx/magic-check.css">
<script type="text/javascript" src="<%=path%>/js/wx/zDialog.js"></script>
<script type="text/javascript" src="<%=path%>/js/wx/zDrag.js"></script>
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
<!-- 弹框 -->
<script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/sweetalert.css">
	
		<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/gaode-map.css">
<jsp:include page="/js/inc.jsp"></jsp:include>
<style type="text/css">
/* body, html, #container {
	width: 100%;
	height: 100%;
	margin: 0px
}

.panel {
    width: 100%;
	background-color: #ddf;
	color: #333;
	border: 1px solid silver;
	box-shadow: 3px 4px 3px 0px silver;
	position: absolute;
	top: 0px;
	left: 0px;
	border-radius: 5px;
	overflow: hidden;
	line-height: 20px;
}

#returnInput {
	width: 100%;
	height: 30px;
	border: 0;
} */
</style>
 <script type="text/javascript">
 var numId="";
  $(document).ready(function(){
    numId = ${param.numId};
    //alert(numId);
    });	
 </script>

<title>居民出行调查</title>

</head>
<body>
	<input id="numId" name="numId" value="${param.numId}" type="hidden" />

	<div id="container" tabindex="0"></div>
	<div class='panel'>
			<table width="100%" border="0">
				<tr style="width: 100%;" height="20%">
					<td width="15%">
						<input type="image" style="width: 100%;" src="../images/mapImageLeft.png" onclick="closemapforfamily()"/>
					</td>
					<td style="width: 60%;">
						<textarea id='returnInput' style="width: 100%;" value='点击地图获取位置/输入关键字获取位置'></textarea>
					</td>
					<td width="25%">
						<input type="image" id="btn" style="width: 100%;" src="../images/mapSubmit.png" onclick="submitmapforfamily(numId)"/>
					</td>
				</tr>
			</table>
			<input placeholder="请输入关键字进行搜索" id="lnglat" hidden="hidden" type="text" />
			<div id='message'></div>
		</div>
	<script type="text/javascript"
		src="http://webapi.amap.com/maps?v=1.3&key=3067ac7a693712711f75a87e43d6e5f9"></script>
	<script type="text/javascript">
	function cl(){
		parentDialog.close();
	}
	var marker = null;
		//加载地图
		var map = new AMap.Map('container', {
			resizeEnable : true,
			zoom : 13,
			//center : [ 116.39, 39.9 ]
		});

		//初始化加载地图时，若center及level属性缺省，地图默认显示用户当前城市范围
		var map = new AMap.Map('mapContainer', {
			resizeEnable : true
		});
		//地图中添加地图操作ToolBar插件
		map.plugin([ 'AMap.ToolBar' ], function() {
			//设置地位标记为自定义标记
			var toolBar = new AMap.ToolBar();
			map.addControl(toolBar);
		});

		//加载地图，调用浏览器定位服务  定位当前位置并获取经纬度
		var map, geolocation;
		map = new AMap.Map('container', {
			resizeEnable : true
		});
		map.plugin('AMap.Geolocation', function() {
			geolocation = new AMap.Geolocation({
				enableHighAccuracy : true,//是否使用高精度定位，默认:true
				timeout : 10000, //超过10秒后停止定位，默认：无穷大
				buttonOffset : new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
				zoomToAccuracy : true, //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
				showCircle: false,      //去掉蓝色圆圈
				buttonPosition : 'RB'
			});
			map.addControl(geolocation);
			geolocation.getCurrentPosition();
			AMap.event.addListener(geolocation, 'complete', onComplete);//返回定位信息
			AMap.event.addListener(geolocation, 'error', onError); //返回定位出错信息
		});
		//解析定位结果
		function onComplete(data) {
			var str = [ '定位成功' ];
			map.setZoomAndCenter(14, [data.position.getLng(), data.position.getLat()]);
			str.push('经度：' + data.position.getLng());
			str.push('纬度：' + data.position.getLat());
			console.log(data.position.getLat());
			if (data.accuracy) {
				str.push('精度：' + data.accuracy + ' 米');
			}//如为IP精确定位结果则没有精度信息
			str.push('是否经过偏移：' + (data.isConverted ? '是' : '否'));
			//alert(str);
			document.getElementById("lnglat").value = data.position.getLng()
					+ "," + data.position.getLat();
			//将定位的位置写入文本框
			AMap.plugin('AMap.Geocoder', function() {
				var geocoder = new AMap.Geocoder({
					//city : "010"//城市，默认：“全国”
				});
				var lnglat = data.position.getLng() + ","
						+ data.position.getLat();
				var input = document.getElementById('returnInput');
				geocoder.getAddress(lnglat, function(status, result) {
					if (status == 'complete') {
						input.value = result.regeocode.formattedAddress
						//alert(result.regeocode.formattedAddress);
						message.innerHTML = ''
					} else {
						message.innerHTML = '无法获取地址'
					}
				});
			});
			identification = 'true';
		}
		//解析定位错误信息
		function onError(data) {
			document.getElementById('returnInput').innerHTML = '定位失败';
		}

		//输入联想并提供经纬度
		AMap.plugin([ 'AMap.Autocomplete', 'AMap.PlaceSearch' ], function() {
			var windowsArr = [];
			marker = [];
			var autoOptions = {
				//city : "010", //城市，默认全国
				input : "returnInput"//使用联想输入的input的id
			};
			autocomplete = new AMap.Autocomplete(autoOptions);
			var placeSearch = new AMap.PlaceSearch({
				//city : '北京',
				map : map
			})
			AMap.event.addListener(autocomplete, "select", function(m) {
				//TODO 针对选中的poi实现自己的功能
				//placeSearch.search(e.poi.name)
				AMap
				.plugin(
						'AMap.Geocoder',
						function() {
							var geocoder = new AMap.Geocoder(
									{

									});
							marker = new AMap.Marker({
								map : map,
								bubble : true
							})
							var input = document
									.getElementById('returnInput');
							lo();
							function lo(e) {
								var address = m.poi.name;
								//alert(address);
								geocoder
										.getLocation(
												address,
												function(
														status,
														result) {
													//alert(address);
													if (status == 'complete'
															&& result.geocodes.length) {
														marker
																.setPosition(result.geocodes[0].location);
														map
																.setCenter(marker
																		.getPosition())
														document
																.getElementById("lnglat").value = result.geocodes[0].location;
														document
																.getElementById('message').innerHTML = ''
													}
												})
							}
						});
			});
		});
		//绑定blur事件支持用户手动输入
		/* $('#returnInput')
				.bind(
						"click keyup change mouseover  mouseout",
						function() {
							map.remove(marker);
							AMap
									.plugin(
											'AMap.Geocoder',
											function() {
												var geocoder = new AMap.Geocoder(
														{
															//city : "010"//城市，默认：“全国”
														});
												marker = new AMap.Marker({
													map : map,
													bubble : true
												})
												var input = document
														.getElementById('returnInput');
												lo();
												function lo(e) {
													var address = input.value;
													geocoder
															.getLocation(
																	address,
																	function(
																			status,
																			result) {
																		//alert(address);
																		if (status == 'complete'
																				&& result.geocodes.length) {
																			marker
																					.setPosition(result.geocodes[0].location);
																			map
																					.setCenter(marker
																							.getPosition())
																			document
																					.getElementById("lnglat").value = result.geocodes[0].location;
																			document
																					.getElementById('message').innerHTML = ''
																		}
																	})
												}
											});
						}); */
		
		//添加默认城市  点击事件完成地址与经纬度带入
		AMap.plugin('AMap.Geocoder', function() {
			var geocoder = new AMap.Geocoder({
				//city : "010"//城市，默认：“全国”
			});
			marker = new AMap.Marker({
				map : map,
				bubble : true
			})
			//将获取的地址写到文本框
			var input = document.getElementById('returnInput');
			var message = document.getElementById('message');
			map.on('click', function(e) {
				marker.setPosition(e.lnglat);
				geocoder.getAddress(e.lnglat, function(status, result) {
					if (status == 'complete') {
						input.value = result.regeocode.formattedAddress
						message.innerHTML = ''
					} else {
						message.innerHTML = '无法获取地址'
					}
				})
			})
			input.onchange = function(e) {
				var address = input.value;
				geocoder.getLocation(address, function(status, result) {
					if (status == 'complete' && result.geocodes.length) {
						marker.setPosition(result.geocodes[0].location);
						map.setCenter(marker.getPosition())
						message.innerHTML = ''
					} else {
						message.innerHTML = '无法获取位置'
					}
				});
			}
			//为地图注册click事件获取鼠标点击出的经纬度坐标
			var clickEventListener = map.on('click', function(e) {
				document.getElementById("lnglat").value = e.lnglat.getLng()
						+ ',' + e.lnglat.getLat()
			});
		});
	</script>
	<script type="text/javascript"
		src="http://webapi.amap.com/demos/js/liteToolbar.js"></script>

</body>
</html>