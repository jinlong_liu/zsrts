<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
    <link rel="stylesheet" href="<%=path%>/css/wx/weui.css">
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <link rel="stylesheet" href="<%=path%>/css/wx/magic-check.css"> 
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
        <!-- 弹框 -->
    <script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/sweetalert.css">
      <jsp:include page="/js/inc.jsp"></jsp:include>
<title>居民出行调查</title>
</head>
<body>
<div style="width:100%; height:100%;background:#ecfff4;text-align:center;">
<img src="../images/wx/personal-info.png" class="fillPictrue" style="width:100%; height:60%;"/><br/><br/><br/>
<span style="font-size:25px;color:#75c89a">第二部分：个人信息</span><br/><br/><br/>
               <div style="text-align:center">
                  <a href="javaScript:personalInfoWrite()"><img src="../images/wx/pi-startwrite.png" style="width:200px; height:60px;"/></a>
               </div>
</div>
</body>
</html>