<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<title>居民出行调查</title>
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
<!-- 弹框 -->
<script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/sweetalert.css">
<!-- 页面样式 -->
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<!-- 遮罩层 -->
<link href="<%=path%>/css/wx/showLoading.css" rel="stylesheet"
	media="screen" />
<script type="text/javascript" src="<%=path%>/js/wx/zDialog.js"></script>
<script type="text/javascript" src="<%=path%>/js/wx/zDrag.js"></script>
<jsp:include page="/js/inc.jsp"></jsp:include>
<style type="text/css">
table td {
	border: 1px solid #F1F1F1
}
</style>
<script type="text/javascript">
	/*屏蔽手机返回键*/
	XBack = {};
	(function(XBack) {
		XBack.STATE = 'x - back';
		XBack.element;

		XBack.onPopState = function(event) {
			event.state === XBack.STATE && XBack.fire();
			XBack.record(XBack.STATE); //初始化事件时，push一下  
		};
		XBack.record = function(state) {
			history.pushState(state, null, location.href);
		};
		XBack.fire = function() {
			var event = document.createEvent('Events');
			event.initEvent(XBack.STATE, false, false);
			XBack.element.dispatchEvent(event);
		};
		XBack.listen = function(listener) {
			XBack.element.addEventListener(XBack.STATE, listener, false);
		};
		XBack.init = function() {
			XBack.element = document.createElement('span');
			window.addEventListener('popstate', XBack.onPopState);
			XBack.record(XBack.STATE);
		};

	})(XBack); // 可以当做js文件 引入这段

	XBack.init();
	XBack.listen(function() {
	});

	$(document).ready(function() {
						var familyId = '${param.family}';
						//alert(familyId);
						var data = {
							'familyId' : familyId
						};
						$
								.ajax({
									type : "GET",
									dataType : "json",
									url : root
											+ "/services/wxgetPersonalRandomOutInfo",
									data : data,
									async : false,
									success : function(result) {
										var obj = eval(result)
										//alert(obj);
										if (obj.length != 0) {

											for (var i = 0; i < obj.length; i++) {
												var objforchoose = obj[i];
												var index = objforchoose.index;
												$('#indexText').val(index);
												var outSp = objforchoose.outStartingpoint;
												var outObj = objforchoose.outObjective;

												var outTime = objforchoose.outTimeDate;
												var getSp = objforchoose.getObjective;
												var getTime = objforchoose.getTimeDate;
												
												var noText = objforchoose.noText;
												if(noText!=null&&noText!=""){
													var url = root + "/wx/error-submit.jsp";
													window.location.href = url;
												}
												if (outSp == 0) {
													outSp = "家";
												} else if (outSp == 1) {
													outSp = "单位";
												} else {
													var outSps = outSp
															.split(":");
													for (var i = 0; i < outSps.length; i++) {
														outSp = outSps[1];
													}
												}
												if (getSp == 0) {
													getSp = "家";
												} else if (getSp == 1) {
													getSp = "单位";
												} else {
													var getSps = getSp
															.split(":");
													for (var i = 0; i < getSps.length; i++) {
														getSp = getSps[1];
													}
												}
												document
														.getElementById("busTotalTime").innerHTML = objforchoose.busTotalTime
														+ "分钟";
												document
														.getElementById("busPriceTicket").innerHTML = objforchoose.busPriceTicket
														+ "元";
												document
														.getElementById("busGoTime").innerHTML = objforchoose.busGoTime
														+ "分钟";
												document
														.getElementById("busWaitTime").innerHTML = objforchoose.busWaitTime
														+ "分钟";
												document
														.getElementById("busRidingTime").innerHTML = objforchoose.busRidingTime
														+ "分钟";

												//document.getElementById("busTotalTime").innerHTML = objforchoose.busTotalTime+"分钟";
												//document.getElementById("busTotalTime").innerHTML = objforchoose.busTotalTime+"分钟";
												document
														.getElementById("metroTotalTime").innerHTML = objforchoose.metroTotalTime
														+ "分钟";
												document
														.getElementById("metroPriceTicket").innerHTML = objforchoose.metroPriceTicket
														+ "元";
												document
														.getElementById("metroGoTime").innerHTML = objforchoose.metroGoTime
														+ "分钟";
												document
														.getElementById("metroWaitTime").innerHTML = objforchoose.metroWaitTime
														+ "分钟";
												document
														.getElementById("metroRidingTime").innerHTML = objforchoose.metroRidingTime
														+ "分钟";

												document
														.getElementById("carTotalTime").innerHTML = objforchoose.carTotalTime
														+ "分钟";
												document
														.getElementById("carTotalCost").innerHTML = objforchoose.carTotalCost
														+ "元";
												document
														.getElementById("carWorkingCost").innerHTML = objforchoose.carWorkingCost
														+ "元";
												document
														.getElementById("carParkCost").innerHTML = objforchoose.carParkCost
														+ "元";

												document
														.getElementById("taxiTotalTime").innerHTML = objforchoose.taxiTotalTime
														+ "分钟";
												document
														.getElementById("taxiTotalCost").innerHTML = objforchoose.taxiTotalCost
														+ "元";

												document
														.getElementById("bicycleTotalTime").innerHTML = objforchoose.bicycleTotalTime
														+ "分钟";

												document
														.getElementById("electricVehicleTotalTime").innerHTML = objforchoose.electricVehicleTotalTime
														+ "分钟";

												document.getElementById("text").innerHTML = "假设您前面填写的第"
														+ index
														+ "次出行(如下表)可以任意选择以下6种方式，请您选择最有可能采用的交通方式?";
												document
														.getElementById("textHint").innerHTML = "您前面填写的第"
														+ index + "次出行信息";
												document.getElementById("out").innerHTML = "<p>"
														+ outSp + "</p>";
												document
														.getElementById("outTime").innerHTML = "<p>"
														+ outTime + "</p>";

												document.getElementById("get").innerHTML = "<p>"
														+ getSp + "</p>";
												document
														.getElementById("getTime").innerHTML = "<p>"
														+ getTime + "</p>";

											}
										} else {
											var url = root + "/wx/finish.jsp";
											window.location.href = url;
										}
										/* swal({   
											title: "温馨提示",   
											text: '根据您填写的第'+index+'次出行、出发地点：'+outSp+'、出行目的:'+outObj+',我们分析出了如下几种出行方式供您选择。',   
											imageUrl: "",
											html: true,
											timer: 5000,   
											showConfirmButton: false
										});*/
									}
								});
					});
</script>
</head>

<body style="background: #FFFFFF;">
	<input id="familyId" name="familyId" type="hidden"
		value="${param.family}" />
		
		<input id="indexText" name="indexText" type="hidden"
		value="" />
	<div>

		<div style="width: 100%; height: 80px; background-color: #04BA74;">
			<div>
				<span style="color: #FFFFFF; font-size: 20px; margin-left: 10%;">为了进一步了解您家庭成员对各种交通方式的选择意愿，请您家庭</span>
				<span style="color: #FF0000; font-size: 20px; margin-left: 0%;">第1位</span>
				<span style="color: #FFFFFF; font-size: 20px; margin-left: 0%;">成员回答以下问题:</span>
			</div>
		</div>
		<div
			style="font-size: 16px; width: 80%; margin-left: 10%; color: red;">
			<span id="text"></span>
		</div>
		<br />
		<div style="font-size: 16px; text-align: center; color: #FFB600;">
			<span id="textHint"></span>
		</div>

		<div style="width: 100%;">
			<table style="width: 100%;">
				<tr style="width: 100%;">
					<td style="width: 50%; text-align: center;"><span
						style="font-size: 16px;">出发地点:</span></td>
					<td style="width: 50%;"><span id="out"
						style="font-size: 16px;"></span></td>
				</tr>

				<tr>
					<td style="text-align: center;"><span style="font-size: 16px;">出发时间:</span></td>
					<td><span id="outTime" style="font-size: 16px;"></span></td>
				</tr>

				<tr>
					<td style="text-align: center;"><span style="font-size: 16px;">到达地点:</span></td>
					<td><span id="get" style="font-size: 16px;"></span></td>
				</tr>

				<tr>
					<td style="text-align: center;"><span style="font-size: 16px;">到达时间:</span></td>
					<td><span id="getTime" style="font-size: 16px;"></span></td>
				</tr>

			</table>
		</div>

		<!-- <div style="margin-left: 15%;"><span id="out" style="font-size: 16px;"></span></div>
<div style="margin-left: 15%;"><span id="outTime" style="font-size: 16px;"></span></div>
<div style="margin-left: 15%;"><span id="get" style="font-size: 16px;"></span></div>
<div style="margin-left: 15%;"><span id="getTime" style="font-size: 16px;"></span></div> -->


		<div>
			<br />
			<div style="text-align: center;">
				<p class="time-sign">请选择您出行会采用的交通方式</p>
			</div>
			<div style="width: 100%;">
				<div class="time-transportation-bus"></div>
				<div class="words-sign" onclick="checkSign(1)">
					<div style="Float: right; margin-right: 10%">
						<input class="magic-radio" type="radio" name="radio" id="bus"
							value="1"> <label for="bus"></label>
					</div>
					<p>01&nbsp&nbsp公交</p>
				</div>
				<hr />
				<div class="words-width" onclick="checkSign(1)">
					<p>
						<span>&nbsp时间(min):</span><span id="busTotalTime"></span>
					</p>
					<p>
						<span>&nbsp票价(元):</span><span id="busPriceTicket"></span>
					</p>
					<p>
						<span>&nbsp走到站时间:</span><span id="busGoTime"></span>
					</p>
					<p>
						<span>&nbsp等车时间:</span><span id="busWaitTime"></span>
					</p>
					<p>
						<span>&nbsp乘车时间:</span><span id="busRidingTime"></span>
					</p>
				</div>
				<hr />
				<br /<br/>
			</div>


			<div style="display:none;">
				<div class="time-transportation-metro"></div>
				<div class="words-sign" onclick="checkSign(2)">
					<div style="Float: right; margin-right: 10%">
						<input class="magic-radio" type="radio" name="radio" id="metro"
							value="2"> <label for="metro"></label>
					</div>
					<p>02&nbsp&nbsp地铁</p>
				</div>
				<hr />
				<div class="words-width" onclick="checkSign(2)">
					<p>
						<span>&nbsp时间(min):</span><span id="metroTotalTime"></span>
					</p>
					<p>
						<span>&nbsp票价(元):</span><span id="metroPriceTicket"></span
					</p>
					<p>
						<span>&nbsp走到站时间:</span><span id="metroGoTime"></span>
					</p>
					<p>
						<span>&nbsp等车时间:</span><span id="metroWaitTime"></span>
					</p>
					<p>
						<span>&nbsp乘车时间:</span><span id="metroRidingTime"></span>
					</p>
				</div>
				<hr />
				<br />
				<br />
			</div>



			<div style="">
				<div class="time-transportation-car"></div>
				<div class="words-sign" onclick="checkSign(3)">
					<div style="Float: right; margin-right: 10%">
						<input class="magic-radio" type="radio" name="radio" id="car"
							value="3"> <label for="car"></label>
					</div>
					<p>02&nbsp&nbsp小汽车</p>
				</div>
				<hr />
				<div class="words-width" onclick="checkSign(3)">
					<p>
						<span>&nbsp时间(min):</span><span id="carTotalTime"></span>
					</p>
					<p>
						<span>&nbsp费用(元):</span><span id="carTotalCost"></span>
					</p>
					<p>
						<span>&nbsp运行费用:</span><span id="carWorkingCost"></span>
					</p>
					<p>
						<span>&nbsp停车费用:</span><span id="carParkCost"></span>
					</p>
				</div>
				<hr />
				<br />
				<br />
			</div>



			<div style="">
				<div class="time-transportation-taxi"></div>
				<div class="words-sign" onclick="checkSign(4)">
					<div style="Float: right; margin-right: 10%">
						<input class="magic-radio" type="radio" name="radio" id="taxi"
							value="4"> <label for="taxi"></label>
					</div>
					<p>03&nbsp&nbsp出租车</p>
				</div>
				<hr />
				<div class="words-width" onclick="checkSign(4)">
					<p>
						<span>&nbsp乘车时间(min):</span><span id="taxiTotalTime"></span>
					</p>
					<p>
						<span>&nbsp费用(元):</span><span id="taxiTotalCost"></span>
					</p>
				</div>
				<hr />
				<br />
				<br />
			</div>



			<div style="">
				<div class="time-transportation-bicycle"></div>
				<div class="words-sign" onclick="checkSign(5)">
					<div style="Float: right; margin-right: 10%">
						<input class="magic-radio" type="radio" name="radio" id="bicycle"
							value="5"> <label for="bicycle"></label>
					</div>
					<p>04&nbsp&nbsp自行车(含共享单车)</p>
				</div>
				<hr />
				<div class="words-width" onclick="checkSign(5)">
					<p>
						<span>&nbsp时间(min):</span><span id="bicycleTotalTime"></span>
					</p>
				</div>
				<hr />
				<br />
				<br />
			</div>



			<div style="">
				<div class="time-transportation-electricVehicle"></div>
				<div class="words-sign" onclick="checkSign(6)">
					<div style="Float: right; margin-right: 10%">
						<input class="magic-radio" type="radio" name="radio"
							id="electricVehicle" value="6"> <label
							for="electricVehicle"></label>
					</div>
					<p>05&nbsp&nbsp电动车</p>
				</div>
				<hr />
				<div class="words-width" onclick="checkSign(6)">
					<p>
						<span>&nbsp时间(min):</span><span id="electricVehicleTotalTime"></span>
					</p>
				</div>
				<hr />
				<br />
				<br />
			</div>





		</div>

		<br />
		<br />
		<div class="next-page-address" id="scandal">
			<a href="javaScript:saveTimeText()">
			<img src="../images/wx/all-write.png" style="width: 200px; height: 60px;" /></a>
		</div>

	</div>
</body>
</html>