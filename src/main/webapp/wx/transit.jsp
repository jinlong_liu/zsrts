<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<title>居民出行调查</title>
    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/magic-check.css">
    <jsp:include page="/js/inc.jsp"></jsp:include>
    <script type="text/javascript">
$(document).ready(function(){
var code = ${param.code };
alert(code);

});
   </script>
</head>
<body>
<input id="code" name="code" value="${param.code }"/>
<%response.sendRedirect("../oAuthServlet?code=${param.code}");%>
</body>
</html>