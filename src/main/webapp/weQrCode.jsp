<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,minimum-scale=1.0, maximum-scale=2.0" />
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-2.1.3.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
<!-- 加载中.... -->
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/jquery.mloading.css" />
<script type="text/javascript" src="<%=path%>/js/wx/jquery.mloading.js"></script>
<!-- 分页时多选弹出框插件插件 -->
<script type="text/javascript"
	src="<%=path%>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
<script type="text/javascript"
	src="<%=path%>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
<!-- import js -->
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>
<link rel="stylesheet" href="<%=path%>/css/wx/example.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
<!-- 弹框 -->
<script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/sweetalert.css">

<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/weQrCode.css">
<script type="text/javascript" src="<%=path%>/js/wx/weQrCode.js"></script>
<title>居民出行调查</title>
</head>
<body><br/>
	<div id="top">
		<div class="tableCss">
		
<!-- 		<div>
   <label class="checkbox-inline">
      <input type="checkbox" id="inlineCheckbox1" value="option1"> 选项 1
   </label>
   <label class="checkbox-inline">
      <input type="checkbox" id="inlineCheckbox2" value="option2"> 选项 2
   </label>
       </div> -->
		
		<div class="form-group"
				style="width: 240px; float: left; height：32px; line-height: 32px;">
		<label class="checkbox-inline">
      <input type="radio" name="optionsRadiosinline" id="optionsRadios" 
         value="1" onclick="hiddenOrblock()">公众二维码
   </label>
   <label class="checkbox-inline">
      <input type="radio" name="optionsRadiosinline" id="optionsRadios" 
         value="2" onclick="hiddenOrblock()" checked>社区二维码
   </label>
       </div>
		
			<div class=""
				style="width: 220px; float: left; height：41px; line-height: 30px;" id="surveyortwo">
				<label id='laberLeftOne' class="">行政区:</label>
				<select id='districtSelect' class=''
					name='districtSelect' style="width: 130px;height:34px;" onchange='getStreet()'>
					<option value=''>请选择</option>
				</select>
			</div>

			<div class=""
				style="width: 200px; float: left; height：41px; line-height: 30px;" id="surveyorone">
				<label id='laberLeftTwo' class="">街道:</label>
				<select id='subOfficeSelect' 
					name='subOfficeSelect' style="width: 130px;height:34px;" onchange='getCommunity()'>
					<option value=''>请选择</option>
				</select>
			</div>
			
            <div class="form-group"
				style="width: 90px; float: left; height：32px; line-height: 32px;" id="surveyor">
            <label>
                  <input type="checkbox" id="inlineCheckbox" value="1">&nbsp; 调查员
            </label>
            </div>
<!-- 			<div class="form-group"
				style="width: 300px; float: left; height：32px; line-height: 32px;">
				<label id='laberLeftThree' class="col-sm-3 control-label">社区:</label>
				<select id='communitySelect' class='form-control select'
					name='communitySelect' style="width: 180px;" onclick=''>
					<option value=''>请选择</option>
				</select>
			</div> -->

			<div style="float: left; width: 10%;" id="cx">
				<button type="button" class="btn btn-primary" style="background:#06B370;" onclick="getListForQr()">查询</button>
			</div>
			<div style="float: left; width: 13%;">
				<button type="button" class="btn btn-primary" onclick="createQr()" style="background:#06B370;">
					生成二维码</button>
			</div>
		</div>
    </div>
    
		<div>
			<br /> <br />
			<div class="tableCss" style="width: 1100px; height: 580px;">

				<table id="table" style="width: 1100px; height: 565px;"></table>
			</div>
		</div>

		
</body>
</html>