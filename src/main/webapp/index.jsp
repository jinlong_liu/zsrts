<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width"> 
     <title>居民出行调查</title>

     <link rel="stylesheet" href="http://cache.amap.com/lbs/static/main1119.css"/>
     <script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=3067ac7a693712711f75a87e43d6e5f9&plugin=AMap.CitySearch"></script>
     <script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>

    <link rel="stylesheet" href="<%=path%>/css/wx/example.css">
    <script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/wx/magic-check.css">
    <jsp:include page="/js/inc.jsp"></jsp:include>
    <script type="text/javascript">

     window.location.href="<%=path%>/services/wxgetStage?openId=${param.openId}";
 
<%--         var ctiyCodes = '${param.ctiyCodes}';
       
       var map = new AMap.Map('container', {
		resizeEnable : true,
		zoom : 13,
		center : [ 116.39, 39.9 ]
	});
	//加载地图，调用浏览器定位服务  定位当前位置并获取经纬度
	var map, geolocation;
	map = new AMap.Map('container', {
		resizeEnable : true
	});
	map.plugin('AMap.Geolocation', function() {
		geolocation = new AMap.Geolocation({
			enableHighAccuracy : true,//是否使用高精度定位，默认:true
			timeout : 10000, //超过10秒后停止定位，默认：无穷大
			buttonOffset : new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
			zoomToAccuracy : true, //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
			showCircle: false,      //去掉圆形区域
			buttonPosition : 'RB'
		});
		geolocation.getCityInfo(function(status, result) {
		    if (status == 'complete') {
		    	var citycode = result.citycode;
		    	alert(result.citycode);
		    	//0371  河南省郑州市
		    	//alert(1);
		    	window.location.href="<%=path%>/services/wxgetStage?openId=${param.openId}&cityCodegd="+citycode;
              
		    }
		    });
		map.addControl(geolocation);
		geolocation.getCurrentPosition();
		AMap.event.addListener(geolocation, 'complete', onComplete);//返回定位信息
		AMap.event.addListener(geolocation, 'error', onError); //返回定位出错信息
	});  --%>
                     
             
</script>
</head>
<body>
<div id="container" tabindex="0"></div>
<input id="openId" name="openId" value="${param.openId}" type="hidden"/>
</body>
</html>