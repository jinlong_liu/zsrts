<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="initial-scale=1.0, user-scalable=no, width=device-width">
<title>居民出行调查</title>

<script type="text/javascript" src="<%=path%>/js/wx/questionnaire.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/magic-check.css">
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript">
	
</script>
</head>
<body style="background-color: #01BBAE;">
	<div>
		<!-- title --><br /><br /><br /><br />
		<div class="qrcode-title"></div>

		<div class="qrcode-big-div">
			<!-- 左div -->
			<div class="qrcode-left-div">
				<img src="/zsrts/images/wx/app-qr-left.png"
					style="width: 100%; height: 700px;" />
			</div>

			<!-- 右div -->
			<div class="qrcode-right-div">
				<br />
				<div style="background-color: #01BBAE;">
					<div class="qrcode-sign-top" style="font-weight: bold;">
						<span>出行轨迹手机APP下载页面及使用说明</span>
					</div>
					<br />
					<div class="word-div-center">
						<!-- <p class="qrcode-p-big">活动规则</p> -->
					</div>
					<div class="word-div-margin" style="margin-left: 10%; width: 82%;">
						<p>
							<span class="qrcode-span-number">①</span><span
								class="qrcode-span-word">&nbsp;&nbsp;参与本项出行轨迹测试的居民可获得20元手机话费奖励。</span>
						</p>
						<p>
							<span class="qrcode-span-number">②</span><span
								class="qrcode-span-word">&nbsp;&nbsp;下载APP并注册完成后，需保持本软件1整天在后台运行，后台运行注意事项请阅读APP里的“使用帮助”。</span>
						</p>
						<p>
							<span class="qrcode-span-number">③</span><span
								class="qrcode-span-word">&nbsp;&nbsp;测试当天晚上睡觉前需补充当天一整天每次出行使用的交通方式和出行目的，补充完成后提交即完成测试。</span>
						</p>
						<p>
							<span class="qrcode-span-number">④</span><span
								class="qrcode-span-word">&nbsp;&nbsp;提交出行信息并经审核通过后，20元话费会在1小时之内充值到注册的手机账户中。</span>
						</p>
						<!-- <p>
							<span class="qrcode-span-number">⑤</span><span
								class="qrcode-span-word">提交出行信息并经审核通过后，20元话费会在1小时之内充值到注册的手机账户。</span>
						</p> -->
					</div>
				</div>

				<!-- 		<div class="qrcod-qr-zw"></div>-->
				<br/><br/><br/><br/>
				<div class="right-color">
					<div class="qrcod-qr" style="background-color: #01BBAE;">
						<div class="div-boder">
							<img src="/zsrts/images/wx/qrCode1.1.png" class="qrcode-img" />
							<div class="qrcode-img-word">
								<p style="font-weight: bold;font-size: 20px;margin-right: 54px;line-height: 80px;">APP下载区</p>
							</div>
							<div class="qrcode-a-one">
								<a href="http://47.92.86.63/zsrts/app/trafficsurveyer1.1.apk"><img
									src="/zsrts/images/wx/app-qrcode-button.png" /></a>
							</div>

						</div>
					</div>

			<!-- 		<div class="qrcod-qr" style="background-color: #01BBAE;">
						<div class="div-boder">
							<img src="/zsrts/images/wx/qrCode1.2.png" class="qrcode-img" />
							<div class="qrcode-word-two">
								<p>参加1个工作日和1天周末测试APP下载区</p>
							</div>
							<div class="qrcode-a-two">
								<a href="http://47.92.86.63/zsrts/app/trafficsurveyer1.2.apk"><img
									src="/zsrts/images/wx/app-qrcode-button.png" /></a>
							</div>

						</div>
					</div>

					<div class="qrcod-qr" style="background-color: #01BBAE;">
						<div class="div-boder">
							<img src="/zsrts/images/wx/qrCode1.3.png" class="qrcode-img" />
							<div class="qrcode-word-three">
								<p>参加1周测试APP下载区</p>
							</div>
							<div class="qrcode-a-two">
								<a href="http://47.92.86.63/zsrts/app/trafficsurveyer1.3.apk"><img
									src="/zsrts/images/wx/app-qrcode-button.png" /></a>
							</div>

						</div>
					</div> -->

				</div>
			</div>



		</div>
	</div>
</body>
</html>