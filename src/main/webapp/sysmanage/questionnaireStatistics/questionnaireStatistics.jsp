<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>问卷统计</title>
    <jsp:include page="/js/inc.jsp"></jsp:include>
	<script type="text/javascript"
	src="<%=path%>/js/jquery//jquery-3.1.0.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
		
	 <link rel="stylesheet" type="text/css" href="<%=path%>/third_party/sweet-alert/sweet-alert.css" /> 
	
	  <!-- 分页时多选弹出框插件插件 -->
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
	<!-- import js -->
	
	<script type="text/javascript"src="<%=path%>/third_party/sweet-alert/sweet-alert.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third_party/echarts/echarts.js"></script> 
	

	<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>
		
	<link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/sysmanage/questionnaireStatistics/questionnaireStatistics.css"  />
	<script type="text/javascript" src="<%=path%>/js/sysmanage/questionnaireStatistics/questionnaireStatistics.js"></script>
		
	<link href="<%=path%>/parametersChange/DataCenter.css" rel="stylesheet" type="text/css" />	
<base>
</head>
<body>

<!-- <div class="bgBox">
        <div class="loaderTip" >
            <img src="../../images/loading.gif" />
            <div class="loaderTip_msg">数据正在加载中，请稍候...</div>
        </div>
    </div> -->

<div class="infoBase">


<div class="radiodiv" style="width: 1080px;">
    <div class="container-fluid">
    <div class="navbar-header">
         
    </div>
    <div class="topBoder" style="height:50px;">
    
        <ul class="nav navbar-nav" style="line-height:50px; height:40px;">
            <li id="view1" style=" height:40px;margin-left:40px;"><input  name=" " type="radio" value="" checked="checked" onclick='view1()' >表格</input></li>
            <li id="view2" style=" height:40px;margin-left:40px;"><input  name=" " type="radio" value="" onclick='view2()' >柱状图</input></li>
            <li id="view3"  style="height:40px;">
             <label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>					  
				<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px;height: 35px;' onchange='diArea(2)' >
				<option value=''>请选择</option> 
				</select> 
            </li>
            <li id="view4"  style=" height:40px;">
            <label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>	 
				<select id='subofficeName' name='type'  class='js-example-disabled-results' style='width:200px; height: 35px;' >
				<option value=''>请选择</option> 
				</select>
            </li>
            <li id="view5" style='margin-left:100px;height:40px;'>
              <button type='button' style='border-radius:6px;border:none;width:60px;line-height:30px;background:#06b370;color:white;margin-top:10px;' onclick='view()'> 
				    查    询
		      </button>
            </li>
        </ul>
    </div>
    
    </div>
   




   <!--  <nav class="navbar navbar-default" role="navigation" style="margin-left:20px;">
    <div class="container-fluid">
    <div class="navbar-header">
         <a class="navbar-brand" >请选择显示样式:</a> 
    </div>
    <div>
        <ul class="nav navbar-nav">
            <li id="view1" class="active"><a  onclick='view1()'>表格</a></li>
            <li id="view2"><a  onclick='view2()'>柱状图</a></li>
            <li id="view3">
             <label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>					  
				<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px;height: 35px;margin-top:10px;' onchange='diArea(2)' >
				<option value=''>请选择</option> 
				</select> 
            </li>
            <li id="view4">
            <label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>	 
				<select id='subofficeName' name='type'  class='js-example-disabled-results' style='width:200px; height: 35px;margin-top:10px;' >
				<option value=''>请选择</option> 
				</select>
            </li>
            <li id="view5" style='margin-left:800px;position:absolute;'>
              <button type='button' style='border-radius:6px;border:none;width:80px;background:#06b370;color:white;margin-top:5px;' onclick='view()'> 
				    查    询
		      </button>
            </li>
        </ul>
    </div>
    </div>
    </nav> -->
    <!-- <div>
        <span id="are" style="top:20px">
                <label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>					  
				<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px;height: 35px;' onchange='diArea(2)' >
				<option value=''>请选择</option> 
				</select> 
				<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>	 
				<select id='subofficeName' name='type'  class='js-example-disabled-results' style='width:200px; height: 35px;' >
				<option value=''>请选择</option> 
				</select> 
        </span>
        
	</div>	 --> 
		

		<!-- 模态框（Modal）end -->  
	</div> 
	
	<div  class="margin-top-table" style="width:1080px;height:565px;">
		 <table id="table"></table> 
	 <div id="view"  style="width:1060px;height:411px;"></div> 
	 </div>
	
</div>
</body>
</html>
