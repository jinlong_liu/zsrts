<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="jquery,ui,easy,easyui,web">
<meta name="description"
	content="easyui help you build your web page easily!">
<title>修改密码</title>
<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript" src="<%=path%>/js/common/md5.js"></script>

 <link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/sysmanage/usermanage/update_password.css"  />
 <script type="text/javascript" src="<%=path%>/js/sysmanage/usermanage/update_password.js"></script>
</head>
<body >
	<div id="main"  >
					
					<div id="title" >
						<div id="title_name" >修改密码</div>
					</div>
					<hr id="hr" >
					<div class="table_old" >
						<label class="date" >原密码：</label>
						<input id="originalPwd"  type="password"></input>
					</div>
					
					<div class="table_new" >
						<label class="date" >新密码：</label> 
						<input id="newPwd"  type="password"></input>
					</div>
					
					<div class="table_qrnew" >
						<label class="date" >确认新密码：</label> 
						<input id="confirmPwd" type="password"></input>
					</div>
					
					<div class="main_bootom" >
						<a onclick="save()" class="easyui-linkbutton dialogbtn orangebtn" href="javascript:void(0)">保存</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a onclick="closeTab();" class="easyui-linkbutton dialogbtn orangebtn" href="javascript:void(0)">关闭</a>
					</div>
			
			
	<!-- 	<table style="width: 1100px;border:1px #b3b3b3 solid;height: 300px;">
			<tr>
				<td align="center">
					<label class="date" >原密码：</label>
					<input id="originalPwd" style="width: 180px;" type="password"></input>
				</td>
			</tr>
			<tr>
				<td align="center">
					<label class="date" >新密码：</label> 
					<input id="newPwd" style="width: 180px;" type="password"></input>
				</td>
			</tr>
			<tr>
				<td align="center">
					<label class="date" >确认新密码：</label> 
					<input id="confirmPwd" style="width: 180px;" type="password"></input>
				</td>
			</tr>
			<tr>
				<td align="center">
					<a onclick="save()" class="easyui-linkbutton dialogbtn orangebtn" href="javascript:void(0)">保存</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a onclick="closeTab();" class="easyui-linkbutton dialogbtn orangebtn" href="javascript:void(0)">关闭</a>
				</td>
			</tr>
		</table> -->
	</div>
</body>
</html>