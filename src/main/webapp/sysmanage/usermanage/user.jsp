<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>用户管理</title>
    <jsp:include page="/js/inc.jsp"></jsp:include>
	<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
	
	  <!-- 分页时多选弹出框插件插件 -->
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
	<!-- import js -->

	<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>
		
	    <link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/sysmanage/usermanage/user.css"  />
		<script type="text/javascript" src="<%=path%>/js/sysmanage/usermanage/user.js"></script>
</head>
<body style="width: 100%">
	
<div class="infoBase">
<div class="radiodiv" style="width:100%;">
		<a style="border:none;margin-left:18px;" data-toggle="modal" data-target="#myModal" onclick='loadRole()' ><!-- class="btn btn-primary" -->
		<img  src="../../images/user_manger_add.png"/> 
		<!-- 新增用户 -->
		</a>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">  
		<div class="modal-dialog" >
		<div class="modal-dialog" role="document">  
		    <div class="modal-content">  
		        <div class="modal-header">  
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">  
		                <span aria-hidden="true">×</span>  
		            </button>  
		            <h4 class="modal-title" id="myModalLabel">新增用户</h4>  
		        </div>  
		        <div class="modal-body">  
					    <label id="userStyle" >用&nbsp;&nbsp;&nbsp;户&nbsp;&nbsp;&nbsp;名&nbsp;:</label>
					    <input  name="userName" id="userName"  placeholder="请输入用户名" style="height:30px" /> 
						
					    <label id="userStyle" >密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
 					    <input type="password"  name="userPassword"  id="userPassword" placeholder="请输入密码" style="height:30px" />
					    <br/>
						<label id="userStyle" >姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
					    <span id="userIdDiv"><input  name="realName" id="realName"  style="height:30px" /></span>
						<label id="userStyle" >电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
					    <span id="userIdDiv"><input  name="phone" id="phone"  style="height:30px" /></span>
						<br/>
						<label id="userStyle" >角&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;色&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
						<select id='selectRole' name="type"  class='js-example-disabled-results'  style='width:175px;height:30px' 
						onchange='loadDistrictNamedistrictName(this)'>
				        </select>
						
						<label id="userStyle" style="display: none">管&nbsp;辖&nbsp;区&nbsp;域&nbsp;:</label> <br/>
						<!-- <label id="userStyle" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>
						<select id='selectRole' name="type"  class='js-example-disabled-results' style='width:200px' >
				        </select>
				        </br>
				        <label id="userStyle" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>
				        <select id='selectRole' name="type"  class='js-example-disabled-results' style='width:200px' >
				        </select>
				        </br>
				        <label id="userStyle" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;社区:</label>
				           <span id = "community"></span>
				           <input type="checkbox" name="test" value="0">社区1
				           <input type="checkbox" name="test" value="1">社区2
				           <input type="checkbox" name="test" value="2">社区3
				           <input type="checkbox" name="test" value="3">社区4 -->
						<div id="are" style="display: none">
						</div>
						<div style="height:80px;width:100%">
						<div style="height:30px;line-height:25px;float:left;">
						<label id="remarkStyle"  >备&nbsp;&nbsp;&nbsp;&nbsp;注&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
						</div>
						<div style="width:80%;float:left;">
					    &nbsp;&nbsp;&nbsp;<textarea name="remark" id="remark" style="width:40%;height:30px;line-height:25px"></textarea>
					    </div>
					    </div>
					  
				    </div>  
				        <div class="modal-footer">  
				            <button type="button" class="btn btn-primary" onclick="save()">保存</button>  
				            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>  
				        </div> 
		    </div>  
		</div>  
		</div>  
		</div>
		
		<!-- 模态框（Modal） -->  
		<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">  
		    <div class="modal-dialog">  
		        <div class="modal-content">  
		            <div class="modal-header">  
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>  
		                <h4 class="modal-title" id="myModalLabel">修改信息</h4>  
		            </div>  
		            <div class="modal-body">  
					    <label id="userStyle" >用&nbsp;&nbsp;&nbsp;户&nbsp;&nbsp;&nbsp;名&nbsp;:</label>
					    <span id="userIdDiv"><input name="userNameUpdate" id="userNameUpdate" style="height:30px"  /></span>
						
						<label id="userStyle" >密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
					    <span id="userIdDiv"><input type="password" name="userPasswordUpdate" id="userPasswordUpdate" style="height:30px" /></span>
					    
						<br/>
						<label id="userStyle" >姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
					    <span id="userIdDiv"><input name="realNameUpadte" id="realNameUpadte"  style="height:30px" /></span>
						<label id="userStyle" >电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
					    <span id="userIdDiv"><input  name="phoneUpdate" id="phoneUpdate"  style="height:30px" /></span>
						<br/>
						<label id="userStyle" >角&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;色&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
						<select id='selectRoleUpdate' name="type"  class='js-example-disabled-results'  style='width:175px;height:30px' 
						onchange='loadDistrictNamedistrictNameUpdate()'>
				        </select>
						
						<label id="userStyle" style="display: none">管&nbsp;辖&nbsp;区&nbsp;域&nbsp;:</label> <br/>

						<div id="are1" style="display: none">
						</div>

						<div style="height:80px">
						<div style="height:30px;line-height:25px;float:left;">
						<label id="remarkStyle">备&nbsp;&nbsp;&nbsp;&nbsp;注&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
						</div>
						<div style="width:80%;float:left;">
							&nbsp;&nbsp;&nbsp;<textarea name="remarkUpdate" id="remarkUpdate" style="width:40%;height:30px;line-height:25px"></textarea>
					    </div>
					    </div>
					  
		            <div class="modal-footer">  
		                <button type="button" class="btn btn-primary" onclick="updateRolenew()">提交更改</button>  
		                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>  
		            </div>  
		        </div>  
		        <!-- /.modal-content -->  
		    </div>  
		    <!-- /.modal -->  
		</div>  
		<!-- 模态框（Modal）end -->  
	</div> 
	
	<div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">  
		    <div class="modal-dialog">  
		        <div class="modal-content">  
		            <div class="modal-header">  
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>  
		                <h4 class="modal-title" id="myModalLabel">详细信息</h4>  
		            </div>  
		            <div class="modal-body">  
					    <label id="userStyle" >用&nbsp;&nbsp;&nbsp;户&nbsp;&nbsp;&nbsp;名&nbsp;:</label>
					    <span id="userIdDiv"><input name="userNameView" id="userNameView" style="height:30px"  /></span>
						
						<label id="userStyle" >密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
					    <span id="userIdDiv"><input type="password" name="userPasswordView" id="userPasswordView" style="height:30px" /></span>
						
						<br/>
						<label id="userStyle" >姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
					    <span id="userIdDiv"><input name="realNameView" id="realNameView"  style="height:30px" /></span>
						<label id="userStyle" >电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
					    <span id="userIdDiv"><input  name="phoneView" id="phoneView"  style="height:30px" /></span>
					    <br/>
						<label id="userStyle" >角&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;色&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
						<select id='selectRoleView' name="type"  class='js-example-disabled-results'  style='width:175px;height:30px' 
						onchange='loadDistrictNamedistrictNameUpdate()'>
				        </select>
						
						<label id="userStyle" style="display: none">管&nbsp;辖&nbsp;区&nbsp;域&nbsp;:</label> <br/>

						<div id="are2" style="display: none">
						</div>
						<div style="height:80px">
						<div style="height:30px;line-height:25px;float:left;">
						<label id="remarkStyle">备&nbsp;&nbsp;&nbsp;&nbsp;注&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
						</div>
						<div style="width:80%;float:left;">
							&nbsp;&nbsp;&nbsp;<textarea name="remarkView" id="remarkView" style="width:40%;height:30px;line-height:25px"></textarea>
					    </div>
					    </div>
					  
		            <div class="modal-footer">  
		                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>  
		            </div>  
		        </div>  
		        <!-- /.modal-content -->  
		    </div>  
		    <!-- /.modal -->  
		</div>  
		<!-- 模态框（Modal）end -->  
	</div> 
	
	<div class="margin-top-table" style="width: 95%;height:90%;">
		 <table id="table"></table> 
	 </div>
</div>
</div>
<script type="text/javascript">
	//新增角色
	function save() {
		var userName = $('#userName').val().trim();
		var userPassword = $('#userPassword').val().trim();
		var realName = $('#realName').val().trim();
		var roleId = $('#selectRole').val().trim();
		var remark = $('#remark').val().trim();

		var phone = $('#phone').val().trim();

		// obj = document.getElementsByName("area");
		//     check_val = [];
		//     for(k in obj){
		//        if(obj[k].checked)
		//           check_val.push(obj[k].value);
		//     }
		var reg = /^(?!_)^[0-9a-zA-Z_]{6,}$/;
		//var reg = /^[\w]$/;
		if (!reg.test(userName)) {
			alert("用户名格式不正确");
			return;
		}
		if(userPassword != ""){
			if (!reg.test(userPassword)) {
				alert("密码格式不正确");
				return;
			}
		}

		if(userName.length<5){
			alert("用户名长度不能低于5位");
			return;
		}
		if (realName == "") {
			alert("姓名不能为空！");
			return;
		}
		var num =/^1(3|4|5|7|8)\d{9}$/;//手机号

		if(phone !=''){
			if(!num.test(phone)){
				alert("电话格式不正确！");
				return;
			}
		}

		if (roleId == "") {
			alert("请选择角色！");
			return;
		}
		var area=0;
		//alert(userName+userPassword+realName+roleId+remark);
		$.post(root+"/services/addUser", {userName:userName,
					userPassword:userPassword,
					realName:realName,
					phone:phone,
					roleId:roleId,
					area:area,
					remark:remark,},
				function(data){
					//var  resu=null;
					var obj = eval('(' + data + ')');//将data转成JSON对象

					//alert(obj.errNo);
					/*for(var key in data)  {
                        console.log("属性：" + key + ",值："+ data[key]);
                        resu=data[27];
                    } */
					if(obj.errNo==0){
						art.dialog.tips('添加成功！', 1.5);
						$('#myModal').modal('hide');
						search();

						$("#userName").val("");
						$("#userPassword").val("");
						$("#realName").val("");
						$("#phone").val("");
						$("#remark").val("");
						//location.reload();
					}else if(obj.errNo==-2){
						art.dialog.tips('角色名已存在！', 1.5);
					}else{
						art.dialog.tips('系统异常', 1.5);
					}
				});
		// var dialog=art.dialog({
		// 	title: '提示',//弹出框 的标题
		// 	content: '是否确定添加？',//弹出框中显示的内容
		// 	ok: function () {
		//
		// 	},
		// 	okVal:'确定',
		// 	cancel: function () {
		// 	},
		//
		// 	fixed:true,
		// 	resize:true
		// });

	}
	var updateIdPubllic=null;
	var updateRolename=null;
	var peopolId=null;
	var actorId=null;
	var authId=null;

	var peopolOld=null;
	var actorOld=null;
	var authOld=null;
	var updateUserId=null;
	var auth= 4;
	function search(){
		$('#table').bootstrapTable('destroy');
		/*	 radioVal = encodeURI(encodeURI(radioVal));    */
		$("#table").bootstrapTable({
			url : root+'/services/getUserList',
			method:'post',
			contentType:'application/x-www-form-urlencoded;charset=UTF-8',
			striped:true,
			pagination:true,
			pageSize:10,
			pageNumber:1,
			height:450,
			pageList:[10,20,30],
			silent: true,
			sortStable:true,
			sortName:'time',
			sortOrder:'desc',
			sidePagination: "client",
			/*responseHandler:function(res){
                return{
                    number:res.number,
                    stoptype:res.stoptype
                };
            },*/

			/*queryParams:{
                radioVal:radioVal
            },*/
			/*queryParams:function(params){
                return {
                   radioVal:radioVal,

               };
            },*/
			formatLoadingMessage:"请稍等,正在加载中。。。。。。。",
			columns:[
				/*
                {
                    checkbox:true,
                    valign:'middle',
                    align:'center'
                },
                */
				{
					field:'i',
					title:'序号',
					width:100,
					valign:'middle',
					align:'center'
				},
				{
					field:"userName",
					title:'用户',
					width:100,
					valign:'middle',
					align:'center'
				},
				{
					field:"realName",
					title:'姓名',
					width:100,
					valign:'middle',
					align:'center'
				},
				{
					field:'roleName',
					title:'角色',
					width:150,
					valign:'middle',
					align:'center'
				},
				{
					field:'phone',
					title:'电话',
					width:150,
					valign:'middle',
					align:'center'
				},
				{
					field:'area',
					title:'区域',
					width:280,
					valign:'middle',
					align:'center'
				},

				{
					field:'revise',
					title:'操作',
					width:200,
					valign:'middle',
					align:'center',
					formatter:function(value,row,index){
						return "<a href='javascript:void(0);' onclick='seeDetail("+row.userId+")'><img  src='../../images/view.png'/></a>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='updateRole("+row.userId+")'><img  src='../../images/update.png'/></a>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='deleteUser("+row.userId+")' ><img  src='../../images/delete.png'/> </a>";
					}
				},

			]

		});
	}
	$().ready(function(){

		/*	var radioVal=$('input:radio[name="stop_info_radio"]:checked').val();       href='reviseRole.jsp?id="+row.id+"'*/
		search();

		//模态框隐藏式触发该事件
		$('#myModal').on('hidden.bs.modal', function () {
			$("#are").empty();
		})
		$('#update').on('hidden.bs.modal', function () {
			$("#are1").empty();
		})
		$('#view').on('hidden.bs.modal', function () {
			$("#are2").empty();
		})
		/* document.getElementById("type1").style.display = "block";
         document.getElementById("type2").style.display = "none";
         document.getElementById("type3").style.display = "none";*/

		/*	$(".stop_info_radio").change(function(){
            radioVal=$('input:radio[name="stop_info_radio"]:checked').val();
            search(radioVal);
        });
        */

	});

	//删除角色
	function deleteUser(id) {

		var dialog=art.dialog({
			title: '提示',//弹出框 的标题
			content: '是否确定删除？',//弹出框中显示的内容
			ok: function () {
				$.post(root+"/services/deleteUser", { id: id},
						function(data){
							var obj = eval('(' + data + ')');//将data转成JSON对象

							if(obj.errNo==0){
								art.dialog.tips('删除成功！', 1.5);
								search();
								//location.reload();
							}else if(obj.errNo==-2){
								art.dialog.tips('不能删除系统管理员！', 1.5);
							}else{
								art.dialog.tips('系统异常', 1.5);
							}
						});
			},
			okVal:'确定',
			cancel: function () {
			},
			fixed:true,
			resize:true
		});


	}




	//查看详细信息页面

	function seeDetail(id){
		$.ajax({
			url:root+'/services/queryUserByUserId',
			type:'post',
			data:{
				id:id,
			},
			success:function(data){
				var length = $.parseJSON(data).length;//根据数组长度判断循环次数
				var len = length-1;
				ids = [];//给复选框赋值id
				districtNumber = [];//行政区列表
				subofficeNumber = [];//街道列表
				$.each($.parseJSON(data),function(i,n){
					if(i==0){
						auth = n.auth;
						//var id=n.id;
						//updateUserId = id;
						var userName = n.userName;
						$('#userNameView').val(userName);
						var userPassword = n.userPassword;
						$('#userPasswordView').val(userPassword);
						var realName = n.realName;
						$('#realNameView').val(realName);

						var phone = n.phone;
						$('#phoneView').val(phone);

						var remark = n.remark;
						$('#remarkView').val(remark);
						var roleId = n.roleId;
						var roleName = n.roleName;
						$("#selectRoleView").empty();
						$("#selectRoleView").append(
								"<option value='" + roleId + "'>"
								+ roleName + "</option>");
						//loadOtherRole(roleId);
						var type = n.type;


						$("#are2").empty();

						var select = "";

						if(type==1)	{
							select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
							select = select+" <div class='form-inline'>";
							select = select+"<label id='userStyle' style='width:15%;float:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label></div>"+
									"<div id = 'communityView' style='width:80%;float:left;'></div>";
							//' <span id = "community"></span></br>';
							select = select + "</div>";

							ids.push(n.areaId);

							//loadDistrictName(1,n.areaId);
							//给复选框赋值

						}
						if(type==2){
							select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
							select = select+" <div >";
							select = select+ "<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>"+
									"<select id='districtName1' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea(2)'>"+
									" </select> "+
									"</div>";
							select = select+" <div class='form-inline'>";
							select = select
									+ "<label id='userStyle' style='width:15%;float:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label></div>"+
									"<div id = 'communityView' style='width:80%;float:left;'></div>";
							//' <span id = "community"></span></br>';
							select = select + "</div>";
							//selectDis(1);
							ids.push(n.areaId);
							districtNumber.push(n.districtNumber);
							//loadSub(2);


						}
						if(type==3){
							select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
							select = select+" <div >";
							select = select+"<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>"+
									"<select id='districtName1' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea2(2)'>"+
									" </select> "+
									"</div>";
							select = select+" <div >";
							select = select
									+ "<label id='userStyle'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>"+
									"<select id='subofficeName1' class='js-example-disabled-results' name='subofficeName'  style='width:200px' "+
									"onchange='suArea(3)'>"
									+ "</select></div>";
							select = select+" <div class='form-inline'>";
							select = select +' <label id="userStyle" style="width:15%;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;社区:</label></div>'+
									"<div id = 'communityView' style='width:80%;float:left;'></div>";
							// ' <span id = "community"></span>';
							select = select + "</div>";
							//selectDis(1);
							ids.push(n.areaId);
							districtNumber.push(n.districtNumber);
							subofficeNumber.push(n.subofficeNumber);

						}

						document.getElementById("are2").innerHTML = document
								.getElementById("are2").innerHTML+select;

					}else{

						ids.push(n.areaId);
						if(n.type == 2){
							districtNumber.push(n.districtNumber);
						}
						if(n.type == 3){
							districtNumber.push(n.districtNumber);
							subofficeNumber.push(n.subofficeNumber);
						}
					}

					if(i==len){//当type为1且循环走完了就加载数据
						if(n.type == 1){
							loadDistrictName1(1,ids,auth);
						}else if(n.type == 2){
							loadSubofficeName1(2,ids,districtNumber);
						}else if(n.type == 3){
							loadCommunityName1(3,ids,districtNumber,subofficeNumber);
						}

					}


				});
			}

		});

		$('#view').modal('show');//显示


	}


	//查看加载小区
	function loadCommunityName1(type,ids,districtNumber,subofficeNumber){
		selectDis=[];
		selectSub=[];
		$.ajax({
			url : root + "/services/queryAreaByUserId",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {
				$("#districtName1").empty();
				$("#subofficeName1").empty();
				$("#communityView").empty();
				var obj = eval("(" + result + ")");
				for(var i = 0; i < obj.length; i++){
					if(obj[i].id==ids[0]){
						$("#districtName1").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].districtName + "</option>");

						$("#subofficeName1").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].subofficeName + "</option>");

						selectDis.push(obj[i].districtNumber);
						selectSub.push(obj[i].subofficeNumber);
					}
				}

				for ( var i = 0; i < obj.length; i++) {
					if(obj[i].id==ids[0]){
						$("#communityView").append(
								"<span style='width:190px;display:block; float:left;'><input type='checkbox' style='width:15%' name='areaView' value="+obj[i].id+">"+obj[i].communityName+"</span>"
						);
					}
					if(obj[i].subofficeNumber == subofficeNumber[0] && obj[i].id!=ids[0]){//继续添加小区复选框
						$("#communityView").append(
								"<span style='width:190px;display:block; float:left;'><input type='checkbox' style='width:15%' name='areaView' value="+obj[i].id+">"+obj[i].communityName+"</span>"
						);
					}

					/*	var flagDis = true;
                        for(j=0;j<selectDis.length;j++){//加载行政区
                            if(obj[i].districtNumber == selectDis[j]){//加载行政区下拉
                                flagDis = false;
                            }

                        }
                        if(flagDis){
                            $("#districtName").append(
                                    "<option value='" + obj[i].id + "'>"
                                     + obj[i].districtName + "</option>");
                            selectDis.push(obj[i].districtNumber);

                        }

                        var flagSub = true;
                        for(k=0;k<selectSub.length;k++){//加载街道
                            if(obj[i].subofficeNumber == selectSub[k]){//加载行政区下拉
                                flagSub = false;
                            }

                        }
                        if(flagSub){
                            $("#subofficeName").append(
                                    "<option value='" + obj[i].id + "'>"
                                     + obj[i].subofficeName + "</option>");
                            selectSub.push(obj[i].subofficeNumber);

                        }	*/

				}//加载完毕

				//给复选框赋值
				obj = document.getElementsByName("areaView");//给复选框赋值

				for(i=0;i<obj.length;i++){
					for(j=0;j<ids.length;j++){
						if(obj[i].value == ids[j]){
							obj[i].checked = true;
							break;
						}
					}
				}


			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}



	//查看加载街道复选框
	function loadSubofficeName1(type,ids,districtNumber){
		select = [];
		$.ajax({
			url : root + "/services/queryAreaByUserId",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {
				$("#districtName").empty();
				$("#communityView").empty();

				var obj = eval("(" + result + ")");

				for ( var i = 0; i < obj.length; i++) {
					if(obj[i].id==ids[0]){//给下拉列表赋值
						$("#districtName1").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].districtName + "</option>");
						select.push(obj[i].districtNumber);
					}
				}

				for ( var i = 0; i < obj.length; i++) {
					if(obj[i].id==ids[0]){//给下拉列表赋值
						$("#communityView").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox'  name='areaView' value="+obj[i].id+">"+obj[i].subofficeName+"</span>"
						);
					}

					if(obj[i].districtNumber == districtNumber[0] && obj[i].id!=ids[0]){
						$("#communityView").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox' name='areaView' value="+obj[i].id+">"+obj[i].subofficeName+"</span>"
						);

					}

					/*var flag = true;
                    for(j=0;j<select.length;j++){
                        if(obj[i].districtNumber == select[j]){//加载行政区下拉
                            //alert(obj[i].districtNumber);
                            flag = false;
                        }

                    }
                    if(flag){
                        $("#districtName").append(
                                "<option value='" + obj[i].id + "'>"
                                 + obj[i].districtName + "</option>");
                        select.push(obj[i].districtNumber);

                    }
                */


				}//加载完毕

				//给复选框赋值
				obj = document.getElementsByName("areaView");//给复选框赋值
				for(i=0;i<obj.length;i++){
					for(j=0;j<ids.length;j++){
						if(obj[i].value == ids[j]){
							obj[i].checked = true;
							break;
						}
					}
				}


			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}



	//查看加载行政区复选框并赋值
	function loadDistrictName1(type,ids,auth){

		$.ajax({
			url : root + "/services/queryAreaByUserId",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {

				$("#communityView").empty();
				var obj = eval("(" + result + ")");
				for ( var i = 0; i < obj.length; i++) {
					if(auth == "3"){

						$("#communityView").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox' checked ='checked' disabled='true' style='width:15%' name='areaView'  value="+obj[i].id+">"+obj[i].districtName+"</span>"
						);
					}else{

						$("#communityView").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox' style='width:15%' name='areaView'  value="+obj[i].id+">"+obj[i].districtName+"</span>"
						);
					}

				}
				obj = document.getElementsByName("areaView");//给复选框赋值

				for(i=0;i<obj.length;i++){
					for(j=0;j<ids.length;j++){
						if(obj[i].value == ids[j]){
							obj[i].checked = true;
							break;
						}
					}

				}
			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}



	//修改 用户信息前展示页面显示
	function updateRole(id){

		$.ajax({
			url:root+'/services/queryUserByUserId',
			type:'post',
			data:{
				id:id,
			},
			success:function(data){
				var length = $.parseJSON(data).length;//根据数组长度判断循环次数
				var len = length-1;
				ids = [];//给复选框赋值id
				districtNumber = [];//行政区列表
				subofficeNumber = [];//街道列表
				$.each($.parseJSON(data),function(i,n){

					if(i==0){
						auth = n.auth;

						var id=n.id;
						updateUserId = id;
						var userName = n.userName;
						$('#userNameUpdate').val(userName);
						var userPassword = n.userPassword;
						$('#userPasswordUpdate').val(userPassword);
						var realName = n.realName;
						$('#realNameUpadte').val(realName);

						var phone = n.phone;
						$('#phoneUpdate').val(phone);

						var remark = n.remark;
						$('#remarkUpdate').val(remark);
						var roleId = n.roleId;
						var roleName = n.roleName;
						$("#selectRoleUpdate").empty();
						$("#selectRoleUpdate").append(
								"<option value='" + roleId + "'>"
								+ roleName + "</option>");
						loadOtherRole(roleId);
						var type = n.type;

						$("#are1").empty();

						var select = "";

						if(type==1)	{
							select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
							select = select+" <div class='form-inline'>";
							select = select+"<label id='userStyle' style='width:15%;float:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label></div>"+
									"<div id = 'community' style='width:80%;float:left;'></div>";
							//' <span id = "community"></span></br>';
							select = select + "</div>";

							ids.push(n.areaId);

							//loadDistrictName(1,n.areaId);
							//给复选框赋值

						}
						if(type==2){
							select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
							select = select+" <div >";
							select = select+ "<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>"+
									"<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea(2)'>"+
									" </select> "+
									"</div>";
							select = select+" <div class='form-inline'>";
							select = select
									+ "<label id='userStyle' style='width:15%;float:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label></div>"+
									"<div id = 'community' style='width:80%;float:left;'></div>";
							//' <span id = "community"></span></br>';
							select = select + "</div>";
							//selectDis(1);
							ids.push(n.areaId);
							districtNumber.push(n.districtNumber);
							//loadSub(2);


						}
						if(type==3){
							select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
							select = select+" <div >";
							select = select+"<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>"+
									"<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea2(2)'>"+
									" </select> "+
									"</div>";
							select = select+" <div >";
							select = select
									+ "<label id='userStyle'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>"+
									"<select id='subofficeName' class='js-example-disabled-results' name='subofficeName'  style='width:200px' "+
									"onchange='suArea(3)'>"
									+ "</select></div>";
							select = select+" <div class='form-inline'>";
							select = select +' <label id="userStyle" style="width:15%;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;社区:</label></div>'+
									"<div id = 'community' style='width:80%;float:left;'></div>";
							// ' <span id = "community"></span>';
							select = select + "</div>";
							//selectDis(1);
							ids.push(n.areaId);
							districtNumber.push(n.districtNumber);
							subofficeNumber.push(n.subofficeNumber);


						}


						document.getElementById("are1").innerHTML = document
								.getElementById("are1").innerHTML+select;

					}else{
						ids.push(n.areaId);
						if(n.type == 2){
							districtNumber.push(n.districtNumber);
						}
						if(n.type == 3){
							districtNumber.push(n.districtNumber);
							subofficeNumber.push(n.subofficeNumber);
						}
					}

					if(i==len){//当type为1且循环走完了就加载数据
						if(n.type == 1){
							loadDistrictName(1,ids,auth);
						}else if(n.type == 2){
							loadSubofficeName(2,ids,districtNumber);
						}else if(n.type == 3){
							loadCommunityName(3,ids,districtNumber,subofficeNumber);
						}

					}


				});
			}

		});
		$('#update').modal('show');//显示

	}


	//加载小区
	function loadCommunityName(type,ids,districtNumber,subofficeNumber){
		selectDis=[];
		selectSub=[];
		$.ajax({
			url : root + "/services/queryAreaByUserId",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {
				$("#districtName").empty();
				$("#subofficeName").empty();
				$("#community").empty();
				var obj = eval("(" + result + ")");
				for(var i = 0; i < obj.length; i++){
					if(obj[i].id==ids[0]){
						$("#districtName").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].districtName + "</option>");

						$("#subofficeName").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].subofficeName + "</option>");

						selectDis.push(obj[i].districtNumber);
						selectSub.push(obj[i].subofficeNumber);
					}
				}

				for ( var i = 0; i < obj.length; i++) {
					if(obj[i].id==ids[0]){
						$("#community").append(
								"<span style='width:190px;display:block; float:left;'><input type='checkbox' style='width:15%' name='area' value="+obj[i].id+">"+obj[i].communityName+"</span>"
						);
					}
					if(obj[i].subofficeNumber == subofficeNumber[0] && obj[i].id!=ids[0]){//继续添加小区复选框
						$("#community").append(
								"<span style='width:190px;display:block; float:left;'><input type='checkbox' style='width:15%' name='area' value="+obj[i].id+">"+obj[i].communityName+"</span>"
						);
					}

					var flagDis = true;
					for(j=0;j<selectDis.length;j++){//加载行政区
						if(obj[i].districtNumber == selectDis[j]){//加载行政区下拉
							flagDis = false;
						}

					}
					if(flagDis){
						$("#districtName").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].districtName + "</option>");
						selectDis.push(obj[i].districtNumber);

					}

					var flagSub = true;
					for(k=0;k<selectSub.length;k++){//加载街道
						if(obj[i].subofficeNumber == selectSub[k]){//加载行政区下拉
							flagSub = false;
						}

					}
					if(flagSub){
						$("#subofficeName").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].subofficeName + "</option>");
						selectSub.push(obj[i].subofficeNumber);

					}

				}//加载完毕

				//给复选框赋值
				obj = document.getElementsByName("area");//给复选框赋值

				for(i=0;i<obj.length;i++){
					for(j=0;j<ids.length;j++){
						if(obj[i].value == ids[j]){
							obj[i].checked = true;
							break;
						}
					}
				}


			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}

	//加载街道复选框
	function loadSubofficeName(type,ids,districtNumber){
		select = [];
		$.ajax({
			url : root + "/services/queryAreaByUserId",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {
				$("#districtName").empty();
				$("#community").empty();

				var obj = eval("(" + result + ")");

				for ( var i = 0; i < obj.length; i++) {
					if(obj[i].id==ids[0]){//给下拉列表赋值
						$("#districtName").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].districtName + "</option>");
						select.push(obj[i].districtNumber);
					}
				}

				for ( var i = 0; i < obj.length; i++) {
					if(obj[i].id==ids[0]){//给下拉列表赋值
						$("#community").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox'  name='area' value="+obj[i].id+">"+obj[i].subofficeName+"</span>"
						);
					}

					if(obj[i].districtNumber == districtNumber[0] && obj[i].id!=ids[0]){
						$("#community").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox' name='area' value="+obj[i].id+">"+obj[i].subofficeName+"</span>"
						);

					}

					var flag = true;
					for(j=0;j<select.length;j++){
						if(obj[i].districtNumber == select[j]){//加载行政区下拉
							//alert(obj[i].districtNumber);
							flag = false;
						}

					}
					if(flag){
						$("#districtName").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].districtName + "</option>");
						select.push(obj[i].districtNumber);

					}



				}//加载完毕

				//给复选框赋值
				obj = document.getElementsByName("area");//给复选框赋值
				for(i=0;i<obj.length;i++){
					for(j=0;j<ids.length;j++){
						if(obj[i].value == ids[j]){
							obj[i].checked = true;
							break;
						}
					}
				}


			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}


	//加载行政区复选框并赋值
	function loadDistrictName(type,ids,auth){//auth为查询的用户的权限，为3为市级管理人员，默认管理所有区域
		//alert(auth);
		$.ajax({
			url : root + "/services/queryAreaByUserId",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {

				$("#community").empty();
				var obj = eval("(" + result + ")");

				for ( var i = 0; i < obj.length; i++) {
					if(auth == "3"){

						$("#community").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox' checked ='checked' disabled='true' style='width:15%' name='area'  value="+obj[i].id+">"+obj[i].districtName+"</span>"
						);
					}else{

						$("#community").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox' style='width:15%' name='area'  value="+obj[i].id+">"+obj[i].districtName+"</span>"
						);
					}
				}
				obj = document.getElementsByName("area");//给复选框赋值

				for(i=0;i<obj.length;i++){
					for(j=0;j<ids.length;j++){
						if(obj[i].value == ids[j]){
							obj[i].checked = true;
							break;
						}
					}

				}
			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}

	//获取到当前角色后，下拉加载其他角色
	function loadOtherRole(roleId){

		$.ajax({
			url : root + "/services/loadRole",
			type : "post",
			/*data : {
                roleId : roleId
            },*/
			success : function(result) {
				var obj = eval("(" + result + ")");
				console.log(obj);
				for ( var i = 0; i < obj.length; i++) {
					if(obj[i].id != roleId){
						$("#selectRoleUpdate").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].roleName + "</option>");
					}

				}
			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}

	//添加时根据选择的对应角色，加载对应的区域
	function loadDistrictNamedistrictNameUpdate(obj){
		//查询对应的角色的权限
		var roleId = $('#selectRoleUpdate').val();
		var data = {"roleId":roleId};
		if(roleId == ''){
			$("#are1").empty();
			return;
		}
		$.ajax({
			type : "POST",
			dataType : "json",
			url : root+"/services/queryAuthFlag",
			data : data,
			success : function(result) {
				$("#are1").empty();
				var select = "";

				if(result.sysAuthDegree==3 || result.sysAuthDegree==4){
					select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
					select = select+" <div class='form-inline'>";
					select = select+"<label id='userStyle' style='width:15%;float:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label></div>"+
							"<div id = 'community' style='width:80%;float:left;'></div>";
					//' <span id = "community"></span></br>';
					select = select + "</div>";
					loadDis(1,result.sysAuthDegree);
				}
				if(result.sysAuthDegree==5){
					select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
					select = select+" <div >";
					select = select+ "<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>"+
							"<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea(2)'>"+
							" </select> "+
							"</div>";
					select = select+" <div class='form-inline'>";
					select = select
							+ "<label id='userStyle' style='width:15%;float:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label></div>"+
							"<div id = 'community' style='width:80%;float:left;'></div>";
					//' <span id = "community"></span></br>';
					select = select + "</div>";
					//selectDis(1);
					loadSub(2);
				}
				if(result.sysAuthDegree==6 || result.sysAuthDegree==7){
					select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
					select = select+" <div >";
					select = select+"<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>"+
							"<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea2(2)'>"+
							" </select> "+
							"</div>";
					select = select+" <div >";
					select = select
							+ "<label id='userStyle'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>"+
							"<select id='subofficeName' class='js-example-disabled-results' name='subofficeName'  style='width:200px' "+
							"onchange='suArea(3)'>"
							+ "</select></div>";
					select = select+" <div class='form-inline'>";
					select = select +' <label id="userStyle" style="width:15%;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;社区:</label></div>'+
							"<div id = 'community' style='width:80%;float:left;'></div>";
					// ' <span id = "community"></span>';
					select = select + "</div>";
					//selectDis(1);
					loadCom(3);
				}
				document.getElementById("are1").innerHTML = document
						.getElementById("are1").innerHTML+select;

			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}



	//修改 用户信息 并保存
	function updateRolenew() {
		//alert(updateUserId);
		var userName = $('#userNameUpdate').val().trim();
		var userPassword = $('#userPasswordUpdate').val().trim();
		var realName = $('#realNameUpadte').val().trim();
		var roleId = $('#selectRoleUpdate').val().trim();
		var remark = $('#remarkUpdate').val().trim();

		var phone = $('#phoneUpdate').val().trim();

		//obj = document.getElementsByName("areaUpdate");
		obj = document.getElementsByName("area");
		check_val = [];
		for(k in obj){
			if(obj[k].checked)
				check_val.push(obj[k].value);
		}
		// alert(check_val);
		//var reg = /^[0-9a-zA-Z]+$/;
		// var reg =  /^(?!_)^[0-9a-zA-Z_]+$/;
		var reg = /^(?!_)^[0-9a-zA-Z_]{6,}$/;
		if(userPassword != ""){
			if (!reg.test(userPassword)) {
				alert("密码格式不正确");
				return;
			}
		}
		if (!reg.test(userName)) {
			alert("用户名格式不正确");
			return;
		}

		if(userName.length<5){
			alert("用户名长度不能低于5位");
			return;
		}
		if (realName == "") {
			alert("姓名不能为空！");
			return;
		}

		var num =/^1(3|4|5|7|8)\d{9}$/;//手机号

		if(phone !=''){
			if(!num.test(phone)){
				alert("电话格式不正确！");
				return;
			}
		}

		if (roleId == "") {
			alert("请选择角色！");
			return;
		}
		var area = check_val.toString();
		var dialog=art.dialog({
			title: '提示',//弹出框 的标题
			content: '是否确定修改？',//弹出框中显示的内容
			ok: function () {
				$.post(root+"/services/updateSysUser", {id:updateUserId,
							userName:userName,
							userPassword:userPassword,
							realName:realName,
							phone:phone,
							roleId:roleId,
							area:area,
							remark:remark,
						},
						function(data){
							var obj = eval('(' + data + ')');//将data转成JSON对象

							if(obj.errNo==0){
								art.dialog.tips('修改成功！', 1.5);
								$('#update').modal('hide');
								search();
								//location.reload();
							}else if(obj.errNo==-2){
								art.dialog.tips('不能修改超级管理员', 1.5);
							}else if(obj.errNo==-3){
								art.dialog.tips('角色名已存在！', 1.5);
							}else{
								art.dialog.tips('系统异常', 1.5);
							}
						});
			},
			okVal:'确定',
			cancel: function () {
			},
			fixed:true,
			resize:true
		});
	}

	/*var _url = "";
    var rowidRemark=null;
    // 添加用户信息
    function addUser() {
        $("#userIdDiv").empty();
        var html = "<input name='userId' id='userId' style='width: 202px;height: 22px;border:1px #b3b3b3 solid;' />";
        $("#userIdDiv").append(html);
        $("#userName").val("");
        $("#remark").val("");
        getUserDeartment("userdateTree");
        getRole("roleId");
        $('#dlg').dialog('open').dialog('center').dialog('setTitle', '添加用户');
    }*/
	// 修改用户信息
	/*function editUser() {
        var row = $('#dg').datagrid('getSelected');
        if (row) {
            $('#dlg').dialog('open').dialog('center').dialog('setTitle', '修改用户');
            $('#fm').form('load', row);
            // roleId编辑选中
            $("#roleId").combotree({
                //获取数据URL
                url : root+'/services/getRoleList',
                //选择树节点触发事件
                onLoadSuccess:function(node,data){
                    var res=data[0].text;
                    //$("#roleId").combotree('setValue',res);//取第一天
              },
                onSelect : function(node) {
                    if(node.text=='运输局'||node.text=='总公司'){
                        alert("请选择其他部门!");
                        $('#'+id).tree("unselect");
                    }
                    if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
                        getLine(node.text);
                    }
                }
            });
            $('#userdateTree').combotree({
                //获取数据URL
                url : root+'/services/getDepartment',
                //选择树节点触发事件
                onLoadSuccess:function(node,data){
                    var res=data[0].text;
                    //$('#userdateTree').combotree('setValue',deptId);//取第一天
              },
                onSelect : function(node) {
                    if(node.text=='运输局'||node.text=='总公司'){
                        alert("请选择其他部门!");
                        $('#userdateTree').tree("unselect");
                    }
                    if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
                        getLine(node.text);
                    }
                }
            });
            // 替换userIdDiv
            $("#userIdDiv").empty();
            $("#userIdDiv").append(row.userId);
            var html = "<input name='userId' id='userId' style='width: 202px;height: 22px;border:1px #b3b3b3 solid;' />";
            $("#userIdDiv").append(html);
            $("#userId").val(row.userId);
            $('#userId').attr("readonly","readonly")
            $('#userdateTree').combotree('setValue',row.deptName);
            $("#inDeptId").val(row.deptId);
            $('#roleId').combotree('setValue',row.roleId);
            rowidRemark=row.id;

        } else {
            alert("请选择一条记录操作！");
        }

    }*/
	// 保存用户信息
	/*function saveUser() {
        var title=$('#dlg').panel('options').title;
        if(title=="修改用户"){
            var deptId=$('#userdateTree').combobox('getValue');
            var roleId=$('#roleId').combobox('getText');
            if(/.*[\u4e00-\u9fa5]+.*$/.test(deptId)) {
                deptId=$("#inDeptId").val();
            }
            if(/.*[\u4e00-\u9fa5]+.*$/.test(roleId)) {
                roleId=$("#inRoleId").val();
            }
            _url = root + '/services/editUser?id=' +rowidRemark+"&deptId="+deptId+"&roleId="+roleId;
        }else if(title=="添加用户"){
            var deptId=$('#userdateTree').combobox('getValue');
            var roleId=$('#roleId').combobox('getText');
            if(/.*[\u4e00-\u9fa5]+.*$/.test(deptId)) {
                deptId=$("#inDeptId").val();
            }
            if(/.*[\u4e00-\u9fa5]+.*$/.test(roleId)) {
                roleId=$("#inRoleId").val();
            }
            _url = root + "/services/addUser?deptId="+deptId+"&roleId="+roleId;
        }
        var userId = $("#userId").val();
        if (userId == "") {
            alert("用户名不能为空！");
            return;
        }
        var userName = $("#userName").val();
        if (userName == "") {
            alert("姓名不能为空！");
            return;
        }
        $('#fm').form('submit', {
            url : _url,
            onSubmit : function() {
                return $(this).form('validate');
            },
            success : function(result) {
                var res = eval('(' + result + ')');
                if (res.flag == "1") {// 操作成功
                    alert(res.message);
                    $('#dlg').dialog('close'); // close the dialog
                    $('#dg').datagrid('reload'); // reload the user data
                } else if (res.flag == "2") {
                    alert(res.message);
                } else {
                    alert(res.message);
                }
            }
        });
    }*/
	// 删除用户信息
	/*function destroyUser() {
        var row = $('#dg').datagrid('getSelected');
        if (!row) {
            alert("请选择一条记录操作！");
            return;
        }
        if (row.userId == "admin") {
            alert("系统管理员不能删除！");
            return;
        }
        if (row) {
            $.messager.confirm('删除用户', '您确定要删除该用户吗?', function(r) {
                if (r) {
                    $.post(root + '/services/delUser', {
                        id : row.id
                    }, function(result) {
                        if (result.flag == "1") {// 操作成功
                            alert(result.message);
                            $('#dg').datagrid('reload'); // reload the user data
                        } else {
                            alert(result.message);
                        }
                    }, 'json');
                }
            });
        }
    }
    */

	//获取部门
	/*function getUserDeartment(id){
        //alert(id);
        //设置只能选择日期的节点，不能选择年和月
        $('#'+id).combotree({
            //获取数据URL
            url : root+'/services/getDepartment',
            //选择树节点触发事件
            onLoadSuccess:function(node,data){
                var res=data[0].text;
                $('#'+id).combotree('setValue',res);//取第一天
          },
            onSelect : function(node) {
                if(node.text=='运输局'||node.text=='总公司'){
                    alert("请选择其他部门!");
                    $('#'+id).tree("unselect");
                }
                if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
                    getLine(node.text);
                }
            }
        });
    }*/

	//获取角色
	/*function getRole(id){
        //alert(id);
        //设置只能选择日期的节点，不能选择年和月
        $('#'+id).combotree({
            //获取数据URL
            url : root+'/services/getRoleList',
            //选择树节点触发事件
            onLoadSuccess:function(node,data){
                var res=data[0].text;
                $('#'+id).combotree('setValue',res);//取第一天
          },
            onSelect : function(node) {
                if(node.text=='运输局'||node.text=='总公司'){
                    alert("请选择其他部门!");
                    $('#'+id).tree("unselect");
                }
                if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
                    getLine(node.text);
                }
            }
        });
    }*/


	//加载角色
	function loadRole(){
		/*$('#userName').val('');
        $('#userPassword').val('');*/
		$.ajax({
			url : root + "/services/loadRole",
			type : "post",
			success : function(result) {
				$("#selectRole").empty();
				$("#selectRole").append(
						"<option value='" +' ' + "'>"
						+ '请选择' + "</option>");

				var obj = eval("(" + result + ")");
				console.log(obj);
				for ( var i = 0; i < obj.length; i++) {
					$("#selectRole").append(
							"<option value='" + obj[i].id + "'>"
							+ obj[i].roleName + "</option>");
				}
			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}


	function loadDistrictNamedistrictName(obj){
		//查询对应的角色的权限
		var roleId = $('#selectRole').val().trim();
		var data = {"roleId":roleId};
		if(roleId == ''){
			$("#are").empty();
			return;
		}
		$.ajax({
			type : "POST",
			dataType : "json",
			url : root+"/services/queryAuthFlag",
			data : data,
			success : function(result) {
				$("#are").empty();
				var select = "";

				if(result.sysAuthDegree==3 || result.sysAuthDegree==4){
					select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
					select = select+" <div class='form-inline'>";
					select = select+"<label id='userStyle' style='width:15%;float:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label></div>"+
							"<div id = 'community' style='width:80%;float:left;'></div>";
					//' <span id = "community"></span></br>';
					select = select + "</div>";
					loadDis(1,result.sysAuthDegree);
				}
				if(result.sysAuthDegree==5){
					select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
					select = select+" <div >";
					select = select+ "<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>"+
							"<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea(2)'>"+
							" </select> "+
							"</div>";
					select = select+" <div class='form-inline'>";
					select = select
							+ "<label id='userStyle' style='width:15%;float:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label></div>"+
							"<div id = 'community' style='width:80%;float:left;'></div>";
					//' <span id = "community"></span></br>';
					select = select + "</div>";
					//selectDis(1);
					loadSub(2);
				}
				if(result.sysAuthDegree==6 || result.sysAuthDegree==7){
					select = "<div class='row' style='height:150px;overflow-x:scroll;'>";
					select = select+" <div >";
					select = select+"<label id='userStyle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行政区:</label>"+
							"<select id='districtName' name='type'  class='js-example-disabled-results' style='width:200px' onchange='diArea2(2)'>"+
							" </select> "+
							"</div>";
					select = select+" <div >";
					select = select
							+ "<label id='userStyle'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;街道:</label>"+
							"<select id='subofficeName' class='js-example-disabled-results' name='subofficeName'  style='width:200px' "+
							"onchange='suArea(3)'>"
							+ "</select></div>";
					select = select+" <div class='form-inline'>";
					select = select +' <label id="userStyle" style="width:15%;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;社区:</label></div>'+
							"<div id = 'community' style='width:80%;float:left;'></div>";
					// ' <span id = "community"></span>';
					select = select + "</div>";
					//selectDis(1);
					loadCom(3);
				}
				document.getElementById("are").innerHTML = document
						.getElementById("are").innerHTML+select;

			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}

	//加载行政区复选框
	function loadDis(type,sysAuthDegree){

		$.ajax({
			url : root + "/services/queryAreaByRole",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {
				$("#community").empty();
				var obj = eval("(" + result + ")");
				for ( var i = 0; i < obj.length; i++) {
					if(sysAuthDegree==3){
						$("#community").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox' checked ='checked' disabled='true' style='width:15%'  name='area' value="+obj[i].id+">"+obj[i].districtName+"</span>"
						);
					}else{
						$("#community").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox' style='width:15%'  name='area' value="+obj[i].id+">"+obj[i].districtName+"</span>"
						);
					}


				}

			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}

	//加载街道复选框
	function loadSub(type){

		$.ajax({
			url : root + "/services/queryAreaByRole",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {
				$("#districtName").empty();
				$("#community").empty();
				var obj = eval("(" + result + ")");

				for ( var i = 0; i < obj.length; i++) {
					if(i==0){
						$("#districtName").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].districtName + "</option>");

						$("#community").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox'  name='area' value="+obj[i].id+">"+obj[i].subofficeName+"</span>"
						);
					}

					if(obj.length>1 && i>0){
						if(obj[0].districtNumber==obj[i].districtNumber){//继续添加社区复选框
							$("#community").append(
									"<span style='width:180px;display:block; float:left;'><input type='checkbox' name='area' value="+obj[i].id+">"+obj[i].subofficeName+"</span>"
							);
						}
						if(obj[0].districtNumber!=obj[i].districtNumber && obj[i-1].districtNumber!=obj[i].districtNumber){//下拉列表填写
							$("#districtName").append(
									"<option value='" + obj[i].id + "'>"
									+ obj[i].districtName + "</option>");
						}
					}

				}

			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}

	//加载小区
	function loadCom(type){
		$.ajax({
			url : root + "/services/queryAreaByRole",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {
				$("#districtName").empty();
				$("#subofficeName").empty();
				$("#community").empty();
				var obj = eval("(" + result + ")");

				for ( var i = 0; i < obj.length; i++) {
					if(i==0){
						$("#districtName").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].districtName + "</option>");

						$("#subofficeName").append(
								"<option value='" + obj[i].id + "'>"
								+ obj[i].subofficeName + "</option>");

						$("#community").append(
								"<span style='width:180px;display:block; float:left;'><input type='checkbox' style='width:15%' name='area' value="+obj[i].id+">"+obj[i].communityName+"</span>"
						);
					}

					if(obj.length>1 && i>0){
						if(obj[0].districtNumber==obj[i].districtNumber){
							if(obj[0].subofficeNumber==obj[i].subofficeNumber){//继续添加社区复选框
								$("#community").append(
										"<span style='width:180px;display:block; float:left;'><input type='checkbox' style='width:15%' name='area' value="+obj[i].id+">"+obj[i].communityName+"</span>"
								);
								//padding-right:40px;
							}

							if(obj[0].subofficeNumber!=obj[i].subofficeNumber && obj[i-1].subofficeNumber!=obj[i].subofficeNumber){//下拉列表填写
								$("#subofficeName").append(
										"<option value='" + obj[i].id + "'>"
										+ obj[i].subofficeName + "</option>");
							}

						}else if(obj[0].districtNumber!=obj[i].districtNumber && obj[i-1].districtNumber!=obj[i].districtNumber){//添加行政区复选框

							$("#districtName").append(
									"<option value='" + obj[i].id + "'>"
									+ obj[i].districtName + "</option>");
						}

					}



				}

			},
			error : function(result) {
				alert("系统错误");
			}
		});
	}


	//加载行政区
	function selectDis(type){
		$.ajax({
			url : root + "/services/getAreaDownList",
			type : "post",
			data : {
				type : type
			},
			success : function(result) {
				$("#districtName").append(
						"<option value='" +' ' + "'>"
						+ '请选择' + "</option>");

				var obj = eval("(" + result + ")");
				console.log(obj);
				for ( var i = 0; i < obj.length; i++) {
					$("#districtName").append(
							"<option value='" + obj[i].areaId + "'>"
							+ obj[i].text + "</option>");
				}

			},
			error : function(result) {
				alert("系统错误");
			}
		});

	}


	function diSub() {

	}


	function diArea2(flag) {
		if (flag == 2) {
			//$("#subofficeName").empty();
			var districtNumber = $('#districtName').val();
			var districtName = $('#districtName').find("option:selected").text();
			if (districtNumber == null) {
				$("#subofficeName").append("<option value=''>请选择</option>");
			}
			if (districtNumber != '') {

				$.ajax({
					url : root + "/services/queryAreaByUserId",
					type : "post",
					data : {
						type : 2,
						districtName : districtName
					},
					success : function(result) {
						$("#subofficeName").empty();
						//$("#community").empty();
						var obj = eval("(" + result + ")");
						if (obj.length == 0) {
							$("#subofficeName").append(
									"<option value='" + ' ' + "'>" + " "
									+ "</option>");
						} else {
							for ( var i = 0; i < obj.length; i++) {
								if(obj[i].districtName == districtName){
									$("#subofficeName").append(
											"<option value='" + obj[i].id + "'>"
											+ obj[i].subofficeName + "</option>");
								}
							}
						}
						suArea(3);
					},
					error : function(result) {
						alert("系统错误");
					}
				});

			}
		}
	}


	/**
	 * 点击行政区加载街道
	 */
	function diArea(flag) {
		if (flag == 2) {
			$("#subofficeName").empty();
			var districtNumber = $('#districtName').val();
			var districtName = $('#districtName').find("option:selected").text();
			/*if (districtNumber == null) {
                $("#subOfficeSel").append("<option value=''>请选择</option>");
            }*/
			if (districtNumber != '') {
				$.ajax({
					url : root + "/services/queryAreaByUserId",
					type : "post",
					data : {
						type : 2,
						districtName : districtName
					},
					success : function(result) {
						$("#community").empty();
						var obj = eval("(" + result + ")");

						for ( var i = 0; i < obj.length; i++) {
							if(obj[i].districtName == districtName){
								$("#community").append(
										"<span style='width:180px;display:block; float:left;'><input type='checkbox' name='area' value="+obj[i].id+">"+obj[i].subofficeName+"</span>"
								);
							}

						}
						/*var obj = eval("(" + result + ")");
                        if (obj.length == 0) {
                            $("#subOfficeSel").append(
                                    "<option value='" + ' ' + "'>" + " "
                                            + "</option>");
                        } else {
                            $("#subOfficeSel").append("<option value=''>请选择</option>");
                            for ( var i = 0; i < obj.length; i++) {
                                $("#subOfficeSel").append(
                                        "<option value='" + obj[i].areaId + "'>"
                                                + obj[i].text + "</option>");
                            }
                        }*/
					},
					error : function(result) {
						alert("系统错误");
					}
				});

			}
		}
	}


	/**
	 * 点击街道加载社区
	 */
	function suArea(flag) {

		if (flag == 3) {
			//$("#subOfficeSel").empty();
			var subofficeNumber = $('#subofficeName').val();
			var subofficeName = $('#subofficeName').find("option:selected").text();

			if (subofficeNumber != '') {
				$.ajax({
					url : root + "/services/queryAreaByUserId",
					type : "post",
					data : {
						type : 3,
						subofficeName:subofficeName
					},
					success : function(result) {
						$("#community").empty();
						var obj = eval("(" + result + ")");
						for ( var i = 0; i < obj.length; i++) {
							if(obj[i].subofficeName == subofficeName){
								$("#community").append(
										"<span style='width:180px;display:block; float:left;'><input type='checkbox' name='area' value="+obj[i].id+">"+obj[i].communityName+"</span>"
								);
							}

						}


					},
					error : function(result) {
						alert("系统错误");
					}
				});

			}
		}
	}


	$(function(){
		$("#dg").datagrid('hideColumn', "deptId");
	});

</script>
</body>
</html>
