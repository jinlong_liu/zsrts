<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>权限管理</title>
    <jsp:include page="/js/inc.jsp"></jsp:include>
    <link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/sysmanage/usermanage/jurisdiction.css"  />
	<script type="text/javascript" src="<%=path%>/js/sysmanage/usermanage/jurisdiction.js"></script>
</head>
<body>
    
    <table id="dg" style="width:1120px;height:500px"  class="easyui-datagrid"  url="<%=path%>/services/getUserList" 
    toolbar="#toolbar" pagination="false" rownumbers="true" fitColumns="true" singleSelect="true">
        <thead>
            <tr>
            	<th field="id" width="30" hidden="true">ID</th>
                <th field="roleId" width="30">角色</th>
                <th field="authName" width="30">权限</th>
                <th field="createDate" width="30">创建时间</th>
                <th field="operator" width="30">操作者</th>
            </tr>
        </thead>
    </table>
    <div id="toolbar">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addUser()">添加</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">修改</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">删除</a>
    </div>
    
     
    <!-- 添加、修改 -->
    <div id="dlg" class="easyui-dialog"  closed="true" buttons="#dlg-buttons" style="width:700px;height:400px;padding:10px 20px;">
        <div class="ftitle">用户管理</div>
        <form id="fm" method="post">
            <label id="userStyle" >用&nbsp;&nbsp;户&nbsp;名:</label>
            <span id="userIdDiv"><input name="userId" id="userId"   /></span>
            
            
            <label id="userStyle">姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名:</label>
            <input name="userName" id="userName" /><br><br>
            
            <label id="userStyle">角&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;色:</label>
			<select id="roleId" name="roleId" class="easyui-combotree" style="width: 204px;height: 24px;margin-top: 5px;border: 1px #b3b3b3 solid;"></select>
			
			<label id="userStyle">部&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;门:</label>
			 <select id="userdateTree" class="easyui-combotree" style="width: 204px;height: 24px;margin-top: 5px;border: 1px #b3b3b3 solid;"></select><br><br>
           

			
			<label id="remarkStyle" >备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注:</label>
            <textarea name="remark" id="remark" ></textarea>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" id="easyUi_button" >保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" id="easyUi_button" >取消</a>
    </div>
    
 
</body>
</html>
