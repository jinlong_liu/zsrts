<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>重置密码</title>
    <jsp:include page="/js/inc.jsp"></jsp:include>
	<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
	
	  <!-- 分页时多选弹出框插件插件 -->
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
	<!-- import js -->

	<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>
   <script type="text/javascript" src="<%=path%>/js/common/md5.js"></script>
   
   	<!-- alert样式 -->
<link rel="stylesheet" type="text/css" 
    href="<%=path%>/third_party/sweet-alert/sweet-alert.css" />
<script type="text/javascript"
	src="<%=path%>/third_party/sweet-alert/sweet-alert.min.js"></script>
		
	    <link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/sysmanage/modifyPassword/modifyPassword.css"  />
		<script type="text/javascript" src="<%=path%>/js/sysmanage/modifyPassword/modifyPassword.js"></script>
</head>
<body class="modifyPassword-body" >

<div class="margin-top-table" style="width: 100%;height:100%;">
		 
		<form action="">
	    <div class="login-title" style="height:20%"></div>
		<div class="login-text" style="margin-left:400px;">
		        <div style="margin-top:10px;"> <img src="../../images/titleModifyPassword.png" ></div>
				<!-- input 图标（img）和文本框（input）不要换行，否则中间会用空隙  -->
				<div class="div-password"><input id="oldpassword" name="oldpassword" type="password" class="password" placeholder="请输入原密码"/></div>
				<div class="div-password"><input id="password" name="password" type="password" class="password" placeholder="请输入新密码"/></div>
				<div class="div-password"><input id="con_password" name="con_password" type="password" class="con_password" placeholder="确认密码 "/></div>
				<div class="div-modify-password"><input class="modify-input-pwdbtn" id="modify" name="modify" type="button" onclick="modifyPwd()"/></div> 
		</div>
		<div style="height:20%"></div>
	</form>
		 
 
	 </div>

</body>
</html>
