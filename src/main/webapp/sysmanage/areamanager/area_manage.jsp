<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>区域管理</title>
<jsp:include page="/js/inc.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
<%-- <link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/table/fileinput.min.css" /> --%>
	
	
<!-- 分页时多选弹出框插件插件 -->
<script type="text/javascript"
	src="<%=path%>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
<script type="text/javascript"
	src="<%=path%>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
<!-- import js -->

<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>
<%-- <script type="text/javascript"
	src="<%=path%>/js/bootstrap/table/fileinput.js"></script> --%>

<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/sysmanage/areamanager/area_manage.css" />
<script type="text/javascript"
	src="<%=path%>/js/sysmanage/areamanager/area_manage.js"></script>
	<script type="text/javascript"
	src="<%=path%>/js/sysmanage/areamanager/ajaxfileupload.js"></script>
<script type="text/javascript">
</script>
<!-- 弹窗 -->
<script type="text/javascript" src="<%=path%>/js/wx/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/wx/sweetalert.css">
<style type="text/css">

</style>
</head>
<body>

	<div class="infoBase">
	 	
		<%-- <c:if test="${sessionScope.adminFlag != 5 }">   --%>
 		 <form  id="formid" style=" width:1100px;" method="post" enctype="multipart/form-data">   
 		 <button type="button" style="margin-left:5px;border:none;data-toggle="modal" data-target="#myModal" onclick="addArea()">
		<img  src="../../images/area.png"/> 
		</button>
		 <label id='userStyle' style="width: 4.5%;margin-left:15px;">行政区:</label>					  
				<select id='districtName' name='type'  class='js-example-disabled-results' style='width:150px;height: 35px;margin-top:10px;' onchange='dArea(2)' >
				<option value=''>请选择</option> 
				</select> 
				<label id='userStyle'style="width: 3% ;margin-left:1%;">街道:</label>	 
				<select id='subofficeName' name='type'  class='js-example-disabled-results' style='width:150px; height: 35px;margin-top:10px;' >
				<option value=''>请选择</option> 
				</select>
				 <button type='button' style='border-radius:6px;border:none;width:80px;background:#06b370;color:white;margin-top:5px;margin-left:4px;height: 35px;' onclick='queryAreaa()'> 
				    查    询
		      </button>
         	<input id="input-6" name="input6" type="radio" value="1" style="margin-left: 4%">行政区
         	<input id="input-6" name="input6" type="radio" value="2" style="margin-left: 30px"><span>街道</span>
         	<input id="input-6" name="input6" type="radio" value="3" style="margin-left: 30px"><span>社区</span>
			  
			 
			 <!-- 选择文件 -->
		 	<a href="javascript:;" class="file" style="border:none;width:80px; height:35px;top:10px;background: url(../../images/chos.png) no-repeat center;text-align:center;">
		 	<input id="file" name="file" type="file" style="width:78px" multiple class="file" >
			</a> 
			
			<!-- 导入 -->
			<!-- <button type="button" class="btn btn-primary" >提交</button> -->
			<input type="button" style=" border:none; background: url(../../images/import.png) no-repeat center;text-align:center;padding:3px 3px;width:78px;height:41px;margin-top: 7px;float:right"  
			onclick="ajaxFileUpload()" /></form>  
       
        
		<div class="radiodiv" style="width:100px">
			<!-- <button type="button" class="btn btn-primary" data-toggle="modal"
				data-target="#myModal" >新增区域</button>
         	 -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel"  aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">新增区域</h4>
						</div>
						<div id="selectDiv" >
						</br>
						 <label id="laberLeft"
									 >区域类型：</label>
									 <input type="radio"    name="chSelect" value="1" onclick="selectOnchang()">行政区
									 &nbsp; &nbsp;
									 <input type="radio"  name="chSelect" value="2" onclick="selectOnchang()">街道
									 &nbsp; &nbsp;
									 <input type="radio"  name="chSelect" value="3" onclick="selectOnchang()" >社区
									 <input type="hidden" id="checkSave" value="0">
									 <input type="hidden" id="id" value=" ">
									 <input type="hidden" id="oldName" value=" ">
							</div>		
									<!-- <select id="type" name="type" class="form-control select2" onchange="selectOnchang()" style='width:200px'>
										<option value="0">请选择</option>
										<option value="1">行政区</option>
										<option value="2">街道</option>
										<option value="3">社区</option>
									</select> -->
						<div id="testdiv" >
						</div>
						<div class="modal-footer">
							<button type="button"  class="btn btn-primary" onclick="addsave()" >保存</button>
							<button type="button" class="btn btn-default" style="margin-right: 43%"data-dismiss="modal">关闭</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal fade" id="myModall" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel"  aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">修改区域</h4>
						</div>
					<!-- 	<div id="districtNameTitle" style="display:none">
						
						 <label id="laberLeft" style="margin-left: 234px; "
									 >修改行政区</label></br>
							</div>		
						<div id="subofficeNameTitle" style="display:none">
						
						 <label id="laberLeft" style="margin-left: 234px;"
									 >修改街道</label></br>
							</div>		
						<div id="communityNameTitle" style="display:none">
						
						 <label id="laberLeft" style="margin-left: 234px;"
									 >修改社区</label></br>
							</div>		
					 -->
									<!-- <select id="type" name="type" class="form-control select2" onchange="selectOnchang()" style='width:200px'>
										<option value="0">请选择</option>
										<option value="1">行政区</option>
										<option value="2">街道</option>
										<option value="3">社区</option>
									</select> -->
						<!-- <div id="districtName" >
						</br><label id='laberLeftTwo'>行政区 &nbsp;   ：</label><input id='districtNameType' type='text' class='form-control select2' style='width:200px' /></br>
						<label id='laberLeftTwo'>行政区编码  ：</label><input id='districtNumber' type='text' class='form-control select2' style='width:200px' />
						</div>
						<div id="subofficeNameOne" >
						</br><label id='laberLeftTwo'>街道 &nbsp;   ：</label><input id='subofficeNameType' type='text' class='form-control select2' style='width:200px' /></br>
						<label id='laberLeftTwo'>街道编码  ：</label><input id='subofficeNumber' type='text' class='form-control select2' style='width:200px' />
						</div>
						<div id="communityName" >
						</br><label id='laberLeftTwo'>社区 &nbsp;   ：</label><input id='communityNameType' type='text' class='form-control select2' style='width:200px' /></br>
						<label id='laberLeftTwo'>社区编码  ：</label><input id='communityNumber' type='text' class='form-control select2' style='width:200px' />
						</div> -->
						<div id="upatediv"></div>
						<div class="modal-footer">
							<button type="button"  class="btn btn-primary" onclick="saveUpdate()" >保存</button>
							<button type="button" class="btn btn-default" style="margin-right: 40%"data-dismiss="modal">关闭</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="margin-top-table" style="width: 1100px; height: 565px;margin-left:5px;">
			<table id="table"></table>
		</div>
	</div>
</body>
</html>
		
									
										
	
				
		
		
