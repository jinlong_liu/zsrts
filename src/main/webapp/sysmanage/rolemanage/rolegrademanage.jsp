<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>学生成绩管理</title>
    <jsp:include page="/js/inc.jsp"></jsp:include>
    <script type="text/javascript"
            src="<%=path%>/js/jquery//jquery-3.1.0.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css"
          href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css"
          href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />

    <!-- 分页时多选弹出框插件插件 -->
    <script type="text/javascript"
            src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
    <script type="text/javascript"
            src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
    <!-- import js -->

    <script type="text/javascript"
            src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
    <script type="text/javascript"
            src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>

    <link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/sysmanage/rolemanage/rolegrademanager.css"  />
    <script type="text/javascript" src="<%=path%>/js/sysmanage/rolemanage/rolegrademanage.js"></script>

    <base>
</head>
<body>


<div class="margin-top-table" style="width:1080px;height:565px;">
    <table id="table"></table>
</div>

<script type="text/javascript">

    function search(){
        $('#table').bootstrapTable('destroy');
        /*	 radioVal = encodeURI(encodeURI(radioVal));    */
        $("#table").bootstrapTable({
            url : root+'/services/getGradeRoleList',
            method:'post',
            contentType:'application/x-www-form-urlencoded;charset=UTF-8',
            striped:true,
            pagination:true,
            pageSize:10,
            pageNumber:1,
            height:450,
            pageList:[10,20,30],
            silent: true,
            sortStable:true,
            sortName:'time',
            sortOrder:'desc',


            formatLoadingMessage:"请稍等,正在加载中。。。。。。。",
            columns:[
                /*
                {
                    checkbox:true,
                    valign:'middle',
                    align:'center'
                },
                */
                {
                    field:'number',
                    title:'number',
                    width:100,
                    valign:'middle',
                    align:'center',
                    formatter:function (value,row,index){
                        alert(value+"yonghu ")
                        return value;
                    }
                },

                {
                    field:"roleName",
                    title:'roleName',
                    width:100,
                    valign:'middle',
                    align:'center'
                },
                {
                    field:'math',
                    title:'math',
                    width:200,
                    valign:'middle',
                    align:'center'
                },
                {
                    field:'chinese',
                    title:'chinese',
                    width:180,
                    valign:'middle',
                    align:'center'
                },
                {
                    field:'english',
                    title:'english',
                    width:200,
                    valign:'middle',
                    align:'center'
                },


            ]

        });
    }
    $().ready(function(){
        /*	var radioVal=$('input:radio[name="stop_info_radio"]:checked').val();       href='reviseRole.jsp?id="+row.id+"'*/
        search();


        });

        /*	$(".stop_info_radio").change(function(){
            radioVal=$('input:radio[name="stop_info_radio"]:checked').val();
            search(radioVal);
        });
        */

</script>
</body>
</html>
