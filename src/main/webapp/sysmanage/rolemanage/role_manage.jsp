<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>角色管理</title>
    <jsp:include page="/js/inc.jsp"></jsp:include>
	<script type="text/javascript"
	src="<%=path%>/js/jquery//jquery-3.1.0.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
	
	  <!-- 分页时多选弹出框插件插件 -->
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
	<!-- import js -->

	<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>
		
	<link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/sysmanage/rolemanage/role_manage.css"  />
	<script type="text/javascript" src="<%=path%>/js/sysmanage/rolemanage/role_manage.js"></script>
		
<base>
</head>
<body>
	
<div class="infoBase">
<div class="radiodiv" style="width: 1080px">
		<a  style="border:none;margin-left:23px;" data-toggle="modal" data-target="#myModal"> 
		 <img  src="../../images/role_manger_add.png"/ > 
		<!-- 新增角色 -->
		</a>  

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">  
		<div class="modal-dialog" >  
		    <div class="modal-content">  
		        <div class="modal-header">  
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">  
		                <span aria-hidden="true">×</span>  
		            </button>  
		            <h4 class="modal-title" id="myModalLabel">新增角色</h4>  
		        </div>  
		        <div class="modal-body">  
					    <label id="userStyle" >角&nbsp;&nbsp;色&nbsp;名:</label>
					    <span id="userIdDiv"><input name="roleName" id="roleName"  style="height:30px;line-height:25px" /></span>
						</br>
					    <label id="userResvice">权&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;限:</label>
					    <label>
					    <input type="radio" name="RadioOptionss" id="inlineRadioCity" value="3" onclick="show(this)" >市级负责人
					    </label>
					    <label >
					    <input type="radio" name="RadioOptionss" id="inlineRadioOne" value="4" onclick="show(this)" >一级负责人
					    </label>
					    <label >
					    <input type="radio" name="RadioOptionss" id="inlineRadioTwo" value="5" onclick="show(this)">二级负责人
					    </label>
					    <label >
					    <input type="radio" name="RadioOptionss" id="inlineRadioThree" value="6" onclick="show(this)">三级负责人
					    </label>
					    <label >
					    <input type="radio" name="RadioOptionss" id="inlineRadioSur" value="7" onclick="show(this)" >调查员
					    </label>
						</br>
						<div id="DivHiden" >
							<label id="userStyle" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							 &nbsp;&nbsp;(注:人员管理,角色管理为一级，二级，三级负责人可选)</label>
							</br>
							<label id="userStyle" >管理权限:</label>
						    <label >
						    <input type="Checkbox" name="Checkbox" id="Checkbox" value="1" >人员管理
						    </label>
						    <label  id="lable1" style="display:inline">
						    <input type="Checkbox" name="CheckboxR" id="CheckboxR" value="2" >角色管理
						    </label>
						    <label id="lable2" style="display:inline">
						    <input type="Checkbox" name="CheckboxA" id="CheckboxA" value="8" >区域管理
						    </label>
						    <label  id="lable3" style="display:inline">
						    <input type="Checkbox" name="CheckboxC" id="CheckboxC" value="9" >参数管理
						    </label>
						     <label  id="lable7" style="display:inline">
						    <input type="Checkbox" name="CheckboxE" id="CheckboxE" value="10" >审核管理
						    </label>
					   </div>
					    <div style="height:80px;width:100%">
						<div style="width:49%;float:left;height:32px;">
						<div style="height:30px;line-height:25px;float:left;">
						<label id="describeStyle" >角色描述:</label></div>
					    <textarea name="describeActor" id="describeActor" style="width:70%;height:30px;line-height:25px"></textarea> 
						</div>
					    <div style="width:49%;float:right;height:32px;margin-right:10px;">
					    <div style="height:30px;line-height:25px;float:left;">
						<label id="remarkStyle" >备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注:</label></div>
						<textarea name="remark" id="remark" style="width:70%;height:30px;line-height:25px"></textarea>
						</div>
					    </div>
					  <!--  <label id="describeStyle" >角&nbsp;色&nbsp;描&nbsp;述:</label>
						</br>
					    <textarea name="describeActor" id="describeActor" ></textarea> 
					    </br> 
						<label id="remarkStyle" >备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注:</label>
						</br>
					    <textarea name="remark" id="remark" ></textarea>  -->
				        </div>  
				        <div class="modal-footer">  
				            <button type="button" class="btn btn-primary" onclick="save()">保存</button>  
				            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>  
				        </div> 
		    </div>  
		</div>  
		</div> 
		<!-- 模态框（Modal） -->  
		<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">  
		    <div class="modal-dialog">  
		        <div class="modal-content">  
		            <div class="modal-header">  
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>  
		                <h4 class="modal-title" id="myModalLabel">修改信息</h4>  
		            </div>  
		            <div class="modal-body">  
				            <label id="userStyle" >角&nbsp;&nbsp;色&nbsp;名:</label>
						    <input name="roleNameUp" id="roleNameUp"  style="height:30px;line-height:25px" />
							</br>
						    <label id="userResvice">权&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;限:</label>
							
						    <label>
						    <input type="radio" name="upRole" id="uproleCity" value="3" onclick="showUp(this)" >市级负责人
						    </label>
						    <label >
						    <input type="radio" name="upRole" id="uproleOne" value="4" onclick="showUp(this)" >一级负责人
						    </label>
						    <label >
						    <input type="radio" name="upRole" id="uproleTwo" value="5" onclick="showUp(this)">二级负责人
						    </label>
						    <label >
						    <input type="radio" name="upRole" id="uproleTree" value="6" onclick="showUp(this)">三级负责人
						    </label>
						    <label >
						    <input type="radio" name="upRole" id="uproleSur" value="7" onclick="showUp(this)" >调查员
						    </label>
							</br>
							<div id="DivHidenT" >
								<label id="userStyle" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							 &nbsp;&nbsp;(注:人员管理,角色管理为一级，二级，三级负责人可选)</label>
								</br>
								<label id="userStyle" >管理权限:</label>
							    <label >
							    <input type="checkbox" id ="CheckUpUser"  name="CheckUpUser"  value="1">人员管理
							    </label>
							    <label id="lable4" style="display:inline">
							    <input type="checkbox" id = "CheckUpActor"  name= "CheckUpActor"   value="2" >角色管理
							    </label>
							    <label id="lable5" style="display:inline">
							    <input type="Checkbox" name="CheckUpA" id="CheckUpA" value="8" >区域管理
							    </label>
							    <label  id="lable6" style="display:inline">
							    <input type="Checkbox" name="CheckUpC" id="CheckUpC" value="9" >参数管理
							    </label>
							    <label  id="lable8" style="display:inline">
							    <input type="Checkbox" name="CheckUpE" id="CheckUpE" value="10" >审核管理
							    </label>
						   </div>
						    <div style="height:80px;width:100%">
							<div style="width:49%;float:left;">
							<div style="height:30px;line-height:25px;float:left;">
							<label id="describeStyle" >角色描述:</label></div>
						    <textarea name="describeActorUp" id=describeActorUp style="width:70%;height:30px;line-height:25px"></textarea> 
							</div>
						    <div style="width:49%;float:right;margin-right:10px;">
						    <div style="height:30px;line-height:25px;float:left;">
							<label id="remarkStyle" >备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注:</label></div>
							<textarea name="remarkUp" id="remarkUp" style="width:70%;height:30px;line-height:25px"></textarea>
							</div>
						    </div>
						   <!-- <label id="describeStyle" >角&nbsp;色&nbsp;描&nbsp;述:</label>
							</br>
						    <textarea name="describeActorUp" id="describeActorUp" ></textarea> 
						    </br>
							<label id="remarkStyle" >备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注:</label>
							</br>
						    <textarea name="remarkUp" id="remarkUp" ></textarea>  -->
		            </div>  
		            <div class="modal-footer">  
		                <button type="button" class="btn btn-primary" onclick="updateRolenew()">提交更改</button>  
		                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>  
		            </div>  
		        </div>  
		        <!-- /.modal-content -->  
		    </div>  
		    <!-- /.modal -->  
		</div>  
		<!-- 模态框（Modal）end -->  
	</div> 
	
	<div class="margin-top-table" style="width:1080px;height:565px;">
		 <table id="table"></table> 
	 </div>
</div>
</div>
</body>
</html>
