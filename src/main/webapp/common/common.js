var lineListCommon = null;
function getNewestDate(){
	var date="";
	$.ajax({
		type : "POST",
		url :root+ "/services/getAvailableDateList",
		dataType : "json",
		async : false,
		success : function(data) {
			date=data[0].children[0].children[0].text;
		}
	});
	return date;
}

//页面一个日期时使用
function getDateTree(){
	var url=root+'/services/getAvailableDateList';
	//设置只能选择日期的节点，不能选择年和月
	$('#dateTree').combotree({
	    //获取数据URL  
	    url : url,
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].children[0].text;
			$('#dateTree').combotree('setValue',res);//取第一天
        },
	    onSelect : function(node) {
	    	//alert(JSON.stringify(node));
	        //返回树对象  
	       /* var tree = $(this).tree;  
	        //选中的节点是否为叶子节点,如果不是叶子节点,清除选中  
	        var isLeaf = tree('isLeaf', node.target);  
	        if (!isLeaf) {  
	            //清除选中  
	            //$('#dateTree').combotree('clear');  
	        }  */
	    }
	});
}

//页面一个日期时使用
function getDateTreeMonth(){
	var url=root+'/services/getAvailableDateListMonth';
	//设置只能选择日期的节点，不能选择年和月
	$('#dateTree').combotree({
	    //获取数据URL  
	    url : url,
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].text;
			$('#dateTree').combotree('setValue',res);//取第一天
      },
	    onSelect : function(node) {
	    	//alert(JSON.stringify(node));
	        //返回树对象  
	       /* var tree = $(this).tree;  
	        //选中的节点是否为叶子节点,如果不是叶子节点,清除选中  
	        var isLeaf = tree('isLeaf', node.target);  
	        if (!isLeaf) {  
	            //清除选中  
	            //$('#dateTree').combotree('clear');  
	        }  */
	    }
	});
}
//页面中当前日期和同比日期时使用
var dateJsonData="";//公用变量
var defaultDate="";//公用变量
function getCurrentAndYearDate(){
	$.ajax({
		type : "POST",
		url : root+"/services/getAvailableDateList",
		dataType : "json",
		async : false,
		success : function(data) {
			dateJsonData=data;
			defaultDate=data[0].children[0].children[0].text;
		}
	});
}

//页面中当前日期和同比日期时使用
function getCurrentAndYearDateMonth(){
	$.ajax({
		type : "POST",
		url : root+"/services/getAvailableDateListMonth",
		dataType : "json",
		async : false,
		success : function(data) {
			dateJsonData=data;
			defaultDate=data[0].children[0].text;
		}
	});
}

//给日期赋值；
function voluationDate(id){
	$('#'+id).combotree('setValue',defaultDate);//默认值
	$('#'+id).combotree('loadData', dateJsonData);//获取日期列表
}


//获取线路
function getLineDate(id){
	//设置只能选择日期的节点，不能选择年和月
	$('#'+id).combotree({
	    //获取数据URL  
	    url : root+'/services/getAvailableDateList',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].children[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
			if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
				getLine(res);
				getLineCommon(res);
			}
        },
	    onSelect : function(node) {
	    	if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
	    		getLine(node.text);
	    		var res=node.text;
	    		getLineCommon(res);
	    	}
	    }
	});
}

//获取线路
function getLineDateMonth(id){
	//设置只能选择日期的节点，不能选择年和月
	$('#'+id).combotree({
	    //获取数据URL  
	    url : root+'/services/getAvailableDateListMonth',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
			if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
				getLine(res);
				getLineCommon(res);
			}
      },
	    onSelect : function(node) {
	    	if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
	    		
	    		getLine(node.text);
	    		var res=node.text;
	    		getLineCommon(res);
	    	}
	    }
	});
}

//获取线路
function getLineDateMonthNoCommon(id){
	//设置只能选择日期的节点，不能选择年和月
	$('#'+id).combotree({
	    //获取数据URL  
	    url : root+'/services/getAvailableDateListMonth',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
			if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
				getLine(res);
				getLineCommon(res);
			}
      },
	    onSelect : function(node) {
	    	if(id == "dateTree" || id=="dateAvgTimeTree" || id == "dateTranTimeTree" || id == "dateLineTree"){
	    		getLine(node.text);
	    		//getLineCommon(res);
	    	}
	    }
	});
}
function getStop(id){
	$('#'+id).combotree({
	    //获取数据URL  
	    url : root+'/services/getAvailableDateList',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].children[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
			getStopName(res);
        },
	    onSelect : function(node) {
	    	getStopName(node.text);
	    }
	});
}
function getStopMonth(id){
	$('#'+id).combotree({
	    //获取数据URL  
	    url : root+'/services/getAvailableDateListMonth',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
			getStopName(res);
        },
	    onSelect : function(node) {
	    	getStopName(node.text);
	    }
	});
}
function getHouseholderNameDate(id){
	//设置只能选择日期的节点，不能选择年和月
	$('#'+id).combotree({
	    //获取数据URL  
		url : root+'/services/getAvailableDateList',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].children[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
			getHouseholderName(res);
        },
	    onSelect : function(node) {
	    	getHouseholderName(node.text);
	    	getLineDa($('#householderName').combobox('getText'),$('#filiale').combobox('getText'), node.text);
	    }
	});
}

//获取日期（期望线、出行量菜单使用）
function getTripDate(id){
	//设置只能选择日期的节点，不能选择年和月
	$('#'+id).combotree({
	    //获取数据URL  
	    url : root+'/services/getAvailableDateList',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].children[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
			if(id == "dateTree"){
				var cityType = $('#cityType').combobox('getValue');//城市类型
	    	 	if(cityType=="1"){//行政区
	    			getStlectData(res);
	    		}else{//街道
	    			getStreetStlectData(res);
	    		}
			}
        },
	    onSelect : function(node) {
	    	if(id == "dateTree"){
	    		var cityType = $('#cityType').combobox('getValue');//城市类型
	    	 	if(cityType=="1"){//行政区
	    			getStlectData(node.text);
	    		}else{//街道
	    			getStreetStlectData(node.text);
	    		}
	    	}
	    }
	});
}
//获取日期（期望线、出行量菜单使用）
function getTripDateMonth(id){
	//设置只能选择日期的节点，不能选择年和月
	$('#'+id).combotree({
	    //获取数据URL  
	    url : root+'/services/getAvailableDateListMonth',  
	    //选择树节点触发事件  
		onLoadSuccess:function(node,data){
			var res=data[0].children[0].text;
			$('#'+id).combotree('setValue',res);//取第一天
			if(id == "dateTree"){
				var cityType = $('#cityType').combobox('getValue');//城市类型
	    	 	if(cityType=="1"){//行政区
	    			getStlectData(res);
	    		}else{//街道
	    			getStreetStlectData(res);
	    		}
			}
      },
	    onSelect : function(node) {
	    	if(id == "dateTree"){
	    		var cityType = $('#cityType').combobox('getValue');//城市类型
	    	 	if(cityType=="1"){//行政区
	    			getStlectData(node.text);
	    		}else{//街道
	    			getStreetStlectData(node.text);
	    		}
	    	}
	    }
	});
}

function getPeriodComboBox(){
	//设置combox为复选框格式 
	$('#period').combobox({                
		formatter : function(row){
		    var opts = $(this).combobox("options");
		    return "<input type='checkbox' class='combobox-checkbox'>" + row[opts.textField];
		},   
		              
		onShowPanel : function(){
		    target = this;
		    var values = $(target).combobox("getValues");
		    $.map(values, function(value){
		        var children = $(target).combobox("panel").children();
				$.each(children, function(index, obj){
				    if(value == obj.getAttribute("value") && obj.children && obj.children.length > 0){
				        obj.children[0].checked = true;
		            }
				});
		    });
		}, 
		               
		onLoadSuccess : function(){
		    var target = this;
		    var values = $(target).combobox("getValues");
		    $.map(values, function(value){
		        var children = $(target).combobox("panel").children();
			$.each(children, function(index, obj){
			    if(value == obj.getAttribute("value") && obj.children && obj.children.length > 0){
			        obj.children[0].checked = true;
		            }
				});
		    });
		},   
		             
		onSelect : function(row){
		    var opts = $(this).combobox("options");
		    var objCom = null;
		    var children = $(this).combobox("panel").children();
		    $.each(children, function(index, obj){
		        if(row[opts.valueField] == obj.getAttribute("value")){
			    	objCom = obj;
				}
		    });
		    
		    if(objCom != null && objCom.children && objCom.children.length > 0){
		        objCom.children[0].checked = true;
		    }
		    
		    
		    //选择全天
		    if(row[opts.valueField] == '0') {
		    	$.each(children, function(index, obj){
			        if(obj.getAttribute("value") != '0'){
				    	objCom = obj;
				    	if(objCom != null && objCom.children && objCom.children.length > 0){
					        objCom.children[0].checked = false;
					        $('#period').combobox('unselect', obj.getAttribute("value"));
					    }
					}
			    });
		    }else if(row[opts.valueField] == '15,16,17,18') {
		    	$.each(children, function(index, obj){
			        if(obj.getAttribute("value") != '15,16,17,18'){
				    	objCom = obj;
				    	if(objCom != null && objCom.children && objCom.children.length > 0){
					        objCom.children[0].checked = false;
					        $('#period').combobox('unselect', obj.getAttribute("value"));
					    }
					}
			    });
		    } else if(row[opts.valueField] == '35,36,37,38') {
		    	$.each(children, function(index, obj){
			        if(obj.getAttribute("value") != '35,36,37,38'){
				    	objCom = obj;
				    	if(objCom != null && objCom.children && objCom.children.length > 0){
					        objCom.children[0].checked = false;
					        $('#period').combobox('unselect', obj.getAttribute("value"));
					    }
					}
			    });
		    } else {
		    	$.each(children, function(index, obj){
			        if(obj.getAttribute("value") == '0' || obj.getAttribute("value") == '15,16,17,18' 
			        	|| obj.getAttribute("value") == '35,36,37,38'){
				    	objCom = obj;
				    	if(objCom != null && objCom.children && objCom.children.length > 0){
					        objCom.children[0].checked = false;
					        $('#period').combobox('unselect', obj.getAttribute("value"));
					    }
					}
			    });
		    }
		},   
		             
		onUnselect : function(row){
		    var opts = $(this).combobox("options");
		    var objCom = null;
		    var children = $(this).combobox("panel").children();
		    $.each(children, function(index, obj){
		        if(row[opts.valueField] == obj.getAttribute("value")){
			    	objCom = obj;
				}
		    });
		    if(objCom != null && objCom.children && objCom.children.length > 0){
		        objCom.children[0].checked = false;
		    }
		}           
	});

	var data = [{'id':'0', 'value':'全天'}, {'id':'15,16,17,18', 'value':'早高峰'}, {'id':'35,36,37,38', 'value':'晚高峰'}, 
	       		{'id':'1', 'value':'00:00到00:30'}, {'id':'2', 'value':'00:30到01:00'},
	       		{'id':'3', 'value':'01:00到01:30'}, {'id':'4', 'value':'01:30到02:00'}, 
	       		{'id':'5', 'value':'02:00到02:30'}, {'id':'6', 'value':'02:30到03:00'},
	       		{'id':'7', 'value':'03:00到03:30'}, {'id':'8', 'value':'03:30到04:00'},
	       		{'id':'9', 'value':'04:00到04:30'}, {'id':'10', 'value':'04:30到05:00'},
	       		{'id':'11', 'value':'05:00到05:30'}, {'id':'12', 'value':'05:30到06:00'},
	       		{'id':'13', 'value':'06:00到06:30'}, {'id':'14', 'value':'06:30到07:00'},
	       		{'id':'15', 'value':'07:00到07:30'}, {'id':'16', 'value':'07:30到08:00'},
	       		{'id':'17', 'value':'08:00到08:30'}, {'id':'18', 'value':'08:30到09:00'},
	       		{'id':'19', 'value':'09:00到09:30'}, {'id':'20', 'value':'09:30到10:00'},
	       		{'id':'21', 'value':'10:00到10:30'}, {'id':'22', 'value':'10:30到11:00'},
	       		{'id':'23', 'value':'11:00到11:30'}, {'id':'24', 'value':'11:30到12:00'},
	       		{'id':'25', 'value':'12:00到12:30'}, {'id':'26', 'value':'12:30到13:00'},
	       		{'id':'27', 'value':'13:00到13:30'}, {'id':'28', 'value':'13:30到14:00'},
	       		{'id':'29', 'value':'14:00到14:30'}, {'id':'30', 'value':'14:30到15:00'},
	       		{'id':'31', 'value':'15:00到15:30'}, {'id':'32', 'value':'15:30到16:00'},
	       		{'id':'33', 'value':'16:00到16:30'}, {'id':'34', 'value':'16:30到17:00'},
	       		{'id':'35', 'value':'17:00到17:30'}, {'id':'36', 'value':'17:30到18:00'},
	       		{'id':'37', 'value':'18:00到18:30'}, {'id':'38', 'value':'18:30到19:00'},
	       		{'id':'39', 'value':'19:00到19:30'}, {'id':'40', 'value':'19:30到20:00'},
	       		{'id':'41', 'value':'20:00到20:30'}, {'id':'42', 'value':'20:30到21:00'},
	       		{'id':'43', 'value':'21:00到21:30'}, {'id':'44', 'value':'21:30到22:00'},
	       		{'id':'55', 'value':'22:00到22:30'}, {'id':'46', 'value':'22:30到23:00'},
	       		{'id':'47', 'value':'23:00到23:30'}, {'id':'48', 'value':'23:30到00:00'}];
	        $('#period').combobox('loadData', data);
	        $('#period').combobox('select', 0);
}

/**
 * 获取当前日期，返回格式yyyy-MM-dd
 * @returns {String}
 */
function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = year + seperator1 + month + seperator1 + strDate;
    return currentDate;
}

/**
 * 获取当前日期，返回格式yyyy-MM-dd HH:mm:ss
 * @returns {String}
 */
function getNowFormatTime() {
    var date = new Date();
    var seperatorDate = "-";
    var seperatorTime = ":";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentTime = year + seperatorDate + month + seperatorDate + strDate
            + " " + date.getHours() + seperatorTime + date.getMinutes()
            + seperatorTime + date.getSeconds();
    return currentTime;
}

//勾选同比复选框
function checkedOnYear(obj,a,select){
	var flag=$("#"+obj).is(":checked");
	if (flag) {
		$('#'+a).linkbutton('enable'); 
		$('#'+select).combotree('enable');
	}else{
		$('#'+a).linkbutton('disable');
		$('#'+select).combotree('disable');
	}
	
}

//获取json地图版本号
var geoJsonPath="";//公用变量
function getGeoJsonVersion(date){
	var geoJsonPath="geoJson/";
	$.ajax({
		type : "POST",
		url : root+"/services/getGeoJsonVersionList?date="+date,
		dataType : "json",
		async : false,
		success : function(data) {
			geoJsonPath+=data.path;
		}
	});
	geoJsonPath+="/china-main-city/";
	return geoJsonPath;
}

//获取geoserver地图版本号
var geoServerPath="";//公用变量
function getGeoServerVersion(date){
	var geoServerPath="";
	var url=root+"/services/getGeoServerVersionList?date="+date;
	$.ajax({
		type : "POST",
		url : url,
		dataType : "json",
		async : false,
		success : function(data) {
			geoServerPath+=data.path;
		}
	});
	return geoServerPath;
}

function getUrlPath(){
	var url=window.location.protocol+"//"+window.location.host;
	var gis_add_local=url+"/geoserver/dcutp/wms";
	var route_segment_sld=url+"/zstraffic/js/map/routeSegment_.sld";
	var busway_sid=url+"/zstraffic/js/map/busway_.sld";
	var route_class_sld=url+"/zstraffic/js/map/routeClass_";
	var configInfo = '{gis_add_local:"'+gis_add_local+'",route_segment_sld:"'+route_segment_sld+'",busway_sid:"'+busway_sid+'",route_class_sld:"'+route_class_sld+'"}';
	$("#gisConfigInfo").val(configInfo);
}

//$(function(){
//    $('#line').combobox({
//        onSelect : function(record){
//    	getLineContrast();
//    }
//});
//});  

//对比 输入的线路是否 存在 在下拉框中
function getLineContrast(id){
	var flag=false;
	var selectValue=$('#'+id).combobox('getText'); 
	var lineCommonlist=lineListCommon;
	var newList=[];
	for(var i=0;i<lineCommonlist.length;i++){
		var linText=lineCommonlist[i].text;
		newList.push(linText);
	}
	if(newList.indexOf(selectValue)==-1){
		
		flag=true;
	}
	return flag;
}

//获取线路
	function getLineCommon(date){
		var url = root+"/services/getLineList?date="+date;
		$.ajax({
			type : "POST",
			url : url,
			dataType : "json",
			async : false,
			success : function(result) {
					var lineList=result.lineList;
					lineListCommon=lineList;
			}
		});
	}
	
