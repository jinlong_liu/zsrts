<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <jsp:include page="./js/inc.jsp"></jsp:include>
    <link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/login_styles.css" />
    <link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/login/login.css" />
    <script type="text/javascript" src="<%=path%>/js/login/login.js"></script>
    <script type="text/javascript" src="<%=path%>/js/common/md5.js"></script>
    
   
</head>
<body class="login-body"  >
    <form action="">
	    <div class="login-title"></div>
		<div class="login-text">
				<!-- input 图标（img）和文本框（input）不要换行，否则中间会用空隙  -->
				<div class="div-login-text2"><img src="./images/login_txt2.png"></div>
				<div class="div-login-line"><img src="./images/login_line.png"></div>
				<div class="div-login-username"><img src="./images/login_username.png" class="login-username"><input id="username" name="username" type="text" class="login-input_username" placeholder="用户名 "/></div>
				<div class="div-login-pwd"><img src="./images/login_password.png" class="login-password"><input id="password" name="password" type="password" class="login-input_password" placeholder="密码 "/></div>
				<div class="div-login-signIn"><input class="login-input-loginbtn" id="login" name="login" type="button" onclick="signIn()"/></div> 
		</div>
	</form>
</body>
</html>