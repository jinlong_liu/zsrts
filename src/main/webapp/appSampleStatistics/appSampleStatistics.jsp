<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>问卷统计</title>
    <jsp:include page="/js/inc.jsp"></jsp:include>
	<script type="text/javascript"
	src="<%=path%>/js/jquery//jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />
		
	 <link rel="stylesheet" type="text/css" href="<%=path%>/third_party/sweet-alert/sweet-alert.css" /> 
	
	<!-- 时间控件     -->
	<link rel="stylesheet" type="text/css"
	   href="<%=path%>/third_party/bootstrap/datetimepicker/css/bootstrap-datetimepicker.css" />
    <script type="text/javascript"
	   src="<%=path%>/third_party/bootstrap/datetimepicker/js/moment-with-locales.js"></script>
    <script type="text/javascript" 
       src="<%=path%>/third_party/bootstrap/datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" 
       src="<%=path%>/third_party/bootstrap/locales/bootstrap-datepicker.zh-CN.js"></script>
	
	  <!-- 分页时多选弹出框插件插件 -->
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
	  <script type="text/javascript"
			src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
	<!-- import js -->
	
	<script type="text/javascript"src="<%=path%>/third_party/sweet-alert/sweet-alert.min.js"></script>
	<script type="text/javascript" src="<%=path%>/third_party/echarts/echarts.js"></script> 
	

	<%-- <script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script> --%>
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
	<script type="text/javascript"
		src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>
		
	<link rel = "stylesheet" type = "text/css" href = "<%=path%>/css/appSampleStatistics/appSampleStatistics.css"  />
	<script type="text/javascript" src="<%=path%>/js/appSampleStatistics/appSampleStatistics.js"></script>
		
	<link href="<%=path%>/parametersChange/DataCenter.css" rel="stylesheet" type="text/css" />	
<base>
</head>
<body>
 <div class="topBoder" >
 <label id='userStyle' style="width: 36px;margin-left:10px;">性别:</label>					  
				<select id='sex' name='type'  class='js-example-disabled-results' style='width:70px;height: 30px;margin-top:10px;'  >
				<option value='-1'>请选择</option> 
				<option value='0'>男</option> 
				<option value='1'>女</option> 
				</select> 
 <label id='age' style="width: 38px;margin-left:15px;">年龄:</label><input id='starAge' class='js-example-disabled-results' name='type' style='width:70px; height: 30px;margin-top:10px;'>
 <label id='age' style="width: 20px;">至:</label>
 <input id='endAge' class='js-example-disabled-results' name='type' style='width:70px; height: 30px;margin-top:10px;'>
 <label id='userStyle' style="width: 92px;margin-left:16px;">是否常住本地:</label>					  
				<select id='isLocal' name='type'  class='js-example-disabled-results' style='width:70px;height: 30px;margin-top:10px;'  >
				<option value='-1'>请选择</option> 
				<option value='0'>是</option> 
				<option value='1'>否</option> 
				</select> 
<label id='userStyle' style="width: 39px;margin-left:15px;">职业:</label>					  
				<select id='work' name='type'  class='js-example-disabled-results' style='width:160px;height: 30px;margin-top:10px;'  >
				<option value='-1'>请选择</option> 
				<option value='1'>企业公司员工</option> 
				<option value='2'>政府及事业单位人</option> 
				<option value='3'>学生</option> 
				<option value='4'>退休</option> 
				<option value='5'>个体经营或自由职业</option> 
				<option value='6'>农林牧鱼业人员</option> 
				<option value='7'>无业</option> 
				<option value='8'>其他</option> 
				</select> 
			<!-- 	
		   <div class='col-sm-6' style="width:260px;falot:left; margin-left: 660;margin-top: -43px;">  
            <div class="form-group">  
				<label id='userStyle' style="width: 92px;margin-left:0px;">日期：</label>
                <div class='input-group date' id='datetimepickertime' style="width:200px;float:right;margin-top:-32px;margin-right:90px;">  
                 <input type='text' class="form-control" name="time" id="date" style="width: 92px;margin-left:0px;" onblur="tomtk()"/>  
                    <span class="input-group-addon">  
                        <span class="glyphicon glyphicon-calendar"></span>  
                    </span> 
                    
              </div>
             </div>
            </div> -->
            
            
            <div class='col-sm-6' style="width:260px;falot:left; margin-left: 752px;margin-top: -34px;">  
            <div class="form-group">  
            <div class="font-registeredStart" style="float:left; margin-top: 6px;">日期：</div>
                <div class='input-group date' id='datetimepickertime'>  
                 <input type='text' class="form-control" id="toDate" name="time" />  
                    <span class="input-group-addon">  
                        <span class="glyphicon glyphicon-calendar"></span>  
                    </span>  
                </div>  
            </div>  
        </div>  
               <button type='button' style='border-radius:6px;border:none;width:60px;background:#06b370;color:white;margin-left:260px;height: 30px;' onclick='search()'> 
				    查    询
		      </button> 
				
	 </div>

	 <div  class="margin-top-table" style="width:1080px;height:456px;">
	 <div id = 'avg' class="avg"> 
	  		平均出行次数:
	 </div>
	 <div id="view"  style="width:1080px;height:400px;"></div> 
	 </div>
	

</body>
<script type="text/javascript">

var picker = $('#datetimepickertime').datetimepicker({  
    format: 'YYYY-MM-DD',
    locale: moment.locale('zh-cn'),  
    //minDate: '2016-7-1'  
});  


</script>
</html>
