<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
String authDegree = request.getSession().getAttribute("authDegree").toString();
String userName = request.getSession().getAttribute("userName").toString();
String[] array = authDegree.split(",");

Integer isSuperAdmin =  (Integer) session.getAttribute("isSuperAdmin");//获取用户是否为超级管理员
if(isSuperAdmin == null){
	isSuperAdmin = 0 ;
}

%>

<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>问卷查询</title>

<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript"
	src="<%=path%>/js/jquery//jquery-2.1.3.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/table/bootstrap-tablea.css" />

<!-- 分页时多选弹出框插件插件 -->
<script type="text/javascript"
	src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
<script type="text/javascript"
	src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
<!-- import js -->

<%-- <script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-1.9.1.min.js"></script> --%>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/table/bootstrap-tablea.js"></script>

<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/questionnaire/ques_tionnaire.css" />
<script type="text/javascript"
	src="<%=path%>/js/questionnaire/ques_tionnaire.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/third_party/maskLayer/DataCenter.css" />
<base>
</head>
<body>
	<input type="hidden" id="userName" value="${isSuperAdmin }">
	<input type="hidden" id="authDegree" value=${authDegree }>
	<div class="bgBox">
		<div class="loaderTip">
			<img src="../images/loading.gif" />
			<div class="loaderTip_msg">数据正在加载中，请稍候...</div>
		</div>
	</div>
	<!-- <div style="width: 31%;margin-top: 15px;">
		 <input type="radio" style="margin-left:10px" name="chSelect" checked="checked" value="1" onclick="query()"><label id='userStyle' style="width: 60px;margin-left:5px;">普通问卷</label>
		 &nbsp; &nbsp;
		 <input type="radio" name="chSelect" value="2" onclick="query()"><label id='userStyle' style="width: 60px;margin-left:5px;">公众问卷</label> -->
	<div class="border">
		<div style="width: 31%; margin-top: 14px;">
			<input type="radio" style="margin-left: 10px" name="chSelect"
				checked="checked" value="1" onclick="query()"><label
				id='userStyle' style="width: 17%; margin-left: 3%;">普通问卷</label>
			&nbsp; &nbsp; <input type="radio" name="chSelect" value="2"
				onclick="query()"><label id='userStyle'
				style="width: 17%; margin-left: 3%;">公众问卷</label>

		</div>
		<!-- 条件查询下拉框 -->
		<div id="public" class="public">
			<label id='userStyle' style="width: 7%; margin-left: -58px;">行政区:</label>
			<select id='district' name='type' class='js-example-disabled-results'
				style='width: 118px; height: 30px; margin-top: 10px;'>
				<option value=''>请选择</option>
			</select>
			<button type='button'
				style='border-radius: 6px; border: none; width: 80px; background: #06b370; color: white; margin-top: 5px; margin-left: 24.5px; height: 30px;'
				onclick='queryPublic()'>查 询</button>
		</div>
		<div id="Conditions"
			style="width: 73%;; margin-left: 27%; margin-top: -3.99%; height: 46px;">
			<label id='userStyle' style="width: 7%; margin-left: -58px;">行政区:</label>
			<select id='districtName' name='type'
				class='js-example-disabled-results'
				style='width: 118px; height: 30px; margin-top: 10px;'
				onchange='diArea(2)'>
				<option value=''>请选择</option>
			</select> <label id='userStyle' style="width: 4.5%; margin-left: 1%;">街道:</label>
			<select id='subofficeName' name='type'
				class='js-example-disabled-results'
				style='width: 118px; height: 30px; margin-top: 10px;'
				onchange='dicommunityName()'>
				<option value=''>请选择</option>
			</select> <label id='userStyle' style="width: 4.5%; margin-left: 1%;">社区:</label>
			<select id='communityName' name='type'
				class='js-example-disabled-results'
				style='width: 118px; height: 30px; margin-top: 10px;'>
				<option value=''>请选择</option>
			</select> <label id='userStyle' style="width: 9%; margin-left: 3%;">家庭编号:</label><input
				id='familyId' class='js-example-disabled-results' name='type'
				style='width: 118px; height: 30px; margin-top: 10px;'>
			<button type='button'
				style='border-radius: 6px; border: none; width: 80px; background: #06b370; color: white; margin-top: 5px; margin-left: 24.5px; height: 30px;'
				onclick='queryAreaa()'>查 询</button>
		</div>
	</div>

	<div class="photo" id="imgOne">
		<img alt="" src="../images/quan.png">&nbsp; 统计信息
	</div>

	<div class="quest" id="information"></div>
	<div class="margin-top-table" id="tableOne"
		style="width: 1100px; height: 550px; margin-left: 5px; margin-top: 8px;">
		<table id="table">
		</table>
	</div>
	<div class="margin-top-table" id="tableTwo"
		style="width: 1100px; height: 550px; margin-left: 5px; margin-top: 8px;">
		<table id="tablePublic">
		</table>
	</div>


	<!-- <div id="myModal" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">x</button>
              <p>问卷详情</p>
    </div>
    <div class="modal-body">
    
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">取消</a>
        <a href="#" class="btn btn-primary" data-dismiss="modal">确定</a>
    </div>
</div> -->

</body>
</html>



