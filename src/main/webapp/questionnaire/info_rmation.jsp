<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
String authDegree = request.getSession().getAttribute("authDegree").toString();
String[] array = authDegree.split(",");
%>


<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>问卷详情</title>

<jsp:include page="/js/inc.jsp"></jsp:include>
<script type="text/javascript"
	src="<%=path%>/js/jquery//jquery-3.1.0.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/bootstrap/table/bootstrap-table.css" />

<!-- 分页时多选弹出框插件插件 -->
<script type="text/javascript"
	src="<%=path %>/js/plug-in/artDialog4.1.6/artDialog.js?skin=blue"></script>
<script type="text/javascript"
	src="<%=path %>/js/plug-in/artDialog4.1.6/plugins/iframeTools.js"></script>
<!-- import js -->
<!-- alert样式 -->
<link rel="stylesheet" type="text/css"
	href="<%=path%>/third_party/sweet-alert/sweet-alert.css" />
<script type="text/javascript"
	src="<%=path%>/third_party/sweet-alert/sweet-alert.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/js/bootstrap/table/bootstrap-table.js"></script>

<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/questionnaire/info_rmation.css" />
<script type="text/javascript"
	src="<%=path%>/js/questionnaire/info_rmation.js"></script>

<base>

</head>
<body onLoad="scrollTo(0,0)">
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 420px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">选择审核状态</h4>
				</div>
				<div id="selectDiv">
					</br> <label id="laberLeft" style="margin-left: 108px">选择状态：</label>
					<input type="radio" name="chSelect" value="1">通过 &nbsp;
					&nbsp; <input type="radio" name="chSelect" value="0">不通过
					&nbsp; &nbsp;



					<!-- <input type="hidden" id="checkSave" value="0"> -->
					<!--  <input type="hidden" id="id" value=" ">
									  <input type="hidden" id="oldName" value=" "> -->

				</div>

				<div id="testdiv"></div>
				<div class="modal-footer">
					<!-- 						<button type="button"  class="btn btn-primary" style="margin-right: 45%" onclick="save()" >确认审核</button>
							<button type="button" class="btn btn-default" style="margin-right: 43%"data-dismiss="modal">取消审核</button> -->
					<button type="button" class="btn btn-primary" onclick="save()">确认</button>
					<button type="button" class="btn btn-default"
						style="margin-right: 37%" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	<div>
		<input id="familyId" value="${param.id}" type="hidden" />
		<div class=title>
			<img src="../images/rectangular.png"
				onclick="window.history.back(-1);"> &nbsp; 问卷详情
		</div>
		<div class=left id="leftSide">
			<div style="float: left; margin-top: 4px; margin-left: 18px;">
				<img src="../images/quan.png">
			</div>
			<div
				style="width: 120px; height: 25px; margin-left: 45px; font-size: 16px; color: rgb(6, 179, 112);">问卷状态</div>
			<div id="quesr"
				style="width: 122px; height: 20px; margin-top: 15px; margin-left: 260px; font-size: 14px; float: left"></div>
			<div id="progress"
				style="width: 122px; height: 20px; margin-top: -19px; margin-left: 45px; font-size: 14px; float: left"></div>
			<c:forEach var="auth" items="<%=array  %>">
				<c:if test="${auth == 10}">
					<button type="button" class="btn btn-primary" id="auditButton"
						style="heigth: 27px; margin-left: 479px; margin-top: -30px;"
						modal" data-target="#myModal" onclick="uodateTotal()">
						审核
						<!-- 修改参数 -->
					</button>
				</c:if>
			</c:forEach>
			<!--   <div style="float: left; margin-top: 1059px; margin-left: 21px;"><img src="../images/quan.png"></div>
  <div style="width: 120px; margin-top: 1092px;height: 25px; margin-left: 45px; font-size: 16px; color: rgb(6, 179, 112);">出行信息</div> -->
		</div>
	</div>
</body>
</html>













