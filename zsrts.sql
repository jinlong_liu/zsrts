/*
 Navicat Premium Data Transfer

 Source Server         : MQTT
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : 47.93.53.225:3306
 Source Schema         : zsrts

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 26/08/2020 20:26:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_bus_info
-- ----------------------------
DROP TABLE IF EXISTS `app_bus_info`;
CREATE TABLE `app_bus_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `displacement` float DEFAULT NULL,
  `actual_service_life` float DEFAULT NULL,
  `isfixed_parking` int(255) DEFAULT NULL,
  `bus_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `bus_property` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for app_family_information
-- ----------------------------
DROP TABLE IF EXISTS `app_family_information`;
CREATE TABLE `app_family_information`  (
  `family_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `family_type` int(255) DEFAULT NULL,
  `relationship_householder` int(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `household_type` int(11) DEFAULT NULL,
  `is_permanent_address` int(255) DEFAULT NULL,
  `income` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `profession` int(11) DEFAULT NULL,
  `educational` int(11) DEFAULT NULL,
  `ic_card` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `company_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `is_marry` int(11) DEFAULT NULL,
  `profession_custom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`family_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_family_information
-- ----------------------------
INSERT INTO `app_family_information` VALUES (1, 1, 1, 1, 1, 18, 1, 1, NULL, 3, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `app_family_information` VALUES (2, 2, 1, 1, 0, 19, 2, 0, '6', 1, NULL, NULL, '青岛科技大学', 1, NULL, NULL);

-- ----------------------------
-- Table structure for app_log_info
-- ----------------------------
DROP TABLE IF EXISTS `app_log_info`;
CREATE TABLE `app_log_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `mobile_phone_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `mobile_phone_manufacturers` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `operating_system_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `login_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `city_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for app_sms
-- ----------------------------
DROP TABLE IF EXISTS `app_sms`;
CREATE TABLE `app_sms`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `city_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `request_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `verification_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `send_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `is_success` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area`  (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `district_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `district_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `parent_suboffice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `suboffice_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `suboffice_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `parent_communiity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `community_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `community_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL,
  `everyday_count` int(11) DEFAULT NULL,
  `follow_count` int(11) DEFAULT NULL,
  `submit_count` int(11) DEFAULT NULL,
  `not_complete_count` int(11) DEFAULT NULL,
  `city_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`area_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth
-- ----------------------------
DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bus_info
-- ----------------------------
DROP TABLE IF EXISTS `bus_info`;
CREATE TABLE `bus_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inhabitant_id` int(11) DEFAULT NULL,
  `displacement` float DEFAULT NULL,
  `actual_ervice_life` float(255, 0) DEFAULT NULL,
  `isfixed_parking` int(255) DEFAULT NULL,
  `bus_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `bus_property` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for charge
-- ----------------------------
DROP TABLE IF EXISTS `charge`;
CREATE TABLE `charge`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `charge_type` int(11) DEFAULT NULL,
  `registered_quantity` int(255) DEFAULT NULL,
  `is_charge_status` int(11) DEFAULT NULL,
  `charge_call_times` int(11) DEFAULT NULL,
  `extended1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extended2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of charge
-- ----------------------------
INSERT INTO `charge` VALUES (1, 1, NULL, NULL, 1, NULL, NULL, '2');

-- ----------------------------
-- Table structure for city_code_url
-- ----------------------------
DROP TABLE IF EXISTS `city_code_url`;
CREATE TABLE `city_code_url`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `city_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `city_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extended_field_one` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extended_field_two` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `prize_sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extended_field_three` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `signature_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for famliy_infomation
-- ----------------------------
DROP TABLE IF EXISTS `famliy_infomation`;
CREATE TABLE `famliy_infomation`  (
  `family_id` int(11) NOT NULL AUTO_INCREMENT,
  `family_type` int(11) DEFAULT NULL,
  `relationship_householder` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `household_type` int(255) DEFAULT NULL,
  `is_permanent_address` int(11) DEFAULT NULL,
  `income` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `profession` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `educational` int(11) DEFAULT NULL,
  `is_card` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `company_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `is_marry` int(11) DEFAULT NULL,
  PRIMARY KEY (`family_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inhabitan
-- ----------------------------
DROP TABLE IF EXISTS `inhabitan`;
CREATE TABLE `inhabitan`  (
  `inhabitant_id` int(11) NOT NULL AUTO_INCREMENT,
  `inhabitant_number` int(11) DEFAULT NULL,
  `inhabitant_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `bike_count` int(11) DEFAULT NULL,
  `electric_bikes` int(11) DEFAULT NULL,
  PRIMARY KEY (`inhabitant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for out_kilometre_scene
-- ----------------------------
DROP TABLE IF EXISTS `out_kilometre_scene`;
CREATE TABLE `out_kilometre_scene`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kilometre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `scene` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `time_total` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `time_go` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `time_wait` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `time_riding` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `price_ticket` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extend_one` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extend_two` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cost_total` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cost_working` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `transportation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extend_three` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extend_four` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for rechargeable_card
-- ----------------------------
DROP TABLE IF EXISTS `rechargeable_card`;
CREATE TABLE `rechargeable_card`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recharge_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `recharge_type` int(11) DEFAULT NULL,
  `valid_state` int(11) DEFAULT NULL,
  `extended1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extended2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for register_number
-- ----------------------------
DROP TABLE IF EXISTS `register_number`;
CREATE TABLE `register_number`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `register_type` int(255) DEFAULT NULL,
  `register_num_day` int(255) DEFAULT NULL,
  `register_num_total` int(255) DEFAULT NULL,
  `register_num_actual` int(255) DEFAULT NULL,
  `extend1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extend2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for residents_travel_information
-- ----------------------------
DROP TABLE IF EXISTS `residents_travel_information`;
CREATE TABLE `residents_travel_information`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inhabitant_id` int(11) DEFAULT NULL,
  `family_id` int(11) DEFAULT NULL,
  `star_trip_time` int(11) DEFAULT NULL,
  `end_trip_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trip_mode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `trip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `bus_line` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `trip_purposes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for statistics_particular
-- ----------------------------
DROP TABLE IF EXISTS `statistics_particular`;
CREATE TABLE `statistics_particular`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) DEFAULT NULL,
  `submit_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `submitter` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `title_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lon` float DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `family_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `submit_count` int(255) DEFAULT NULL,
  `submit_stage` int(11) DEFAULT NULL,
  `follow_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `district_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `district_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `parent_suboffice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `suboffice_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `suboffice_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `parent_communiity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `community_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `community_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `ctiy_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `is_surveryor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extened_field_one` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extened_field_two` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extened_field_three` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `submit_lon` float DEFAULT NULL,
  `submit_lat` float DEFAULT NULL,
  `position_deviate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `remind_number` int(11) DEFAULT NULL,
  `num_title` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_appuser
-- ----------------------------
DROP TABLE IF EXISTS `sys_appuser`;
CREATE TABLE `sys_appuser`  (
  `appuser_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `electric_bikes` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` int(255) DEFAULT NULL,
  `Occupation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `family_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `stag` int(11) DEFAULT NULL,
  `car_count` int(11) DEFAULT NULL,
  `family_mumber_count` int(11) DEFAULT NULL,
  `city_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`appuser_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_appuser
-- ----------------------------
INSERT INTO `sys_appuser` VALUES (1, '测试1', '123456', '2020-08-20 18:24:06', 1, 13, 1, '苦逼开发', '地球', 1, 1, 4, '1');
INSERT INTO `sys_appuser` VALUES (2, '测试2', '123', '2020-08-23 19:25:54', 2, 15, 0, '苦逼测试', '山东', 2, 2, 2, '2');
INSERT INTO `sys_appuser` VALUES (3, '刘海旭', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_appuser` VALUES (4, '郝伯炎', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_appuser` VALUES (5, '杨超', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_appuser` VALUES (6, '胡进宝', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_appuser_thirdparty
-- ----------------------------
DROP TABLE IF EXISTS `sys_appuser_thirdparty`;
CREATE TABLE `sys_appuser_thirdparty`  (
  `id` int(11) NOT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  `mac_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_appuser_thirdparty
-- ----------------------------
INSERT INTO `sys_appuser_thirdparty` VALUES (1, 5, 'E63CD9DFF7A7');
INSERT INTO `sys_appuser_thirdparty` VALUES (2, 3, 'CBD66DED55DF\r\n');
INSERT INTO `sys_appuser_thirdparty` VALUES (3, 4, 'EBEAADF8A9F3');
INSERT INTO `sys_appuser_thirdparty` VALUES (4, 6, ' FDE6AE627C33');

-- ----------------------------
-- Table structure for sys_authorization
-- ----------------------------
DROP TABLE IF EXISTS `sys_authorization`;
CREATE TABLE `sys_authorization`  (
  `auth_id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_degree` int(255) DEFAULT NULL,
  `auth_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`auth_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_authorization
-- ----------------------------
INSERT INTO `sys_authorization` VALUES (1, 1, '人员管理权限');
INSERT INTO `sys_authorization` VALUES (2, 2, '角色管理权限');
INSERT INTO `sys_authorization` VALUES (3, 3, '市级负责人');
INSERT INTO `sys_authorization` VALUES (4, 4, '一级负责人');
INSERT INTO `sys_authorization` VALUES (5, 5, '二级负责人');
INSERT INTO `sys_authorization` VALUES (6, 6, '三级负责人');
INSERT INTO `sys_authorization` VALUES (7, 7, '调查员');
INSERT INTO `sys_authorization` VALUES (8, 8, '区域管理权限');
INSERT INTO `sys_authorization` VALUES (9, 9, '参数管理权限');
INSERT INTO `sys_authorization` VALUES (10, 10, '审核管理权限');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `describeActor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `operator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (2, '管理员', '人员管理权限', '2020-08-20 17:36:28', '一级负责人', 'joker');
INSERT INTO `sys_role` VALUES (3, '超级管理员', '全部权限', '2020-08-20 19:22:31', '全部权限', 'joker');

-- ----------------------------
-- Table structure for sys_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_auth`;
CREATE TABLE `sys_role_auth`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_role_id` int(11) DEFAULT NULL,
  `sys_auth_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_auth
-- ----------------------------
INSERT INTO `sys_role_auth` VALUES (4, 2, 8);
INSERT INTO `sys_role_auth` VALUES (5, 2, 3);
INSERT INTO `sys_role_auth` VALUES (6, 3, 1);
INSERT INTO `sys_role_auth` VALUES (7, 3, 2);
INSERT INTO `sys_role_auth` VALUES (8, 3, 8);
INSERT INTO `sys_role_auth` VALUES (9, 3, 9);
INSERT INTO `sys_role_auth` VALUES (10, 3, 10);
INSERT INTO `sys_role_auth` VALUES (11, 3, 3);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `realName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `operator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `is_super_admin` int(11) DEFAULT NULL,
  `city_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (5, 'admin', '超级管理员', 'e10adc3949ba59abbe56e057f20f883e', '123', '2020-08-20 07:24:04', '', NULL, '1101110110', 1, NULL);
INSERT INTO `sys_user` VALUES (11, 'ordinary', '管理员', 'e10adc3949ba59abbe56e057f20f883e', '管理员权限', '2020-08-23 07:57:04', 'admin', NULL, '1101110110', 0, NULL);
INSERT INTO `sys_user` VALUES (12, 'manager', '超级管理员', 'e10adc3949ba59abbe56e057f20f883e', '超级管理员', '2020-08-23 07:57:38', 'admin', NULL, '17664521331', 1, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_User_id` int(11) DEFAULT NULL,
  `sys_role_id` int(11) DEFAULT NULL,
  `create_date` datetime(0) DEFAULT NULL,
  `operator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (3, 5, 3, '2020-08-20 19:24:05', 'joker');
INSERT INTO `sys_user_role` VALUES (8, 10, 3, '2020-08-23 19:51:32', 'admin');
INSERT INTO `sys_user_role` VALUES (9, 11, 2, '2020-08-23 19:57:05', 'admin');
INSERT INTO `sys_user_role` VALUES (10, 12, 3, '2020-08-23 19:57:39', 'admin');

-- ----------------------------
-- Table structure for trajectory_2020_08_21
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_08_21`;
CREATE TABLE `trajectory_2020_08_21`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trajectory_2020_08_21
-- ----------------------------
INSERT INTO `trajectory_2020_08_21` VALUES (2, 1, '2020-08-21 18:19:22', '2020-08-21 18:19:09', '120.48471,36.120222');
INSERT INTO `trajectory_2020_08_21` VALUES (3, 1, '2020-08-21 19:10:48', '2020-08-21 19:10:48', '116.377823,39.931978');
INSERT INTO `trajectory_2020_08_21` VALUES (4, 1, '2020-08-21 19:11:18', '2020-08-21 19:11:20', '110.165241,20.032594');
INSERT INTO `trajectory_2020_08_21` VALUES (5, 1, '2020-08-21 19:11:55', '2020-08-21 19:11:57', '87.604729,43.828587');
INSERT INTO `trajectory_2020_08_21` VALUES (6, 4, '2020-08-26 11:47:15', '2020-08-26 11:47:15', '11.220000267028809,22.110000610351562');

-- ----------------------------
-- Table structure for trajectory_2020_08_26
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_08_26`;
CREATE TABLE `trajectory_2020_08_26`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1869 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for trajectory_2020_08_27
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_08_27`;
CREATE TABLE `trajectory_2020_08_27`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1869 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for trajectory_2020_08_28
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_08_28`;
CREATE TABLE `trajectory_2020_08_28`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1869 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for trajectory_2020_08_29
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_08_29`;
CREATE TABLE `trajectory_2020_08_29`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1869 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for trajectory_2020_08_30
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_08_30`;
CREATE TABLE `trajectory_2020_08_30`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1869 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for trajectory_2020_08_31
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_08_31`;
CREATE TABLE `trajectory_2020_08_31`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trajectory_2020_08_31
-- ----------------------------
INSERT INTO `trajectory_2020_08_31` VALUES (2, 1, '2020-08-21 18:19:22', '2020-08-21 18:19:09', '120.48471,36.120222');
INSERT INTO `trajectory_2020_08_31` VALUES (3, 1, '2020-08-21 19:10:48', '2020-08-21 19:10:48', '116.377823,39.931978');
INSERT INTO `trajectory_2020_08_31` VALUES (4, 1, '2020-08-21 19:11:18', '2020-08-21 19:11:20', '110.165241,20.032594');
INSERT INTO `trajectory_2020_08_31` VALUES (5, 1, '2020-08-21 19:11:55', '2020-08-21 19:11:57', '87.604729,43.828587');
INSERT INTO `trajectory_2020_08_31` VALUES (6, 4, '2020-08-26 11:47:15', '2020-08-26 11:47:15', '11.220000267028809,22.110000610351562');

-- ----------------------------
-- Table structure for trajectory_2020_09_01
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_09_01`;
CREATE TABLE `trajectory_2020_09_01`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1869 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for trajectory_2020_09_02
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_09_02`;
CREATE TABLE `trajectory_2020_09_02`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1869 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for trajectory_2020_09_03
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_09_03`;
CREATE TABLE `trajectory_2020_09_03`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1869 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for trajectory_2020_09_04
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_2020_09_04`;
CREATE TABLE `trajectory_2020_09_04`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1869 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for trajectory_date
-- ----------------------------
DROP TABLE IF EXISTS `trajectory_date`;
CREATE TABLE `trajectory_date`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trajectory_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trajectory_date
-- ----------------------------
INSERT INTO `trajectory_date` VALUES (1, 1, '2020-08-21 18:18:51', '2020-08-21 18:18:51', '120.487189,36.125455');
INSERT INTO `trajectory_date` VALUES (2, 1, '2020-08-21 18:19:22', '2020-08-21 18:19:09', '120.48471,36.120222');

-- ----------------------------
-- Table structure for transportation_time
-- ----------------------------
DROP TABLE IF EXISTS `transportation_time`;
CREATE TABLE `transportation_time`  (
  `id` int(11) NOT NULL,
  `time_start` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `time_end` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `transportation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `kilometre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extend_one` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `extend_two` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for travel_information
-- ----------------------------
DROP TABLE IF EXISTS `travel_information`;
CREATE TABLE `travel_information`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `end_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `start_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `end_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `start_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `end_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `trip_mode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `trip_objective` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date` datetime(0) DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `trip_time` int(11) DEFAULT NULL,
  `is_send_children` int(11) DEFAULT NULL,
  `send_adress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `send_date` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `trip_gps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `start_format_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `end_format_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `city_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of travel_information
-- ----------------------------
INSERT INTO `travel_information` VALUES (1, 1, '2020-08-21 10:23:01', '2020-08-23 19:25:07', '泰安', '济南', '21.21.21.21', '22.22.22.22', '1', '1', 1, '2020-08-20 00:00:00', NULL, NULL, NULL, 1, '青岛科技大学附属幼儿园', '2020-08-23 19:25:07', '1', NULL, NULL, NULL, NULL);
INSERT INTO `travel_information` VALUES (2, 1, '2020-08-23 20:23:04', '2020-08-23 20:23:04', '泰安', '青岛', '22.2.2.2.2', '2.2.2.2.2.2', '2', '2', 1, '2020-08-20 00:00:00', NULL, NULL, NULL, 1, NULL, '2020-08-23 19:25:24', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user_area
-- ----------------------------
DROP TABLE IF EXISTS `user_area`;
CREATE TABLE `user_area`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
